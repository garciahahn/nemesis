classdef FiniteElementTest < matlab.unittest.TestCase
    % FiniteElementTest tests the Finite Element implementation
    
    methods (Test)
        function elementCreation1D_Hermite1(testCase)
            System.init
            System.loadSettings(0)
    
            p=6; q=6; c=1;
            spatialDimension = 1;            
            spaceTime = false;
            
            for p=4:10
                q=p;
                standardElement = LSQdisc_5(p,q,c,spatialDimension,spaceTime);

                % Check some parameters
                actSolution = standardElement.dofType;
                expSolution = [ zeros(c+1,1) ; ones(p-2*(c+1),1) ; zeros(c+1,1) ];
                testCase.verifyEqual(actSolution,expSolution);

                actSolution = standardElement.dofTypeID;
                expSolution = [ ones(c+1,1) ; ones(p-2*(c+1),1) ; 2*ones(c+1,1) ];
                testCase.verifyEqual(actSolution,expSolution);

                actSolution = numel(standardElement.basis);
                testCase.verifyEqual(actSolution,1);
                
                testCase.verifyEqual( size(standardElement.dofe), [1 4] );
                
                actSolution = all(standardElement.refinementRatio(2:end));
                testCase.verifyEqual( actSolution, true );
                
                actSolution = class(standardElement.basis{1});
                testCase.verifyEqual(actSolution,'Hermite1');
            end
            
            
        end
        function elementCreation1D_p2q2c0(testCase)
            System.init
            System.loadSettings(0)
    
            p=2; q=2; c=0;
            spatialDimension = 1;            
            spaceTime = false;
            standardElement = LSQdisc_5(p,q,c,spatialDimension,spaceTime);
            actSolution = standardElement.dofType;
            expSolution = [0;0];
            testCase.verifyEqual(actSolution,expSolution);
        end
    end
    
end 