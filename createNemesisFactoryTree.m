function nemesisFactoryTree = createNemesisFactoryTree()
    nemesisFactoryTree = matlab.settings.FactoryGroup.createToolboxGroup('nemesis2','Hidden',false);

    toolboxFontGroup = addGroup(nemesisFactoryTree,'font','Hidden',false);
    addSetting(toolboxFontGroup,'FontSize','FactoryValue',11,'Hidden',false,'ValidationFcn',@matlab.settings.mustBeNumericScalar)    
    addSetting(toolboxFontGroup,'FontColor','FactoryValue','Black','Hidden',false,'ValidationFcn',@matlab.settings.mustBeStringScalar);
end