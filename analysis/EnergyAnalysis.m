classdef EnergyAnalysis
  %UNTITLED Summary of this class goes here
  %   Detailed explanation goes here
  
  properties
    Property1
  end
  
  methods
    function obj = untitled(inputArg1,inputArg2)
      %UNTITLED Construct an instance of this class
      %   Detailed explanation goes here
      obj.Property1 = inputArg1 + inputArg2;
    end
    
    function outputArg = method1(obj,inputArg)
      %METHOD1 Summary of this method goes here
      %   Detailed explanation goes here
      outputArg = obj.Property1 + inputArg;
    end
    

    function loadEnergy()
        instance = System.instance;

        energyFile = [System.settings.outp.directory System.settings.outp.energyFilename '.txt'];

        opts = detectImportOptions(energyFile);

        opts.VariableNamesLine          = 1;
        opts.Delimiter                  = {'\t',' ',','};
        opts.ConsecutiveDelimitersRule  = 'join';
        opts.LeadingDelimitersRule      = 'ignore';
        opts.VariableTypes(:)           = {'double'};

        instance.energyTable = readtable(energyFile,opts);

        % Fix to remove an empty column, which might appear by a white
        % space at the end of each line
        if (iscell(instance.energyTable{1,end}))
            if (isempty(instance.energyTable{1,end}{1}))
                emptyColumn = true;
                for i=1:size(instance.energyTable,1)
                    if ( ~isempty(instance.energyTable{i,end}{1}) )
                        emptyColumn = false;
                    end
                end
                if (emptyColumn)
                    instance.energyTable(:,end) = [];
                end
            end
        end

        if (System.rank==0)
            fprintf('SYSTEM     : The energy table has been loaded\n');
        end
    end

    function saveEnergy(energyBalance)
        instance = System.instance;

        %save(['saveEnergy.' int2str(System.rank) '.mat']);

        time = System.settings.time.current;

        energyFile = [System.settings.outp.directory System.settings.outp.energyFilename '.txt'];

        if (time==0)
            deltaG   = 0;
            total    = energyBalance.surface + energyBalance.totalKinetic + energyBalance.dissipation + deltaG;
            %total    = energyBalance.surface + energyBalance.totalKinetic + energyBalance.dissipation + energyBalance.phaseDissipation + deltaG;

            instance.energyTable = [ table(time), energyBalance, table(deltaG,total) ];                

        else
            % Summed dissipation

            %save(['saveEnergy.' int2str(System.rank) '.mat']);

            % Accumulate dissipation energies
%                 energyBalance.dissipation       = energyBalance.dissipation       + instance.energyTable.dissipation(end);
%                 energyBalance.divergence        = energyBalance.divergence        + instance.energyTable.divergence(end);
%                 energyBalance.phaseDissipation0 = energyBalance.phaseDissipation0 + instance.energyTable.phaseDissipation0(end);
%                 energyBalance.phaseDissipation1 = energyBalance.phaseDissipation1 + instance.energyTable.phaseDissipation1(end);
%                 energyBalance.phaseDissipation2 = energyBalance.phaseDissipation2 + instance.energyTable.phaseDissipation2(end);

            deltaG   = energyBalance.gravitational - instance.energyTable.gravitational(1);
            %total    = energyBalance.surface + energyBalance.totalKinetic + energyBalance.dissipation + deltaG;
            total    = energyBalance.surface + energyBalance.totalKinetic + energyBalance.dissipation + deltaG;

            currentEnergyTable   = [ table(time), energyBalance, table(deltaG,total) ];

            % Create a new table if no instance.energyTable is present,
            % or add to existing table
            if (~isempty(instance.energyTable))

                % Check if the time level is present already, and
                % replace it if it is
                if ( instance.energyTable.time(end) < time )
                    instance.energyTable = [instance.energyTable; currentEnergyTable];
                else                        
                    % Remove array for same time level
                    %flags = (instance.energyTable.time==time);
                    %instance.energyTable(flags,:) = [];

                    % Extract before & after arrays
                    flags   = (instance.energyTable.time<time);
                    before  = instance.energyTable(flags,:);

                    flags   = (instance.energyTable.time>time);
                    after   = instance.energyTable(flags,:);

                    instance.energyTable = [before; currentEnergyTable; after];

                    % Backup the existing energyFile so a new one will
                    % be created (normally only one line is appended)
                    System.preventOverwrite(energyFile)
                end
            else
                instance.energyTable = currentEnergyTable;
            end
        end

        % Write table to file (only last row)
        if (NMPI.instance.rank==0)
            if (time==0 || exist(energyFile,'file')~=2)                    
                System.writeHeader(energyFile,instance.energyTable.Properties.VariableNames);
                System.appendToFile(energyFile,instance.energyTable{1,:}')
            else
                System.appendToFile(energyFile,instance.energyTable{end,:}')
            end
        end
    end

    function out = getEnergyTable()
        instance = System.instance;
        out = instance.energyTable;
    end

    
  end
end

