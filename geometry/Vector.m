classdef Vector < handle
    %VECTOR Summary of this class goes here
    %   Detailed explanation goes here
    
    properties( Access = private )
        
    end
    
    properties
        direction = [0,0,0];
        isUnit = false;
        angle = 0;
    end
    
    methods
        function s = Vector(varargin)
            %VECTOR Construct an instance of this class
            %   Detailed explanation goes here
            if ( isa(varargin{1},'Angle') )
                s.angle = varargin{1};
                s.direction = s.getDirectionFromAngle;
            else
                s.direction = varargin{1};
            end
            
            if nargin>1
                % force a unit vector
                if (varargin{2})
                    s.makeUnit();
                end
            else
                s.checkUnit();
            end
        end
        
        function out = x(s)
            out = s.direction(1);
        end
        
        function out = y(s)
            out = s.direction(2);
        end
        
        function out = z(s)
            out = s.direction(3);
        end
        
%         function outputArg = method1(obj,inputArg)
%             %METHOD1 Summary of this method goes here
%             %   Detailed explanation goes here
%             outputArg = obj.Property1 + inputArg;
%         end
    end
    
    methods ( Access=private )
        function direction = getDirectionFromAngle(s,angle)
            if (nargin==1)
                angle = s.angle;
            end
            direction = [ cos( angle.inRadians ), sin( angle.inRadians ), 0 ];
        end
        
        function s = makeUnit(s)
            s.direction = s.direction / norm(s.direction);
            s.isUnit = true;
        end
        
        function s = checkUnit(s)
            if (norm(s.direction)==1)
                s.isUnit = true;
            else
                s.isUnit = false;
            end
        end
    end
end

