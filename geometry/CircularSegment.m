%CIRCULARSEGMENT A circular segment is the cut off region of a circle
%   This class can be used to create a circular segment and derive several quantities of interest from it.
%   The variable name are related with the plot on Wikipedia (https://en.wikipedia.org/wiki/Circular_segment)
%
classdef CircularSegment < matlab.mixin.SetGet

  properties
    id              % 
    radius          % R
    chord           % c
    theta           % angle
    arc_length      % s
    apothem         % d: height of the triangular portion
    sagitta         % h: height of the segment
    area            % A: area of the segment
  end % properties
  
  methods
    
    %% CONSTRUCTORS
    %CIRCULARSEGMENT Construct an instance of this class
    %   The object can be constructed from
    function self = CircularSegment( id )
      if (nargin==0)
        self.id = 0;
      else
        self.id = id;
      end
    end % CircularSegment
    
    %% OBJECT FUNCTIONS
    %DERIVE Derive remaining values from provided values
    %   Use the area and theta to derive all other values (for equations see Wikipedia)
    function derive( self )
      if ( ~isempty(self.area) && ~isempty(self.theta) )
        self.radius     = sqrt( ( 2*self.area ) / ( 2*self.theta.inRadians - sin( 2*self.theta.inRadians ) ) );
        self.chord      = 2*self.radius * sin( self.theta.inRadians );
        self.arc_length = 2*self.theta.inRadians * self.radius;
        self.apothem    = self.radius * cos( self.theta.inRadians );
        self.sagitta    = self.radius - self.apothem;
      else
        error('Please set the area and theta first')
      end
    end % derive

    %% SET FUNCTIONS
    function set.theta( self, angle )
      if ( isa(angle,'Angle') )
        self.theta = angle;
      else
        error('The volume must be op type Angle')
      end
    end % set.theta
    
    function set.area( self, area )
      if ( isnumeric(area) )
        if ( area < 0 )
          error('The area should be positive')
        else
          self.area = area;
        end
      else
        error('The area must be numeric')
      end
    end % set.area
  end % methods
  
end % classdef