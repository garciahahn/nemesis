classdef Point < handle
    %POINT Geometry object
    
    properties( Constant, Access=private )
        MAXDIMENSION = 4;
    end
    
    properties( Constant )
        dimension = 1;
    end
    
    properties
        location = zeros(Point.MAXDIMENSION,1);
    end
    
    methods
        function s = Point(location)
            %POINT Construct a Point with a certain location
            s.location( 1:numel(location) ) = location;
        end
        
        function x = x(s)
            %X Returns the x-location of this point
            x = s.location(1);
        end
        
        function y = y(s)
            %Y Returns the y-location of this point
            y = s.location(2);
        end
        
        function z = z(s)
            %Z Returns the z-location of this point
            z = s.location(3);
        end
        
        function t = t(s)
            %T Returns the t-location of this point
            t = s.location(4);
        end
    end
    
    methods (Access=private)
        
    end
end

