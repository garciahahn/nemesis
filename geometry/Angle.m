classdef Angle < handle
    %ANGLE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties( Access=private, Constant )
        radiansPerDegree = pi / 180;
        degreesPerRadian = 180 / pi;
    end
    
    properties
        value
        unit
    end
    
    methods
        function s = Angle(value,unit)
            %ANGLE Construct an instance of this class
            %   Detailed explanation goes here
            s.value = value;
            
            if (isa(unit,'AngleUnits'))
                s.unit = unit;
            else
                error('Please provide a unit of type AngleUnits')
            end
        end

        function out = inDegrees(s)
            %INDEGREES Return the angle in degrees
            if (s.unit==AngleUnits.Degrees)
                out = s.value;
            else
                out = s.value * Angle.degreesPerRadian;
            end
        end
        
        function out = inRadians(s)
            %INRADIANS Return the angle in radians
            if (s.unit==AngleUnits.Radians)
                out = s.value;
            else
                out = s.value * Angle.radiansPerDegree;
            end
        end
    end
end

