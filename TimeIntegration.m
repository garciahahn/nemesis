classdef TimeIntegration
    %TIMEINTEGRATION Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        timeMethod
    end
    
    methods
%         function obj = TimeIntegration(inputArg1,inputArg2)
%             %TIMEINTEGRATION Construct an instance of this class
%             %   Detailed explanation goes here
%             obj.Property1 = inputArg1 + inputArg2;
%         end
        
%         function outputArg = method1(obj,inputArg)
%             %METHOD1 Summary of this method goes here
%             %   Detailed explanation goes here
%             outputArg = obj.Property1 + inputArg;
%         end
    end
    
    methods (Static)
        
        function out = getTime(timeMethod,mesh,element,field)
            
            activeSettings = System.settings;
            
            if nargin==1
                switch timeMethod
                    case TimeMethod.SpaceTime
                        out = 0;

                    case TimeMethod.BDF2
                        out = 3/2 / activeSettings.time.step;

                    case TimeMethod.EulerImplicit
                        out = 1 / activeSettings.time.step;
                        
                    case TimeMethod.Steady
                        out = 1; 
                end
                
            elseif nargin==3
                switch timeMethod
                    case TimeMethod.SpaceTime
                        out = element.getDt;

                    case TimeMethod.BDF2
                        out = 3/2 * element.getH / activeSettings.time.step;

                    case TimeMethod.EulerImplicit
                        out = element.getH / activeSettings.time.step;
                        
                    case TimeMethod.Steady
                        out = element.getH; 
                end
                
            elseif nargin==4
                switch timeMethod
                    case TimeMethod.SpaceTime
                        out = element.getVecZ;

                    case TimeMethod.BDF2                    
                        v1  = field.oldVal(element.id,mesh,1);
                        v2  = field.oldVal(element.id,mesh,2);
                        out = (2*v1 - 0.5*v2) / activeSettings.time.step;

                    case TimeMethod.EulerImplicit
                        v1  = field.oldVal(element.id,mesh,1);
                        out = v1 / activeSettings.time.step;
                        
                    case TimeMethod.Steady
                        %v1  = field.oldVal(element.id,s.mesh,1);
                        v1  = field.val2(element.id,mesh);
                        out = v1;%/ System.settings.time.step;
                end
            end
        end
        
        function out = getTimeStep()
            activeSettings = System.settings;
            out = activeSettings.time.step;
        end
        
        
        
%         function [Dt,Dt_rhs] = getTimeCombined(timeMethod,mesh,element,field)
%             
%             selectedTimeMethod = timeMethod;
%             
%             while isempty(Dt_rhs)
%                 Dt_rhs = TimeIntegration.getDt_rhs(  )
%             end
%             
%             % Get Dt_rhs, 
%             switch selectedTimeMethod
%                 case TimeMethod.SpaceTime
%                     Dt_rhs = element.getVecZ;
% 
%                 case TimeMethod.BDF2                    
%                     v1  = field.oldVal(element.id,mesh,1);
%                     v2  = field.oldVal(element.id,mesh,2);
%                     Dt_rhs = (2*v1 - 0.5*v2) / System.settings.time.step;
% 
%                 case TimeMethod.EulerImplicit
%                     v1  = field.oldVal(element.id,mesh,1);
%                     Dt_rhs = v1 / System.settings.time.step;
% 
%                 case TimeMethod.Steady
%                     %v1  = field.oldVal(element.id,s.mesh,1);
%                     v1  = field.val2(element.id,mesh);
%                     Dt_rhs = v1;%/ System.settings.time.step;
%             end
%             
%             
%             
%             switch timeMethod
%                 case TimeMethod.SpaceTime
%                     out = 0;
% 
%                 case TimeMethod.BDF2
%                     out = 3/2 / System.settings.time.step;
% 
%                 case TimeMethod.EulerImplicit
%                     out = 1 / System.settings.time.step;
% 
%                 case TimeMethod.Steady
%                     out = 1; 
%             end
%                 
%             elseif nargin==3
%                 switch timeMethod
%                     case TimeMethod.SpaceTime
%                         out = element.getDt;
% 
%                     case TimeMethod.BDF2
%                         out = 3/2 * element.getH / System.settings.time.step;
% 
%                     case TimeMethod.EulerImplicit
%                         out = element.getH / System.settings.time.step;
%                         
%                     case TimeMethod.Steady
%                         out = element.getH; 
%                 end
%                 
%             elseif nargin==4
%                 
% 
%         end
%         
%         function out = getDt_rhs()
%             
%         
%         end
        
        
    end
end

