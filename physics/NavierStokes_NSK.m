classdef NavierStokes_NSK < Physics
    %NAVIERSTOKES_NSK Implementation of the energy equation for NSK
    %   Detailed explanation goes here    
    properties (Access=private)
        REI; WEI;
    end
    
    properties (Constant)
        % Variable ids for this physics
        R = 1;
        u = 2;
        v = 3;
        q = 4;
    end
    
    properties
        Re; We; k; cv;
    end
    
    methods
        function s = NavierStokes_NSK(mesh,settings)
            s = s@Physics(mesh);
            
            s.shortName = 'NSK';
            s.longName  = 'NavierStokesKorteweg';
            s.name      = 'NSK';
            
            s.nEq       = 4;
            s.variables = {'R','u','v','q'};
            
            s.initialize(settings);
        end
        
        function initialize(s,settings)
            initialize@Physics(s,settings);

            s.steady    = settings.steady;
            
            s.Re        = System.settings.phys.Re;
            s.We        = System.settings.phys.We;
            s.k         = System.settings.phys.k;
            s.cv        = System.settings.phys.cv;
            
            s.REI       = 1.0/s.Re;
            s.WEI       = 1.0/s.We;
        end

        function time = setupEquations(s)
            % Function to setup the NSK equation. These equations can be 
            % found in the following article of Keunsoo (section 2):
            %   Thermal two-phase flow with a phase-field method
            %   Int. Journal of Multiphase Flow, 2019
            %
            t1 = toc;
            
            U	= FieldCollection.get('u');
            V   = FieldCollection.get('v');
            R   = FieldCollection.get('R');
            q   = FieldCollection.get('q');
            T   = FieldCollection.get('T');
                        
            for e=s.mesh.eRange
                element	= s.mesh.getElement(e);         % Select the active element
                el      = element.localID;              % Get the local element number
                
                %% elemental operators
                matZ    = element.getMatZ;
                vecZ    = element.getVecZ;
                H       = element.getH;
                Dx      = element.getDx;
                Dy      = element.getDy;
                if (s.steady)
                    Dt       = 0;
                    dUdt_rhs = vecZ;
                    dVdt_rhs = vecZ;
                else
                    Dt          = s.getTime( element );
                    dRdt_rhs    = s.getTime( element, R );
                    dUdt_rhs    = s.getTime( element, U );
                    dVdt_rhs    = s.getTime( element, V );
                end
                Dxx     = element.getDxx;
                Dxy     = element.getDxy;
                Dyy     = element.getDyy;
                                
                %% u-velocity derivatives
                Uk      = U.val2(e,s.mesh);
                Ux      = U.x(FieldType.Current,e);
                Uy      = U.y(FieldType.Current,e);
                Ut      = U.t(FieldType.Current,e);
                
                %% v-velocity derivatives
                Vk      = V.val2(e,s.mesh);
                Vx      = V.x(FieldType.Current,e);
                Vy      = V.y(FieldType.Current,e);
                Vt      = V.t(FieldType.Current,e);
                
                %% density derivatives
                Rk      = R.val2(e,s.mesh);
                Rx      = R.x(FieldType.Current,e);
                Ry      = R.y(FieldType.Current,e);
                
                %% q derivatives (q is the Laplacian of the density R, so q = Rxx + Ryy)
                qx      = q.x(FieldType.Current,e);
                qy      = q.y(FieldType.Current,e);
                
                %% T derivatives
                Tk      = T.val2(e,s.mesh);
                Tx      = T.x(FieldType.Current,e);
                Ty      = T.y(FieldType.Current,e);
                
                %% auxiliary terms
                convU   = Ut + Uk.*Ux + Vk.*Uy;
                convV   = Vt + Uk.*Vx + Vk.*Vy;
                divU    = Ux + Vy;
                Conv    = Dt + Uk.*Dx + Vk.*Dy; 
                Lap     = Dxx + Dyy;
                
                %PR      = 24./(3-Rk).^2 - 6*Rk;
                PR      = 24*Tk./(3-Rk).^2 - 6*Rk;
                PT      = 8*Rk./(3-Rk);

                ConvRR = Conv + divU.*H;
                ConvRU = Rk.*Dx + Rx.*H;
                ConvRV = Rk.*Dy + Ry.*H;

                ConvUR = PR.*Dx + (convU - qx).*H;
                %ConvUR = PR.*Dx + (convU - s.WEI*qx).*H;
                ConvUU = Rk.*Conv  - s.REI/3 * Dxx - s.REI * Lap + Rk.*Ux.*H;
                ConvUV = Rk.*Uy.*H - s.REI/3 * Dxy;

                ConvVR = PR.*Dy + (convV - qy).*H;
                %ConvVR = PR.*Dy + (convV - s.WEI*qy).*H;
                ConvVU = Rk.*Vx.*H - s.REI/3 * Dxy;
                ConvVV = Rk.*Conv  - s.REI/3 * Dyy - s.REI * Lap + Rk.*Vy.*H;

                grN = dRdt_rhs + Uk.*Rx + Rk.*Ux + Vk.*Ry + Rk.*Vy;
                %guN = dUdt_rhs + Rk.*Ut + 2*(Rk.*Uk).*Ux + 2*(Rk.*Vk).*Uy - s.WEI * Rk.*qx - PT.*Tx;
                %gvN = dVdt_rhs + Rk.*Vt + 2*(Rk.*Uk).*Vx + 2*(Rk.*Vk).*Vy - s.WEI * Rk.*qy - PT.*Ty;
                guN = dUdt_rhs + Rk.*Ut + 2*(Rk.*Uk).*Ux + 2*(Rk.*Vk).*Uy - Rk.*qx - PT.*Tx;
                gvN = dVdt_rhs + Rk.*Vt + 2*(Rk.*Uk).*Vx + 2*(Rk.*Vk).*Vy - Rk.*qy - PT.*Ty;
            
                %% Assign L and G
                    %            r         u          v           q
                s.opL{el} = [ ConvRR     ConvRU     ConvRV      matZ         ; ...
                              ConvUR     ConvUU     ConvUV    -Rk.*Dx   ; ...
                              ConvVR     ConvVU     ConvVV    -Rk.*Dy   ; ...
                              s.WEI*Lap   matZ       matZ        -H        ] ;
                         
                s.opG{el} = [   grN    ; ...
                                guN    ; ...
                                gvN    ; ...
                                vecZ ] ;

                %%% Scale equations with weights
                %eqW = repmat(s.eqWeights',numel(vecZ),1);
                %s.opL{el} = eqW(:) .* s.opL{el};
                %s.opG{el} = eqW(:) .* s.opG{el};
                %
                %%% Apply AMR scaling
                %s.opL{el}   = s.opL{el} * superkron(eye(s.Nvar),s.mesh.Z{e});
                %
                %% Create SEM weight
                %s.W{el}     = repmat(Wel,s.Nvar,1);
                %s.sqrtW{el} = repmat(sqrt(Wel),s.Nvar,1);
                
                %% Set quadrature weights, apply all scaling to L and G
                s.setWeightAndScale( element );
            end
            
            t2 = toc;
            time = t2-t1;
        end              

        function updateBC(s)
            % do nothing
        end

        function prescribed = getPrescribed(s,e)
            if nargin==2
                prescribed = [s.fields{1}.getPrescribed(e)];
            else
                prescribed = cell(s.mesh.numLocalElements,1);
                for e=1:numel(prescribed)
                    element = s.mesh.getLocalElement(e);
                    prescribed{e} = s.getPrescribed( element.globalID );
                end
            end
        end
        
        function out = getPrescribedValues(s,e)
            out = [s.fields{1}.getPrescribedValues(e)];
        end
        
        % The mean temperature (Tm) is defined by: Tm = Et_dot / m_dot
        %
        % See p.427 of Fundamentals of Heat & Mass Transfer, 
        % 4th edition, 1996, authors: Incropera & DeWitt
        %
        % Parameters:
        %  boundary     plane to integrate over
        %  disc         discretization (optional, for over-integration)
        %
        function Tm = meanTemperature(s,boundary,disc_in)
            % Initialize the variables
            Et_dot  = 0;
            m_dot   = 0;
            Tm      = 0;
            
            U = FieldCollection.get('u');
            V = FieldCollection.get('v');
            C = FieldCollection.get('c');
            
            %cv  = 1;
            %rho = 1;
            %rho         = (1-s.lamRho)*Ck + s.lamRho;    
            %k           = (1-s.lamk)*Ck + s.lamk;
            %cp          = (1-s.lamcp)*Ck + s.lamcp;
            
            R = 8.3144598;
            
            if (System.settings.phys.dynamicReferenceFrame)
                CH = PhysicsCollection.get(2);
                CH.computeCenterOfMassVelocity(true,false);       % determine the center-of-mass velocity: compute the enclosed mass, and without overIntegration
                uc = CH.cmv_total(:,1);
                vc = CH.cmv_total(:,2);
            end
            
            % If no C is available assume C=1 and vc=0
            if (C~=C)
                rho = @(e,disc) 1;
                cv  = @(e,disc) R + (1-s.lamcp) * System.settings.phys.cp(1);
                uc = 0;
                vc = 0;
            else
                rho = @(e,disc) (1-s.lamRho)*C.getValueInterpolated( e ,disc, s.mesh) + s.lamRho;
                cv  = @(e,disc) R + ((1-s.lamcp)*C.getValueInterpolated( e ,disc, s.mesh) + s.lamcp) * System.settings.phys.cp(1);
            end

%             if (boundary.getBase.spaceDimension==1)
%                 if (boundary.dir==2)
%                     vel = @(e,disc) U.getValueInterpolated( e ,disc, s.mesh) + mean(uc);
%                 elseif (boundary.dir==1)
%                     vel = @(e,disc) V.getValueInterpolated( e ,disc, s.mesh) + mean(vc);
%                 end
%             else
                if (boundary.dir==1)
                    vel = @(e,disc) U.getValueInterpolated( e ,disc, s.mesh) + mean(uc);
                elseif (boundary.dir==2)
                    vel = @(e,disc) V.getValueInterpolated( e ,disc, s.mesh) + mean(vc);
                end
%             end
            T   = @(e,disc) s.fields{1}.getValueInterpolated( e ,disc, s.mesh);
            
            Et      = @(e,disc) rho(e,disc) .* vel(e,disc) .* cv(e,disc) .* (T(e,disc));
            Et_dot  = boundary.computeIntegral( Et, false );
            
%             if (Et_dot==0)
%                 s.fields{1}.add( 1 );
%                 Et      = @(e,disc) rho(e,disc) .* vel(e,disc) .* cv(e,disc) .* (T(e,disc));
%                 Et_dot  = boundary.computeIntegral( Et, false );
%             end
            
            m       = @(e,disc) rho(e,disc) .* vel(e,disc) .* cv(e,disc);
            m_dot   = boundary.computeIntegral( m, false );
            
            % For verification purposes:
            %t       = @(e,disc) 1;
            %area    = boundary.computeIntegral( t, false );
            
            Tm = Et_dot ./ m_dot;
            
            if (Parallel3D.MPI.rank==0)
                %fprintf('Total mass : %f (effective radius : %f)\n',s.mass,sqrt(s.mass/pi))
                fprintf('Et_dot = % 8.2e\n',Et_dot);
                fprintf('m_dot  = % 8.2e\n',m_dot);
                fprintf('Tm     = % 8.2e\n',Tm);
            end
        end
        
        function plotNusselt(s)            
            layer = 1;
            
            id = 0;
            for e=s.mesh.eRange %1:numel(s.alpha)
                id = id+1;
                element = s.mesh.getElement(e);
                
                field = s.fields{1};
                
                T  = field.val2(e,s.mesh);
                Tx = field.y2(e,s.mesh);
                
                [X,Y,Z,T] = element.getNodes;
                X = squeeze(X); Y = squeeze(Y);
                Z = squeeze(Z); T = squeeze(T);
                
                Tm = 0;
                
                data = Tx ./ (0-T);
                data = reshape( data, element.finite.qSize );
                
                if (System.settings.time.spaceTime)
                    if (System.settings.mesh.sdim==2)
                        if (layer==1)
                            plotData = data(:,:,end);
                        elseif (layer==0)
                            plotData = data(:,:,1);
                        end
                        surf(X(:,:,end),Y(:,:,end),plotData,'FaceColor','interp');
                    else
                        if (layer==1)
                            plotData = data(:,end);
                        elseif (layer==0)
                            plotData = data(:,1);
                        end
                        surf(X,T,data,'FaceColor','interp');
                    end
                else
                    plotData = data;
                    if (System.settings.mesh.sdim==1)
                        plot(X(:,end),plotData,'Color','k');
                    elseif (System.settings.mesh.sdim==2)
                        surLSQf(X,Y,plotData,'FaceColor','interp');
                    else
                        error('Check Subfield.plot : only sDim == 1 and == 2 are supported')
                    end
                end
                
                hold on;
            end
            
            if (System.settings.mesh.dim==3)
                x0 = s.mesh.X(1,1);
                x1 = s.mesh.X(2,1);
                y0 = s.mesh.X(1,2);
                y1 = s.mesh.X(2,2);

                pbx = x1 - x0;
                pby = y1 - y0;
                pbaspect([pbx pby 1])

                %zlim([-10 10]);
                
                colorbar;
                view([25,25]);                
            end
        end

        function output(s,outputDir,id)
            data.t = s.getT( 1, s.getElementalAlpha(Variable.t) );
            for e=Parallel3D.MPI.eStart:Parallel3D.MPI.eEnd
                data.t{e} = reshape(data.t{e},s.mesh.getElement(e).disc.Q);
                data.t{e} = data.t{e}(:,:,end);
            end
                        
            for e=1:s.mesh.getNumLocalElements
                data.x{e,1} = s.mesh.getLocalElement(e).xelem;
                data.y{e,1} = s.mesh.getLocalElement(e).yelem;
            end
            
            data.eStart = Parallel3D.MPI.eStart;
            data.eEnd   = Parallel3D.MPI.eEnd;
            
            save([outputDir 'info-' num2str(id,'%.4d')  '--' num2str(Parallel3D.MPI.rank,'%.3d') '.mat'],'data') ;
        end   

        function plotExact(s,fig,position)
            if nargin==2
                position = 1;
            end
            
            s.plotMode = 1;
            s.plot(fig,position);
            s.plotMode = 0;
        end
        
        function plotInterpolated(s,fig,position)
            if nargin==2
                position = 1;
            end
            
            s.plotMode = 2;
            s.plot(fig,position);
            s.plotMode = 0;
        end
        
        function plotAll(s,fig,data,mesh)
            
            if nargin==3
                mesh = s.mesh;
            end
            
            screensize = get( groot, 'Screensize' );
            sizex = 1600;
            sizey = 600;
            set(fig,'position',[0.5*(screensize(3)-sizex) 0.5*(screensize(4)-sizey) sizex sizey]); 
            
            rows = 1;
            cols = 1;
            
            pbx = s.mesh.x1 - s.mesh.x0;
            pby = s.mesh.y1 - s.mesh.y0;
            
            
            % T-plot
            t = data{1};
            minT    = min( cellfun(@(x) min(x(:)),t) );
            maxT    = max( cellfun(@(x) max(x(:)),t) );
            
            maxT = 1;
            
            pos = 1;
            delete(subplot(rows,cols,pos))
            subplot(rows,cols,pos)
            xlim([mesh.x0 mesh.x1])
            ylim([mesh.y0 mesh.y1])
            %zlim([minU    maxU   ])
            pbz = maxT - minT;
            pbaspect([pbx pby pbz])
            view([0,90])
            grid off;
            hold on;
            
            for e=1:mesh.numLocalElements
                element = mesh.getLocalElement(e);
                surf(element.xelem,element.yelem,t{e},'FaceColor','interp');
            end
            
%             % U-velocity at x=0.5
%             pos = 4;
%             delete(subplot(rows,cols,pos))
%             subplot(rows,cols,pos)
%             for e=1:mesh.numLocalElements
%                 element = mesh.getLocalElement(e);
%                 z=interp2(element.xelem,element.yelem,u{e},0.5,element.yelem);
%                 plot(element.yelem,z,'k');
%                 hold on;
%             end
%             xlim([0 1])
%             view([90,-90])
%             
%             % V-velocity at x=0.5
%             pos = 5;
%             delete(subplot(rows,cols,pos))
%             subplot(rows,cols,pos);
%             for e=1:mesh.numLocalElements
%                 element = mesh.getLocalElement(e);
%                 z=interp2(element.xelem,element.yelem,v{e},0.5,element.yelem);
%                 plot(element.yelem,z,'k');
%                 hold on;
%             end
%             ylim([-1e-3 1e-3])
%             view([90,-90]) 
        end
        
        % Default refinement criteria (false)
        function criteria = getRefinementCriteria(s,e)
            criteria(1) = false;
        end
        
%         function out = save(s)
%             out = [];
%             
%             if (Parallel3D.MPI.rank==0 || System.settings.outp.parallelWrite)
%                 EE                = struct;
%                 EE.class          = class(s);
%                 EE.name           = s.name;
%                 EE.mesh           = s.mesh.save;
%                 
%                 EE.settings       = s.settings;
%                 
%                 EE.fields       = cell(s.Nvar,1);
%                 for n=1:s.Nvar
%                     EE.fields{n} = save( s.fields{n} );
%                 end
%                 
%                 % Additional NS settings
%                 EE.steady     	= s.steady;                 % True if the system of equations is steady
%                 EE.fdata        = s.fdata;
%                 EE.exact        = s.exact;                  % True if this physics has an exact solution
%                 EE.maxLevel     = s.maxLevel;               % Maximum level for AMR
%                 EE.eqWeights    = s.eqWeights;           	% Weights for every equation
%                 EE.nConvType    = s.nConvType;              % Residual convergence criteria based on [ value; residual ]
%                 EE.cConvType    = s.cConvType;              % Coupling convergence criteria based on [ value; residual ]
%                 EE.nMaxIter     = s.nMaxIter;           	% Maximum number of nonlinear iterations
%                 EE.cMaxIter     = s.cMaxIter;           	% Maximum number of coupling iterations
%                 EE.nConvergence = s.nConvergence;       	% Nonlinear convergence for each variable
%                 EE.cConvergence = s.cConvergence;        	% Coupling convergence for each variable
%                 EE.rConvergence = s.rConvergence;           % Residual convergence for each equation and the whole system
%                 
%                 out = EE;
%             end
%         end
%         
       	function preTimeLoop(s)
            if (s.usePreTimeLoop)
                
                s.preTimeStep();
                
                %% Apply boundary conditions
                s.resetBoundaryConditions();
                Settings.setBoundaryConditions( s );

                % By solving the steady equations, the solution will be
                % obtained faster
                s.steady    = true;
                s.coupled   = true;

    %             %% Apply temporal conditions
    %             if (System.settings.time.spaceTime)
    %                 s.updateTimeSlab();
    %                 if (~s.steady)
    %                     s.applyInitialConditions();
    %                 end
    %             elseif ~isempty(System.settings.time.steppingMethod)
    %                 physics.copySolution(FieldType.Timelevel1);
    %             end

                %% Solve nonlinear equations                
                s.solveNonLinear();

                s.steady    = s.settings.steady;
                s.coupled   = s.settings.coupled;
            end
        end
        
        function out = addDroplet(s,rhoArray,x,y,~,~,center,radius)
            %function w = Rho_begin(x,y,xc,yc,r0,Rho1,Rho2,We)
    
            % old Keunsoo code
            %out = (Rho1+Rho2)/2 - (Rho1-Rho2)/2*tanh((sqrt((x-xc).^2+(y-yc).^2)-r0)*sqrt(We)/2) ;
            %out = Rho2*ones(size(x)) ;
            
            Rho1 = rhoArray(1);
            Rho2 = rhoArray(2);
            
            r0 = radius;
            xc = center(1);
            yc = center(2);
            
            d  = r0 - sqrt((x-xc).^2+(y-yc).^2);        % positive inside the droplet
            
            out = (Rho1+Rho2)/2 + (Rho1-Rho2)/2 * tanh( d * sqrt(s.We)/2 );
        end
        
        function deriveQ(s,variableNumber)
            qField = s.fields{variableNumber};
            
            for e=s.mesh.eRange
                element = s.mesh.getElement(e);
                el = element.localID;

                rField = s.fields{1};
                Rxx = rField.xx2( e );
                Ryy = rField.yy2( e );

                qValues = s.WEI * ( Rxx + Ryy );

                qField.setValue(el,qValues);
            end
            
            qField.computeAlpha;
        end
    end
    
    methods (Static)
        function out = load(solution)
            if (~isa(solution,'Solution'))
                error('The first argument of load should be a Solution object')
            end
            
            out = EnergyEquation_NSK;
            out.loadObject(solution);
        end 
    end
    
end

