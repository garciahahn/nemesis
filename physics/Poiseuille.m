classdef Poiseuille < NavierStokes
    %Poiseuille 2D Steady Navier-Stokes benchmark
    %   Detailed explanation goes here
        
    methods
        function s = Poiseuille(mesh,settings)
            s = s@NavierStokes(mesh,settings);
            
            if nargin>0
                s.shortName = 'Po';
                s.longName  = 'Poiseuille';
                s.name      = 'Poiseuille';

                s.steady  = true;
                %s.initialize(settings);
            end
        end
    end
    
    methods (Access=protected)
        function loadObject(s,solution)
            % load from parent
            loadObject@NavierStokes(s,solution)
            
            % specific loads for the Poiseuille object
            % none
        end
    end
    
    methods (Static)
        function out = load(s)
            out = Poiseuille();
            out.loadObject(s);
        end
    end
    
end

