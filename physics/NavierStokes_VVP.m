classdef NavierStokes_VVP < Physics
    % Navier Stokes system with Velocity Vorticity Pressure formulation
    
    
    properties (Access=protected)
        % Gravity cartesian components
        gx; gy;
        
        % Inverses of non-dimensional parameters
        REI; FRI2; WEI;
        
        % NS non-dimensional parameters
        Ja; Pr; Re; We; Fr; alpha; Cn_L;
        % JULIAN: What is this for?
        % DOC!
        lamRho, lamMu;
        
        gradP = zeros(3,1);
        oldMean = 1;
        
        % Default values
        % DOC!
        sharpRho    = false;
        sharpMu     = false;
        useMassFlux = false;
        eps         = 0.1;

        kx_all, ky_all;
        
        maxVelocityNorm;

    end
    
    properties 
        % Fields
        stField;
        
        % JULIAN ADDED
        % DOC!
        % Some weights for something
        dynamicWeight = false;
        computeDynamicWeight = false;
        weight = 1;

        % JULIAN ADDED
        % Selected linearization solving method
        % Currently there are two methods implemented:
        %     - Piccard
        %     - Newton
        % Possible selection for this parameter can be seen in the
        % enumeration file 'Linearization.m'
        linearization;
        
        % DOC!
        withInterfaceForcing = true;
        zeroGravity = false;
    end
    
    methods
        % Class constructor
        function s = NavierStokes_VVP( mesh, settings )
            s = s@Physics( mesh );
                        
            if nargin > 0
                s.shortName = 'NS-VVP';
                s.longName  = 'Navier-Stokes Velocity-Vorticity-Pressure Formulation';
                s.name = 'NS-VVP';
                   
                % nEq: number of equations
                s.nEq = 4;
%{
                JULIAN ADDED
                  - u:   velocity component in the x direction
                  - v:   velcotiy component in the y direction
                  - w_z: vorticity component in the z direction
                  - p:   pressure
%}
                s.variables = { 'u', 'v', 'w_z', 'p'};
                s.initialize(settings);
            end % if
        end % function NavierStokes_VVP
        
        %% Initialization
        function initialize(s,settings)
            initialize@Physics(s,settings);
            
            s.Re     = System.settings.phys.Re;   
            s.We     = System.settings.phys.We;
            s.Fr     = System.settings.phys.Fr;
            s.Pr     = System.settings.phys.Pr;
            s.Ja     = System.settings.phys.Ja;

            s.alpha  = System.settings.phys.alpha;
            s.lamRho = System.settings.phys.lamRho;
            s.lamMu  = System.settings.phys.lamMu;
            s.Cn_L   = System.settings.phys.Cn * System.settings.phys.radius;   % Cahn number

            s.linearization = System.settings.phys.linearization;
            
            % JULIAN MOD
            if ( s.Re > 0 )
                s.REI = 1.0/s.Re;
            else
                s.REI = 0;
            end % if

            if ( s.We > 0 )
                s.WEI = 1.0/s.We;
            else
                s.WEI = 0;
            end % if

            if ( s.Fr > 0 )
                s.FRI2 = 1.0/s.Fr;
            else
                s.FRI2 = 0;
            end % if               

            if ( System.settings.phys.g == 0 )
                s.gx = 0;
                s.gy = 0;
            else
                s.gx = System.settings.phys.g(1);
                s.gy = System.settings.phys.g(2);
            end % if
            
            % Navier-Stokes specific settings
            % DOC!
            if ( isfield( settings, 'eps' ) )
                s.eps = settings.eps;
            end % if
            
            % DOC!
            if ( isfield( settings, 'sharpRho' ) )
                s.sharpRho = settings.sharpRho;
            end % if
            
            % DOC!
            if ( isfield( settings, 'sharpMu' ) )
                s.sharpMu = settings.sharpMu;
            end % if
            
            % DOC!
            if ( isfield( settings, 'pressureGradient' ) )
                s.gradP = settings.pressureGradient;
            end % if
            
            % JULIAN REQUEST
            % Change this check to default passive function
            if ( isfield( settings, 'usePreTimeLoop' ) )
                s.usePreTimeLoop = settings.usePreTimeLoop; 
            end % if
            
            % JULIAN REQUEST
            % Idem as previous item
            if ( isfield( settings, 'usePreTimeStep' ) )
                s.usePreTimeStep = settings.usePreTimeStep; 
            end % if
            
            % DOC!
            if ( isfield( settings, 'useMassFlux' ) )
                s.useMassFlux = settings.useMassFlux;
            end % if
        end % function initialize
        
        %% Setting up equations
        % Setup the NS equations (space-time)
        function time = setupEquations_spaceTime(s)
            t1 = toc;

            % JULIAN ADDED
            % Coupled is currently working only for the Cahn Hilliard case
            if ( System.settings.comp.coupling.enabled && s.coupled )
                C   = FieldCollection.get('c');
                Omg = FieldCollection.get('omg');
                CH = PhysicsCollection.get('CH');
                
                % Get the curvature from the CH simulation, use the NS.mesh as
                % the base mesh for the data
                [kx,ky] = CH.getCurvature(s.mesh);
                diffusiveFlowRate = CH.getDiffusiveFlowRate(s.mesh);
                
                if (s.useMassFlux)
                    massFlux = CH.getMassFlux(s.mesh);
                end

                % Droplet/bubble center of gravity velocity at temporal
                % quadrature points
                if (System.settings.phys.dynamicReferenceFrame)
                    % determine the center-of-mass velocity: compute the enclosed mass, and without overIntegration
                    CH.computeCenterOfMassVelocity(true,false);
                    
                    uc = CH.cmv_total(:,1) - System.settings.phys.bubbleVelocity(1);
                    vc = CH.cmv_total(:,2) - System.settings.phys.bubbleVelocity(2);
                    
                    absCenterVelocity = sqrt( uc.^2 + vc.^2 );
                else
                    uc  = [];
                    vc  = [];
                    absCenterVelocity = [];
                end % if
            else
                C   = NaN;
                Omg = NaN;
                kx  = [];
                ky  = [];
                uc  = [];
                vc  = [];
                absCenterVelocity = [];
            end % if
            
            U = s.getField( Variable.u );
            if ( s.mesh.sDim > 1 )
                V = s.getField( Variable.v );
                W_z = s.getField( Variable.w_z );
            end % if
            P = s.getField( Variable.p );
            
            % JULIAN ADDED
            % Cycle through elements
            for e = s.mesh.eRange
                % Select the active element
                element    = s.mesh.getElement( e );
                element_ID = element.localID;
                
                % From CH (2=SolutionMode.Coupling)
                % JULIAN ASK
                % Why C ~= C?
                if ( C ~= C || isempty( C ) )
                    Ck    = 1;
                    dCkdx = 0;
                    dCkdy = 0;
                    dCkdt = 0;
                else
                    Ck    = C.val2( e, s.mesh );
                    dCkdx = C.x2( e, s.mesh );
                    dCkdy = C.y2( e, s.mesh );
                    dCkdt = C.t2( e, s.mesh );
                end % if

                if ( Omg ~= Omg || isempty( Omg ) )
                    Omgk     = 0;
                    nablaOmg = 0;
                else
                    Omgk     = Omg.val2(e,s.mesh);
                    nablaOmg = Omg.xx2(e,s.mesh) + Omg.yy2(e,s.mesh);
                end % if

                gradC = sqrt( dCkdx.^2 + dCkdy.^2 );
                
                %% Density
                if ( ~s.sharpRho )
                    rho     = ( 1 - s.lamRho ) * Ck + s.lamRho;
                    drhodx  = ( 1 - s.lamRho ) * dCkdx;
                    drhody  = ( 1 - s.lamRho ) * dCkdy;
                else
                    Hs = Heaviside(Ck);
                    rho     = (1-s.lamRho)*Hs + s.lamRho ;
                    drhodx  = 0;
                    drhody  = 0;
                end % if
                
                %% Dynamic viscosity
                if ( ~s.sharpMu )
                    mu      = ( 1 - s.lamMu ) * Ck + s.lamMu;
                    dmudx   = ( 1 - s.lamMu ) * dCkdx;
                    dmudy   = ( 1 - s.lamMu ) * dCkdy;
                else
                    Hs = Heaviside(Ck);
                    mu = ( 1 - s.lamMu ) * Hs + s.lamMu;
                    Ds = Delta( Ck );
                    dmudx = ( 1 - s.lamMu ) * Ds .* dCkdx;
                    dmudy = ( 1 - s.lamMu ) * Ds .* dCkdy;
                end % if
                
                Uk    = U.val( FieldType.Current, e );
                dUkdx = U.x( FieldType.Current, e );
                dUkdy = U.y( FieldType.Current, e );
                Pk    = P.val( FieldType.Current, e );
                Vk    = V.val( FieldType.Current, e );
                dVkdx = V.x( FieldType.Current, e );
                dVkdy = V.y( FieldType.Current, e );
                
                if (s.SDIM>2)
                    Wk    = W.val(e);
                    dWkdx = W.x(e);
                    dWkdy = W.y(e);
                end % if

                dPolddx = 0;
                dPolddy = 0;

                % Get all elemental operators/arrays
                matZ = element.getMatZ();
                vecZ = element.getVecZ();
                H    = element.getH();
                Dx   = element.getDx();
                Dy   = element.getDy();
                if ( s.steady )
                    Dt       = matZ;
                    dUdt_rhs = vecZ;
                    dVdt_rhs = vecZ;
                else
                    Dt       = s.getTime( element );
                    dUdt_rhs = s.getTime( element, U );
                    dVdt_rhs = s.getTime( element, V );
                end % if

                Dxx = element.getDxx();
                Dyy = element.getDyy();
                Dxy = element.getDxy();
                Dxt = element.getDxt();
                Dyt = element.getDyt();
                Wel = element.getW();

                if ( dCkdt == 0 )
                    dCkdt = vecZ;
                end % if

                if ( nablaOmg == 0 )
                    nablaOmg = vecZ;
                end % if

                % Moving reference frame
                if (System.settings.phys.dynamicReferenceFrame)
                    % x-direction
                    if ( ~isempty(uc) )
                        Uk = reshape( Uk, element.finite.qSize );
                        for i=1:numel(uc)
                            Uk(:,:,i) = Uk(:,:,i) - uc(i);
                        end
                        Uk = Uk(:);
                    else
                        uc = 0;
                    end
                    
                    % y-direction
                    if ( ~isempty(vc) )
                        Vk = reshape( Vk, element.finite.qSize );
                        for i=1:numel(vc)
                            Vk(:,:,i) = Vk(:,:,i) - vc(i);
                        end
                        Vk = Vk(:);
                    else
                        vc = 0;
                    end
                end % if
                
                %% Advection terms
                % Used for both Picard and Newton linearization
                Comm = Dt + Uk .* Dx + Vk .* Dy;

                switch s.linearization
                    case Linearization.Newton
                        ConvUU    = Comm + dUkdx .* H;
                        ConvUV    =        dUkdy .* H;
                        ConvVU    =        dVkdx .* H;
                        ConvVV    = Comm + dVkdy .* H;
                        ConvU_rhs = dUdt_rhs + Uk .* dUkdx + Vk .* dUkdy;
                        ConvV_rhs = dVdt_rhs + Uk .* dVkdx + Vk .* dVkdy;
                    case Linearization.Picard
                        ConvUU    = Comm;
                        ConvUV    = matZ;
                        ConvVU    = matZ;
                        ConvVV    = Comm;
                        ConvU_rhs = vecZ;
                        ConvV_rhs = vecZ;
                end
                
                %% Diffusion terms
                Lapl   = Dxx + Dyy;
                Nabla2 = s.REI * mu .* Lapl;
                comp   = s.REI * mu / 3;

                % x-momentum
                dVisUU = s.REI*( 2*dmudx.*Dx +   dmudy.*Dy ) + Nabla2 + comp.*Dxx;
                dVisUV = s.REI*(   dmudy.*Dx               ) +          comp.*Dxy;

                % y-momentum
                dVisVU = s.REI*(                 dmudx.*Dy ) +          comp.*Dxy;
                dVisVV = s.REI*(   dmudx.*Dx + 2*dmudy.*Dy ) + Nabla2 + comp.*Dyy;
                
                %% Gravity
                if ( System.settings.comp.coupling.enabled && s.coupled)
                    rhoa = s.lamRho;        % used to remove the effect of gravity on the phase with C=0
                else
                    rhoa = 1;
                end
                g_x = (s.FRI2)*(rho-rhoa).*s.gx;
                g_y = (s.FRI2)*(rho-rhoa).*s.gy;
                
                %% Surface tension
                if ( ~isempty(kx) && ~isempty(ky) )
                    st_x = s.WEI * kx{element_ID};
                    st_y = s.WEI * ky{element_ID};
                else
                    st_x = 0;
                    st_y = 0;
                end

                % As proposed by Pontaza (2006), here a regularized (perturbed) 
                % form of the divergence-free constraint to enforce the 
                % incompressibility condition can be used if eps~=0
                % Typical values for eps are 0.01 - 0.05;
                if s.eps == 0 %|| all( P.val(e) ==0 )
                    L33 = matZ;
                    f3  = vecZ;
                else
                    L33 = s.eps * H;
                    f3  = s.eps * Pk;
                end
                
                classic = true;
                   
                % JULIAN TODO
                % Here implement operator L
                if (classic)
                    s.opL{element_ID} = [ rho.*ConvUU-dVisUU    rho.*ConvUV-dVisUV      Dx    ; ...
                                  rho.*ConvVU-dVisVU    rho.*ConvVV-dVisVV   	Dy   ];
              
                    defaultDivergenceEquation = true;
                    
                    if (defaultDivergenceEquation)
                        s.opL{element_ID} = [  s.opL{element_ID} ; ...
                                       rho.*Dx      rho.*Dy	    L33  ];
                    else
                        if (element.finite.spaceTime)
                            s.opL{element_ID} = [  s.opL{element_ID} ; ...
                                           rho.*Dxt      rho.*Dyt	    matZ  ];
                        else
                            s.opL{element_ID} = [  s.opL{element_ID} ; ...
                                           rho.*Dx/dt	 rho.*Dy/dt	    matZ  ];
                        end
                    end

                    if (s.nEq==4)
                        % Stronger coupling between pressure and velocities
                        if (element.finite.spaceTime)
                            s.opL{element_ID} = [  s.opL{element_ID} ; ...
                                          (rho.*Dxt + drhodx.*Dt)    (rho.*Dyt + drhody.*Dt)    -Lapl  ];               % space-time components
                        else
                            s.opL{element_ID} = [  s.opL{element_ID} ; ...
                                          (rho.*Dx + drhodx.*H)/dt   (rho.*Dy + drhody.*H)/dt   -Lapl  ];               % time-stepping components
                        end
                    end

                    %% Setup RHS
                    % JULIAN ADDED
                    % dPoldxx, dPolddy: these are doing nothing here
                    % st_x, st_y: these are surface tensions, as expected
                    fvx     = rho.*ConvU_rhs + g_x + st_x - s.gradP(1) - dPolddx;
                    fvy     = rho.*ConvV_rhs + g_y + st_y - s.gradP(2) - dPolddy;
                    fdiv    = f3;

                    s.opG{element_ID} = [ fvx   ; ...
                                  fvy   ; ...
                                  fdiv ];

                    if (s.nEq==4)
                        s.opG{element_ID} = [ s.opG{element_ID} ; ...
                                        vecZ   ];
                    end % if
                           
                        
                else
                    % Dodd & Ferrante (2014) decomposition of the pressure
                    % gradient term into a constant part (1/rho_0) and a
                    % variable part )1/rho_{n+1}), and then treat the constant
                    % part implicitly and the variable part explicitly.
                    s.opL{element_ID} = [ rho.*( Dt+ConvUU )-dVisUU-Nabla2x+massFluxVel      rho.*(    ConvUV )-dVisUV                         rho.*Dx  ; ...
                                  rho.*(    ConvVU )-dVisVU                          rho.*( Dt+ConvVV )-dVisVV-Nabla2y+massFluxVel     rho.*Dy  ; ...
                                  Dx                                                 Dy                                                L33     ];
                              
                    fvx = fvx - (1 - rho) .* dPkdx;
                    fvy = fvy - (1 - rho) .* dPkdy;

                    s.opG{element_ID} = [ weight1.*(fvx) ; ...
                                  weight2.*(fvy) ; ...
                                  weight3.*f3  ];

                end % if
                          
                if (s.withForcing)
                    s.opG{element_ID} = s.opG{element_ID} - s.forcing{element_ID};
                end % if
                
                %% Scale equations with weights (can differ between elements)
                scaling = 1;
%                 scaling = [     1./rho        ; ...
%                                 1./rho        ; ...
%                                 1./rho        ; ...
%                             ones(size(vecZ)) ];

                %% Set quadrature weights, apply scaling to L and G. 
                % NB : eqWeights are applied inside the function already,
                %      no need to add them to 'scaling'
                s.setWeightAndScale( element, scaling );
            end
            
            t2 = toc;
            time = t2-t1;
        end
        
        % Function to determine which equations to setup for 2D cases
        function time = setupEquations_2D(s)
            if (s.numStages==1)
                time = s.setupEquations_2D_full;
            elseif (s.numStages==2)
                switch s.activeStep
                    case 1
                        time = s.setupEquations_2D_pStep();
                    case 2
                        time = s.setupEquations_2D_velStep();
                end
            elseif (s.numStages==3)
                
                s.needsConvergence(1,1) = false;
                
                if (s.manufacturedSolution)
                    s.forcing = s.stageForcing{ s.activeStage };                 % Select the correct forcing
                end
                switch s.activeStage
                    case 1
                        s.useInverse = false;
                        time = s.setupEquations_2D_stage1();
                    case 2
                        s.useInverse = false;
                        time = s.setupEquations_2D_stage2();
                    case 3
                        s.useInverse = true;
                        time = s.setupEquations_2D_stage3();
                end
            end
        end
        
        % JULIAN ADDED
        % DATE: 17.01.2020 13:37
        % Base version used in the original Navier-Stokes physics
        % written by Marcel Kwakkel.
        function time = DEPRECATED_setupEquations_2D_full(s)
            t1 = toc;

            U   = FieldCollection.get('u');
            V   = FieldCollection.get('v');
            P   = FieldCollection.get('p');
                        
            if ( System.settings.comp.coupling.enabled && s.coupled)
                C   = FieldCollection.get('c');
                Omg = FieldCollection.get('omg');

                % Get the curvature from the CH simulation, use the NS.mesh as
                % the base mesh for the data
                CH = PhysicsCollection.get('CH');
                [kx,ky] = CH.getCurvature(s.mesh);
            else
                C   = NaN; 
                Omg = NaN; 
                kx  = [];
                ky  = [];
            end % if
           
            for e = s.mesh.eRange
                % Select the active element
                element = s.mesh.getElement(e);
                element_ID = element.localID;
                                
                % From CH (2=SolutionMode.Coupling)
                if ( C ~= C || isempty( C ) )
                    Ck    = 1;
                    dCkdx = 0;
                    dCkdy = 0;
                    dCkdt = 0;
                else
                    Ck    = C.val2(e,s.mesh);
                    dCkdx = C.x2(e,s.mesh);
                    dCkdy = C.y2(e,s.mesh);
                    dCkdt = C.t2(e,s.mesh);
                end % if

                if ( Omg ~= Omg || isempty(Omg))
                    Omgk    = 0;
                    nablaOmg= 0;
                else
                    Omgk    = Omg.val2(e,s.mesh);
                    dOmgkdy = Omg.y2(e,s.mesh);
                end % if
                
                %% Density
                if (~s.sharpRho)
                    rho     = (1-s.lamRho)*Ck + s.lamRho ;
                    drhodx  = (1-s.lamRho)*dCkdx;
                    drhody  = (1-s.lamRho)*dCkdy;
                else
                    Hs = Heaviside(Ck,s.heavisideThickness);
                    rho     = (1-s.lamRho)*Hs + s.lamRho ;
                end % if
                
                %% Dynamic viscosity
                if (~s.sharpMu)
                    mu      = (1-s.lamMu )*Ck + s.lamMu ;
                    dmudx   = (1-s.lamMu )*dCkdx ;
                    dmudy   = (1-s.lamMu )*dCkdy ;
                else
                    Hs = Heaviside(Ck,s.heavisideThickness);
                    mu     = (1-s.lamMu)*Hs + s.lamMu ;
                    
                    Ds = Delta(Ck,s.heavisideThickness);
                    dmudx = (1-s.lamMu ) * Ds .* dCkdx;
                    dmudy = (1-s.lamMu ) * Ds .* dCkdy;
                end % if
                
                Uk    = U.val(FieldType.Current,e);
                dUkdx = U.x(FieldType.Current,e);
                dUkdy = U.y(FieldType.Current,e);
                
                Pk    = P.val(FieldType.Current,e);
                dPkdx = P.x(FieldType.Current,e);
                dPkdy = P.y(FieldType.Current,e);
                
                Vk    = V.val(FieldType.Current,e);
                dVkdx = V.x(FieldType.Current,e);
                dVkdy = V.y(FieldType.Current,e);
                
                % Get all elemental operators/arrays
                matZ    = element.getMatZ;
                vecZ    = element.getVecZ;
                H       = element.getH;
                Dx      = element.getDx;
                Dy      = element.getDy;

                if ( s.steady )
                    Dt       = matZ;
                    dUdt_rhs = vecZ;
                    dVdt_rhs = vecZ;
                else
                    Dt       = s.getTime( element );
                    dUdt_rhs = s.getTime( element, U );
                    dVdt_rhs = s.getTime( element, V );
                end % if

                Dxx     = element.getDxx;
                Dyy     = element.getDyy;
                Dxy     = element.getDxy;

                if (s.timeMethod == TimeMethod.SpaceTime)
                    Dxt     = element.getDxt;
                    Dyt     = element.getDyt;
                end % if
                
                if (dCkdt==0)
                    dCkdt = vecZ;
                end % if

                %% Diffusion : viscous terms
                Lapl   = Dxx + Dyy;
                Nabla2 = s.REI * mu .* Lapl;
                comp   = s.REI * mu / 3;
                
                difUU  = s.REI*( 2*dmudx.*Dx +   dmudy.*Dy ) + Nabla2 + comp.*Dxx;      % x-mom : U components
                difUV  = s.REI*(   dmudy.*Dx               )          + comp.*Dxy;      % x-mom : V components
                difVU  = s.REI*(                 dmudx.*Dy )          + comp.*Dxy;      % y-mom : U components
                difVV  = s.REI*(   dmudx.*Dx + 2*dmudy.*Dy ) + Nabla2 + comp.*Dyy;      % y-mom : V components

                %% Advection : nonlinear terms
                Comm = Dt + Uk.*Dx + Vk.*Dy;          % Used for both Picard and Newton linearization
                
                switch s.linearization
                    case Linearization.Newton
                        advUU       = Comm + dUkdx .* H;
                        advUV       =        dUkdy .* H;
                        advVU       =        dVkdx .* H;
                        advVV       = Comm + dVkdy .* H;
                        advU_rhs    = dUdt_rhs + Uk.*dUkdx + Vk.*dUkdy ;
                        advV_rhs    = dVdt_rhs + Uk.*dVkdx + Vk.*dVkdy ; 
                    case Linearization.Picard
                        advUU       = Comm;
                        advUV       = matZ;
                        advVU       = matZ;
                        advVV       = Comm;
                        advU_rhs    = dUdt_rhs;
                        advV_rhs    = dVdt_rhs;
                end % switch
                
                %% Gravity
                if ( System.settings.comp.coupling.enabled && s.coupled)
                    % Used to remove the effect of gravity on the phase with C=0
                    rhoa = s.lamRho;
                else
                    rhoa = 1;
                end % if

                gxtmp = s.gx;
                gytmp = s.gy;

                %% Equation 1: x-momentum
                fvx = rho .* advU_rhs - s.gradP(1) + s.FRI2 * ( rho - rhoa ) .* gxtmp;

                % Add the surface tension force if non-zero
                if ( ~isempty(kx) )
                    fvx  = fvx + s.WEI * (kx{element_ID});
                end % if

                %% Equation 2: y-momentum
                fvy = rho .* advV_rhs - s.gradP(2) + s.FRI2 * ( rho - rhoa ) .* gytmp;

                % Add the surface tension force if non-zero
                if ( ~isempty(ky) )
                    fvy  = fvy + s.WEI * ky{element_ID};
                end % if

                % Get the local element number
                element_ID = element.localID;
                
                % As proposed by Pontaza (2006), here a regularized (perturbed)
                % form of the divergence-free constraint to enforce the
                % incompressibility condition can be used if eps~=0
                % Typical values for eps are 0.01 - 0.05;
                if s.eps == 0 %|| all( P.val(e) ==0 )
                    L33 = matZ;
                    f3  = vecZ;
                else % if
                    L33 = s.eps * H;
                    f3  = s.eps * Pk;
                end % if

                activeSettings = System.settings;
                if (activeSettings.phys.dynamicBC == 1)
                    % Dynamic boundary conditions
                    if ( isempty( element.neighbor{1, 2} ) ) %|| isempty(element.neighbor{2,1})) % NB: {1,1} is the left neighbor, {2,1} is the right neighbor
                        qLocations = (element.finite.logicalCoordinates(2,:) == -1);  % bottom quadrature points
                        gradC = sqrt( dCkdx.^2 + dCkdy.^2 );
                        nx = -dCkdx ./ gradC;
                        ny = -dCkdy ./ gradC;
                        ratio = ny./nx;
                        theta_eq = 100/180*pi;
                        % NB: abs(atan(-ratio(qLocations))) == 0 if the
                        % contact angle is 90 degrees
                        dtheta = (pi/2 - theta_eq) + abs(atan(-ratio(qLocations)));

                        % An extra force to the momentum equation is added,
                        % which will cause the contact line to move. This
                        % clForce should become zero if the contact line
                        % has reached it's (prescribed) equilibrium angle.
                        %
                        %   clForce > 0: advancing contact line
                        %   clForce < 0: retracting contact line
                        clForce = -1000 * s.Cn_L * dCkdx( qLocations );
                        fvx( qLocations ) = fvx( qLocations ) + clForce;
                    end % if
                end % if

                % Classic method
                s.opL{element_ID} = [ rho.*advUU-difUU      rho.*advUV-difUV    1*(1-relaxation)*Dx   ; ...
                    rho.*advVU-difVU      rho.*advVV-difVV    1*(1-relaxation)*Dy  ; ...
                    Dx                     Dy          L33 ];

                s.opG{element_ID} = [ fvx; ...
                                      fvy; ...
                                      f3  ];

                % JULIAN ADDED
                % DATE: 17.01.2020 11:58
                % DOC! (What is this?)
                if s.nEq == 4
                    NS_method = 2;
                else
                    NS_method = 0;
                end % if
                
                switch NS_method
                    case 0
                        % Do nothing, use classic
                    case 1
                        % Dodd & Ferrante (2014) decomposition of the pressure
                        % gradient term into a constant part (1/rho_0) and a
                        % variable part )1/rho_{n+1}), and then treat the constant
                        % part implicitly and the variable part explicitly.
                        % s.opL{el} = [ weight1.*(rho.*( Dt+ConvUU )-dVisUU-Nabla2x)      weight1.*(rho.*(    ConvUV )-dVisUV)                         weight1.*(rho.*Dx)  ; ...
                        %               weight2.*(rho.*(    ConvVU )-dVisVU)                          weight2.*(rho.*( Dt+ConvVV )-dVisVV-Nabla2y)     weight2.*(rho.*Dy)  ; ...
                        %               weight3.*(Dx)                                                 weight3.*(Dy)                                                weight3.*L33       ];
                        %
                        % fvx = fvx - (1 - rho) .* dPkdx;
                        % fvy = fvy - (1 - rho) .* dPkdy;
                        %
                        % s.opG{el} = [ weight1.*(fvx) ; ...
                        %               weight2.*(fvy) ; ...
                        %               weight3.*f3  ]; %  ;                        
                    case 2
                        % Improved coupling between velocity and pressure
                        % The divergence of the momentum equation is added,
                        % which leads to a Poisson equation for the
                        % pressure.
                        if (~element.finite.spaceTime)
                            if (s.steady)
                                s.opL{element_ID} = [ s.opL{element_ID};
                                    matZ      matZ    -Lapl  ];                           % time-stepping components
                            else
                                dt = System.settings.time.step;
                                s.opL{element_ID} = [ s.opL{element_ID};
                                    (rho.*Dx + drhodx.*H)/dt   (rho.*Dy + drhody.*H)/dt   -Lapl  ];                           % time-stepping components
                            end % if

                        else
                            s.opL{element_ID} = [ s.opL{element_ID}                                                                              ; ...
                                rho.*Dxt + drhodx.*Dt   	rho.*Dyt + drhody.*Dt  	-(Dxx+Dyy) ];  % space-time components
                        end % if
                        
                        s.opG{element_ID} = [ s.opG{element_ID} ; ...
                            vecZ   ];
                end % switch
                
                if ( s.withForcing )
                    s.opG{element_ID} = s.opG{element_ID} - s.forcing{element_ID};
                end % if
                
                %% Scale equations with weights (can differ between elements)
                if ( NS_method==0 || NS_method==1 )
                    unitVec = ones( size(vecZ) );
                    scaling = [ unitVec ./ rho ;...
                                unitVec ./ rho ;...
                                unitVec ];
                else
                    scaling = [ unitVec ./ rho ; ...
                                unitVec ./ rho ; ...
                                unitVec        ; ...
                                unitVec ./ rho ];
                end % if

                %% Set quadrature weights, apply all scaling to L and G
                s.setWeightAndScale( element, scaling );
            end % for (cycling through all elements)

            t2 = toc;
            time = t2 - t1;
        end % function DEPRECATED_setupEquations_2D_full

        % Setup the NS equations
        function time = setupEquations_2D_full(s)
            t1 = toc();
            
%####################################################################%
%%%%%%% Collect all fields used to compute dynamic equations   %%%%%%%
%####################################################################%
            U   = FieldCollection.get('u');
            V   = FieldCollection.get('v');
            W_z = FieldCollection.get('w_z');
            P   = FieldCollection.get('p');
            % Deal with the concentration coefficient used in Cahn Hilliard
            % equations
            if ( System.settings.comp.coupling.enabled && s.coupled)
                C   = FieldCollection.get('c');
                Omg = FieldCollection.get('omg');
                % Get the curvature from the CH simulation, use the NS.mesh as
                % the base mesh for the data
                CH = PhysicsCollection.get('CH');
                [kx,ky] = CH.getCurvature(s.mesh);
            else
                C   = NaN; 
                Omg = NaN; 
                kx  = [];
                ky  = [];
            end % if
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            % Cycle through all the elements of the mesh
            for e = s.mesh.eRange
                % Select the active element
                element = s.mesh.getElement(e);
                element_ID = element.localID;

%
%############################################################################%
%%%%% Collect and compute important operators and vector/matrix elements %%%%%
%############################################################################%
                matZ = element.getMatZ();
                vecZ = element.getVecZ();
                H    = element.getH();
                Dx   = element.getDx();
                Dy   = element.getDy();
                Dxx  = element.getDxx();
                Dyy  = element.getDyy();
                Dxy  = element.getDxy();
                Lapl = Dxx + Dyy;
                
                if ( s.steady )
                    Dt       = matZ;
                    dUdt_rhs = vecZ;
                    dVdt_rhs = vecZ;
                else
                    Dt       = s.getTime( element );
                    dUdt_rhs = s.getTime( element, U );
                    dVdt_rhs = s.getTime( element, V );
                end % if

                if (s.timeMethod == TimeMethod.SpaceTime)
                    Dxt = element.getDxt();
                    Dyt = element.getDyt();
                end % if
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%############################################################################%
%%%%%%% Compute and colect important derivatives for the computation   %%%%%%%
%############################################################################%
                % From CH (2=SolutionMode.Coupling)
                if ( C ~= C || isempty( C ) )
                    Ck    = 1;
                    dCkdx = 0;
                    dCkdy = 0;
                    dCkdt = 0;
                else
                    Ck    = C.val2( e, s.mesh );
                    dCkdx = C.x2( e, s.mesh );
                    dCkdy = C.y2( e, s.mesh );
                    dCkdt = C.t2( e, s.mesh );
                end % if

                if ( Omg ~= Omg || isempty( Omg ) )
                    Omgk     = 0;
                    nablaOmg = 0;
                else
                    Omgk    = Omg.val2( e, s.mesh );
                    dOmgkdy = Omg.y2( e, s.mesh );
                end % if
                
                % Density
                if (~s.sharpRho)
                    rho     = (1-s.lamRho)*Ck + s.lamRho ;
                    drhodx  = (1-s.lamRho)*dCkdx;
                    drhody  = (1-s.lamRho)*dCkdy;
                else
                    Hs = Heaviside(Ck,s.heavisideThickness);
                    rho     = (1-s.lamRho)*Hs + s.lamRho ;
                end % if
                
                % Dynamic viscosity
                if (~s.sharpMu)
                    mu      = (1-s.lamMu )*Ck + s.lamMu ;
                    dmudx   = (1-s.lamMu )*dCkdx ;
                    dmudy   = (1-s.lamMu )*dCkdy ;
                else
                    Hs = Heaviside(Ck,s.heavisideThickness);
                    mu     = (1-s.lamMu)*Hs + s.lamMu ;
                    
                    Ds = Delta(Ck,s.heavisideThickness);
                    dmudx = (1-s.lamMu ) * Ds .* dCkdx;
                    dmudy = (1-s.lamMu ) * Ds .* dCkdy;
                end % if
                
                Uk    = U.val(FieldType.Current,e);
                dUkdx = U.x(FieldType.Current,e);
                dUkdy = U.y(FieldType.Current,e);
                
                Vk    = V.val(FieldType.Current,e);
                dVkdx = V.x(FieldType.Current,e);
                dVkdy = V.y(FieldType.Current,e);
                
                Pk    = P.val(FieldType.Current,e);
                
                W_zk  = W_z.val( FieldType.Current, e);
                
                if (dCkdt==0)
                    dCkdt = vecZ;
                end % if
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%############################################################################%
%%%%%%% Setting operators L and G                                      %%%%%%%
%############################################################################%
                % Diffusion : viscous terms
                Nabla2 = mu .* Lapl;
                comp   = mu / 3;

                difUU  = s.REI * ( 2*dmudx.*Dx +   dmudy.*Dy + Nabla2 + comp .* Dxx);      % x-mom : U components
                difUV  = s.REI * (   dmudy.*Dx                        + comp .* Dxy);      % x-mom : V components
                difVU  = s.REI * (                 dmudx.*Dy          + comp .* Dxy);      % y-mom : U components
                difVV  = s.REI * (   dmudx.*Dx + 2*dmudy.*Dy + Nabla2 + comp .* Dyy);      % y-mom : V components

                % Advection : nonlinear terms
                Comm = Dt + Uk .* Dx + Vk .* Dy;          % Used for both Picard and Newton linearization
                
                switch s.linearization
                    case Linearization.Newton
                        advUU    = Comm + dUkdx .* H;
                        advUV    =        dUkdy .* H;
                        advVU    =        dVkdx .* H;
                        advVV    = Comm + dVkdy .* H;
                        advU_rhs = dUdt_rhs + Uk .* dUkdx + Vk .* dUkdy;
                        advV_rhs = dVdt_rhs + Uk .* dVkdx + Vk .* dVkdy;
                    case Linearization.Picard
                        advUU    = Comm;
                        advUV    = matZ;
                        advVU    = matZ;
                        advVV    = Comm;
                        advU_rhs = dUdt_rhs;
                        advV_rhs = dVdt_rhs;
                end % switch

                % Gravity
                if ( System.settings.comp.coupling.enabled && s.coupled)
                    % Used to remove the effect of gravity on the phase with C=0
                    rhoa = s.lamRho;
                else
                    rhoa = 1;
                end % if

                gxtmp = s.gx;
                gytmp = s.gy;

                % Equation 1: x-momentum
                fvx = rho .* advU_rhs - s.gradP(1) + s.FRI2 * ( rho - rhoa ) .* gxtmp;

                % Add the surface tension force if non-zero
                if ( ~isempty(kx) )
                    fvx  = fvx + s.WEI * (kx{element_ID});
                end % if

                % Equation 2: y-momentum
                fvy = rho .* advV_rhs - s.gradP(2) + s.FRI2 * ( rho - rhoa ) .* gytmp;

                % Add the surface tension force if non-zero
                if ( ~isempty(ky) )
                    fvy  = fvy + s.WEI * ky{element_ID};
                end % if
                
                % As proposed by Pontaza (2006), here a regularized (perturbed)
                % form of the divergence-free constraint to enforce the
                % incompressibility condition can be used if eps~=0
                % Typical values for eps are 0.01 - 0.05;
                if s.eps == 0 %|| all( P.val(e) ==0 )
                    L33 = matZ;
                    f3  = vecZ;
                else % if
                    L33 = s.eps * H;
                    f3  = s.eps * Pk;
                end % if

                activeSettings = System.settings;
                if (activeSettings.phys.dynamicBC == 1)
                    % Dynamic boundary conditions
                    if ( isempty( element.neighbor{1, 2} ) ) %|| isempty(element.neighbor{2,1})) % NB: {1,1} is the left neighbor, {2,1} is the right neighbor
                        qLocations = (element.finite.logicalCoordinates(2,:) == -1);  % bottom quadrature points
                        gradC = sqrt( dCkdx.^2 + dCkdy.^2 );
                        nx = -dCkdx ./ gradC;
                        ny = -dCkdy ./ gradC;
                        ratio = ny./nx;
                        theta_eq = 100/180*pi;
                        % NB: abs(atan(-ratio(qLocations))) == 0 if the
                        % contact angle is 90 degrees
                        dtheta = (pi/2 - theta_eq) + abs(atan(-ratio(qLocations)));

                        % An extra force to the momentum equation is added,
                        % which will cause the contact line to move. This
                        % clForce should become zero if the contact line
                        % has reached it's (prescribed) equilibrium angle.
                        %
                        %   clForce > 0: advancing contact line
                        %   clForce < 0: retracting contact line
                        clForce = -1000 * s.Cn_L * dCkdx( qLocations );
                        fvx( qLocations ) = fvx( qLocations ) + clForce;
                    end % if
                end % if

                % Classic method
                s.opL{element_ID} = [ rho .* advUU, rho .* advUV,  s.REI * Dy, Dx; ...
                                      rho .* advVU, rho .* advVV, -s.REI * Dx, Dy; ...
                                      Dy,           -Dx,           H,          matZ; ...
                                      Dx,            Dy,           matZ,       matZ];

                s.opG{element_ID} = [ fvx; ...
                                      fvy; ...
                                      vecZ; ...
                                      f3 ];

                % JULIAN ADDED
                % DATE: 17.01.2020 11:58
                % DOC! (What is this?)
%                 if s.nEq == 4
%                     NS_method = 2;
%                 else
%                     NS_method = 0;
%                 end % if

                % JULIAN ADDED
                % DATE: 17.01.2020 13:55
                % I fixed this case so I didn't need to delete the previous
                % information. This will be deleted once I am able to ask
                % Marcel what the NS_methods mean and why only 0 and 2 were
                % used.
                NS_method = 0;

                switch NS_method
                    case 0
                        % Do nothing, use classic
                    case 1
                        % Dodd & Ferrante (2014) decomposition of the pressure
                        % gradient term into a constant part (1/rho_0) and a
                        % variable part )1/rho_{n+1}), and then treat the constant
                        % part implicitly and the variable part explicitly.
                        % s.opL{el} = [ weight1.*(rho.*( Dt+ConvUU )-dVisUU-Nabla2x)      weight1.*(rho.*(    ConvUV )-dVisUV)                         weight1.*(rho.*Dx)  ; ...
                        %               weight2.*(rho.*(    ConvVU )-dVisVU)                          weight2.*(rho.*( Dt+ConvVV )-dVisVV-Nabla2y)     weight2.*(rho.*Dy)  ; ...
                        %               weight3.*(Dx)                                                 weight3.*(Dy)                                                weight3.*L33       ];
                        %
                        % fvx = fvx - (1 - rho) .* dPkdx;
                        % fvy = fvy - (1 - rho) .* dPkdy;
                        %
                        % s.opG{el} = [ weight1.*(fvx) ; ...
                        %               weight2.*(fvy) ; ...
                        %               weight3.*f3  ]; %  ;                        
                    case 2
                        % Improved coupling between velocity and pressure
                        % The divergence of the momentum equation is added,
                        % which leads to a Poisson equation for the
                        % pressure.
                        if (~element.finite.spaceTime)
                            if (s.steady)
                                s.opL{element_ID} = [ s.opL{element_ID};
                                    matZ      matZ    -Lapl  ];                           % time-stepping components
                            else
                                dt = System.settings.time.step;
                                s.opL{element_ID} = [ s.opL{element_ID};
                                    (rho.*Dx + drhodx.*H)/dt   (rho.*Dy + drhody.*H)/dt   -Lapl  ];                           % time-stepping components
                            end % if

                        else
                            s.opL{element_ID} = [ s.opL{element_ID}                                                                              ; ...
                                rho.*Dxt + drhodx.*Dt   	rho.*Dyt + drhody.*Dt  	-(Dxx+Dyy) ];  % space-time components
                        end % if
                        
                        s.opG{element_ID} = [ s.opG{element_ID} ; ...
                            vecZ   ];
                end % switch
                
                if ( s.withForcing )
                    s.opG{element_ID} = s.opG{element_ID} - s.forcing{element_ID};
                end % if
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%############################################################################%
%%%%%%% Scaling equations before solving                               %%%%%%%
%############################################################################%
                % Scale equations with weights (can differ between elements)
                % JULIAN ADDED
                % DATE: 17.01.2020 15:14
                % DOC! Ask Marcel about the equations where this is
                % weightened. Specially the second method where the rho
                % variable appears three times.
                if ( NS_method == 0 || NS_method == 1 )
                    unitVec = ones( size(vecZ) );
                    scaling = [ unitVec ./ rho ;...
                                unitVec ./ rho ;...
                                unitVec ;
                                unitVec ];
                else
                    scaling = [ unitVec ./ rho ; ...
                                unitVec ./ rho ; ...
                                unitVec        ; ...
                                unitVec ./ rho ];
                end % if

                % Set quadrature weights, apply all scaling to L and G
                s.setWeightAndScale( element, scaling );
            end % for (cycling through all elements)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            t2 = toc;
            time = t2 - t1;
        end % function setupEquations_2D_full
        
        % Setup the NS equations (2 stage)
        function time = setupEquations_2D_pStep(s)
            t1 = toc;

            % Set the number of variables, equations and the equation weights
            s.activeVariables = [3];
            s.nEq             = 2;
            s.activeEqWeights = s.eqWeights(1:2);
            
            kappa = 0;
            
            U   = FieldCollection.get('u');
            V   = FieldCollection.get('v');
            P   = FieldCollection.get('p');
                        
            if ( System.settings.comp.coupling.enabled && s.coupled)
                C   = FieldCollection.get('c');
                Omg = FieldCollection.get('omg');

                % Get the curvature from the CH simulation, use the NS.mesh as
                % the base mesh for the data
                CH = PhysicsCollection.get('CH');
                [kx,ky] = CH.getCurvature(s.mesh);
            else
                C   = NaN; 
                Omg = NaN; 
                kx  = [];
                ky  = [];
            end
           
            for e=s.mesh.eRange
                %t(1) = toc;
                
                % Select the active element
                element = s.mesh.getElement(e);
                
                el = element.localID;
                                
                % From CH (2=SolutionMode.Coupling)
                if (C~=C || isempty(C))
                    Ck      = 1;
                    dCkdx   = 0;
                    dCkdy   = 0;
                    dCkdt   = 0;

                else
                    Ck      = C.val2(e,s.mesh);
                    %dCkdx   = CH.Cn/CH.Cn_L * C.x2(e,s.mesh);
                    %dCkdy   = CH.Cn/CH.Cn_L * C.y2(e,s.mesh);
                    dCkdx   = C.x2(e,s.mesh);
                    dCkdy   = C.y2(e,s.mesh);
                    dCkdt   = C.t2(e,s.mesh);
                    %dCkdt   = 0;
                end

                if (Omg~=Omg || isempty(Omg))
                    Omgk    = 0;
                    nablaOmg= 0;
                else
                    Omgk    = Omg.val2(e,s.mesh);
                    nablaOmg= Omg.xx2(e,s.mesh) + Omg.yy2(e,s.mesh);
                end
                
                %Omgk = Omega{el}(:);
                %gradC   = sqrt( dCkdx.^2 + dCkdy.^2 );
                
                %% Density
                if (~s.sharpRho)
                    rho     = (1-s.lamRho)*Ck + s.lamRho ;
                    drhodx  = (1-s.lamRho)*dCkdx;
                    drhody  = (1-s.lamRho)*dCkdy;
                else
                    Hs = Heaviside(Ck,s.Cn_L);
                    rho     = (1-s.lamRho)*Hs + s.lamRho ;
                    %rho( Ck>=0.5 ) = 1;
                    %rho( Ck< 0.5 ) = s.lamRho;
                end
                
                %% Dynamic viscosity
                if (~s.sharpMu)
                    mu      = (1-s.lamMu )*Ck + s.lamMu ;
                    dmudx   = (1-s.lamMu )*dCkdx ;
                    dmudy   = (1-s.lamMu )*dCkdy ;
                else
                    Hs = Heaviside(Ck,s.Cn_L);
                    mu     = (1-s.lamMu)*Hs + s.lamMu ;
                    
                    Ds = Delta(Ck,s.Cn_L);
                    dmudx = (1-s.lamMu )*Ds.*dCkdx ;
                    dmudy = (1-s.lamMu )*Ds.*dCkdy ;
                end
                
                %t(2) = toc;
                
                Uk    = U.val(FieldType.Current,e);
                dUkdx = U.x(FieldType.Current,e);
                dUkdy = U.y(FieldType.Current,e);
                Ux    = U.x(FieldType.Current,e);
                Uy    = U.y(FieldType.Current,e);
                Uxx   = U.xx(FieldType.Current,e);
                Uxy   = U.xy(FieldType.Current,e);
                Uyy   = U.yy(FieldType.Current,e);
                
                %dUdt  = U.z(e);
                %dVdt  = V.z(e);                
                Pk    = P.val(FieldType.Current,e);
                dPkdx = P.x(FieldType.Current,e);
                dPkdy = P.y(FieldType.Current,e);
                
                Vk    = V.val(FieldType.Current,e);
                dVkdx = V.x(FieldType.Current,e);
                dVkdy = V.y(FieldType.Current,e);
                Vx    = V.x(FieldType.Current,e);
                Vy    = V.y(FieldType.Current,e);
                Vxx   = V.xx(FieldType.Current,e);
                Vxy   = V.xy(FieldType.Current,e);
                Vyy   = V.yy(FieldType.Current,e);
                
%                 if (s.SDIM>2)
%                     Wk    = W.val(e);
%                     dWkdx = W.x(e);
%                     dWkdy = W.y(e);
%                 end
                
%                 dPolddx = dPkdx; %P.timeLevel{1}.x(e);
%                 dPolddy = dPkdy; %P.timeLevel{1}.y(e);
                                
                % Get all elemental operators/arrays
                matZ    = element.getMatZ;
                vecZ    = element.getVecZ;
                H       = element.getH;
                Dx      = element.getDx;
                Dy      = element.getDy;
                if (s.steady)
                    Dt       = matZ;
                    dUdt_rhs = vecZ;
                    dVdt_rhs = vecZ;
                else
                    Dt       = s.getTime( element );
                    dUdt_rhs = s.getTime( element, U );
                    dVdt_rhs = s.getTime( element, V );
                end
%                 Dxx     = element.getDxx;
%                 Dyy     = element.getDyy;
%                 Dxy     = element.getDxy;
%                 if (s.timeMethod == TimeMethod.SpaceTime)
%                     Dxt     = element.getDxt;
%                     Dyt     = element.getDyt;
%                 end
                %Wel     = element.getW;

%                 
%                 if (dCkdt==0)
%                     dCkdt = vecZ;
%                 end
%                 if (nablaOmg==0)
%                     nablaOmg = vecZ;
%                 end
                
%                 % Moving reference frame
%                 if (System.settings.phys.dynamicReferenceFrame)
%                     % x-direction
%                     if ( ~isempty(uc) )
%                         Uk = reshape( Uk, element.finite.qSize );
%                         for i=1:numel(uc)
%                             Uk(:,:,i) = Uk(:,:,i) - uc(i);
%                         end
%                         Uk = Uk(:);
%                     else
%                         uc = 0;
%                     end
%                     
%                     % y-direction
%                     if ( ~isempty(vc) )
%                         Vk = reshape( Vk, element.finite.qSize );
%                         for i=1:numel(vc)
%                             Vk(:,:,i) = Vk(:,:,i) - vc(i);
%                         end
%                         Vk = Vk(:);
%                     else
%                         vc = 0;
%                     end
%                 end
                
                %% Diffusion : viscous terms
                Laplu  = Uxx+Uyy;
                Laplv  = Vxx+Vyy;
                Nablau = s.REI * mu .* Laplu;
                Nablav = s.REI * mu .* Laplv;
                comp   = s.REI * mu / 3;
                
                difUU  = s.REI*( 2*dmudx.*Ux +   dmudy.*Uy ) + Nablau + comp.*Uxx;      % x-mom : U components
                difUV  = s.REI*(   dmudy.*Vx               )          + comp.*Vxy;      % x-mom : V components
                difVU  = s.REI*(                 dmudx.*Uy )          + comp.*Uxy;      % y-mom : U components
                difVV  = s.REI*(   dmudx.*Vx + 2*dmudy.*Vy ) + Nablav + comp.*Vyy;      % y-mom : V components
                
%                 simplifyViscousTerm = 1; % see Kang (2000)
%                 difUV  = s.REI*(   dmudy.*Dx + (1-simplifyViscousTerm)*mu.*Dxy );
%                 difVU  = s.REI*(   dmudx.*Dy + (1-simplifyViscousTerm)*mu.*Dxy );
%                 
%                 if (~isempty(Dxx) && ~isempty(Dyy))
%                     Nabla2x  = s.REI* mu.*( Dxx + Dyy );
%                     Nabla2y  = s.REI* mu.*( Dxx + Dyy );
%                 else
%                     Nabla2x  = matZ;
%                     Nabla2y  = matZ;
%                 end
                
%                 Nabla2x  = s.REI* mu.*( (2-simplifyViscousTerm)*Dxx + Dyy) ;
%                 Nabla2y  = s.REI* mu.*( Dxx + (2-simplifyViscousTerm)*Dyy) ;

                if (s.timeMethod==TimeMethod.Steady)
                    DUDt = dUdt_rhs;
                    DVDt = dVdt_rhs;
                else
                    DUDt = Uk / System.settings.time.step;
                    DVDt = Vk / System.settings.time.step;
                end

                %% Advection : nonlinear terms
                Commu = DUDt + Uk.*Ux + Vk.*Uy;          % Used for both Picard and Newton linearization
                Commv = DVDt + Uk.*Vx + Vk.*Vy;
                %Comm = Dt + Uk.*Dx + Vk.*Dy;          % Used for both Picard and Newton linearization
                
                switch s.linearization
                    case Linearization.Newton
                        advUU       = Commu + dUkdx .* Uk;
                        advUV       =         dUkdy .* Vk;
                        advVU       =         dVkdx .* Uk;
                        advVV       = Commv + dVkdy .* Vk;
                        advU_rhs    = dUdt_rhs + Uk.*dUkdx + Vk.*dUkdy ;
                        advV_rhs    = dVdt_rhs + Uk.*dVkdx + Vk.*dVkdy ;
                    case Linearization.Picard
                        advUU       = Comm;
                        advUV       = matZ;
                        advVU       = matZ;
                        advVV       = Comm;
                        advU_rhs    = dUdt_rhs;
                        advV_rhs    = dVdt_rhs;
                end
                
                %% Gravity
                if ( System.settings.comp.coupling.enabled && s.coupled)
                    rhoa = s.lamRho;        % used to remove the effect of gravity on the phase with C=0
                else
                    rhoa = 1;
                end
                
                relaxation = 1;
                
%                 if (~s.zeroGravity)
                    gxtmp = s.gx;
                    gytmp = s.gy;
                    
                    %% Equation 1: x-momentum
                    fvx     = rho.*advU_rhs - s.gradP(1) + (s.FRI2)*(rho-rhoa).*gxtmp + (1-relaxation)*dPkdx ;%+ 0*(1-s.lamRho) * Uk .* diffusiveFlowRate{el}(:);
                    
                    % Add the surface tension force if non-zero
                    if ( ~isempty(kx) )
                        fvx  = fvx + s.WEI * kx{el};
                    end
                    
                    %% Equation 2: y-momentum
                    fvy     = rho.*advV_rhs - s.gradP(2) + (s.FRI2)*(rho-rhoa).*gytmp + (1-relaxation)*dPkdy;     %+ 0*(1-s.lamRho) * Vk .* diffusiveFlowRate{el}(:);
                    
                    % Add the surface tension force if non-zero
                    if ( ~isempty(ky) )
                        fvy  = fvy + s.WEI * ky{el};
                    end
         
                
                % As proposed by Pontaza (2006), here a regularized (perturbed) 
                % form of the divergence-free constraint to enforce the 
                % incompressibility condition can be used if eps~=0
                % Typical values for eps are 0.01 - 0.05;
                if s.eps == 0 %|| all( P.val(e) ==0 )
                    L33 = matZ;
                    f3  = vecZ;
                else
                    L33 = s.eps * H;
                    f3  = s.eps * Pk;
                end
                
                % Classic method
                s.opL{el} = [ Dx  ; ...
                              Dy ];

                s.opG{el} = [ fvx - (rho.*advUU-difUU) - (rho.*advUV-difUV)  ; ...
                              fvy - (rho.*advVU-difVU) - (rho.*advVV-difVV) ]; %  ;

                NS_method = 0;
                          
                switch NS_method
                    case 0
                        % do nothing, use classic
                              
                    case 1
%                         % Dodd & Ferrante (2014) decomposition of the pressure
%                         % gradient term into a constant part (1/rho_0) and a
%                         % variable part )1/rho_{n+1}), and then treat the constant
%                         % part implicitly and the variable part explicitly.
%                         s.opL{el} = [ weight1.*(rho.*( Dt+ConvUU )-dVisUU-Nabla2x)      weight1.*(rho.*(    ConvUV )-dVisUV)                         weight1.*(rho.*Dx)  ; ...
%                                       weight2.*(rho.*(    ConvVU )-dVisVU)                          weight2.*(rho.*( Dt+ConvVV )-dVisVV-Nabla2y)     weight2.*(rho.*Dy)  ; ...
%                                       weight3.*(Dx)                                                 weight3.*(Dy)                                                weight3.*L33       ];
% 
%                         fvx = fvx - (1 - rho) .* dPkdx;
%                         fvy = fvy - (1 - rho) .* dPkdy;
%                 
%                         s.opG{el} = [ weight1.*(fvx) ; ...
%                                       weight2.*(fvy) ; ...
%                                       weight3.*f3  ]; %  ;
                        
                    case 2
                        % Improved coupling between velocity and pressure
                        % The divergence of the momentum equation is added,
                        % which leads to a Poisson equation for the
                        % pressure. 
                        
                        if (~element.finite.spaceTime)
                            if (s.steady)
                                s.opL{el} = [ s.opL{el};
                                              matZ      matZ    -Lapl  ];                           % time-stepping components
                            else
                                dt = System.settings.time.step;
                                s.opL{el} = [ s.opL{el};
                                              (rho.*Dx + drhodx.*H)/dt   (rho.*Dy + drhody.*H)/dt   -Lapl  ];                           % time-stepping components
                            end
                                      
                        else
                            s.opL{el} = [ s.opL{el}                                                                              ; ...
                                          rho.*Dxt + drhodx.*Dt   	rho.*Dyt + drhody.*Dt  	-(Dxx+Dyy) ];  % space-time components
                        end
                        
                        s.opG{el} = [ s.opG{el} ; ...
                                        vecZ   ];
                end
                          
                if (s.withForcing)
                    s.opG{el} = s.opG{el} - s.forcing{el};
                end
                
                %% Scale equations with weights (can differ between elements)
                if (true)
                    if (NS_method==0 || NS_method==1)
                        unitVec = ones( size(vecZ) );
                        scaling = [ unitVec./rho  ; ...
                                    unitVec./rho ];
%                         scaling = [ unitVec ; ...% ./ (s.WEI.*kx{el}+1) ; ...
%                                     unitVec ; ...%./ (s.WEI.*ky{el}+1) ; ...
%                                     unitVec .* rho ];
%                         scaling = [ unitVec./ rho ; ...
%                                     unitVec./ rho ; ...
%                                     unitVec      ];
                    else
                        scaling = [ unitVec./rho  ; ...
                                    unitVec./rho  ; ...
                                    unitVec       ; ...
                                    unitVec./rho ];
                    end
                else
                    scaling = 1;
                end
                
                %% Set quadrature weights, apply all scaling to L and G
                s.setWeightAndScale( element, scaling );
            end
            
            t2 = toc;
            time = t2-t1;
        end
        
        function time = setupEquations_2D_velStage(s)
            t1 = toc;

            % Get the fields
            U   = FieldCollection.get('u');
            V   = FieldCollection.get('v');
            P   = FieldCollection.get('p');
            
            % Set the number of variables, equations and the equation weights
            s.activeVariables = [1,2];
            s.nEq             = 3;
            s.activeEqWeights = s.eqWeights(1:3);
            
            if ( System.settings.comp.coupling.enabled && s.coupled)
                C   = FieldCollection.get('c');
                Omg = FieldCollection.get('omg');

                % Get the curvature from the CH simulation, use the NS.mesh as
                % the base mesh for the data
                CH = PhysicsCollection.get('CH');
                [kx,ky] = CH.getCurvature(s.mesh);

                % Droplet/bubble center of gravity velocity at temporal
                % quadrature points
                if (System.settings.phys.dynamicReferenceFrame)
                    CH.computeCenterOfMassVelocity(true,false);       % determine the center-of-mass velocity: compute the enclosed mass, and without overIntegration
                    
                    uc = CH.cmv_total(:,1) - System.settings.phys.bubbleVelocity(1);
                    vc = CH.cmv_total(:,2) - System.settings.phys.bubbleVelocity(2);
                    
                    absCenterVelocity = sqrt( uc.^2 + vc.^2 );
                else
                    uc  = [];
                    vc  = [];
                    absCenterVelocity = [];
                end
            else
                C   = NaN; 
                Omg = NaN; 
                kx  = [];
                ky  = [];
                uc  = [];
                vc  = [];
            end
           
            for e=s.mesh.eRange
                
                % Select the active element
                element = s.mesh.getElement(e);
                
                el = element.localID;
                                
                % From CH (2=SolutionMode.Coupling)
                if (C~=C || isempty(C))
                    Ck      = 1;
                    dCkdx   = 0;
                    dCkdy   = 0;
                    dCkdt   = 0;

                else
                    Ck      = C.val2(e,s.mesh);
                    %dCkdx   = CH.Cn/CH.Cn_L * C.x2(e,s.mesh);
                    %dCkdy   = CH.Cn/CH.Cn_L * C.y2(e,s.mesh);
                    dCkdx   = C.x2(e,s.mesh);
                    dCkdy   = C.y2(e,s.mesh);
                    dCkdt   = C.t2(e,s.mesh);
                    %dCkdt   = 0;
                end

                if (Omg~=Omg || isempty(Omg))
                    Omgk    = 0;
                    nablaOmg= 0;
                else
                    Omgk    = Omg.val2(e,s.mesh);
                    nablaOmg= Omg.xx2(e,s.mesh) + Omg.yy2(e,s.mesh);
                end
                
                %Omgk = Omega{el}(:);
                %gradC   = sqrt( dCkdx.^2 + dCkdy.^2 );
                
                %% Density
                if (~s.sharpRho)
                    rho     = (1-s.lamRho)*Ck + s.lamRho ;
                    drhodx  = (1-s.lamRho)*dCkdx;
                    drhody  = (1-s.lamRho)*dCkdy;
                else
                    Hs = Heaviside(Ck);
                    rho     = (1-s.lamRho)*Hs + s.lamRho ;
                    %rho( Ck>=0.5 ) = 1;
                    %rho( Ck< 0.5 ) = s.lamRho;
                end
                
                %% Dynamic viscosity
                if (~s.sharpMu)
                    mu      = (1-s.lamMu )*Ck + s.lamMu ;
                    dmudx   = (1-s.lamMu )*dCkdx ;
                    dmudy   = (1-s.lamMu )*dCkdy ;
                else
                    Hs = Heaviside(Ck);
                    mu     = (1-s.lamMu)*Hs + s.lamMu ;
                    
                    Ds = Delta(Ck);
                    dmudx = (1-s.lamMu )*Ds.*dCkdx ;
                    dmudy = (1-s.lamMu )*Ds.*dCkdy ;
                    
                    %mu( Ck>=0.5 ) = 1;
                    %mu( Ck< 0.5 ) = s.lamMu;
                    %dmudx = 0; %(1-s.lamMu )*dCkdx ;
                    %dmudy = 0; %(1-s.lamMu )*dCkdy ;
                end
                
                %t(2) = toc;
                
%                 Uk    = U.val(FieldType.Current,e);
%                 dUkdx = U.x(FieldType.Current,e);
%                 dUkdy = U.y(FieldType.Current,e);
%                 
%                 Vk    = V.val(FieldType.Current,e);
%                 dVkdx = V.x(FieldType.Current,e);
%                 dVkdy = V.y(FieldType.Current,e);
% 
%                 dPkdx = P.x(FieldType.Current,e);
%                 dPkdy = P.y(FieldType.Current,e);
                
                Uk    = U.val2(e);
                dUkdx = U.x2(e);
                dUkdy = U.y2(e);
                
                Vk    = V.val2(e);
                dVkdx = V.x2(e);
                dVkdy = V.y2(e);

                dPkdx = P.x2(e);
                dPkdy = P.y2(e);
                
                % Get all elemental operators/arrays
                matZ    = element.getMatZ;
                vecZ    = element.getVecZ;
                H       = element.getH;
                Dx      = element.getDx;
                Dy      = element.getDy;
                if (s.steady)
                    Dt       = matZ;
                    dUdt_rhs = vecZ;
                    dVdt_rhs = vecZ;
                else
                    Dt       = s.getTime( element );
                    dUdt_rhs = s.getTime( element, U );
                    dVdt_rhs = s.getTime( element, V );
                end
                Dxx     = element.getDxx;
                Dyy     = element.getDyy;
                Dxy     = element.getDxy;
                if (s.timeMethod == TimeMethod.SpaceTime)
                    Dxt     = element.getDxt;
                    Dyt     = element.getDyt;
                end
                                
                % Moving reference frame
                if (System.settings.phys.dynamicReferenceFrame)
                    % x-direction
                    if ( ~isempty(uc) )
                        Uk = reshape( Uk, element.finite.qSize );
                        for i=1:numel(uc)
                            Uk(:,:,i) = Uk(:,:,i) - uc(i);
                        end
                        Uk = Uk(:);
                    else
                        uc = 0;
                    end
                    
                    % y-direction
                    if ( ~isempty(vc) )
                        Vk = reshape( Vk, element.finite.qSize );
                        for i=1:numel(vc)
                            Vk(:,:,i) = Vk(:,:,i) - vc(i);
                        end
                        Vk = Vk(:);
                    else
                        vc = 0;
                    end
                end
                
                %% Diffusion : viscous terms
                Lapl   = Dxx + Dyy;
                Nabla2 = s.REI * mu .* Lapl;
                comp   = s.REI * mu / 3;
                
                difUU  = s.REI*( 2*dmudx.*Dx +   dmudy.*Dy ) + Nabla2 + comp.*Dxx;      % x-mom : U components
                difUV  = s.REI*(   dmudy.*Dx               )          + comp.*Dxy;      % x-mom : V components
                difVU  = s.REI*(                 dmudx.*Dy )          + comp.*Dxy;      % y-mom : U components
                difVV  = s.REI*(   dmudx.*Dx + 2*dmudy.*Dy ) + Nabla2 + comp.*Dyy;      % y-mom : V components

                %% Advection : nonlinear terms
                Comm = Dt + Uk.*Dx + Vk.*Dy;          % Used for both Picard and Newton linearization
                
                switch s.linearization
                    case Linearization.Newton
                        advUU       = Comm + dUkdx .* H;
                        advUV       =        dUkdy .* H;
                        advVU       =        dVkdx .* H;
                        advVV       = Comm + dVkdy .* H;
                        advU_rhs    = dUdt_rhs + Uk.*dUkdx + Vk.*dUkdy ;
                        advV_rhs    = dVdt_rhs + Uk.*dVkdx + Vk.*dVkdy ; 
                    case Linearization.Picard
                        advUU       = Comm;
                        advUV       = matZ;
                        advVU       = matZ;
                        advVV       = Comm;
                        advU_rhs    = dUdt_rhs;
                        advV_rhs    = dVdt_rhs;
                end
                
                %% Gravity
                if ( System.settings.comp.coupling.enabled && s.coupled)
                    rhoa = s.lamRho;        % used to remove the effect of gravity on the phase with C=0
                else
                    rhoa = 1;
                end
                gxtmp = s.gx;
                gytmp = s.gy;

%                 relaxation = 1;
                
                %% Equation 1: x-momentum
                fvx     = rho.*advU_rhs - s.gradP(1) + (s.FRI2)*(rho-rhoa).*gxtmp - dPkdx;  %- (1-relaxation)*dPkdx ;%+ 0*(1-s.lamRho) * Uk .* diffusiveFlowRate{el}(:);

                % Add the surface tension force if non-zero
                if ( ~isempty(kx) )
                    fvx  = fvx + s.WEI * kx{el};
                end

                %% Equation 2: y-momentum
                fvy     = rho.*advV_rhs - s.gradP(2) + (s.FRI2)*(rho-rhoa).*gytmp - dPkdy;  % - (1-relaxation)*dPkdy;     %+ 0*(1-s.lamRho) * Vk .* diffusiveFlowRate{el}(:);

                % Add the surface tension force if non-zero
                if ( ~isempty(ky) )
                    fvy  = fvy + s.WEI * ky{el};
                end
                
                % Classic method
                s.opL{el} = [ rho.*advUU-difUU      rho.*advUV-difUV  ; ...
                              rho.*advVU-difVU      rho.*advVV-difVV  ; ...
                              Dx                    Dy               ];

                s.opG{el} = [ fvx   ; ...
                              fvy   ; ...
                              vecZ ]; %  ;
                          
                if (s.withForcing)
                    s.opG{el} = s.opG{el} - s.forcing{el};
                end
                
                %% Scale equations with weights (can differ between elements)
                if (true)
                    unitVec = ones( size(vecZ) );
                    scaling = [ unitVec./rho  ; ...
                                unitVec./rho  ; ...
                                unitVec      ];
                else
                    scaling = 1;
                end
                
                %% Set quadrature weights, apply all scaling to L and G
                s.setWeightAndScale( element, scaling );
            end
            
            t2 = toc;
            time = t2-t1;
        end
        
        
        function time = setupEquations_2D_stage1(s)
            t1 = toc;

            % Get the fields
            U   = FieldCollection.get('u');
            V   = FieldCollection.get('v');
            P   = FieldCollection.get('p');
            
            % Set the number of variables, equations and the equation weights
            s.activeVariables = [1,2];
            s.nEq             = 2;
            s.activeEqWeights = s.eqWeights(s.activeVariables);
            
            if ( System.settings.comp.coupling.enabled && s.coupled)
                C   = FieldCollection.get('c');
                Omg = FieldCollection.get('omg');

                % Get the curvature from the CH simulation, use the NS.mesh as
                % the base mesh for the data
                CH = PhysicsCollection.get('CH');
                [s.kx_all,s.ky_all] = CH.getCurvature(s.mesh);

                % Droplet/bubble center of gravity velocity at temporal
                % quadrature points
                if (System.settings.phys.dynamicReferenceFrame)
                    CH.computeCenterOfMassVelocity(true,false);       % determine the center-of-mass velocity: compute the enclosed mass, and without overIntegration
                    
                    uc = CH.cmv_total(:,1) - System.settings.phys.bubbleVelocity(1);
                    vc = CH.cmv_total(:,2) - System.settings.phys.bubbleVelocity(2);
                    
                    absCenterVelocity = sqrt( uc.^2 + vc.^2 );
                else
                    uc  = [];
                    vc  = [];
                    absCenterVelocity = [];
                end
            else
                C   = NaN; 
                Omg = NaN; 
                s.kx_all = 0;
                s.ky_all = 0;
                uc  = [];
                vc  = [];
            end
           
            for e=s.mesh.eRange
                
                % Select the active element
                element = s.mesh.getElement(e);
                
                el = element.localID;
                                
                % From CH (2=SolutionMode.Coupling)
                if (C~=C || isempty(C))
                    Ck      = 1;
                    dCkdx   = 0;
                    dCkdy   = 0;
                    dCkdt   = 0;

                else
                    Ck      = C.val2(e,s.mesh);
                    %dCkdx   = CH.Cn/CH.Cn_L * C.x2(e,s.mesh);
                    %dCkdy   = CH.Cn/CH.Cn_L * C.y2(e,s.mesh);
                    dCkdx   = C.x2(e,s.mesh);
                    dCkdy   = C.y2(e,s.mesh);
                    dCkdt   = C.t2(e,s.mesh);
                    %dCkdt   = 0;
                end

%                 if (Omg~=Omg || isempty(Omg))
%                     Omgk    = 0;
%                     nablaOmg= 0;
%                 else
%                     Omgk    = Omg.val2(e,s.mesh);
%                     nablaOmg= Omg.xx2(e,s.mesh) + Omg.yy2(e,s.mesh);
%                 end
                
                %Omgk = Omega{el}(:);
                %gradC   = sqrt( dCkdx.^2 + dCkdy.^2 );
                
                %% Density
                if (~s.sharpRho)
                    rho     = (1-s.lamRho)*Ck + s.lamRho ;
%                     drhodx  = (1-s.lamRho)*dCkdx;
%                     drhody  = (1-s.lamRho)*dCkdy;
                else
                    Hs = Heaviside(Ck);
                    rho     = (1-s.lamRho)*Hs + s.lamRho ;
                    %rho( Ck>=0.5 ) = 1;
                    %rho( Ck< 0.5 ) = s.lamRho;
                end
                
                %% Dynamic viscosity
                if (~s.sharpMu)
                    mu      = (1-s.lamMu )*Ck + s.lamMu ;
                    dmudx   = (1-s.lamMu )*dCkdx ;
                    dmudy   = (1-s.lamMu )*dCkdy ;
                else
                    Hs = Heaviside(Ck);
                    mu     = (1-s.lamMu)*Hs + s.lamMu ;
                    
                    Ds = Delta(Ck);
                    dmudx = (1-s.lamMu )*Ds.*dCkdx ;
                    dmudy = (1-s.lamMu )*Ds.*dCkdy ;
                    
                    %mu( Ck>=0.5 ) = 1;
                    %mu( Ck< 0.5 ) = s.lamMu;
                    %dmudx = 0; %(1-s.lamMu )*dCkdx ;
                    %dmudy = 0; %(1-s.lamMu )*dCkdy ;
                end
                
                %t(2) = toc;
                
%                 Uk    = U.val(FieldType.Current,e);
%                 dUkdx = U.x(FieldType.Current,e);
%                 dUkdy = U.y(FieldType.Current,e);
%                 
%                 Vk    = V.val(FieldType.Current,e);
%                 dVkdx = V.x(FieldType.Current,e);
%                 dVkdy = V.y(FieldType.Current,e);
% 
%                 dPkdx = P.x(FieldType.Current,e);
%                 dPkdy = P.y(FieldType.Current,e);
                
                Uk    = U.val2(e);
                dUkdx = U.x2(e);
                dUkdy = U.y2(e);
                
                Vk    = V.val2(e);
                dVkdx = V.x2(e);
                dVkdy = V.y2(e);

                dPkdx = P.x2(e);
                dPkdy = P.y2(e);
                
                % Get all elemental operators/arrays
                matZ    = element.getMatZ;
                vecZ    = element.getVecZ;
                H       = element.getH;
                Dx      = element.getDx;
                Dy      = element.getDy;
%                 if (s.steady)
%                     Dt       = matZ;
%                     dUdt_rhs = vecZ;
%                     dVdt_rhs = vecZ;
%                 else
                    Dt       = s.getTime( element );
                    dUdt_rhs = s.getTime( element, U );
                    dVdt_rhs = s.getTime( element, V );
%                 end
                Dxx     = element.getDxx;
                Dyy     = element.getDyy;
                Dxy     = element.getDxy;
                %if (s.timeMethod == TimeMethod.SpaceTime)
                %    Dxt     = element.getDxt;
                %    Dyt     = element.getDyt;
                %end
                                
                % Moving reference frame
                if (System.settings.phys.dynamicReferenceFrame)
                    % x-direction
                    if ( ~isempty(uc) )
                        Uk = reshape( Uk, element.finite.qSize );
                        for i=1:numel(uc)
                            Uk(:,:,i) = Uk(:,:,i) - uc(i);
                        end
                        Uk = Uk(:);
                    else
                        uc = 0;
                    end
                    
                    % y-direction
                    if ( ~isempty(vc) )
                        Vk = reshape( Vk, element.finite.qSize );
                        for i=1:numel(vc)
                            Vk(:,:,i) = Vk(:,:,i) - vc(i);
                        end
                        Vk = Vk(:);
                    else
                        vc = 0;
                    end
                end
                
                %% Diffusion : viscous terms
                difUU  = s.REI*(  4/3*dmudx.*Dx +     dmudy.*Dy ) + s.REI*mu.*( 4/3*Dxx +     Dyy ); 	% x-mom : U components
                difUV  = s.REI*(      dmudy.*Dx - 2/3*dmudx.*Dy ) + s.REI*mu.*( 1/3*Dxy           );    % x-mom : V components
                difVU  = s.REI*( -2/3*dmudy.*Dx +     dmudx.*Dy ) + s.REI*mu.*( 1/3*Dxy           );    % y-mom : U components
                difVV  = s.REI*(      dmudx.*Dx + 4/3*dmudy.*Dy ) + s.REI*mu.*(     Dxx + 4/3*Dyy ); 	% y-mom : V components

                %% Advection : nonlinear terms
                Comm = Dt + Uk.*Dx + Vk.*Dy;          % Used for both Picard and Newton linearization
                
                switch s.linearization
                    case Linearization.Newton
                        advUU       = Comm + dUkdx .* H;
                        advUV       =        dUkdy .* H;
                        advVU       =        dVkdx .* H;
                        advVV       = Comm + dVkdy .* H;
                        advU_rhs    = dUdt_rhs + Uk.*dUkdx + Vk.*dUkdy ;
                        advV_rhs    = dVdt_rhs + Uk.*dVkdx + Vk.*dVkdy ; 
                    case Linearization.Picard
                        advUU       = Comm;
                        advUV       = matZ;
                        advVU       = matZ;
                        advVV       = Comm;
                        advU_rhs    = dUdt_rhs;
                        advV_rhs    = dVdt_rhs;
                end
                
                %% Gravity
                if ( System.settings.comp.coupling.enabled && s.coupled)
                    rhoa = s.lamRho;        % used to remove the effect of gravity on the phase with C=0
                else
                    rhoa = 1;
                end
                gxtmp = s.gx;
                gytmp = s.gy;

%                 relaxation = 1;
                                
                %% Equation 1: x-momentum
                fvx     = rho.*advU_rhs - s.gradP(1) + (s.FRI2)*(rho-rhoa).*gxtmp - dPkdx;  % - (1-relaxation)*dPkdx ;%+ 0*(1-s.lamRho) * Uk .* diffusiveFlowRate{el}(:);

                % Add the surface tension force if non-zero
                if ( ~isempty(s.kx_all) )
                    fvx  = fvx + s.WEI * s.kx_all{el};
                end

                %% Equation 2: y-momentum
                fvy     = rho.*advV_rhs - s.gradP(2) + (s.FRI2)*(rho-rhoa).*gytmp - dPkdy;  % - (1-relaxation)*dPkdy;     %+ 0*(1-s.lamRho) * Vk .* diffusiveFlowRate{el}(:);

                % Add the surface tension force if non-zero
                if ( ~isempty(s.ky_all) )
                    fvy  = fvy + s.WEI * s.ky_all{el};
                end
                
                % Classic method
                s.opL{el} = [ rho.*advUU-difUU      rho.*advUV-difUV  ; ...
                              rho.*advVU-difVU      rho.*advVV-difVV ];

                s.opG{el} = [ fvx  ; ...
                              fvy ]; %  ;
                          
                if (s.withForcing)
                    s.opG{el} = s.opG{el} - s.forcing{el};
                end
                
                %% Scale equations with weights (can differ between elements)
                if (true)
                    unitVec = ones( size(vecZ) );
                    scaling = [ unitVec./rho  ; ...
                                unitVec./rho ];
                else
                    scaling = 1;
                end
                
                %% Set quadrature weights, apply all scaling to L and G
                s.setWeightAndScale( element, scaling );
            end
            
            t2 = toc;
            time = t2-t1;
        end
        
        function time = setupEquations_2D_stage2(s)
            t1 = toc;
            
            U	= FieldCollection.get('u');
            V   = FieldCollection.get('v');

            % Set the number of variables, equations and the equation weights
            s.activeVariables = [1,2];
            s.nEq             = 2;
            s.activeEqWeights = s.eqWeights(s.activeVariables);
            
            for e=s.mesh.eRange
                %t(1) = toc;
                
                %% Select the active element and assing the local element number
                element = s.mesh.getElement(e);
                el = element.localID;
                                
                %% Get all elemental operators/arrays
                %H       = element.getH;
                Dx      = element.getDx;
                Dy      = element.getDy;

                vecZ    = element.getVecZ;
                
                %Uk    = U.val(FieldType.Current,e);
                %Vk    = V.val(FieldType.Current,e);
                %dUkdx = U.x(FieldType.Current,e);
                dUkdy = U.y2(e);
                dVkdx = V.x2(e);
                %dVkdy = V.y(FieldType.Current,e);
                
                % Vorticities:
                %   omega_x = dVdz - dWdy
                %   omega_y = dWdx - dUdz 
                %   omega_z = dUdy - dVdx
                % 
                %   Lap(omega) = omega_xx + omega_yy + omega_zz = 0
                %
                %   DUDT = dUdt + U*dUdx + V*dUdy + W*dUdz
                %
                %   dUdt + U*dUdx + V*dUdy + W*dUdz + s.REI * ( (omega_y)_z - (omega_z)_y )
                %                                   + s.REI * ( (Wxz-Uzz) - (Uyy-Vxy)   )
                %                                   + s.REI * ( (Ux+Vy+Wz)_x - Lap(U) )
                
                s.opL{el} = [  Dx     Dy  ; ...   % continuity
                               Dy    -Dx ];       % omega_z

                %% Setup G
                fv1 = vecZ;
                fv2 = dUkdy - dVkdx;
                                    
                s.opG{el} = [  fv1  ; ...
                               fv2 ];
                
                if (s.withForcing)
                    s.opG{el} = s.opG{el} - s.forcing{el};
                end
                
                unitVec = ones( size(vecZ) );
                scaling = [ 1*unitVec  ; ...
                            1*unitVec ];
                           
                %% Set quadrature weights, apply all scaling to L and G
                s.setWeightAndScale( element, scaling );
            end
            
            t2 = toc;
            time = t2-t1;
        end
        
        function time = setupEquations_2D_stage3(s)
            t1 = toc;
            
            [momx_field,momy_field] = s.constructMomentumFields;
            
            U	= FieldCollection.get('u');
            V   = FieldCollection.get('v');
%             P   = FieldCollection.get('p');
%             C   = FieldCollection.get('c');

            % Set the number of variables, equations and the equation weights
            s.activeVariables = [3];
            s.nEq             = 1;
            s.activeEqWeights = s.eqWeights(s.activeVariables);
            
            if ( System.settings.comp.coupling.enabled && s.coupled)
                C   = FieldCollection.get('c');
%                 Omg = FieldCollection.get('omg');

                % Get the curvature from the CH simulation, use the NS.mesh as
                % the base mesh for the data
                CH = PhysicsCollection.get('CH');
%                 [divKappa] = CH.getDivCurvature(s.mesh);
                %[ssfx,ssfy] = CH.getSSF(s.mesh);
%                 stField = FieldCollection.get('st');
                
                % Get the curvature from the CH simulation, use the NS.mesh as
                % the base mesh for the data
                CH = PhysicsCollection.get('CH');
                [kLapC] = CH.getCurvatureLapC(s.mesh);

                st = CH.getSurfaceTension;

                % Droplet/bubble center of gravity velocity at temporal
                % quadrature points
                if (System.settings.phys.dynamicReferenceFrame)
                    CH.computeCenterOfMassVelocity(true,false);       % determine the center-of-mass velocity: compute the enclosed mass, and without overIntegration
                    
                    uc = CH.cmv_total(:,1) - System.settings.phys.bubbleVelocity(1);
                    vc = CH.cmv_total(:,2) - System.settings.phys.bubbleVelocity(2);
                    
                    absCenterVelocity = sqrt( uc.^2 + vc.^2 );
                else
                    uc  = [];
                    vc  = [];
                    absCenterVelocity = [];
                end
            else
                C   = NaN; 
                Omg = NaN; 
                divKappa  = [];
                uc  = [];
                vc  = [];
                st  = [];
            end
            
%             % Compute the mean for Pxx and Pyy
%             P.xx2_av_init;
%             P.yy2_av_init;
            
            for e=s.mesh.eRange
                
                %% Select the active element and assing the local element number
                element = s.mesh.getElement(e);
                el = element.localID;
                                
                %% Obtain the Ck, Cx and Cy from the C field
                if (C~=C || isempty(C))
                    Ck = 1;
                    Cx = 0;
                    Cy = 0;
                else
                    Ck = C.val2(e,s.mesh);
                    Cx = C.x2(e);
                    Cy = C.y2(e);
                end

                %% Density
                if (~s.sharpRho)
                    rho     = (1-s.lamRho)*Ck + s.lamRho ;                    
                    rhox    = (1-s.lamRho)*Cx;
                    rhoy    = (1-s.lamRho)*Cy;
                else
                    Hs = Heaviside(Ck);
                    rho     = (1-s.lamRho)*Hs + s.lamRho ;
                end
                
                %% Dynamic viscosity
                if (~s.sharpMu)
                    %mu      = (1-s.lamMu )*Ck + s.lamMu ;
                    dmudx   = (1-s.lamMu )*Cx ;
                    dmudy   = (1-s.lamMu )*Cy ;
                else
                    Hs = Heaviside(Ck);
                    mu     = (1-s.lamMu)*Hs + s.lamMu ;
                    
                    Ds = Delta(Ck);
                    dmudx = (1-s.lamMu )*Ds.*dCkdx ;
                    dmudy = (1-s.lamMu )*Ds.*dCkdy ;
                end
                
                %% Get all elemental operators/arrays
                Dxx     = element.getDxx;
                Dyy     = element.getDyy;
                vecZ    = element.getVecZ;
%                 matZ    = element.getMatZ;
                
                Uk = U.val2(e);
                Vk = V.val2(e);
                Ux = U.x2(e);
                Uy = U.y2(e);
                Vx = V.x2(e);
                Vy = V.y2(e);
                
                Uxx = U.xx2(e);
                Uxy = U.xy2(e);
                Uyy = U.yy2(e);
                Vxx = V.xx2(e);
                Vxy = V.xy2(e);
                Vyy = V.yy2(e);
                
%                 Pxx = P.xx2(e);
%                 Pyy = P.yy2(e);
%                 
%                 lapP = Pxx + Pyy;

                % Vorticities:
                %   omega_x = dVdz - dWdy
                %   omega_y = dWdx - dUdz 
                %   omega_z = dUdy - dVdx
                % 
                %   Lap(omega) = omega_xx + omega_yy + omega_zz = 0
                %
                %   DUDT = dUdt + U*dUdx + V*dUdy + W*dUdz
                %
                %   dUdt + U*dUdx + V*dUdy + W*dUdz + s.REI * ( (omega_y)_z - (omega_z)_y )
                %                                   + s.REI * ( (Wxz-Uzz) - (Uyy-Vxy)   )
                %                                   + s.REI * ( (Ux+Vy+Wz)_x - Lap(U) )

                s.opL{el} = [ Dxx+Dyy ];      % Laplacian

%                 gravity = s.FRI2 * ( rhox * s.gx + rhoy * s.gy );
%                 
%                 %% Setup G
%                 fv1 = vecZ - rho .* (Ux.^2 + 2*Uy.*Vx + Vy.^2) + gravity; % + Cx.*Px + Cy.*Py; % + alpha*(dUkdx+dVkdy);          % + s.WEI * (kxx{el}+kyy{el}+kzz{el}); %-( dUkdx.^2 + dVkdy.^2 + dWkdz.^2 + 2*( dUkdy.*dVkdx ) );
%                 
% %                 if (s.steady)
% %                     Dt       = matZ;
% %                     dUdt_rhs = vecZ;
% %                     dVdt_rhs = vecZ;
% %                 else
%                     Dt       = s.getTime();
%                     dUdt_rhs = s.getTime( element, U );
%                     dVdt_rhs = s.getTime( element, V );
% %                 end
%                 
%                 %% Advection : nonlinear terms
%                 advUU       = Uk .* Dt - dUdt_rhs + Uk.*Ux + Vk.*Uy;
%                 advVV       = Vk .* Dt - dVdt_rhs + Uk.*Vx + Vk.*Vy;
%                 
%                 fv1 = fv1 - ( rhox .* (advUU) + rhoy .* (advVV) );
%                 
%                 %% Diffusion : viscous terms
%                 difUU  = s.REI*dmudx.*( 4/3*Uxx +     Uyy ); 	% x-mom : U components
%                 difUV  = s.REI*dmudx.*( 1/3*Vxy           );    % x-mom : V components
%                 difVU  = s.REI*dmudy.*( 1/3*Uxy           );    % y-mom : U components
%                 difVV  = s.REI*dmudy.*(     Vxx + 4/3*Vyy ); 	% y-mom : V components
%                 
%                 fv1 = fv1 + difUU + difUV + difVU + difVV;
%                 
%                 if ( ~isempty(st) )
% %                     fv1 = fv1 - s.WEI * rho .* ( ssfx{el}./element.getW + ssfy{el}./element.getW );
%                     %fv1 = fv1 - s.WEI * rho .* divKappa{el};
%                     fv1 = fv1 + s.WEI * st{el};
%                 end
                
                fv1 = - rho .* ( Ux.^2 + Vx.^2 + Uy.^2 + Vy.^2 ) + s.WEI * kLapC{el} ; %  (momx_field.x2(e) + momy_field.y2(e)); % + lapP;
                
                s.opG{el} = [ fv1 ];
                
                if (s.withForcing)
                    s.opG{el} = s.opG{el} - s.forcing{el};
                end
                
                %% Scale equations with weights (can differ between elements)
                if (true)
                    unitVec = ones( size(vecZ) );
%                     scaling = [ unitVec./ ( rho .* (divKappa{el}+1) ) ];
        
%                     scaleCurv = unitVec;
%                     scaleCurv( (ssfx{el} + ssfy{el})~=0 ) = 1;
                    %scaleCurv( abs(divKappa{el})>2 ) = 10;
                    
%                     scaling = unitVec ./ (s.WEI * st{el}+1);
                    scaling = unitVec; % ./ rho;
                    
%                     scaling = [ unitVec./ ( rho .* scaleCurv ) ];
                else
                    scaling = 1;
                end
                
                %% Set quadrature weights, apply all scaling to L and G
                s.setWeightAndScale( element, scaling );
            end
            
            t2 = toc;
            time = t2-t1;
        end
        
        %% Private fields
        function [momx_field,momy_field] = constructMomentumFields(s)
            % Function to create momentum fields, which can be used to
            % determine the rhs of the 3rd stage
            
%             s.momx = Field
           
            momx = cell( numel(s.mesh.eRange),1 );
            momy = cell( numel(s.mesh.eRange),1 );

            % Get the source fields
            U   = FieldCollection.get('u');
            V   = FieldCollection.get('v');
            P   = FieldCollection.get('p');
            
            if ( System.settings.comp.coupling.enabled && s.coupled)
                C   = FieldCollection.get('c');
                %Omg = FieldCollection.get('omg');

                % Get the curvature from the CH simulation, use the NS.mesh as
                % the base mesh for the data
                CH = PhysicsCollection.get('CH');
                
%                 [kx,ky] = CH.getCurvature(s.mesh);
                
                % Droplet/bubble center of gravity velocity at temporal
                % quadrature points
                if (System.settings.phys.dynamicReferenceFrame)
                    CH.computeCenterOfMassVelocity(true,false);       % determine the center-of-mass velocity: compute the enclosed mass, and without overIntegration
                    
                    uc = CH.cmv_total(:,1) - System.settings.phys.bubbleVelocity(1);
                    vc = CH.cmv_total(:,2) - System.settings.phys.bubbleVelocity(2);
                    
                    absCenterVelocity = sqrt( uc.^2 + vc.^2 );
                else
                    uc  = [];
                    vc  = [];
                    absCenterVelocity = [];
                end
            else
                C   = NaN; 
                Omg = NaN; 
                uc  = [];
                vc  = [];
                
                s.kx_all = [];
                s.ky_all = [];
            end
           
            for e=s.mesh.eRange
                element = s.mesh.getElement(e);                             % Select the active element                
                el = element.localID;
                                
                % From CH (2=SolutionMode.Coupling)
                if (C~=C || isempty(C))
                    Ck      = 1;
                    dCkdx   = 0;
                    dCkdy   = 0;
                else
                    Ck      = C.val2(e,s.mesh);
                    dCkdx   = C.x2(e,s.mesh);
                    dCkdy   = C.y2(e,s.mesh);
                end
                
                %% Density
                if (~s.sharpRho)
                    rho     = (1-s.lamRho)*Ck + s.lamRho ;
                else
                    Hs = Heaviside(Ck);
                    rho     = (1-s.lamRho)*Hs + s.lamRho ;
                    %rho( Ck>=0.5 ) = 1;
                    %rho( Ck< 0.5 ) = s.lamRho;
                end
                
                %% Dynamic viscosity
                if (~s.sharpMu)
                    mu      = (1-s.lamMu )*Ck + s.lamMu ;
                    dmudx   = (1-s.lamMu )*dCkdx ;
                    dmudy   = (1-s.lamMu )*dCkdy ;
                else
                    Hs = Heaviside(Ck);
                    mu     = (1-s.lamMu)*Hs + s.lamMu ;
                    
                    Ds = Delta(Ck);
                    dmudx = (1-s.lamMu )*Ds.*dCkdx ;
                    dmudy = (1-s.lamMu )*Ds.*dCkdy ;
                    
                    %mu( Ck>=0.5 ) = 1;
                    %mu( Ck< 0.5 ) = s.lamMu;
                    %dmudx = 0; %(1-s.lamMu )*dCkdx ;
                    %dmudy = 0; %(1-s.lamMu )*dCkdy ;
                end
                
                %t(2) = toc;
                
%                 Uk    = U.val(FieldType.Current,e);
%                 dUkdx = U.x(FieldType.Current,e);
%                 dUkdy = U.y(FieldType.Current,e);
%                 
%                 Vk    = V.val(FieldType.Current,e);
%                 dVkdx = V.x(FieldType.Current,e);
%                 dVkdy = V.y(FieldType.Current,e);
% 
%                 dPkdx = P.x(FieldType.Current,e);
%                 dPkdy = P.y(FieldType.Current,e);
                
                Uk    = U.val2(e);
                dUkdx = U.x2(e);
                dUkdy = U.y2(e);
                uxx   = U.xx2(e);
                uxy   = U.xy2(e);
                uyy   = U.yy2(e);
                
                Vk    = V.val2(e);
                dVkdx = V.x2(e);
                dVkdy = V.y2(e);
                vxx   = V.xx2(e);
                vxy   = V.xy2(e);
                vyy   = V.yy2(e);
                
                dPkdx = P.x2(e);
                dPkdy = P.y2(e);
                
                % Get all elemental operators/arrays
                matZ    = element.getMatZ;
                vecZ    = element.getVecZ;

                if (s.steady)
                    Dt       = matZ;
                    dUdt_rhs = vecZ;
                    dVdt_rhs = vecZ;
                else
                    Dt       = s.getTime();
                    dUdt_rhs = s.getTime( element, U );
                    dVdt_rhs = s.getTime( element, V );
                end
                                
                % Moving reference frame
                if (System.settings.phys.dynamicReferenceFrame)
                    % x-direction
                    if ( ~isempty(uc) )
                        Uk = reshape( Uk, element.finite.qSize );
                        for i=1:numel(uc)
                            Uk(:,:,i) = Uk(:,:,i) - uc(i);
                        end
                        Uk = Uk(:);
                    else
                        uc = 0;
                    end
                    
                    % y-direction
                    if ( ~isempty(vc) )
                        Vk = reshape( Vk, element.finite.qSize );
                        for i=1:numel(vc)
                            Vk(:,:,i) = Vk(:,:,i) - vc(i);
                        end
                        Vk = Vk(:);
                    else
                        vc = 0;
                    end
                end
                
                %% Diffusion : viscous terms
                difUU  = s.REI*( 2*dmudx.*dUkdx +   dmudy.*dUkdy ) + s.REI * mu .* (uxx+uyy) + s.REI * mu / 3 .* uxx;      % x-mom : U components
                difUV  = s.REI*(   dmudy.*dVkdx                  )                           + s.REI * mu / 3 .* vxy;      % x-mom : V components
                difVU  = s.REI*(                    dmudx.*dUkdy )                           + s.REI * mu / 3 .* uxy;      % y-mom : U components
                difVV  = s.REI*(   dmudx.*dVkdx + 2*dmudy.*dVkdy ) + s.REI * mu .* (vxx+vyy) + s.REI * mu / 3 .* vyy;      % y-mom : V components

                %% Advection : nonlinear terms
                advUU       = Uk .* Dt - dUdt_rhs + Uk.*dUkdx + Vk.*dUkdy;
                advVV       = Vk .* Dt - dVdt_rhs + Uk.*dVkdx + Vk.*dVkdy;
                
                %% Gravity
                if ( System.settings.comp.coupling.enabled && s.coupled)
                    rhoa = s.lamRho;        % used to remove the effect of gravity on the phase with C=0
                else
                    rhoa = 1;
                end
                gxtmp = s.gx;
                gytmp = s.gy;
                
                momx{el} = rho.*advUU - difUU - difUV + s.gradP(1) - (s.FRI2)*(rho-rhoa).*gxtmp - s.WEI * s.kx_all{el};%  + dPkdx;       %% Equation 1: x-momentum
                momy{el} = rho.*advVV - difVU - difVV + s.gradP(2) - (s.FRI2)*(rho-rhoa).*gytmp - s.WEI * s.ky_all{el};%  + dPkdy;       %% Equation 2: y-momentum
                
            end
            
            periodic = [false false  ; ...
                        false false ];
            
            momx_field = Field(s.mesh,1,'momx',periodic);
            momy_field = Field(s.mesh,2,'momy',periodic);
            
            momx_field.addSubField(FieldType.Current,true);
            momy_field.addSubField(FieldType.Current,true);
            
            momx_field.setValue( [], momx );
            momy_field.setValue( [], momy );
            
            momx_field.computeAlpha;
            momy_field.computeAlpha;
        end
        
        %% Boundary conditions
        function out = divCurlx( s, element )
            e  = element.globalID;
            el = element.localID;
            
            C = FieldCollection.get('c');
            U = FieldCollection.get('u');
            V = FieldCollection.get('v');
                        
            u0  = U.oldVal(element.id,[],0);
            u1  = U.oldVal(element.id,[],1);
            u2  = U.oldVal(element.id,[],2);
            
            Ck  = C.val2( e );
            Cx  = C.x2( e );
            Cy  = C.y2( e );
            
            %% Density
            if (~s.sharpRho)
                rho     = (1-s.lamRho)*Ck + s.lamRho ;
            else
                Hs = Heaviside(Ck);
                rho     = (1-s.lamRho)*Hs + s.lamRho ;
            end
                
            %% Dynamic viscosity
            if (~s.sharpMu)
                mu      = (1-s.lamMu )*Ck + s.lamMu ;
                dmudx   = (1-s.lamMu )*Cx ;
                dmudy   = (1-s.lamMu )*Cy ;
            else
                error('please implement a sharp viscosity in divCurlx')
            end
            
            %% Curvature
            if (~isempty(s.kx_all))
                kx = s.kx_all{el};
            else
                kx = 0;
            end
            
            if ( ~isempty(u0) && ~isempty(u1) && ~isempty(u2) )
                DuDt = 0.5*( 3*u0 - 4*u1 + u2 ) / System.settings.time.step;
            elseif ( ~isempty(u0) && ~isempty(u1) )
                DuDt = ( u0 - u1 ) / System.settings.time.step;
            else
                DuDt = 0;
            end
            ux   = U.x2(e);
            uy   = U.y2(e);
            vx   = V.x2(e);
            uxx  = U.xx2(e);
            uyy  = U.yy2(e);
            vxy  = V.xy2(e);
            uux  = U.val2(e) .* U.x2(e);
            vuy  = V.val2(e) .* U.y2(e);
            
            %% Diffusion : viscous terms
            difUU = s.REI * ( 2*dmudx.*ux + dmudy.*uy + mu.*(uxx+uyy) + mu/3.*uxx );	% x-mom : U components
            difUV = s.REI * (   dmudy.*vx                             + mu/3.*vxy );    % x-mom : V components

            %% Gravity
            if ( System.settings.comp.coupling.enabled && s.coupled)
                rhoa = s.lamRho;        % used to remove the effect of gravity on the phase with C=0
            else
                rhoa = 1;
            end
            
            %            advection               diffusion   fixed grad(P)            gravit           surface tension
            out = -( rho.*(DuDt + uux + vuy) - difUU - difUV + s.gradP(1) - (s.FRI2)*(rho-rhoa).*s.gx - s.WEI * kx );
                  
            % Add forcing for the x-momentum equation for manufactured solutions
            if (s.withForcing)
                forcing = NS.stageForcing{1};
                out = out - forcing{ element.id }(1:element.finite.numQuadratureNodes);
            end            
        end
        
        function out = divCurly( s, element )
            e  = element.globalID;
            el = element.localID;
            
            C = FieldCollection.get('c');
            U = FieldCollection.get('u');
            V = FieldCollection.get('v');
            
            v0  = V.oldVal(element.id,[],0);
            v1  = V.oldVal(element.id,[],1);
            v2  = V.oldVal(element.id,[],2);
            
            Ck  = C.val2( e );
            Cx  = C.x2( e );
            Cy  = C.y2( e );
            
            %% Density
            if (~s.sharpRho)
                rho     = (1-s.lamRho)*Ck + s.lamRho ;
            else
                Hs = Heaviside(Ck);
                rho     = (1-s.lamRho)*Hs + s.lamRho ;
            end
                
            %% Dynamic viscosity
            if (~s.sharpMu)
                mu      = (1-s.lamMu )*Ck + s.lamMu ;
                dmudx   = (1-s.lamMu )*Cx ;
                dmudy   = (1-s.lamMu )*Cy ;
            else
                error('please implement a sharp viscosity in divCurlx')
            end
            
            %% Curvature
            if (~isempty(s.ky_all))
                ky = s.ky_all{el};
            else
                ky = 0;
            end
            
            if ( ~isempty(v0) && ~isempty(v1) && ~isempty(v2) )
                DvDt = 0.5*( 3*v0 - 4*v1 + v2 ) / System.settings.time.step;
            elseif ( ~isempty(v0) && ~isempty(v1) )
                DvDt = ( v0 - v1 ) / System.settings.time.step;
            else
                DvDt = 0;
            end
            vx   = V.x2(e);
            vy   = V.y2(e);
            uy   = U.y2(e);
            uxy  = U.xy2(e);
            vxx  = V.xx2(e);
            vyy  = V.yy2(e);
            uvx  = U.val2(e) .* V.x2(e);
            vvy  = V.val2(e) .* V.y2(e);
            
            %% Diffusion : viscous terms
            difVU = s.REI * (                 dmudx.*uy                 + mu/3.*uxy );  % y-mom : U components
            difVV = s.REI * (   dmudx.*vx + 2*dmudy.*vy + mu.*(vxx+vyy) + mu/3.*vyy );  % y-mom : V components

            %% Gravity
            if ( System.settings.comp.coupling.enabled && s.coupled)
                rhoa = s.lamRho;        % used to remove the effect of gravity on the phase with C=0
            else
                rhoa = 1;
            end
            
            %            advection               diffusion   fixed grad(P)          gravity          surface tension
            out = -( rho.*(DvDt + uvx + vvy) - difVU - difVV + s.gradP(2) - (s.FRI2)*(rho-rhoa).*s.gy - s.WEI * ky );
            %out = -( DvDt + uvx + vvy - s.REI * ( vxx - uxy ) );
            
            % Add forcing for the y-momentum equation for manufactured solutions
            if (s.withForcing)
                forcing = s.stageForcing{1};
                out = out - forcing{ element.id }(element.finite.numQuadratureNodes+1:2*element.finite.numQuadratureNodes);
            end
        end
        
        %% Post-processing
        function output(s,outputDir,id)
            data.u = s.getU( 1, s.getElementalAlpha(Variable.u) );
            for e=NMPI.instance.eStart:NMPI.instance.eEnd
                data.u{e} = reshape(data.u{e},s.mesh.getElement(e).disc.Q);
                data.u{e} = data.u{e}(:,:,end);
            end
            
            data.v = s.getU( 1, s.getElementalAlpha(Variable.v) );
            for e=NMPI.instance.eStart:NMPI.instance.eEnd
                data.v{e} = reshape(data.v{e},s.mesh.getElement(e).disc.Q);
                data.v{e} = data.v{e}(:,:,end);
            end
            
            data.p = s.getU( 1, s.getElementalAlpha(Variable.p) );
            for e=NMPI.instance.eStart:NMPI.instance.eEnd
                data.p{e} = reshape(data.p{e},s.mesh.getElement(e).disc.Q);
                data.p{e} = data.p{e}(:,:,end);
            end
            
            for e=1:s.mesh.getNumLocalElements
                data.x{e,1} = s.mesh.getLocalElement(e).xelem;
                data.y{e,1} = s.mesh.getLocalElement(e).yelem;
            end
            
            data.eStart = NMPI.instance.eStart;
            data.eEnd   = NMPI.instance.eEnd;
            
            save([outputDir 'info-' num2str(id,'%.4d')  '--' num2str(NMPI.instance.rank,'%.3d') '.mat'],'data') ;
        end   

        function plotExact(s,fig,position)
            if nargin==2
                position = 1;
            end
            
            s.plotMode = 1;
            s.plot(fig,position);
            s.plotMode = 0;
        end
        
        function plotInterpolated(s,fig,position)
            if nargin==2
                position = 1;
            end
            
            s.plotMode = 2;
            s.plot(fig,position);
            s.plotMode = 0;
        end
                
        function plotAll(s,fig,data,mesh)
            
            if nargin==3
                mesh = s.mesh;
            end
            
            screensize = get( groot, 'Screensize' );
            sizex = 1600;
            sizey = 600;
            set(fig,'position',[0.5*(screensize(3)-sizex) 0.5*(screensize(4)-sizey) sizex sizey]); 
            
            rows = 1;
            cols = 3;
            
            pbx = s.mesh.x1 - s.mesh.x0;
            pby = s.mesh.y1 - s.mesh.y0;
            
            
            % U-plot
            u = data{1};
            minU    = min( cellfun(@(x) min(x(:)),u) );
            maxU    = max( cellfun(@(x) max(x(:)),u) );
            
            %if minU==0 && maxU==0
                maxU = 1;
            %end
            
            pos = 1;
            delete(subplot(rows,cols,pos))
            subplot(rows,cols,pos)
            xlim([mesh.x0 mesh.x1])
            ylim([mesh.y0 mesh.y1])
            %zlim([minU    maxU   ])
            pbz = maxU - minU;
            pbaspect([pbx pby pbz])
            view([0,90])
            grid off;
            hold on;
            
            for e=1:mesh.numLocalElements
                element = mesh.getLocalElement(e);
                surf(element.xelem,element.yelem,u{e},'FaceColor','interp');
            end
            
            % V-plot
            v = data{2};
            minV    = min( cellfun(@(x) min(x(:)),v) );
            maxV    = max( cellfun(@(x) max(x(:)),v) );
            
            %if minV==0 && maxV==0
                maxV = 1;
            %end
            
            pos = 2;
            delete(subplot(rows,cols,pos))
            subplot(rows,cols,pos)
            xlim([mesh.x0 mesh.x1])
            ylim([mesh.y0 mesh.y1])
            %zlim([minV    maxV   ])
            pbz = maxV - minV;
            pbaspect([pbx pby pbz])
            view([0,90])
            grid off;
            hold on;
            
            v = data{2};
            for e=1:mesh.numLocalElements
                element = mesh.getLocalElement(e);
                surf(element.xelem,element.yelem,v{e},'FaceColor','interp');
            end

            
            % P-plot
            pos = 3;
            delete(subplot(rows,cols,pos))
            subplot(rows,cols,pos)
            p = data{3};
            for e=1:mesh.numLocalElements
                element = mesh.getLocalElement(e);
                surf(element.xelem,element.yelem,p{e},'FaceColor','interp');
                hold on;
            end
%             shading interp
            xlim([mesh.x0 mesh.x1])
            ylim([mesh.y0 mesh.y1])
            grid off;
            pbaspect([pbx pby 1])
            view([0,90])
            
%             % U-velocity at x=0.5
%             pos = 4;
%             delete(subplot(rows,cols,pos))
%             subplot(rows,cols,pos)
%             for e=1:mesh.numLocalElements
%                 element = mesh.getLocalElement(e);
%                 z=interp2(element.xelem,element.yelem,u{e},0.5,element.yelem);
%                 plot(element.yelem,z,'k');
%                 hold on;
%             end
%             xlim([0 1])
%             view([90,-90])
%             
%             % V-velocity at x=0.5
%             pos = 5;
%             delete(subplot(rows,cols,pos))
%             subplot(rows,cols,pos);
%             for e=1:mesh.numLocalElements
%                 element = mesh.getLocalElement(e);
%                 z=interp2(element.xelem,element.yelem,v{e},0.5,element.yelem);
%                 plot(element.yelem,z,'k');
%                 hold on;
%             end
%             ylim([-1e-3 1e-3])
%             view([90,-90]) 
        end
        
        % Main function which calls other functions
        function writeValues(s)
            filename = [ System.settings.outp.directory s.name '-' System.settings.outp.valueFilename '.txt' ];
            
            s.computeMaxVelocityNorm();
            
            if (System.rank==0)
                if (System.settings.time.current==0)
                    header = {'iter','time','maxVelocityNorm','dPdx','dPdy','velDivergence'};
                    System.writeHeader(filename,header)
                end

                values = zeros(6,1);
                values(1) = System.settings.time.iter; 
                values(2) = System.settings.time.current;
                values(3) = s.maxVelocityNorm;
                values(4) = s.gradP(1);
                values(5) = s.gradP(2);
                values(6) = s.computeDivergence(false);

                System.appendToFile(filename,values)
            end
        end
        
%         % Default refinement criteria (false)
%         function criteria = getRefinementCriteria(s,e)
%             criteria(1) = false;
%         end
        
        %% Function to derive the kappa field from the C field
        function deriveP(s,variableNumber)
            pField = s.fields{variableNumber};

            CH = PhysicsCollection.get('CH');
            %kappaField = CH.getKappa();
            cField     = CH.fields{1};
            
            for e=s.mesh.eRange
                element = s.mesh.getElement(e);
                el = element.localID;

                
% 
%                 C   = cField.val2( e );
%                 Cx  = cField.x2( e );
%                 Cy  = cField.y2( e );
%                 Cxx = cField.xx2( e );
%                 Cxy = cField.xy2( e );
%                 Cyy = cField.yy2( e );
% 
%                 normC1 = Cx.^2 + Cy.^2;
% 
%                 pValues = s.WEI * s.Cn_L * ( ( Cy.^2 .* Cxx + Cx.^2.*Cyy - 2*Cx.*Cy.*Cxy )./ normC1);
                
                pValues = s.WEI * cField.val2(e,s.mesh);
                
                %pValues = s.WEI * kappaField{e};

                pField.setValue(el,pValues);
            end
                    
            pField.computeAlpha;
        end
        
        function setZeroGravity(s,flag)
            s.zeroGravity = flag;
        end
        
        function computeMaxVelocityNorm(s)
            U = s.getField( Variable.u );
            V = s.getField( Variable.v );
            
            maxVelocityNorm = 0;
            
            for e=s.mesh.eRange                
                % Get dUkdx and dVkdy
                Uk = U.val(FieldType.Current,e);
                Vk = V.val(FieldType.Current,e);
                
                maxU = max(abs(Uk(:)));
                maxV = max(abs(Vk(:)));
                
                maxLocal = max(maxU,maxV);
                
                maxVelocityNorm = max(maxVelocityNorm,maxLocal);
            end
            
            s.maxVelocityNorm = NMPI.Allreduce(maxVelocityNorm,1,'M','maxVelocityNorm');
            
            %if (NMPI.instance.rank==0)
            %    fprintf('Maximum velocity norm (defined as: max(|U|,|V|)) : %e\n',maxVelocityNorm);
            %end
        end
        
        function out = computeDivergence(s,verbose)
            
            if nargin==1
                verbose = true;
            end
            
            U = s.getField( Variable.u );
            V = s.getField( Variable.v );
            
            L2divTotal = 0;
            check = 0;
            
            for e=s.mesh.eRange
                
                % Set the element
                element = s.mesh.getElement(e);
                
                %[X,Y,Z,T] = element.getNodes;
                
                % Get dUkdx and dVkdy
                dUkdx = U.x(FieldType.Current,e);
                dVkdy = V.y(FieldType.Current,e);
                
                div   = dUkdx + dVkdy;
                
                w     = element.getW;
                
                % Set the integration weights and Jacobian for 2D spatial planes
                %w = finiteElement.W;
                %J = element.J;
      
                L2divTotal = L2divTotal + dot( div(:).^2, w);
                check = check + dot( (1+0*div(:)).^2, w);
            end
            
            %s.massTotal     = NMPI_Allreduce(massTotal   ,Qt,'+',NMPI.instance.mpi_comm_world);
            %s.massEnclosed  = NMPI_Allreduce(massEnclosed,Qt,'+',NMPI.instance.mpi_comm_world);
            %checkGlobal     = NMPI_Allreduce(check,Qt,'+',NMPI.instance.mpi_comm_world);
            
            %s.massLossTotal     = s.massTotal(1)   -s.massTotal(end);
            %s.massLossEnclosed  = s.massEnclosed(1)-s.massEnclosed(end);
            %checkLoss           = check(1)-check(end);
            
            if (NMPI.instance.rank==0 && verbose)
                fprintf('Total divergence error (L2) : %e (volume check : %e)\n',sqrt(L2divTotal),check);
            end
            
            if (~verbose)
                out = L2divTotal;
            end
        end
        
        function plotDivergence(s)
            U = s.getField( Variable.u );
            V = s.getField( Variable.v );
            
            hold on;
            
            for e=s.mesh.eRange
                
                % Set the element
                element = s.mesh.getElement(e);
                
                [X,Y,Z,T] = element.getNodes;
                
                % Get dUkdx and dVkdy
                dUkdx = U.x2(e);
                dVkdy = V.y2(e);
                
                div = dUkdx + dVkdy;
                div = reshape(div,size(X));
                
                surf(X,Y,div);
            end
        end
        
        function plotVorticity(s)
            U = s.getField( Variable.u );
            V = s.getField( Variable.v );
            
            hold on;
            
            for e=s.mesh.eRange
                
                % Set the element
                element = s.mesh.getElement(e);
                
                [X,Y,Z,T] = element.getNodes;
                
                % Get dUkdx and dVkdy
                dUkdy = U.y2(e);
                dVkdx = V.x2(e);
                
                div = dUkdy - dVkdx;
                div = reshape(div,size(X));
                
                surf(X,Y,div);
            end
        end
        
        % The local wall shear stress is defined as (see Fluid Mechanics 
        % (Kundu & Cohen) page 335):
        %
        %   tauw = mu(C) * du/dn,
        %
        % where u is the velocity parallel to the wall, and n the wall
        % normal vector. By integrating this value along a wall, the drag 
        % can be determined.
        %
        % Parameters:
        %  boundary     plane to integrate over
        %  disc         discretization (optional, for over-integration)
        %
        function drag = computeDrag(s,boundary)
            % Initialize the variables
            drag = 0;
            
            U = FieldCollection.get('u');
            V = FieldCollection.get('v');
            C = FieldCollection.get('c');
                                    
            % If no C is available assume C=1 and vc=0
            if (C~=C)
                mu = @(e,disc) 1;
            else
                mu  = @(e,disc) (1-s.lamMu)*C.getValueInterpolated( e ,disc, s.mesh) + s.lamMu;
            end

            if System.settings.time.method==TimeMethod.SpaceTime
                if (boundary.dir==1)
                    velGradient = @(e,disc) V.getDxInterpolated( e ,disc, s.mesh);
                elseif (boundary.dir==2)
                    velGradient = @(e,disc) U.getDyInterpolated( e ,disc, s.mesh);
                end
            else
                if (boundary.dir==1)
                    velGradient = @(e,disc) U.getDyInterpolated( e ,disc, s.mesh);
                elseif (boundary.dir==2)
                    velGradient = @(e,disc) V.getDxInterpolated( e ,disc, s.mesh);
                end
            end
            
            drag = @(e,disc) mu(e,disc) .* velGradient(e,disc);
            drag = boundary.computeIntegral( drag, false );
                        
            % Correct the sign:
            drag = boundary.patchList{1}.dir * drag;
            
            % For verification purposes:
            %t       = @(e,disc) 1;
            %area    = boundary.computeIntegral( t, false );
            
            % To get the corresponding pressure gradient for a symmetric channel:
            %channelLength = System.settings.mesh.y(end) - System.settings.mesh.y(1);
            %dpdy = 2*drag / ( System.settings.phys.Re * System.settings.time.step * channelLength );
            
            writeToScreen = false;
            
            if (writeToScreen)
                if (NMPI.instance.rank==0)
                    %fprintf('Total mass : %f (effective radius : %f)\n',s.mass,sqrt(s.mass/pi))
                    fprintf('drag   = %8.2e\n',drag);
                end
            end
        end
        
       	function preTimeLoop(s)
            if (s.usePreTimeLoop)
                
                % Use the cField to setup the pressure
                %cField = FieldCollection.get('c');
                %s.fields{3}.add(cField);
                
                s.preTimeStep();
                
                timeMethod   = s.timeMethod;
                s.timeMethod = TimeMethod.Steady;
                
                %% Apply boundary conditions
                if (s.numStages==1)
                    s.resetBoundaryConditions();
                    activeSettings = System.settings;
                    activeSettings.setBoundaryConditions( s );
                end

                % By solving the steady equations, the solution will be
                % obtained faster
                s.steady    = true;
                s.coupled   = true; 
                s.maxNonlinear = 5;
                
                eqWeights_tmp = s.eqWeights;
                
                g_tmp = [ s.gx; s.gy ];
                s.gx = 0; s.gy = 0;
                
%                 if (System.settings.time.method==TimeMethod.SpaceTime)
%                     s.eqWeights(1) = 0.2;
%                     s.eqWeights(2) = 0.2;
%                     s.eqWeights(3) = 1;
%                 else
%                     s.eqWeights(1) = 1;
%                     s.eqWeights(2) = 1;
%                     s.eqWeights(3) = 1;
%                 end

    %             %% Apply temporal conditions
    %             if (System.settings.time.spaceTime)
    %                 s.updateTimeSlab();
    %                 if (~s.steady)
    %                     s.applyInitialConditions();
    %                 end
    %             elseif ~isempty(System.settings.time.steppingMethod)
    %                 physics.copySolution(FieldType.Timelevel1);
    %             end

                %% Solve nonlinear equations                
                s.solveNonLinear(1);

                s.timeMethod    = timeMethod;
                s.steady        = s.settings.steady;
                s.coupled       = s.settings.coupled;
                s.maxNonlinear  = s.settings.maxNonlinear;
                %s.eps          = s.settings.eps;
                s.eqWeights     = eqWeights_tmp;
                s.gx = g_tmp(1); s.gy = g_tmp(2);
            end
        end
        
        function preTimeStep(s)
            if (s.usePreTimeStep)
                % Compute the new the gradP
                CH = PhysicsCollection.get(2);

                CH.computeCenterOfMassVelocity(true,false);       % determine the center-of-mass velocity: compute the enclosed mass, and without overIntegration
                %uc = CH.cmv(:,1);
                vc = CH.cmv_total(:,2);

                %if (isempty(s.vc_old))
                %    s.vc_old = vc;
                %end                    

                denominator = abs(mean(vc));
                
                newScheme = true;
                
                if (~newScheme)
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    %% OLD PRESSURE CORRECTION SCHEME %%
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    % This old pressure correction scheme uses the drop
                    % velocity to rescale the pressure gradient. This
                    % approach is used in Aland et al. (2013), but did not
                    % work very well here 
                    
                    %if (s.gradP(2)>=0)
                    %    s.gradP(2) = System.settings.phys.system{1}.pressureGradient(2);
                    %end

                    % Set the adaptive pressure gradient
                    if ( denominator ~= 0 )
                        %if ( abs(s.oldMean-1) - abs(denominator-1) > 0 )
                        %    if ( mod(System.settings.time.current,10)==5 )
                        %        s.gradP(2) = s.gradP(2) ./ denominator;
                        %    end
                        %else
                            s.gradP(2) = s.gradP(2) ./ denominator;
                        %end 
                    end

                    s.oldMean = denominator;
                    
                else
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    %% NEW PRESSURE CORRECTION SCHEME %%
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    % This new pressure correction scheme uses the total
                    % wall shear stress to determine the drag. From this
                    % drag a pressure gradient is computed, which should
                    % maintain a constant bulk velocity. To reach a constant
                    % bubble/droplet velocity of 1.0, the pressure gradient 
                    % is divided by this velocity.
                    %
                    boundary  = s.initFEBoundary(1,0,false);
                    dragLeft  = s.computeDrag(boundary);

                    boundary  = s.initFEBoundary(1,1,false);
                    dragRight = s.computeDrag(boundary);

                    channelLength = System.settings.mesh.y(end) - System.settings.mesh.y(1);
                    
                    pressureGradient = (dragLeft + dragRight) / ( System.settings.phys.Re * channelLength );
                    
                    if (s.timeMethod==TimeMethod.SpaceTime)
                        pressureGradient = pressureGradient / System.settings.time.step;
                    end

                    if (pressureGradient~=0)
                        s.gradP(2) = pressureGradient ./ denominator;
                    end
                end

                if (System.rank==0)
                    fprintf('Updated pressure gradient : %e [ abs(mean(vc)) : %e ]\n',s.gradP(2),denominator);
                end
            end
        end
        
        
        function plotPressure(s)
            C = s.getField( Variable.c );
            P = s.getField( Variable.p );
            
            hold on;
            
            kappaField = FieldCollection.get('kap');
            
            for e=s.mesh.eRange
                
                % Set the element
                element = s.mesh.getElement(e);
                
                [X,Y,Z,T] = element.getNodes;
                
                % Get dUkdx and dVkdy
                dCkdx = C.x2(e);
                dCkdy = C.y2(e);
                px = P.x2(e);
                py = P.y2(e);
                
                kappa = kappaField.val2(e);
                
                gradC = dCkdx.^2 + dCkdy.^2;
                
                value = s.Cn_L * sqrt(px.^2 + py.^2) - kappa;
                %value = (dCkdx .* px + dCkdy .* py) ./ gradC;
                value = reshape(value,size(X));
                
                surf(X,Y,value);
            end
        end
    end
    
    methods (Static)
        function plotParallel(nproc,variable)
            
            fig = figure(1);
            clf
            
            for i=1:nproc
                disp('NEW PROC')
                temp = load(['NS_' int2str(i-1) '.mat']);
                temp.NS.plotVariable(fig,0,variable,'')
                hold on
            end
            
            drawnow;            
        end 
    end
    
end

