classdef no_subcooling < Physics
    %ENERGYEQUATION_NSK Implementation of the energy equation for NSK
    %   Detailed explanation goes here    
    properties (Access=private)
        REI; WEI;
    end
    
    properties (Constant)
        % Variable ids for this physics
        T = 1;
    end
    
    properties
        Re; We; k; Cv;
    end
    
    methods
        function s = no_subcooling(mesh,settings)
            s = s@Physics(mesh);
            
            s.shortName = 'EE';
            s.longName  = 'Energy_NSK';
            s.name      = 'EE';
            
            s.nEq       = 1;
            s.variables = {'T'};
            
            s.initialize(settings);
        end
        
        function initialize(s,settings)
            initialize@Physics(s,settings);

            s.steady    = settings.steady;
            
            s.Re        = System.settings.phys.Re;
            s.We        = System.settings.phys.We;
            s.k         = System.settings.phys.k;
            s.Cv        = System.settings.phys.cv;
            
            s.REI       = 1.0/s.Re;
            s.WEI       = 1.0/s.We;
        end

        function time = setupEquations(s)
            % Function to setup the energy equation for the NSK. These
            % equations can be found in the following article of Keunsoo 
            % (at the end of section 2):
            %   Thermal two-phase flow with a phase-field method
            %   Int. Journal of Multiphase Flow, 2019
            %
            t1 = toc;
            
            weight1 = s.eqWeights(1);
            
            U   = FieldCollection.get('u');
            V   = FieldCollection.get('v');
            R   = FieldCollection.get('R');
            q   = FieldCollection.get('q');
                        
            for e=s.mesh.eRange
                element = s.mesh.getElement(e);         % Select the active element
                el      = element.localID;              % Get the local element number
                
                %% elemental operators
                H       = element.getH;
                Dx      = element.getDx;
                Dy      = element.getDy;
                if (s.steady)
                    Dt = 0;
                else
                    Dt = element.getDt;
                end
                Dxx     = element.getDxx;
                Dyy     = element.getDyy;
                Wel     = element.getW;
                matZ    = element.getMatZ;              %#ok<NASGU>
                vecZ    = element.getVecZ;              %#ok<NASGU>
                
                
                %% u-velocity derivatives
                Uk      = U.val2(e,s.mesh);
                Ux      = U.x(FieldType.Current,e);
                Uy      = U.y(FieldType.Current,e);
                Ut      = U.t(FieldType.Current,e);
                Uxx     = U.xx(FieldType.Current,e);
                Uxy     = U.xy(FieldType.Current,e);
                Uyy     = U.yy(FieldType.Current,e);
                
                %% v-velocity derivatives
                Vk      = V.val2(e,s.mesh);
                Vx      = V.x(FieldType.Current,e);
                Vy      = V.y(FieldType.Current,e);
                Vt      = V.t(FieldType.Current,e);
                Vxx     = V.xx(FieldType.Current,e);
                Vxy     = V.xy(FieldType.Current,e);
                Vyy     = V.yy(FieldType.Current,e);
                
                %% density derivatives
                Rk      = R.val2(e,s.mesh);
                Rx      = R.x(FieldType.Current,e);
                Ry      = R.y(FieldType.Current,e);
                Rxt     = R.xt(FieldType.Current,e);
                Ryt     = R.yt(FieldType.Current,e);
                Rxx     = R.xx(FieldType.Current,e);
                Rxy     = R.xy(FieldType.Current,e);
                Ryy     = R.yy(FieldType.Current,e);
                
                %% q derivatives (q is the Laplacian of the density R, so q = Rxx + Ryy)
                qk      = q.val2(e,s.mesh);
                qx      = q.x(FieldType.Current,e);
                qy      = q.y(FieldType.Current,e);
                
                %% auxiliary terms
                convU   = Ut + Uk.*Ux + Vk.*Uy;
                convV   = Vt + Uk.*Vx + Vk.*Vy;
                divU    = Ux + Vy;
                Conv    = Dt + Uk.*Dx + Vk.*Dy; 
                Lap     = Dxx + Dyy;
                
                PT      = 8*Rk ./ (3-Rk);
                PR      = 24 ./ (3-Rk).^2;
                
                %% RHS terms
                alpha   = - s.WEI * (   Rx.*(Rxt + Uk.*Rxx + Vk.*Rxy) + Ry.*(Ryt + Uk.*Rxy + Vk.*Ryy) ) ...
                          - ( (Rk.*Uk).*convU + (Rk.*Vk).*convV ) - (3*Rk.^2 + (s.WEI/2) * (Rx.^2+Ry.^2)) .* divU;
                           
                % beta    = -s.WEI * ( divU .* ( Rx.^2+Ry.^2 + Rk.*(Rxx+Ryy) ) ...
                %                      + Rk.* ( (Uxx+Vxy).*Rx + (Uxy+Vyy).*Ry ) );
                beta    = 0;
                gamma   = s.REI * (   Uk .* ( (4/3)*Uxx + (1/3)*Vxy + Uyy ) + Vk .* ( Vxx + (1/3)*Uxy + (4/3)*Vyy ) ...
                                    + (4/3)*(Ux.^2) - (4/3)*Ux.*Vy + Uy.^2 + Vx.^2 + 2*Vx.*Uy + (4/3)*Vy.^2 ) ;
                                
                delta   = s.WEI * ( (Rk.*Uk).*qx + (Rk.*Vk).*qy + Ux .* ( Rk.*qk - 0.5*Rx.^2 + 0.5*Ry.^2 ) ...
                                            - (Rx.*Ry).*(Uy+Vx) + Vy .* ( Rk.*qk + 0.5*Rx.^2 - 0.5*Ry.^2 ) ) ;
            
                epsilon = 6 * Rk .* ( Uk.*Rx + Vk.*Ry ) + 3 * (Rk.^2) .* divU;
            
                %% Assign L and G
                s.opL{el} = weight1 * [ s.Cv*Rk.*Conv + PT.*(Uk.*Dx + Vk.*Dy) - s.k * Lap + (PR.*(Rx.*Uk + Ry.*Vk) + PT.*divU).*H ]; %#ok<NBRAK>
            
                s.opG{el} = weight1 * [ alpha + beta + gamma + delta + epsilon ]; %#ok<NBRAK>

                s.opL{el}   = s.opL{el} * superkron(eye(s.Nvar),s.mesh.Z{e});
                s.W{el}     = repmat(Wel,s.Nvar,1);
                s.sqrtW{el} = repmat(sqrt(Wel),s.Nvar,1);
            end
            
            t2 = toc;
            time = t2-t1;
        end              

        function updateBC(s)
            % do nothing
        end

        function prescribed = getPrescribed(s,e)
            if nargin==2
                prescribed = [s.fields{1}.getPrescribed(e)];
            else
                prescribed = cell(s.mesh.numLocalElements,1);
                for e=1:numel(prescribed)
                    element = s.mesh.getLocalElement(e);
                    prescribed{e} = s.getPrescribed( element.globalID );
                end
            end
        end
        
        function out = getPrescribedValues(s,e)
            out = [s.fields{1}.getPrescribedValues(e)];
        end

        function output(s,outputDir,id)
            data.t = s.getT( 1, s.getElementalAlpha(Variable.t) );
            for e=Parallel3D.MPI.eStart:Parallel3D.MPI.eEnd
                data.t{e} = reshape(data.t{e},s.mesh.getElement(e).disc.Q);
                data.t{e} = data.t{e}(:,:,end);
            end
                        
            for e=1:s.mesh.getNumLocalElements
                data.x{e,1} = s.mesh.getLocalElement(e).xelem;
                data.y{e,1} = s.mesh.getLocalElement(e).yelem;
            end
            
            data.eStart = Parallel3D.MPI.eStart;
            data.eEnd   = Parallel3D.MPI.eEnd;
            
            save([outputDir 'info-' num2str(id,'%.4d')  '--' num2str(Parallel3D.MPI.rank,'%.3d') '.mat'],'data') ;
        end   

        function criteria = getRefinementCriteria(s,e)
            criteria(1) = false;
        end
        
        % function out = save(s)
        %     out = [];
            
        %     if (System.rank==0 || System.settings.outp.parallelWrite)
        %         EE                = struct;
        %         EE.class          = class(s);
        %         EE.name           = s.name;
        %         EE.mesh           = s.mesh.save;
                
        %         EE.settings       = s.settings;
                
        %         EE.fields       = cell(s.Nvar,1);
        %         for n=1:s.Nvar
        %             EE.fields{n} = save( s.fields{n} );
        %         end
                
        %         % Additional NS settings
        %         EE.steady       = s.steady;                 % True if the system of equations is steady
        %         EE.fdata        = s.fdata;
        %         EE.exact        = s.exact;                  % True if this physics has an exact solution
        %         EE.maxLevel     = s.maxLevel;               % Maximum level for AMR
        %         EE.eqWeights    = s.eqWeights;              % Weights for every equation
        %         EE.nConvType    = s.nConvType;              % Residual convergence criteria based on [ value; residual ]
        %         EE.cConvType    = s.cConvType;              % Coupling convergence criteria based on [ value; residual ]
        %         EE.nMaxIter     = s.nMaxIter;               % Maximum number of nonlinear iterations
        %         EE.cMaxIter     = s.cMaxIter;               % Maximum number of coupling iterations
        %         EE.nConvergence = s.nConvergence;           % Nonlinear convergence for each variable
        %         EE.cConvergence = s.cConvergence;           % Coupling convergence for each variable
        %         EE.rConvergence = s.rConvergence;           % Residual convergence for each equation and the whole system
                
        %         out = EE;
        %     end
        % end
        
        function preTimeLoop(s)
            if (s.usePreTimeLoop)
                
                s.preTimeStep();

                s.resetBoundaryConditions();
                Settings.setBoundaryConditions( s );

                s.steady    = true;
                s.coupled   = true;
              
                s.solveNonLinear();

                s.steady    = s.settings.steady;
                s.coupled   = s.settings.coupled;
            end
        end
    end
    
    methods (Static)
        function out = load(solution)
            if (~isa(solution,'Solution'))
                error('The first argument of load should be a Solution object')
            end
            
            out = no_subcooling;
            out.loadObject(solution);
        end 
    end
    
end

