classdef NSK1D < Physics
  % NSK 1D system
  
  properties (Constant)
    % Variable ids for this physics
    R = 1;
    q = 2;
  end

  properties
    R_g, a, b, T, K_cap;
  end % properties

  methods
    function s = NSK1D(mesh,settings)
      s = s@Physics(mesh);
      
      s.shortName = 'NSK1D';
      s.longName  = 'NavierStokesKorteweg_1Dimensional';
      s.name    = 'NSK1D';

      s.nEq     = 2;
      s.variables = {'R','q'};
      
      s.initialize(settings);
    end
    
    function initialize(s,settings)
      initialize@Physics(s,settings);

      s.steady  = settings.steady;
      s.R_g   = 8.314;
      s.a     = System.settings.phys.first_vdw;
      s.b     = System.settings.phys.second_vdw;
      s.T     = System.settings.phys.temp;
      s.K_cap = System.settings.phys.K;
    end

    function time = setupEquations(s)
      % Function to setup the NSK equation. These equations can be 
      % found in the following article of Keunsoo (section 2):
      %   Thermal two-phase flow with a phase-field method
      %   Int. Journal of Multiphase Flow, 2019
      %
      t1 = toc;
      
      R   = FieldCollection.get('R');
      q   = FieldCollection.get('q');
      R_g = s.R_g;

      for e=s.mesh.eRange
        element	= s.mesh.getElement(e);     % Select the active element
        el    = element.localID;        % Get the local element number
        
        %% elemental operators
        matZ  = element.getMatZ;
        vecZ  = element.getVecZ;
        H     = element.getH;
        Dx    = element.getDx;
        Dy    = element.getDy;
        if (s.steady)
          Dt     = 0;
          dUdt_rhs = vecZ;
          dVdt_rhs = vecZ;
        else
          Dt      = s.getTime( element );
          dRdt_rhs  = s.getTime( element, R );
        end
        Dxx   = element.getDxx;
        Dxy   = element.getDxy;
        Dyy   = element.getDyy;


        %% density derivatives
        Rk    = R.val2(e,s.mesh);
        Rx    = R.x(FieldType.Current,e);
        Ry    = R.y(FieldType.Current,e);
        
        %% q derivatives (q is the Laplacian of the density R, so q = Rxx + Ryy)
        qx    = q.x(FieldType.Current,e);
        qy    = q.y(FieldType.Current,e);

        %% auxiliary terms
        Lap   = Dxx + Dyy;

        s.opL{el} = [ (((R_g * s.T)/(1 - s.b .* Rk).^2) - 2 * s.a * Rk) * Dx, - s.K_cap .* Rk .* Dx ;
                      Dxx, -H ];
        s.opG{el} = [ vecZ ;
                      vecZ ] ;

        s.setWeightAndScale( element );
      end
      
      t2 = toc;
      time = t2-t1;
    end        

    function updateBC(s)
      % do nothing
    end

    function prescribed = getPrescribed(s,e)
      if nargin==2
        prescribed = [s.fields{1}.getPrescribed(e)];
      else
        prescribed = cell(s.mesh.numLocalElements,1);
        for e=1:numel(prescribed)
          element = s.mesh.getLocalElement(e);
          prescribed{e} = s.getPrescribed( element.globalID );
        end
      end
    end
    


    function output(s,outputDir,id)
      data.t = s.getT( 1, s.getElementalAlpha(Variable.t) );
      for e=Parallel3D.MPI.eStart:Parallel3D.MPI.eEnd
        data.t{e} = reshape(data.t{e},s.mesh.getElement(e).disc.Q);
        data.t{e} = data.t{e}(:,:,end);
      end
            
      for e=1:s.mesh.getNumLocalElements
        data.x{e,1} = s.mesh.getLocalElement(e).xelem;
        data.y{e,1} = s.mesh.getLocalElement(e).yelem;
      end
      
      data.eStart = Parallel3D.MPI.eStart;
      data.eEnd   = Parallel3D.MPI.eEnd;
      
      save([outputDir 'info-' num2str(id,'%.4d')  '--' num2str(Parallel3D.MPI.rank,'%.3d') '.mat'],'data') ;
    end

    % Default refinement criteria (false)
    function criteria = getRefinementCriteria(s,e)
      criteria(1) = false;
    end

     function preTimeLoop(s)
      if (s.usePreTimeLoop)
        
        s.preTimeStep();
        
        %% Apply boundary conditions
        s.resetBoundaryConditions();
        Settings.setBoundaryConditions( s );

        % By solving the steady equations, the solution will be
        % obtained faster
        s.steady  = true;
        s.coupled   = true;

        %% Solve nonlinear equations        
        s.solveNonLinear();

        s.steady  = s.settings.steady;
        s.coupled   = s.settings.coupled;
      end
    end
    
    function out = addDroplet(s,rhoArray,x,y,~,~,center,radius)
      %function w = Rho_begin(x,y,xc,yc,r0,Rho1,Rho2,We)
  
      % old Keunsoo code
      %out = (Rho1+Rho2)/2 - (Rho1-Rho2)/2*tanh((sqrt((x-xc).^2+(y-yc).^2)-r0)*sqrt(We)/2) ;
      %out = Rho2*ones(size(x)) ;
      
      Rho1 = rhoArray(1);
      Rho2 = rhoArray(2);
      
      r0 = radius;
      xc = center(1);
      yc = center(2);
      
      d  = r0 - sqrt((x-xc).^2+(y-yc).^2);    % positive inside the droplet
      
      out = (Rho1+Rho2)/2 + (Rho1-Rho2)/2 * tanh( d * sqrt(s.We)/2 );
    end
    
    function deriveQ(s,variableNumber)
      qField = s.fields{variableNumber};
      
      for e=s.mesh.eRange
        element = s.mesh.getElement(e);
        el = element.localID;

        rField = s.fields{1};
        Rxx = rField.xx2( e );
        Ryy = rField.yy2( e );

        qValues = s.WEI * ( Rxx + Ryy );

        qField.setValue(el,qValues);
      end
      
      qField.computeAlpha;
    end
  end
  
  methods (Static)
    function out = load(solution)
      if (~isa(solution,'Solution'))
        error('The first argument of load should be a Solution object')
      end
      
      out = EnergyEquation_NSK;
      out.loadObject(solution);
    end 
  end
  
end

