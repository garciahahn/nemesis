classdef Equations < Physics
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        f;
        sol_f11;
        sol_f12;
        sol_f13;
        sol_f21;
        sol_f22;
        sol_f23;
        
        xSlope, ySlope;
        
        withGlobal, directSolve;
        
        c,d;
        
        alphaGlobal;
    end
    
    methods
        function s = Equations(mesh)
            s.mesh = mesh;
            
            s.Nvar = 1;
            
            % global approach
            s.withGlobal = false; 
            s.directSolve = true; % solver of the global approach: direct / QR-decomposition
            
            s.f = cell(mesh.getNumGlobalElements,1);
            s.alpha = cell(mesh.getNumGlobalElements,1);
            
            s.sol_f11 = cell(mesh.getNumGlobalElements,1) ;
            s.sol_f12 = cell(mesh.getNumGlobalElements,1) ;
            s.sol_f13 = cell(mesh.getNumGlobalElements,1) ;
            s.sol_f21 = cell(mesh.getNumGlobalElements,1) ;
            s.sol_f22 = cell(mesh.getNumGlobalElements,1) ;
            s.sol_f23 = cell(mesh.getNumGlobalElements,1) ;

            s.xSlope = 10;
            s.ySlope = 10;
        end
        
        function criteria = getRefinementCriteria(s,e)
            element = s.mesh.getGlobalElement(e);
            criteria(1) = (max(abs(element.xelem))<=0.5 && max(abs(element.yelem))<=2.0);
        end
              
        function setFunction(s,c,d)
            % General function: f(x,y,z) = c1 * x^d1 + c2 * y^d2 + c3 * z^d3;
            for e=1:s.mesh.getNumGlobalElements
                element = s.mesh.getGlobalElement(e);
                
                Qx = element.disc.dofeq(1);
                Qy = element.disc.dofeq(2);
                Qz = element.disc.dofeq(3);
                
                s.f{e} = zeros(Qx,Qy,Qz) ;
                s.c = c;
                s.d = d;
                

                xc = (s.mesh.x1+s.mesh.x0);
                yc = (s.mesh.y1+s.mesh.y0);
                zc = (s.mesh.z1+s.mesh.z0);

                for z=1:Qz
                    s.f{e}(:,:,z) = kron( ones(1,Qy), c(1)*(element.xelem - xc).^d(1)) ...
                                   + kron( (c(2)*(element.yelem - yc).^d(2))', ones(Qx,1)) ...
                                   + c(3) * (element.zelem(z) - zc).^d(3) * kron( ones(1,Qy), ones(Qx,1));
                end
            end 
        end
        
        function createAlpha(s)
            % Compute the corresponding 'alpha' for each function
            for e=1:s.mesh.getNumGlobalElements
                element = s.mesh.getGlobalElement(e);
                s.alpha{e} = zeros(element.disc.dofev*s.mesh.getNumGlobalElements,1);
                s.alpha{e} = (element.getH * s.mesh.Z{e}) \ s.f{e}(:);
            end
        end
        
        function createAlphaGlobal(s)
            alpha = zeros(s.getNumDof,1);
            
            % Compute the corresponding 'alpha' for each function
%             for l=2:-1:0
%             for l=0:1:2
                for e=1:s.mesh.getNumGlobalElements
                    element = s.mesh.getGlobalElement(e);
%                     if (element.getLv==l)
                        alpha( element.GM(:) ) = (element.getH * element.Z ) \ s.f{e}(:);
%                     end
                end
%             end

            s.alphaGlobal = alpha;
        end
        
        function solve(s)
            for e=1:s.mesh.getNumGlobalElements
                element = s.mesh.getGlobalElement(e);               
                s.sol_f11{e} = element.getDx  * element.Z * s.alpha{e};
                s.sol_f12{e} = element.getDy  * element.Z * s.alpha{e};
                s.sol_f13{e} = element.getDz  * element.Z * s.alpha{e};
                s.sol_f21{e} = element.getDxx * element.Z * s.alpha{e};
                s.sol_f22{e} = element.getDyy * element.Z * s.alpha{e};
%                 if (C{4}=='C1') 
%                     s.sol_f23{e} = disc.Dzz * (mesh.Jt(e))^2 * Z{e} * alpha_f1{e};
%                 end
            end 
        end
        
        function solveGlobal(s)
            for e=1:s.mesh.getNumGlobalElements
                element = s.mesh.getGlobalElement(e);
                if (element.getLv==2)
                    temp = superkron(eye(4),eye(5),diag([1 0.5 1 1 1 0.5]));
                    s.sol_f11{e} = element.getDx  * element.Z *  s.alphaGlobal( element.GM(:) );
                else
                    s.sol_f11{e} = element.getDx  * element.Z * s.alphaGlobal( element.GM(:) );
                end
%                 s.sol_f11{e} = element.getDx * element.Z * s.alphaGlobal( element.GM(:) );
                s.sol_f12{e} = element.getDy * element.Z * s.alphaGlobal( element.GM(:) );
                s.sol_f13{e} = element.getDz * element.Z * s.alphaGlobal( element.GM(:) );
                
                if (element.getLv==1)
                    temp = superkron(eye(4),eye(5),diag([1 0.5 1 1 1 0.5]));
                    s.sol_f21{e} = element.getDxx * element.Z * s.alphaGlobal( element.GM(:) );
                else
                    s.sol_f21{e} = element.getDxx * element.Z * s.alphaGlobal( element.GM(:) );
                end
                s.sol_f22{e} = element.getDyy * element.Z * s.alphaGlobal( element.GM(:) );
%                 if (C{4}=='C1') 
%                     s.sol_f23{e} = disc.Dzz * (mesh.Jt(e))^2 * Z{e} * alpha_f1{e};
%                 end
            end 
        end
        
        function plot(s)
            fig = figure(116);

            superTitle = s.getTitle;
            
            plotFunction(fig,1,s.mesh,s.f,'original function (t=0)',superTitle);
            plotFunction(fig,2,s.mesh,s.f,'original function (y=0)',superTitle);
            plotFunction(fig,3,s.mesh,s.f,'original function (x=0)',superTitle);
            plotFunction(fig,4,s.mesh,s.sol_f11,'Derivative x (t=0)',superTitle);
            plotFunction(fig,5,s.mesh,s.sol_f12,'Derivative y (t=0)',superTitle);
            plotFunction(fig,6,s.mesh,s.sol_f13,'Derivative t (y=0)',superTitle);
            plotFunction(fig,7,s.mesh,s.sol_f21,'Derivative xx (t=0)',superTitle);
            plotFunction(fig,8,s.mesh,s.sol_f22,'Derivative yy (t=0)',superTitle); 
        end
        
        function [superTitle] = getTitle(s)
            cString = cell(3,1);
            dString = cell(3,1);
            for i=1:3
                if ( (s.c(i)==1 && s.d(i)~=0) || s.c(i)==0)
                    cString{i} = ' ';
                else
                    cString{i} = num2str(s.c(i));
                end
                if (s.d(i)==1 || s.d(i)==0)
                    dString{i} = ' ';
                else
                    dString{i} = num2str(s.d(i));
                end
            end

            var ={'X','Y','Z'};
            
            superTitle = ['Function : '];
            for i=1:3
                if (s.c(i)==0) 
                    % change nothing
                else
                    if (s.d(i)==0)
                        superTitle = [superTitle ,cString{i}];
                    else
                        superTitle = [superTitle ,cString{i},' ' var{i} '^{',dString{i},'}'];
                    end
                end
                if i<3
                    superTitle = [superTitle ,' + '];
                end
            end
%                     ' + ',...
%                                         cString{2},' Y^{',dString{2},'} + ',...
%                                         cString{3},' T^{',dString{3},'}']; 
        end


    end
    
end

