classdef NSK_MM_Cart < Physics
    %JULIAN_NSK Implementation of the momentum and continuum equations for Navier-Stokes-Korteweg
    properties (Access=private)
        REI; WEI;
    end
    
    properties (Constant)
        % Variable ids for this physics
        R = 1;
        u = 2;
        v = 3;
        q = 4;
    end
    
    properties
        Re; We; k; cv; bf; % bf: body forces
    end
    
    methods
        function s = NSK_MM_Cart(mesh,settings)
            s = s@Physics(mesh);
            
            s.shortName = 'NSK_MM_Cart';
            s.longName  = 'NSK_MassMomentum_Cartesian';
            s.name      = 'NSK_MM_Cart';
            
            s.nEq       = 4;
            s.variables = {'R','u','v','q'};
            
            s.initialize(settings);
        end
        
        function initialize(s,settings)
            initialize@Physics(s,settings);

            s.steady    = settings.steady;
            
            s.Re        = System.settings.phys.Re;
            s.We        = System.settings.phys.We;
            s.k         = System.settings.phys.k;
            s.cv        = System.settings.phys.cv;
            s.bf        = System.settings.phys.adim_bodyf; % Dimensionless body forces

            s.REI       = 1.0/s.Re;
            s.WEI       = 1.0/s.We;
        end

        function time = setupEquations(s)
            % Function to setup the NSK equation. These equations can be 
            % found in the following article of Keunsoo (section 2):
            %   Thermal two-phase flow with a phase-field method
            %   Int. Journal of Multiphase Flow, 2019
            %
            t1 = toc;
            
            U   = FieldCollection.get('u');
            V   = FieldCollection.get('v');
            R   = FieldCollection.get('R');
            q   = FieldCollection.get('q');
            T   = FieldCollection.get('T');

            for e=s.mesh.eRange
                element = s.mesh.getElement(e);         % Select the active element
                el      = element.localID;              % Get the local element number
                
                % elemental operators
                matZ    = element.getMatZ;
                vecZ    = element.getVecZ;
                H       = element.getH;
                Dx      = element.getDx;
                Dy      = element.getDy;
                if (s.steady)
                    Dt       = 0;
                    dUdt_rhs = vecZ;
                    dVdt_rhs = vecZ;
                else
                    Dt          = s.getTime( element );
                    dRdt_rhs    = s.getTime( element, R );
                    dUdt_rhs    = s.getTime( element, U );
                    dVdt_rhs    = s.getTime( element, V );
                end
                Dxx     = element.getDxx;
                Dxy     = element.getDxy;
                Dyy     = element.getDyy;
                                
                % u-velocity derivatives
                Uk      = U.val2(e,s.mesh);
                Ux      = U.x(FieldType.Current,e);
                Uy      = U.y(FieldType.Current,e);
                Ut      = U.t(FieldType.Current,e);
                
                % v-velocity derivatives
                Vk      = V.val2(e,s.mesh);
                Vx      = V.x(FieldType.Current,e);
                Vy      = V.y(FieldType.Current,e);
                Vt      = V.t(FieldType.Current,e);
                
                % density derivatives
                Rk      = R.val2(e,s.mesh);
                Rx      = R.x(FieldType.Current,e);
                Ry      = R.y(FieldType.Current,e);
                
                % q derivatives (q is the Laplacian of the density R, so q = Rxx + Ryy)
                qx      = q.x(FieldType.Current,e);
                qy      = q.y(FieldType.Current,e);
                
                % T derivatives
                Tk      = T.val2(e,s.mesh);
                Tx      = T.x(FieldType.Current,e);
                Ty      = T.y(FieldType.Current,e);
                
                % Body forces
                b       = System.settings.phys.b;
                bx      = ones(size(vecZ)) * b(1);
                by      = ones(size(vecZ)) * b(2);

                % auxiliary terms
                convU   = Ut + Uk.*Ux + Vk.*Uy;
                convV   = Vt + Uk.*Vx + Vk.*Vy;
                divU    = Ux + Vy;
                Conv    = Dt + Uk.*Dx + Vk.*Dy; 
                Lap     = Dxx + Dyy;

                PR      = 24*Tk./(3-Rk).^2;
                PT      = 8*(Rk./(3-Rk)).^2;

                ConvRR = Conv + divU.*H;
                ConvRU = Rk.*Dx + Rx.*H;
                ConvRV = Rk.*Dy + Ry.*H;
                ConvUR = (PR - 6*Rk).*Dx +...
                    (convU - qx - s.bf*bx + 24*Tx./(3-Rk).^2 + 2*PR.*Rx./(3-Rk) - 6*Rx).*H;
                ConvUU = Rk.*Conv  - s.REI/3 * Dxx - s.REI * Lap + Rk.*Ux.*H;
                ConvUV = Rk.*Uy.*H - s.REI/3 * Dxy;
                ConvVR = (PR - 6*Rk).*Dy +...
                    (convV - qy - s.bf*by + 24*Ty./(3-Rk).^2 + 2*PR.*Ry./(3-Rk) - 6*Ry).*H;
                ConvVU = Rk.*Vx.*H - s.REI/3 * Dxy;
                ConvVV = Rk.*Conv  - s.REI/3 * Dyy - s.REI * Lap + Rk.*Vy.*H;

                grN = dRdt_rhs + Uk.*Rx + Rk.*Ux + Vk.*Ry + Rk.*Vy;
                guN = dUdt_rhs + Rk.*Ut + 2*(Rk.*Uk).*Ux + 2*(Rk.*Vk).*Uy...
                    - Rk.*qx + PT.*Tx + 2*PR.*Rk.*Rx./(3-Rk) - 6*Rk.*Rx;
                gvN = dVdt_rhs + Rk.*Vt + 2*(Rk.*Uk).*Vx + 2*(Rk.*Vk).*Vy...
                    - Rk.*qy + PT.*Ty + 2*PR.*Rk.*Ry./(3-Rk) - 6*Rk.*Ry;
            
                % Assign L and G
                    %            r         u          v           q
                s.opL{el} = [ ConvRR     ConvRU     ConvRV      matZ ; ...
                              ConvUR     ConvUU     ConvUV    -Rk.*Dx; ...
                              ConvVR     ConvVU     ConvVV    -Rk.*Dy; ...
                              s.WEI*Lap   matZ       matZ        -H  ] ;
                         
                s.opG{el} = [   grN    ; ...
                                guN    ; ...
                                gvN    ; ...
                                vecZ ] ;

                s.setWeightAndScale( element );
            end
            
            t2 = toc;
            time = t2-t1;
        end              

        % Default refinement criteria (false)
        function criteria = getRefinementCriteria(s,e)
            criteria(1) = false;
        end
 

        function out = addVaporLayer(s, rhoArray, x, y, ~, ~, b, amp)
            Rho1 = rhoArray(1);
            Rho2 = rhoArray(2);
            
            
            d  = -(-y + b + amp*cos(2*pi*x));
            
            out = (Rho1+Rho2)/2 + (Rho1-Rho2)/2 * tanh( d * sqrt(s.We)/2 );
        end
        
        function out = addDroplet(s,rhoArray,x,y,~,~,center,radius)
            Rho1 = rhoArray(1);
            Rho2 = rhoArray(2);
            
            r0 = radius;
            xc = center(1);
            yc = center(2);
            
            d  = r0 - sqrt((x-xc).^2+(y-yc).^2);        % positive inside the droplet
            
            out = (Rho1+Rho2)/2 + (Rho1-Rho2)/2 * tanh( d * sqrt(s.We)/2 );
        end
        
        function out = addDropletModifiable(s,rhoArray,x,y,~,~,center,radius,factor)
            Rho1 = rhoArray(1);
            Rho2 = rhoArray(2);
            
            r0 = radius;
            xc = center(1);
            yc = center(2);
            
            d  = r0 - sqrt((x-xc).^2+(y-yc).^2);        % positive inside the droplet
            
            out = Rho2 + ((Rho1-Rho2)/2) * (1 + tanh( d * sqrt(s.We) * factor ) );
        end
        
        function out = addSteepDroplet(s,rhoArray,x,y,~,~,center,radius)
            Rho1 = rhoArray(1);
            Rho2 = rhoArray(2);
            
            r0 = radius;
            xc = center(1);
            yc = center(2);
            
            d  = r0 - sqrt((x-xc).^2+(y-yc).^2);        % positive inside the droplet
            
            out = Rho2 + ((Rho1-Rho2)/2) * (1 + tanh( d * sqrt(s.We) * 100 ) );
        end
        
        function out = addDoubleDroplet(s,rhoArray,x,y,~,~,center,radius)
            %function w = Rho_begin(x,y,xc,yc,r0,Rho1,Rho2,We)
    
            % old Keunsoo code
            %out = (Rho1+Rho2)/2 - (Rho1-Rho2)/2*tanh((sqrt((x-xc).^2+(y-yc).^2)-r0)*sqrt(We)/2) ;
            %out = Rho2*ones(size(x)) ;
            
            Rho1 = rhoArray(1);
            Rho2 = rhoArray(2);
            
            r0 = radius;
            xc = center(1);
            yc = center(2);
            
            d  = r0 - sqrt((x-xc).^2+(y-yc).^2);        % positive inside the droplet
            d2 = r0 - sqrt((x + xc) .^ 2 + (y - yc) .^ 2);
            
            out = Rho2 ...
                + ((Rho1-Rho2)/2) * ( 2 ...
                                    + tanh( d * sqrt(s.We)/2 ) ...
                                    + tanh( d2 * sqrt(s.We)/2 ));
        end
        
        function out = addManyDroplet(s,rhoArray,x,y,center,radius)
            droplet_num = size(center, 1);
            arg = 0;
            for i = 1:droplet_num
                d = radius(1) - sqrt( (x - center(i, 1)) .^ 2 + (y - center(i, 2)) .^ 2);
                arg = arg + 1 + tanh(d * sqrt(s.We)/2);
            end % droplet_num
            arg = arg * ((rhoArray(1) - rhoArray(2))/2);
            out = rhoArray(2) + arg;
        end % function addManyDroplet

        function out = addHalfChannelBottom(s,rhoArray,x,y,~,~, pos)
            %function w = Rho_begin(x,y,xc,yc,r0,Rho1,Rho2,We)
    
            % old Keunsoo code
            %out = (Rho1+Rho2)/2 - (Rho1-Rho2)/2*tanh((sqrt((x-xc).^2+(y-yc).^2)-r0)*sqrt(We)/2) ;
            %out = Rho2*ones(size(x)) ;
            
            Rho1 = rhoArray(1);
            Rho2 = rhoArray(2);
            
            d  = pos - y;
            
            out = (Rho1+Rho2)/2 + (Rho1-Rho2)/2 * tanh( d * sqrt(s.We)/2 );
        end % function
        
        function out = addHalfChannelRight(s,rhoArray,x,y,~,~,pos)
            %function w = Rho_begin(x,y,xc,yc,r0,Rho1,Rho2,We)
    
            % old Keunsoo code
            %out = (Rho1+Rho2)/2 - (Rho1-Rho2)/2*tanh((sqrt((x-xc).^2+(y-yc).^2)-r0)*sqrt(We)/2) ;
            %out = Rho2*ones(size(x)) ;
            
            Rho1 = rhoArray(1);
            Rho2 = rhoArray(2);
            
            d  = x - pos;
            
            out = (Rho1+Rho2)/2 + (Rho1-Rho2)/2 * tanh( d * sqrt(s.We)/2 );
        end % function
    end
    methods (Static)
        function out = load(solution)
            if (~isa(solution,'Solution'))
                error('The first argument of load should be a Solution object')
            end
            
            out = EnergyEquation_NSK;
            out.loadObject(solution);
        end 
    end
    
end

