classdef Poisson < Physics
    %POISSON
    %   Solve the Poisson equation: u_xx + u_yy = f;
    
    properties
    end
    
    methods
        % Class constructor
        function s = Poisson(mesh,settings)
            s = s@Physics(mesh);
                        
            if nargin>0
                s.shortName = 'Ps';
                s.longName  = 'Poisson';
                s.name = 'Ps';

                s.nEq = 1;
                s.variables = {'u'};
                
                s.initialize(settings);
            end
        end
        
        function time = setupEquations(s,physics)
            t1 = toc;

            if (nargin==2)
                s.allPhysics = physics;
            end
                                    
            %s.Gi = zeros(s.mesh.numElements,1);
            
            u = FieldCollection.get('u');
            
            s.eqWeights = s.settings.eqWeights;
            
            for e=s.mesh.eRange
                
                % Select the active element
                element = s.mesh.getElement(e);
                
                % Get all elemental operators/arrays                
                if (s.mesh.sDim==1)
                    Dxx  = element.getDxx;
                    %Dt  = element.getDy;
                elseif (s.mesh.sDim==2)
                    H   = element.getH;
                    Dxx = element.getDxx;
                    Dyy = element.getDyy;
                end
                
                vecZ    = element.getVecZ;
                               
                % Get the local element number
                el = element.localID;
                
                [X,Y,Z,T] = element.getNodes();
                X = squeeze(X); Y = squeeze(Y);
                Z = squeeze(Z); T = squeeze(T);
                
                uxx = u.xx2( el );
                uyy = u.yy2( el );
                
                if (s.mesh.sDim==1)
                    s.opL{el} = Dxx;
                    s.opG{el} = vecZ + System.settings.phys.f(X,Y,Z,T);
                    
                   	if (s.withForcing)
                        s.opG{el} = s.opG{el} + s.forcing{el};
                    end
                    
                elseif (s.mesh.sDim==2)                    
                    s.opL{el} = (Dxx + Dyy + 0*H);
                    
                    withResidualCorrection = false;
                                        
                    if (withResidualCorrection)

                        if (el==1)
                            avRes = s.residualField{1}.value{1};
                            avRes(end,:) = avRes(end,:) - (s.residualField{1}.value{2}(1,:));
                            %avRes(:,end) = avRes(:,end) - (s.residualField{1}.value{3}(:,1)); 
                            avRes(1,1:end-1) = 2*avRes(1,1:end-1);
    %                         avRes(2,1) = 1*avRes(2,1);
                            resFix = avRes(:) - s.residualField{1}.value{1}(:);
                        elseif (el==2)
                            avRes = s.residualField{1}.value{2};
    %                         
                            %avRes(1,:) = avRes(1,:) - (s.residualField{1}.value{1}(end,:));
                            %avRes(:,end) = avRes(:,end) - (s.residualField{1}.value{4}(:,1));
    %                         avRes(end,1:end-1) = 2*avRes(end,1:end-1);
    %                         avRes(2:end-1, 1 ) = 2*avRes(2:end-1, 1 );
                            resFix = avRes(:) - s.residualField{1}.value{2}(:);
                        elseif (el==3)
                            avRes = s.residualField{1}.value{el};
%                             avRes(:,1) = avRes(:,1) - (s.residualField{1}.value{1}(:,end));  
                            %avRes(end,:) = avRes(end,:) - (s.residualField{1}.value{4}(1,:)); 
    %                         avRes(1,2:end) = 2*avRes(1,2:end);
                            %avRes(2:end-1,end) = 2*avRes(2:end-1,end);
                            resFix = avRes(:) - s.residualField{1}.value{el}(:);
                        elseif (el==4)
                            avRes = s.residualField{1}.value{el};
                            %avRes(:,1) = avRes(:,1) - (s.residualField{1}.value{2}(:,end));  
                            %avRes(1,:) = avRes(1,:) - (s.residualField{1}.value{3}(end,:));
    %                         avRes(end,2:end-1) = 2*avRes(end,2:end-1);
    %                         avRes(2:end,end) = 2*avRes(2:end,end);
                            resFix = avRes(:) - s.residualField{1}.value{el}(:);
                        else
                            resFix = vecZ;
                        end
                    else
                        resFix = 0;
                    end

                    s.opG{el} = vecZ + System.settings.phys.f(X,Y,Z,T) - resFix;
                    
                   	if (s.withForcing)
                        s.opG{el} = s.opG{el} + s.forcing{el};
                    end
                end
                
                %% Set quadrature weights, apply all scaling to L and G
                s.setWeightAndScale( element );
%                 
%                 %% AMR scaling
%                 Ze = s.mesh.Z{e};
%                 s.opL{el} = s.opL{el} * superkron(eye(s.Nvar),Ze);
%                 
%                 %% Boundary scaling
%                 bcW = element.getBoundaryQuadratureWeights;
%                 bcW = repmat(bcW(:),s.nEq,1);
%                 s.opL{el} = bcW(:) .* s.opL{el};
%                 s.opG{el} = bcW(:) .* s.opG{el};
%                 
%                 s.W{el}      = repmat(Wel,s.Nvar,1);
%                 s.sqrtW{el}  = repmat(sqrt(Wel),s.Nvar,1);
            end
            
            t2 = toc;
            time = t2-t1;
        end
        
%         function out = getConvergence(s,fieldType,residual)
%             if nargin==2
%                 switch fieldType
%                     case FieldType.Nonlinear
%                         out = reshape( System.settings.comp.nConvergence, [numel(System.settings.comp.nConvergence),1] );
%                     case FieldType.Coupled
%                         out = reshape( System.settings.comp.cConvergence, [numel(System.settings.comp.cConvergence),1] );
%                 end
%             else
%                 out = reshape( System.settings.comp.rConvergence, [numel(System.settings.comp.rConvergence),1] );
%             end
%         end
        
        function data = save(s)
            % Merge solution from all processors on the master rank
            
            data  = struct;
            
            % First synchronize the fields in parallel
            data.fields = cell(s.Nvar,1);
            for n=1:s.Nvar
                data.fields{n} = save( s.fields{n} );
            end
            
            if (NMPI.instance.rank==0 || System.settings.outp.parallelWrite )
                data.class        = class(s);
                data.name         = s.name;
                data.mesh         = s.mesh.save;
                               
                data.settings     = s.settings;
            end
        end
    end
    
    methods (Access=protected)
        function loadObject(s,solution)
            % load from parent
            loadObject@Poisson(s,solution)
            
            % specific loads for the Poisson object
            % none
        end
    end
    
    methods (Static)
        function out = load(s)
            out = Poisson();
            out.loadObject(s);
        end
    end
    
end

