classdef NSK_MM_Cyl < Physics
    %NSK_MM_CYL
    properties (Access=private)
        REI; WEI;
    end
    
    properties (Constant)
        % Variable ids for this physics
        R = 1;
        u = 2;
        v = 3;
        q = 4;
    end
    
    properties
        Re; We; k; cv; bf; % bf: body forces
    end
    
    methods
        function s = NSK_MM_Cyl(mesh,settings)
            s = s@Physics(mesh);
            
            s.shortName = 'NSK_MM_Cyl';
            s.longName  = 'NSK_MassMomentum_Cylindrical';
            s.name      = 'NSK_MM_Cyl';
            
            s.nEq       = 4;
            s.variables = {'R','u','v','q'};
            
            s.initialize(settings);
        end
        
        function initialize(s,settings)
            initialize@Physics(s,settings);

            s.steady    = settings.steady;
            
            s.Re        = System.settings.phys.Re;
            s.We        = System.settings.phys.We;
            s.k         = System.settings.phys.k;
            s.cv        = System.settings.phys.cv;
            s.bf        = System.settings.phys.adim_bodyf; % Dimensionless body forces

            s.REI       = 1.0/s.Re;
            s.WEI       = 1.0/s.We;
        end

        function time = setupEquations(s)
            % 
            t1 = toc;
            
            U   = FieldCollection.get('u');
            V   = FieldCollection.get('v');
            R   = FieldCollection.get('R');
            q   = FieldCollection.get('q');
            T   = FieldCollection.get('T');

            for e=s.mesh.eRange
                element = s.mesh.getElement(e);         % Select the active element
                el      = element.localID;              % Get the local element number
                
                % elemental operators
                matZ    = element.getMatZ;
                vecZ    = element.getVecZ;
                H       = element.getH;
                Dx      = element.getDx;
                Dy      = element.getDy;
                if (s.steady)
                    Dt       = 0;
                    dUdt_rhs = vecZ;
                    dVdt_rhs = vecZ;
                else
                    Dt          = s.getTime( element );
                    dRdt_rhs    = s.getTime( element, R );
                    dUdt_rhs    = s.getTime( element, U );
                    dVdt_rhs    = s.getTime( element, V );
                end
                Dxx     = element.getDxx;
                Dxy     = element.getDxy;
                Dyy     = element.getDyy;
                                
                % Coordinates
                [Xc, ~, ~, ~] = element.getNodes();
                Xc = Xc(:);
                Xc = 1./Xc;
                % Due to the symmetry boundary conditions at the center of
                % the domain (r=0), it is safe to equate the factor 1/r = 0,
                % even though the real value is infinite. This is because
                % all variables in the system that use the expression 1/r
                % are 0 when r = 0. This makes easier to work around the
                % singularity problem when r = 0.
                Xc(Xc == Inf) = 0;

                % u-velocity derivatives
                Uk      = U.val2(e,s.mesh);
                Ux      = U.x(FieldType.Current,e);
                Uy      = U.y(FieldType.Current,e);
                Ut      = U.t(FieldType.Current,e);
                
                % v-velocity derivatives
                Vk      = V.val2(e,s.mesh);
                Vx      = V.x(FieldType.Current,e);
                Vy      = V.y(FieldType.Current,e);
                Vt      = V.t(FieldType.Current,e);
                
                % density derivatives
                Rk      = R.val2(e,s.mesh);
                Rx      = R.x(FieldType.Current,e);
                Ry      = R.y(FieldType.Current,e);
                
                % q derivatives
                qx      = q.x(FieldType.Current,e);
                qy      = q.y(FieldType.Current,e);
                
                % T derivatives
                Tk      = T.val2(e,s.mesh);
                Tx      = T.x(FieldType.Current,e);
                Ty      = T.y(FieldType.Current,e);
                
                % Body forces
                b       = System.settings.phys.b;
                bx      = ones(size(vecZ)) * b(1);
                by      = ones(size(vecZ)) * b(2);

                % Auxiliary terms
                divU    = Ux + Xc .* Uk + Vy;

                
                % Pressure related terms
                PR      = 48*Tk./(3-Rk).^3;
                PT      = 8*(Rk./(3-Rk)).^2;


%=========================================================================================
                % Systems of equations |
                %-----------------------

                % Mass conservation equation
                L_MR  = Dt + divU.*H + Uk.*Dx + Vk.*Dy;
                L_MU  = Rk .* (Xc.*H + Dx) + Rx.*H;
                L_MV  = Rk.*Dy + Ry.*H;
                L_Mq  = matZ;

                % Momentum conservation equation in r direction
                L_PrR = (Ut + Uk.*Ux + Vk.*Uy + 24./(3-Rk).^2.*Tx...
                            + PR.*Rx - 6.*Rx - qx - s.bf*bx) .* H...
                        + (PR.*(3-Rk)/2 - 6.*Rk) .* Dx;
                L_PrU = (Rk.*Ux + s.REI.*Xc.*Xc) .* H + (Rk) .* Dt...
                        + (Rk.*Uk - 4/3*s.REI*Xc) .* Dx...
                        + (Rk.*Vk) .* Dy...
                        + (-4/3*s.REI) .* Dxx + (-4/3*s.REI) .* Dyy;
                L_PrV = (Rk.*Uy) .* H + (-1/3*s.REI) .* Dxy;
                L_Prq = (-Rk) .* Dx;

                % Momentum consevation equation in z direction
                L_PzR = (Vt + Uk.*Vx + Vk.*Vy + 24./(3-Rk).^2.*Ty...
                            + PR.*Ry - 6.*Ry - qy - s.bf*by) .* H...
                        + (PR.*(3-Rk)/2 - 6.*Rk) .* Dy;
                L_PzU = (Rk.*Vx) .* H + (-s.REI/3*Xc) .* Dy + (-s.REI/3) .* Dxy;
                L_PzV = (Rk.*Vy) .* H + (Rk) .* Dt...
                        + (Rk.*Uk - s.REI*Xc) .* Dx...
                        + (Rk.*Vk) .* Dy...
                        + (-s.REI) .* Dxx + (-s.REI*4/3) .* Dyy;
                L_Pzq = (-Rk) .* Dy;

                % Order reduction equation. Laplacian of density
                L_DRR = s.WEI*(Dxx + Xc.*Dx + Dyy);
                L_DRU = matZ;
                L_DRV = matZ;
                L_DRq = -H;
%=========================================================================================
                % Source Terms |
                %---------------

                G_M  = divU.*Rk + Uk.*Rx + Vk.*Ry;
                G_Pr = Rk.*Ut + 2*Rk.*Uk.*Ux + 2*Rk.*Vk.*Uy + PT.*Tx + PR.*Rx.*Rk...
                       - 6*Rk.*Rx - Rk.*qx;
                G_Pz = Rk.*Vt + 2*Rk.*Uk.*Vx + 2*Rk.*Vk.*Vy + PT.*Ty + PR.*Ry.*Rk...
                       - 6*Rk.*Ry - Rk.*qy;
                G_DR = vecZ;

%=========================================================================================
                % Assign L and G |
                %-----------------

                %             R      U      V      q
                s.opL{el} = [ L_MR,  L_MU,  L_MV,  L_Mq ; ...
                              L_PrR, L_PrU, L_PrV, L_Prq; ...
                              L_PzR, L_PzU, L_PzV, L_Pzq; ...
                              L_DRR, L_DRU, L_DRV, L_DRq ];

                s.opG{el} = [ G_M; ...
                              G_Pr;...
                              G_Pz;...
                              G_DR ];
%=========================================================================================

                % Set quadrature weights, apply all scaling to L and G
                s.setWeightAndScale( element );
            end
            
            t2 = toc;
            time = t2-t1;
        end              


        function out = addDroplet(s,rhoArray,x,y,~,~,center,radius)
            %function w = Rho_begin(x,y,xc,yc,r0,Rho1,Rho2,We)
    
            % old Keunsoo code
            %out = (Rho1+Rho2)/2 - (Rho1-Rho2)/2*tanh((sqrt((x-xc).^2+(y-yc).^2)-r0)*sqrt(We)/2) ;
            %out = Rho2*ones(size(x)) ;
            
            Rho1 = rhoArray(1);
            Rho2 = rhoArray(2);
            
            r0 = radius;
            xc = center(1);
            yc = center(2);
            
            d  = r0 - sqrt((x-xc).^2+(y-yc).^2);        % positive inside the droplet
            
            out = (Rho1+Rho2)/2 + (Rho1-Rho2)/2 * tanh( d * sqrt(s.We)/2 );
        end

        function out = addDropletModifiable(s,rhoArray,x,y,~,~,center,radius,factor)
            Rho1 = rhoArray(1);
            Rho2 = rhoArray(2);
            
            r0 = radius;
            xc = center(1);
            yc = center(2);
            
            d  = r0 - sqrt((x-xc).^2+(y-yc).^2);        % positive inside the droplet
            
            out = Rho2 + ((Rho1-Rho2)/2) * (1 + tanh( d * sqrt(s.We) * factor ) );
        end

        function criteria = getRefinementCriteria(s,e)
            criteria(1) = false;
        end
    end

    
    
    methods (Static)
        function out = load(solution)
            if (~isa(solution,'Solution'))
                error('The first argument of load should be a Solution object')
            end
            
            out = NSK_MM_Cyl;
            out.loadObject(solution);
        end 
    end
    
end

