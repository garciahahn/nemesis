classdef Kovasznay < NavierStokes
    %KOVASZNAY 2D Steady Navier-Stokes benchmark
    %   Detailed explanation goes here
    
    properties
        lambda;
    end
    
    methods
        function s = Kovasznay(mesh,settings)
            s = s@NavierStokes(mesh,settings);
            
            s.steady    = true;
            s.shortName = 'Ko';
            s.longName  = 'Kovasznay';
            s.name      = 'Kovasznay';
            
            %s.initialize(); %mesh,s.Re,0,0,1,1,maxNIter);
        end

%         function applyInitialConditions(s,bcType)
%             applyInitialConditions@NavierStokes(s,bcType);
%             fprintf('Kovasznay initial conditions applied\n')
%         end
        
        % Boundary conditions:
        %
        % Dirichlet on all walls, where the solution is obtained from the
        % analytical solution
        %
        function applyBoundaryConditions(s,bcType)
            
            fprintf('BC weight in minimization problem = %e\n',s.wbc)
            
            % Set the function handle which provides the analytical solution
            fh = @s.analyticalSolution;
            
            bcSet = 'Keunsoo';
            
            switch bcSet
                case 'Nektar'
                    % Velocity u:
                    s.applyDirichlet( Neighbor.Left    , Variable.u,  fh, bcType );     % fixedValue: uniform 0
                    s.applyDirichlet( Neighbor.Right   , Variable.u,  fh, bcType );     % fixedValue: uniform 0
                    s.applyNeumann( Neighbor.Bottom  , Variable.u,  0, bcType );     % fixedValue: uniform 0
                    s.applyNeumann( Neighbor.Top     , Variable.u,  0, bcType );     % function

                    % Velocity v:
                    s.applyDirichlet( Neighbor.Left    , Variable.v,  fh, bcType );     % fixedValue: uniform 0
                    s.applyDirichlet( Neighbor.Right   , Variable.v,  fh, bcType );     % fixedValue: uniform 0
                    s.applyDirichlet( Neighbor.Bottom  , Variable.v,  0, bcType );     % fixedValue: uniform 0
                    s.applyDirichlet( Neighbor.Top     , Variable.v,  0, bcType );     % fixedValue: uniform 0

                    % Pressure: p=0 on left/right (in-/outflow), dp/dn=0 on bottom/top
                    s.applyNeumann( Neighbor.Left    , Variable.p,  0, bcType );     % zeroGradient: dp/dx=0
                    s.applyDirichlet( Neighbor.Right   , Variable.p,  fh, bcType );     % zeroGradient: dp/dx=0
                    s.applyNeumann( Neighbor.Bottom  , Variable.p,  0, bcType );     % zeroGradient: dp/dy=0
                    s.applyNeumann( Neighbor.Top     , Variable.p,  0, bcType );     % zeroGradient: dp/dy=0
                    
                case 'Keunsoo'
                    % Velocity u:
                    s.applyDirichletField( Neighbor.Left    , s.uField,  fh, bcType );     % fixedValue: uniform 0
                    s.applyDirichletField( Neighbor.Right   , s.uField,  fh, bcType );     % fixedValue: uniform 0
                    s.applyDirichletField( Neighbor.Bottom  , s.uField,  fh, bcType );     % fixedValue: uniform 0
                    s.applyDirichletField( Neighbor.Top     , s.uField,  fh, bcType );     % function

                    % Velocity v:
                    s.applyDirichletField( Neighbor.Left    , s.vField,  fh, bcType );     % fixedValue: uniform 0
                    s.applyDirichletField( Neighbor.Right   , s.vField,  fh, bcType );     % fixedValue: uniform 0
                    s.applyDirichletField( Neighbor.Bottom  , s.vField,  fh, bcType );     % fixedValue: uniform 0
                    s.applyDirichletField( Neighbor.Top     , s.vField,  fh, bcType );     % fixedValue: uniform 0

                    % Pressure: p=0 on left/right (in-/outflow), dp/dn=0 on bottom/top
                    s.applyDirichletField( Neighbor.Left    , s.pField,  fh, bcType );     % zeroGradient: dp/dx=0
                    s.applyDirichletField( Neighbor.Right   , s.pField,  fh, bcType );     % zeroGradient: dp/dx=0
                    s.applyDirichletField( Neighbor.Bottom  , s.pField,  fh, bcType );     % zeroGradient: dp/dy=0
                    s.applyDirichletField( Neighbor.Top     , s.pField,  fh, bcType );     % zeroGradient: dp/dy=0
            
                    % s.applyDirichlet( 9, Variable.p,  fh, bcType, s.wbc, true );     % zeroGradient: p=0
                    
            end
            
            fprintf('Kovasznay boundary conditions applied\n')
        end

        
        function plot(s,fig,position)
            uField = s.uField; %s.getValues(Variable.u);
            vField = s.vField; %s.getValues(Variable.v);
            pField = s.pField; %s.getValues(Variable.p);
            
            if nargin==2
                position = 1;
            end
            
            u = cell(s.mesh.getNumLocalElements,1);
            v = cell(s.mesh.getNumLocalElements,1);
            p = cell(s.mesh.getNumLocalElements,1);
            
            if (position==0)
                for e=1:s.mesh.getNumLocalElements
                    u{e} = reshape(uField{e},s.mesh.getLocalElement(e).disc.Q);
                    u{e} = u{e}(:,:,1)';

                    v{e} = reshape(vField{e},s.mesh.getLocalElement(e).disc.Q);
                    v{e} = v{e}(:,:,1)';

                    p{e} = reshape(pField{e},s.mesh.getLocalElement(e).disc.Q);
                    p{e} = p{e}(:,:,1)';
                end
            else
                for e=1:s.mesh.getNumLocalElements
                    u{e} = reshape(uField{e},s.mesh.getLocalElement(e).disc.Q);
                    u{e} = u{e}(:,:,end)';

                    v{e} = reshape(vField{e},s.mesh.getLocalElement(e).disc.Q);
                    v{e} = v{e}(:,:,end)';

                    p{e} = reshape(pField{e},s.mesh.getLocalElement(e).disc.Q);
                    p{e} = p{e}(:,:,end)';
                end
            end
            
            s.plotAll(fig,u,v,p);
            s.plotReference();
            
            drawnow;
        end
        
        function plotAnalyticalSolution(s,fig)
            for e=1:s.mesh.getNumLocalElements
                element = s.mesh.getLocalElement(e);
                u{e} = s.analyticalSolution(Variable.u,element.xelem,element.yelem)';
                v{e} = s.analyticalSolution(Variable.v,element.xelem,element.yelem)';
                p{e} = s.analyticalSolution(Variable.p,element.xelem,element.yelem)';
            end
            
            s.plotAll(fig,u,v,p);
        end
           
        function plotAll(s,fig,u,v,p)
            screensize = get( groot, 'Screensize' );
            sizex = 625;
            sizey = 1000;
            set(fig,'position',[0.5*(screensize(3)-sizex) 0.5*(screensize(4)-sizey) sizex sizey]); 
            
            % U-plot
            delete(subplot(3,2,1))
            subplot(3,2,1)
            for e=1:s.mesh.getNumLocalElements
                element = s.mesh.getLocalElement(e);
                surf(element.xelem,element.yelem,u{e});
                hold on;
            end
            view([0,90])
            
            % V-plot
            delete(subplot(3,2,3))
            subplot(3,2,3)
            for e=1:s.mesh.getNumLocalElements
                element = s.mesh.getLocalElement(e);
                surf(element.xelem,element.yelem,v{e});
                hold on;
            end
            view([0,90])
            
            % P-plot
            delete(subplot(3,2,5))
            subplot(3,2,5)
            for e=1:s.mesh.getNumLocalElements
                element = s.mesh.getLocalElement(e);
                surf(element.xelem,element.yelem,p{e});
                hold on;
            end
            view([0,90])
            
            % U-velocity at x=0.5
            delete(subplot(3,2,2))
            subplot(3,2,2)
            for e=1:s.mesh.getNumLocalElements
                element = s.mesh.getLocalElement(e);
                z=interp2(element.xelem,element.yelem,u{e},0.5,element.yelem);
                plot(element.yelem,z,'k');
                hold on;
            end
            view([90,-90])
            
            % V-velocity at x=0.5
            delete(subplot(3,2,4))
            subplot(3,2,4);
            for e=1:s.mesh.getNumLocalElements
                element = s.mesh.getLocalElement(e);
                z=interp2(element.xelem,element.yelem,v{e},0.5,element.yelem);
                plot(element.yelem,z,'k');
                hold on;
            end
            view([90,-90]) 
            
            % Velocity contours
            delete(subplot(3,2,6))
            subplot(3,2,6)
            for e=1:s.mesh.getNumLocalElements
                element = s.mesh.getLocalElement(e);
                z=interp2(element.xelem,element.yelem,p{e},0.5,element.yelem);
                plot(element.yelem,z,'k');
%                 contourf(element.xelem,element.yelem,hypot(u{e},v{e}),[0:0.25:10]);
                hold on;
            end
            view([0,90])
        end
        
        function plotReference(s)
            for e=1:s.mesh.getNumLocalElements
                element = s.mesh.getLocalElement(e);
                u{e} = s.analyticalSolution(Variable.u,element.xelem,element.yelem)';
                v{e} = s.analyticalSolution(Variable.v,element.xelem,element.yelem)';
                p{e} = s.analyticalSolution(Variable.p,element.xelem,element.yelem)';
            end
            
            subplot(3,2,2);
            for e=1:s.mesh.getNumLocalElements
                element = s.mesh.getLocalElement(e);
                z=interp2(element.xelem,element.yelem,u{e},0.5,element.yelem);
                plot(element.yelem,z,'r');
                hold on;
            end
            view([90,-90]) 
            
            subplot(3,2,4);
            for e=1:s.mesh.getNumLocalElements
                element = s.mesh.getLocalElement(e);
                z=interp2(element.xelem,element.yelem,v{e},0.5,element.yelem);
                plot(element.yelem,z,'r');
                hold on;
            end
            view([90,-90])
            
            subplot(3,2,6);
            for e=1:s.mesh.getNumLocalElements
                element = s.mesh.getLocalElement(e);
                z=interp2(element.xelem,element.yelem,p{e},0.5,element.yelem);
                plot(element.yelem,z,'r');
                hold on;
            end
            view([90,-90]) 
        end
        
        function out = analyticalSolution(s,variable,x,y,z)
            switch variable
                case Variable.u
                    out = 1 - exp(s.lambda.*x) .* cos(2*pi*y');
                case Variable.v
                    out = s.lambda/(2*pi) * exp(s.lambda.*x) .* sin(2*pi*y');
                case Variable.p
                    out = s.P0 - 1/2 * exp(2*s.lambda.*x) .* exp(0*y');
            end
            if ~exist('z','var') || isempty(z)
                z=1;
            end
            out = repmat(out,1,1,length(z));
        end
    end
    
    methods (Access=private)

    end
    
    methods (Static)
        function s = run()
            s = Kovasznay(mesh,Re,n);
        end
    end
end

