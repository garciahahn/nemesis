classdef CahnHilliard2D_3eq < CahnHilliard
    %CahnHilliard Implementation of the 2D Cahn-Hilliard equation
    %   A 3rd equation is solved for kappa
    
    properties
        % no extra properties required
    end
    
    methods
        
        function s = CahnHilliard2D_3eq(mesh,settings)            
            s = s@CahnHilliard(mesh,settings);
            
            s.shortName = 'CH';
            s.longName  = 'CahnHilliard';
            s.name      = 'CH';
            s.Nvar      = 3;
            s.nEq       = 3;
            s.variables = {'c','omg','kap'};
            
            s.initialize(settings);       
        end
        
        function initialize(s,settings)
            initialize@CahnHilliard(s,settings);
        end

        function time = setupEquations(s)
            t1 = toc;
            
            U   = FieldCollection.get('u');
            V   = FieldCollection.get('v');
            P   = FieldCollection.get('p');
            C   = FieldCollection.get('c');
            Omg = FieldCollection.get('omg');
            kappaField = FieldCollection.get('kap');
            
            if (~System.settings.comp.coupling.enabled || ~s.coupled)
                U = NaN;
                V = NaN;
                Uk = 0;
                Vk = 0;
            end
            
            % Droplet/bubble center of gravity velocity at temporal quadrature points
            if (System.settings.phys.dynamicReferenceFrame && s.coupled && System.settings.comp.coupling.enabled)
                s.computeCenterOfMassVelocity(true,false);       % determine the center-of-mass velocity: compute the enclosed mass, and without overIntegration
                uc = s.cmv_total(:,1);% - System.settings.phys.bubbleVelocity(1);
                vc = s.cmv_total(:,2);% - System.settings.phys.bubbleVelocity(2);
                %fprintf('WARNING : USING DYNAMIC REFERENCE FRAME')
            end

            %kappaGamma = s.computeKappaGamma();
            
            %% CH) Setup operators
            for e=s.mesh.eRange
                
                % Set active element
                element = s.mesh.getElement(e);
                el      = element.localID;          % e is the global id
                
                %% Get current field values
                Ck          = C.val(FieldType.Current,e);
                dCkdx       = C.x(FieldType.Current,e);
                dCkdy       = C.y(FieldType.Current,e);
                dCkdxx      = C.xx(FieldType.Current,e);
                dCkdyy      = C.yy(FieldType.Current,e);
                dOmgkdx     = Omg.x(FieldType.Current,e);
                dOmgkdy     = Omg.y(FieldType.Current,e);
                dOmgkdxx    = Omg.xx(FieldType.Current,e);
                dOmgkdyy    = Omg.yy(FieldType.Current,e);
                
                kappax      = kappaField.x(FieldType.Current,e);
                kappay      = kappaField.y(FieldType.Current,e);
                %kappaxx     = kappaField.xx(FieldType.Current,e);
                %kappaxy     = kappaField.xy(FieldType.Current,e);
                %kappayy     = kappaField.yy(FieldType.Current,e);

                gradC2 = dCkdx.^2 + dCkdy.^2;
                
                
                if (isempty(dOmgkdxx))
                    dCkdxx   = 0;
                    dCkdyy   = 0;
                    dOmgkdxx = 0;
                    dOmgkdyy = 0;
                end
                
                if ( U==U )
                    Uk = U.val2(e,s.mesh);
                    ux = U.x2(e,s.mesh);
                else
                    Uk = 0;
                    ux = 0;
                end
                
                if ( V==V )
                    Vk = V.val2(e,s.mesh);
                    vy = V.y2(e,s.mesh);
                else
                    Vk = 0;
                    vy = 0;
                end
                
                if ( P==P )
                    %Pk = P.val2(e,s.mesh);
                    px = P.x2(e,s.mesh);
                    py = P.y2(e,s.mesh);
                else
                    %Pk = 0;
                    px = 0;
                    py = 0;
                end
                
                % Moving reference frame: adjust the velocities
                if (System.settings.phys.dynamicReferenceFrame && s.coupled && System.settings.comp.coupling.enabled)
                    %fprintf('WARNING : USING DYNAMIC REFERENCE FRAME')
                    Uk = Uk(:) - uc; %System.settings.phys.bubbleVelocity(1);
                    Vk = Vk(:) - vc; %System.settings.phys.bubbleVelocity(2);        
                end
                
                %% Elemental operators
                H   = element.getH;
                Dx  = element.getDx;
                Dy  = element.getDy;
                                
                if (s.steady)                    
                    Dt       = 0;
                    dCdt_rhs = 0;
                else
                    Dt       = s.getTime( element );
                    dCdt_rhs = s.getTime( element, C );
                end
                
                Dxx     = element.getDxx;
                Dyy     = element.getDyy;
                Dxy     = element.getDxy;
                matZ    = element.getMatZ;
                vecZ    = element.getVecZ;
                
                Nabla2  = Dxx + Dyy;                                                            % Laplacian
                Dnn    = (dCkdx.^2 .* Dxx + 2*dCkdx.*dCkdy.*Dxy + dCkdy.^2.*Dyy)./ gradC2;      % normal directional second derivative
                Dtt    = (dCkdy.^2 .* Dxx - 2*dCkdx.*dCkdy.*Dxy + dCkdx.^2.*Dyy)./ gradC2;      % tangential directional second derivative
                
%                 if (~isempty(kappaField))
%                     kappatt    = (dCkdy.^2 .* kappaxx - 2*dCkdx.*dCkdy.*kappaxy + dCkdx.^2.*kappayy)./ gradC2;      % tangential directional second derivative for kappa
%                 end

                %% Compute parts of equations
                Conv        = (Uk.*Dx + Vk.*Dy); % + 0*(Ukx + Vky).*H);         % convection
                Grav        = 0; %(g(1)*time.*Dx + g(2)*time.*Dy);              % gravity
                sigmaConv   = 0; %0.1 * ( dCkdy.*Dx - dCkdx.*Dy ) ./ sqrt(gradC2);   % curvature 
                            % + 0*Dxy;

                % The mobility can be used to ensure conservation of
                % enclosed mass (option 3). The standard non  degenerate 
                % mobility is obtained with option 1.
                switch s.mobilityType
                    case 1
                        % By default mobilityType == 1 is selected
                        Mobil = 1;
                        auxi = 0;
                    case 2
                        % Mobility type as used by Keunsoo in one of his articles
                        Mobil = (Ck - Ck.^2);
                        auxi  = ( 1 - 2*Ck );
                    case 3
                        % Enclosed mass conerving mobility, it must be a function of (C-0.5)
                        Mobil = abs(Ck - 0.5);
                        auxi  = (Ck - 0.5) / Mobil;              % derivative of Mobil
                    otherwise 
                        error('unsupported mobility type (use a value in the range 1-3)')
                end
                
                theta   = 0.5;  % controls balance between Mphi and Momg
                Mphi    = -(s.PEI) * ( auxi * (  theta) .* ( dOmgkdx.*Dx + dOmgkdy.*Dy ) );
                Momg    = -(s.PEI) * ( auxi * (1-theta) .* ( dCkdx  .*Dx + dCkdy  .*Dy ) );

                if (s.explicitDivergence)
                    divU = (ux + vy) .* H;
                else
                    divU = 0;
                end

                S0 = 1;
                S1 = 0;
                S2 = 0;

                G1 = 0; %s.Cn_L^2 * kappatt;

                L11 = Dt + Conv + sigmaConv + 0*Grav + Mphi + divU - S1 * (Dxx + Dyy); % - 1/s.We * s.alpha * sqrt(normOmg1) .* H;
                L12 = Momg;
                L13 = matZ;
                
                % Second equation: relation between C and omega
                L21 = 1/s.Cn_L * (3*Ck.^2 - 3*Ck + 0.5).*H - s.Cn_L * (Dnn);
                L22 = -H;
                L23 = matZ;
                
                % Third equation: relation between C and kappa (kappa = -eps * d^2 C / dt^2)
                L31 = s.Cn_L * (Dtt);                                       % eps * d^2 C / dt^2; alternative: s.Cn_L * (Dxx + Dyy - Dnn)
                L32 = matZ;                                                 % no omega dependence
                L33 = H;                                                    % kappa
                G3  = vecZ + 0*s.Cn_L * sqrt(px.^2 + py.^2);                % zero rhs
                
%                 L31 = matZ;
%                 L32 = matZ;
%                 L33 = H;
%                 G3  = s.Cn_L * kappaGamma{e};

                % Classic CH
                switch s.pfMethod
                    case pfMethod.classicCH
                        L12 = L12 - s.PEI * Mobil .* (Dxx + Dyy);           % diffusion of omega
                        L13 = L13 - s.PEI * Mobil .* (Dxx + Dyy);           % diffusion of kappa
                        
                        L21 = 1/s.Cn_L * (3*Ck.^2 - 3*Ck + 0.5).*H - s.Cn_L * (Dnn);

                    case pfMethod.classicAC
                        L12 = L12 - 1/s.We * s.alpha .* Mobil .* H;
                        L21 = 1/s.Cn_L * (3*Ck.^2 - 3*Ck + 0.5).*H - s.Cn_L * (Dxx + Dyy);

                    case pfMethod.balancedCH    % with directional second derivative
                        %L12 = L12 - s.PEI * Mobil .* ( (dCkdx.^2 .* Dxx + 2*dCkdx.*dCkdy.*Dxy + dCkdy.^2.*Dyy)./ normC);     % directional second derivative
                        %L12 = L12 - s.PEI * Mobil .* (Dxx + Dyy);                                                             % Laplacian for potential diffusion
                        
                        L21 = 1/s.Cn_L * (3*Ck.^2 - 3*Ck + 0.5).*H - s.Cn_L * Dnn;
                        L12 = L12 - s.PEI * Mobil .* (Dxx+Dyy);
                        L13 = L13 - 0*s.PEI * Mobil .* (Dtt);

                    case pfMethod.balancedAC    % with directional second derivative
                        L12 = 1/s.We * s.alpha .* H;
                        L21 = 1/s.Cn_L * (3*Ck.^2 - 3*Ck + 0.5).*H - s.Cn_L * Dnn;

                    case pfMethod.balancedACH 	% with directional second derivative
% %                             L12 = 100/s.We .* (Ck .* (1-Ck)) .* H;
% %                             L12 = 500* s.Cn_L .* (abs(Ck-0.5)).^(1/2) .* H;
%                         %L12 = 500* s.Cn_L .* (abs(Ck-0.5)) .* H;
% 
%                         L11 = L11 + s.Cn_L * (kappax .* Dy - kappay .* Dx) .* (dCkdy-dCkdx);
% 
%                         advTerm = sign(Ck-0.5) .* (Ck.*(1-Ck)-0.25);
%                         L12 = 50/s.Cn_L * advTerm .* H;
% 
%                         %L12 = (0*s.Cn_L .* (normC).^(1/2) + 500* s.Cn_L .* (abs(Ck-0.5)).^(1/2) ).* H;
% 
%                         %x = abs(Ck-0.5);
%                         %L12 = 500* s.Cn_L .* ( x-1/3*x.^3 ) .* H;
%                         %L12 = L12 - 1/s.Pe * (Dxx + Dyy);
%                         L12 = L12 - s.PEI * Dnn;
%                         L21 = 1/s.Cn_L * (3*Ck.^2 - 3*Ck + 0.5).*H - s.Cn_L * Dnn;

                        Mobil3 = 1; %abs(Ck-0.5);
                        L12 = s.PEI .* s.Cn_L .* Mobil3 .* H - s.PEI * Mobil .* s.Cn_L.^3 .* (Dxx + Dyy);
                        %L12 = L12 + s.PEI * s.Cn_L .* H;
                        
                        L21 = 1/s.Cn_L * (3*Ck.^2 - 3*Ck + 0.5).*H - s.Cn_L * Dnn;
                        
                    case pfMethod.classicACH
                        L12 = 1/s.We * s.alpha .* H - s.PEI * (Dxx + Dyy);
                        L21 = 1/s.Cn_L * (3*Ck.^2 - 3*Ck + 0.5).*H - s.Cn_L * (Dxx + Dyy);

                    case pfMethod.meanKwakkel
                        advTerm = sign(Ck-0.5) .* (Ck.*(1-Ck)-0.25);

                        %L11 = L11 + 0.01 * ( kappax .* Dy - kappay .* Dx ); % .* advTerm;

                        %G1 = -0.1 * advTerm .* 2 .* s.H{el}(:);

                        L12 = 50/s.Cn_L * advTerm .* H;

                        L12 = L12 - s.PEI * Dnn;
                        L21 = 1/s.Cn_L * (3*Ck.^2 - 3*Ck + 0.5).*H - s.Cn_L * Dnn;

                    case pfMethod.kappa
                        S0 = 0;
                        advTerm = sign(Ck-0.5);


                        L11 = L11./ gradC2 + 50/s.Cn_L * advTerm ./ gradC2 .* (1/s.Cn_L * (3*Ck.^2 - 3*Ck + 0.5).*H );
                        G1  = G1  + 50/s.Cn_L * advTerm ./ gradC2 .* (1/s.Cn_L * (2*Ck.^3 - 1.5*Ck.^2)     );
                        L12 =     - 50/s.Cn_L * advTerm .* s.Cn_L ./ gradC2 ./ gradC2 .* H;

                        %L12 = L12 - s.PEI * ( (dCkdx.^2 .* Dxx + 2*dCkdx.*dCkdy.*Dxy + dCkdy.^2.*Dyy)./ normC);

                        L21 = ( (dCkdx.^2 .* Dxx + 2*dCkdx.*dCkdy.*Dxy + dCkdy.^2.*Dyy)); %./ normC);
                end

                
                    
                relaxation = 1.0;

                L12 = relaxation * L12;
                G1  = G1 + dCdt_rhs + vecZ + (1-relaxation) * ( s.PEI * (dOmgkdxx + dOmgkdyy) ) - S1 * (dCkdxx+dCkdyy);

                % without correction:
                G2  = S0 * 1/s.Cn_L * (2*Ck.^3 - 1.5*Ck.^2) - S2 * (dCkdxx+dCkdyy); % .* sqrt(gradC2);

                % Dynamic bc
                if (System.settings.phys.dynamicBC==2)
                        
                    if (isempty(element.neighbor{1,2}) ) %|| isempty(element.neighbor{2,1})) % NB: {1,1} is the left neighbor, {2,1} is the right neighbor
%                         if (isempty(element.neighbor{1,2}))
                            qLocations = (element.finite.logicalCoordinates(2,:)==-1);  % bottom quadrature points
%                         elseif (isempty(element.neighbor{2,1}))
%                             qLocations = (element.finite.logicalCoordinates(1,:)== 1);
%                         end

                        nx = -dCkdx ./ sqrt(gradC2);
                        ny = -dCkdy ./ sqrt(gradC2);

                        ratio = ny./nx;

                        theta = pi/2 + abs(atan(-ratio(qLocations))); %/pi*180

                        nx = nx(qLocations);
                        ny = ny(qLocations);

                        ddW = 2 * sqrt(2*pi) * ( theta .* cos(2*theta) - sin(2*theta) + theta ) .* sqrt(2*theta-sin(2*theta)) ./ (pi * (sin(2*theta)-2*theta).^2 );
% %                          
                        L11( qLocations, : ) = L11( qLocations, : ) + 1 * ddW .* ( sign(atan(-ratio(qLocations))) .* Dx( qLocations, : ) - Dy( qLocations, : ) ); % + Dy( qLocations, : ));
%                         L11( qLocations, : ) = L11( qLocations, : ) + 1 * ddW .* ( Dx( qLocations, : ) - 0*Dy( qLocations, : ) ); % + Dy( qLocations, : ));

                        % NB: ny * Dx = nx * Dy by definition! The
                        % tangential gradient is zero
                        %


%                         L12( qLocations, : ) = L12( qLocations, : ) - 0/s.Pe * Dx( qLocations, : );
%                         L21( qLocations, : ) = L21( qLocations, : ) - s.Cn_L * Dx( qLocations, : );        % adds eps * grad_n(C) * cos(theta) to L21 (omega equation, C variable) at the left wall
                    end
                end
                
                %L13 = - 2*abs(Ck-0.5) .* (Dxx + Dyy);
                
%                 type = 1;           % 0: balanced, 1: classic
                
                %L13 = -type * (Dxx + Dyy);
                %L13 = L13 - s.PEI/s.Cn_L * type * (Dtt);

                % with correction:
                %G2  = -1 + 1/s.Cn_L * (2*Ck.^3 - 1.5*Ck.^2) .* (1-1./factor);
    
                %% Setup L and G
                s.opL{el} = [ L11 L12 L13  ; ...
                              L21 L22 L23  ; ...
                              L31 L32 L33 ];

                s.opG{el} = [ G1  ; ...
                              G2  ; ...
                              G3 ];
                          
                if (s.withForcing)
                    s.opG{el} = s.opG{el} - s.forcing{el};
                end
                
                %% Set quadrature weights, apply all scaling to L and G
                s.setWeightAndScale( element );
            end 
            
            t2 = toc;
            time = t2-t1;
        end

        % This function computes the additional explicit corrections for the
        % phase-field method. For each equation a cell array with
        % correction values is created: pfCorrection1 for equation 1, and
        % pfCorrection2 for equation 2.
        %
        % The options for the correction are:
        %   0   : no correction
        %   1   : mean curvature correction
        %   2   : profile correction
        %   3   : flux correction
        %
        function computePhaseFieldCorrection(s,method)
            
            % Use the correctionMethod provided through settings if none is
            % passed to this function
            if nargin==1
                method = s.correctionMethod;
            end
            
            % Create cell arrays for correction values for each equation
            s.pfCorrection1 = cell( numel(s.mesh.eRange),1 );
            s.pfCorrection2 = cell( numel(s.mesh.eRange),1 );
            
            switch method
                
                % no correction
                case 0  
                    for e=s.mesh.eRange
                        el = s.mesh.getElement(e).localID;
                        s.pfCorrection1{el} = 0;
                        s.pfCorrection2{el} = 0;
                    end
                    
                % mean curvature
                case 1
                    Cf = FieldCollection.get('c');
                    Ff = FieldCollection.get('omg');
                    
                    s.computeH2(s.mesh,1);
                    for e=s.mesh.eRange                        
                        element = s.mesh.getElement(e);
                        el = element.localID;

                        F     = Ff.val(FieldType.Current,e);
                        Fx    = Ff.x(FieldType.Current,e);
                        Fy    = Ff.y(FieldType.Current,e);
                        Cx    = Cf.x(FieldType.Current,e);
                        Cy    = Cf.y(FieldType.Current,e);
                        
                        %gradFC = Fx.*Cx + Fy.* Cy;
                        gradC  = sqrt( Cx.^2 + Cy.^2 );
                        %factor = gradFC ./ gradC;
                        factor = gradC;
                    
                        s.pfCorrection1{el} = element.finite.vecZ;
                        s.pfCorrection2{el} = s.Cn_L^2 * ( 2 * s.H{el}(:) .* factor );
                        %s.pfCorrection2{el} = s.Cn/s.Cn_L * (s.alpha/s.Cn * s.Cn_L^2 * 2 * s.H{el}(:) .* gradC + F).* gradC;
                    end
                    
                % profile correction
                %   A penalty flux is introduced to force the interface
                %   profile towards its equilibrium
                case 2 
                    alpha  = 0.025;
                    lambda = alpha/s.Cn_L;
                    Cf     = FieldCollection.get('c');
                    %Ff     = FieldCollection.get('omg');
                    
                    s.computeH2(s.mesh,1);
                    
                    for e=s.mesh.eRange
                        element = s.mesh.getElement(e);
                        
                        el = element.localID;
                        
                        C   = Cf.val(FieldType.Current,e);
                        Cx  = Cf.x(FieldType.Current,e);
                        Cy  = Cf.y(FieldType.Current,e);
                        Cxx = Cf.xx(FieldType.Current,e);
                        Cyy = Cf.yy(FieldType.Current,e);
                        
                        normC = sqrt( Cx.^2+Cy.^2 );
                                       
                        divTerm = (C-0.5).*C.*(C-1)/(s.Cn_L^2) - normC .* 2 .* s.H{el}(:);
                        
                        s.pfCorrection1{el} = -(lambda/s.Pe) * ( (Cxx+Cyy) - divTerm );
                        s.pfCorrection2{el} = element.finite.vecZ;
                        
                        % reshape 
                        %s.pfCorrection1{el} = reshape( s.pfCorrection1{el}, element.finite.qSize );
                        %s.pfCorrection2{el} = reshape( s.pfCorrection2{el}, element.finite.qSize );
                    end
                    
                case 3
                    % This correction computes the projection of the
                    % chemical potential gradient onto the normal to the
                    % interface as proposed by Zhang et al. (2017).
                    % The chemical potential is denoted by F
                    
                    Cf  = FieldCollection.get('c');
                    Ff  = FieldCollection.get('omg');
                    
                    s.computeH2(s.mesh,1);
                    
                    for e=s.mesh.eRange
                        element = s.mesh.getElement(e);
                        
                        el = element.localID;
                        
                        C   = Cf.val(FieldType.Current,e);
                        Cx  = Cf.x(FieldType.Current,e);
                        Cy  = Cf.y(FieldType.Current,e);
                        Cxx = Cf.xx(FieldType.Current,e);
                        Cyy = Cf.yy(FieldType.Current,e);
                        Cxy = Cf.xy(FieldType.Current,e);
                        
                        %F   = Ff.val(FieldType.Current,e);
                        Fx  = Ff.x(FieldType.Current,e);
                        Fy  = Ff.y(FieldType.Current,e);
                        Fxx = Ff.xx(FieldType.Current,e);
                        Fyy = Ff.yy(FieldType.Current,e);
                        Fxy = Ff.xy(FieldType.Current,e);
                        
                        normC = sqrt( Cx.^2 + Cy.^2 );
                        
                        %term1 = Cx.^2 .* Fxx + 2*Cx.*Cy.*Fxy + Cy.^2 .* Fyy;
                        term1 = 0*Cx.^2 .* Fxx + 0*2*Cx.*Cy.*Fxy + 0*Cy.^2 .* Fyy;
                        term2 = (Cx.*Fx + Cy.*Fy) .* ( Cxx + Cyy );
                        term3 = Cx.*Fx .* Cxx + ( Cx.*Fy + Fx.*Cy ).*Cxy + Cy.*Fy .* Cyy;
                        term4 = 2 * (Cx.*Fx + Cy.*Fy) .* ( Cx.^2 .* Cxx + 2*Cx.*Cy.*Cxy + Cy.^2 .* Cyy );
                                                
                        s.pfCorrection1{el} = -(1/s.Pe) * ( ( normC.^2 .* (term1 + term2 + term3) - term4 ) ./ normC.^4 - 0*( Fxx+Fyy ) );
                        s.pfCorrection1{el}( abs(normC.^4)<1e-8 ) = 0;
                        % NB: the term above is equal to ( Fxx+Fyy ) at
                        % equilibrium, so it cancels out the Laplacian of F
                        s.pfCorrection2{el} = element.finite.vecZ;
                        
                        % reshape 
                        %s.pfCorrection1{el} = reshape( s.pfCorrection1{el}, element.finite.qSize );
                        %s.pfCorrection2{el} = reshape( s.pfCorrection2{el}, element.finite.qSize );
                    end
                    
                % profile correction (personal method)
                %   A penalty flux is introduced to force the interface
                %   profile towards its equilibrium
                case 4
                    alpha  = 0.025;
                    lambda = alpha/s.Cn_L;
                    %lambda = 1;
                    Cf     = FieldCollection.get('c');
                    %Ff     = FieldCollection.get('omg');
                    
                    s.computeH2(s.mesh,1);
                    
                    for e=s.mesh.eRange
                        element = s.mesh.getElement(e);
                        
                        el = element.localID;
                        
                        C   = Cf.val(FieldType.Current,e);
                        Cx  = Cf.x(FieldType.Current,e);
                        Cy  = Cf.y(FieldType.Current,e);
                        Cxx = Cf.xx(FieldType.Current,e);
                        Cyy = Cf.yy(FieldType.Current,e);
                        Cxy = Cf.xy(FieldType.Current,e);
                        
                        normC = sqrt( Cx.^2+Cy.^2 );
                        cutoff = (1e-3)^2 / ( sqrt(2) * s.Cn_L );
                        
                        dd_direc = ( Cx.^2.*Cxx + 2*Cx.*Cy.*Cxy + Cy.^2.*Cyy ) ./ ( Cx.^2+Cy.^2 );
                                                
%                         term = 2*(C-0.5) .* normC + C.*(1-C)./normC .* dd_direc;
%                         %term = term .* normC;
%                         term( normC<cutoff ) = 0;
                        
                        term = 1/s.Cn_L^2 * (C-0.5).* C.*(1-C) + dd_direc;
                        
                        s.pfCorrection1{el} = - (lambda/s.Pe) * term;
                        s.pfCorrection2{el} = element.finite.vecZ;
                        
                        % reshape 
                        %s.pfCorrection1{el} = reshape( s.pfCorrection1{el}, element.finite.qSize );
                        %s.pfCorrection2{el} = reshape( s.pfCorrection2{el}, element.finite.qSize );
                    end
            end
        end
                
        function plotPhaseFieldCorrection(s,method)
            
            % Use the correctionMethod provided through settings if none is
            % passed to this function
            if nargin==1
                method = s.correctionMethod;
            end
      
            s.computePhaseFieldCorrection(method);
                        
            pbx = s.mesh.X(2,1) - s.mesh.X(1,1);
            pby = s.mesh.X(2,2) - s.mesh.X(1,2);

            %xlim([mesh.x0 mesh.x1])
            %ylim([mesh.y0 mesh.y1])
            
            %zlim([-5 5])
            
            grid off;
            pbaspect([pbx pby 1])

            shading interp
            view([0,90])
            
            hold on;
            
            % Subplot 1 : C correction
%             subplot(1,2,1);            
            for e=1:s.mesh.numLocalElements
                element = s.mesh.getElement(e);
                
                el = element.localID;

                [X,Y] = element.getNodes;
                X = squeeze(X);
                Y = squeeze(Y);
                
                % C correction
                if (method==1)
                    temp = reshape( full(s.pfCorrection2{el}), element.finite.qSize );
                else
                    temp = reshape( full(s.pfCorrection1{el}), element.finite.qSize );
                end
                surface(X(:,:,end),Y(:,:,end),temp(:,:,end));
            end
            zlim([-1 1])
            
%             % Subplot 2 : Omega correction
%             subplot(1,2,2);            
%             for e=1:s.mesh.numLocalElements
%                 element = s.mesh.getElement(e);
%                 
%                 el = element.localID;
% 
%                 [X,Y] = element.getNodes;
%                 X = squeeze(X);
%                 Y = squeeze(Y);
%                 
%                 % C correction
%                 temp = reshape( full(s.pfCorrection2{el}), element.finite.qSize );
%                 surface(X(:,:,end),Y(:,:,end),temp(:,:,end));
%             end
        end

        % See Ding & Spelt (2007)
        %     Yue & Feng (2011)
        %     Dong (2012)
        %     Yue et al (2010)
        function applyDynamicContactAngle_v1(s,boundaryNumber,field,theta_s,bcType,coupledPhysics)
            boundary = s.mesh.getBoundary(boundaryNumber);
            activeFEBoundary = s.boundary_v1{boundaryNumber};   
                        
            if (isempty(theta_s))
                theta_s = System.settings.phys.ContactAngle;
            end
            
            switch bcType
                case BCType.Strong
                    error('A strong version of the dynamic contact angle bc has not been implemented')
                
                case BCType.Weak
                    
                    %U = coupledPhysics{2}.getField(1,s);
                    %V = coupledPhysics{2}.getField(2,s);
                    
                    U = FieldCollection.get('u');
                    V = FieldCollection.get('v');
                    
                    for be=1:boundary.getNumElements
                        
                        %indp = (1:boundary.getElements(be).disc.dofev) + (variable-1)*boundary.getElements(be).disc.dofev;
                        
                        e = boundary.getElementNumbers(be);
                                                
                        if ismember(e,s.mesh.eRange)
                            
                            element = s.mesh.getElement(e);
                            indq    = boundary.getHposQ(element.disc.dofeq);        % these are logicals

                            indqD   = boundary.getDposQ(element.disc.dofeq);        % these are logicals
                            
                            if (isempty(activeFEBoundary.opL{be,field.getVariable}))
                                activeFEBoundary.opL{be,field.getVariable} = sparse(element.disc.dofevq,element.disc.dofev);
                            end
                            if (isempty(activeFEBoundary.opG{be,field.getVariable}))
                                activeFEBoundary.opG{be,field.getVariable} = sparse(element.disc.dofevq,1);
                            end
                            if (isempty(activeFEBoundary.W{be}))
                                activeFEBoundary.W{be}                     = sparse(element.disc.dofevq,1);
                            end
                            
                            Dw = System.settings.phys.Dw;
                            
                            Uk      = U.val(e);
                            Vk      = V.val(e);
                            Conv    = ( Uk(indq).*boundary.Dx + Vk(indq).*boundary.Dy );
                            
%                             Ck = wi*C(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' + (1-wi)*Ci(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' ;

                            % NB: theta is in radians
                            %
                            % fw' = 6 * (C^2-C) * sigma * cos(theta)
                            % lambda = 6*sqrt(2) * sigma / Cn
                            % fw' / lambda = 0.5*sqrt(2) * (C^2-C) / Cn * cos(theta)

                            Ck = s.cField.val(e); %getC(e,SolutionMode.Nonlinear);
                            
                            factor      = -1/(sqrt(2)*System.settings.phys.Cn) * cos(theta_s/180*pi) .* (Ck(indq)-1); % 0 degrees: hydrophillic, 180 degrees: hydrophobic
                            factorrhs   = -1/(sqrt(2)*System.settings.phys.Cn) * cos(theta_s/180*pi) .* Ck(indq) .* (Ck(indq)-1); % 0 degrees: hydrophillic, 180 degrees: hydrophobic
                            
                            % the additional 'Ck(indq)' is included by H
                            
                            %boundary.opL{be}( indq(:), indp(:) ) = ( -Dw*(boundary.Dz+Conv) + boundary.Dn + factor.*boundary.H )* s.mesh.getZ(e);
                            % L = ( -Dw*(disc.By0.Dz*mesh.Jz(elem)+Conv) + disc.By0.D*mesh.Jy(elem) + (disc.By0.H)*0.5*sqrt(2)/Cn*cos(theta_s/180*pi)*sparse(diag(1-Ck')))

                            %mixingEnergy = System.settings.phys.alpha * System.settings.phys.Cn * System.settings.phys.sigma;
                            
                            valueVector = -1*factorrhs + 0*ones( nnz(indqD), 1 );

                            activeFEBoundary.opL{be,field.getVariable}( indqD(:), : ) = ( -Dw*(boundary.Dz+Conv) + boundary.Dn + 0*factor.*boundary.H )* s.mesh.getZ(e);
                                                                                                                  %boundary.Dn * s.mesh.getZ(e);      % unit operator
                            activeFEBoundary.W{be}( indqD(:) )                        = (boundary.Wdisc)./(boundary.J(be));
                            activeFEBoundary.opG{be,field.getVariable}( indqD(:) )    = valueVector(:) / boundary.Jn(be); 
                        end
                    end
            end
        end

        % See Ding & Spelt (2007)
        %     Yue & Feng (2011)
        %     Dong (2012)
        %     Yue et al (2010)
        function applyDynamicContactAngle(s,bcID,boundaryNumber,field,theta_s,bcType,coupledPhysics)
%             boundary = s.mesh.getBoundary(boundaryNumber);
            activeFEBoundary = s.boundary{bcID};   
                        
            if (isempty(theta_s))
                theta_s = System.settings.phys.ContactAngle;
            end
            
            switch bcType
                case BCType.Strong
                    error('A strong version of the dynamic contact angle bc has not been implemented')
                
                case BCType.Weak
                    
                    % Initialize the LGW cell arrays of this weak FEBoundary
                    activeFEBoundary.init(s.Nvar);
                    
%                     U = coupledPhysics{2}.getField(1,s);
%                     V = coupledPhysics{2}.getField(2,s);
                    U = FieldCollection.get('u');
                    V = FieldCollection.get('v');
                    
                    for be=1:activeFEBoundary.getNumElements
                        
                        %indp = (1:boundary.getElements(be).disc.dofev) + (variable-1)*boundary.getElements(be).disc.dofev;
                        
                        e = activeFEBoundary.getElementNumbers(be);
                                                
                        if ismember(e,s.mesh.eRange)
                            
                            element = s.mesh.getElement(e);
                            indq    = activeFEBoundary.getHposQ(element.disc.dofeq);        % these are logicals
                            indqD   = activeFEBoundary.getDposQ(element.disc.dofeq);        % these are logicals
                            
                            if (isempty(activeFEBoundary.opL{be,field.getVariable}))
                                activeFEBoundary.opL{be,field.getVariable} = sparse(element.disc.dofevq,element.disc.dofev);
                            end
                            if (isempty(activeFEBoundary.opG{be,field.getVariable}))
                                activeFEBoundary.opG{be,field.getVariable} = sparse(element.disc.dofevq,1);
                            end
                            if (isempty(activeFEBoundary.W{be}))
                                activeFEBoundary.W{be}                     = sparse(element.disc.dofevq,1);
                            end
                            
                            Dw = System.settings.phys.Dw;
                            
                            Uk      = U.val(e);
                            Vk      = V.val(e);
                            Conv    = ( Uk(indq).*activeFEBoundary.Dx + Vk(indq).*activeFEBoundary.Dy );
                            
%                             Ck = wi*C(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' + (1-wi)*Ci(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' ;

                            % NB: theta is in radians
                            %
                            % fw' = 6 * (C^2-C) * sisogma * cos(theta)
                            % lambda = 6*sqrt(2) * sigma / Cn
                            % fw' / lambda = 0.5*sqrt(2) * (C^2-C) / Cn * cos(theta)

                            Ck = s.cField.val(e); %getC(e,SolutionMode.Nonlinear);
                            
                            factor      = -1/(sqrt(2)*System.settings.phys.Cn) * cos(theta_s/180*pi) .* (Ck(indq)-1); % 0 degrees: hydrophillic, 180 degrees: hydrophobic
                            factorrhs   = -1/(sqrt(2)*System.settings.phys.Cn) * cos(theta_s/180*pi) .* Ck(indq) .* (Ck(indq)-1); % 0 degrees: hydrophillic, 180 degrees: hydrophobic
                            
                            dCkdx = s.cField.x(e);
                            dCkdy = s.cField.y(e);
                            
                            gradC = sqrt(dCkdx.^2 + dCkdy.^2);
                            
                            %factorrhs   = cos(theta_s/180*pi) .* gradC(indq);
                            
                            % the additional 'Ck(indq)' is included by H
                            
                            %boundary.opL{be}( indq(:), indp(:) ) = ( -Dw*(boundary.Dz+Conv) + boundary.Dn + factor.*boundary.H )* s.mesh.getZ(e);
                            % L = ( -Dw*(disc.By0.Dz*mesh.Jz(elem)+Conv) + disc.By0.D*mesh.Jy(elem) + (disc.By0.H)*0.5*sqrt(2)/Cn*cos(theta_s/180*pi)*sparse(diag(1-Ck')))

                            %mixingEnergy = System.settings.phys.alpha * System.settings.phys.Cn * System.settings.phys.sigma;
                            
                            valueVector = -1*factorrhs + 0*ones( nnz(indqD), 1 );

                            activeFEBoundary.opL{be,field.getVariable}( indqD(:), : ) = ( -Dw*(activeFEBoundary.Dz+Conv) + activeFEBoundary.Dn + 0*factor.*activeFEBoundary.H )* s.mesh.getZ(e);
                                                                                                                  %boundary.Dn * s.mesh.getZ(e);      % unit operator
                            activeFEBoundary.W{be}( indqD(:) )                        = (activeFEBoundary.Wdisc)./(activeFEBoundary.J(be));
                            activeFEBoundary.opG{be,field.getVariable}( indqD(:) )    = valueVector(:) / activeFEBoundary.Jn(be); 
                            
%                             value = 0;
                            
%                             if (~iscell(value) && length(value)==1)
%                                 boundary.opG{be}( indq(:) ) = value*ones(length(boundary.Wdisc),1);
%                             elseif (iscell(value) && numel(value{1})==element.disc.dofevq)
%                                 values = value{e}(:,:,end);
%                                 boundary.opG{be}( indq(:) ) = values(:);
%                             elseif (iscell(value) && numel(value{1})==numel(indq))
%                                 boundary.opG{be}( indq(:) ) = value{e}(:);
%                             else
%                                 disp('check dirichlet in CH')
%                             end
                            
%                             s.K{e}( ind,ind ) = s.K{e}(ind,ind) + weight * L' * W * L;
%                             if length(value)==1
%                                 s.G{e}(   ind   ) = s.G{e}(   ind   ) + weight * L' * W * value*ones(length(boundary.W),1);
%                             else
%                                 s.G{e}(   ind   ) = s.G{e}(   ind   ) + weight * L' * W * value{be}(:);
%                             end
                        end
                    end
                end
% % %             
% % %             %- dPhi/dx=dOmg/dx=0 at y=y0(bottom)
% % %             for ebc = 1:length(mesh.BCy0.elem)
% % %                 elem = mesh.BCy0.elem(ebc) ;
% % %                 dCbc = zeros(Qx*Qz,1) ;
% % %                 dOmgbc = zeros(Qx*Qz,1) ;
% % %                 indC = [1:mesh.dofev] ;
% % %                 indOmg = [(mesh.dofev+1):2*mesh.dofev] ;
% % %                 if elem>=e_start && elem<=e_end
% % %                     Ck = wi*C(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' + (1-wi)*Ci(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' ;
% % % 
% % %                     Dw = 0.9;
% % %                     %Uk = 0; % wc*U(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' + (1-wc)*Uc(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' ;
% % %                     %Vk = 0; % wc*V(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' + (1-wc)*Vc(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' ;
% % %                     %Conv    = ( diag(Uk)*disc.Dx*mesh.Jx(elem) + diag(Vk)*disc.Dy*mesh.Jy(elem) );
% % %                     Conv = 0;
% % % 
% % %                     CH.Ke{elem-e_start+1}(indC,indC) = CH.Ke{elem-e_start+1}(indC,indC) + wbc* ( ( -Dw*(disc.By0.Dz'*mesh.Jz(elem)+Conv') + disc.By0.D'*mesh.Jy(elem) + 0.5*sqrt(2)/Cn*cos(theta_s/180*pi)*sparse(diag(1-Ck'))*disc.By0.H') * ...
% % %                                                                                                       diag(disc.By0.p.w)./(mesh.Jx(elem)*mesh.Jz(elem))*...
% % %                                                                                                      ( -Dw*(disc.By0.Dz*mesh.Jz(elem)+Conv) + disc.By0.D*mesh.Jy(elem) + (disc.By0.H)*0.5*sqrt(2)/Cn*cos(theta_s/180*pi)*sparse(diag(1-Ck'))) );                                                                                             
% % % 
% % % 
% % %     %                 CH.Ke{elem-e_start+1}(indC,indC) = CH.Ke{elem-e_start+1}(indC,indC) + wbc* ( (disc.By0.D'*mesh.Jy(elem) + 0.5*sqrt(2)/Cn*cos(theta_s/180*pi)*sparse(diag(1-Ck'))*disc.By0.H') * ...
% % %     %                                                                                               diag(disc.By0.p.w)./(mesh.Jx(elem)*mesh.Jz(elem))*...
% % %     %                                                                                              (disc.By0.D*mesh.Jy(elem) + (disc.By0.H)*0.5*sqrt(2)/Cn*cos(theta_s/180*pi)*sparse(diag(1-Ck'))) );                                                                                             
% % % 
% % %     %                 CH.Ke{elem-e_start+1}(indC,indC)=CH.Ke{elem-e_start+1}(indC,indC) + (disc.By0.D'*mesh.Jy(elem))*diag(disc.By0.p.w)./(mesh.Jx(elem)*mesh.Jz(elem))*(disc.By0.D*mesh.Jy(elem))*wbc ;
% % %                     CH.Ge{elem-e_start+1}(indC)=CH.Ge{elem-e_start+1}(indC) + (disc.By0.D'*mesh.Jy(elem))*diag(disc.By0.p.w)./(mesh.Jx(elem)*mesh.Jz(elem))*dCbc*wbc ;
% % % 
% % %     %                 Ck = wi*C(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' + (1-wi)*Ci(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' ;
% % %     %                 CH.Ke{elem-e_start+1}(indC,indC) = CH.Ke{elem-e_start+1}(indC,indC) + wbc* ( (disc.By0.D'*mesh.Jy(elem) + 2*sqrt(2)/Cn*cos(theta_s/180*pi)*sparse(diag(1-Ck'))*disc.By0.H') * ...
% % %     %                                                                                               diag(disc.By0.p.w)./(mesh.Jx(elem)*mesh.Jz(elem))*...
% % %     %                                                                                              (disc.By0.D*mesh.Jy(elem) + (disc.By0.H)*2*sqrt(2)/Cn*cos(theta_s/180*pi)*sparse(diag(1-Ck'))) );
% % %     %                 CH.Ge{elem-e_start+1}(indC)=CH.Ge{elem-e_start+1}(indC) + (disc.By0.D'*mesh.Jy(elem))*diag(disc.By0.p.w)./(mesh.Jx(elem)*mesh.Jz(elem))*dCbc*wbc ;
% % % 
% % %                     CH.Ke{elem-e_start+1}(indOmg,indOmg)=CH.Ke{elem-e_start+1}(indOmg,indOmg) + (disc.By0.D'*mesh.Jy(elem))*diag(disc.By0.p.w)./(mesh.Jx(elem)*mesh.Jz(elem))*(disc.By0.D*mesh.Jy(elem))*wbc ;
% % %                     CH.Ge{elem-e_start+1}(indOmg)=CH.Ge{elem-e_start+1}(indOmg) + (disc.By0.D'*mesh.Jy(elem))*diag(disc.By0.p.w)./(mesh.Jx(elem)*mesh.Jz(elem))*dOmgbc*wbc ;
% % %                 end
% % %             end
        end
        
        function applyDirichletContactAngle(s,boundaryNumber,field,theta_s,bcType,coupledPhysics)
            boundary = s.mesh.getBoundary(boundaryNumber);
            activeFEBoundary = s.boundary{boundaryNumber};   
                        
            if (isempty(theta_s))
                theta_s = System.settings.phys.ContactAngle;
            end
            
            switch bcType
                case BCType.Strong
                    error('A strong version of the dynamic contact angle bc has not been implemented')
                
                case BCType.Weak
                    
                    %C = coupledPhysics{1}.getField(1,s);
                    
                    %U = coupledPhysics{2}.getField(1,s);
                    %V = coupledPhysics{2}.getField(2,s);
                    
                    C = FieldCollection.get('c');
                    U = FieldCollection.get('u');
                    V = FieldCollection.get('v');
                    
                    for be=1:boundary.getNumElements
                        
                        %indp = (1:boundary.getElements(be).disc.dofev) + (variable-1)*boundary.getElements(be).disc.dofev;
                        
                        e = boundary.getElementNumbers(be);
                                                
                        if ismember(e,s.mesh.eRange)
                            
                            element = s.mesh.getElement(e);
                            indq    = boundary.getHposQ(element.disc.dofeq);        % these are logicals

                            indqD   = boundary.getDposQ(element.disc.dofeq);        % these are logicals
                            
                            if (isempty(activeFEBoundary.opL{be,field.getVariable}))
                                activeFEBoundary.opL{be,field.getVariable} = sparse(element.disc.dofevq,element.disc.dofev);
                            end
                            if (isempty(activeFEBoundary.opG{be,field.getVariable}))
                                activeFEBoundary.opG{be,field.getVariable} = sparse(element.disc.dofevq,1);
                            end
                            if (isempty(activeFEBoundary.W{be}))
                                activeFEBoundary.W{be}                     = sparse(element.disc.dofevq,1);
                            end
                            
                            Dw = System.settings.phys.Dw;
                            
                            Uk      = U.val(e);
                            Vk      = V.val(e);
                            Conv    = ( Uk(indq).*boundary.Dx + Vk(indq).*boundary.Dy );
                            
%                             Ck = wi*C(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' + (1-wi)*Ci(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' ;

                            % NB: theta is in radians
                            %
                            % fw' = 6 * (C^2-C) * sigma * cos(theta)
                            % lambda = 6*sqrt(2) * sigma / Cn
                            % fw' / lambda = 0.5*sqrt(2) * (C^2-C) / Cn * cos(theta)

                            Ck = s.cField.val(e); %getC(e,SolutionMode.Nonlinear);
                            Cxx = s.cField.xx(e);
                            Cyy = s.cField.yy(e);
                            
                            %factor      = -1/(sqrt(2)*System.settings.phys.Cn) * cos(theta_s/180*pi) .* (Ck(indq)-1); % 0 degrees: hydrophillic, 180 degrees: hydrophobic
                            %factorrhs   = -1/(sqrt(2)*System.settings.phys.Cn) * cos(theta_s/180*pi) .* Ck(indq) .* (Ck(indq)-1); % 0 degrees: hydrophillic, 180 degrees: hydrophobic
                            
                            % the additional 'Ck(indq)' is included by H
                            
                            %boundary.opL{be}( indq(:), indp(:) ) = ( -Dw*(boundary.Dz+Conv) + boundary.Dn + factor.*boundary.H )* s.mesh.getZ(e);
                            % L = ( -Dw*(disc.By0.Dz*mesh.Jz(elem)+Conv) + disc.By0.D*mesh.Jy(elem) + (disc.By0.H)*0.5*sqrt(2)/Cn*cos(theta_s/180*pi)*sparse(diag(1-Ck')))

                            %mixingEnergy = System.settings.phys.alpha * System.settings.phys.Cn * System.settings.phys.sigma;
                            
                            %valueVector = -1*factorrhs + 0*ones( nnz(indqD), 1 );
                            
                            C   = Ck(indq);
                            Cxx = Cxx(indq);
                            Cyy = Cyy(indq);
                            
                            valueVector = System.settings.phys.alpha/System.settings.phys.Cn * ( (3*C.^3-3/2*C.^2+0.5.*C) - System.settings.phys.Cn^2 .* ( Cxx + Cyy ) );

                            activeFEBoundary.opL{be,field.getVariable}( indqD(:), : ) = ( -0*Dw*(boundary.Dz+Conv) + 0*boundary.Dn + boundary.H )* s.mesh.getZ(e);
                                                                                                                  %boundary.Dn * s.mesh.getZ(e);      % unit operator
                            activeFEBoundary.W{be}( indqD(:) )                        = (boundary.Wdisc)./(boundary.J(be));
                            activeFEBoundary.opG{be,field.getVariable}( indqD(:) )    = valueVector(:) / boundary.Jn(be); 
                            
%                             value = 0;
                            
%                             if (~iscell(value) && length(value)==1)
%                                 boundary.opG{be}( indq(:) ) = value*ones(length(boundary.Wdisc),1);
%                             elseif (iscell(value) && numel(value{1})==element.disc.dofevq)
%                                 values = value{e}(:,:,end);
%                                 boundary.opG{be}( indq(:) ) = values(:);
%                             elseif (iscell(value) && numel(value{1})==numel(indq))
%                                 boundary.opG{be}( indq(:) ) = value{e}(:);
%                             else
%                                 disp('check dirichlet in CH')
%                             end
                            
%                             s.K{e}( ind,ind ) = s.K{e}(ind,ind) + weight * L' * W * L;
%                             if length(value)==1
%                                 s.G{e}(   ind   ) = s.G{e}(   ind   ) + weight * L' * W * value*ones(length(boundary.W),1);
%                             else
%                                 s.G{e}(   ind   ) = s.G{e}(   ind   ) + weight * L' * W * value{be}(:);
%                             end
                        end
                    end
            end          
        end

        function createInterface(s)

            cField        = s.getField(Variable.c,s);
            mesh          = cField.mesh;
            cData         = cField.value([],[],3);
            C             = cell(mesh.numLocalElements,1);
            
            %% Interface settings
            s.interface         = struct();
            s.interface.value   = 0.5;
            s.interface.active  = true;
            s.interface.props   = {'Color', 'k', 'LineWidth', 2};
            s.interface.x       = cell(mesh.numLocalElements,1);
            s.interface.y       = cell(mesh.numLocalElements,1);
            %interface.z        = cell(mesh.numLocalElements,1);                     
            
            %% Extract interface 
            % Find the contour where the C surface is equal to the
            % requested interface value. Next, extract the x- and y-locations 
            % from the contour matrix C. Interpolate on the C surface
            % to find z-locations for the intersection. Finally plot the
            % line with some properties.
            for e=1:mesh.numLocalElements
                element = mesh.getLocalElement(e);
                
%                 [X,Y] = meshgrid( element.nodes{1:end-1} );
                [X,Y] = element.getNodes;
                X = squeeze(X);
                Y = squeeze(Y);
                
                C{e} = contourc(X,Y,cData{e}(:,:,end)',[s.interface.value s.interface.value]);
                if (~isempty(C{e}))
%                     s.interface.x{e} = C{e}(1, 2:1+C{e}(2,1));
%                     s.interface.y{e} = C{e}(2, 2:1+C{e}(2,1));
                    s.interface.x{e} = C{e}(1, 2:end);
                    s.interface.y{e} = C{e}(2, 2:end);
                    if C{e}(2,1)~=size(C{e},2)-1
                       s.interface.x{e}(:, C{e}(2,1)+1 ) = [];
                       s.interface.y{e}(:, C{e}(2,1)+1 ) = [];
                       s.interface.x{e}(:, 1:C{e}(2,1) ) = fliplr( s.interface.x{e}(:, 1:C{e}(2,1) ) );
                       s.interface.y{e}(:, 1:C{e}(2,1) ) = fliplr( s.interface.y{e}(:, 1:C{e}(2,1) ) );
                    end
                    %interface.z{e} = interp2(X{e}(1,:)', Y{e}(:,1), dataVar{e}, interface.x{e}, interface.y{e});
                    %line(s.interface.x{e}, s.interface.y{e}, repmat(s.interface.value,size(s.interface.x{e})), s.interface.props{:});
                end
            end
        end
        
        % Function to plot the mean curvature as computed from the C field
        function plotH(s)
            % The C function is defined by: C = 0.5 + 0.5 * tanh( z / (2*sqrt(2)*Cn) )
            % From this function the half width parameter w is defined as w = 2*sqrt(2)*Cn
            % Furthermore, we define m = C * (C-1)
            
            s.computeH2(s.mesh,true);
            
            for e=s.mesh.eRange                
                element = s.mesh.getElement(e);
                el = element.localID;
                
                [X,Y] = element.getNodes;
                
                X = squeeze(X); Y = squeeze(Y);
                
                meanCurvature = reshape( s.H{el}, element.finite.qSize );
                
                surface(X(:,:,end),Y(:,:,end),meanCurvature(:,:,end));
                hold on;
            end
            
            x0 = s.mesh.X(1,1);
            x1 = s.mesh.X(2,1);
            y0 = s.mesh.X(1,2);
            y1 = s.mesh.X(2,2);
            
            pbx = x1 - x0;
            pby = y1 - y0;
            
            grid off;
            pbaspect([pbx pby 1])

            zlim([-2 2])
            
            %shading interp
            view([0,90])
        end
        
        % Function to plot the mean curvature as computed from the C field
        function plotHGradC(s)
            % The C function is defined by: C = 0.5 + 0.5 * tanh( z / (2*sqrt(2)*Cn) )
            % From this function the half width parameter w is defined as w = 2*sqrt(2)*Cn
            % Furthermore, we define m = C * (C-1)
            
            s.computeH2(s.mesh,true);
            
            C = s.fields{1};
            
            for e=s.mesh.eRange                
                element = s.mesh.getElement(e);
                el = element.localID;
                
                [X,Y] = element.getNodes;
                
                cx  = C.x2(e);
                cy  = C.y2(e);
                gradC = sqrt( cx.^2 + cy.^2 );
                
                X = squeeze(X); Y = squeeze(Y);
                
                meanCurvature = reshape( s.H{el}, element.finite.qSize );
                gradC         = reshape( gradC  , element.finite.qSize );
                
                surface(X(:,:,end),Y(:,:,end),meanCurvature(:,:,end).*gradC);
                hold on;
            end
            
            x0 = s.mesh.X(1,1);
            x1 = s.mesh.X(2,1);
            y0 = s.mesh.X(1,2);
            y1 = s.mesh.X(2,2);
            
            pbx = x1 - x0;
            pby = y1 - y0;
            
            grid off;
            pbaspect([pbx pby 1])

            zlim([-2 20])
            
            %shading interp
            view([0,90])
        end
        
        % Function to plot the mean curvature as computed from the C field
        function plotH2_av(s)
            % The C function is defined by: C = 0.5 + 0.5 * tanh( z / (2*sqrt(2)*Cn) )
            % From this function the half width parameter w is defined as w = 2*sqrt(2)*Cn
            % Furthermore, we define m = C * (C-1)
            
            s.computeH2_av(s.mesh,true);
            
            for e=s.mesh.eRange                
                element = s.mesh.getElement(e);
                el = element.localID;
                
                [X,Y] = element.getNodes;
                
                X = squeeze(X); Y = squeeze(Y);
                
                meanCurvature = reshape( s.H{el}, element.finite.qSize );
                
                surface(X(:,:,end),Y(:,:,end),meanCurvature(:,:,end));
                hold on;
            end
            
            zlim([-2 2])
        end
        
        % Function to plot the mean curvature as computed from the C field
        function plotDelta(s)
            % The C function is defined by: C = 0.5 + 0.5 * tanh( z / (2*sqrt(2)*Cn) )
            % From this function the half width parameter w is defined as w = 2*sqrt(2)*Cn
            % Furthermore, we define m = C * (C-1)
            
            s.computeH(s.mesh,true);
            
            C = s.fields{1};
            
            for e=s.mesh.eRange                
                element = s.mesh.getElement(e);
                el = element.localID;
                
                [X,Y] = element.getNodes;
                
                X = squeeze(X); Y = squeeze(Y);
                
                c      = reshape(C.getValue(el),element.finite.qSize);
                cxx    = reshape(C.xx(FieldType.Current,el),element.finite.qSize);
                cyy    = reshape(C.yy(FieldType.Current,el),element.finite.qSize);
                
                [X,Y]    = element.getNodes;
                                
                c = c(:,:,end);
                
                %potential = s.alpha/s.Cn * s.Cn/s.Cn_L * ( 1*(c.^3-3/2*c.^2+1/2*c) - 1*s.Cn_L^2*(cxx+cyy) );
                potential = s.alpha/s.Cn * ( 1*(c.^3-3/2*c.^2+1/2*c) - 1*s.Cn_L^2*(cxx+cyy) );
                
                lhs = potential;
                rhs = s.Cn_L/s.Cn * s.H{el};
                
                delta = lhs-rhs;
                
                surface(X(:,:,end),Y(:,:,end),delta(:,:,end));
                hold on;
            end
        end
        
        function plotInterface(s,plotCurv)
            % Function to plot the interface, which is defined by C=1/2.
            % For a 2D setup it will be a curve, and for a 3D setup a
            % surface
            
            if nargin==1
                plotCurv = false;
            end
            
            scale = 5;
            
            integrateHO = false;
            if (integrateHO)
                plotOrder = 10;
                plotFE = FiniteElement(System.settings.stde.Pn,plotOrder*ones(size(System.settings.stde.Q)),System.settings.stde.C,s.mesh.sDim,s.mesh.spaceTime);
            end
            
            s.computeH2_av;
%             s.computeH2;
            
            hold on

            xL_all = [];
            yL_all = [];
            val_all = [];
            
            for e=s.mesh.eRange
                
                % Set the element
                element = s.mesh.getElement(e);
                
                if (integrateHO)
                    element.setFinite( plotFE );
                end
                
                [x,y,z,t] = element.getNodes();
                
                x = squeeze(x);
                y = squeeze(y);
                t = squeeze(t);
                
                c = s.fields{1};
                
                cValues = reshape( c.val(FieldType.Current,e), size(x) );
                
%                 if ( s.timeMethod == TimeMethod.SpaceTime )
%                     lEnd = 2;
%                 else
%                     lEnd = 1;
%                 end
%                 
%                 if l==1
%                     pos = 1;
%                 else
%                     pos = size(x,3);
%                 end

                % 3D
                %p = patch(isosurface(x(:,:,l),y(:,:,l),t(:,:,l),cValues(:,:,l),0.5));
                %verts = get(p, 'Vertices');
                %faces = get(p, 'Faces');

                % 2D
                % create a contour object for C = 0.5
                %p = contour(x(:,:,pos),y(:,:,pos),cValues(:,:,pos),[0.5 0.5]);

                %p = contourc(x(:,1),y(1,:),cValues(:,:)',[0.5 0.5]);
                p = contours(x, y, cValues, [0.5 0.5]);
                
%                 Iblur = 3 * imgaussfilt( 1.0*(cValues>0.5) , 2.0);
%                 Iblur = 3 * (1.0*(cValues>=0.5));
%                 Iblur = cValues;
                
                if (~isempty(p))
                    % Extract the x- and y-locations from the contour matrix C.
                    xL = p(1, 2:end);
                    yL = p(2, 2:end);
                    
                    if isempty(xL_all)
                        xL_all = xL(1:end)';
                        yL_all = yL(1:end)';
                    else
                        xL_all = [ xL_all ; NaN; xL(1:end)' ];
                        yL_all = [ yL_all ; NaN; yL(1:end)' ];
                    end
                    
                    if (plotCurv)
                        values = plotCurv*scale* 2*s.H{e};
%                         values = imgaussfilt( values, 0.2);
                        
                    else
                        values = scale * ones( size(x) );
                    end
                    
                    % Interpolate on the first surface to find z-locations for the intersection line.
                    zL = interp2(x', y', values', xL, yL,'linear');
                    
%                     val_all = [val_all zL];
                    
                    % Visualize the line.
%                     line(xL, yL, zL, 'Marker','s','Color', 'r', 'LineWidth', 3);
                    
                    Idx = knnsearch([x(:) y(:)],[xL' yL'],'K',2); % nearest neighbor indices
                    
                    xN = x(Idx);
                    yN = y(Idx);
                    zN = ones( size(xN) );
                   
                    scatter3(xN(:),yN(:),zN(:),[],'g','Marker','s','LineWidth', 3);
                    
                    F1 = scatteredInterpolant(xL(:),yL(:),zL(:));
                    
                    if numel(zL)>2
                        F1.ExtrapolationMethod = 'nearest';
                        curv = F1(x,y);
                        curv = mean(curv(:)) * ones( size(x)) ;
                    else
                        curv = mean(xL) * ones( size(x)) ;
                    end
%                     z  = interp2(xL,yL,zL,x,y);

                    surf(x,y,curv)
                end
                
%                 surf(x,y,Iblur)
                
                surface(x,y,zeros(size(x)));
                
                
                if (integrateHO)
                    element.setFinite(); % reset to computation finite element
                end
                
%                     % use the contour object to compute the linear line length 
%                     % for each segment (p(1,1)==0.5 and p(2,1)=number of segments)
%                     if (~isempty(p))
%                         diff = zeros(2,p(2,1)-1);
%                         for n=1:p(2,1)-1
%                             diff(1,n) = p(1,n+2) - p(1,n+1);
%                             diff(2,n) = p(2,n+2) - p(2,n+1);
%                             diff(3,n) = sqrt(diff(1,n)^2 + diff(2,n)^2);
%                         end
%                         length(l) = length(l) + sum(diff(3,:));
%                     end
            end
            
%             [xL_all yL_all] = polymerge(xL_all,yL_all,1e-5);
%             p=[xL_all yL_all];
%             
% %             for i=2:size(p,1)
% %                 if ( norm(p(i-1,:)-p(i,:)) < 1e-4 )
% %                     p(i,:) = ( p(i-1,:) + p(i,:) ) /2;
% %                     p(i-1,:) = NaN; 
% %                 end
% %             end
%             
%             % remove NaN
%             p(any(isnan(p), 2), :) = [];
%             
%             for i=2:size(p,1)
%                 if ( norm(p(i-1,:)-p(i,:)) < 1e-4 )
%                     p(i,:) = ( p(i-1,:) + p(i,:) ) /2;
%                     p(i-1,:) = NaN; 
%                 end
%             end
%             
%             % compare start/end
%             if ( norm(p(1,:)-p(end,:)) < 1e-4 )
%                 p(end,:) = ( p(1,:) + p(end,:) ) /2;
%                 p(1,:) = NaN; 
%             end
%             
%             % remove NaN
%             p(any(isnan(p), 2), :) = [];
%             
%             p2 = interparc(1000,p(:,1),p(:,2));
%             p2 = p;
%             
%             curv = LineCurvature2D(p2);
%             line(p2(:,1), p2(:,2), abs(curv), 'Marker','s','Color', 'b', 'LineWidth', 3);
            
%             clf;plot(val_all,'Marker','s'); hold on;test = imgaussfilt3( val_all, 5,'Padding','circular');plot(test,'r')

            x0 = s.mesh.X(1,1);
            x1 = s.mesh.X(2,1);
            y0 = s.mesh.X(1,2);
            y1 = s.mesh.X(2,2);
            
            pbx = x1 - x0;
            pby = y1 - y0;
            
            grid off;
            pbaspect([pbx pby 1])

            xlim([x0 x1])
            ylim([y0 y1])
            zlim([-1 6])
            
            %shading interp
            view([30,30])
        end
        
        % Function to plot the mean curvature as computed from the C field
        function computeH(s,mesh,useMeanCurvature)
            % The C function is defined by: C = 0.5 + 0.5 * tanh( z / (2*sqrt(2)*Cn) )
            % From this function the half width parameter w is defined as w = 2*sqrt(2)*Cn
            % Furthermore, we define m = C * (C-1)
            
            if (nargin==1)
                mesh = s.mesh;
                useMeanCurvature = s.useMeanCurvature;
            elseif (nargin==2)
                useMeanCurvature = s.useMeanCurvature;
            end
            
            s.H = cell( numel(mesh.eRange),1 );
            
            if (useMeanCurvature)
                w = 2*sqrt(2) * s.Cn_L;
                C = s.fields{1};

                for e=mesh.eRange
                    element = mesh.getElement(e);
                    el = element.localID;

                    c   = C.val2(e,mesh);
                    cx  = C.x2(e,mesh);
                    cy  = C.y2(e,mesh);
                    cxx = C.xx2(e,mesh);
                    cyy = C.yy2(e,mesh);
                    cxy = C.xy2(e,mesh);

                    gradC = sqrt( cx.^2 + cy.^2 );

                    m = c .* (c-1);

%                     H_enum  = -0.5*w * ( (2*s.Cn_L^2 * (cy).^2 + m.^2).*cxx ...
%                                        + (2*s.Cn_L^2 * (cx).^2 + m.^2).*cyy ...
%                                        -  4*s.Cn_L^2 * cx.*cy.*cxy ...
%                                        -  2*m.*(c-0.5) .* (cx.^2+cy.^2) );
% %                     H_enum  = -0.5*w * ( -(2*s.Cn_L^2 * (cx).^2).*cxx ...
% %                                        - (2*s.Cn_L^2 * (cy).^2).*cyy ...
% %                                        -  4*s.Cn_L^2 * cx.*cy.*cxy ...
% %                                        -  2*m.*(c-0.5) .* (cx.^2+cy.^2) );
%                     H_denom = ( 2*s.Cn_L^2 * ( cx.^2 + cy.^2 ) + m.^2 ) .^(3/2);
% 
%                     H = H_enum ./ H_denom;
%                     meanCurvature = (1 / sqrt(2)) .* H; %* s.Cn_L^2 .* H;
                    
                    H_enum  = -0.5*s.Cn_L * ( (0.25*m.^2 + 0.5*s.Cn_L^2 * (cy).^2).*cxx ...
                                            + (0.25*m.^2 + 0.5*s.Cn_L^2 * (cx).^2).*cyy ...
                                            -  s.Cn_L^2 * cx.*cy.*cxy ...
                                            -  0.5*m.*(c-0.5) .* (cx.^2+cy.^2) );
                    H_denom = ( 0.25*m.^2 + 0.5*s.Cn_L^2 * (cx.^2+cy.^2) ) .^(3/2);
                    
                    H = H_enum ./ H_denom;
                    meanCurvature = H;

                    s.H{el} = reshape( meanCurvature, element.finite.qSize );
                end
                
            else
                for e=1:numel(mesh.eRange)
                    s.H{e} = 0;
                end
            end
        end
        
        % Function to plot the mean curvature as computed from the C field
        function computeH2(s,mesh,useMeanCurvature)
            % The C function is defined by: C = 0.5 + 0.5 * tanh( z / (2*sqrt(2)*Cn) )
            % From this function the half width parameter w is defined as w = 2*sqrt(2)*Cn
            % Furthermore, we define m = C * (C-1)
            
            if (nargin==1)
                mesh = s.mesh;
                useMeanCurvature = s.useMeanCurvature;
            elseif (nargin==2)
                useMeanCurvature = s.useMeanCurvature;
            end
            
            useMeanCurvature = true;
            
            s.H = cell( numel(mesh.eRange),1 );
            
            if (useMeanCurvature)
                C = s.fields{1};

                for e=mesh.eRange
                    element = mesh.getElement(e);
%                     element.setFinite();
                    
                    el = element.localID;

                    %c   = C.val2(e,mesh);
                    cx  = C.x2(e,mesh);
                    cy  = C.y2(e,mesh);
                    cxx = C.xx2(e,mesh);
                    cyy = C.yy2(e,mesh);
                    cxy = C.xy2(e,mesh);  
                                        
                    gradC = sqrt( cx.^2 + cy.^2 );
                    
                    %H_enum  = gradC.^2 .* (cxx+cyy) - (cx.^2.*cxx + 2*cx.*cy.*cxy + cy.^2.*cyy );
                    H_enum  = -cy.^2.*cxx + 2*cx.*cy.*cxy - cx.^2.*cyy;     % equal to: gradC^T * H(C) * gradC - gradC.^2 * Lap(C)
                    H_denom = gradC.^3;
                    
                    H = H_enum ./ H_denom;
                    
                    H( abs(H_denom)<1e-8 ) = 0;
                    meanCurvature = 0.5 * H;    % NB: the last term is a Jacobian
                    
                    s.H{el} = reshape( meanCurvature, element.finite.qSize );
                end
                
            else
                for e=1:numel(mesh.eRange)
                    s.H{e} = 0;
                end
            end
        end
        
        % Function to plot the mean curvature as computed from the C field
        function computeH2_av(s,mesh,useMeanCurvature)
            % The C function is defined by: C = 0.5 + 0.5 * tanh( z / (2*sqrt(2)*Cn) )
            % From this function the half width parameter w is defined as w = 2*sqrt(2)*Cn
            % Furthermore, we define m = C * (C-1)
            
            if (nargin==1)
                mesh = s.mesh;
                useMeanCurvature = s.useMeanCurvature;
            elseif (nargin==2)
                useMeanCurvature = s.useMeanCurvature;
            end
            
            useMeanCurvature = true;
            
            s.H = cell( numel(mesh.eRange),1 );
            
            if (useMeanCurvature)
                C = s.fields{1};

                % Compute the mean for Cxx
                C.xx2_av_init;
                C.yy2_av_init;
                
                for e=mesh.eRange
                    element = mesh.getElement(e);
                    el = element.localID;
                    
%                     element.setFinite();

                    c   = C.val2(e,mesh);
                    cx  = C.x2(e,mesh);
                    cy  = C.y2(e,mesh);

                    cxx = C.xx2_mean(e);
                    cyy = C.yy2_mean(e);
                    
                    %cxx = C.xx2(e,mesh);
                    %cyy = C.yy2(e,mesh);
                    
                    cxy = C.xy2(e,mesh);  
                                        
                    normC = sqrt( cx.^2 + cy.^2 );
                    
                    %H_enum  = gradC.^2 .* (cxx+cyy) - (cx.^2.*cxx + 2*cx.*cy.*cxy + cy.^2.*cyy );
                    H_enum  = -cy.^2.*cxx + 2*cx.*cy.*cxy - cx.^2.*cyy;     % equal to: gradC^T * H(C) * gradC - gradC.^2 * Lap(C)
                    H_denom = normC.^3;
                    
                    H = H_enum ./ H_denom;
                    
                    H( abs(H_denom)<1e-8 ) = 0;
                    meanCurvature = 0.5 * H;    % NB: the last term is a Jacobian
                    
                    s.H{el} = meanCurvature;
                    %s.H{el}( abs(c-0.5)>0.499 ) = 0;
                    
                    s.H{el} = reshape( s.H{el}, element.finite.qSize );
                end
                
            else
                for e=1:numel(mesh.eRange)
                    s.H{e} = 0;
                end
            end
        end
        
        
        % Function to plot the mean curvature as computed from the C field
        function computeH2_3D(s,mesh,useMeanCurvature)
            % The C function is defined by: C = 0.5 + 0.5 * tanh( z / (2*sqrt(2)*Cn) )
            % From this function the half width parameter w is defined as w = 2*sqrt(2)*Cn
            % Furthermore, we define m = C * (C-1)
            
            if (nargin==1)
                mesh = s.mesh;
%                 useMeanCurvature = s.useMeanCurvature;
            elseif (nargin==2)
%                 useMeanCurvature = s.useMeanCurvature;
            end
            
            useMeanCurvature = true;
            
            s.H = cell( numel(mesh.eRange),1 );
            
            if (useMeanCurvature)
                C = s.fields{1};

                for e=mesh.eRange
                    element = mesh.getElement(e);
                    el = element.localID;

                    c   = C.val2(e,mesh);
                    cx  = C.x2(e,mesh);
                    cy  = C.y2(e,mesh);
                    cz  = C.z2(e,mesh);
                    cxx = C.xx2(e,mesh);
                    cyy = C.yy2(e,mesh);
                    czz = C.zz2(e,mesh);
                    cxy = C.xy2(e,mesh);
                    cxz = C.xz2(e,mesh);
                    cyz = C.yz2(e,mesh);
                                        
                    gradC = sqrt( cx.^2 + cy.^2 + cz.^2 );
                    
                    %H_enum  = gradC.^2 .* (cxx+cyy) - (cx.^2.*cxx + 2*cx.*cy.*cxy + cy.^2.*cyy );
                    %H_enum  = -cy.^2.*cxx + 2*cx.*cy.*cxy - cx.^2.*cyy;     
                    
                    % H_enum = gradC^T * H(C) * gradC - gradC.^2 * Lap(C)
                    H_enum  =   cx.^2.*cxx  +   cy.^2.*cyy  +   cz.^2.*czz  + ...
                              2*cx.*cy.*cxy + 2*cx.*cz.*cxz + 2*cy.*cz.*cyz - ...
                              gradC.^2 .* ( cxx + cyy + czz );
                    H_denom = gradC.^3;
                    
                    H = H_enum ./ H_denom;
                    
                    H( abs(H_denom)<1e-8 ) = 0;
                    meanCurvature = 0.5 * H;
                    
                    s.H{el} = reshape( meanCurvature, element.finite.qSize );
                end
                
            else
                for e=1:numel(mesh.eRange)
                    s.H{e} = 0;
                end
            end
        end
        
        
        function computeG(s)
            % The C function is defined by: C = 0.5 + 0.5 * tanh( z / (2*sqrt(2)*Cn) )
            % From this function the half width parameter w is defined as w = 2*sqrt(2)*Cn
            % Furthermore, we define m = C * (C-1)
            
            w = 2*sqrt(2) * s.Cn_L;
            C = s.fields{1};
            
            s.G = cell( numel(s.mesh.eRange),1 );
            
            for e=s.mesh.eRange
                element = s.mesh.getElement(e);
                el = element.localID;
                
                c   = C.val('Current',e);                
                cx  = C.x('Current',e);
                cy  = C.y('Current',e);
                cxx = C.xx('Current',e);
                cyy = C.yy('Current',e);
                cxy = C.xy('Current',e);
                
                m = c .* (1-c);
%                 
%                 G_enum  = -4*w * ( (2*s.Cn_L^2 * ( cx.^2 .* cxx + cy.^2.*cyy + 2*cx.*cy.*cxy...
%                                    -  2*m.*(c-0.5) .* (cx.^2+cy.^2) );
%                 G_denom = ( 2*s.Cn_L^2 * ( cx.^2 + cy.^2 ) + m.^2 );
                
                G = G_enum ./ G_denom;
                
                meanCurvature = G_enum ./ G_denom;
                
                s.G{el} = reshape( meanCurvature, element.finite.qSize );
                
                %surface(X(:,:,end),Y(:,:,end),meanCurvature(:,:,end));
                %hold on;
            end
        end
        
        function out = getMeanKappa(s)
            s.computeH();
            
            out = cell(numel(s.H),1);
            
            for i=1:numel(s.H)
                out{i} = s.H{i}(:);
                %out{i} = s.alpha/(4*sqrt(2)) * s.Cn_L/s.Cn * s.H{i}(:);
            end
        end
        
        function [kx,ky] = getCurvature(s,mesh)
            if (nargin==1)
                mesh = s.mesh;
            end
            
            s.computeH2(mesh,1);
            
            kx = cell(numel(s.H),1);
            ky = cell(numel(s.H),1);
            
            C   = s.fields{1};
            Omg = s.fields{2};
            
            % Default option: 1
            % New kappaGamma option: 3
            curvatureOption = 1;
            
            kappaMean = 0;
            
            if (curvatureOption>=3)
                kappaGamma = s.computeKappaGamma;
            end
            
            for e=mesh.eRange
                element = mesh.getElement(e);
                el = element.localID;

                Omgk    = Omg.val2(e,mesh);
                dCkdx   = C.x2(e,mesh);
                dCkdy   = C.y2(e,mesh);

                switch curvatureOption
                    case 0
                        % The curvature (kappa) is defined as 2*H, where H is the mean curvature. Therefore, 
                        % the norm of the curvature of this method is equal to |kappa| * |gradC|^2
                        gradC   = sqrt( dCkdx.^2 + dCkdy.^2 );
                        kx{el} = s.alpha * ( 0*Omgk + s.Cn_L * ( 2 * s.H{el}(:) .* gradC ) ) .* dCkdx;
                        ky{el} = s.alpha * ( 0*Omgk + s.Cn_L * ( 2 * s.H{el}(:) .* gradC ) ) .* dCkdy;
                    case 1
                        % Here the norm of the curvature becomes equal to |kappa| * |gradC|
                        %gradC   = sqrt( dCkdx.^2 + dCkdy.^2 );
                        kx{el} = (2 * s.H{el}(:) - kappaMean) .* dCkdx; % .* (s.alpha * s.Cn_L * gradC).^6;
                        ky{el} = (2 * s.H{el}(:) - kappaMean) .* dCkdy; % .* (s.alpha * s.Cn_L * gradC).^6;
                    case 2 
                        % Here the norm of the curvature becomes equal to |kappa|
                        gradC   = sqrt( dCkdx.^2 + dCkdy.^2 );
                        kx{el} = 2 * s.H{el}(:) .* dCkdx ./ gradC;
                        ky{el} = 2 * s.H{el}(:) .* dCkdy ./ gradC;

                    case 3
                        gradC   = sqrt( dCkdx.^2 + dCkdy.^2 );

                        scaler = s.alpha * s.Cn_L * gradC;

                        kx{el} = scaler .* kappaGamma{el}(:) .* dCkdx ./ gradC;
                        ky{el} = scaler .* kappaGamma{el}(:) .* dCkdy ./ gradC;
                        
                    case 4
                        Ck = C.val2(e,mesh);
                        gradC   = sqrt( dCkdx.^2 + dCkdy.^2 );
                        
                        %Hs = Heaviside(Ck);
                        Ds = Delta(Ck,s.heavisideThickness);
                        
                        %Ds = Delta(Ck);
                        
                        kx{el} = kappaGamma{el}(:).*Ds.*dCkdx ./ gradC;
                        ky{el} = kappaGamma{el}(:).*Ds.*dCkdy ./ gradC;
                end
            end
        end
        
%         function [kappa] = getKappa(s,mesh)
%             if (nargin==1)
%                 mesh = s.mesh;
%             end
%             
%             s.computeH2(mesh,1);
%             
%             kappa = cell(numel(s.H),1);
%             
%             C   = s.fields{1};
%             
%             % Mean curvature correction
%             for e=mesh.eRange
%                 element = mesh.getElement(e);
%                 el = element.localID;
% 
%                 dCkdx   = C.x2(e,mesh);
%                 dCkdy   = C.y2(e,mesh);
% 
%                 gradC   = sqrt( dCkdx.^2 + dCkdy.^2 );
%                 
%                 kappa{el} = (s.alpha) * ( s.Cn_L * ( 2 * s.H{el}(:) .* gradC ) );
% %                 kappa{el} = ( s.Cn_L * ( 2 * s.H{el}(:) ) ) .* gradC;
%             end
%         end
        
        function out = getKappa(s)
            mesh = s.mesh;
            
            %% Step A : Initialize/update the kappa Field
            kappaField = FieldCollection.get('kappa');
            
            if (~isa(kappaField,'Field'))
                fieldID  = 6;
                activate = true;
                periodic = [ false false  ;...
                             false false ];
                kappaField = Field(s.mesh,fieldID,'kappa',periodic);
                FieldCollection.add( kappaField );
                kappaField.addSubField(FieldType.Current,activate,[],[],[],true);
            else
                % if kappaField exists, verify if the mesh has been updated
                kappaField.meshUpdate;
            end
            
            %save(['getSurfaceTension.' int2str(System.rank) '.mat']);

            % Load fields to be used
            C = s.fields{1};
            
            % Compute the mean curvature
            s.computeH2_av(mesh,1);

            % Compute kappa and store as a field
            for e=mesh.eRange
                element = mesh.getElement(e);
                el      = element.localID;

                dCkdx   = C.x2(e,mesh);
                dCkdy   = C.y2(e,mesh);
                normC   = sqrt( dCkdx.^2 + dCkdy.^2 );

%                 value = 2 * s.H{el}(:); %.* normC ) );           % both alpha and Cn_L are used for scaling
                %value = (s.alpha) * ( s.Cn_L * ( 2 * s.H{el}(:) .* normC ) );           % both alpha and Cn_L are used for scaling
                value = ( 2 * s.H{el}(:) .* normC );           % both alpha and Cn_L are used for scaling
                
                kappaField.setValue(el,value);
            end

            % Ensure the alpha values are computed for the field
            kappaField.computeAlpha;
            
            out = kappaField;
        end
        
        function [st] = getSurfaceTension(s,mesh)
            % Function to compute the surface tension term as required for
            % the 3 staged NS equations. This term is the divergence of the
            % surface tension term used in the single staged NS equations.
            
            if (nargin==1)
                mesh = s.mesh;
            end
                        
            %% Step A : Initialize/update the kappa Field
            kappaField = FieldCollection.get('kappa');
            
            if (~isa(kappaField,'Field'))
                fieldID  = 6;
                activate = true;
                periodic = [ false false  ;...
                             false false ];
                kappaField = Field(s.mesh,fieldID,'kappa',periodic);
                FieldCollection.add( kappaField );
                kappaField.addSubField(FieldType.Current,activate,[],[],[],true);
            else
                % if kappaField exists, verify if the mesh has been updated
                kappaField.meshUpdate;
            end
            
            %save(['getSurfaceTension.' int2str(System.rank) '.mat']);

            % Load fields to be used
            C = s.fields{1};
            
            % Compute the mean curvature
            s.computeH2_av(mesh,1);

            % Compute kappa and store as a field
            for e=mesh.eRange
                element = mesh.getElement(e);
                el      = element.localID;

                dCkdx   = C.x2(e,mesh);
                dCkdy   = C.y2(e,mesh);
                normC   = sqrt( dCkdx.^2 + dCkdy.^2 );

%                 value = 2 * s.H{el}(:); %.* normC ) );           % both alpha and Cn_L are used for scaling
                %value = (s.alpha) * ( s.Cn_L * ( 2 * s.H{el}(:) .* normC ) );           % both alpha and Cn_L are used for scaling
                value = ( 2 * s.H{el}(:) .* normC );           % both alpha and Cn_L are used for scaling
                
                kappaField.setValue(el,value);
            end

            % Ensure the alpha values are computed for the field
            kappaField.computeAlpha;

            %% Step B : Compute the surface tension term for a staged NS
            st = cell(numel(s.H),1);
            
            % Compute the mean for Cxx
            C.xx2_av_init;
            C.yy2_av_init;
            
            for e=mesh.eRange
                element = mesh.getElement(e);
                el      = element.localID;
                
                Cx  = C.x2(e);
                Cy  = C.y2(e);
                Cxx = C.xx2_mean(e);
                Cyy = C.yy2_mean(e);
                
                k   = kappaField.val2(e);
                kx  = kappaField.x2(e);
                ky  = kappaField.y2(e);
                
                % By setting kappa = 2*H*|grad(C)|, the surface tension
                % contribution is given by: 
                %   st = div( kappa * grad(C) )
                %      = ( kappa_x * cx + kappa_y * cy ) + kappa * Lapl(C)
                st{el} = kx.*Cx + ky.*Cy + k.*(Cxx+Cyy);
            end
        end
        
        function [ssfx,ssfy] = getSSF(s,mesh)
            if (nargin==1)
                mesh = s.mesh;
            end
            
            s.computeH2_av(mesh,1);
            
            ssfx = cell(numel(s.H),1);
            ssfy = cell(numel(s.H),1);
            
            C   = s.fields{1};
            Omg = s.fields{2};

            % Compute the mean for Cxx
            C.xx2_av_init;
            C.yy2_av_init;

            v1 = [0.5;ones(6,1);0.5];
            scaler = kron(v1,v1);
            
            % Mean curvature correction
            for e=mesh.eRange
                element = mesh.getElement(e);
                el = element.localID;

                Ck      = C.val2(e,mesh);
                Omgk    = Omg.val2(e,mesh);
                dCkdx   = C.x2(e,mesh);
                dCkdy   = C.y2(e,mesh);
                %Cxx     = C.xx2(e,mesh);
                %Cyy     = C.yy2(e,mesh);

                Cxx     = C.xx2_mean(e);
                Cyy     = C.yy2_mean(e);

                gradC   = sqrt( dCkdx.^2 + dCkdy.^2 );

%                         kxx = 2 * (s.alpha) * ( 0*Omgk + 1*s.Cn_L * ( 2 * s.H{el}(:) .* gradC ) ) .* Cxx;
%                         kyy = 2 * (s.alpha) * ( 0*Omgk + 1*s.Cn_L * ( 2 * s.H{el}(:) .* gradC ) ) .* Cyy;
                kxx = (s.alpha) *( s.Cn_L * ( 2 * s.H{el}(:) ) ) .* Cxx;
                kyy = (s.alpha) *( s.Cn_L * ( 2 * s.H{el}(:) ) ) .* Cyy;

%                         kxx = Cxx;
%                         kyy = Cyy;

                %ssf{el} = kxx + kyy;

                ssfx{el} = reshape(2.0*(Ck>=0.5),element.finite.qSize);
                ssfy{el} = reshape(2.0*(Ck>=0.5),element.finite.qSize);

                imax = element.finite.qSize(1);
                jmax = element.finite.qSize(2);

                for j=1:jmax
                    for i=1:imax
                        if (i<imax)
                            if ssfx{el}(i,j)<=0 && ssfx{el}(i+1,j)>0
                                ssfx{el}(i  ,j)=-1;
                                ssfx{el}(i+1,j)= 1;
                            end
                        end
                        if j<jmax
                            if ssfy{el}(i,j)<=0 && ssfy{el}(i,j+1)>0
                                ssfy{el}(i,j  )=-1;
                                ssfy{el}(i,j+1)= 1;
                            end
                        end
                        if (i>1)
                            if ssfx{el}(i,j)<=0 && ssfx{el}(i-1,j)>0
                                ssfx{el}(i  ,j)=-1;
                                ssfx{el}(i-1,j)= 1;
                            end
                        end
                        if j>1
                            if ssfy{el}(i,j)<=0 && ssfy{el}(i,j-1)>0
                                ssfy{el}(i,j  )=-1;
                                ssfy{el}(i,j-1)= 1;
                            end
                        end
                    end
                end

                ssfx{el}( ssfx{el}==2 ) = 0;
                ssfy{el}( ssfy{el}==2 ) = 0;
                
%                 ssf{el} = -scaler .* ssf{el}(:);
                ssfx{el} = -ssfx{el}(:);
                ssfy{el} = -ssfy{el}(:);
            end

            
        end
        
        
        function [kx,ky,kz] = getCurvature_3D(s,mesh)
            if (nargin==1)
                mesh = s.mesh;
            end
            
            s.computeH2_3D(mesh,1);
            
            kx = cell(numel(s.H),1);
            ky = cell(numel(s.H),1);
            kz = cell(numel(s.H),1);
            
            C   = s.fields{1};
            Omg = s.fields{2};
            
%             selected = s.correctionMethod;
            
            selected = 1;
            
            switch selected
                case 1
                    % Mean curvature correction
                    for e=mesh.eRange
                        element = mesh.getElement(e);
                        el = element.localID;

                        Omgk    = Omg.val2(e,mesh);
                        dCkdx   = C.x2(e,mesh);
                        dCkdy   = C.y2(e,mesh);
                        dCkdz   = C.z2(e,mesh);
                        
                        gradC   = sqrt( dCkdx.^2 + dCkdy.^2 + dCkdz.^2 );
                        
                        kx{el} = (s.alpha) * ( 0*Omgk + 1*s.Cn_L * ( 2 * s.H{el}(:) .* gradC ) ) .* dCkdx;
                        ky{el} = (s.alpha) * ( 0*Omgk + 1*s.Cn_L * ( 2 * s.H{el}(:) .* gradC ) ) .* dCkdy;
                        kz{el} = (s.alpha) * ( 0*Omgk + 1*s.Cn_L * ( 2 * s.H{el}(:) .* gradC ) ) .* dCkdz;
                    end
                    
%                 case 2
%                     % Curvature from : -alpha * (C-1/2) * grad( omega )
%                     for e=mesh.eRange
%                         element = mesh.getElement(e);
%                         el = element.localID;
% 
%                         Ck        = C.val2(e,mesh);
%                         dOmgkdx   = Omg.x2(e,mesh);
%                         dOmgkdy   = Omg.y2(e,mesh);
%                         
%                         kx{el} = -s.alpha * (Ck-0.5).*dOmgkdx;
%                         ky{el} = -s.alpha * (Ck-0.5).*dOmgkdy;
%                     end
%                     
%                 otherwise
%                     % Any other correction
%                     for e=mesh.eRange
%                         element = mesh.getElement(e);
%                         el = element.localID;
% 
%                         Omgk    = Omg.val2(e,mesh);
%                         dCkdx   = C.x2(e,mesh);
%                         dCkdy   = C.y2(e,mesh);
%                         
%                         gradC   = sqrt( dCkdx.^2 + dCkdy.^2 );
%                         
%                         kx{el} = s.alpha * Omgk.*dCkdx;
%                         ky{el} = s.alpha * Omgk.*dCkdy;
%                         %kx{el} = (s.alpha) * ( Omgk + s.Cn_L^2 * ( 2 * s.H{el}(:) .* gradC ) ) .* dCkdx;
%                         %ky{el} = (s.alpha) * ( Omgk + s.Cn_L^2 * ( 2 * s.H{el}(:) .* gradC ) ) .* dCkdy;
%                     end
            end
        end
        
        function [kxx,kyy,kzz] = getCurvature2_3D(s,mesh)
            if (nargin==1)
                mesh = s.mesh;
            end
            
            s.computeH2_3D(mesh,1);
            
            kxx = cell(numel(s.H),1);
            kyy = cell(numel(s.H),1);
            kzz = cell(numel(s.H),1);
            
            C   = s.fields{1};
            Omg = s.fields{2};
            
%             selected = s.correctionMethod;
            
            selected = 1;
            
            switch selected
                case 1
                    % Mean curvature correction
                    for e=mesh.eRange
                        element = mesh.getElement(e);
                        el = element.localID;

                        Omgk  = Omg.val2(e,mesh);
                        dCkdx = C.x2(e,mesh);
                        dCkdy = C.y2(e,mesh);
                        dCkdz = C.z2(e,mesh);
                        cxx   = C.xx2(e,mesh);
                        cyy   = C.yy2(e,mesh);
                        czz   = C.zz2(e,mesh);
                        
                        gradC   = sqrt( dCkdx.^2 + dCkdy.^2 + dCkdz.^2 );
                        
                        kxx{el} = (s.alpha) * ( 0*Omgk + 1*s.Cn_L * ( 2 * s.H{el}(:) .* gradC ) ) .* cxx;
                        kyy{el} = (s.alpha) * ( 0*Omgk + 1*s.Cn_L * ( 2 * s.H{el}(:) .* gradC ) ) .* cyy;
                        kzz{el} = (s.alpha) * ( 0*Omgk + 1*s.Cn_L * ( 2 * s.H{el}(:) .* gradC ) ) .* czz;
                    end
            end
        end
        
        
        function [dfr] = getDiffusiveFlowRate(s,mesh)
            if nargin==1
                mesh = s.mesh;
            end
            
            dfr = cell(numel(mesh.eRange),1);
            Omg = s.fields{2};
            
            for e=mesh.eRange
                element = mesh.getElement(e);
                el = element.localID;
                dfr{el} = 1/s.Pe * (s.Cn/s.Cn_L)*( Omg.xx2(e,mesh) + Omg.yy2(e,mesh) );
            end
        end
        
        % Returns the rhs of the phase-field equation (scalar value in each
        % quadrature point)
        function [mf] = getMassFlux(s,mesh)
            if nargin==1
                mesh = s.mesh;
            end
            
            mf  = cell(numel(mesh.eRange),1);
            C   = s.fields{1};
            Omg = s.fields{2};
            
            for e=mesh.eRange
                element = mesh.getElement(e);
                el = element.localID;

                Omgk    = Omg.val2(e,mesh);
                dCkdx   = C.x2(e,mesh);
                dCkdy   = C.y2(e,mesh);
                Cxx     = C.xx2(e,mesh);
                Cxy     = C.xy2(e,mesh);
                Cyy     = C.yy2(e,mesh);
                
                normC2  = sqrt( dCkdx.^2 + dCkdy.^2 );
                
                %mf{el} = -1/s.We * s.alpha .* Omgk + 1/s.Pe * (dCkdx.^2 .* Cxx + 2*dCkdx.*dCkdy.*Cxy + dCkdy.^2.*Cyy) ./ normC2;
                mf{el} = -1/s.We * s.alpha .* Omgk + 1/s.Pe * (Cxx + Cyy);
            end
        end
        
        function plotAll(s,fig,~,mesh)
            verbose = false;
            
            if nargin==3
                mesh = s.mesh;
            end
            
            %screensize = get( groot, 'Screensize' );
            %sizex = 1600;
            %sizey = 478;
            %set(fig,'position',[0.5*(screensize(3)-sizex) 0.5*(screensize(4)-sizey) sizex sizey]); 
            
            rows = 1;
            cols = s.Nvar+1;
            
            s.createInterface;

            
            defaultProps = {'FaceColor','interp','edgecolor','none'};
            %shading interp
                        
            %% Get the meshgrid for each element
            [xGrids,yGrids] = mesh.getMeshgrids;
%             
%             for e=1:mesh.numLocalElements
%                 element = mesh.getLocalElement(e);
%                 [X{e},Y{e}] = meshgrid(element.xelem,element.yelem);
%             end            
%             if (verbose)
%                 fprintf('Total time meshgrid   : %f\n',toc-t1)
%             end
            
            %% Extract interface     
%             t1          = toc;
%             var         = Variable.c;
%             dataVar     = data{var};
                        
%             C             = cell(mesh.numLocalElements,1);
%             s.interface.x = cell(mesh.numLocalElements,1);
%             s.interface.y = cell(mesh.numLocalElements,1);
%             %interface.z = cell(mesh.numLocalElements,1);
%             
%             % Find the contour where the C surface is equal to the
%             % requested interface value. Next, extract the x- and y-locations 
%             % from the contour matrix C. Interpolate on the C surface
%             % to find z-locations for the intersection. Finally plot the
%             % line with some properties.
%             for e=1:mesh.numLocalElements
%                 element = mesh.getLocalElement(e);
%                 C{e} = contourc(element.xelem,element.yelem,dataVar{e},[s.interface.value s.interface.value]);
%                 if (~isempty(C{e}))
% %                     s.interface.x{e} = C{e}(1, 2:1+C{e}(2,1));
% %                     s.interface.y{e} = C{e}(2, 2:1+C{e}(2,1));
%                     s.interface.x{e} = C{e}(1, 2:end);
%                     s.interface.y{e} = C{e}(2, 2:end);
%                     if C{e}(2,1)~=size(C{e},2)-1
%                        s.interface.x{e}(:, C{e}(2,1)+1 ) = [];
%                        s.interface.y{e}(:, C{e}(2,1)+1 ) = [];
%                        s.interface.x{e}(:, 1:C{e}(2,1) ) = fliplr( s.interface.x{e}(:, 1:C{e}(2,1) ) );
%                        s.interface.y{e}(:, 1:C{e}(2,1) ) = fliplr( s.interface.y{e}(:, 1:C{e}(2,1) ) );
%                     end
%                     %interface.z{e} = interp2(X{e}(1,:)', Y{e}(:,1), dataVar{e}, interface.x{e}, interface.y{e});
%                     %line(s.interface.x{e}, s.interface.y{e}, repmat(s.interface.value,size(s.interface.x{e})), s.interface.props{:});
%                 end
%             end
%             if (verbose)
%                 fprintf('Total time interface : %f\n',toc-t1)
%             end
            
            %% C plot
            position    = 1;
            sub         = subplot(rows,cols,position);
            cla(sub);
            s.plotVar(Variable.c);
            
%             t1          = toc;
%             var         = Variable.c;
%             dataVar     = data{var};
%             minC        = min( cellfun(@(x) min(x(:)),dataVar) );
%             maxC        = max( cellfun(@(x) max(x(:)),dataVar) );
%             
%             % plot settings
%             delete(subplot(rows,cols,var))
%             subplot(rows,cols,var)
%             xlim([mesh.x0 mesh.x1])
%             ylim([mesh.y0 mesh.y1])
%             pbaspect([pbx pby 1])
%             view([0,90])
%             grid off;
%             hold on;
%             
%             for e=1:mesh.numLocalElements
%                 surface(X{e},Y{e},dataVar{e},defaultProps{:});
%                 if s.interface.active && ~isempty(s.interface.x{e})
%                     line(s.interface.x{e}, s.interface.y{e}, repmat(maxC,size(s.interface.x{e})), s.interface.props{:});
%                 end
%             end
%             if (verbose)
%                 fprintf('Total time C   : %f\n',toc-t1)
%             end

            %% Omega plot
            position    = 2;
            sub         = subplot(rows,cols,position);
            cla(sub);
            s.plotVar(Variable.omg);
            
            
%             t1          = toc; 
%             var         = Variable.omg;
%             dataVar     = data{var};
%             maxOmega    = max( cellfun(@(x) max(x(:)),dataVar) );
%             
%             delete(subplot(rows,cols,var))
%             subplot(rows,cols,var)            
%             xlim([mesh.x0 mesh.x1])
%             ylim([mesh.y0 mesh.y1])
%             grid off;
%             pbaspect([pbx pby 1])
%             view([0,90])
%             hold on;
%             
%             for e=1:mesh.numLocalElements
%                 surface(X{e},Y{e},dataVar{e},defaultProps{:});                
%                 if interface.active && ~isempty(interface.x{e})
%                     line(interface.x{e}, interface.y{e}, repmat(maxOmega,size(interface.x{e})),interface.props{:});
%                 end
%             end
%             if (verbose)
%                 fprintf('Total time Omg : %f\n',toc-t1)
%             end
            
            %% Surface tension plot
            t1      = toc;
            var     = 3;
            delete(subplot(rows,cols,var))
            s.plotSurfaceTension( subplot(rows,cols,var), s.interface );
            if (verbose)
                fprintf('Total time ST  : %f\n',toc-t1)
            end
            
%             dataVar = data{var};
%             for e=1:s.mesh.getNumLocalElements
%                 element = s.mesh.getLocalElement(e);
%                 surf(element.xelem,element.yelem,dataVar{e});
%                 hold on;
%             end
%             shading interp
%             xlim([s.mesh.x0 s.mesh.x1])
%             ylim([s.mesh.y0 s.mesh.y1])
%             grid off;
%             pbaspect([pbx pby 1])
%             view([0,90])
            
%             % c-plot
%             delete(subplot(2,1,1))
%             subplot(2,1,1)
%             c = data{1};
%             for e=1:s.mesh.getNumLocalElements
%                 element = s.mesh.getLocalElement(e);
%                 surf(element.xelem,element.yelem,c{e});
%                 hold on;
%             end
%             xlim([s.mesh.x0 s.mesh.x1])
%             view([30,30])
%             %view([0,90])
            
%             % omg-plot
%             delete(subplot(2,1,2))
%             subplot(2,1,2)
%             omg = data{2};
%             for e=1:s.mesh.getNumLocalElements
%                 element = s.mesh.getLocalElement(e);
%                 surf(element.xelem,element.yelem,omg{e});
%                 hold on;
%             end
%             view([30,30])
%             %view([0,90])
            
%             % P-plot
%             delete(subplot(3,2,5))
%             subplot(3,2,5)
%             p = data{3};
%             for e=1:s.mesh.getNumLocalElements
%                 element = s.mesh.getLocalElement(e);
%                 surf(element.xelem,element.yelem,p{e});
%                 hold on;
%             end
%             view([30,30])
%             %view([0,90])
%             
%             % U-velocity at x=0.5
%             delete(subplot(3,2,2))
%             subplot(3,2,2)
%             for e=1:s.mesh.getNumLocalElements
%                 element = s.mesh.getLocalElement(e);
%                 z=interp2(element.xelem,element.yelem,u{e},0.5,element.yelem);
%                 plot(element.yelem,z,'k');
%                 hold on;
%             end
%             xlim([0 1])
%             view([90,-90])
%             
%             % V-velocity at x=0.5
%             delete(subplot(3,2,4))
%             subplot(3,2,4);
%             for e=1:s.mesh.getNumLocalElements
%                 element = s.mesh.getLocalElement(e);
%                 z=interp2(element.xelem,element.yelem,v{e},0.5,element.yelem);
%                 plot(element.yelem,z,'k');
%                 hold on;
%             end
%             ylim([-1e-3 1e-3])
%             view([90,-90]) 

            %% Position subfigures
            d = 0.01;
            
            width  = (1-(cols+1)*2*d)/cols;
            height = 1;
            
            s1 = subplot(rows,cols,1);
            set(s1,'position',[2*d 2*d width height])
            
            s2 = subplot(rows,cols,2);
            set(s2,'position',[4*d+width 0.02 width height])
            
            s3 = subplot(rows,cols,3);
            set(s3,'position',[6*d+2*width 0.02 width height])
        end
        
        % Main function which calls other functions
        function writeValues(s)
            filename = [ System.settings.outp.directory s.name '-' System.settings.outp.valueFilename '.txt' ];
            
            perimeter = s.computePerimeter2D;
            s.computeMass(System.getIntegrationDisc);
            s.computeCenterOfMass(false);
            
            s.computeCenterOfMassVelocity(false);
            
            if (System.rank==0)
                if (System.settings.time.current==0)
                    header = {'iter','time','totalMass','massLossTotal','cmx_total','cmy_total','cmu_total','cmv_total',...
                                            'enclosedMass','massLossEnclosed','cmx_enclosed','cmy_enclosed','cmu_enclosed','cmv_enclosed',...
                                            'perimeter'};
                    System.writeHeader(filename,header)
                end

                values = zeros(15,1);
                values(1)  = System.settings.time.iter; 
                values(2)  = System.settings.time.current;
                values(3)  = s.massTotal(end);
                values(4)  = s.massLossTotal;
                values(5)  = s.cm_total(end,1);
                values(6)  = s.cm_total(end,2);
                values(7)  = s.cmv_total(end,1);
                values(8)  = s.cmv_total(end,2);
                values(9)  = s.massEnclosed(end);
                values(10) = s.massLossEnclosed;
            	values(11) = s.cm_enclosed(end,1);
                values(12) = s.cm_enclosed(end,2);
                values(13) = s.cmv_enclosed(end,1);
                values(14) = s.cmv_enclosed(end,2);
                values(15) = perimeter;

                System.appendToFile(filename,values)
            end
        end
        
        function out = computeOmega(s)
            % Fenergy = (3*Ck.^2 - 3*Ck + 0.5).*H - s.Cn^2 * ( Nabla2 );
            clf;
                        
            layer = 1;
            
            alphaC = s.fields{1}.active.alpha;
            
            out = cell( numel(s.mesh.eRange),1 );
            
            for e=s.mesh.eRange
                element = s.mesh.getElement(e);
                el = element.localID;
                
                %Ck = C.val(FieldType.Current,e);
                Ck = s.fields{1}.value{el}(:);
                Ze = s.mesh.Z{e};
                Nabla2 = element.getDxx + element.getDyy;
                data = (Ck.^3 - 3/2*Ck.^2 + 0.5.*Ck) - s.Cn_L^2 * ( Nabla2 * Ze * alphaC{el}(:) );
                data = s.alpha/s.Cn * data;
                
                out{el} = reshape(data,element.finite.qSize);
                
                %[X,Y] = element.getNodes;
                %if (layer==1)
                %    plotData = data(:,:,end);
                %elseif (layer==0)
                %    plotData = data(:,:,1);
                %end
                %surf(X(:,:,:,end),Y(:,:,:,end),plotData,'FaceColor','interp');
                %hold on;
            end
            
%             x0 = s.mesh.X(1,1);
%             x1 = s.mesh.X(2,1);
%             y0 = s.mesh.X(1,2);
%             y1 = s.mesh.X(2,2);
%             
%             pbx = x1 - x0;
%             pby = y1 - y0;
%             pbaspect([pbx pby 1])
%                
%             colorbar;
%             view([25,25]);
        end
        
        % Function to compute the perimeter of the C=0.5 patch
        function out = computePerimeter2D(s)
            
            length = zeros(2,1);
            
            for e=s.mesh.eRange
                
                % Set the element
                element = s.mesh.getElement(e);
                
                [x,y,z,t] = element.getNodes();
                
                x = squeeze(x);
                y = squeeze(y);
                t = squeeze(t);
                
                c = s.fields{1};
                
                try
                    cValues = reshape( c.val(FieldType.Current,e), size(x) );
                catch
                    disp('')
                end
                
                if ( s.timeMethod == TimeMethod.SpaceTime )
                    lEnd = 2;
                else
                    lEnd = 1;
                end
                
                for l=1:lEnd
                    
                    if l==1
                        pos = 1;
                    else
                        pos = size(x,3);
                    end
                    
                    % 3D
                    %p = patch(isosurface(x(:,:,l),y(:,:,l),t(:,:,l),cValues(:,:,l),0.5));
                    %verts = get(p, 'Vertices');
                    %faces = get(p, 'Faces');
                    
                    % 2D
                    % create a contour object for C = 0.5
                    %p = contour(x(:,:,pos),y(:,:,pos),cValues(:,:,pos),[0.5 0.5]);
                    
                    p = contourc(x(:,1,pos),y(1,:,pos),cValues(:,:,pos)',[0.5 0.5]);
                    
                    % use the contour object to compute the linear line length 
                    % for each segment (p(1,1)==0.5 and p(2,1)=number of segments)
                    if (~isempty(p))
                        diff = zeros(3,p(2,1)-1);
                        for n=1:p(2,1)-1
                            diff(1,n) = p(1,n+2) - p(1,n+1);
                            diff(2,n) = p(2,n+2) - p(2,n+1);
                            diff(3,n) = sqrt(diff(1,n)^2 + diff(2,n)^2);
                        end
                        length(l) = length(l) + sum(diff(3,:));
                    end
                end
            end
            
            
            
            length = NMPI.Allreduce(length,numel(length),'+','computePerimeter2D');
            
            out = length(lEnd);
            
            %fprintf('perimeter start / end : %e / %e\n',length)
                
%             [x,y,z,v] = flow;
%             p = patch(isosurface(x,y,z,v,-3));
%             isonormals(x,y,z,v,p)
%             set(p,'FaceColor','red','EdgeColor','none');
%             daspect([1 1 1])
%             view(3); 
%             camlight 
%             lighting gouraud
%             verts = get(p, 'Vertices');
%             faces = get(p, 'Faces');
%             a = verts(faces(:, 2), :) - verts(faces(:, 1), :);
%             b = verts(faces(:, 3), :) - verts(faces(:, 1), :);
%             c = cross(a, b, 2);
%             area = 1/2 * sum(sqrt(sum(c.^2, 2)));
%             fprintf('\nThe surface area is %f\n\n', area);
        end
        
        % Function to compute the total and enclosed mass.
        % The total mass is a simple summation over all elements, and
        % should remain constant in time. The enclosed mass is the mass
        % where C >= 0.5, and its value can change in time.
        function computeMass(s,finiteElement)
            if (nargin==1)
                finiteElement = FECollection.get(1);
            end
            
            if (finiteElement.spaceTime)
                Qt = finiteElement.Q( end );
            else
                Qt = 1;
            end
            
            % Reset the total/enclosed mass for each time level
            massTotal    = zeros(Qt,1);
            massEnclosed = zeros(Qt,1);
            check        = zeros(Qt,1);
            
            for e=s.mesh.eRange
                
                % Set the element
                element = s.mesh.getElement(e);
                
                %[X,Y,Z,T] = element.getNodes;
                
                % Construct the total and enclosed c arrays
                cTotal    = s.fields{1}.getValueInterpolated(e,finiteElement,s.mesh);
                
                cEnclosed = cTotal;
                cEnclosed(cEnclosed< 0.5) = 0;
                cEnclosed(cEnclosed>=0.5) = 1;
                
                % Set the integration weights and Jacobian for 2D spatial planes
                w = finiteElement.Wspatial;
                J = element.JSpatial;
      
                if (finiteElement.spaceTime)
                    for t=1:Qt
                        sliceValues     = cTotal(:,:,t);
                        massTotal(t)    = massTotal(t)    + sliceValues(:)' * w * J;

                        sliceValues     = cEnclosed(:,:,t);
                        massEnclosed(t) = massEnclosed(t) + sliceValues(:)' * w * J;

                        %sliceValues     = ones( size(cEnclosed(:,:,t)) );
                        %check(t)        = check(t) + sliceValues(:)' * w * J;
                    end
                else
                    %if element.dimension==2
                        sliceValues     = cTotal(:);
                        massTotal       = massTotal + sliceValues(:)' * w * J;

                        sliceValues     = cEnclosed(:);
                        massEnclosed    = massEnclosed + sliceValues(:)' * w * J;
                        
%                     elseif (element.dimension==3)
%                         sliceValues     = cTotal(:);
%                         massTotal       = massTotal + sliceValues(:)' * w * J;
% 
%                         sliceValues     = cEnclosed(:);
%                         massEnclosed    = massEnclosed + sliceValues(:)' * w * J;
%                     end
                end
            end
            
            if (System.settings.anal.debug)
                fprintf('Before NMPI_Allreduce in CahnHilliard.computeMass\n')
            end
            
            s.massTotal     = NMPI.Allreduce(massTotal   ,Qt,'+','massTotal');
            s.massEnclosed  = NMPI.Allreduce(massEnclosed,Qt,'+','massEnclosed');
            checkGlobal     = NMPI.Allreduce(check,Qt,'+','checkGlobal (mass)');
            
            s.massLossTotal     = s.massTotal(1)   -s.massTotal(end);
            s.massLossEnclosed  = s.massEnclosed(1)-s.massEnclosed(end);
            checkLoss           = check(1)-check(end);
            
            %if (NMPI.instance.rank==0)
            %    fprintf('Total mass (begin/end)    : %f / %f (mass loss = %8.2e)\n',s.massTotal(1),s.massTotal(end),s.massLossTotal);
            %    fprintf('Enclosed mass (begin/end) : %f / %f (mass loss = %8.2e)\n',s.massEnclosed(1),s.massEnclosed(end),s.massLossEnclosed);
            %    %fprintf('Enclosed mass (begin/end) : %f / %f (mass loss = %8.2e)\n',check(1),check(end),checkLoss);
            %end
        end
        
        function computeCenterOfMass(s,varargin)
            switch System.settings.mesh.sdim
                case 2
                    s.computeCenterOfMass_2D(varargin{:});
                case 3
                    s.computeCenterOfMass_3D(varargin{:});
            end
        end
        
        function computeCenterOfMassVelocity(s,varargin)
            switch System.settings.mesh.sdim
                case 2
                    s.computeCenterOfMassVelocity_2D(varargin{:});
                case 3
                    s.computeCenterOfMassVelocity_3D(varargin{:});
            end
        end
        
        % The center of mass (CoM) can be determined by:
        %   CoM = sum( C * distance ) / sum( C )
        % for each spatial direction. Here 'distance' is a value wrt the
        % origin of the 
        function computeCenterOfMass_2D(s,computeMass,Q)
            if (nargin==2)
                disc = System.getIntegrationDisc;
            else
                error('unsupported option')
                %disc = LSQdisc_5(System.settings.stde.Pn,Q,System.settings.stde.C,System.settings.stde.sDim,System.settings.time.spaceTime);
            end
            
            Qs = disc.Q(1)*disc.Q(2);       % Spatial Q
            
            if (System.settings.time.method==TimeMethod.SpaceTime)
                Qt = disc.Q(3);                 % Temporal Q
            else
                Qt = 1;
            end
            
            sDim = 2;
            
            % Reset the cm(x,y) for each time level
            cm_total    = zeros(Qt,sDim);
            cm_enclosed = zeros(Qt,sDim);

            for e=s.mesh.eRange
                % Set the element & locations of the quadrature points
                element     = s.mesh.getElement(e);
                locations   = element.getLocations(disc);
                
                if (System.settings.time.method==TimeMethod.SpaceTime)
                    xLoc = superkron( ones(numel(locations{3}),1), ones(numel(locations{2}),1), locations{1} );
                    yLoc = superkron( ones(numel(locations{3}),1), locations{2}, ones(numel(locations{1}),1) );
                else
                    xLoc = superkron( ones(numel(locations{2}),1), locations{1} );
                    yLoc = superkron( locations{2}, ones(numel(locations{1}),1) );
                end
                
                % Get the values for the modified integration order
                cTotal = s.fields{1}.getValueInterpolated( e ,disc, s.mesh);
                
                cEnclosed = cTotal; 
                cEnclosed(cEnclosed<0.5)  = 0;
                cEnclosed(cEnclosed>=0.5) = 1;
                
                % Set the integration weights and Jacobian for 2D spatial planes
                w = disc.Wspatial;
                J = element.J(1)*element.J(2);
      
                % Compute the mass * distance
                cmx_total = cTotal(:) .* xLoc; cmx_total = reshape(cmx_total,[Qs,Qt]);
                cmy_total = cTotal(:) .* yLoc; cmy_total = reshape(cmy_total,[Qs,Qt]);
                
                % Compute the mass * distance
                cmx_enclosed = cEnclosed(:) .* xLoc; cmx_enclosed = reshape(cmx_enclosed,[Qs,Qt]);
                cmy_enclosed = cEnclosed(:) .* yLoc; cmy_enclosed = reshape(cmy_enclosed,[Qs,Qt]);
                
                % Integrate the values for each element and sum to get the
                % total values on this rank
                for t=1:Qt
                    cm_total(t,1) = cm_total(t,1) + cmx_total(:,t)' * w * J;               % x-direction
                    cm_total(t,2) = cm_total(t,2) + cmy_total(:,t)' * w * J;               % y-direction
                    
                    cm_enclosed(t,1) = cm_enclosed(t,1) + cmx_enclosed(:,t)' * w * J;               % x-direction
                    cm_enclosed(t,2) = cm_enclosed(t,2) + cmy_enclosed(:,t)' * w * J;               % y-direction
                end
            end
            
            if (System.settings.anal.debug)
                fprintf('Before NMPI_Allreduce in CahnHilliard.computeCenterOfMass\n')
            end
            
            cm_total = NMPI.Allreduce(cm_total,numel(cm_total),'+','cm_total');
            cm_total = reshape(cm_total,[Qt,sDim]);
            
            cm_enclosed = NMPI.Allreduce(cm_enclosed,numel(cm_enclosed),'+','cm_enclosed');
            cm_enclosed = reshape(cm_enclosed,[Qt,sDim]);            
            
            % Compute the mass for each time plane
            if (computeMass)
                s.computeMass(disc);
            end
            
            % Finally compute the cm(x,y) for each time plane
            s.cm_total = cm_total ./ s.massTotal;
            
            % Finally compute the cm(x,y) for each time plane
            s.cm_enclosed = cm_enclosed ./ s.massEnclosed;
            
%             if (NMPI.instance.rank==0)
%                 %fprintf('Total mass : %f (effective radius : %f)\n',s.mass,sqrt(s.mass/pi))
%                 fprintf('Center of mass (begin/end) : (%f,%f / %f,%f) -- delta : (%8.2e,%8.2e) \n',s.cm(1,1),s.cm(1,2),s.cm(end,1),s.cm(end,2),s.cm(end,1)-s.cm(1,1),s.cm(end,2)-s.cm(1,2))
%             end
        end
        
        function computeCenterOfMass_3D(s,computeMass,Q)
            if (nargin==2)
                disc = System.getIntegrationDisc;
            else
                error('unsupported option')
            end
            
            sDim = 3;
            
            if (System.settings.mesh.sdim~=sDim)
                error('Please set System.settings.mesh.sdim == %d\n',sDim)
            end
            
            Qs = prod(disc.Q(1:sDim));                                      % Spatial Q
            if (System.settings.time.method==TimeMethod.SpaceTime)
                Qt = disc.Q(sDim+1);                                        % Temporal Q
            else
                Qt = 1;
            end
            
            % Reset the cm(x,y,z) for each time level
            cm_total    = zeros(Qt,sDim);
            cm_enclosed = zeros(Qt,sDim);

            for e=s.mesh.eRange
                % Set the element & locations of the quadrature points
                element     = s.mesh.getElement(e);
                locations   = element.getLocations(disc);
                
                if (System.settings.time.method==TimeMethod.SpaceTime)
                    error('3D space + time is not supported yet')
                else
                    xLoc = superkron( ones(numel(locations{3}),1), ones(numel(locations{2}),1), locations{1} );
                    yLoc = superkron( ones(numel(locations{3}),1), locations{2}, ones(numel(locations{1}),1) );
                    zLoc = superkron( locations{3}, ones(numel(locations{2}),1), ones(numel(locations{1}),1) );
                end
                
                % Get the values for the modified integration order
                cTotal = s.fields{1}.getValueInterpolated( e ,disc, s.mesh);
                
                cEnclosed = cTotal; 
                cEnclosed(cEnclosed<0.5)  = 0;
                cEnclosed(cEnclosed>=0.5) = 1;
                
                % Set the integration weights and Jacobian for 2D spatial planes
                w = disc.Wspatial;
                J = element.J(1) * element.J(2) * element.J(3);
      
                % Compute the mass * distance
                cmx_total = cTotal(:) .* xLoc; cmx_total = reshape(cmx_total,[Qs,Qt]);
                cmy_total = cTotal(:) .* yLoc; cmy_total = reshape(cmy_total,[Qs,Qt]);
                cmz_total = cTotal(:) .* zLoc; cmz_total = reshape(cmz_total,[Qs,Qt]);
                
                % Compute the mass * distance
                cmx_enclosed = cEnclosed(:) .* xLoc; cmx_enclosed = reshape(cmx_enclosed,[Qs,Qt]);
                cmy_enclosed = cEnclosed(:) .* yLoc; cmy_enclosed = reshape(cmy_enclosed,[Qs,Qt]);
                cmz_enclosed = cEnclosed(:) .* zLoc; cmz_enclosed = reshape(cmz_enclosed,[Qs,Qt]);
                
                % Integrate the values for each element and sum to get the
                % total values on this rank
                for t=1:Qt
                    cm_total(t,1) = cm_total(t,1) + cmx_total(:,t)' * w * J;               % x-direction
                    cm_total(t,2) = cm_total(t,2) + cmy_total(:,t)' * w * J;               % y-direction
                    cm_total(t,3) = cm_total(t,3) + cmz_total(:,t)' * w * J;               % z-direction
                    
                    cm_enclosed(t,1) = cm_enclosed(t,1) + cmx_enclosed(:,t)' * w * J;               % x-direction
                    cm_enclosed(t,2) = cm_enclosed(t,2) + cmy_enclosed(:,t)' * w * J;               % y-direction
                    cm_enclosed(t,3) = cm_enclosed(t,3) + cmz_enclosed(:,t)' * w * J;               % z-direction
                end
            end
            
            if (System.settings.anal.debug)
                fprintf('Before NMPI_Allreduce in CahnHilliard.computeCenterOfMass\n')
            end
            
            cm_total = NMPI.Allreduce(cm_total,numel(cm_total),'+','cm_total');
            cm_total = reshape(cm_total,[Qt,sDim]);
            
            cm_enclosed = NMPI.Allreduce(cm_enclosed,numel(cm_enclosed),'+','cm_enclosed');
            cm_enclosed = reshape(cm_enclosed,[Qt,sDim]);            
            
            % Compute the mass for each time plane
            if (computeMass)
                s.computeMass(disc);
            end
            
            % Finally compute the cm(x,y) for each time plane
            s.cm_total = cm_total ./ s.massTotal;
            
            % Finally compute the cm(x,y) for each time plane
            s.cm_enclosed = cm_enclosed ./ s.massEnclosed;
            
%             if (NMPI.instance.rank==0)
%                 %fprintf('Total mass : %f (effective radius : %f)\n',s.mass,sqrt(s.mass/pi))
%                 fprintf('Center of mass (begin/end) : (%f,%f / %f,%f) -- delta : (%8.2e,%8.2e) \n',s.cm(1,1),s.cm(1,2),s.cm(end,1),s.cm(end,2),s.cm(end,1)-s.cm(1,1),s.cm(end,2)-s.cm(1,2))
%             end
        end
        
        % The center of mass velocity (CoM_vel) can be determined by:
        %   CoM_vel = sum( C * U ) / sum( C )
        % for each spatial direction. Here 'U' is the velocity vector
        function computeCenterOfMassVelocity_2D(s,varargin)
            
            [computeMass,overIntegration] = VerifyInput(varargin,{true,true});
            
            U = FieldCollection.get('u');
            V = FieldCollection.get('v');
            %save(['com1.' int2str(System.rank) '.mat']);
            
            if (U==U && V==V)
                remesh = ~isequal(U.mesh.Lv,s.mesh.Lv);

                if (remesh)
                    U.initSubField(FieldType.Interpolated,s.getMesh)
                    V.initSubField(FieldType.Interpolated,s.getMesh)
                end

                if (overIntegration)
                    disc = System.getIntegrationDisc;
                %else
                %    error('unsupported option')
                %    %disc = LSQdisc_5(System.settings.stde.Pn,Q,System.settings.stde.C,System.settings.stde.sDim,System.settings.time.spaceTime);
                end

                sDim = 2;
                count = 0;

                %maxV = 0;

                for e=s.mesh.eRange
                    count = count+1;

                    % Set the element & locations of the quadrature points
                    element     = s.mesh.getElement(e);

                    if (~overIntegration)
                        disc = element.finite;
                    end

                    Qs = disc.Q(1)*disc.Q(2);       % Spatial Q

                    if (System.settings.time.method==TimeMethod.SpaceTime)
                        Qt = disc.Q(3);                 % Temporal Q
                    else
                        Qt = 1;
                    end

                    if count==1
                        % Reset the cm(x,y) for each time level
                        cm_total    = zeros(Qt,sDim);
                        cm_enclosed = zeros(Qt,sDim);
                    end

                    if (remesh)
                        udata = U.getValueInterpolated(e,disc,s.mesh);
                        vdata = V.getValueInterpolated(e,disc,s.mesh);
                    else
                        udata = U.getValueInterpolated(e,disc);
                        vdata = V.getValueInterpolated(e,disc);
                    end

                    % Get the values for the modified integration order
                    cTotal = s.fields{1}.getValueInterpolated(e,disc);

                    cEnclosed = cTotal;
                    cEnclosed(cEnclosed<0.5)  = 0;
                    cEnclosed(cEnclosed>=0.5) = 1;

                    % Set the integration weights and Jacobian for 2D spatial planes
                    w = disc.Wspatial;
                    J = element.J(1)*element.J(2);

                    % Compute the mass * distance
                    cmx_total = cTotal(:) .* udata(:); cmx_total = reshape(cmx_total,[Qs,Qt]);
                    cmy_total = cTotal(:) .* vdata(:); cmy_total = reshape(cmy_total,[Qs,Qt]);

                    cmx_enclosed = cEnclosed(:) .* udata(:); cmx_enclosed = reshape(cmx_enclosed,[Qs,Qt]);
                    cmy_enclosed = cEnclosed(:) .* vdata(:); cmy_enclosed = reshape(cmy_enclosed,[Qs,Qt]);

                    %maxV = max(maxV,max(vdata(:)));

                    % Integrate the values for each element and sum to get the
                    % total values on this rank
                    for t=1:Qt
                        cm_total(t,1) = cm_total(t,1) + cmx_total(:,t)' * w * J;               % x-direction
                        cm_total(t,2) = cm_total(t,2) + cmy_total(:,t)' * w * J;               % y-direction

                        cm_enclosed(t,1) = cm_enclosed(t,1) + cmx_enclosed(:,t)' * w * J;               % x-direction
                        cm_enclosed(t,2) = cm_enclosed(t,2) + cmy_enclosed(:,t)' * w * J;               % y-direction
                    end
                end

                if (System.settings.anal.debug)
                    fprintf('Before NMPI_Allreduce in CahnHilliard.computeCenterOfMassVelocity\n')
                end

                cm_total = NMPI.Allreduce(cm_total,numel(cm_total),'+','cm_total');
                cm_total = reshape(cm_total,[Qt,sDim]);

                cm_enclosed = NMPI.Allreduce(cm_enclosed,numel(cm_enclosed),'+','cm_enclosed');
                cm_enclosed = reshape(cm_enclosed,[Qt,sDim]);

                % Compute the mass for each time plane
                if (computeMass)
                    s.computeMass(disc);
                end

%                 if (isfield(System.settings.phys,'bubbleVelocity'))
%                     goalVelocity = repmat(System.settings.phys.bubbleVelocity,size(cm_total,1),1);
%                 else
                    goalVelocity = 0;
%                 end

                % Finally compute the cm(x,y) for each time plane
                s.cmv_total     = cm_total    ./ s.massTotal    + goalVelocity;
                s.cmv_enclosed  = cm_enclosed ./ s.massEnclosed + goalVelocity;

            else
                s.cmv_total     = zeros( 1, System.settings.mesh.sdim );
                s.cmv_enclosed  = zeros( 1, System.settings.mesh.sdim );
            end
        end
        
        % The center of mass velocity (CoM_vel) can be determined by:
        %   CoM_vel = sum( C * U ) / sum( C )
        % for each spatial direction. Here 'U' is the velocity vector
        function computeCenterOfMassVelocity_3D(s,varargin)
            
            [computeMass,overIntegration] = VerifyInput(varargin,{true,true});
            
            U = FieldCollection.get('u');
            V = FieldCollection.get('v');
            W = FieldCollection.get('w');
            %save(['com1.' int2str(System.rank) '.mat']);
            
            if (U==U && V==V && W==W)
                remesh = ~isequal(U.mesh.Lv,s.mesh.Lv);

                if (remesh)
                    U.initSubField(FieldType.Interpolated,s.getMesh)
                    V.initSubField(FieldType.Interpolated,s.getMesh)
                    W.initSubField(FieldType.Interpolated,s.getMesh)
                end

                if (overIntegration)
                    disc = System.getIntegrationDisc;
                %else
                %    error('unsupported option')
                %    %disc = LSQdisc_5(System.settings.stde.Pn,Q,System.settings.stde.C,System.settings.stde.sDim,System.settings.time.spaceTime);
                end

                sDim = 3;
                count = 0;

                %maxV = 0;

                for e=s.mesh.eRange
                    count = count+1;

                    % Set the element & locations of the quadrature points
                    element     = s.mesh.getElement(e);

                    if (~overIntegration)
                        disc = element.finite;
                    end

                    Qs = disc.Q(1)*disc.Q(2)*disc.Q(3);       % Spatial Q

                    if (System.settings.time.method==TimeMethod.SpaceTime)
                        Qt = disc.Q(4);                 % Temporal Q
                    else
                        Qt = 1;
                    end

                    if count==1
                        % Reset the cm(x,y) for each time level
                        cm_total    = zeros(Qt,sDim);
                        cm_enclosed = zeros(Qt,sDim);
                    end

                    if (remesh)
                        udata = U.getValueInterpolated(e,disc,s.mesh);
                        vdata = V.getValueInterpolated(e,disc,s.mesh);
                        wdata = W.getValueInterpolated(e,disc,s.mesh);
                    else
                        udata = U.getValueInterpolated(e,disc);
                        vdata = V.getValueInterpolated(e,disc);
                        wdata = W.getValueInterpolated(e,disc);
                    end

                    % Get the values for the modified integration order
                    cTotal = s.fields{1}.getValueInterpolated(e,disc);

                    cEnclosed = cTotal;
                    cEnclosed(cEnclosed<0.5)  = 0;
                    cEnclosed(cEnclosed>=0.5) = 1;

                    % Set the integration weights and Jacobian for 2D spatial planes
                    w = disc.Wspatial;
                    J = element.J(1)*element.J(2)*element.J(3);

                    % Compute the mass * distance
                    cmx_total = cTotal(:) .* udata(:); cmx_total = reshape(cmx_total,[Qs,Qt]);
                    cmy_total = cTotal(:) .* vdata(:); cmy_total = reshape(cmy_total,[Qs,Qt]);
                    cmz_total = cTotal(:) .* wdata(:); cmz_total = reshape(cmz_total,[Qs,Qt]);

                    cmx_enclosed = cEnclosed(:) .* udata(:); cmx_enclosed = reshape(cmx_enclosed,[Qs,Qt]);
                    cmy_enclosed = cEnclosed(:) .* vdata(:); cmy_enclosed = reshape(cmy_enclosed,[Qs,Qt]);
                    cmz_enclosed = cEnclosed(:) .* wdata(:); cmz_enclosed = reshape(cmz_enclosed,[Qs,Qt]);

                    %maxV = max(maxV,max(vdata(:)));

                    % Integrate the values for each element and sum to get the
                    % total values on this rank
                    for t=1:Qt
                        cm_total(t,1) = cm_total(t,1) + cmx_total(:,t)' * w * J;               % x-direction
                        cm_total(t,2) = cm_total(t,2) + cmy_total(:,t)' * w * J;               % y-direction
                        cm_total(t,3) = cm_total(t,3) + cmy_total(:,t)' * w * J;               % z-direction

                        cm_enclosed(t,1) = cm_enclosed(t,1) + cmx_enclosed(:,t)' * w * J;               % x-direction
                        cm_enclosed(t,2) = cm_enclosed(t,2) + cmy_enclosed(:,t)' * w * J;               % y-direction
                        cm_enclosed(t,3) = cm_enclosed(t,3) + cmz_enclosed(:,t)' * w * J;               % z-direction
                    end
                end

                if (System.settings.anal.debug)
                    fprintf('Before NMPI_Allreduce in CahnHilliard.computeCenterOfMassVelocity\n')
                end

                cm_total = NMPI.Allreduce(cm_total,numel(cm_total),'+','cm_total');
                cm_total = reshape(cm_total,[Qt,sDim]);

                cm_enclosed = NMPI.Allreduce(cm_enclosed,numel(cm_enclosed),'+','cm_enclosed');
                cm_enclosed = reshape(cm_enclosed,[Qt,sDim]);

                % Compute the mass for each time plane
                if (computeMass)
                    s.computeMass(disc);
                end

                if (isfield(System.settings.phys,'bubbleVelocity'))
                    goalVelocity = repmat(System.settings.phys.bubbleVelocity,size(cm_total,1),1);
                else
                    goalVelocity = 0;
                end

                % Finally compute the cm(x,y) for each time plane
                s.cmv_total     = cm_total    ./ s.massTotal    + goalVelocity;
                s.cmv_enclosed  = cm_enclosed ./ s.massEnclosed + goalVelocity;

            else
                s.cmv_total     = zeros( 1, System.settings.mesh.sdim );
                s.cmv_enclosed  = zeros( 1, System.settings.mesh.sdim );
            end
        end
        
        
        function computeMeanOmega(s,disc)
            if (nargin==1)
                disc = s.getMesh.getElement(1).finite;
            end
            
            Qt = disc.Q(3);
            
            % Reset the mass for each time level
            meanValues  = zeros(Qt,1);
            volume      = zeros(Qt,1);
            intGradC    = zeros(Qt,1);
               
            for e=s.mesh.eRange
                
                % Set the element
                element = s.mesh.getElement(e);
                
                % Get the values for the modified integration order
                values = s.omgField.getValueInterpolated(e,disc,s.mesh);
                
                Cx = s.cField.getDxInterpolated(e,disc,s.mesh);
                Cy = s.cField.getDyInterpolated(e,disc,s.mesh);
                
                gradC = sqrt( Cx.^2 + Cy.^2 );
                
%                 values(values<0.5) = 0;
                
                % Set the integration weights and Jacobian for 2D spatial planes
                wxy  = element.disc.Wspatial;
                J    = element.J(1)*element.J(2);
      
                for t=1:Qt
                    xyValues        = values(:,:,t);
                    meanValues(t)	= meanValues(t) + xyValues(:)' * wxy / J;
                    volume(t)       = volume(t) + sum(wxy) / J;
                    
                    gradCValues     = gradC(:,:,t);
                    intGradC(t)     = intGradC(t) + gradCValues(:)' * wxy / J;
                end
                
%                 % Space
% %                 data = C{e};
%                 data = C.getValueInterpolated(e,disc);
%                 element = s.mesh.getLocalElement(e);
%                 wxy = kron(element.disc.wy,element.disc.wx);
%                 if position == 0
%                     % time level 0 of element
%                     mass(1) = mass(1) + data(1:(element.disc.Qx*element.disc.Qy))' * wxy / (element.J(1)*element.J(2)) ;
%                 else
%                     mass(1) = mass(1) + data((element.disc.Qz-1)*(element.disc.Qx*element.disc.Qy)+1:element.disc.Qz*(element.disc.Qx*element.disc.Qy))' * wxy / (element.J(1)*element.J(2)) ;
%                 end
%                 
%                 % SpaceTime
%                 data = C{e};                
%                 element = s.mesh.getLocalElement(e);
%                 J = prod( element.J );
%                 wxyz = superkron(element.disc.wz,element.disc.wy,element.disc.wx);
%                 mass(2) = mass(2) + data' * wxyz / J;
            end
            
            if (System.settings.anal.debug)
                fprintf('Before NMPI_Allreduce in CahnHilliard.computeMass\n')
            end
            
            s.meanOmega     = NMPI.Allreduce(meanValues,Qt,'+','meanOmega');
            s.totalVolume   = NMPI.Allreduce(volume,Qt,'+','totalVolume');
            s.intGradC      = NMPI.Allreduce(intGradC,Qt,'+','intGradC');
            
%             s.massLoss = s.mass(1)-s.mass(end);
            
%             if (NMPI.instance.rank==0)
%                 fprintf('Mean omega (begin/end)             : %f / %f\n',s.meanOmega(1)/s.totalVolume(1),s.meanOmega(end)/s.totalVolume(end));
%                 fprintf('Mean curvature (begin/end)         : %f / %f\n',s.meanOmega(1)/s.intGradC(1),s.meanOmega(end)/s.intGradC(end));
%                 fprintf('Mean curvature - 2*pi (begin/end)  : %f / %f\n',2*pi/s.intGradC(1),2*pi/s.intGradC(end));
%             end
            
            s.kappa = (2*pi) / mean(s.intGradC);
        end
        
        function computeLS(s)
            
            % check if init is required
            if (isempty(s.LS))
                s.LS = cell( s.mesh.numElements, 1);
                init = true;
            else
                init = false;
            end
            
            % compute LS
            for e=1:s.mesh.numElements
                element = s.mesh.getElement(e);
                el = element.localID;
                
                %Ck = C.val(FieldType.Current,e);
                if (init)
                    Ck = s.fields{1}.value{el}(:);
                else
                    Ck = s.fields{1}.value{el}(:) + (0.5 + 0.5*tanh( s.LS{e} / (2*sqrt(2)*s.Cn_L) ));
                end
                
                s.LS{e} = sqrt(2) * s.Cn_L * log( abs( Ck ./ (1-Ck) ));
                
                delta = 0.00001;
                s.LS{e}( abs(Ck)>1-delta ) = sqrt(2) * s.Cn_L * log( abs( (1-delta) ./ (  delta) ) );
                s.LS{e}( abs(Ck)<  delta ) = sqrt(2) * s.Cn_L * log( abs(    delta  ./ (1-delta) ) );
            end
            
            % update C_SEM
            for e=1:s.mesh.numElements
                element = s.mesh.getElement(e);
                el = element.localID;
                
                [X,Y]    = element.getNodes;
                     
                X = squeeze(X); Y = squeeze(Y);
                
                Ck = s.fields{1}.value{el}(:);
                
                %data = reshape( full(s.LS{e}), element.finite.qSize );
                Cext = Ck(:) - (0.5 + 0.5*tanh( s.LS{e} / (2*sqrt(2)*s.Cn_L) ));
                data = reshape( full(Cext), element.finite.qSize );
                
                %potential = s.alpha/s.Cn * s.Cn/s.Cn_L * ( 1*(c.^3-3/2*c.^2+1/2*c) - 1*s.Cn_L^2*(cxx+cyy) );
                %potential = (s.alpha/s.Cn_L) * ( 1*(c.^3-3/2*c.^2+1/2*c) - 1*s.Cn_L^2*(cxx+cyy) );
                
                surf(X(:,:,end),Y(:,:,end), data(:,:,end));
                
                hold on;
            end
            
            clf;
            for e=1:s.mesh.numElements
                element = s.mesh.getElement(e);
                el = element.localID;
                
                [X,Y]    = element.getNodes;
                     
                X = squeeze(X); Y = squeeze(Y);
                
                Ck = s.fields{1}.value{el}(:);
                
                data = reshape( full(s.LS{e}), element.finite.qSize );
                [Fx, Fy] = gradient(data,X(:,1,1),X(1,:,1),X(1,1,:));
                
                %Cext = Ck(:) - (0.5 + 0.5*tanh( s.LS{e} / (2*sqrt(2)*s.Cn_L) ));
                %data = reshape( full(Cext), element.finite.qSize );
                
                data = Fy(:); % sqrt(2) / (8*s.Cn_L) * Fy(:) .* (1 - (tanh( s.LS{e} / (2*sqrt(2)*s.Cn_L) ) ).^2 );
                data = reshape( full(data), element.finite.qSize );
                
                %potential = s.alpha/s.Cn * s.Cn/s.Cn_L * ( 1*(c.^3-3/2*c.^2+1/2*c) - 1*s.Cn_L^2*(cxx+cyy) );
                %potential = (s.alpha/s.Cn_L) * ( 1*(c.^3-3/2*c.^2+1/2*c) - 1*s.Cn_L^2*(cxx+cyy) );
                
                surf(X(:,:,end),Y(:,:,end), data(:,:,end));
                
                hold on;
            end
            
            disp('')
            
        end
        
        % Only useful for cField
        function plotPotential(s)
            C = s.fields{1};
            
            P = FieldCollection.get('p');
            
            for e=1:s.mesh.numElements
                element = s.mesh.getElement(e);
                c      = reshape(C.getValue(e),element.finite.qSize);
                cx = reshape(C.x(FieldType.Current,e),element.finite.qSize);
                cy = reshape(C.y(FieldType.Current,e),element.finite.qSize);
                cxx    = reshape(C.xx(FieldType.Current,e),element.finite.qSize);
                cyy    = reshape(C.yy(FieldType.Current,e),element.finite.qSize);
                
                px = reshape(P.x(FieldType.Current,e),element.finite.qSize);
                py = reshape(P.y(FieldType.Current,e),element.finite.qSize);
                
                [X,Y]    = element.getNodes;
                                
                c = c(:,:,end);
                
                %potential = s.alpha/s.Cn * s.Cn/s.Cn_L * ( 1*(c.^3-3/2*c.^2+1/2*c) - 1*s.Cn_L^2*(cxx+cyy) );
%                 potential = (s.alpha) * ( 1/s.Cn_L*(c.^3-3/2*c.^2+1/2*c) - s.Cn_L*(cxx+cyy) );
                potential = (1) * ( 1/s.Cn_L*(c.^3-3/2*c.^2+1/2*c) - s.Cn_L*(cxx+cyy) ) - s.Cn_L * sqrt(px.^2+py.^2);
                
                surf(X(:,:,end),Y(:,:,end), potential(:,:,end));
                
                hold on;
            end
                        
            colorbar;
            view([25,25]);
%             title(titleString);
        end
        
        % Only useful for cField
        function plotKappaCorrection(s)
            C = s.fields{1};
            kappa = s.getKappa;
            
            for e=1:s.mesh.numElements
                element = s.mesh.getElement(e);
                
                id = element.id;
                
                %c      = reshape(C.getValue(e),element.finite.qSize);
                cx = reshape(C.x(FieldType.Current,e),element.finite.qSize);
                cy = reshape(C.y(FieldType.Current,e),element.finite.qSize);
                cxy = C.xy2(id);
                
                gradc = sqrt(cx.^2+cy.^2);
                
                %cxx    = reshape(C.xx(FieldType.Current,e),element.finite.qSize);
                %cyy    = reshape(C.yy(FieldType.Current,e),element.finite.qSize);
                
                kappax = kappa.x2(id);
                kappay = kappa.y2(id);
                
                %px = reshape(P.x(FieldType.Current,e),element.finite.qSize);
                %py = reshape(P.y(FieldType.Current,e),element.finite.qSize);
                
                [X,Y]    = element.getNodes;
                                
                %c = c(:,:,end);
                
                %potential = s.alpha/s.Cn * s.Cn/s.Cn_L * ( 1*(c.^3-3/2*c.^2+1/2*c) - 1*s.Cn_L^2*(cxx+cyy) );
%                 potential = (s.alpha) * ( 1/s.Cn_L*(c.^3-3/2*c.^2+1/2*c) - s.Cn_L*(cxx+cyy) );
%                 potential = ( -sign(cx(:)) .* kappax .* cx(:) + 0*sign(cy(:)) .* kappay .* cy(:) ) ; %.* cxy ; %.* ( 0*cy(:) - cx(:) ); % ./ gradc(:);
                potential = ( kappax .* cy(:) - kappay .* cx(:) ) .* (cx(:)-cy(:)) ; %.* cxy ; %.* ( 0*cy(:) - cx(:) ); % ./ gradc(:);
                
                surf(X,Y,reshape(potential,size(X)));
                
                hold on;
            end
                        
            colorbar;
            view([25,25]);
%             title(titleString);
        end
        
       function plotKappaGradient(s)
            C = s.fields{1};
            kappa = s.getKappa;
            
            for e=1:s.mesh.numElements
                element = s.mesh.getElement(e);
                
                id = element.id;
                
                %c      = reshape(C.getValue(e),element.finite.qSize);
                cx = reshape(C.x(FieldType.Current,e),element.finite.qSize);
                cy = reshape(C.y(FieldType.Current,e),element.finite.qSize);
                cxy = C.xy2(id);
                
                gradc = sqrt(cx.^2+cy.^2);
                
                %cxx    = reshape(C.xx(FieldType.Current,e),element.finite.qSize);
                %cyy    = reshape(C.yy(FieldType.Current,e),element.finite.qSize);
                
                kappaVal  = kappa.val2(id);
%                 kappax = kappa.x2(id);
%                 kappay = kappa.y2(id);
                
                %px = reshape(P.x(FieldType.Current,e),element.finite.qSize);
                %py = reshape(P.y(FieldType.Current,e),element.finite.qSize);
                
                [X,Y]    = element.getNodes;
                                
                %c = c(:,:,end);
                
                %potential = s.alpha/s.Cn * s.Cn/s.Cn_L * ( 1*(c.^3-3/2*c.^2+1/2*c) - 1*s.Cn_L^2*(cxx+cyy) );
%                 potential = (s.alpha) * ( 1/s.Cn_L*(c.^3-3/2*c.^2+1/2*c) - s.Cn_L*(cxx+cyy) );
                potential = (kappaVal -2 * gradc(:)) .* cxy ; %.* (cy(:)-cx(:));
                
                surf(X,Y,reshape(potential,size(X)));
                
                hold on;
            end
                        
            colorbar;
            view([25,25]);
%             title(titleString);
        end
        
        % Only useful for cField
        function plotMeanOmega(s)
            C = s.fields{1};
            
            for e=1:s.mesh.numElements
                element = s.mesh.getElement(e);
                c      = reshape(C.getValue(e),element.finite.qSize);
                cx     = reshape(C.x(FieldType.Current,e),element.finite.qSize);
                cy     = reshape(C.y(FieldType.Current,e),element.finite.qSize);
                cxx    = reshape(C.xx(FieldType.Current,e),element.finite.qSize);
                cyy    = reshape(C.yy(FieldType.Current,e),element.finite.qSize);
                cxy    = reshape(C.xy(FieldType.Current,e),element.finite.qSize);
                
                gradC2 = cx.^2+cy.^2;
                
                [X,Y]    = element.getNodes;
                                
                %c = c(:,:,end);
                
                %potential = s.alpha/s.Cn * s.Cn/s.Cn_L * ( 1*(c.^3-3/2*c.^2+1/2*c) - 1*s.Cn_L^2*(cxx+cyy) );
                
                %meanOmega = (s.alpha/s.Cn) * ( (c.^3-3/2*c.^2+1/2*c).*sqrt(gradC2) - 1*s.Cn_L^2.*(cx.^2.*cxx + 2*cx.*cy.*cxy + cy.^2.*cyy)./sqrt(gradC2) );
                %meanOmega = (s.alpha/s.Cn_L) * ( 1*(c.^3-3/2*c.^2+1/2*c) - 1*s.Cn_L^2.*(cx.^2.*cxx + 2*cx.*cy.*cxy + cy.^2.*cyy)./(gradC2) );
                meanOmega = (1/s.Cn_L) * ( 1*(c.^3-3/2*c.^2+1/2*c) - 1*s.Cn_L^2.*(cx.^2.*cxx + 2*cx.*cy.*cxy + cy.^2.*cyy)./(gradC2) );
                
                surf(X(:,:,end),Y(:,:,end), meanOmega(:,:,end));
                
                hold on;
            end
                        
            colorbar;
            view([25,25]);
%             title(titleString);
        end
        
        % Only useful for cField
        function plotKappaTest(s)
            C = s.fields{1};
            kField = s.fields{3};
            
            for e=1:s.mesh.numElements
                element = s.mesh.getElement(e);
                c      = reshape(C.getValue(e),element.finite.qSize);
                cx     = reshape(C.x(FieldType.Current,e),element.finite.qSize);
                cy     = reshape(C.y(FieldType.Current,e),element.finite.qSize);
                cxx    = reshape(C.xx(FieldType.Current,e),element.finite.qSize);
                cxy    = reshape(C.xy(FieldType.Current,e),element.finite.qSize);
                cyy    = reshape(C.yy(FieldType.Current,e),element.finite.qSize);
                k      = reshape(kField.getValue(e),element.finite.qSize);
                kx     = reshape(kField.x(FieldType.Current,e),element.finite.qSize);
                ky     = reshape(kField.y(FieldType.Current,e),element.finite.qSize);
                
                dS = (cx.^2+cy.^2);
                
                cnn    = ( cx.^2 .* cxx + cy.^2 .* cyy + 2 * cx .* cy .* cxy ) ./ dS;
                
                dS = (cx.^2+cy.^2);
                dK = sqrt(kx.^2+ky.^2);
                %dV = 1-2*abs(c-0.5);
                dV = c;
                
                kappa = k .* (dK.*(dS)*s.Cn_L + 0.5 * k .* cnn);
                                
                [X,Y]    = element.getNodes;
                
                surf(X,Y,kappa);
                
                hold on;
            end
                        
            colorbar;
            view([25,25]);
%             title(titleString);
        end
        
        function plotLS(s)
            C = FieldCollection.get('c');
            
            for e=s.mesh.eRange
                element = s.mesh.getElement(e);

                Ck      = C.val(FieldType.Current,e);
                
                [x,y]   = element.getNodes;
                
                x = squeeze(x);
                y = squeeze(y);
                 
                LS = 2*s.alpha/3 * s.Cn_L * log( abs( Ck./(1-Ck) ) );
                
                LS = reshape( LS, size(x) );
                
                LS_exact = 0 * (0.5 - sqrt( (x-1).^2 + (y-1).^2 ) );
                
                hold on;
                surface(x(:,:,end),y(:,:,end),LS(:,:,end)-LS_exact(:,:,end))
            end
        end
        
        function plotCurvature(s)
            
%             if nargin<3
%                 s.createInterface;
%                 interface = s.interface;
%             end

            curvature = cell(s.mesh.numLocalElements,1);
            
            C   = s.getField(1); %.current;
            Omg = s.getField(2);  %.current;
            
            mesh = C.mesh;
            
            [kx,ky] = s.getCurvature(s.mesh);
            
            for el=1:mesh.numLocalElements
                
                e = mesh.getLocalElement(el).globalID;
                %Ck      = C.val(FieldType.Current,e);
                %dCkdx   = C.x(FieldType.Current,e);
                %dCkdy   = C.y(FieldType.Current,e);
                %Omgk    = Omg.val(FieldType.Current,e);
                %curvature{el} = Omgk./ (s.Cn * s.alpha * sqrt(dCkdx.^2+dCkdy.^2));
                %curvature{el}( (abs(curvature{el})>100) ) = 0;
                %Cxx      = C.xx2(el);
                %Cxy      = C.xy2(el);
                %Cyy      = C.yy2(el);
                
                %curvature{el} = (Cxx+Cyy) - 0*sqrt( kx{el}.^2 + ky{el}.^2 );
                curvature{el} = sqrt( kx{el}.^2 + ky{el}.^2 ); % ./ sqrt( dCkdx.^2 + dCkdy.^2 );
                
                curvature{el} = ky{el};
            end
                        
            x0 = s.mesh.X(1,1);
            x1 = s.mesh.X(2,1);
            y0 = s.mesh.X(1,2);
            y1 = s.mesh.X(2,2);
            
            pbx = x1 - x0;
            pby = y1 - y0;

            %xlim([mesh.x0 mesh.x1])
            %ylim([mesh.y0 mesh.y1])
            
            %zlim([0 10])
            
            grid off;
            pbaspect([pbx pby 1])

            shading interp
            view([0,90])
            
            hold on;
            
%             X = cell(mesh.numLocalElements,1);
%             Y = cell(mesh.numLocalElements,1);
            curv = cell(mesh.numLocalElements,1);

            %% Create a meshgrid
            %for e=1:mesh.numLocalElements
            %    [X{e},Y{e}] = meshgrid( mesh.getLocalElement(e).xelem,mesh.getLocalElement(e).yelem );
            %end
            
            %% Compute the surface tension data
            for e=1:mesh.numLocalElements
                temp = curvature{e};
                temp = reshape(temp,mesh.getLocalElement(e).finite.qSize);
                curv{e} = temp; %(:,:,end);
            end
            
            maxST = max( cellfun(@(x) max(x(:)),curv) );
            
            % Integration of the surface tension
            for e=1:mesh.numLocalElements
                element = mesh.getElement(e);
                %wxy = kron(element.disc.wy,element.disc.wx);
                %intST = intST + data((element.disc.Qz-1)*(element.disc.Qx*element.disc.Qy)+1:element.disc.Qz*(element.disc.Qx*element.disc.Qy))' * wxy / (element.J(1)*element.J(2)) ;
                %intST = intST + surfaceTension{e}(:) .* wxy/(element.J(1)*element.J(2));

                [X,Y] = element.getNodes;
                X = squeeze(X);
                Y = squeeze(Y);
                
                surface(X(:,:,end),Y(:,:,end),curv{e}(:,:,end));
                
                %if interface.active && ~isempty(interface.x{e})
                %    line(interface.x{e}, interface.y{e}, repmat(maxST,size(interface.x{e})), interface.props{:});
                %end
            end
        end
        
        function plotDivCurvature(s)
            
%             if nargin<3
%                 s.createInterface;
%                 interface = s.interface;
%             end

            curvature = cell(s.mesh.numLocalElements,1);
            
            C   = s.getField(1); %.current;
            Omg = s.getField(2);  %.current;
            
            mesh = C.mesh;
            
            %H = s.getH2(s.mesh);
            
            for el=1:mesh.numLocalElements
                
                e = mesh.getLocalElement(el).globalID;
                Ck      = C.val(FieldType.Current,e);
                %cx   = C.x(FieldType.Current,e);
                %cy   = C.y(FieldType.Current,e);
                %Omgk    = Omg.val(FieldType.Current,e);
                %curvature{el} = Omgk./ (s.Cn * s.alpha * sqrt(dCkdx.^2+dCkdy.^2));
                %curvature{el}( (abs(curvature{el})>100) ) = 0;
                
                %curvature{el} = 2 * s.H{el}(:) kx{el} .* cy - ky{el} .* cx; %sqrt( kx{el}.^2 + ky{el}.^2 );
                
                advTerm = sign(Ck-0.5) .* (Ck.*(1-Ck)-0.25);            
                curvature{el} = advTerm .* 2 .* s.H{el}(:);
            end
                        
            x0 = s.mesh.X(1,1);
            x1 = s.mesh.X(2,1);
            y0 = s.mesh.X(1,2);
            y1 = s.mesh.X(2,2);
            
            pbx = x1 - x0;
            pby = y1 - y0;

            %xlim([mesh.x0 mesh.x1])
            %ylim([mesh.y0 mesh.y1])
            
            grid off;
            pbaspect([pbx pby 1])

            shading interp
            view([0,90])
            
            hold on;
            
%             X = cell(mesh.numLocalElements,1);
%             Y = cell(mesh.numLocalElements,1);
            curv = cell(mesh.numLocalElements,1);

            %% Create a meshgrid
            %for e=1:mesh.numLocalElements
            %    [X{e},Y{e}] = meshgrid( mesh.getLocalElement(e).xelem,mesh.getLocalElement(e).yelem );
            %end
            
            %% Compute the surface tension data
            for e=1:mesh.numLocalElements
                temp = curvature{e};
                temp = reshape(temp,mesh.getLocalElement(e).finite.qSize);
                curv{e} = temp; %(:,:,end);
            end
            
            maxST = max( cellfun(@(x) max(x(:)),curv) );
            
            % Integration of the surface tension
            for e=1:mesh.numLocalElements
                element = mesh.getElement(e);
                %wxy = kron(element.disc.wy,element.disc.wx);
                %intST = intST + data((element.disc.Qz-1)*(element.disc.Qx*element.disc.Qy)+1:element.disc.Qz*(element.disc.Qx*element.disc.Qy))' * wxy / (element.J(1)*element.J(2)) ;
                %intST = intST + surfaceTension{e}(:) .* wxy/(element.J(1)*element.J(2));

                [X,Y] = element.getNodes;
                X = squeeze(X);
                Y = squeeze(Y);
                
                surface(X(:,:,end),Y(:,:,end),curv{e}(:,:,end));
                
                %if interface.active && ~isempty(interface.x{e})
                %    line(interface.x{e}, interface.y{e}, repmat(maxST,size(interface.x{e})), interface.props{:});
                %end
            end
        end
        
        
        % The surface energy can be determined from the Helmholtz free 
        % energy density model, which in the Cahn-Hilliard model is the 
        % sum of the local Helmholtz energy and surface tension effects.
        function out = computeSurfaceEnergy(s)

            
            
%             if (nargin==1)
%                 activeField = FieldType.Current;
%             end

            % Set the Fields
            C   = s.getField(1,s);
            
            % Get the mesh (should be same for both fields!)
            mesh = C.mesh;
            
            numElements = mesh.numLocalElements;
           
            % Initialize psi and gradC for all elements
            psi   = cell(numElements,1);
            gradC = cell(numElements,1);
           
            for el=1:numElements
                e = mesh.getElement(el).globalID;
                
                Ck          = C.val(e);
                dCkdx       = C.x(e);
                dCkdy       = C.y(e);
                
                psi{e}      = 0.25 * Ck.^2 .* (1-Ck).^2;
                gradC{e}    = dCkdx.^2+dCkdy.^2;
            end

            surfaceEnergy = 0;
            
            for e=1:numElements
                element = mesh.getLocalElement(e);
                
                data = 0.5*s.Cn .* gradC{e} + psi{e} ./ s.Cn;
                
                % Set the integration weights and Jacobian for 2D spatial planes
                w = finiteElement.Wspatial;
                J = element.JSpatial;
                
                surfaceEnergy = surfaceEnergy + data((element.disc.Qz-1)*(element.disc.Qx*element.disc.Qy)+1:element.disc.Qz*(element.disc.Qx*element.disc.Qy))' * wxy / (element.J(1)*element.J(2)) ;
            end
            
            surfaceEnergy = surfaceEnergy * s.alpha;

            if (System.settings.anal.debug)
                fprintf('Before NMPI_Allreduce in CahnHilliard.computeSurfaceEnergy\n')
            end
            
%             fprintf('Max of surface tension : %f\n',stMax)
            surfaceEnergy = NMPI.Allreduce(surfaceEnergy,1,'+','surfaceEnergy');
%             if (NMPI.instance.rank==0)
%                 fprintf('Total surface energy  : %f\n',surfaceEnergy)
%             end
            
            out = surfaceEnergy;
        end
        
        function computeSurfaceTension(s,activeField)

            %if (nargin==1)
                finiteElement = FECollection.get(1);
            %end
            
            if (nargin==1)
                activeField = FieldType.Current;
            end

            % Set the Fields
            C   = s.getField(1,s);
            Omg = s.getField(2,s);
            
            % Get the mesh (should be same for both fields!)
            mesh = C.mesh;
            
            numElements = mesh.numLocalElements;
            
            fx = cell(numElements,1);
            fy = cell(numElements,1);
            
            if (finiteElement.spaceTime)
                Qt = finiteElement.Q( end );
            else
                Qt = 1;
            end
            
            % Compute the surface tension per element
%             for el=1:numElements
%                 e = mesh.getElement(el).globalID;
%                 
%                 Ck      = C.val(FieldType.Current,e);
%                 dCkdx   = C.x(FieldType.Current,e);
%                 dCkdy   = C.y(FieldType.Current,e);
%                 Omgk    = Omg.val(FieldType.Current,e);
% 
%                 fx{e} = (Omgk).*dCkdx;
%                 fy{e} = (Omgk).*dCkdy;
%             end
            
            [fx,fy] = s.getCurvature;

            % Reset the total/enclosed mass for each time level
            surfaceTension = zeros(Qt,1);
            
            for e=1:numElements
                element = mesh.getLocalElement(e);
                
                data = sqrt(fx{e}.^2+fy{e}.^2);
                data = reshape( data, element.finite.qSize );
                
                % Set the integration weights and Jacobian for 2D spatial planes
                w = finiteElement.Wspatial;
                J = element.JSpatial;

                for t=1:Qt
                    sliceValues = data(:,:,t);
                    surfaceTension(t) = surfaceTension(t) + dot( sliceValues(:),w) * J;
                end
            end
            
            surfaceTension = surfaceTension / s.We;

            if (System.settings.anal.debug)
                fprintf('Before NMPI_Allreduce in CahnHilliard.computeSurfaceTension\n')
            end
            
%             fprintf('Max of surface tension : %f\n',stMax)
            surfaceTension = NMPI.Allreduce(surfaceTension,numel(surfaceTension),'+','surfaceTension');
            if (NMPI.instance.rank==0)
                for i=1:Qt
                    fprintf('Total surface tension (t=%d) : %f (%f pi)\n',i,surfaceTension(i),surfaceTension(i)/pi)
                end
            end
        end
        
        function plotSurfaceTension(s,fig,interface)
            
%             if nargin<3
%                 s.createInterface;
%                 interface = s.interface;
%             end
            
%             fx = cell(s.mesh.numLocalElements,1);
%             fy = cell(s.mesh.numLocalElements,1);
            
            %Omega = FieldCollection.get('omg');
%             C     = FieldCollection.get('c');
%             Omg   = FieldCollection.get('omg');
            
            cField   = s.fields{1};
            
            %Omg = s.getField(2,s);  %.current;
            
            %activeField = FieldType.Current;
            % 
            %C.setActive(activeField);
            %Omg.setActive(activeField);
            
            mesh = s.mesh;
            
%             Cn      = System.settings.phys.Cn;
%             alpha   = System.settings.phys.alpha;
            
%             meanOmg = 0;
            
%             for el=1:mesh.numLocalElements
% %                 Ck      = s.getC(e,SolutionMode.Normal);
% %                 dCkdx   = s.getCx(e,SolutionMode.Normal);
% %                 dCkdy   = s.getCy(e,SolutionMode.Normal);
% %                 Omgk    = s.getOmg(e,SolutionMode.Normal);
%                 
%                 element = s.mesh.getElement(el);
% 
%                 e = mesh.getLocalElement(el).globalID;
% 
%                 Ck      = C.val(FieldType.Current,e);
%                 dCkdx   = C.x(FieldType.Current,e);
%                 dCkdy   = C.y(FieldType.Current,e);
%                 Omgk    = Omg.val(FieldType.Current,e); % - s.meanOmega(1);
% 
%                 %Ze = s.mesh.Z{e};
%                 %Nabla2 = element.getDxx + element.getDyy;
%                 %data = (Ck.^3 - 3/2*Ck.^2 + 0.5.*Ck) - Cn^2 * ( Nabla2 * Ze * C.alpha{e}(:) );
%                 %Omgk = alpha/Cn * data;
%                 
%                 
%                 fx{el} = Omgk.*dCkdx;
%                 fy{el} = Omgk.*dCkdy;
%                 
% %                 meanOmg = meanOmg+mean(Omgk);
%                 
% %                 fx{e} = Omgk;
% %                 fy{e} = Omgk;
%             end
            
            [kx,ky] = s.getCurvature;
            
%             meanOmg = meanOmg / mesh.numLocalElements*4
%             
%             meanOmg = 4;
% 
%             for el=1:mesh.numLocalElements
%                 e = mesh.getLocalElement(el).globalID;
%                 dCkdx   = C.x(e);
%                 dCkdy   = C.y(e);
%                 Omgk    = Omg.val(e);
%                 fx{el} = (Omgk-meanOmg).*dCkdx;
%                 fy{el} = (Omgk-meanOmg).*dCkdy;
%             end
            
%             subplot(3,1,1)
%             data = fx;
%             for e=1:s.mesh.getNumElements
%                 if (size(data{e},2)==1)
%                     data{e} = reshape(data{e},s.mesh.getElement(e).disc.Q);
%                     [X,Y] = meshgrid( s.mesh.getElement(e).xelem,s.mesh.getElement(e).yelem );
%                     surf(X,Y,data{e}(:,:,end)');
%                 else
%                     [X,Y] = meshgrid( s.mesh.getElement(e).xelem,s.mesh.getElement(e).yelem );
%                     surf(X,Y,data{e}(:,:,end)');
%                 end
%                 hold on;
%             end
%             
%             subplot(3,1,2)
%             data = fy;
%             for e=1:s.mesh.getNumElements
%                 if (size(data{e},2)==1)
%                     data{e} = reshape(data{e},s.mesh.getElement(e).disc.Q);
%                     [X,Y] = meshgrid( s.mesh.getElement(e).xelem,s.mesh.getElement(e).yelem );
%                     surf(X,Y,data{e}(:,:,end)');
%                 else
%                     [X,Y] = meshgrid( s.mesh.getElement(e).xelem,s.mesh.getElement(e).yelem );
%                     surf(X,Y,data{e}(:,:,end)');
%                 end
%                 hold on;
%             end
%             
%             subplot(3,1,3)

            intST = 0;
            
            pbx = mesh.X(2,1) - mesh.X(1,1);
            pby = mesh.X(2,2) - mesh.X(1,2);

            %xlim( mesh.X(1,:) );
            %ylim( mesh.X(2,:) )
            
            grid off;
            pbaspect([pbx pby 1])

            shading interp
            %view([0,90])
            colorbar;
            view([25,25]); 
            
            hold on;
            
            X = cell(mesh.numLocalElements,1);
            Y = cell(mesh.numLocalElements,1);
            surfaceTension = cell(mesh.numLocalElements,1);

            %% Create a meshgrid
            for e=1:mesh.numLocalElements
                [X1,Y1] = mesh.getLocalElement(e).getNodes;
                X{e} = X1(:,:,end);
                Y{e} = Y1(:,:,end);
            end
            
            %% Compute the surface tension data
            for e=1:mesh.numLocalElements
                
                c   = cField.val2(e);
                cx  = cField.x2(e);
                cy  = cField.y2(e);
                cxx = cField.xx2(e);
                cyy = cField.yy2(e);
                
%                 temp = - s.alpha * s.Cn_L * (cxx+cyy) .* sqrt(cx.^2+cy.^2);
                %temp = ((c.^3-3/2*c.^2+c/2)/s.Cn_L - s.Cn_L * (cxx+cyy));
                
                temp = sqrt(kx{e}.^2+ky{e}.^2);
                temp = reshape(temp,mesh.getLocalElement(e).finite.qSize);
                surfaceTension{e} = s.WEI * temp(:,:,1);
            end
            
            maxST = max( cellfun(@(x) max(x(:)),surfaceTension) );
            
            % Integration of the surface tension
            for e=1:mesh.numLocalElements
                element = mesh.getLocalElement(e);
%                 wxy = kron(element.disc.wy,element.disc.wx);
                
                wxy = element.getW_spatial;
                
                %intST = intST + data((element.disc.Qz-1)*(element.disc.Qx*element.disc.Qy)+1:element.disc.Qz*(element.disc.Qx*element.disc.Qy))' * wxy / (element.J(1)*element.J(2)) ;
                intST = intST + surfaceTension{e}(:) .* wxy(1:element.finite.numSpatialPoints); %*(element.J(1)*element.J(2));

                surface(X{e},Y{e},surfaceTension{e});
                
%                 if interface.active && ~isempty(interface.x{e})
%                     line(interface.x{e}, interface.y{e}, repmat(maxST,size(interface.x{e})), interface.props{:});
%                 end
            end
            
            if (System.settings.anal.debug)
                fprintf('Before NMPI_Allreduce in CahnHilliard.plotSurfaceTension\n')
            end
            
%             fprintf('Max of surface tension : %f\n',stMax)

            intST = NMPI.Allreduce(intST,1,'+','intST');
            
%             if (NMPI.instance.rank==0)
%                 fprintf('Total surface tension  : %f (%f pi)\n',surfaceTension,surfaceTension/pi)
%             end



        end
        
        function plotSurfaceTensionHalf(s,fig,interface)
            
            if nargin<3
                s.createInterface;
                interface = s.interface;
            end
            
            fx = cell(s.mesh.numLocalElements,1);
            fy = cell(s.mesh.numLocalElements,1);
            
            C   = s.getField(1,s); %.current;
            Omg = s.getField(2,s);  %.current;
            
            %activeField = FieldType.Current;
            % 
            %C.setActive(activeField);
            %Omg.setActive(activeField);
            
            mesh = C.mesh;
            
%             meanOmg = 0;
            
            for el=1:mesh.numLocalElements
%                 Ck      = s.getC(e,SolutionMode.Normal);
%                 dCkdx   = s.getCx(e,SolutionMode.Normal);
%                 dCkdy   = s.getCy(e,SolutionMode.Normal);
%                 Omgk    = s.getOmg(e,SolutionMode.Normal);
                
                e = mesh.getLocalElement(el).globalID;

                Ck      = C.val(e);
                dCkdx   = C.x(e);
                dCkdy   = C.y(e);
                Omgk    = Omg.val(e); % - s.meanOmega(1);

                fx{el} = Omgk.*dCkdx;
                fy{el} = Omgk.*dCkdy;
                
%                 meanOmg = meanOmg+mean(Omgk);
                
%                 fx{e} = Omgk;
%                 fy{e} = Omgk;
            end
            
%             meanOmg = meanOmg / mesh.numLocalElements*4
%             
%             for el=1:mesh.numLocalElements
%                 e = mesh.getLocalElement(el).globalID;
%                 dCkdx   = C.x(e);
%                 dCkdy   = C.y(e);
%                 Omgk    = Omg.val(e);
%                 fx{el} = (Omgk-meanOmg).*dCkdx;
%                 fy{el} = (Omgk-meanOmg).*dCkdy;
%             end
            
%             subplot(3,1,1)
%             data = fx;
%             for e=1:s.mesh.getNumElements
%                 if (size(data{e},2)==1)
%                     data{e} = reshape(data{e},s.mesh.getElement(e).disc.Q);
%                     [X,Y] = meshgrid( s.mesh.getElement(e).xelem,s.mesh.getElement(e).yelem );
%                     surf(X,Y,data{e}(:,:,end)');
%                 else
%                     [X,Y] = meshgrid( s.mesh.getElement(e).xelem,s.mesh.getElement(e).yelem );
%                     surf(X,Y,data{e}(:,:,end)');
%                 end
%                 hold on;
%             end
%             
%             subplot(3,1,2)
%             data = fy;
%             for e=1:s.mesh.getNumElements
%                 if (size(data{e},2)==1)
%                     data{e} = reshape(data{e},s.mesh.getElement(e).disc.Q);
%                     [X,Y] = meshgrid( s.mesh.getElement(e).xelem,s.mesh.getElement(e).yelem );
%                     surf(X,Y,data{e}(:,:,end)');
%                 else
%                     [X,Y] = meshgrid( s.mesh.getElement(e).xelem,s.mesh.getElement(e).yelem );
%                     surf(X,Y,data{e}(:,:,end)');
%                 end
%                 hold on;
%             end
%             
%             subplot(3,1,3)

            intST = 0;
            
            pbx = 2*(mesh.x1 - mesh.x0);
            pby = mesh.y1 - mesh.y0;

            xlim([-mesh.x1 mesh.x1])
            ylim([mesh.y0 mesh.y1])
            
            grid off;
            pbaspect([pbx pby 1])

            shading interp
            view([0,90])
            
            hold on;
            
            X = cell(mesh.numLocalElements,1);
            Y = cell(mesh.numLocalElements,1);
            surfaceTension = cell(mesh.numLocalElements,1);

            %% Create a meshgrid
            for e=1:mesh.numLocalElements
                [X{e},Y{e}] = meshgrid( mesh.getLocalElement(e).xelem,mesh.getLocalElement(e).yelem );
            end
            
            %% Compute the surface tension data
            for e=1:mesh.numLocalElements
                temp = sqrt(fx{e}.^2+fy{e}.^2);
                temp = reshape(temp,mesh.getLocalElement(e).disc.Q);
                surfaceTension{e} = temp(:,:,1);
            end
            
            maxST = max( cellfun(@(x) max(x(:)),surfaceTension) );
            
            % Integration of the surface tension
            for e=1:mesh.numLocalElements
                element = mesh.getLocalElement(e);
                wxy = kron(element.disc.wy,element.disc.wx);
                %intST = intST + data((element.disc.Qz-1)*(element.disc.Qx*element.disc.Qy)+1:element.disc.Qz*(element.disc.Qx*element.disc.Qy))' * wxy / (element.J(1)*element.J(2)) ;
                intST = intST + surfaceTension{e}(:) .* wxy/(element.J(1)*element.J(2));

                surface(X{e},Y{e},surfaceTension{e}');
                
                if interface.active && ~isempty(interface.x{e})
                    line(interface.x{e}, interface.y{e}, repmat(maxST,size(interface.x{e})), interface.props{:});
                end
                
                % second half
                surface(-X{e},Y{e},surfaceTension{e}');
                
                if interface.active && ~isempty(interface.x{e})
                    line(-interface.x{e}, interface.y{e}, repmat(maxST,size(interface.x{e})), interface.props{:});
                end
            end
            
            if (System.settings.anal.debug)
                fprintf('Before NMPI_Allreduce in CahnHilliard.plotSurfaceTension\n')
            end
            
%             fprintf('Max of surface tension : %f\n',stMax)

            intST = NMPI.Allreduce(intST,1,'+','intST');
            
%             if (NMPI.instance.rank==0)
%                 fprintf('Total surface tension  : %f (%f pi)\n',surfaceTension,surfaceTension/pi)
%             end



        end

        %% Function to derive the omega field from the C field
%         function deriveOmega(s,variableNumber)
%             omgField = s.fields{variableNumber};
%             
%             switch s.pfMethod
%                 case {pfMethod.balancedCH,pfMethod.balancedACH,pfMethod.meanKwakkel} %#ok<PROPLC>
%                     for e=s.mesh.eRange
%                         element = s.mesh.getElement(e);
%                         el = element.localID;
%                         
%                         cField = s.fields{1};
%                         
%                         C   = cField.val2( e );
%                         Cx  = cField.x2( e );
%                         Cy  = cField.y2( e );
%                         Cxx = cField.xx2( e );
%                         Cxy = cField.xy2( e );
%                         Cyy = cField.yy2( e );
%                         
%                         normC1 = Cx.^2 + Cy.^2;
%                         
%                         omgValues = 1/s.Cn_L * ( C.^3 - 3/2 * C.^2 + 1/2 * C ) ...
%                                     - s.Cn_L * ( ( Cx.^2 .* Cxx   +   Cy.^2.*Cyy  + 2*Cx.*Cy.*Cxy )./ normC1);
%                         
%                         omgField.setValue(el,omgValues);
%                     end
%                     
%                 case {pfMethod.classicCH} %#ok<PROPLC>
%                     for e=s.mesh.eRange
%                         element = s.mesh.getElement(e);
%                         el = element.localID;
%                         
%                         cField = s.fields{1};
%                         
%                         C   = cField.val2( e );
%                         Cx  = cField.x2( e );
%                         Cy  = cField.y2( e );
%                         Cxx = cField.xx2( e );
%                         Cxy = cField.xy2( e );
%                         Cyy = cField.yy2( e );
%                         
%                         omgValues = 1/s.Cn_L * ( C.^3 - 3/2 * C.^2 + 1/2 * C ) ...
%                                     - s.Cn_L * ( Cxx + Cyy );
%                         
%                         omgField.setValue(el,omgValues);
%                     end
%                     
%                 case {pfMethod.kappa} %#ok<PROPLC>
%                     for e=s.mesh.eRange
%                         element = s.mesh.getElement(e);
%                         el = element.localID;
%                         
%                         cField = s.fields{1};
%                         
%                         C   = cField.val2( e );
%                         Cx  = cField.x2( e );
%                         Cy  = cField.y2( e );
%                         Cxx = cField.xx2( e );
%                         Cxy = cField.xy2( e );
%                         Cyy = cField.yy2( e );
%                         
%                         normC1 = Cx.^2 + Cy.^2;
%                         
%                         omgValues = ( ( Cx.^2 .* Cxx   +   Cy.^2.*Cyy  + 2*Cx.*Cy.*Cxy )./ normC1);
%                         
%                         omgField.setValue(el,omgValues);
%                     end
%                     
%                 otherwise 
%                     error('Only balancedCH is supported in deriveOmega')
%             end
%             
%             omgField.computeAlpha;
%         end
%         
        %% Function to derive the kappa field from the C field
        function deriveKappa(s,variableNumber)
            kappaField = s.fields{variableNumber};

            for e=s.mesh.eRange
                element = s.mesh.getElement(e);
                el = element.localID;

                cField = s.fields{1};

                C   = cField.val2( e );
                Cx  = cField.x2( e );
                Cy  = cField.y2( e );
                Cxx = cField.xx2( e );
                Cxy = cField.xy2( e );
                Cyy = cField.yy2( e );

                normC1 = Cx.^2 + Cy.^2;

                kappaValues = -s.Cn_L * ( ( Cy.^2 .* Cxx - 2*Cx.*Cy.*Cxy + Cx.^2.*Cyy )./ normC1);

                kappaValues( normC1<1e-10 ) = 0;
                
                kappaField.setValue(el,kappaValues);
            end
                    
            kappaField.computeAlpha;
        end
        

        %%%%%%%%%%% FLAT INTERFACE
        function out = addFlatInterface(s,variable,x,y,z,t,center,normal)
            switch variable
                case Variable.c
                    out = s.initialFlatInterface(x,y,z,t,center,normal);
                case Variable.omg
                    out = 0 * s.initialFlatInterface(x,y,z,t,center,normal);
            end
        end
        
        function out = addFlatInterfaceStretch(s,variable,x,y,z,t,center,normal,stretch)
            switch variable
                case Variable.c
                    out = s.initialFlatInterfaceStretch(x,y,z,t,center,normal,stretch);
                case Variable.omg
                    out = 0 * s.initialFlatInterfaceStretch(x,y,z,t,center,normal,stretch);
            end
        end
        
        %%%%%%%%%%% DROPLET
%         function [varargout] = addDroplet(s,variable,x,y,z,t,center,radius,radius2)
%             
%             if (s.mesh.sDim==2)
%                 center(3:end) = 0;
%             end
%             
%             switch variable
%                 case Variable.c
%                     if (nargin==8)% || radius==radius2 )
%                         varargout = cell(1,3);
%                         [varargout{1:3}] = s.initialCircle(x,y,z,t,center,radius);
%                     else
%                         varargout = cell(1,3);
%                         [varargout{1:3}] = s.initialEllipse(x,y,z,t,center,radius,radius2);
%                     end
%                 case Variable.omg
%                     if (nargin==8)
%                         varargout = cell(1,3);
%                         [varargout{1:3}] = s.initialOmg(x,y,z,t,center,radius);
%                     else
%                         varargout = cell(1,3);
%                         [varargout{1:3}] = s.initialEllipseOmg(x,y,z,t,center,radius,radius2);
%                     end
%             end
%         end
%         
%         function out = addDropletStretch(s,variable,x,y,z,t,center,radius,radius2,stretch)
%             
%             if (s.mesh.sDim==2)
%                 center(3:end) = 0;
%             end
%             
%             switch variable
%                 case Variable.c
%                     out = s.initialCircle(x,y,z,t,center,radius,stretch);
%                 case Variable.omg
% %                     if (nargin==8)
%                         varargout = cell(1,3);
%                         [varargout{1:3}] = s.initialOmg(x,y,z,t,center,radius);
% %                     else
% %                         varargout = cell(1,3);
% %                         [varargout{1:3}] = s.initialEllipseOmg(x,y,z,t,center,radius,radius2);
% %                     end
%             end
%         end
        
        function [varargout] = addTaylorDroplet(s,variable,x,y,z,t,center,radius,length)
            
            if (s.mesh.sDim==2)
                center(3:end) = 0;
            end
            
            switch variable
                case Variable.c
                    varargout = cell(1,3);
                    [varargout{1:3}] = s.initialTaylorC(x,y,z,t,center,radius,length);
                case Variable.omg
                    varargout = cell(1,3);
                    [varargout{1:3}] = 0*s.initialTaylorC(x,y,z,t,center,radius,length);
            end
        end
        
        % Function to initialize a spinodal decompostion
        %   Based on the equation used in Yang et al. (2017)
        %
        %       C = 0.5 + delta * rand(x,y,z)
        %   
        %   where rand is a random number from a normal random 
        %   distribution with zero mean. This is slightly different
        %   from Yang et al. (2017), as not all random values are in 
        %   the [-1,+1] domain.
        %
        function out = addSpinodal(s,variable,x,y,z,t,average,delta,box)
            rng shuffle
            
            if nargin==8
                randomValues = randn( size(x) );
                randomValues = randomValues - mean(randomValues);

                out = average + delta * randomValues;
                
            else
                randomValues = randn( size(x) );
                randomValues = randomValues - mean(randomValues);

                average = average / ( prod(box(:,2)-box(:,1)) );
                
                out = average + delta * randomValues;
                
                flags = false( numel(out),1 );
                flags( logical( ( (x(:)>box(1,1)) .* (x(:)<box(1,2))) .* ( (y(:)>box(2,1)) .* (y(:)<box(2,2)))) ) = true;
                
                out( ~flags ) = 0;
            end
        end
%         
%         function out = addDroplet3D(s,variable,x,y,z,t,center,radius)
%             if (variable==1)
%                 c = zeros( size(x) );
%             
%                 % Center location
%                 xc = center(1);
%                 yc = center(2);
%                 zc = center(3);
% 
%                 % The interface thickness is scaled with Cn_L, which is equal
%                 % to eps/L. In the settings Cn = eps/R, so here the radius
%                 % (scaled by L) can be used.
%                 % Through this the interface thickness is proportional to the
%                 % radius, and not to the length scale that is used to
%                 % nondimensionalize the equations.
%                 denominator = (2*sqrt(2)*s.Cn_L);
% 
%                 for i=1:numel(x)
%                     dx2 = (x(i)-xc)^2;
%                     dy2 = (y(i)-yc)^2;
%                     dz2 = (z(i)-zc)^2;
% 
%                     r = sqrt( dx2 + dy2 + dz2 );
% 
%                     %% VALUE
%                     c(i) = 0.5 + 0.5 * tanh( (radius - r) / (denominator) ); 
%                 end
% 
%                 out = c;
%             else
%                 out = zeros(size(x));
%             end
%         end
        
        function out = addTorus3D(s,variable,x,y,z,t,center,R,r)
            % Function to create a torus shaped initial condition. The
            % major radius is given by R, the minor radius by r. The aspect
            % ratio is defined as R/r, and is typically a value between 2 and 3.
                        
            if (variable==1)                
                % This LS function defines phi>0 inside the torus, and phi<0 
                % outside. The interface itself is defined by phi=0
                phi = -( (sqrt(x.^2 + y.^2) - R).^2 + z.^2 - r.^2 );
                        
                % Convert phi (LS value) to C (phase-field value). The
                % interface is now defined by C=0.5, C>0.5 is inside and 
                % C<0.5 is outside. The denominator is to have control over
                % the thickness of the diffuse interface.
                denominator = (2*sqrt(2)*s.Cn_L);
                out = 0.5 + 0.5 * tanh( phi / denominator);
                  
            else
                out = zeros(size(x));              
            end
        end
        
        function out = addComplex3D(s,variable,x,y,z,t,center,D)
            % Function to return the initial condition as used by Zhang and
            % Ye (2016), see equation 43 of their article
            
            if (variable==1)                
                % This LS function defines phi>0 inside the complex structure, and
                % phi<0 outside. The interface itself is defined by phi=0
                phi = 1.0 - ( (x.^2+y.^2-D^2).^2 + (z-1).^2 .* (z+1).^2 ) .* ...
                            ( (y.^2+z.^2-D^2).^2 + (x-1).^2 .* (x+1).^2 ) .* ...
                            ( (z.^2+x.^2-D^2).^2 + (y-1).^2 .* (y+1).^2 );

                % Convert phi (LS value) to C (phase-field value). The
                % interface is now defined by C=0.5, C>0.5 is inside and 
                % C<0.5 is outside. The denominator is to have control over
                % the thickness of the diffuse interface.
                denominator = (2*sqrt(2)*s.Cn_L);
                out = 0.5 + 0.5 * tanh( phi / denominator);
                  
            else
                out = zeros(size(x));              
            end
        end
       
%         function out = addInterface(s,variable,x,y,z,t,center,radius,radius2)
%             switch variable
%                 case Variable.c
%                     if (nargin==8)
%                         out = s.initialCircle(x,y,z,t,center,radius);
%                     end
%                 case Variable.omg
%                     if (nargin==8)
%                         out = s.initialOmg(x,y,z,t,center,radius);
%                     end
%             end
%         end
        
       	function preTimeLoop(s)
            if (s.usePreTimeLoop)
                
                s.preTimeStep();
                
                %% Apply boundary conditions
                s.resetBoundaryConditions();
                activeSettings = System.settings;
                activeSettings.setBoundaryConditions( s );

                timeMethod = s.timeMethod;
                
                % By solving the steady equations, the solution will be
                % obtained faster
                s.Pe            = 0;
                s.PEI           = 0;
                s.steady        = false;
                
                %s.timeMethod    = TimeMethod.EulerImplicit;
                s.coupled       = false; 
                s.maxNonlinear  = 5;
                
                s.timeMethod = TimeMethod.Steady;
                
                eqWeights_tmp = s.eqWeights;
                
                if (System.settings.time.method==TimeMethod.SpaceTime)
                    s.eqWeights(1) = 1;
                    s.eqWeights(2) = 1;
                else
                    s.eqWeights(1) = 1;
                    s.eqWeights(2) = 1;
                end

                %% Solve nonlinear equations 
%                 s.shiftTimeSolutions(TimeMethod.EulerImplicit);
                s.solveNonLinear(1);

                s.steady    = s.settings.steady;
                s.coupled   = s.settings.coupled;
                s.maxNonlinear  = s.settings.maxNonlinear;
                s.Pe        = System.settings.phys.Pe;
                s.PEI       = 1 / s.Pe;
                %s.eps       = s.settings.eps;
                %s.eqWeights = s.settings.eqWeights;
                s.eqWeights = eqWeights_tmp;
                s.timeMethod    = timeMethod;
            end
        end
        
       	function preTimeLoop2(s)
            if (s.stabilize)
                s.stabilizeInterface;
            end
        end
        
        
        function updateBC(s)
            s.resetBoundaryConditions();
            activeSettings = System.settings;
            activeSettings.setBoundaryConditions( s );
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    end
    
    methods(Access=protected)
        function setC(s,value)
%             c.value = value;
            s.c = cell(s.mesh.getNumLocalElements,1);
            for e=1:s.mesh.getNumLocalElements
                element = s.mesh.getLocalElement(e);
                s.c{e} = value*ones(element.disc.dofeq);
            end
        end
        
        function setOmg(s,value)
            s.omg = cell(s.mesh.getNumLocalElements,1);
            for e=1:s.mesh.getNumLocalElements
                element = s.mesh.getLocalElement(e);
                s.omg{e} = value*ones(element.disc.dofeq);
            end
        end
        
        function addC(s,value)
            if (isa(value,'function_handle'))
                for e=1:s.mesh.getNumLocalElements
                    element = s.mesh.getLocalElement(e);

                    x = element.xelem;
                    y = element.yelem;
                    z = element.zelem;
                    
                    s.c{e} = max( s.c{e} , value(x,y,z) );
                end
            else
                for e=1:s.mesh.getNe
                    s.c{e} = max( s.c{e} , value );
                end
            end
        end
        

        
%         
% 
%         
% 
%         % Function which defines an circle through a specified center with 
%         % a given radius. The x,y,z data are the locations where the C
%         % data needs to be evaluated. The center is a vector of length 3,
%         % which contains its x,y,z data. Inside the circle C=1, outside C=0
%         function [c,Dc,DDc] = initialCircle(s,x,y,z,t,center,radius,stretch)
%             if length(center)~=3
%                 center( numel(center)+1:4 ) = 0;
%             end
%             
%             if (nargin<8)
%                 stretch = 1;
%             end
%             
%             elementSize = size(x);
%             
%             % VALUES
%             c = zeros( elementSize );
%                     
%             % D VALUES
%             Dc = cell(1,s.mesh.sDim);
%             for i=1:s.mesh.sDim
%                 Dc{i} = zeros( elementSize );
%             end
%             
%             % DD VALUES
%             DDc = cell(1,s.mesh.sDim);
%             for i=1:s.mesh.sDim
%                 DDc{i} = zeros( elementSize );
%             end
%             
%             % Element nodes
%             if (isempty(z))
%                 z = 0;
%             end
%             
%             % Center location
%             xc = center(1);
%             yc = center(2);
%             zc = center(3);
%             
%             % The interface thickness is scaled with Cn_L, which is equal
%             % to eps/L. In the settings Cn = eps/R, so here the radius
%             % (scaled by L) can be used.
%             % Through this the interface thickness is proportional to the
%             % radius, and not to the length scale that is used to
%             % nondimensionalize the equations.
%             denominator = (2*sqrt(2)*s.Cn_L*stretch);
%             
%             for i=1:numel(x)
%                 dx2 = (x(i)-xc)^2;
%                 dy2 = (y(i)-yc)^2;
%                 dz2 = (z(i)-zc)^2;
%                 
%                 r = sqrt( dx2 + dy2 + dz2 );
%                 
%                 %% VALUE
%                 %c(i) = 0.5 + 0.5 * tanh( (radius - r) / (denominator) );
%                 if (denominator~=0)
%                     c(i) = 0.5 + 0.5 * tanh( (radius - r) / (denominator) );
%                 else
%                     c(i) = 0.5 + 0.5 * sign(radius - r);
%                 end
%                 
%                 %% D VALUE   
%                 %if (r~=0)
%                 %    factor = sqrt(2) * (1-tanh( (radius-r)/denominator )^2) / (8*r*s.Cn_L);
%                 %    %factor = (1-tanh( (1-r/radius)/(denominator*radius) )^2) / (2*denominator*r*s.Cn_L);
%                 %else
%                 %    factor = 0;
%                 %end
%                 %Dc{1}(i) = -x(i) * factor;
%                 %Dc{2}(i) = -y(i) * factor; 
%             end
%         end
%         
%         % Function which defines an circle through a specified center with 
%         % a given radius. The x,y,z data are the locations where the C
%         % data needs to be evaluated. The center is a vector of length 3,
%         % which contains its x,y,z data. Inside the circle C=1, outside C=0
%         function [c,Dc,DDc] = initialTaylorC(s,x,y,z,t,center,radius,stretch)
%             if length(center)~=3
%                 center( numel(center)+1:4 ) = 0;
%             end
%             
%             elementSize = size(x);
%             
%             % VALUES
%             c = zeros( elementSize );
%                     
%             % D VALUES
%             Dc = cell(1,s.mesh.sDim);
%             for i=1:s.mesh.sDim
%                 Dc{i} = zeros( elementSize );
%             end
%             
%             % DD VALUES
%             DDc = cell(1,s.mesh.sDim);
%             for i=1:s.mesh.sDim
%                 DDc{i} = zeros( elementSize );
%             end
%             
%             % Element nodes
%             if (isempty(z))
%                 z = 0;
%             end
%             
%             % Center location
%             xc = center(1);
%             yc = center(2);
%             zc = center(3);
%             
%             denominator = (2*sqrt(2)*s.Cn_L);
%             
%             for i=1:numel(x)
%                 
%                 
%                 if (abs(y(i)-yc) < stretch/2)
%                     %% linear zone
%                     dx = radius - abs(x(i)-xc);
%                     
%                     %% VALUE
%                     c(i) = 0.5 + 0.5 * tanh( dx / (denominator) );
% % 
% %                     %% D VALUE   
% %                     if (r~=0)
% %                         factor = sqrt(2) * (1-tanh( (radius-r)/denominator )^2) / (8*r*s.Cn_L);
% %                         %factor = (1-tanh( (1-r/radius)/(denominator*radius) )^2) / (2*denominator*r*s.Cn_L);
% %                     else
% %                         factor = 0;
% %                     end
% %                     Dc{1}(i) = -x(i) * factor;
% %                     Dc{2}(i) = -y(i) * factor; 
%                     
%                 else
%                     % circular zone
%                     dx2 = (x(i)-xc)^2;
%                     dy2 = (abs(y(i)-yc)-stretch/2)^2;
%                     dz2 = (z(i)-zc)^2;
%                     
%                     r = sqrt( dx2 + dy2 + dz2 );
%                 
%                     %% VALUE
%                     %c(i) = 0.5 + 0.5 * tanh( (radius - r) / (denominator) );
%                     c(i) = 0.5 + 0.5 * tanh( (radius - r) / (denominator) );
% 
% %                     %% D VALUE   
% %                     if (r~=0)
% %                         factor = sqrt(2) * (1-tanh( (radius-r)/denominator )^2) / (8*r*s.Cn_L);
% %                         %factor = (1-tanh( (1-r/radius)/(denominator*radius) )^2) / (2*denominator*r*s.Cn_L);
% %                     else
% %                         factor = 0;
% %                     end
% %                     Dc{1}(i) = -x(i) * factor;
% %                     Dc{2}(i) = -y(i) * factor; 
%                 end
%             end
%         end
%         
%         function [c,Dc,DDc] = initialEllipse(s,x,y,z,t,center,xRadius,yRadius)
%             if length(center)~=3
%                 error('The center should have length == 3')
%             end
%             
%             Dc = []; DDc = [];
%             
%             elementSize = size(x);
%             c = zeros( elementSize );
%             
%             eqRadius = sqrt(xRadius*yRadius);
% 
%             denominator = (2*sqrt(2)*s.Cn_L)/eqRadius;
% 
%             xc = center(1);
%             yc = center(2);
%             zc = center(3);
% 
%             for i=1:numel(x)
%                 dx = (x(i)-xc);
%                 dy = (y(i)-yc);
%                 dz = (z(i)-zc);
% 
%                 c(i) = 0.5 + 0.5 * tanh( (1 - sqrt( ((dx)/xRadius)^2 + ((dy)/yRadius)^2) ) / denominator );
%             end
%         end
%         
%         % Analytical solution for omega, which complies with the solution for
%         % the variable c (see above). Omega is defined by:
%         % 
%         %   omega = 1/eps * (C^3 - 3/2*C^2 + 1/2*C) - eps * laplacian(C)
%         %
%         function [omg,Domg,DDomg] = initialOmg(s,x,y,z,t,center,radius)
%             if length(center)~=3
%                 center( numel(center)+1:4 ) = 0;
%             end
%             
%             elementSize = size(x); %[ max(1,length(x)) max(1,length(y)) max(1,length(z))];
%             
%             % VALUES
%             omg = zeros( elementSize );
%                     
%             % D VALUES
%             Domg = cell(1,s.mesh.sDim);
%             for i=1:s.mesh.sDim
%                 Domg{i} = zeros( elementSize );
%             end
%             
%             % DD VALUES
%             DDomg = cell(1,s.mesh.sDim);
%             for i=1:s.mesh.sDim
%                 DDomg{i} = zeros( elementSize );
%             end
%             
%             % Element nodes
%             if (isempty(z))
%                 z = 0;
%             end
%             
%             % Center location
%             xc = center(1);
%             yc = center(2);
%             zc = center(3);
%                         
%             denominator = (2*sqrt(2)*s.Cn_L);
%             
%             for i=1:numel(x)
%                 dx2 = (x(i)-xc)^2;
%                 dy2 = (y(i)-yc)^2;
%                 dz2 = (z(i)-zc)^2;
% 
%                 r  = sqrt( dx2 + dy2 + dz2 );
%                 r2 = r;                        
% 
%                 if (r<radius)
%                     r = 2*radius-r;
%                 end 
%                 
%                 % METHOD 2 : 1 equation ( NB: s.alpha/s.Cn is a scaling )
%                 if (r~=0)
%                     omg(i) = s.Cn_L/s.Cn * s.alpha/s.Cn_L * (s.Cn_L*sqrt(2)) / (8*r*cosh( (r-radius)/denominator )^2);
%                     %omg(i) = s.alpha/s.Cn * (s.Cn*sqrt(2)) / (8*r/radius*cosh( (r/radius-1)/denominator )^2);
%                 else
%                     omg(i) = omg(i) + 0;
%                 end
% 
%                 %% D VALUE
%                 if (r2>0.1)
%                     factor = s.alpha/s.Cn * (s.Cn*sqrt(2) * cosh( (r2-radius)/denominator ) + r2 * sinh( (r2-radius)/denominator ) ) / ( 8* (r2*cosh( (r2-radius)/denominator ))^3 );
%                 else
%                     factor = 0;
%                 end
%                 Domg{1}(i) = -(x(i)-xc) * factor;
%                 Domg{2}(i) = -(y(i)-yc) * factor;
%                 Domg{3}(i) = -(z(i)-zc) * factor;
%             end
% 
%             if (nnz(any(omg<0))>0)
%                 omg(omg<0) = min(min(min(omg(omg>0))));
%             end
%             
%             %omg = omg*0 + 1.5;
%             %omg = 0*omg;
%         end
%         
%         function [omg,Domg,DDomg] = initialEllipseOmg(s,x,y,z,t,center,xRadius,yRadius,zRadius)
%             if length(center)~=3
%                 error('The center should have length == 3')
%             end
%             
%             if nargin<9
%                 zRadius = 1;
%             end
%             
%             elementSize = size(x); %[ max(1,length(x)) max(1,length(y)) max(1,length(z))];
%             
%             % VALUES
%             omg = zeros( elementSize );
%                     
%             % D VALUES
%             Domg = cell(1,s.mesh.sDim);
%             for i=1:s.mesh.sDim
%                 Domg{i} = zeros( elementSize );
%             end
%             
%             % DD VALUES
%             DDomg = cell(1,s.mesh.sDim);
%             for i=1:s.mesh.sDim
%                 DDomg{i} = zeros( elementSize );
%             end
%             
%             % Element nodes
%             if (isempty(z))
%                 z = 0;
%             end
%             
%             eqRadius = sqrt(xRadius*yRadius*zRadius);
%             
%             xc = center(1);
%             yc = center(2);
%             zc = center(3);
%                         
%             %factor = s.alp/(s.We);
%             denominator = (2*sqrt(2)*s.Cn_L); %/ eqRadius;
%             
%             for i=1:numel(x)
%                 dx2 = ((x(i)-xc)/xRadius)^2;
%                 dy2 = ((y(i)-yc)/yRadius)^2;
%                 dz2 = ((z(i)-zc)/zRadius)^2;
%                 
%                 r  = eqRadius*sqrt( dx2 + dy2 + dz2 );
%                 r2 = r;                        
% 
%                 if (r<eqRadius)
%                     r = 2*eqRadius-r;
%                 end 
%                 
%                 % METHOD 2 : 1 equation ( NB: s.alpha/s.Cn is a scaling )
%                 if (r~=0)
%                     omg(i) = s.Cn_L/s.Cn * s.alpha/s.Cn_L * (s.Cn_L*sqrt(2)) / (8*r*cosh( (r-eqRadius)/denominator )^2);
%                     %omg(i) = s.alpha/s.Cn * (s.Cn*sqrt(2)) / (8*r/radius*cosh( (r/radius-1)/denominator )^2);
%                 else
%                     omg(i) = omg(i) + 0;
%                 end
% 
% %                 %% D VALUE
% %                 if (r2>0.1)
% %                     factor = s.alpha/s.Cn * (s.Cn*sqrt(2) * cosh( (r2-radius)/denominator ) + r2 * sinh( (r2-radius)/denominator ) ) / ( 8* (r2*cosh( (r2-radius)/denominator ))^3 );
% %                 else
% %                     factor = 0;
% %                 end
% %                 Domg{1}(i) = -(x(i)-xc) * factor;
% %                 Domg{2}(i) = -(y(i)-yc) * factor;
% %                 Domg{3}(i) = -(z(i)-zc) * factor;
%             end
% 
%             if (nnz(any(omg<0))>0)
%                 omg(omg<0) = min(min(min(omg(omg>0))));
%             end
%             
% %             
% %             
% %             
% %             for j=1:length(y)
% %                 for i=1:length(x)
% %                     r = sqrt( ((x(i)-xc)/xRadius)^2 + ((y(j)-yc)/yRadius)^2 );
% % %                     if (r~=0)
% % %                         omg(i,j,:) = factor * sqrt(2) / ( 8 * r * cosh( ( (sqrt(2)*(r-radius)) / (4*s.Cn) ) )^2 );
% % %                     else
% % %                         omg(i,j,:) = 0; % to prevent 'inf' solution
% % %                     end
% %                     
% %                     % Only c
% % % %                     c = 0.5 + 0.5 * tanh( (radius - r ) / denominator );
% % % %                     omg(i,j,:) = (4*sqrt(2) / s.Cn) * ( c.^3 - 3/2 * c.^2 + 1/2 * c ); %
% %                         
% %                     %if (r<radius)
% %                     %    r = 2*radius-r;
% %                     %end
% %                     
% %                     %omg(i,j,:) = 0.75*sqrt(2) * sinh( (r-1)/denominator ) / ( s.Cn * cosh( (r-1) / denominator )^3);
% % 
% %                     % Only laplacian
% %                     if (r~=0)
% %                         %omg(i,j,:) = omg(i,j,:) - ( 0.75*sqrt(2) * sinh( (r-1)/denominator ) * eqRadius * r - 1.5 * s.Cn*cosh( (r-1)/denominator ) ) / ...
% %                         %                                                                           ( eqRadius * r * s.Cn*cosh( (r-1)/denominator )^3 );
% %                         omg(i) = s.Cn_L/s.Cn * s.alpha/s.Cn_L * (s.Cn_L*sqrt(2)) / (8*r*cosh( (r-radius)/denominator )^2);
% %                     else
% %                         %omg(i,j,:) = omg(i,j,:) + 0;
% %                     end
% %                     
% %                     
% %                     
% % %                     epsilon = s.Cn;
% % %                     if (r~=0)
% % %                         omg(i,j,:) = omg(i,j,:) + 3*sqrt(2)*(cosh((-radius+r)*sqrt(2)/(4*epsilon))*sqrt(2)*epsilon-sinh((-radius+r)*sqrt(2)/(4*epsilon))*r) / (4*epsilon*r*cosh((-radius+r)*sqrt(2)/(4*epsilon))^3);
% % %                     else
% % %                         omg(i,j,:) = omg(i,j,:) + 0; % to prevent 'inf' solution
% % %                     end
% %                 end
% %             end
% %                         
% %             if (nnz(any(omg<0))>0)
% %                 omg(omg<0) = min(min(min(omg(omg>0))));
% %             end
% %             
% %             omg = 1+0*omg / (System.settings.phys.alpha);
%         end
%         
%         
%         
        %% Stabilize the interfaces
        % By the discretization, the interface fields (C and omega) are not 
        % in balance yet (even when the exact initial solution for both are
        % used). This function solves the system of a steady and uncoupled 
        % (velocities are zero, so no interface advection) system of
        % equations. This will only slighlty adjust both C and omega
        % fields, and ensure there is no interface movement due to
        % discretization limitations.
        function stabilizeInterface(s)
            if (System.rank==0 && System.settings.outp.verbose)
                System.newLine();
                fprintf('PHYSICS    : Interface stabilization started\n')
            end
            
            %System.setVariableOffsets(Physics);
            %System.setItem(1,'timestep',0,'%8d');
            %System.setItem(2,'time level',0,'%14.2e');
            
            temp = s.maxNonlinear;
            
            s.maxNonlinear = 5;
            
            s.coupled = false;
            
            for t=1:1

                %% Apply boundary conditions
                s.resetBoundaryConditions();
                activeSettings = System.settings;
                activeSettings.setBoundaryConditions( s );

                if (System.settings.time.spaceTime)
                    s.updateTimeSlab();
                    if (~s.steady)
                        %% Apply initial conditions
                        s.applyInitialConditions();
                    end
                elseif ~isempty(System.settings.time.steppingMethod)
                    s.copySolution(FieldType.Timelevel1);
                else
                    s.copySolution(FieldType.Timelevel1);
                end
            
            %% Solve nonlinear equations
%             settings = System.settings;
%             couplingSetting = settings.getCoupling;
%             settings.setConvergence([true; false],[true; false]);
            
%             for cIter=1:System.settings.comp.coupling.maxIter                       
                time1 = toc;
% 
%                 System.setIter('coupling',cIter);
% 
%                 isConvergedValue    = false(numel(System.settings.phys.classes),1);
%                 isConvergedResidual = false(numel(System.settings.phys.classes),1);
                
                % Solve the nonlinear equations for each element in the
                % Physics cell array.
                

                
                %for i=1:numel(System.settings.phys.classes)                
%                     if ( isa(Physics{i},'NavierStokes') )
%                         Physics{i}.isSteady = true;
%                         Physics{i}.setZeroGravity(true);
%                         settings.setCoupling(true);
% %                         settings.setInitialWeight(0);
%                     elseif ( isa(Physics{i},'CahnHilliard') )
%                         Physics{i}.isSteady = false;
%                         settings.setCoupling(false);
% %                         settings.setInitialWeight(100);
%                     end
% 
% %                     for j=1:numel(System.settings.phys.classes)
% %                         Physics{j}.activeField = FieldType.Coupled;
% %                     end
%                     
%                     for j=1:numel(System.settings.phys.classes)
%                         Physics{j}.activeField = FieldType.Interpolated;
%                         Physics{j}.updateInterpolated(Physics)
%                     end
%                     Physics{i}.solveNonLinear(Physics);

                %if (~System.settings.anal.useCellFun)
                 %   for i=1:numel(System.settings.phys.classes)  
                try
                    %s.steady = true;

                    %s.Pe = 1;
                    s.solveNonLinear();

                    %s.Pe = System.settings.phys.Pe;

                    %s.steady = false;
                catch Mexp
                    disp('ERROR in solveNonLinear (CahnHilliard.m)')
                    save(['solveNonlinear.' int2str(System.rank) '.mat']);
                end   
                    
            end
            
            s.maxNonlinear = temp;
            s.coupled = s.settings.coupled;
            
                %end
                                
%                 time2 = toc;
% 
%                 % Output the time of the coupling step, and disable the
%                 % iteration output
%                 System.setItem(3,'iter',cIter,'%4d');
%                 System.setItem(10,'time',time2-time1,'%10.2f');
%                 System.setItemFlags(11,false);
%                 System.outputNonLinear(Physics);
%                 
%                 % Update the mesh
% %                 for i=1:numel(System.settings.phys.classes) 
% %                     Physics{i}.meshUpdate(System.settings.mesh.maxLevel(i));
% %                 end
%                 if ( ( all(isConvergedValue)     && System.settings.comp.coupling.convType(1) ) || ...
%                      ( all(isConvergedResidual)  && System.settings.comp.coupling.convType(2) ) )
%                     break
%                 end
%                 
%                 %if ( all(isConvergedValue) )
%                 %    break
%                 %end
%             end
            
%             % Reset some values for further computation
%             for i=1:numel(System.settings.phys.classes)                
%                 if ( isa(s,'NavierStokes') )
%                     s.isSteady = false;
%                     s.setZeroGravity(false);
%                 end
%             end

%             settings.setCoupling(couplingSetting);
%             settings.setConvergence([true; false],[true; false]);
            
            if (System.rank==0 && System.settings.outp.verbose)
                fprintf('PHYSICS    : Interface stabilization finished, wall clock time : %f\n',toc-time1)
            end
        end

    end
    
    methods (Static)
        function out = load(solution)
            if (~isa(solution,'Solution'))
                error('The first argument of load should be a Solution object')
            end
            out = CahnHilliard;
            out.loadObject(solution);
        end 
    end    
end

