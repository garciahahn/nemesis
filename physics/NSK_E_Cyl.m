classdef NSK_E_Cyl < Physics
    % NSK_E_CYL
    properties (Access=private)
        REI; WEI;
    end
    
    properties (Constant)
        % Variable ids for this physics
        T = 1;
    end
    
    properties
        Re; We; k; Cv;
    end
    
    methods
        function s = NSK_E_Cyl(mesh,settings)
            s = s@Physics(mesh);
            
            s.shortName = 'NSK_E_Cyl';
            s.longName  = 'NSK_Energy_Cylindrical';
            s.name      = 'NSK_E_Cyl';
            
            s.nEq       = 1;
            s.variables = {'T'};
            
            s.initialize(settings);
        end
        
        function initialize(s,settings)
            initialize@Physics(s,settings);

            s.steady    = settings.steady;
            
            s.Re        = System.settings.phys.Re;
            s.We        = System.settings.phys.We;
            s.k         = System.settings.phys.k;
            s.Cv        = System.settings.phys.cv;
            
            s.REI       = 1.0/s.Re;
            s.WEI       = 1.0/s.We;
        end

        function time = setupEquations(s)

            t1 = toc;
            
            weight1 = s.eqWeights(1);
            
            U	= FieldCollection.get('u');
            V   = FieldCollection.get('v');
            R   = FieldCollection.get('R');
            q   = FieldCollection.get('q');
                        
            for e=s.mesh.eRange
                element	= s.mesh.getElement(e);         % Select the active element
                el      = element.localID;              % Get the local element number
                
                % elemental operators
                H       = element.getH;
                Dx      = element.getDx;
                Dy      = element.getDy;
                if (s.steady)
                    Dt = 0;
                else
                    Dt = element.getDt;
                end
                Dxx     = element.getDxx;
                Dyy     = element.getDyy;
                Wel     = element.getW;
                matZ    = element.getMatZ;              %#ok<NASGU>
                vecZ    = element.getVecZ;              %#ok<NASGU>
                
                % Coordinates
                [Xc, ~, ~, ~] = element.getNodes();
                Xc = Xc(:);
                Xc = 1./Xc;
                % Due to the symmetry boundary conditions at the center of
                % the domain (r=0), it is safe to equate the factor 1/r = 0,
                % even though the real value is infinite. This is because
                % all variables in the system that use the expression 1/r
                % are 0 when r = 0. This makes easier to work around the
                % singularity problem when r = 0.
                Xc(Xc == Inf) = 0;

                % u-velocity derivatives
                Uk      = U.val2(e,s.mesh);
                Ux      = U.x(FieldType.Current,e);
                Uy      = U.y(FieldType.Current,e);
                
                % v-velocity derivatives
                Vk      = V.val2(e,s.mesh);
                Vx      = V.x(FieldType.Current,e);
                Vy      = V.y(FieldType.Current,e);
                
                % density derivatives
                Rk      = R.val2(e,s.mesh);

                % Auxiliary terms
                divU    = Ux + Xc .* Uk + Vy;

                PR      = 24 ./ (3-Rk).^2;
                
                L = (PR.*divU) .* H + (s.Cv*Rk) .* Dt...
                    + (s.Cv*Rk.*Uk-s.k*Xc) .* Dx + (s.Cv*Rk.*Vk) .* Dy...
                    + (Dxx + Dyy) .* (-s.k);

                G = (2*Ux.*Ux + Uy.*Uy + Vx.*Vx + 2*Vy.*Vy + 2*Uy.*Vx...
                    - 2/3 * divU.*divU)*s.REI;
            
                % Assign L and G
                s.opL{el} = [ L ];
                s.opG{el} = [ G ];

                s.opL{el}   = s.opL{el} * superkron(eye(s.Nvar),s.mesh.Z{e});
                s.W{el}     = repmat(Wel,s.Nvar,1);
                s.sqrtW{el} = repmat(sqrt(Wel),s.Nvar,1);
            end
            
            t2 = toc;
            time = t2-t1;
        end              
    end
    
    methods (Static)
        function out = load(solution)
            if (~isa(solution,'Solution'))
                error('The first argument of load should be a Solution object')
            end
            
            out = NSK_E_Cyl;
            out.loadObject(solution);
        end 
    end
    
end

