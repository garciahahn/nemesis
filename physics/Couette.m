classdef Couette < NavierStokes
    % Couette 2D Steady Navier-Stokes benchmark
    %   This class is nothing more than a simple Navier-Stokes case
        
    methods
        function s = Couette(mesh,settings)
            s = s@NavierStokes(mesh,settings);
            
            s.shortName = 'Couette';
            s.longName  = 'Couette';
            s.name      = 'Couette';
            
            s.steady  = true;
                        
%             Re          = System.settings.phys.Re;
%             g           = System.settings.phys.g;
%             maxNIter    = System.settings.comp.nonlinear.maxIter;
%             maxCIter    = System.settings.comp.coupling.maxIter;

            %s.initialize(Re,[],[],g,[],1,1,maxNIter,maxCIter)
        end
    end
    
    methods (Access=protected)
        function loadObject(s,solution)
            % load from parent
            loadObject@NavierStokes(s,solution)
            
            % specific loads for the Poiseuille object
            % none
        end
    end
    
    methods (Static)
        function out = load(s)
            out = Couette();
            out.loadObject(s);
        end
    end
    
end

