classdef EnergyEquation < Physics
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here    
    properties (Access=private)
        REI; PRI;
    end
    
    properties 
        % NS properties
        Re; Pr; 
        lamRho; lamMu; lamk; lamcp;
        sharpRho; sharpMu;
        
        dynamicWeight = false;
        computeDynamicWeight = false;
        
        withInterfaceForcing = true;
        zeroGravity = false;
    end
    
    methods
        function s = EnergyEquation(mesh,settings)
            s = s@Physics(mesh);
            
            s.shortName = 'EE';
            s.longName  = 'Energy';
            s.name      = 'EE';
            
            s.nEq       = 1;
            s.variables = {'T'};
            
            s.initialize(settings);
        end
        
        function initialize(s,settings)
            initialize@Physics(s,settings);

            s.steady    = settings.steady;
            
            s.Re        = System.settings.phys.Re;
            s.Pr        = System.settings.phys.Pr;
            s.lamRho    = System.settings.phys.lamRho;
            s.lamMu     = System.settings.phys.lamMu;
           
            s.lamk      = System.settings.phys.k(2) / System.settings.phys.k(1);
            s.lamcp     = System.settings.phys.cp(2) / System.settings.phys.cp(1);
            
            s.REI  = 1.0/s.Re;
            s.PRI  = 1.0/s.Pr;
            
            if (isfield(settings,'sharpRho'))
                s.sharpRho = settings.sharpRho;
            end
            
            if (isfield(settings,'sharpMu'))
                s.sharpMu = settings.sharpMu;
            end
        end
                
        % 
        function time = setupEquations(s,step)
            t1 = toc;

            weight1 = s.eqWeights(1);
            
            U	= FieldCollection.get('u');
            V   = FieldCollection.get('v');
            C   = FieldCollection.get('c');
            T   = FieldCollection.get('T');
            
            CH = PhysicsCollection.get(2);
            
%             % Droplet/bubble center of gravity velocity at temporal
%             % quadrature points
            if (System.settings.phys.dynamicReferenceFrame)
                    CH.computeCenterOfMassVelocity(true,false);       % determine the center-of-mass velocity: compute the enclosed mass, and without overIntegration
                    uc = CH.cmv_total(:,1);
                    vc = CH.cmv_total(:,2);
                %fprintf('WARNING : USING DYNAMIC REFERENCE FRAME')
            else
                uc  = [];
                vc  = [];
                absCenterVelocity = [];
            end
                        
            for e=s.mesh.eRange
                
                % Select the active element
                element     = s.mesh.getElement(e);
                
                % Extract field data
                if (C==C)
                    Ck    	= C.val2(e,s.mesh);
                    dCkdx   = C.x2(e,s.mesh);
                    dCkdy   = C.y2(e,s.mesh);
                else
                    Ck      = 1;
                    dCkdx   = 0;
                    dCkdy   = 0;
                end
                
                Uk   	= U.val2(e,s.mesh);
                Vk    	= V.val2(e,s.mesh);
                
                dUkdx   = U.x(FieldType.Current,e);
                dUkdy   = U.y(FieldType.Current,e);
                
                dVkdx   = V.x(FieldType.Current,e);
                dVkdy   = V.y(FieldType.Current,e);
                
                Txx     = T.xx(FieldType.Current,e);
                Tyy     = T.yy(FieldType.Current,e);

                % Moving reference frame
                if (System.settings.phys.dynamicReferenceFrame)
                    % x-direction
                    if ( ~isempty(uc) )
                        Uk = reshape( Uk, element.finite.qSize );
                        for i=1:numel(uc)
                            Uk(:,:,i) = Uk(:,:,i) + uc(i);
                        end
                        Uk = Uk(:);
                    end
                    
                    % y-direction
                    if ( ~isempty(vc) )
                        Vk = reshape( Vk, element.finite.qSize );
                        for i=1:numel(uc)
                            Vk(:,:,i) = Vk(:,:,i) + vc(i);
                        end
                        Vk = Vk(:);
                    end
                end
                
                if (s.sharpRho)
                    dispersed   = (Ck>0.5);
                else
                    dispersed   = Ck;
                end
                rho         = (1-s.lamRho)*Ck + s.lamRho;
                
                k           = (1-s.lamk )*Ck + s.lamk;
                cp          = (1-s.lamcp)*Ck + s.lamcp;
                mu          = (1-s.lamMu)*Ck + s.lamMu;
                
                dkdx        = (1-s.lamk )*dCkdx ;
                dkdy        = (1-s.lamk )*dCkdy ;
                
                % Get all elemental operators/arrays
                H       = element.getH;
                Dx      = element.getDx;
                Dy      = element.getDy;
                if (s.steady)
                    Dt = 0;
                else
                    Dt = element.getDt;
                end
                Dxx     = element.getDxx;
                Dyy     = element.getDyy;
                Wel     = element.getW;
                matZ    = element.getMatZ;
                vecZ    = element.getVecZ;
                                
                Laplacian = Dxx + Dyy;
                
                theta = 1.0;      % theta = 1 means fully implicit
                
                
                
                %PRIlocal = s.PRI * k ./ cp;
                
                % Convection and diffusion terms
                %   NB: k and cp depend on the phase!
                Conv     = Uk.*Dx + Vk.*Dy + 0*( dUkdx + dVkdy ) .* H;
                Diff     = theta * s.REI * s.PRI ./ cp .* ( k .* Laplacian + dkdx.*Dx + dkdy.*Dy );
                fv       = vecZ - (1-theta) * s.REI * s.PRI * k ./ cp .* (Txx+Tyy);
                
%                 mu       = (1-s.lamMu )*Ck + s.lamMu ;
                
                viscDiss = 0*(s.REI / 1868 * mu) .* ( (dUkdy+dVkdx).^2 + 2*(dUkdx.^2+dVkdy.^2) - 2/3 * (dUkdx+dVkdy).^2 );
                                                    
                qFlux    = 0;
                
                % Get the local element number
                el = element.localID;
                
                %                              T
                s.opL{el} = weight1 * [ rho.*( Dt+Conv )-Diff-qFlux ] ;

                s.opG{el} = weight1 * [ fv + viscDiss ];
                                            
                s.opL{el} = s.opL{el} * superkron(eye(s.Nvar),s.mesh.Z{e});
                
                s.W{el}      = repmat(Wel,s.Nvar,1);
                s.sqrtW{el}  = repmat(sqrt(Wel),s.Nvar,1);
            end
            
            t2 = toc;
            time = t2-t1;
        end              
                
%         function out = getConvergence(s,fieldType,residual)
%             if nargin==2
%                 switch fieldType
%                     case FieldType.Nonlinear
%                         out = reshape( System.settings.comp.EE.nConvergence, [numel(System.settings.comp.EE.nConvergence),1] );
%                     case FieldType.Coupled
%                         out = reshape( System.settings.comp.EE.cConvergence, [numel(System.settings.comp.EE.cConvergence),1] );
%                 end
%             else
%                 out = reshape( System.settings.comp.EE.rConvergence, [numel(System.settings.comp.EE.rConvergence),1] );
%             end
%         end
        
%         function setNormItems(s,fieldType,iter)
%             L2Norm = s.getL2Norm(fieldType);
%             System.setItems(s,iter,L2Norm);
%         end
               
        function updateBC(s,step)
            for b=1:numel(s.boundary)
                if ~isempty(s.boundary{b})
                    if (s.boundary{b}.requiresNonLinearUpdate)
                    	boundary_dst = s.boundary{b};
                        boundary_src = s.boundary{b+1};
                        Tm_src  = s.meanTemperature( boundary_src );
                        Tm_dst  = s.meanTemperature( boundary_dst );
                        Tm_goal = s.settings.meanTemp_in;
                        variable    = 1;
                        if (Tm_src~=0)
                            scaling(1) = Tm_src;
                            scaling(2) = Tm_dst;
                            scaling(3) = Tm_goal;
                            %fprintf('Scaling = %f\n',scaling)
                            s.copyBoundarySolution(boundary_src,boundary_dst,variable,scaling)
                            s.setBC( boundary_dst, variable, s.fields{variable}, BCType.Weak, BCType.Dirichlet );       % zeroGradient: dT/dx=0
                        end
                        boundary_dst.requiresNonLinearUpdate = true;
                    end
                end
            end
        end

        function prescribed = getPrescribed(s,e)
            if nargin==2
                prescribed = [s.fields{1}.getPrescribed(e)];
            else
                prescribed = cell(s.mesh.numLocalElements,1);
                for e=1:numel(prescribed)
                    element = s.mesh.getLocalElement(e);
                    prescribed{e} = s.getPrescribed( element.globalID );
                end
            end
        end
        
        function out = getPrescribedValues(s,e)
            out = [s.fields{1}.getPrescribedValues(e)];
        end
        
        % The mean temperature (Tm) is defined by: Tm = Et_dot / m_dot
        %
        % See p.427 of Fundamentals of Heat & Mass Transfer, 
        % 4th edition, 1996, authors: Incropera & DeWitt
        %
        % Parameters:
        %  boundary     plane to integrate over
        %  disc         discretization (optional, for over-integration)
        %
        function Tm = meanTemperature(s,boundary,disc_in)
            % Initialize the variables
            Et_dot  = 0;
            m_dot   = 0;
            Tm      = 0;
            
            U = FieldCollection.get('u');
            V = FieldCollection.get('v');
            C = FieldCollection.get('c');
            
            %cv  = 1;
            %rho = 1;
            %rho         = (1-s.lamRho)*Ck + s.lamRho;    
            %k           = (1-s.lamk)*Ck + s.lamk;
            %cp          = (1-s.lamcp)*Ck + s.lamcp;
            
            R = 8.3144598;
            
            if (System.settings.phys.dynamicReferenceFrame)
                CH = PhysicsCollection.get(2);
                CH.computeCenterOfMassVelocity(true,false);       % determine the center-of-mass velocity: compute the enclosed mass, and without overIntegration
                uc = CH.cmv_total(:,1);
                vc = CH.cmv_total(:,2);
            end
            
            % If no C is available assume C=1 and vc=0
            if (C~=C)
                rho = @(e,disc) 1;
                cv  = @(e,disc) R + (1-s.lamcp) * System.settings.phys.cp(1);
                uc = 0;
                vc = 0;
            else
                rho = @(e,disc) (1-s.lamRho)*C.getValueInterpolated( e ,disc, s.mesh) + s.lamRho;
                cv  = @(e,disc) R + ((1-s.lamcp)*C.getValueInterpolated( e ,disc, s.mesh) + s.lamcp) * System.settings.phys.cp(1);
            end

            if (~s.mesh.spaceTime)
                % no space-time mesh
                if (boundary.dir==2)
                    vel = @(e,disc) U.getValueInterpolated( e ,disc, s.mesh) + mean(uc);
                elseif (boundary.dir==1)
                    vel = @(e,disc) V.getValueInterpolated( e ,disc, s.mesh) + mean(vc);
                end
                
            else
                if (boundary.dir==1)
                    vel = @(e,disc) U.getValueInterpolated( e ,disc, s.mesh) + mean(uc);
                elseif (boundary.dir==2)
                    vel = @(e,disc) V.getValueInterpolated( e ,disc, s.mesh) + mean(vc);
                end
            end
            
            T   = @(e,disc) s.fields{1}.getValueInterpolated( e ,disc, s.mesh);
            
            Et      = @(e,disc) rho(e,disc) .* vel(e,disc) .* cv(e,disc) .* (T(e,disc));
            Et_dot  = boundary.computeIntegral( Et, false );
            
%             if (Et_dot==0)
%                 s.fields{1}.add( 1 );
%                 Et      = @(e,disc) rho(e,disc) .* vel(e,disc) .* cv(e,disc) .* (T(e,disc));
%                 Et_dot  = boundary.computeIntegral( Et, false );
%             end
            
            m       = @(e,disc) rho(e,disc) .* vel(e,disc) .* cv(e,disc);
            m_dot   = boundary.computeIntegral( m, false );
            
            % For verification purposes:
            %t       = @(e,disc) 1;
            %area    = boundary.computeIntegral( t, false );
            
            Tm = Et_dot ./ m_dot;
            
%             if (System.rank==0)
%                 %fprintf('Total mass : %f (effective radius : %f)\n',s.mass,sqrt(s.mass/pi))
%                 fprintf('Et_dot = % 8.2e\n',Et_dot);
%                 fprintf('m_dot  = % 8.2e\n',m_dot);
%                 fprintf('Tm     = % 8.2e\n',Tm);
%             end
        end
        
        function plotNusselt(s)            
            layer = 1;
            
            id = 0;
            for e=s.mesh.eRange %1:numel(s.alpha)
                id = id+1;
                element = s.mesh.getElement(e);
                
                field = s.fields{1};
                
                T  = field.val2(e,s.mesh);
                Tx = field.y2(e,s.mesh);
                
                [X,Y,Z,T] = element.getNodes;
                X = squeeze(X); Y = squeeze(Y);
                Z = squeeze(Z); T = squeeze(T);
                
                Tm = 0;
                
                data = Tx ./ (0-T);
                data = reshape( data, element.finite.qSize );
                
                if (System.settings.time.spaceTime)
                    if (System.settings.mesh.sdim==2)
                        if (layer==1)
                            plotData = data(:,:,end);
                        elseif (layer==0)
                            plotData = data(:,:,1);
                        end
                        surf(X(:,:,end),Y(:,:,end),plotData,'FaceColor','interp');
                    else
                        if (layer==1)
                            plotData = data(:,end);
                        elseif (layer==0)
                            plotData = data(:,1);
                        end
                        surf(X,T,data,'FaceColor','interp');
                    end
                else
                    plotData = data;
                    if (System.settings.mesh.sdim==1)
                        plot(X(:,end),plotData,'Color','k');
                    elseif (System.settings.mesh.sdim==2)
                        surLSQf(X,Y,plotData,'FaceColor','interp');
                    else
                        error('Check Subfield.plot : only sDim == 1 and == 2 are supported')
                    end
                end
                
                hold on;
            end
            
            if (System.settings.mesh.dim==3)
                x0 = s.mesh.X(1,1);
                x1 = s.mesh.X(2,1);
                y0 = s.mesh.X(1,2);
                y1 = s.mesh.X(2,2);

                pbx = x1 - x0;
                pby = y1 - y0;
                pbaspect([pbx pby 1])

                %zlim([-10 10]);
                
                colorbar;
                view([25,25]);                
            end
        end

        function output(s,outputDir,id)
            data.t = s.getT( 1, s.getElementalAlpha(Variable.t) );
            for e=NMPI.instance.eStart:NMPI.instance.eEnd
                data.t{e} = reshape(data.t{e},s.mesh.getElement(e).disc.Q);
                data.t{e} = data.t{e}(:,:,end);
            end
                        
            for e=1:s.mesh.getNumLocalElements
                data.x{e,1} = s.mesh.getLocalElement(e).xelem;
                data.y{e,1} = s.mesh.getLocalElement(e).yelem;
            end
            
            data.eStart = NMPI.instance.eStart;
            data.eEnd   = NMPI.instance.eEnd;
            
            save([outputDir 'info-' num2str(id,'%.4d')  '--' num2str(NMPI.instance.rank,'%.3d') '.mat'],'data') ;
        end   

        function plotExact(s,fig,position)
            if nargin==2
                position = 1;
            end
            
            s.plotMode = 1;
            s.plot(fig,position);
            s.plotMode = 0;
        end
        
        function plotInterpolated(s,fig,position)
            if nargin==2
                position = 1;
            end
            
            s.plotMode = 2;
            s.plot(fig,position);
            s.plotMode = 0;
        end
        
        function plotAll(s,fig,data,mesh)
            
            if nargin==3
                mesh = s.mesh;
            end
            
            screensize = get( groot, 'Screensize' );
            sizex = 1600;
            sizey = 600;
            set(fig,'position',[0.5*(screensize(3)-sizex) 0.5*(screensize(4)-sizey) sizex sizey]); 
            
            rows = 1;
            cols = 1;
            
            pbx = s.mesh.x1 - s.mesh.x0;
            pby = s.mesh.y1 - s.mesh.y0;
            
            
            % T-plot
            t = data{1};
            minT    = min( cellfun(@(x) min(x(:)),t) );
            maxT    = max( cellfun(@(x) max(x(:)),t) );
            
            maxT = 1;
            
            pos = 1;
            delete(subplot(rows,cols,pos))
            subplot(rows,cols,pos)
            xlim([mesh.x0 mesh.x1])
            ylim([mesh.y0 mesh.y1])
            %zlim([minU    maxU   ])
            pbz = maxT - minT;
            pbaspect([pbx pby pbz])
            view([0,90])
            grid off;
            hold on;
            
            for e=1:mesh.numLocalElements
                element = mesh.getLocalElement(e);
                surf(element.xelem,element.yelem,t{e},'FaceColor','interp');
            end
            
%             % U-velocity at x=0.5
%             pos = 4;
%             delete(subplot(rows,cols,pos))
%             subplot(rows,cols,pos)
%             for e=1:mesh.numLocalElements
%                 element = mesh.getLocalElement(e);
%                 z=interp2(element.xelem,element.yelem,u{e},0.5,element.yelem);
%                 plot(element.yelem,z,'k');
%                 hold on;
%             end
%             xlim([0 1])
%             view([90,-90])
%             
%             % V-velocity at x=0.5
%             pos = 5;
%             delete(subplot(rows,cols,pos))
%             subplot(rows,cols,pos);
%             for e=1:mesh.numLocalElements
%                 element = mesh.getLocalElement(e);
%                 z=interp2(element.xelem,element.yelem,v{e},0.5,element.yelem);
%                 plot(element.yelem,z,'k');
%                 hold on;
%             end
%             ylim([-1e-3 1e-3])
%             view([90,-90]) 
        end
        
        % Default refinement criteria (false)
        function criteria = getRefinementCriteria(s,e)
            criteria(1) = false;
        end
        
        function out = save(s,filename)
            out = [];
            
            if (NMPI.instance.rank==0 || System.settings.outp.parallelWrite)
                EE                = struct;
                EE.class          = class(s);
                EE.name           = s.name;
                EE.mesh           = s.mesh.save;
                
                EE.settings       = s.settings;
                
                EE.fields       = cell(s.Nvar,1);
                for n=1:s.Nvar
                    EE.fields{n} = save( s.fields{n} );
                end
                
                % Additional NS settings
                EE.steady     	= s.steady;                 % True if the system of equations is steady
                EE.fdata        = s.fdata;
                EE.exact        = s.exact;                  % True if this physics has an exact solution
                EE.maxLevel     = s.maxLevel;               % Maximum level for AMR
                EE.eqWeights    = s.eqWeights;           	% Weights for every equation
                EE.nConvType    = s.nConvType;              % Residual convergence criteria based on [ value; residual ]
                EE.cConvType    = s.cConvType;              % Coupling convergence criteria based on [ value; residual ]
                EE.nMaxIter     = s.nMaxIter;           	% Maximum number of nonlinear iterations
                EE.cMaxIter     = s.cMaxIter;           	% Maximum number of coupling iterations
                EE.nConvergence = s.nConvergence;       	% Nonlinear convergence for each variable
                EE.cConvergence = s.cConvergence;        	% Coupling convergence for each variable
                EE.rConvergence = s.rConvergence;           % Residual convergence for each equation and the whole system
                
                out = EE;
            end
        end
        
       	function preTimeLoop(s)
            if (s.usePreTimeLoop)
                
                s.preTimeStep();
                
                %% Apply boundary conditions
                s.resetBoundaryConditions();
                activeSettings = System.settings;
                activeSettings.setBoundaryConditions( s );

                % By solving the steady equations, the solution will be
                % obtained faster
                s.steady    = true;
                s.coupled   = true; 
                %s.eps       = 0.05;
                %s.eqWeights = s.settings.eqWeights;
                %s.eqWeights(1) = 0.01;
                %s.eqWeights(2) = 0.01;

                timeMethod    = s.timeMethod;
                
                s.timeMethod = TimeMethod.Steady;
                
    %             %% Apply temporal conditions
    %             if (System.settings.time.spaceTime)
    %                 s.updateTimeSlab();
    %                 if (~s.steady)
    %                     s.applyInitialConditions();
    %                 end
    %             elseif ~isempty(System.settings.time.steppingMethod)
    %                 physics.copySolution(FieldType.Timelevel1);
    %             end

                %% Solve nonlinear equations                
                s.solveNonLinear();

                s.steady        = s.settings.steady;
                s.coupled       = s.settings.coupled;
                s.timeMethod    = timeMethod;
                
                %s.eps       = s.settings.eps;
                %s.eqWeights = s.settings.eqWeights;
            end
        end
    end
        
%     methods (Access=protected)
%         function loadObject(s,solution)
%             EE = solution.data.EE;
%             
%             s.name = EE.name;        
%             s.tConvergence = EE.tConvergence;
%                        
%             if (isa(EE.mesh,class(MeshRefine3D)))
%                 fprintf('Please save the mesh as a struct\n')
%                 mesh = EE.mesh;
%                 meshStruct = mesh.save;
%             elseif (isstruct(EE.mesh))
%                 meshStruct = EE.mesh;
%             else
%                 error('Undefined mesh type')
%             end
%             
%             stde = LSQdisc_4.load(solution.data.standardElement);
%             mesh = MeshRefine3D.load(stde,meshStruct);
%                                     
%             if (nargin>0)
%                 s.initialize(mesh,EE.Re,EE.Pr,NS.lamRho,15,15);
%             end
%             
%             s.fields{1}.current.loadAlphaArray(NS.t);
%             
%             s.fields{1}.coupled{1}.loadAlphaArray(NS.t);
%         end
%         
%     end
    
    methods (Static)
        function out = load(solution)
            if (~isa(solution,'Solution'))
                error('The first argument of load should be a Solution object')
            end
            
            out = EnergyEquation;
            out.loadObject(solution);
        end
        
        function plotParallel(nproc,variable)
            
            fig = figure(1);
            clf
            
            for i=1:nproc
                disp('NEW PROC')
                temp = load(['EE_' int2str(i-1) '.mat']);
                temp.EE.plotVariable(fig,0,variable,'')
                hold on
            end
            
            drawnow;            
        end 
    end
    
end

