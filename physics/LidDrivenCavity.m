classdef LidDrivenCavity < NavierStokes
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        beta = 50;
    end
    
    methods
        function s = LidDrivenCavity(mesh,Re,maxNIter)
            if (nargin==1)
                s.Re = s.RE;
            else
                s.Re = Re;
            end
            
            s.isSteady = true;
            
            s.mesh = mesh;
            
            s.initialize(mesh,s.Re,0,0,1,1,maxNIter);
        end
                
        % BC for a Poiseuille flow
        function applyBoundaryConditions(s,bcType)
            % Set the function handle which provides the analytical solution
            fh = @s.lidVelocity;
            
            % Velocity u:
            s.applyDirichletField( Neighbor.Left    , s.uField,  0, bcType );     % fixedValue: uniform 0
            s.applyDirichletField( Neighbor.Right   , s.uField,  0, bcType );     % fixedValue: uniform 0
            s.applyDirichletField( Neighbor.Bottom  , s.uField,  0, bcType );     % fixedValue: uniform 0
            s.applyDirichletField( Neighbor.Top     , s.uField,  fh, bcType );     % function

            % Velocity v:
            s.applyDirichletField( Neighbor.Left    , s.vField,  0, bcType );     % fixedValue: uniform 0
            s.applyDirichletField( Neighbor.Right   , s.vField,  0, bcType );     % fixedValue: uniform 0
            s.applyDirichletField( Neighbor.Bottom  , s.vField,  0, bcType );     % fixedValue: uniform 0
            s.applyDirichletField( Neighbor.Top     , s.vField,  0, bcType );     % fixedValue: uniform 0

            % Pressure: p=0 on left/right (in-/outflow), dp/dn=0 on bottom/top
%             s.applyNeumann( Neighbor.Left      , Variable.p,  0, bcType, setAlpha );     % zeroGradient: dp/dx=0
%             s.applyNeumann( Neighbor.Right     , Variable.p,  0, bcType, setAlpha );     % zeroGradient: dp/dx=0
%             s.applyNeumann( Neighbor.Bottom    , Variable.p,  0, bcType, setAlpha );     % zeroGradient: dp/dy=0
%             s.applyNeumann( Neighbor.Top       , Variable.p,  0, bcType, setAlpha );     % zeroGradient: dp/dy=0
            
            s.applyDirichletLine( Neighbor.Left, Neighbor.Bottom, s.pField,  0, BCType.Strong );     % fixedValue: p=0
            fprintf('Lid driven cavity boundary conditions applied\n')
        end      
        
        function plot(s,fig,position)
            uField = s.uField; %s.getValues(Variable.u);
            vField = s.vField; %s.getValues(Variable.v);
            pField = s.pField; %s.getValues(Variable.p);
            
            if nargin==2
                position = 1;
            end
            
            u = cell(s.mesh.getNumLocalElements,1);
            v = cell(s.mesh.getNumLocalElements,1);
            p = cell(s.mesh.getNumLocalElements,1);
            
            if (position==0)
                for e=1:s.mesh.getNumElements
                    u{e} = reshape(uField{e},s.mesh.getElement(e).disc.Q);
                    u{e} = u{e}(:,:,1)';

                    v{e} = reshape(vField{e},s.mesh.getElement(e).disc.Q);
                    v{e} = v{e}(:,:,1)';

                    p{e} = reshape(pField{e},s.mesh.getElement(e).disc.Q);
                    p{e} = p{e}(:,:,1)';
                end
            else
                for e=1:s.mesh.getNumElements
                    u{e} = reshape(uField{e},s.mesh.getElement(e).disc.Q);
                    u{e} = u{e}(:,:,end)';

                    v{e} = reshape(vField{e},s.mesh.getElement(e).disc.Q);
                    v{e} = v{e}(:,:,end)';

                    p{e} = reshape(pField{e},s.mesh.getElement(e).disc.Q);
                    p{e} = p{e}(:,:,end)';
                end
            end
            
            s.plotAll(fig,u,v,p);
            
            drawnow;
        end
           
        function plotAll(s,fig,u,v,p)
            screensize = get( groot, 'Screensize' );
            sizex = 1000;
            sizey = 1000;
            set(fig,'position',[0.5*(screensize(3)-sizex) 0.5*(screensize(4)-sizey) sizex sizey]); 
            
            % U-plot
            delete(subplot(3,3,1))
            subplot(3,3,1)
            for e=1:s.mesh.getNumElements
                element = s.mesh.getElement(e);
                surf(element.xelem,element.yelem,u{e});
                hold on;
            end
            view([0,90])
            
            % V-plot
            delete(subplot(3,3,4))
            subplot(3,3,4)
            for e=1:s.mesh.getNumElements
                element = s.mesh.getElement(e);
                surf(element.xelem,element.yelem,v{e});
                hold on;
            end
            view([0,90])
            
            % P-plot
            delete(subplot(3,3,7))
            subplot(3,3,7)
            for e=1:s.mesh.getNumElements
                element = s.mesh.getElement(e);
                surf(element.xelem,element.yelem,p{e});
                hold on;
            end
            view([0,90])
            
            % U-velocity at x=0.5
            delete(subplot(3,3,2))
            subplot(3,3,2)
            for e=1:s.mesh.getNumElements
                element = s.mesh.getElement(e);
                z=interp2(element.xelem,element.yelem,u{e},0.5,element.yelem);
                plot(z,element.yelem,'k');
                hold on;
            end
            xlim([-0.5,1.0])
            view([0,90])
            
            % V-velocity at y=0.5
            delete(subplot(3,3,5))
            subplot(3,3,5);
            for e=1:s.mesh.getNumElements
                element = s.mesh.getElement(e);
                z=interp2(element.xelem,element.yelem,v{e},element.xelem,0.5);
                plot(element.xelem,z,'k');
                hold on;
            end
            view([0,90]) 
            
            % Velocity contours
            for var=1:s.Nvar
            	delete(subplot(3,3,3+(var-1)*3))
                subplot(3,3,3+(var-1)*3)
                for e=1:s.mesh.getNumElements
                    element = s.mesh.getElement(e);

                    %z=interp2(element.xelem,element.yelem,p{e},0.5,element.yelem);
                    %plot(element.yelem,z,'k');

                    if var==1
                        contourf(element.xelem,element.yelem,hypot(u{e},v{e}),[0:0.02:0.3]);
                    elseif (var==2)
                        diff = 0.02;
                        [X,Y] = meshgrid(0:diff:1,0:diff:1);
                        u2=interp2(element.xelem,element.yelem,u{e},X,Y);
                        v2=interp2(element.xelem,element.yelem,v{e},X,Y);
                        quiver(X,Y,u2,v2,'color',[0 0 0]); 
                    else
%                         diff = 0.02;
%                         startx = element.xelem(1)-mod(element.xelem(1),diff)+diff : diff2 : element.xelem(end)-mod(element.xelem(end),diff);
%                         starty = element.yelem(1)-mod(element.yelem(1),diff)+diff : diff2 : element.yelem(end)-mod(element.yelem(end),diff);
%                         if (startx(end)~=element.xelem(end))
%                             startx = [element.xelem(1) startx element.xelem(end)];
%                         end
%                         if (starty(end)~=element.yelem(end))
%                             starty = [element.yelem(1) starty element.yelem(end)];
%                         end
%                         [X,Y] = meshgrid(startx,starty);
%                         u2=interp2(element.xelem,element.yelem,u{e},X,Y);
%                         v2=interp2(element.xelem,element.yelem,v{e},X,Y);
%                         
%                         diff2 = 0.02;
%                         startx = element.xelem(1)-mod(element.xelem(1),diff2)+diff2 : diff2 : element.xelem(end)-mod(element.xelem(end),diff2);
%                         starty = element.yelem(1)-mod(element.yelem(1),diff2)+diff2 : diff2 : element.yelem(end)-mod(element.yelem(end),diff2);
%                         
%                         startx = element.xelem(1):0.02:element.xelem(end);
%                         starty = element.yelem(1):0.02:element.yelem(end); %element.yelem;%*ones(size(startx));
%                         if (startx(end)~=element.xelem(end))
%                             startx = [startx element.xelem(end)];
%                         end
%                         if (starty(end)~=element.yelem(end))
%                             starty = [starty element.yelem(end)];
%                         end
%                         [X2,Y2] = meshgrid(startx,starty);
%                         streamline(X,Y,u2,v2,X2,Y2);
%                         levels = 0.01:0.05:0.5;
%                         contour(element.xelem,element.yelem,hypot(u{e},v{e}),levels);
%                         hold on
%                         levels = 0.0005:0.0005:0.005;
%                         contour(element.xelem,element.yelem,hypot(u{e},v{e}),levels);
                    end

%                     ind = (1:length(s.residual{e})/s.Nvar) + (var-1)*(length(s.residual{e})/s.Nvar);        % indices of only 1 variable
%                     data = s.residual{e}(ind);
%                     data = reshape(data,length(element.xelem),length(element.yelem),length(element.zelem));
%                     surf(element.xelem,element.yelem,abs(data(:,:,end))); % residual
                    hold on;
                end
                xlim([0,1])
                ylim([0,1])
                view([0,90])
            end

            delete(subplot(3,3,8))
            subplot(3,3,8)
            for e=1:s.mesh.getNumElements
                element = s.mesh.getElement(e);
                surf(element.xelem,element.yelem,s.residualNorm(e)*ones(length(element.xelem),length(element.yelem))); % residual
                hold on;
            end
            view([0,90])     
        end
        
        % Function to provide the lid velocity in the xz-plane (if the 
        % length of y==1), or in the yz-plane (if the length of x==1)
        function out = lidVelocity(s,variable,x,y,z)
            if (length(y)==1)
                out = zeros(length(x),length(z));
                w = x;
            elseif (length(x)==1)
                out = zeros(length(x),length(z));
                w = y;
            else
                disp('ERROR: this lid velocity request is not supported')
            end
            
            for i=1:length(w)
                wPos = w(i);
                if (w(i)<=0.5)
                    out(i,:) = tanh(s.beta*wPos);
                else
                    out(i,:) = -tanh(s.beta*(wPos-1));
                end
            end
        end
    end
    
end

