classdef Physics < handle
    %UNTITLED3 Summary of this class goes here
    %   Detailed explanation goes here
    properties (Access=protected)
        mesh;
    end
    
    properties
        % General properties
        Nvar;
        nEq;
        
        K; G; W; sqrtW;
        opL; opG;        
        
%         GM;
        dof;
        
        hasStrongBC = true;
        hasWeakBC;
        
%         alpha;
%         alphac;     % coupling
%         alphai;     % non-linear 
        
%         alphaElement;
%         alphaElementi;
%         alphaElementc;

        name;
%         problemCase;
        
        boundary;
        
        residual;
        residualNorm;
        
%         alphaTemp;
        
        % Boundary / initial conditions
%         initialType;
%         prescribed;
        
        % Numerical properties
        iter_cg;
        iter;
        epsilon = 1E-12 ;
%         wi = 1.0;
%         wc = 1.0;
        wbc = 10000;            % weight for boundary condition
        wic = 10000;            % weight for initial condition
        
%         stop_iter = 1E-3;   % for coupling convergence
        nIter, maxNIter;    % Number of non-linear iterations
        
        % Timing parameters
        timings;
    end
    
    methods
        
        % This function updates the mesh: refinement where the physics
        % needs it (through checkRefinement). The updateAMR first refines
        % the elements based on the 'needsRefinement' flags, and then checks 
        % iteratively for the 1-level irregularity constraint next.
        % Finally, when convergence of refinement has been reached, the new
        % limits for the parallel processes are set, and the AMR mapping arrays 
        % Z are created.
        function isUpdated = refineMesh(s,maxLevel)

            isUpdated = false;
            
%             s.mesh.setElementLimits();
            needsRefinement = s.checkRefinement(maxLevel);
            
%             while s.mesh.getMaxLevel < maxLevel
            if nnz(needsRefinement)>0
                %needsRefinement = s.checkRefinement(maxLevel);
                [numElementsAMR] = s.mesh.updateAMR(needsRefinement);

%                 fprintf('Number of elements during refinement : %d %d %d\n',numElementsAMR(1),numElementsAMR(2),numElementsAMR(3));
                
                if (numElementsAMR(1)~=numElementsAMR(2))
                    fprintf('AMR        : increase of the number of elements from %4d to %4d due to refinement upto level %d\n',numElementsAMR(1),numElementsAMR(2),s.mesh.getMaxLevel)
                end
                if (numElementsAMR(2)~=numElementsAMR(3))
                    fprintf('AMR        : increase of the number of elements from %4d to %4d due to 1-level irregularity constraint\n',numElementsAMR(2),numElementsAMR(3)) 
                end
                if (numElementsAMR(1)<numElementsAMR(3))
                    isUpdated = true;
                    s.mesh.isReady = false;
                end
                if (numElementsAMR(1)==numElementsAMR(3)) % || s.mesh.getMaxLevel==maxLevel)
                    isUpdated = false;
%                     break;
                end
                
                s.mesh.setElementLimits();
%                 needsRefinement = s.checkRefinement(maxLevel);
            end
        end
            
        function updateMesh(s,maxLevel)
            s.mesh.update(maxLevel);
                        
            %[mutualDof,numMutualDof] = s.mesh.getMutualDof(2);
            %fprintf('numMutualDof between rank %d and rank 2 = %d\n',Parallel3D.MPI.rank,numMutualDof)
            
            fprintf('There are %d localDofs on rank %d\n',s.mesh.getNumLocalDof,Parallel3D.MPI.rank)
            
%             s.setGM;
        end
        
        function time = setupNormalEquations(s)
            t1 = toc;
                        
            for e=1:s.getNumLocalElements
                
                                % Select the active element
                %                 element = s.mesh.getElement(e);                
                %                 W       = element.getW;                
                %                 s.W{e} = superkron( eye(s.Nvar), diag(W) );
                                % s.K{e} = dsyrk(full( sqrt(s.W{e})*s.opL{e}*kron(eye(s.Nvar),Z{e}) ) );
                                % Wt = repmat(sqrt(W), s.Nvar, 1);
                                % ZCell = repmat({Z{e}}, 1, s.Nvar);
                                % Zt = blkdiag(ZCell{:});
                                % s.K{e} = dsyrk( full( Wt.*s.opL{e}*Zt ) );                
                %                 Wt = repmat(sqrt(W), s.Nvar, 1);
                                % ZCell = repmat({Z{e}}, 1, s.Nvar);
                                % Zt = blkdiag(ZCell{:});
                %                 s.K{e} = dsyrk( full( Wt .* s.opL{e} * superkron(eye(s.Nvar),Z) ) );
                                % s.K{e} = s.opL{e}' * s.W{e} * s.opL{e};
                                % s.K{e} = kron(eye(s.Nvar),Z{e})' * s.K{e} * kron(eye(s.Nvar),Z{e}) ;
                %                 s.G{e} = kron(eye(s.Nvar),Z)' * s.G{e} ;

                s.K{e} = dsyrkL( full( s.sqrtW{e} .* s.opL{e} ) );
                s.G{e} = s.opL{e}' * (s.W{e} .* s.opG{e});

            end
            
            %% Weak boundaries
            for i=1:6
                if i<5
                    weight = s.wbc;
                else
                    weight = s.wic;
                end
                
                activeBoundary = s.boundary{i};
                
                for be=1:activeBoundary.getNumLocalElements

                    e = activeBoundary.getLocalElementNumbers(be);

                    if inrange(e,1,s.getNumLocalElements)
                              
                        offset = 0;
                        
                        for n=1:s.Nvar
                            field = s.getField(n);

                            numDof = nnz(~field{e}.prescribed);

                            if (~isempty(activeBoundary.opL{be,n}))  
                                ind = (1:numDof) + offset;
                                         
                                s.K{e}(ind,ind) = s.K{e}(ind,ind) + weight * dsyrkL( full( sqrt(activeBoundary.W{be}) .* activeBoundary.opL{be,n}(:,~field{e}.prescribed(:)) ) );
                                s.G{e}(  ind  ) = s.G{e}(  ind  ) + weight * activeBoundary.opL{be,n}(:,~field{e}.prescribed(:))' * (activeBoundary.W{be} .* activeBoundary.opG{be,n});
                            end
                            
                            offset = offset + numDof;
                        end
                    end
                   
                end
            end
            
            t2 = toc;
            time = t2-t1;
        end
        
        function boundary = getBoundary(s,boundaryNumber)
            boundary = s.mesh.getBoundary(boundaryNumber) ;
        end      
        
%         function out = getAlphaN(s,element,variable,wi)
%             if (nargin==3)
%                 wi = s.wi;
%             end
%             out = ( wi ) * s.alphaElement{element}(:,variable) + ...
%                   (1-wi) * s.alphaElementi{element}(:,variable);
%         end
%         
%         function out = getAlphaC(s,element,variable,wc)
%             if (nargin==3)
%                 wc = s.wc;
%             end
%             out = ( wc ) * s.alphaElement{element}(:,variable) + ...
%                   (1-wc) * s.alphaElementc{element}(:,variable);
%         end
        
%         function out = getAlpha(s,element,variable)
%             out = s.alphaElement{element}(:,variable);
%         end
        
%         function out = getElementalAlpha(s,variable)
%             out = cell(s.mesh.getNumLocalElements,1);
%             if (nargin==2)
%                 for e=1:s.mesh.getNumLocalElements
% %                     element = s.mesh.getLocalElement(e);
% 
%                     GM = s.getGM(e,variable);
% 
% %                     GM = s.getGM(e+Parallel3D.MPI.eStart-1,variable);
% %                     element = s.mesh.getLocalElement(e);
% %                     GM = element.GM+(variable-1)*s.mesh.getNumLocalDof;
% %                     
% %                     if (Parallel3D.MPI.rank==1)
% %                         variable
% %                         GM
% %                         GM2+s.mesh.getNumLocalDof
% %                         size(s.alpha)
% %                     end
% %                     exit
%                     out{e} = s.alpha( GM );
%                 end
%             elseif (nargin==1)
%                 GM = s.getGM;
%                 for e=1:s.mesh.getNumLocalElements
%                     out{e} = s.alpha( GM{e} );
% %                     out{e} = s.alpha( GM{e+Parallel3D.MPI.eStart-1} );
%                 end 
%             end
%         end
        
%         function setElementalAlpha(s)
%             s.alphaElement = cell(s.mesh.getNumLocalElements,1);
%             tempGM = s.getGM;
%             for e=1:s.getNumLocalElements
% %                 element = s.mesh.getLocalElement(e);
% %                 GM = element.GM;
% %                 s.alphaElement{e} = s.alpha( GM(:) );
% %                 s.alphaElement{e} = reshape(s.alphaElement{e}, size(GM) );
%                 s.alphaElement{e} = s.alpha( tempGM{e}(:) );
%                 s.alphaElement{e} = reshape(s.alphaElement{e}, size(tempGM{e}) );
%             end
%             if isempty(s.alphaElementi)
%                 s.alphaElementi = s.alphaElement;
%                 s.alphaElementc = s.alphaElement;
%             end
%         end
%         
%         function setGlobalAlpha(s)
%             tempGM = s.getGM;
%             for e=1:s.getNumLocalElements
%                 s.alpha( tempGM{e}(:) ) = s.alphaElement{e}(:);
%             end
%         end
        
        
        % Function to set the initial alpha based on boundary and initial
        % conditions
        function setInitialAlpha(s,boundaryNumber,field,derivative,value)
            boundary = s.mesh.getBoundary(boundaryNumber);    

            variable = field.getVariable;
            alpha = field.getAlphaArray;
            
            if (derivative==0)
                temp    = boundary.H;
                tempPos = boundary.Hpos;
            elseif (derivative==1)
                temp    = boundary.Dn;
                tempPos = boundary.Dpos;
            end

            elemental = true; % elemental / global
            
            if (elemental)

                for be=1:boundary.getNumLocalElements
                    e = boundary.getLocalElementNumbers(be);
                    
                    if inrange(e,1,s.getNumLocalElements)
                        
                        element = s.mesh.getLocalElement(e);

                        GM = s.getGM(e+Parallel3D.MPI.eStart-1,variable);                   % Get the GatheringMatrix for this GLOBAL element/variable
%                         GM = element.GM;
                        GM2{e,1} = GM(:,:,end);
                                                     
                        % Step 1: Initialize alpha( tempPos ) to 0
                        alpha( GM(tempPos) ) = 0;

                        % Step 2: Assign boundary condition values
                        if (~isa(value,'function_handle'))
                            if ~isa(value,'Field') 
                                bc = (temp==1);                  % Get only the elements that are exactly equal to 1
                                bc(:,all(~bc,1)) = [];                % Remove empty columns
                                bc(all(~bc,2),:) = [];                % Remove empty rows

                                valueVector = value*ones( nnz(bc), 1 );

                                [~,col]     = find(temp==1);
                                bcGM        = GM(col);
                            else
                                bc = temp*element.Z;                  % 
                                bc(:,all(~bc,1)) = [];                % Remove empty columns
                                bc(all(~bc,2),:) = [];                % Remove empty rows

                                valueVector = value{e}(:,:,end);

                                colNonZero  = ~all(~temp,1);      % Find all nonzero column numbers
                                bcGM        = GM(colNonZero);           % Use the nonzero columns to reduce the GM for just the boundary
                            end
                        else
                            % use the function handle, which provides a valueVector based on (x,y,z) location 
                            bc = temp;                       % 
                            bc(:,all(~bc,1)) = [];                % Remove empty columns
                            bc(all(~bc,2),:) = [];                % Remove empty rows

                            element = s.mesh.getElement(e);
                            x = element.xelem;
                            y = element.yelem;
                            z = element.zelem;

                            switch boundary.side
                                case Neighbor.Left
                                    x = s.mesh.x0;
                                case Neighbor.Right
                                    x = s.mesh.x1;
                                case Neighbor.Bottom
                                    y = s.mesh.y0;
                                case Neighbor.Top
                                    y = s.mesh.y1;
                                case Neighbor.Front
                                    z = s.mesh.z0;
                                case Neighbor.Back
                                    z = s.mesh.z1;
                                case 9
                                    x = -0.5;
                                    y = -0.5;
                            end

                            valueVector = value(variable,x,y,z);

                            colNonZero  = ~all(~temp,1);      % Find all nonzero column numbers
                            bcGM        = GM(colNonZero);           % Use the nonzero columns to reduce the GM for just the boundary
                        end

                        % Compute the corresponding alpha for the
                        % prescribed boundary condition
                        alpha( bcGM ) = bc \ valueVector(:);
%                         end

                        if (boundaryNumber==5)
                            A{e,1} = bc' * bc;
                            b{e,1} = bc' * valueVector(:);
                        end
                    end
                end
%                 end
                
            else
%                 for be=1:boundary.getNumElements
%                     e = boundary.getElementNumbers(be);
%                     if (e>=Parallel3D.MPI.eStart && e<=Parallel3D.MPI.eEnd)
% 
%                         GM = s.getGM(e,variable);                   % Get the GatheringMatrix for this element/variable
% 
%                         % Step 1: Initialize alpha( tempPos ) to 0
%                         s.alpha( GM(tempPos) ) = 0;
% 
%                         % Step 2: Assign boundary condition values
%                         if (~isa(value,'function_handle'))
%                             if length(value)==1 
%                                 bc = (temp==1);                  % Get only the elements that are exactly equal to 1
%                                 bc(:,all(~bc,1)) = [];                % Remove empty columns
%                                 bc(all(~bc,2),:) = [];                % Remove empty rows
% 
%                                 valueVector = value*ones( nnz(bc), 1 );
% 
%                                 [~,col]     = find(temp==1);
%                                 bcGM        = GM(col);
%                             else
%                                 bc = temp;                       % 
%                                 bc(:,all(~bc,1)) = [];                % Remove empty columns
%                                 bc(all(~bc,2),:) = [];                % Remove empty rows
% 
%                                 valueVector = value{e}(:,:,end);
% 
%                                 colNonZero  = ~all(~temp,1);      % Find all nonzero column numbers
%                                 bcGM        = GM(colNonZero);           % Use the nonzero columns to reduce the GM for just the boundary
%                             end
%                         else
%                             % use the function handle, which provides a valueVector based on (x,y,z) location 
%                             bc = temp;                       % 
%                             bc(:,all(~bc,1)) = [];                % Remove empty columns
%                             bc(all(~bc,2),:) = [];                % Remove empty rows
% 
%                             element = s.mesh.getElement(e);
%                             x = element.xelem;
%                             y = element.yelem;
%                             z = element.zelem;
% 
%                             switch boundary.side
%                                 case Neighbor.Left
%                                     x = s.mesh.x0;
%                                 case Neighbor.Right
%                                     x = s.mesh.x1;
%                                 case Neighbor.Bottom
%                                     y = s.mesh.y0;
%                                 case Neighbor.Top
%                                     y = s.mesh.y1;
%                                 case Neighbor.Front
%                                     z = s.mesh.z0;
%                                 case Neighbor.Back
%                                     z = s.mesh.z1;
%                                 case 9
%                                     x = -0.5;
%                                     y = -0.5;
%                             end
% 
%                             valueVector = value(variable,x,y,z);
% 
%                             colNonZero  = ~all(~temp,1);      % Find all nonzero column numbers
%                             bcGM        = GM(colNonZero);           % Use the nonzero columns to reduce the GM for just the boundary
%                         end
% 
%                         % Compute the corresponding alpha for the
%                         % prescribed boundary condition
%                         s.alpha( bcGM ) = bc \ valueVector(:);
%                         A()
%                     end
%                 end
            end
            
            if (boundaryNumber==7)
                GMflag = false(s.getNumDof,1);
                fprintf('numBoundaryElements  : %d\n',boundary.getNumLocalElements)
                fprintf('numDof in Physics    : %d\n',s.getNumDof)
%                 fprintf('max value in Physics : %d\n',max(GM2{be}(:)))
%                 counter = 0;
                for be=1:boundary.getNumLocalElements
                    e = boundary.getLocalElementNumbers(be);
                    if inrange(e,1,s.getNumLocalElements)
%                         counter = counter+1;
                        GMflag( GM2{e}(:) ) = true;
                    end
                end            
%                 numFreeDof = nnz(GMflag);
%                 GMrenumbering = zeros(s.getNumDof,1);
%                 GMrenumbering(GMflag) = (1:numFreeDof);
% %                 counter = 0;
%                 for be=1:boundary.getNumLocalElements
%                     e = boundary.getLocalElementNumbers(be);
%                     if inrange(e,1,s.getNumLocalElements)
% %                         counter = counter+1;
%                         GMnew{e,1} = GMrenumbering(GM2{e}(:));
%                     end
%                 end
                
                for be=1:boundary.getNumLocalElements
                    s.prescribed{be}(:,2) = true;
                    s.prescribed{be}(1:(layer-1)*numel(GM2{be})+1:end,1) = true;
                    s.prescribed{be}(layer*numel(GM2{be})+1:end,1) = true;
                end                
                
                alpha_in = s.alpha( GMflag,1 );
                [alf,iter]=PrecElemCG_par_light(A,b,GM2, s.prescribed,s.mesh.mutualGM, s.getNumDof,s.epsilon, alpha_in);

%                 alpha2 = zeros(s.getNumDof,1);
%                 alpha2( GMflag,1 ) = alf;
%                 s.alpha = alpha2;
                
                s.alpha( GMflag,1 ) = alf;
            end
            
%             field.setAlphaSolution(alpha);
            
        end
        
        
        function applyConditions(s,bcType)            
%             s.applyInitialConditions(bcType);
            s.applyBoundaryConditions(bcType);
        end
        
       % Function to set a Dirichlet boundary condition for the 'variable' to a 'boundary'
        %
        %   boundary    : side of the domain (1-6)
        %   variable    : u(1), v(2), w(3), p(4)
        %   value       : constant value (for now)
        %   type        : Weak(1), Strong(2)
        %   init        : true/false; compute alpha?
        %
%         function applyDirichlet(s,boundaryNumber,variable,value,bcType,init)
%             boundary = s.mesh.getBoundary(boundaryNumber);
%             
%             activeFEBoundary = s.boundary{boundaryNumber};     
%             
%             if (init)
%                 s.setInitialAlpha(boundaryNumber,variable,0,value);
%             end
%             
%             % For a strong bc only prescribed flags are set to true, for a
%             % weak bc 
%             switch bcType
%                 case BCType.Strong
%                     for be=1:boundary.getNumLocalElements
%                         e = boundary.getLocalElementNumbers(be);
%                         if inrange(e,1,s.getNumLocalElements)
%                             s.prescribed{e}(boundary.Hpos,variable) = true;
%                         end
%                     end
%                     
%                 case BCType.Weak
%                     for be=1:boundary.getNumLocalElements
%                         e = boundary.getLocalElementNumbers(be);
%                         
%                         if inrange(e,1,s.getNumLocalElements)
%                             
%                             element=s.mesh.getLocalElement(e);
%                             
%                             indp = (1:boundary.getLocalElements(be).disc.dofev) + (variable-1)*boundary.getLocalElements(be).disc.dofev;
%                             indq = boundary.getHposQ(element.disc.dofeq);
%                             
%                             % Step 1: Determine the valueVector
%                             if (~isa(value,'function_handle'))
%                                 if length(value)==1 
%                                     valueVector = value*ones( nnz(indq), 1 );
%                                 else
%                                     valueVector = value{e}(:,:,end);
%                                 end
%                             else
%                                 x = element.xelem;
%                                 y = element.yelem;
%                                 z = element.zelem;
%                                 
%                                 switch boundary.side
%                                     case Neighbor.Left
%                                         x = s.mesh.x0;
%                                     case Neighbor.Right
%                                         x = s.mesh.x1;
%                                     case Neighbor.Bottom
%                                         y = s.mesh.y0;
%                                     case Neighbor.Top
%                                         y = s.mesh.y1;
%                                     case Neighbor.Front
%                                         z = s.mesh.z0;
%                                     case Neighbor.Back
%                                         z = s.mesh.z1;
%                                     case 9
%                                         x = -0.5;
%                                         y = -0.5;
%                                 end
%                                 
%                                 valueVector = value(variable,x,y,z);
%                             end
%                             
%                             if (isempty(activeFEBoundary.opL{be,variable}))
%                                 activeFEBoundary.opL{be,variable} = sparse(element.disc.dofevq,element.disc.dofev);
%                                 activeFEBoundary.opG{be,variable} = sparse(element.disc.dofevq,1);
%                                 activeFEBoundary.W{be}            = sparse(element.disc.dofevq,1);
%                             end
% 
%                             activeFEBoundary.opL{be,variable}( indq(:), : ) = boundary.H * s.mesh.getZ(e);      % unit operator
%                             activeFEBoundary.W{be}( indq(:) )               = (boundary.Wdisc)./(boundary.J(be));
%                             activeFEBoundary.opG{be,variable}( indq(:) )    = valueVector(:);
%                         end
%                     end
%             end
%         end
        
        function applyDirichletField(s,boundaryNumber,field,value,bcType,init)
            boundary = s.mesh.getBoundary(boundaryNumber);
            
            activeFEBoundary = s.boundary{boundaryNumber};     
            
            if (init)
                field.setInitialAlpha(boundaryNumber,0,value);
            end
                        
            % For a strong bc only prescribed flags are set to true, for a
            % weak bc 
            switch bcType
                case BCType.Strong
                    for be=1:boundary.getNumLocalElements
                        e = boundary.getLocalElementNumbers(be);
                        if inrange(e,1,s.getNumLocalElements)
                            field.setPrescribed(e,boundary.Hpos);
                        end
                    end
                    
                case BCType.Weak
                    for be=1:boundary.getNumLocalElements
                        e = boundary.getLocalElementNumbers(be);
                        
                        if inrange(e,1,s.getNumLocalElements)
                            
                            element=s.mesh.getLocalElement(e);
                            
%                             indp = (1:boundary.getLocalElements(be).disc.dofev) + (variable-1)*boundary.getLocalElements(be).disc.dofev;
                            indq = boundary.getHposQ(element.disc.dofeq);
                            
                            % Step 1: Determine the valueVector
                            if (~isa(value,'function_handle'))
                                if ~isa(value,'Field')
                                    valueVector = value*ones( nnz(indq), 1 );
                                else
                                    tmp_value = reshape(value{e},size(indq));
                                    valueVector = tmp_value(:,:,end);
                                end
                            else
                                x = element.xelem;
                                y = element.yelem;
                                z = element.zelem;
                                
                                switch boundary.side
                                    case Neighbor.Left
                                        x = s.mesh.x0;
                                    case Neighbor.Right
                                        x = s.mesh.x1;
                                    case Neighbor.Bottom
                                        y = s.mesh.y0;
                                    case Neighbor.Top
                                        y = s.mesh.y1;
                                    case Neighbor.Front
                                        z = s.mesh.z0;
                                    case Neighbor.Back
                                        z = s.mesh.z1;
                                    case 9
                                        x = -0.5;
                                        y = -0.5;
                                end
                                
                                valueVector = value(field.getVariable,x,y,z);
                            end
                            
                            if (isempty(activeFEBoundary.opL{be,field.getVariable}))
                                activeFEBoundary.opL{be,field.getVariable} = sparse(element.disc.dofevq,element.disc.dofev);
                            end
                            if (isempty(activeFEBoundary.opG{be,field.getVariable}))
                                activeFEBoundary.opG{be,field.getVariable} = sparse(element.disc.dofevq,1);
                            end
                            if (isempty(activeFEBoundary.W{be}))
                                activeFEBoundary.W{be}                     = sparse(element.disc.dofevq,1);
                            end

                            activeFEBoundary.opL{be,field.getVariable}( indq(:), : ) = boundary.H * s.mesh.getZ(e);      % unit operator
                            activeFEBoundary.W{be}( indq(:) )                        = (boundary.Wdisc)./(boundary.J(be));
                            activeFEBoundary.opG{be,field.getVariable}( indq(:) )    = valueVector(:);
                        end
                    end
            end
        end
        
%         function applyNeumann(s,boundaryNumber,variable,value,bcType,init)
%             boundary = s.mesh.getBoundary(boundaryNumber);
%             activeFEBoundary = s.boundary{boundaryNumber};   
%             
%             if (init)
%                 s.setInitialAlpha(boundaryNumber,variable,1,value);
%             end
%             
%             switch bcType
%                 case BCType.Strong
%                     for be=1:boundary.getNumLocalElements
%                         e = boundary.getLocalElementNumbers(be);
%                         if inrange(e,1,s.getNumLocalElements)
%                             s.prescribed{e}(boundary.Dpos,variable) = true;
%                         end
%                     end                    
%                 case BCType.Weak
%                     for be=1:boundary.getNumLocalElements
%                         e = boundary.getLocalElementNumbers(be);
%                         if inrange(e,1,s.getNumLocalElements)
%                             
%                             element=s.mesh.getLocalElement(e);
%                             
%                             indp = [1:boundary.getLocalElements(be).disc.dofev] + (variable-1)*boundary.getLocalElements(be).disc.dofev;
%                             indq = boundary.getDposQ(element.disc.dofeq);
%                             
%                             if (~isa(value,'function_handle'))
%                                 if length(value)==1
%                                     valueVector = value*ones( nnz(indq), 1 );
%                                 else
%                                     valueVector = value;
%                                 end                            
%                             else
%                                 x = element.xelem;
%                                 y = element.yelem;
%                                 z = element.zelem;
%                                 
%                                 switch boundary.side
%                                     case Neighbor.Left
%                                         x = s.mesh.x0;
%                                     case Neighbor.Right
%                                         x = s.mesh.x1;
%                                     case Neighbor.Bottom
%                                         y = s.mesh.y0;
%                                     case Neighbor.Top
%                                         y = s.mesh.y1;
%                                     case Neighbor.Front
%                                         z = s.mesh.z0;
%                                     case Neighbor.Back
%                                         z = s.mesh.z1;
%                                     case 9
%                                         x = -0.5;
%                                         y = -0.5;
%                                 end
%                                 
%                                 valueVector = value(variable,x,y,z);
%                             end
% 
%                             if (isempty(activeFEBoundary.opL{be,variable}))
%                                 activeFEBoundary.opL{be,variable} = sparse(element.disc.dofevq,element.disc.dofev);
%                                 activeFEBoundary.opG{be,variable} = sparse(element.disc.dofevq,1);
%                                 activeFEBoundary.W{be}            = sparse(element.disc.dofevq,1);
%                             end
% 
%                             activeFEBoundary.opL{be,variable}( indq(:), : ) = boundary.Dn * s.mesh.getZ(e);      % unit operator
%                             activeFEBoundary.W{be}( indq(:) )               = (boundary.Wdisc)./(boundary.J(be));
%                             activeFEBoundary.opG{be,variable}( indq(:) )    = valueVector(:); 
%                         end
%                     end
%             end
%         end
        
        function applyNeumannField(s,boundaryNumber,field,value,bcType,init)
            boundary = s.mesh.getBoundary(boundaryNumber);
            activeFEBoundary = s.boundary{boundaryNumber};   
            
%             if (init)
%                 s.setInitialAlpha(boundaryNumber,variable,1,value);
%             end
            field.setInitialAlpha(boundaryNumber,1,value);
            
            switch bcType
                case BCType.Strong
                    for be=1:boundary.getNumLocalElements
                        e = boundary.getLocalElementNumbers(be);
                        if inrange(e,1,s.getNumLocalElements)
                            field.setPrescribed(e,boundary.Dpos);
%                             s.prescribed{e}(boundary.Dpos,variable) = true;
                        end
                    end                    
                case BCType.Weak
                    for be=1:boundary.getNumLocalElements
                        e = boundary.getLocalElementNumbers(be);
                        if inrange(e,1,s.getNumLocalElements)
                            
                            element=s.mesh.getLocalElement(e);
                            
%                             indp = [1:boundary.getLocalElements(be).disc.dofev] + (variable-1)*boundary.getLocalElements(be).disc.dofev;
                            indq = boundary.getDposQ(element.disc.dofeq);
                            
                            if (~isa(value,'function_handle'))
                                if length(value)==1
                                    valueVector = value*ones( nnz(indq), 1 );
                                else
                                    valueVector = value;
                                end                            
                            else
                                x = element.xelem;
                                y = element.yelem;
                                z = element.zelem;
                                
                                switch boundary.side
                                    case Neighbor.Left
                                        x = s.mesh.x0;
                                    case Neighbor.Right
                                        x = s.mesh.x1;
                                    case Neighbor.Bottom
                                        y = s.mesh.y0;
                                    case Neighbor.Top
                                        y = s.mesh.y1;
                                    case Neighbor.Front
                                        z = s.mesh.z0;
                                    case Neighbor.Back
                                        z = s.mesh.z1;
                                    case 9
                                        x = -0.5;
                                        y = -0.5;
                                end
                                
                                valueVector = value(field.getVariable,x,y,z);
                            end
                            
                            if (isempty(activeFEBoundary.opL{be,field.getVariable}))
                                activeFEBoundary.opL{be,field.getVariable} = sparse(element.disc.dofevq,element.disc.dofev);
                            end
                            if (isempty(activeFEBoundary.opG{be,field.getVariable}))
                                activeFEBoundary.opG{be,field.getVariable} = sparse(element.disc.dofevq,1);
                            end
                            if (isempty(activeFEBoundary.W{be}))
                                activeFEBoundary.W{be}                     = sparse(element.disc.dofevq,1);
                            end

                            activeFEBoundary.opL{be,field.getVariable}( indq(:), : ) = boundary.Dn * s.mesh.getZ(e);      % unit operator
                            activeFEBoundary.W{be}( indq(:) )                        = (boundary.Wdisc)./(boundary.J(be));
                            activeFEBoundary.opG{be,field.getVariable}( indq(:) )    = valueVector(:); 
                        end
                    end
            end
        end
        
%         function time = processWeakBC(s)
%             t1 = toc;
%             
% %             for i=q:6
% %                 boundary = s.mesh.getBoundary(boundaryNumber);
% %                 
% %                 s.opL
% %             end
%                 
%             
%             t2 = toc;
%             time = t2-t1;
%         end
        
        % This function reduces opL and moves the prescribed strong
        % solution to the rhs opG.
        function time = processStrongBC(s)
            t1 = toc;
            
            for e=1:s.getNumLocalElements
                
                prescribed = s.getPrescribed(e);
                
                % Prescribed vector
                numPrescribed = nnz(prescribed);
                if (numPrescribed>0)
                    prescribedVector 	= s.getPrescribedValues(e);

                    % opL
                    opL               	= s.opL{e};                             % copy current opL
                    opLprescribed      	= opL(:,prescribed);            % prescribed section of opL
                    opL(:,prescribed)  	= [];                                   % remove prescribed columns

                    % opG
                    opG               	= s.opG{e};                             % copy current opG
                    opG             	= opG - opLprescribed*prescribedVector; % move prescribed part to the rhs

                    % Save new operators
                    s.opL{e}                = opL;
                    s.opG{e}                = opG;
                end
            end
            
            t2 = toc;
            time = t2-t1;
        end
        
        function solveNonLinear(s,maxIter,iterative)
            for n=1:maxIter

                fprintf('=============== %s : NON LINEAR STEP %.4d ===============\n',s.name,n)
                
                % s.updateMesh(maxLevel); 

                s.setup(n);           

                s.solve(iterative);

                % s.computeResidual();

                if ( s.isConverged(SolutionMode.Nonlinear) )
                    break
                end
            end 
        end
        
        function solve(s,method)
            
%             time0 = 0;
%             time1 = 0;
%             time2 = 0;
            
%             time0 = s.processWeakBC();
            time1 = s.processStrongBC();
            time2 = s.setupNormalEquations;
            
            fprintf('TIMINGS : strongBC = %f   setupNE = %f\n',time1,time2)
                        
            % Save the current solution if non-linear relaxation is used
%             if (s.wi<1)
%                 s.alphai = s.alpha;
%                 s.alphaElementi = s.alphaElement;
%             end
            if (method==1 || Parallel3D.MPI.nproc>1)
                % Iterative
%                 alphaTmp = s.alpha;
    
                alphaTmp = s.getAlpha();
                
                fprintf('size of alpha = %d',size(alphaTmp))
                
                t1 = toc;
                [alpha,s.iter_cg] = PrecElemCG_par_light(s.K,s.G,s.getGMFree,s.mesh.mutualGM,s.getNumDof,s.epsilon,alphaTmp);
                t2 = toc;
                fprintf('Time in Matlab solver = %f\n',t2-t1);
                
                s.setAlpha(alpha);
                
%                 t3 = toc;
%                 [s.alpha,s.iter_cg] = PrecElemCG_par_mex(s.K,s.G,s.getGMFree,s.prescribed,s.getNumDof,s.epsilon,alphaTmp);
%                 t4 = toc;
%                 fprintf('Time in Mex solver    = %f\n',t4-t3);
            else
                % Direct
                alphaTmp = s.getAlpha();
                temp = Solver(s.K,s.G,s.getGMFree,s.getNumDof,alphaTmp);
                s.setAlpha(temp.x);
            end
            
            % make elementalAlpha 
%             s.setElementalAlpha();
        end
        
        function setGM(s)
            s.GM = s.mesh.octTree.getGM;
            
            if (s.dof~=s.mesh.dofPerVariable * s.Nvar)
                s.dof = s.mesh.dofPerVariable * s.Nvar;

                totalDOF = NMPI_Allreduce(s.dof,1,'+',Parallel3D.MPI.mpi_comm_world) ; 

                if (Parallel3D.MPI.rank==0 )
                    fprintf('%s : Total number of DOF : %d\n',s.name,totalDOF)
                end
            end            
        end
        
        function out = getGM(s,element,var)
            if (nargin==3)
%                 t3=toc;
%                 out = s.mesh.octTree.getGM{element};
%                 t4=toc;
%                 t1=toc;
                out = s.mesh.octTree.getGM{element} + (var-1)*s.mesh.octTree.getDofv;
%                 t2=toc;
%                 fprintf('time in GM (nargin==3) %f %f\n',t2-t1,t4-t3)
            elseif (nargin==1)
%                 t3=toc;
                out = cell(s.mesh.getNumLocalElements,1);
                for e=1:s.getNumLocalElements
                    temp = s.mesh.octTree.getGM{e}(:);
                    for i=2:s.Nvar
                        temp = [temp (temp(:,1) + (i-1)*s.mesh.octTree.getDofv)];
                    end
                    out{e} = temp;
                end
                disp('WARNING: Physics.getGM with nargin==1, which might be inefficient!')
%                 t4=toc;
%                 fprintf('time in GM (nargin==1) %f\n',t4-t3)
            end
        end
        
        function GM = getGMFree(s)
            GM     = cell(s.mesh.getNumLocalElements,1);
            numDof = zeros(s.Nvar,1);

            for e=1:s.getNumLocalElements
                for i=1:s.Nvar
                    field = s.getField(i);
                    if (i==1)
                        GM{e}     = s.mesh.octTree.getGM{e}( ~field{e}.prescribed(:) );
                        numDof(i)   = numDof(i)+numel(GM{e,i});
                    else
                        varGM       = s.mesh.octTree.getGM{e}( ~field{e}.prescribed(:) );
                        numDof(i)   = numDof(i)+numel(varGM);
                        GM{e}       = [GM{e};varGM+(i-1)*s.mesh.dofPerVariable];
                    end
                end
                % fprintf('Variable %d has %d DOF\n',i,numDof(i));
            end
        end
        
        function out = getNumDof(s)
            out = s.mesh.getDofv * s.Nvar;
        end
        
        function out = getNumLocalDof(s)
            if (isempty(s.mesh.getNumLocalDof))
                s.mesh.setDofRank();
            end
            out = s.mesh.getNumLocalDof * s.Nvar;
        end
        
        function plotMesh(s,fig,position,title)
            s.mesh.plot(fig,0,title);
        end
        
        function plotResidualNorm(s)
            for e=1:s.mesh.getNumGlobalElements
                element = s.mesh.getGlobalElement(e);
                surf(element.xelem,element.yelem,s.residualNorm(e)*ones(length(element.xelem),length(element.yelem))); % residual
                hold on;
            end
            view([0,90])  
        end
        
        function plotVariable(s,fig,position,variable,titleString,layer)
            if (nargin<6)
                layer = 1;
            end
            
            if (position<=1)
                clf(fig)
                screensize = get( groot, 'Screensize' );
                sizex = 1000;
                sizey = 1000;
                
%                 if (s.problemCase==ProblemCase.LidDrivenCavity)
%                     sizex = 1200;
%                     sizey = 1000;   
%                 end
                
                set(fig,'position',[0.5*(screensize(3)-sizex) 0.5*(screensize(4)-sizey) sizex sizey]); 
            end
            
            field = s.getField(variable);
            data = field;
            
%             data = s.getValues(variable);
%             data = s.c;
             
            if (position>0)
                subplot(2,2,position);
            end

            dir = 0;

            for e=1:s.mesh.getNumGlobalElements
                if (size(data{e},2)==1)
                    data{e} = reshape(data{e},s.mesh.getGlobalElement(e).disc.Q);
        %             c{e}(1:end-1:end,1:end-1:end,1)
                    [X,Y] = meshgrid( s.mesh.getGlobalElement(e).xelem,s.mesh.getGlobalElement(e).yelem );
                    if (layer==1)
                        surf(X,Y,data{e}(:,:,end)');
                    elseif (layer==0)
                        surf(X,Y,data{e}(:,:,1)');
                    end
                else
                    [X,Y] = meshgrid( s.mesh.getGlobalElement(e).xelem,s.mesh.getGlobalElement(e).yelem );
        %             surf(X,Y,c{e}(1:5,1:5,5)');
%                     surf(X,Y,data(1:size(data,1),1:size(data,2),1)');
                    if (layer==1)
                        surf(X,Y,data{e}(:,:,end)');
                    elseif (layer==0)
                        surf(X,Y,data{e}(:,:,1)');
                    end
        %             surf(mesh.xgbl{e}(1:end-1:end),mesh.ygbl{e}(1:end-1:end),c{e}(1:end-1:end,1:end-1:end,1));
                end

            %             surf(mesh.xgbl{e},mesh.ygbl{e},c{e}(:,:,1));
            %             surf(mesh.xgbl{e}(1:end-1:end),mesh.ygbl{e}(1:end-1:end),c{e}(1:end-1:end,1:end-1:end,1));

                hold on;
            end
            
            colorbar;
%             if (s.problemCase==ProblemCase.LidDrivenCavity)
%                 view([0,90]) 
%             else
                view([25,25]) 
%             end
            
%             for e=1:mesh.getNumElements
%             %             surf(mesh.xgbl{e},mesh.ygbl{e},c{e}(:,:,1));
%             %             surf(mesh.xgbl{e}(1:end-1:end),mesh.ygbl{e}(1:end-1:end),c{e}(1:end-1:end,1:end-1:end,1));
%         %         surf(mesh.xgbl{e}(1:end-1:end),mesh.ygbl{e}(1:end-1:end),0*c{e}(1:end-1:end,1:end-1:end,1));
%                 plotPlane(e,dir,mesh)
%                 hold on;
%             end
            title(titleString)
%             colorbar off
%             hold off
%             if (dir==3)
%                 view([0,90])
%                 pbaspect([(mesh.x1-mesh.x0) (mesh.y1-mesh.y0) 1])
%             elseif (dir==2)
%                 view([90,0])
%                 pbaspect([1 (mesh.y1-mesh.y0) 100*(mesh.t1-mesh.t0)])
%             elseif (dir==1)
%                 view([0,0])
%                 pbaspect([(mesh.x1-mesh.x0) 1 100*(mesh.t1-mesh.t0)])
%             elseif (dir==0)
%                 view([0,90])
%                 pbaspect([(mesh.x1-mesh.x0) (mesh.y1-mesh.y0) 10*(mesh.t1-mesh.t0)])
%             end

%             xlabel('x') ;
%             ylabel('y') ;
%             zlabel('t') ;   
        end
        
        function plotStreamline(s,fig,position,variable,variable2,title)
            if (position==1)
                clf(fig)
                screensize = get( groot, 'Screensize' );
                sizex = 1000;
                sizey = 1000;
                
%                 if (s.problemCase==ProblemCase.LidDrivenCavity)
%                     sizex = 1200;
%                     sizey = 1000;   
%                 end
                
                set(fig,'position',[0.5*(screensize(3)-sizex) 0.5*(screensize(4)-sizey) sizex sizey]); 
            end

            if (variable==4)
                variable = 3;
            end
            
            data = s.getValues(variable);
            data2 = s.getValues(variable2);
            
            subplot(2,2,position);

            dir = 0;

            for e=1:s.mesh.getNumElements
                if (size(data{e},2)==1)
                    data{e} = reshape(data{e},s.mesh.getElement(e).disc.Q);
        %             c{e}(1:end-1:end,1:end-1:end,1)
                    [X,Y] = meshgrid( s.mesh.getElement(e).xelem,s.mesh.getElement(e).yelem );
                    data2{e} = reshape(data2{e},s.mesh.getElement(e).disc.Q);
                    [sx,sy] = meshgrid(0:0.1:1,0:0.1:1);
                    u = data{e}(:,:,end)';
                    v = data2{e}(:,:,end)';
                    streamData = stream2(X,Y,u,v,sx,sy);
                    streamline( streamData );
%                     test = streamslice(X,Y,u,v);
                else
                    [X,Y] = meshgrid(mesh.xgbl{e},mesh.ygbl{e});
        %             surf(X,Y,c{e}(1:5,1:5,5)');
                    surf(X,Y,c{e}(1:size(c{e},1),1:size(c{e},2),1)');
        %             surf(mesh.xgbl{e}(1:end-1:end),mesh.ygbl{e}(1:end-1:end),c{e}(1:end-1:end,1:end-1:end,1));
                end

            %             surf(mesh.xgbl{e},mesh.ygbl{e},c{e}(:,:,1));
            %             surf(mesh.xgbl{e}(1:end-1:end),mesh.ygbl{e}(1:end-1:end),c{e}(1:end-1:end,1:end-1:end,1));

                hold on;
            end
            
            colorbar;
%             if (s.problemCase==ProblemCase.LidDrivenCavity)
%                 view([0,90]) 
%             else
                view([25,25]) 
%             end  
        end
        
        function outputTimings(s)
            nTimings = length(timings);
            tAll = NMPI_Allreduce(s.timings,nTimings,'+',Parallel3D.MPI.mpi_comm_world) ;
            if (Parallel3D.MPI.rank==0)
                fprintf('Total time : %f\n',tAll(1))
            end 
        end
        
        function computeResidual(s)
            disp('computeResidual')
            alphaElements   = s.getElementalAlpha();
            for e=1:s.mesh.getNumLocalElements
                s.residual{e} = s.opL{e} * alphaElements{e}(~s.prescribed{e}) - s.opG{e};
                s.residualNorm(e) = sqrt(s.residual{e}' * s.residual{e});
            end
        end
        
        function out = getMesh(s)
            out = s.mesh;
        end
    end
    
    methods (Access=protected)
        
        function out = getOpL(s,e,equation,variable)
            eqRange     = size(s.opL{e},1) / s.nEq;
            varRange    = size(s.opL{e},2) / s.Nvar;
            out = s.opL{e}( (equation-1)*eqRange +1:equation*eqRange , ...
                            (variable-1)*varRange+1:variable*varRange );
        end
        
        function out = getOpG(s,e,equation)
            eqRange     = size(s.opL{e},1) / s.nEq;
            out = s.opG{e}( (equation-1)*eqRange +1:equation*eqRange , 1 );
        end
        
%         function setAlphaTemp(s,element,variable)
%             s.alphaTemp{variable} = s.getAlpha(element,variable,s.wi);
%         end
        
        function out = getValue(s,e,variable,solMode)
            if (nargin==3)
                solMode = SolutionMode.Normal;
            elseif (nargin<3 || nargin >4)
                disp('ERROR: wrong number of arguments for getValue');
            end
            
            element = s.mesh.getLocalElement(e);

            switch solMode
                case SolutionMode.Nonlinear
                    elementAlpha = s.getAlphaN(e,variable,s.wi);
                case SolutionMode.Coupling
                    elementAlpha = s.getAlphaC(e,variable,s.wc);
                case SolutionMode.Normal
                    elementAlpha = s.getAlpha(e,variable);
            end
            
            out = element.getH * element.Z * elementAlpha;
        end
        
        function out = getValues(s,variable)
            % disp('getValues')
            alphaCells = s.getElementalAlpha(variable);
            out = cell(s.mesh.getNumLocalElements,1);
            for e=1:s.getNumLocalElements
                out{e} = s.mesh.getLocalElement(e).getH * (s.mesh.Z{e} * alphaCells{e}(:) );
            end
        end
        
%         function out = getValuex(s,e,variable,solMode)
%             if (nargin==3)
%                 solMode = SolutionMode.Normal;
%             elseif (nargin<3 || nargin >4)
%                 disp('ERROR: wrong number of arguments for getValue');
%             end
%             
%             element = s.mesh.getLocalElement(e);
% 
%             switch solMode
%                 case SolutionMode.Nonlinear
%                     elementAlpha = s.getAlphaN(e,variable,s.wi);
%                 case SolutionMode.Coupling
%                     elementAlpha = s.getAlphaC(e,variable,s.wc);
%                 case SolutionMode.Normal
%                     elementAlpha = s.getAlpha(e,variable);
%             end
%             
%             out = element.getDx * element.Z * elementAlpha;
%             %ux = s.mesh.getElement(element).getDx * s.mesh.Z{element} * s.alphaTemp{variable} ; 
%             %ux = s.mesh.getElement(element).getDx * s.mesh.Z{element} * s.getAlpha(element,variable,s.wi);
%         end
%         
%         function out = getValuey(s,e,variable,solMode)
%             if (nargin==3)
%                 solMode = SolutionMode.Normal;
%             elseif (nargin<3 || nargin >4)
%                 disp('ERROR: wrong number of arguments for getValue');
%             end
%             
%             element = s.mesh.getLocalElement(e);
% 
%             switch solMode
%                 case SolutionMode.Nonlinear
%                     elementAlpha = s.getAlphaN(e,variable,s.wi);
%                 case SolutionMode.Coupling
%                     elementAlpha = s.getAlphaC(e,variable,s.wc);
%                 case SolutionMode.Normal
%                     elementAlpha = s.getAlpha(e,variable);
%             end
%             
%             out = element.getDy * element.Z * elementAlpha;            
%             %uy = s.mesh.getElement(element).getDy * s.mesh.Z{element} * s.alphaTemp{variable} ; 
%             %uy = s.mesh.getElement(element).getDy * s.mesh.Z{element} * s.getAlpha(element,variable,s.wi);
%         end
        
%         function out = computeValue(s,variable)
%             alphaElements   = s.getElementalAlpha(variable);
%             out             = cell(s.mesh.getNumLocalElements,1);
%             for e=1:s.mesh.getNumLocalElements
%                 out{e} = s.mesh.getLocalElement(e).getH * (s.mesh.Z{e} * alphaElements{e}(:) );
%                 out{e} = reshape(out{e}, s.mesh.getLocalElement(e).disc.Q);
%             end
%         end
        
        % Check which elements need refinement
        function needsRefinement = checkRefinement(s,maxLevel)
            needsRefinement = false(s.mesh.getNumLocalElements,1);

            for e=1:s.mesh.getNumLocalElements
                refinementCriteria = s.getRefinementCriteria(e);
                
                element = s.mesh.octTree.getLocalElement(e);
                
                if (any(refinementCriteria) && element.lvl<maxLevel)
                    needsRefinement(e) = true;
                    % element.needsRefinement = true;
                end
            end
            
            % Convert from local to global and sync between processors
            needsRefinementAll = false(s.mesh.getNumGlobalElements,1);
            needsRefinementAll(s.mesh.getElementRank) = needsRefinement;            
            needsRefinement = NMPI_AllreduceLogical(needsRefinementAll,s.mesh.getNumGlobalElements,'|',Parallel3D.MPI.mpi_comm_world) ;
        end  
        
        function out = getNumLocalElements(s)
            out = s.mesh.getNumLocalElements;
        end
    end
    
    methods (Abstract)
        result = getRefinementCriteria(s);
    end
    
    
    
end

