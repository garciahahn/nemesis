classdef Droplet_C0 < CahnHilliard_C0
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods        
        function addDroplet(s,center,radius)            
            fh = @(x,y,z,t) s.initialCircle(x,y,z,t,center,radius);
            %s.cField.add( fh );
            s.initField(1,FieldType.Current,fh)
            
            s.initField(2,FieldType.Current,0)
            s.initField(3,FieldType.Current,0)
            
            fh = @(x,y,z,t) s.initialOmg(x,y,z,t,center,radius);
            %s.omgField.add( fh );
            s.initField(4,FieldType.Current,fh)
            
            s.initField(5,FieldType.Current,0)
            s.initField(6,FieldType.Current,0)
        end
        
        function out = exactC(s,x,y,z,t,center,radius)
            out = squeeze( s.initialCircle(x,y,z,t,center,radius) );
        end
        
        function out = exactCx(s,x,y,z,t,center,radius)
            out = 0*squeeze( s.initialCircle(x,y,z,t,center,radius) );
        end
        
        function out = exactCy(s,x,y,z,t,center,radius)
            out = 0*squeeze( s.initialCircle(x,y,z,t,center,radius) );
        end
        
        function out = exactOmg(s,x,y,z,t,center,radius)
            out = squeeze( s.initialOmg(x,y,z,t,center,radius) );
        end
        
        function out = exactOmgx(s,x,y,z,t,center,radius)
            out = 0*squeeze( s.initialOmg(x,y,z,t,center,radius) );
        end
        
        function out = exactOmgy(s,x,y,z,t,center,radius)
            out = 0*squeeze( s.initialOmg(x,y,z,t,center,radius) );
        end
    end
    
end

