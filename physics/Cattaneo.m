classdef Cattaneo < Physics
    %LINEARADVECTION 1D and 2D LinearAdvection equation
    %   As the exact solution is known, the LinearAdvection equation can be 
    %   used to verify the implementation and determine the accuracy of the 
    %   method. 
    
    properties (Access=private)
    end
    
    properties
        uField
    end
    
    methods
        function s = Cattaneo(mesh)
            s.name          = 'C';
            s.shortName     = 'C';
            s.longName      = 'Cattaneo';
            
            s.mesh = mesh;
            
            s.nEq  = 2;
            s.initFields('T','q');
            
            s.maxNIter  = System.settings.comp.nonlinear.maxIter;
            s.maxCIter  = System.settings.comp.coupling.maxIter;
            
            s.eqWeights     = [ 1 1 ];
            s.nConvergence  = [1E-4; 1E-4]; 
            s.cConvergence  = [1E-4; 1E-4]; 
            s.rConvergence  = [1E-6; 1E-6; 1E-6]; 
        end

        function time = setupEquations(s,physics)
            t1 = toc;

            if (nargin==2)
                s.allPhysics = physics;
            end
            
            %s.eqWeights = System.settings.comp.equationWeights * ones(s.nEq,1);
            
            gamma = System.settings.phys.gamma;
            tau   = System.settings.phys.tau;
            k     = System.settings.phys.k;
            
            lamRho    = System.settings.phys.lamRho;
            lamMu     = System.settings.phys.lamMu;
            Re        = System.settings.phys.Re; 
            Pr        = System.settings.phys.Pr; 
            REI = 1/Re;
            PRI = 1/Pr;
            
            C       = FieldCollection.get('c');
            U       = FieldCollection.get('u');
            V       = FieldCollection.get('v');
            
            for e=s.mesh.eRange
                % Select the active element
                element = s.mesh.getElement(e);
                
                % Get all elemental operators/arrays
                H  = element.getH;
                Dx = element.getDx;
                
                if (C~=C)
                    Ck    = 1;
                else
                    Ck    = C.val(FieldType.Current,e);
                end
                
                if (U~=U)
                    Uk    = 0;
                    dUkdx = 0;
                else
                    Uk    = U.val(FieldType.Current,e);
                    dUkdx = U.x(FieldType.Current,e);
                    dUkdy = U.y(FieldType.Current,e);
                end

                if (V~=V)
                    Vk    = 0;
                    dVkdy = 0;
                else
                    Vk    = V.val(FieldType.Current,e);
                    dVkdx = V.x(FieldType.Current,e);
                    dVkdy = V.y(FieldType.Current,e);
                end
                
                if (System.settings.time.spaceTime)
                    Dt  = element.getDt;
%                     Dt1 = 0;
%                     theta = 1;
%                     dUdx1 = 0;
%                     dUdy1 = 0;
                else
%                     Dt      = element.getH / System.settings.time.step;
%                     U       = FieldCollection.get('u');
%                     Dt1     = U.val(FieldType.Timelevel1,e) / System.settings.time.step; % timelevel n-1
%                     dUdx1   = U.x(FieldType.Timelevel1,e);        % timelevel n-1
%                     dUdy1   = U.y(FieldType.Timelevel1,e);        % timelevel n-1
%                     theta   = 0.5;
                end
                if (s.mesh.sDim==2)
                    Dy = element.getDy;
                end
                
                Wel     = element.getW;
                vecZ    = element.getVecZ;
                               
                % Get the local element number
                el = element.localID;
                
                if (s.mesh.sDim==1)
                    Conv     = Uk.*Dx + 0*dUkdx .* H;
                    
                    s.opL{el} = [ gamma*(Dt+Conv)        Dx     ;
                                      k*Dx          tau*(Dt+Conv)+H  ];
                    s.opG{el} = [ vecZ ;
                                  vecZ ];
                    
                elseif (s.mesh.sDim==2)
                    Conv     = Uk.*Dx + Vk.*Dy;
                    
                    %Diff     = s.REI*s.PRI * Laplacian;
                    mu       = (1-lamMu )*Ck + lamMu ;  
                
                    viscDiss = (REI / 1868 * mu) .* ( (dUkdy+dVkdx).^2 + 2*(dUkdx.^2+dVkdy.^2) - 2/3 * (dUkdx+dVkdy).^2 );
                    
                    s.opL{el} = [ gamma*(Dt+Conv)      REI*PRI * (Dx+Dy)     ;
                                      k*(Dx+Dy)     tau*(Dt+Conv)+H  ];
                    s.opG{el} = [ vecZ + viscDiss;
                                  vecZ ];
                end
                
                Z = s.mesh.Z{e};
                         
                s.opL{el} = s.opL{el} * superkron(eye(s.Nvar),Z);
                
                s.W{el}      = repmat(Wel,s.Nvar,1);
                s.sqrtW{el}  = repmat(sqrt(Wel),s.Nvar,1);
            end
            
            t2 = toc;
            time = t2-t1;
        end             
                
        function out = getConvergence(s,fieldType,residual)
            if nargin==2
                switch fieldType
                    case FieldType.Nonlinear
                        out = reshape( s.nConvergence, [numel(s.nConvergence),1] );
                    case FieldType.Coupled
                        out = reshape( s.cConvergence, [numel(s.cConvergence),1] );
                end
            else
                out = reshape( s.rConvergence, [numel(s.rConvergence),1] );
            end
        end
        
        function save(s,filename)
            % Merge solution from all processors on the master rank
            u = s.uField.getGlobalAlpha;
            
            if (Parallel3D.MPI.rank==0)
                LA              = struct;
                LA.class        = class(s);
                LA.name         = s.name;
                LA.mesh         = s.mesh.save;
                LA.uConvergence	= s.uConvergence;
                LA.u            = u;
                
                save(filename,'LA','-append');
            end
        end
    end
        
    methods (Access=protected)
        function loadObject(s,solution)
            NS = solution.data.NS;
            
            s.name = NS.name;        
            s.uConvergence = NS.uConvergence;
            s.vConvergence = NS.vConvergence;
            s.pConvergence = NS.pConvergence;
                       
            if (isa(NS.mesh,class(MeshRefineMultiD)))
                fprintf('Please save the mesh as a struct\n')
                mesh = NS.mesh;
                meshStruct = mesh.save;
            elseif (isstruct(NS.mesh))
                meshStruct = NS.mesh;
            else
                error('Undefined mesh type')
            end
            
            stde = LSQdisc_5.load(solution.data.standardElement);
            mesh = MeshRefineMultiD.load(stde,meshStruct);

            if (isfield(NS.mesh,'name'))
                mesh.setName(NS.mesh.name);
            else
                mesh.setName(NS.name);
            end
            
            if (nargin>0)
                s.initialize(mesh,NS.Re,NS.We,NS.Fr,NS.g,NS.alpha,NS.lamRho,NS.lamMu,15,15);
            end
            
            s.uField.current.loadAlphaArray(NS.u);
            
            s.uField.coupled{1}.loadAlphaArray(NS.u);
        end        
    end
    
    methods (Static)
        function out = load(solution)
            if (~isa(solution,'Solution'))
                error('The first argument of load should be a Solution object')
            end
            out = NavierStokes;
            out.loadObject(solution);
        end
        
        function plotParallel(nproc,variable)
            
            fig = figure(1);
            clf
            
            for i=1:nproc
                disp('NEW PROC')
                temp = load(['NS_' int2str(i-1) '.mat']);
                temp.NS.plotVariable(fig,0,variable,'')
                hold on
            end
            
            drawnow;            
        end 
    end
    
end

