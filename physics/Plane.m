classdef Plane < CahnHilliard
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function addPlane(s,normalVector,point)
            fh = @(x,y,z) s.initialPlane(x,y,z,normalVector,point);
            s.addC( fh );
        end
        
        % Function which defines an plane through a specified point and with 
        % a given normalVector. The x,y,z data are the locations where the C
        % data needs to be evaluated. The point is a vector of length 3,
        % which contains its x,y,z data. The normalVector is also of length 3
        function out = initialPlane(s,x,y,z,normalVector,point)

            if length(normalVector)~=3
                error('The normalVector should have length == 3')
            end
            
            if length(point)~=3
                error('The point should have length == 3')
            end
            
            if (normalVector(3)~=0)
                error('A plane with a normal vector in time is not possible')
            else
                out = zeros(length(x),length(y),length(z));
                
                denominator = (2*sqrt(2)*s.Cn);
                
                if (normalVector(2)==0)         % normal in x-direction
                    for i=1:length(x)
                        out(i,:,:) = 0.5 + sign(normalVector(1)) * 0.5 * tanh( (point(1)-x(i)) / denominator ) ;
                    end
                elseif (normalVector(1)==0)     % normal in y-direction
                    for i=1:length(y)
                        out(:,i,:) = 0.5 + sign(normalVector(2)) * 0.5 * tanh( (point(2)-y(i)) / denominator ) ;
                    end 
                else
                    error('A plane with a normal vector in xy-direction has not implemented')
                end    
            end
        end        
    end
    
end

