classdef Korteweg_Momentum_2D < Physics
    %KORTEWEG_MOMENTUM_2D Non-Stokes implementation of the Navier Korteweg momentum equations
    properties (Access=private)
        REI_mu; REI_lambda;
    end
    
    properties (Constant)
        % Variable ids for this physics
        r = 1;
        u = 2;
        v = 3;
        l = 4;
    end
    
    properties
        % These are: 
        %           - Re_mu: the Reynolds related to the mu (shear stress) viscosity,
        %           - Re_lambda: the Reynolds related to the lambda (compression) viscosity,
        %           - Ca: capillary number,
        %           - G: body force number,
        %           - T: temperature of the isothermal system.
        Re_mu; Re_lambda;
        Ca; G; T;
    end
    
    methods
        function s = Korteweg_Momentum_2D(mesh,settings)
            s = s@Physics(mesh);
            
            s.shortName = 'KM2D';
            s.longName  = 'KortewegMomentum';
            s.name      = 'KM2D';
            
            s.nEq       = 4;
            s.variables = {'r','u','v','l'};
            
            s.initialize(settings);
        end
        
        function initialize(s,settings)
            initialize@Physics(s,settings);

            s.steady    = settings.steady;

            s.Re_mu     = System.settings.phys.Re_mu;
            s.Re_lambda = System.settings.phys.Re_lambda;
            s.Ca        = System.settings.phys.Ca;
            s.G         = System.settings.phys.G; % Dimensionless body forces
            s.T         = System.settings.phys.T;

            s.REI_mu     = 1.0/s.Re_mu;
            s.REI_lambda = 1.0/s.Re_lambda;
        end

        function time = setupEquations(s)
            % Function to setup the KM2D equation.
            % FIXME: Add where the notes for these equations could be found!
            t1 = toc; % these inconsistencies and lack of elegance is what drives me crazy about matlab!
            
            u   = FieldCollection.get('u');
            v   = FieldCollection.get('v');
            r   = FieldCollection.get('r');
            l   = FieldCollection.get('l');

            for e = s.mesh.eRange
                element = s.mesh.getElement(e); % Select the active element
                el      = element.localID;      % Get the local element number
                
                %% elemental operators
                matZ    = element.getMatZ;
                vecZ    = element.getVecZ;
                H       = element.getH;
                Dx      = element.getDx;
                Dy      = element.getDy;

                if (s.steady)
                    Dt       = 0;
                    dUdt_rhs = vecZ;
                    dVdt_rhs = vecZ;
                else
                    Dt     = s.getTime( element );
                    rt_rhs = s.getTime( element, r );
                    ut_rhs = s.getTime( element, u );
                    vt_rhs = s.getTime( element, v );
                end % if steady

                Dxx     = element.getDxx;
                Dxy     = element.getDxy;
                Dyy     = element.getDyy;
                
                %% density derivatives
                rk      = r.val2(e,s.mesh);
                rx      = r.x(FieldType.Current,e);
                ry      = r.y(FieldType.Current,e);
              
                %% u-velocity derivatives
                uk      = u.val2(e,s.mesh);
                ux      = u.x(FieldType.Current,e);
                uy      = u.y(FieldType.Current,e);
                ut      = u.t(FieldType.Current,e);
                
                %% v-velocity derivatives
                vk      = v.val2(e,s.mesh);
                vx      = v.x(FieldType.Current,e);
                vy      = v.y(FieldType.Current,e);
                vt      = v.t(FieldType.Current,e);
                
                %% l derivatives (l is the Laplacian of the density r, so l = rxx + ryy)
                lk      = l.val2(e, s.mesh);
                lx      = l.x(FieldType.Current,e);
                ly      = l.y(FieldType.Current,e);
                
                
                %% Body forces
                b       = System.settings.phys.b;
                bx      = ones(size(vecZ)) * b(1);
                by      = ones(size(vecZ)) * b(2);

                %% auxiliary terms
                advect_uk   = ut + uk.*ux + vk.*uy;
                advect_vk   = vt + uk.*vx + vk.*vy;
                div_uk      = ux + vy;
                advect      = Dt + uk.*Dx + vk.*Dy; 
                laplacian   = Dxx + Dyy;
                REIS        = s.REI_mu + s.REI_lambda;
                
                press      = -2*rk + (8/27) * T * (1/1-rk)^2;

                rr = advect + div_uk.*H;
                ru = rk.*Dx + rx.*H;
                rv = rk.*Dy + ry.*H;
                rl = matZ;

                ur = press .* Dx + (advect_uk - qx - s.G * bx) .* H;
                uu = rk .* advect + rk .* ux .* H - s.REI_mu .* laplacian - REIS .* Dxx;
                uv = rk .* uy .* H - REIS .* Dxy;
                ul = -rk .* Dx;

                vr = press .* Dy + (advect_vk - qy - s.G * by) .* H;
                vu = rk .* vx .* H - REIS .* Dxy;
                vv = rk .* advect + rk .* vy .* H - s.REI_mu .* laplacian - REIS .* Dyy;
                vl = -rk .* Dy;

                lr = s.Ca^2 .* laplacian;
                lu = matZ;
                lv = matZ;
                ll = -H;

                rhs_r = rt_rhs + uk .* rx + vk .* ry + rk .* div_uk;
                rhs_u = ut_rhs + rk .* ut + 2*(rk .* uk) .* ux + 2 * (rk .* vk) .* uy - rk .* lx;
                rhs_v = vt_rhs + rk .* vt + 2*(rk .* uk) .* vx + 2 * (rk .* vk) .* vy - rk .* ly;
                rhs_l = vecZ;

                %% Assign L and G
                %              r | u | v | l
                s.opL{el} = [ rr, ru, rv, rl;
                              ur, uu, uv, ul;
                              vr, vu, vv, vl;
                              lr, lu, lv, ll];
                         
                s.opG{el} = [ rhs_r;
                              rhs_u;
                              rhs_v;
                              rhs_l];
                
                % Set quadrature weights, apply all scaling to L and G
                s.setWeightAndScale( element );
            end
            
            t2 = toc;
            time = t2-t1;
        end              

        function updateBC(s)
            % do nothing
        end

        function prescribed = getPrescribed(s,e)
            if nargin==2
                prescribed = [s.fields{1}.getPrescribed(e)];
            else
                prescribed = cell(s.mesh.numLocalElements,1);
                for e=1:numel(prescribed)
                    element = s.mesh.getLocalElement(e);
                    prescribed{e} = s.getPrescribed( element.globalID );
                end
            end
        end
        
        function out = getPrescribedValues(s,e)
            out = [s.fields{1}.getPrescribedValues(e)];
        end

        function output(s,outputDir,id)
            data.t = s.getT( 1, s.getElementalAlpha(Variable.t) );
            for e=Parallel3D.MPI.eStart:Parallel3D.MPI.eEnd
                data.t{e} = reshape(data.t{e},s.mesh.getElement(e).disc.Q);
                data.t{e} = data.t{e}(:,:,end);
            end
                        
            for e=1:s.mesh.getNumLocalElements
                data.x{e,1} = s.mesh.getLocalElement(e).xelem;
                data.y{e,1} = s.mesh.getLocalElement(e).yelem;
            end
            
            data.eStart = Parallel3D.MPI.eStart;
            data.eEnd   = Parallel3D.MPI.eEnd;
            
            save([outputDir 'info-' num2str(id,'%.4d')  '--' num2str(Parallel3D.MPI.rank,'%.3d') '.mat'],'data') ;
        end
        
        % Default refinement criteria (false)
        function criteria = getRefinementCriteria(s,e)
            criteria(1) = false;
        end
        
        function preTimeLoop(s)
            if (s.usePreTimeLoop)
                
                s.preTimeStep();
                
                %% Apply boundary conditions
                s.resetBoundaryConditions();
                Settings.setBoundaryConditions( s );

                % By solving the steady equations, the solution will be
                % obtained faster
                s.steady    = true;
                s.coupled   = true;

                %% Solve nonlinear equations                
                s.solveNonLinear();

                s.steady    = s.settings.steady;
                s.coupled   = s.settings.coupled;
            end
        end
        
        function out = addDroplet(s,rhoArray,x,y,~,~,center,radius)
            
            Rho1 = rhoArray(1);
            Rho2 = rhoArray(2);
            
            r0 = radius;
            xc = center(1);
            yc = center(2);
            
            d  = r0 - sqrt((x-xc).^2+(y-yc).^2); % positive inside the droplet
            
            out = (Rho1+Rho2)/2 + (Rho1-Rho2)/2 * tanh( d * sqrt(s.We)/2 );
        end
        
        function out = addSteepDroplet(s,rhoArray,x,y,~,~,center,radius)
            Rho1 = rhoArray(1);
            Rho2 = rhoArray(2);
            
            r0 = radius;
            xc = center(1);
            yc = center(2);
            
            d  = r0 - sqrt((x-xc).^2+(y-yc).^2);        % positive inside the droplet
            
            out = Rho2 + ((Rho1-Rho2)/2) * (1 + tanh( d * sqrt(s.We) * 100 ) );
        end
        
        function out = addDoubleDroplet(s,rhoArray,x,y,~,~,center,radius)
           
            Rho1 = rhoArray(1);
            Rho2 = rhoArray(2);
            
            r0 = radius;
            xc = center(1);
            yc = center(2);
            
            d  = r0 - sqrt((x-xc).^2+(y-yc).^2); % positive inside the droplet
            d2 = r0 - sqrt((x + xc) .^ 2 + (y - yc) .^ 2);
            
            out = Rho2 ...
                + ((Rho1-Rho2)/2) * ( 2 ...
                                    + tanh( d * sqrt(s.We)/2 ) ...
                                    + tanh( d2 * sqrt(s.We)/2 ));
        end
        
        function out = addManyDroplet(s,rhoArray,x,y,center,radius)
            droplet_num = size(center, 1);
            arg = 0;
            for i = 1:droplet_num
                d = radius(1) - sqrt( (x - center(i, 1)) .^ 2 + (y - center(i, 2)) .^ 2);
                arg = arg + 1 + tanh(d * sqrt(s.We)/2);
            end % droplet_num
            arg = arg * ((rhoArray(1) - rhoArray(2))/2);
            out = rhoArray(2) + arg;
        end % function addManyDroplet

        function out = addHalfChannelBottom(s,rhoArray,x,y,~,~, pos)
            Rho1 = rhoArray(1);
            Rho2 = rhoArray(2);
            
            d  = pos - y;
            
            out = (Rho1+Rho2)/2 + (Rho1-Rho2)/2 * tanh( d * sqrt(s.We)/2 );
        end % function
    
    methods (Static)
        function out = load(solution)
            if (~isa(solution,'Solution'))
                error('The first argument of load should be a Solution object')
            end
            
            out = EnergyEquation_NSK;
            out.loadObject(solution);
        end 
    end
    
end

