classdef Physics < handle
    %PHYSICS Blueprint for each physics class
    %   Each physics class to be implemented by the user must use this
    %   Physics class as a base (define it as a superclass). It ensures all
    %   required variables and functions will be defined automatically, and
    %   thereby prevents 
    
    properties (SetAccess=protected, GetAccess=public)
        mesh;
    end
    
    properties (Access=private)
        dof;
    end
    
    properties
        SDIM;
        
        fields;                         % cell array of length nVar: each variable has its own field 
        fieldGroup;                     % collection of fields used for output to Paraview
        fieldData;
        
        activeField; 
        
        % General properties
        Nvar;
        nEq;
        variables;
        activeVariables;
        %periodic;
        
        nonlinearIteration; couplingIteration;
        
        K; G; W; sqrtW;
        opL; opG;
        useInverse = false;
        
        A; f; globalL = false;

        name;                           % will be removed
        shortName; longName;            % short and long name for this physics
        
        boundary; boundary_v1;
        
        residual; residualL2, residualL2N, residualL2C;
        
        residualField;
        residualIntegral;
        residualRefine; residualCoarsen;
        
        maxResidual = 1E-6;
        
        residualNorm;
        residualNormN, residualNormC;
        
        relaxation = 1;
        
        nonlinear; coupled;
        
        converged, norms;
        needsConvergence;
        
        activeMaterial;
        
        usePreTimeLoop = false;
        usePreTimeStep = false;
                
        % Numerical properties
        iter_cg;
        iter;
        
        solverInfo;
        
        silent  = true;
        
        % Timing parameters
        timings;

%{
  Julián ADDED: 'code_times' struct will be used for the times and need to
      measure for the current training.
%}
        
        code_times = struct('non_linear', struct);
        
        withForcing;            % use forcing (default: false)
        forcing;                % single stage forcing
        stageForcing;           % cell array with forcing for each stage
        
        plotMode;
        
        %% Default values
        maxNonlinear    = 1;    % Maximum number of nonlinear iterations
        maxCoupling     = 1;    % Maximum number of coupling iterations        
        nConvergence    = 1e-6;
        cConvergence    = 1e-6;
        rConvergence    = 1e-6;
        maxLevel        = 0;    % Maximum refinement level
        eqWeights       = 1; 
        activeEqWeights = 1;
        exact           = false;
        steady          = false;
        nConvType       = [true; true];
        cConvType       = [true; true];
        
        heavisideThickness  = 0.1;
        
        timeMethod;
        
        manufacturedSolution = false;
        
        nTable;
        cTable;
        
        numStages    = 1;
        activeStage  = 1;
        
        stageSolver;
        
        settings;
    end
    
    methods
        % 
        function s = Physics(mesh)
            s.mesh = mesh;
        end
        
        function initialize(s,settings)
            % Set the number of variables
            if (numel(s.variables)~=0)
                s.Nvar = numel(s.variables);
                s.activeVariables = 1:s.Nvar;
            end
            
            % Assign the provided settings to this physics
            s.settings = settings;
            
            % Mesh material
            if (isfield(settings,'activeMaterial'))
                s.activeMaterial = settings.activeMaterial;
                s.mesh.setActiveMaterial( s.activeMaterial );
            else
                s.activeMaterial = Material.All;
            end
            
            % After restarting the physics.settings contains an additional
            % settings which has all the initial settings provided to the
            % physics at initialization. It might happen that not all
            % necessary settings are save in physics.settings, therefore
            % here any missing fields are added to the current settings.
            if ( isfield(settings,'settings') )
                f = fieldnames(settings.settings);
                for i = 1:length(f)
                    if (~isfield(settings,f{i}))
                       settings.(f{i}) = settings.settings.(f{i});
                    end
                end
                s.settings = rmfield(settings,'settings');
            end
            
            % Nonlinear convergence
            if (isfield(settings,'nConvergence'))
                s.nConvergence = settings.nConvergence;
                if (numel(s.nConvergence)~=s.Nvar)
                    if (System.rank==0 && numel(s.nConvergence)~=1)
                        fprintf('WARNING    : nConvergence should have 1 or %d elements, using %f for all values\n',s.Nvar,s.nConvergence(1))
                    end
                    s.nConvergence = repmat( s.nConvergence(1), s.Nvar, 1 );
                end
            else
                s.nConvergence = repmat( s.nConvergence(1), s.Nvar, 1 );
            end
            
            % Coupling convergence
            if (isfield(settings,'cConvergence'))
                s.cConvergence = settings.cConvergence;
                if (numel(s.cConvergence)~=s.Nvar)
                    if (System.rank==0 && numel(s.cConvergence)~=1)
                        fprintf('WARNING    : cConvergence should have 1 or %d elements, using %f for all values\n',s.Nvar,s.cConvergence(1))
                    end
                    s.cConvergence = repmat( s.cConvergence(1), s.Nvar, 1 );
                end
            else
                s.cConvergence = repmat( s.cConvergence(1), s.Nvar, 1 );
            end
            
            % Residual convergence
            if (isfield(settings,'rConvergence'))
                s.rConvergence = settings.rConvergence;
                if (numel(s.rConvergence)~=s.nEq+1)
                    if ( System.rank==0 && numel(s.rConvergence)~=1 )
                        fprintf('WARNING    : rConvergence should have 1 or %d elements, using %f for all values\n',s.nEq+1,s.rConvergence(1))
                    end
                    s.rConvergence = repmat( s.rConvergence(1), s.nEq+1, 1 );
                end
            else
                s.rConvergence = repmat( s.rConvergence(1), s.nEq+1, 1 );
            end
            
            % Maximal number of nonlinear iterations
            if (isfield(settings,'maxNonlinear'))
                s.maxNonlinear = max(1, settings.maxNonlinear);
            else
                activeSettings = System.settings;
                s.maxNonlinear = activeSettings.comp.maxNonlinear;
            end
            
            % Maximal number of coupling iterations
            if (isfield(settings,'maxCoupling'))
                s.maxCoupling = max(1, settings.maxCoupling);
            else
                activeSettings = System.settings;
                s.maxCoupling = activeSettings.comp.maxCoupling;
            end
            
            % Maxlevel
            if (isfield(settings,'maxLevel'))
                s.maxLevel = max(0, settings.maxLevel);
            else
                activeSettings = System.settings;
                s.maxLevel = activesettings.mesh.maxLevel;
            end
            
            % Exact solution present
            if (isfield(settings,'exact'))
                s.exact = settings.exact;
            end
            
            % Equation weights
            if (isfield(settings,'eqWeights'))
                s.eqWeights = settings.eqWeights;
                
                if (numel(s.eqWeights)~=s.nEq)
                    if ( System.rank==0 && numel(s.eqWeights)~=1 )
                        fprintf('WARNING    : eqWeights should have 1 or %d elements, using %f for all values\n',s.nEq,s.eqWeights(1))
                    end
                    s.eqWeights = repmat( s.eqWeights(1), s.nEq, 1 );
                end
            else
                s.eqWeights = repmat( s.eqWeights(1), s.nEq, 1 );
            end
            
            s.activeEqWeights = s.eqWeights;
            
            % Steady problem
            if (isfield(settings,'steady'))
                s.steady = settings.steady;
            end
            
            % Nonlinear convergence type
            if (isfield(settings,'nConvType'))
                if (numel(s.nConvType)~=numel(settings.nConvType))
                    error('Physics.initialize error :: nConvType should have %d elements\n',numel(s.nConvType))
                end
                s.nConvType = settings.nConvType;
            end
            
            % Coupling convergence type
            if (isfield(settings,'cConvType'))
                if (numel(s.cConvType)~=numel(settings.cConvType))
                    error('Physics.initialize error :: cConvType should have %d elements\n',numel(s.cConvType))
                end
                s.cConvType = settings.cConvType;
            end
            
            
            
            % Assign the periodicity of fields
            if (~isfield(s.settings,'fieldData') && ~isfield(s.settings,'fdata'))
                s.fieldData = [];
                %error('Please use fdata for periodicity')
                
                % No detailed field periodicity information has been provided.
                % Use the general information from the mesh file, which
                % is used for all DOFs on the boundary of the domain
                meshPeriodic = s.mesh.periodic;
                
                % Initialize fieldData, which is a cell array of dimensions Nvar
                s.fieldData = cell(s.Nvar,1);
                
                % Continuity of the standardElement, which is required to
                % set the correct amount of flags. The
                % fieldData.periodic contains periodic
                activeSettings = System.settings;
                maxContinuity  = max( activeSettings.stde.C );
                
                % Construct the fieldPeriodicity from the meshPeriodicity
                for n=1:s.Nvar
                    s.fieldData{ n }.periodic = repmat( meshPeriodic, maxContinuity+1, 1 );
                end
                
            else
                if (isfield(s.settings,'fieldData'))
                    s.fieldData = settings.fieldData;
                elseif (isfield(s.settings,'fdata'))
                    % for backwards compatibility, but a warning is printed
                    s.fieldData = settings.fdata;
                    if (System.rank==0)
                        fprintf('*** Warning *** : fdata has been replaced by fieldData, the field fdata will not be supported in future versions!\n')
                    end
                end
            end
            
            % Relaxation
            if (isfield(settings,'relaxation'))
                s.relaxation = settings.relaxation;                         % Relaxation from settings
            else
                s.relaxation = s.relaxation * ones( s.Nvar, 1 );            % Default relaxation
            end
            
            % Restart or initialize the fields for this physics
            if (isfield(settings,'fields'))
                s.initFields(settings.fields);                  % initialize from restart fields
                s.settings = rmfield(s.settings,'fields');      % remove the fields from the settings (without removal fields are save twice within future restart files)
            else
                s.initFields();                     % init new field
            end
            
            % Coupled physics
            if (~isfield(s.settings,'coupled'))
                s.coupled = false;
            else
                s.coupled = settings.coupled;
            end
            
            % Nonlinear physics
            if (~isfield(s.settings,'nonlinear'))
                s.nonlinear = false;
            else
                s.nonlinear = settings.nonlinear;
            end
            
            if (isfield(settings,'usePreTimeLoop'))
                s.usePreTimeLoop   = settings.usePreTimeLoop; 
            end
            
            if (isfield(settings,'usePreTimeStep'))
                s.usePreTimeStep   = settings.usePreTimeStep; 
            end
            
%             if (isfield(settings,'initCondWeight'))
%                 s.initCondWeight   = settings.initCondWeight; 
%             end

            % Method for time advancement
            if (~isfield(s.settings,'timeMethod'))
                s.timeMethod = System.settings.time.method;
            else
                s.timeMethod = settings.timeMethod;
            end
            
            if (isfield(settings,'numStages'))
                s.numStages = settings.numStages;
            end
            
            s.needsConvergence = true( s.numStages , 1 );
            
            % Manufactured solution
            if (~isfield(s.settings,'manufacturedSolution'))
                s.manufacturedSolution = false;
            else
                s.manufacturedSolution = settings.manufacturedSolution;
            end
            
            if (isfield(settings,'heavisideThickness'))
                s.heavisideThickness   = settings.heavisideThickness; 
            end
        end
        
        function initFields(s,restartData)
            s.Nvar          = numel(s.variables);
            s.fields        = cell(s.Nvar,1);
            s.fieldGroup    = FieldGroup(s.Nvar,s.shortName,s.longName);
            
            for n=1:s.Nvar

                if ( isa(FieldCollection.get(s.variables{n}),'Field') )
                    % link with existing field
                    s.fields{n} = FieldCollection.get( s.variables{n} );
                    s.fieldGroup = [];
                    
                else
                    % create a new Field
                    
                    %if (~isfield(s.settings,'periodic'))
                    %    s.fields{n} = Field(s.mesh,n,s.variables{n},s.periodic(n));
                    %else
                    %    error('Periodic information not found')
%                     if isempty(s.fieldData)
%                         s.fields{n} = Field(s.mesh,n,s.variables{n},false, s.relaxation);
%                     else
                        s.fields{n} = Field(s.mesh,n,s.variables{n},s.fieldData{n}.periodic,s.relaxation(n));
%                     end
                    %end

                    s.fields{n}.addSubField(FieldType.Current,true);            % by default only the 'Current' SubField will be initialized (and activated)

                    if nargin==2
                        s.fields{n}.load( restartData{n} );
                    end

                    s.fieldGroup.add(s.fields{n},0,s.variables{n},n);              % 0 means scalar field
                    FieldCollection.add( s.fields{n} );
                end
            end
            
            s.residual          = cell(s.mesh.numLocalElements,1);
            
            s.residualField     = cell(s.nEq,1);
            
            for n=1:s.nEq
                eqName = [s.name '_eq' num2str(n)];
                s.residualField{n}  = Field(s.mesh,n,eqName);
                s.residualField{n}.addSubField(FieldType.Current,true);
            end
        end
        
%         function initField(s,fieldNumber,fieldType,value)
%             s.fields{ fieldNumber }.addSubField( fieldType, [], [], [], value );
%             s.setupValueEquation( value );
%             s.resetBoundaryConditions();
%             activeSettings = System.settings
%             activeSettings.setBoundaryConditions( s, s );
%             s.solve(1,fieldNumber);
%         end
        
        function extrapolateFieldsInTime(s)
            for n=1:numel(s.fields)
                s.fields{n}.extrapolateInTime();
            end
        end
        
        function init(s)            
            % Copy alpha from current time level to previous time level 
            % (stored as alphac) if its the first nonlinear iteration            
            s.withForcing = false;
            
            s.updateFields;
            
            s.opG       = cell(s.mesh.numLocalElements,1);  
            s.opL       = cell(s.mesh.numLocalElements,1);
            
            s.K         = cell(s.mesh.numLocalElements,1);
            s.G         = cell(s.mesh.numLocalElements,1);
            s.W         = cell(s.mesh.numLocalElements,1);
            s.sqrtW     = cell(s.mesh.numLocalElements,1);
            
            s.residual 	= cell(s.mesh.numLocalElements,1);
        end
        
        function set.mesh(s,mesh)
            s.mesh = mesh;
        end
        
        function out = get.mesh(s)
            out = s.mesh;
        end
                
        function time = setupEquations(s)
            if s.timeMethod == TimeMethod.SpaceTime
                time = s.setupEquations_spaceTime;
            else
                if (System.settings.mesh.sdim==2)
                    time = s.setupEquations_2D;
                else
                    time = s.setupEquations_3D;
                end
            end
        end
        
        function applyInitialConditions(s,bcType)
            if (nargin==1)
                bcType = BCType.Weak;
            end
            
            % Set the initial conditions for a space-time of time-stepping
            % approach. 
            if ( System.settings.time.spaceTime ) 
                
%                 if isa(s,'CahnHilliard') || isa(s,'CahnHilliard_temp')
%                     minus = 1;
%                 elseif (isa(s,'NavierStokes'))
%                     %if (System.settings.time.iter==0)
%                         minus = 1;
%                     %else
% %                         minus = 0;
%                     %end
%                 elseif (isa(s,'EnergyEquation'))
%                     minus = 0;
%                     
%                 elseif (isa(s,'WaveEquation'))
%                     minus = 0;
%                         
%                 else
%                     error('Please provide a correct minus for Physics.applyInitialConditions')
%                 end
                
                % Verify initCondWeight does exist and show an warning if
                % it does not. In that case zero is assumed and no initial
                % weak boundary condition is used.
                if (~isfield(s.settings,'initCondWeight'))
                    warning('No weights for the initial condition provided, assuming zero now. To remove this message: add initCondWeight[1:numFields] to the settings for each physics')
                    initCondWeight = zeros(numel(s.fields),1);
                elseif (numel(s.settings.initCondWeight)~=numel(s.fields))
                    warning('The weights for the initial condition have a wrong size, assuming zero now. To remove this message: add initCondWeight[1:numFields] to the settings for each physics')
                    initCondWeight = zeros(numel(s.fields),1);
                else
                    initCondWeight = s.settings.initCondWeight;
                end
                
                % Loop over the variables/fields, and add a weak boundary
                % condition if the weight is larger than zero. 
                for n=1:numel(s.fields)
                    if (initCondWeight(n)>0)
                        boundary_t0 = s.initFEBoundary( s.mesh.dim, 0 );        % t=0 boundary
                        s.setBC(boundary_t0, s.fields{n}, s.fields{n}, bcType, BCType.Dirichlet, initCondWeight(n));
                    end
                end
                
            else
                % do nothing
            end
            
            if (System.rank==0 && System.settings.outp.debug)
                fprintf('PHYSICS    : Initial conditions have been applied\n')
            end
        end
        
        function setNumFEBoundary(s,n)
            if (isempty(s.boundary))
                s.boundary = cell(n,1);
            end
        end
        
        function resetFEBoundaries(s)
            s.boundary = [];
        end

        function out = initFEBoundary(s,varargin)
            % Initialization of a FE Boundary. 
            %   id       : number of the FE Boundary
            %   varargin : boundary numbers, which will be converted to Boundary objects
            %
            % The flag is used to indicate if the passed boundaries need to be
            % combined (logical OR), or intersected (logical AND).
            
            if (numel(varargin)==1 && ~isa(varargin{1},'Patch'))
                tempBoundaryNumbers = cell(2,1);
                
                % passed argument is a Neighbor enum, convert to positions
%                 switch varargin{1}
%                     case {Neighbor.Left,Neighbor.Right}
%                         tempBoundaryNumbers{1} = 1;         % x-direction
%                     case {Neighbor.Bottom,Neighbor.Top}
%                         tempBoundaryNumbers{1} = 2;         % y-direction
%                     case {Neighbor.Front,Neighbor.Back}
%                         tempBoundaryNumbers{1} = 3;         % z-direction
%                 end
%                 
%                 switch varargin{1}
%                     case {Neighbor.Left,Neighbor.Bottom,Neighbor.Front}
%                         tempBoundaryNumbers{2} = 0;         % lower position
%                     case {Neighbor.Right,Neighbor.Top,Neighbor.Back}
%                         tempBoundaryNumbers{2} = 1;
%                 end

                switch varargin{1}
                    case {Boundary.x0,Boundary.x1}
                        tempBoundaryNumbers{1} = 1;         % x-direction
                    case {Boundary.y0,Boundary.y1}
                        tempBoundaryNumbers{1} = 2;         % y-direction
                    case {Boundary.z0,Boundary.z1}
                        tempBoundaryNumbers{1} = 3;         % z-direction
                end
                switch varargin{1}
                    case {Boundary.x0,Boundary.y0,Boundary.z0}
                        tempBoundaryNumbers{2} = 0;         % lower position
                    case {Boundary.x1,Boundary.y1,Boundary.z1}
                        tempBoundaryNumbers{2} = 1;
                end
                varargin = tempBoundaryNumbers;
            end

            if (~islogical(varargin{end}))
                id = numel([s.boundary{:}])+1;

                try
                    if (~isempty(s.boundary{id}))
                        needsUpdate = ~isequal(s.boundary{id}.getBase,s.mesh.getBoundary( varargin{1} ));
                    else
                        needsUpdate = false;
                    end
                catch
                    disp('error! Problem in Physics.initFEBoundary')
                end

            else
                needsUpdate = true;
            end

            try
                if ( needsUpdate || isempty(s.boundary{id}) )

                    if (~isa(varargin{1},'Patch'))
                        if (~islogical(varargin{end}))
                            if ( ischar(varargin{end}) )
                                flag = varargin{end};
                                numBoundaries = (numel(varargin)-1) / 2;
                            else
                                numBoundaries = numel(varargin) / 2;
                                flag = '|';                          % default flag: intersection
                            end

                            % Convert the boundaryNumbers to Boundary objects
                            patches = cell(numBoundaries,1);
                            for i=1:numBoundaries
                                patches{i} = s.mesh.getPatch( varargin{ 2*(i-1)+1 : 2*i } );
                            end            

                            s.boundary{id} = FEBoundary(id,patches,flag);

                        else
                            if ( ischar(varargin{end-1}) )
                                flag = varargin{end-1};
                                numBoundaries = (numel(varargin)-2) / 2;
                            else
                                numBoundaries = (numel(varargin)-1) / 2;
                                flag = '|';                          % default flag: intersection
                            end

                            % Convert the boundaryNumbers to Boundary objects
                            patches = cell(numBoundaries,1);
                            for i=1:numBoundaries
                                patches{i} = s.mesh.getPatch( varargin{ 2*(i-1)+1 : 2*i } );
                            end            

                            out = FEBoundary(0,patches,flag);
                            return

                        end
                    else
                        s.boundary{id} = FEBoundary(id,varargin,'|');
                    end
                end
            catch MExp
                error('LSQDIM:physics:Physics.m :: boundary ID issue (increase numFEBoundaries in Physics.m, now set to %d)\n',id-1)
            end
            out = s.boundary{id};
        end
              
        function update(s)
            % If the mesh has been updated by other physics, the sizes of some 
            % variables needs to be updated
            if (numel(s.opG)~=s.mesh.numLocalElements)
                s.updateFields;
                
                s.opG   = cell(s.mesh.numLocalElements,1);  
                s.opL   = cell(s.mesh.numLocalElements,1);

                s.K     = cell(s.mesh.numLocalElements,1);
                s.G     = cell(s.mesh.numLocalElements,1);
                s.W     = cell(s.mesh.numLocalElements,1);
                s.sqrtW = cell(s.mesh.numLocalElements,1);

                s.residual      = cell(s.mesh.numLocalElements,1);
            end
        end
        
        function time = setupNormalEquations(s,variable)
            % Setup the full normal equations for each element, which 
            % consist of the contribution of the physical equations for
            % each element and the weak boundary conditions.
            
            % Some properties / settings
            t1              = toc;                      % Start timer
            useMex          = System.useMexSolver;      % Use mex functions (BLAS)
            withStrongBC    = false;                    % Use strong BC

            s.globalL = false;
            
            if (~s.globalL)

                %% Initialize with the equations for each element
                %% mex version
                
%                 sum1 = 0;
%                 sum2 = 0;
%                 sum3 = 0;
                
                for e=s.mesh.eRange
                    element = s.mesh.getElement(e);
                    el      = element.localID;

                    if (useMex)
                        % Optimized L'WL through a mex function (BLAS routine)
%                         t1=toc;
                        s.K{el} = dsyrkL( full( ( s.sqrtW{el}) .* s.opL{el} ) );
%                         t2=toc;
%                         sum1 = sum1 + t2 - t1;
                        s.G{el} = ( (s.W{el} .* s.opG{el})' * s.opL{el} )';            % this should be faster than : s.G{el} = s.opL{el}' * ( s.W{el} .* s.opG{el});

                    else
                        % Original L'WL through default Matlab multiplication
                        s.K{el} = s.opL{el}' * ( s.W{el} .* s.opL{el});
                        s.G{el} = s.opL{el}' * ( s.W{el} .* s.opG{el});
                    end
                end
                
                
%                 %% matlab version (taking advantage of symmetry)
% %                 a = zeros( size(s.opL{1}) );
%                 for e=s.mesh.eRange
%                     element = s.mesh.getElement(e);
%                     el      = element.localID;
%                     % Original L'WL through default Matlab multiplication
%                     a = s.sqrtW{el} .* s.opL{el};
%                     t2=toc;
%                     
%                     s.K{el} = a' * a;
%                     t3=toc;
%                     sum3 = sum3 + t3 - t2;
%                     s.G{el} = s.opL{el}' * ( s.W{el} .* s.opG{el});
%                 end
                
%                 %% matlab version
%                 for e=s.mesh.eRange
%                     element = s.mesh.getElement(e);
%                     el      = element.localID;
%                     % Original L'WL through default Matlab multiplication
%                     t2=toc;
%                     s.K{el} = s.opL{el}' * ( s.W{el} .* s.opL{el});
%                     t3=toc;
%                     sum2 = sum2 + t3 - t2;
%                     s.G{el} = s.opL{el}' * ( s.W{el} .* s.opG{el});
%                 end

                
%                 fprintf('mex vs. matlab(orig) vs. matlab(symm) = %f vs %f vs %f\n',sum1,sum2,sum3);
                
            else
                % Create a global L first, then compute L'*W*L
                dh = s.mesh.getDoFHandler(true);
                L = sparse(s.nEq*dh.numqDof,numel(s.activeVariables) * dh.numDof);
                G = zeros(s.nEq*dh.numqDof,1);
                W = zeros(s.nEq*dh.numqDof,1);
                
                for e=s.mesh.eRange
                    element = s.mesh.getElement(e);
                    el      = element.localID;
                    
                    q = dh.qdofs{e}(:);
                    m = dh.dofs{e}(:);
                    for n=2:s.nEq
                        q = [ q; dh.qdofs{e}(:) + dh.numqDof ];
                        m = [ m; dh.dofs{e}(:) + dh.numDof ];
                    end
                    
                    L( q,m ) = L( q,m ) + s.opL{el};
                    G( q,1 ) = G( q,1 ) + s.opG{el};
                    W( q,1 ) = W( q,1 ) + s.W{el};
                end
                
                s.A = L' * ( W .* L );
                s.f = L' * ( W .* G );
            end
            
            % For strong BC a specific field is required
            if (withStrongBC)
                if ( isa(variable,'Field') ) %#ok<UNRCH>
                    selectedField = variable;
                elseif (variable~=0)
                    selectedField = s.getField(variable);
                end
            end
            
            %% Add all weak boundaries
            for i=1:numel(s.boundary)                
                % Select an active boundary from the cell array
                activeBoundary = s.boundary{i};
                
                if ( ~isempty(activeBoundary) && activeBoundary.active)
                
                    % Get the weight for this weak boundary condition
                    weight = activeBoundary.weight;
                    
                    if (s.globalL)
                        Lb = sparse( size(L,1), size(L,2) );
                        Wb = zeros( size(W) );
                        Gb = zeros( size(G) );
                    end
                    
                    % Loop over the elements of the activeBoundary, and add
                    % it's contribution to K and G if it is on the current
                    % processor 
                    for be=1:activeBoundary.getNumElements

                        try
                            e = activeBoundary.getElementNumbers(be);
                        catch
                        	error('LSQDIM:physics:Physics.m')
                        end
                            
                        if (~s.globalL)
                            % Is this element located on this processor?
                            if ismember(e,s.mesh.eRange)

                                element = s.mesh.getElement(e);
                                el = element.localID;

                                % Determine the number of free DOFs and quadrature 
                                % points (QP) for this element
                                if (variable==0)
                                    numDof = element.finite.numDof * s.Nvar;
                                elseif (numel(variable)==1)
                                    numDof = element.finite.numDof;
                                else
                                    numDof = element.finite.numDof * numel(variable);
                                end
                                numQP  = element.finite.numQuadratureNodes;

                                % Set the prescribed flags for all variables (DOFs)
                                % This is used for strong BC
                                if (withStrongBC)
                                    prescribed = false(numDof,1); %#ok<UNRCH>
                                    offset = 0;
                                    if (variable==0)
                                        for n=1:s.Nvar
                                            prescribed( offset+1 : offset+element.finite.numDof ) = s.getField(n).getPrescribed(e);
                                            offset = offset + element.finite.numDof;
                                        end 
                                    else
                                        prescribed( offset+1 : offset+element.finite.numDof ) = selectedField.getPrescribed(e);
                                        offset = offset + element.finite.numDof;
                                    end
                                end

                                % Initialize temporary L and G arrays
                                Ltmp = sparse(numQP,numDof);
                                Gtmp = zeros(numQP,1);

                                % Add the L and G for each variable to the temporary 
                                % L and G arrays.
                                offset = 0;
                                if (variable==0)                        
                                    for n=1:s.Nvar
                                        if (~isempty(activeBoundary.opL{be,n}))                                
                                            Ltmp( : , offset+1:offset+element.finite.numDof ) = Ltmp( : , offset+1:offset+element.finite.numDof ) + activeBoundary.opL{be,n};
                                        end                            
                                        offset = offset + element.finite.numDof;
                                    end
                                elseif (numel(variable)==1)
                                    if (isa(variable,'Field'))
                                        variable = variable.getVariable;
                                    end

                                    if (~isempty(activeBoundary.opL{be,variable}))                                
                                        Ltmp( : , offset+1:offset+element.finite.numDof ) = Ltmp( : , offset+1:offset+element.finite.numDof ) + activeBoundary.opL{be,variable};
                                    end 
                                else
                                    if (activeBoundary.singleL)
                                        Ltmp = activeBoundary.opL{be};
                                        Gtmp = activeBoundary.opG{be};
                                    else
                                        for n=variable
                                            if (~isempty(activeBoundary.opL{be,n}))                                
                                                Ltmp( : , offset+1:offset+element.finite.numDof ) = Ltmp( : , offset+1:offset+element.finite.numDof ) + activeBoundary.opL{be,n};
                                            end                            
                                            offset = offset + element.finite.numDof;
                                        end 
                                    end 
                                        
                                end

                                if (~activeBoundary.singleL)
                                    Gtmp( : ) = Gtmp( : ) + activeBoundary.opG{be};
                                end

                                % Remove prescribed columns from L
                                if (withStrongBC)
                                    ind  = ~prescribed;
                                    Ltmp = Ltmp(:,ind);
                                else
                                    ind = 1:size(s.K{el},1);
                                end

                                % Create the weight arrays
                                try
                                    if (activeBoundary.singleL)
                                        We = activeBoundary.W{be};
                                        We = repmat(We,s.Nvar,1);
                                    else
                                        We = activeBoundary.W{be};
                                    end
                                    sqrtWe = sqrt(We);
                                catch MExp
                                    error('error with boundary condition')
                                end

                                if (useMex)
                                    try
                                        s.K{el}(ind,ind) = s.K{el}(ind,ind) + weight * dsyrkL( full( sqrtWe .* Ltmp ) );
                                    catch
                                        We = activeBoundary.W{be};
                                        numEquations = size(Ltmp,1) / size(We,1);
                                        We = repmat(We,numEquations,1);
                                        sqrtWe = sqrt(We);
                                        s.K{el}(ind,ind) = s.K{el}(ind,ind) + weight * dsyrkL( full( sqrtWe .* Ltmp ) );
                                    end
                                else
                                    s.K{el}(ind,ind) = s.K{el}(ind,ind) + weight * ( Ltmp' * ( We .* Ltmp ) );
                                end
                                s.G{el}(  ind  ) = s.G{el}(  ind  ) + weight * Ltmp' * ( We .* Gtmp );
                            end
                        
                        else
                            % global L (boundary)
                            

                            
                            % Is this element located on this processor?
                            if ismember(e,s.mesh.eRange)

                                element = s.mesh.getElement(e);
                                el = element.localID;

                                % Determine the number of free DOFs and quadrature 
                                % points (QP) for this element
                                if (variable==0)
                                    numDof = element.finite.numDof * s.Nvar;
                                elseif (numel(variable)==1)
                                    numDof = element.finite.numDof;
                                else
                                    numDof = element.finite.numDof * numel(variable);
                                end
                                numQP  = element.finite.numQuadratureNodes;

%                                 % Set the prescribed flags for all variables (DOFs)
%                                 % This is used for strong BC
%                                 if (withStrongBC)
%                                     prescribed = false(numDof,1); %#ok<UNRCH>
%                                     offset = 0;
%                                     if (variable==0)
%                                         for n=1:s.Nvar
%                                             prescribed( offset+1 : offset+element.finite.numDof ) = s.getField(n).getPrescribed(e);
%                                             offset = offset + element.finite.numDof;
%                                         end 
%                                     else
%                                         prescribed( offset+1 : offset+element.finite.numDof ) = selectedField.getPrescribed(e);
%                                         offset = offset + element.finite.numDof;
%                                     end
%                                 end

                                % Initialize temporary L and G arrays
                                Ltmp = sparse(numQP,numDof);
                                Gtmp = zeros(numQP,1);

                                qPos = dh.qdofs{e}(:);
                                mPos = dh.dofs{e}(:);
                                
                                % Add the L and G for each variable to the temporary 
                                % L and G arrays.
                                offset = 0;
                                if (variable==0)                        
                                    for n=1:s.Nvar
                                        if (~isempty(activeBoundary.opL{be,n}))                                
                                            Lb( qPos , offset+1:offset+element.finite.numDof ) = Lb( qPos , offset+1:offset+element.finite.numDof ) + activeBoundary.opL{be,n};
                                        end                            
                                        offset = offset + element.finite.numDof;
                                    end
                                elseif (numel(variable)==1)
                                    if (isa(variable,'Field'))
                                        variable = variable.getVariable;
                                    end

                                    if (~isempty(activeBoundary.opL{be,variable}))                                
                                        Lb( qPos , offset+mPos ) = Lb( qPos , offset+mPos ) + activeBoundary.opL{be,variable};
                                    end 
                                else
                                    for n=variable
                                        if (~isempty(activeBoundary.opL{be,n}))                                
                                            Ltmp( : , offset+1:offset+element.finite.numDof ) = Ltmp( : , offset+1:offset+element.finite.numDof ) + activeBoundary.opL{be,n};
                                        end                            
                                        offset = offset + element.finite.numDof;
                                    end
                                end

                                Gb( qPos ) = Gb( qPos ) + activeBoundary.opG{be};

                                Wb( qPos ) = Wb( qPos ) + activeBoundary.W{be};
                                
%                                 % Remove prescribed columns from L
%                                 if (withStrongBC)
%                                     ind  = ~prescribed;
%                                     Ltmp = Ltmp(:,ind);
%                                 else
%                                     ind = 1:size(s.K{el},1);
%                                 end
% 
%                                 % Create the weight arrays
%                                 try
%                                     We     = activeBoundary.W{be};                            
%                                     sqrtWe = sqrt(We);
%                                 catch MExp
%                                     error('error with boundary condition')
%                                 end
% 
%                                 if (useMex)
%                                     s.K{el}(ind,ind) = s.K{el}(ind,ind) + weight * dsyrkL( full( sqrtWe .* Ltmp ) );
%                                 else
%                                     s.K{el}(ind,ind) = s.K{el}(ind,ind) + weight * ( Ltmp' * ( We .* Ltmp ) );
%                                 end
%                                 s.G{el}(  ind  ) = s.G{el}(  ind  ) + weight * Ltmp' * ( We .* Gtmp );
                            end
                            

                            
                        end

                    end
                    
                    if (s.globalL)
                        Ab = Lb' * (Wb .* Lb);
                        fb = Lb' * (Wb .* Gb);

                        s.A = s.A + weight * Ab;
                        s.f = s.f + weight * fb;
                    end
                
                end
            end
            
%             %% Add residual correction
%             withResidualCorrection = false;
%             if (withResidualCorrection)
%                 for e=1:s.mesh.numLocalElements
%                     el = e;
%                     if (el==1)
%                         avRes = s.residualField{1}.value{1};
%                         avRes(end,:) = avRes(end,:) - (s.residualField{1}.value{2}(1,:));
%                         avRes(:,end) = avRes(:,end) - (s.residualField{1}.value{3}(:,1)); 
%                         avRes(1,1:end-1) = 2*avRes(1,1:end-1);
%                         avRes(2:end,1) = 2*avRes(2:end,1);
%                         resFix = avRes(:) - s.residualField{1}.value{1}(:);
%                     elseif (el==2)
%                         avRes = s.residualField{1}.value{2};
%                         avRes(1,:) = avRes(1,:) - (s.residualField{1}.value{1}(end,:));
%                         avRes(:,end) = avRes(:,end) - (s.residualField{1}.value{4}(:,1));
%                         avRes(end,1:end-1) = 2*avRes(end,1:end-1);
%                         avRes(2:end-1,1) = 2*avRes(2:end-1,1);
%                         resFix = avRes(:) - s.residualField{1}.value{2}(:);
%                     elseif (el==3)
%                         avRes = s.residualField{1}.value{el};
%                         avRes(:,1) = avRes(:,1) - (s.residualField{1}.value{1}(:,end));  
%                         avRes(end,:) = avRes(end,:) - (s.residualField{1}.value{4}(1,:)); 
%                         avRes(1,2:end-1) = 2*avRes(1,2:end-1);
%                         avRes(2:end-1,end) = 2*avRes(2:end-1,end);
%                         resFix = avRes(:) - s.residualField{1}.value{el}(:);
%                     elseif (el==4)
%                         avRes = s.residualField{1}.value{el};
%                         avRes(:,1) = avRes(:,1) - (s.residualField{1}.value{2}(:,end));  
%                         avRes(1,:) = avRes(1,:) - (s.residualField{1}.value{3}(end,:));
%                         avRes(end,2:end) = 2*avRes(end,2:end);
%                         avRes(2:end-1,end) = 2*avRes(2:end-1,end);
%                         resFix = avRes(:) - s.residualField{1}.value{el}(:);
%                     else
%                         resFix = vecZ;
%                     end
% 
%                     s.G{e} = s.G{e} + 1 * s.opL{e}' * ( (s.W{e}) .* resFix );
%                 end
%             end
            
            % Compute the time used for settings up the normal equations
            t2 = toc; time = t2-t1;
        end

        function resetBoundaryConditions(s)
%             for i=1:numel(s.boundary)
%                 if (~isempty(s.boundary{i}))
%                     s.boundary{i}.reset();
%                 end
%             end
            s.resetFEBoundaries;
            s.setNumFEBoundary(30);
            if (System.rank==0 && System.settings.outp.debug)
                fprintf('PHYSICS    : Boundary/initial conditions have been reset for %s\n',s.name)
            end
        end
        
        function updateBoundaryConditions(s) %#ok<MANU>
            % no update by default, but you can overload this function in a 
            % derived Physics if required
        end
        
        function initial(s)
            if (System.settings.outp.debug)
                fprintf('started with initial physics on rank %d\n',System.rank)
            end
            
            cellfun(@(x) x.reset, s.fields);
            activeSettings = System.settings;
            activeSettings.setInitialConditions(s);
            
            if (System.settings.outp.debug)
                fprintf('finished with initial physics on rank %d\n',System.rank)
            end
        end

        function setInitial(s,maxLevel)
            % General function to set the initial solution (which is provided 
            % by the Settings.m file) to the physical fields. During this
            % function the mesh is refined (as defined by the criteria in the
            % Settings.m file) and the arrays (that depend on the number of 
            % elements) in the physics object are initialized.

            % Reset all the fields and set the initial solution
            %cellfun(@(x) x.reset, s.fields);
            s.initial();
            
            % Set the refine & coarsen conditions, the initial field values
            % can be used to set these flags
            activeSettings = System.settings;
            refineCondition     = @(e,refinementLevel) activeSettings.getRefinementCriteria(s,e,refinementLevel);
            coarsenCondition    = @(e,refinementLevel) activeSettings.getCoarseningCriteria(s,e,refinementLevel);
            
            % 
            updatePhysics = @() s.initial();
            s.mesh.meshUpdate(maxLevel,refineCondition,coarsenCondition,updatePhysics,s.name);
            
            
            
%             % Set some mesh properties before refining the mesh
%             s.mesh.initAMR();
            
            
            
%             % Refine the mesh upto maxLevel
%             for i=1:maxLevel
%                 
%                 refineFunction  = @(e,refinementLevel) Settings.getRefinementCriteria(s,e,refinementLevel);
%                 coarsenFunction = @(e,refinementLevel) Settings.getCoarseningCriteria(s,e,refinementLevel);
%                 
%                 % Check if the set solution will update the mesh
%                 [isConverged,isUpdated] = s.mesh.processAMR( maxLevel, refineFunction, coarsenFunction );
%                                 
%                 if (isUpdated)                    
%                     % Reset all fields and set the initial solution
%                     cellfun(@(x) x.reset, s.fields);
%                     s.initial();
%                 end
%                 
%                 if (isConverged)
%                     break;
%                 end
%             end
            
            % Initialize the physics properties (depends on the number of elements)
            s.init();         
        end
        
        function setWeightAndScale(s,element,scaler)
            if (nargin==2)
                scaler = 1;                     % default setting
            end
            
            %% Extract element info
            el      = element.localID;          % local element id
            e       = element.id;               % global element id
            Wel     = element.getW;             % quadrature weights
            vecZ    = element.getVecZ;          % zero vector
            Ze      = s.mesh.Z{e};              % AMR scaling
            
            %% Default eqWeights (obtained from Settings.m)
            eqW = repmat(s.activeEqWeights',numel(vecZ),1);
            
            %% Equation boundary weights
            % An increased weight for the equations in quadrature 
            % points on the boundary ensures a solution that obeys the
            % equation locally better. However, the global residual 
            % (and thereby global error) might be higher as well. By
            % default the weight is set to 1, so it's disabled.
            bcW = element.getBoundaryQuadratureWeights;
            bcW = repmat(bcW(:),s.nEq,1);
            
            %% Scaling by element level
            elementLevel = (1)^element.lvl;
            
            %% Compute the full scaler (vector operation)
            fullScaler = elementLevel * eqW(:) .* bcW(:) .* scaler(:);
            
            %% Scale both L and G (matrix-vector and vector-vector operation)
            s.opL{el} = fullScaler .* s.opL{el};
            s.opG{el} = fullScaler .* s.opG{el};
            
            %% AMR scaling (apply only to L)
            nVar = numel(s.activeVariables);
            s.opL{el} = s.opL{el} * superkron(eye(nVar),Ze);

            %% Set the quadrature weights
            s.W{el}      = repmat(Wel,s.nEq,1);
            s.sqrtW{el}  = repmat(sqrt(Wel),s.nEq,1);
        end
        
        function meshUpdate(s,maxLevel)
            % General meshUpdate
            
            activeSettings      = System.settings;
            refineCondition     = @(e,refinementLevel) activeSettings.getRefinementCriteria(s,e,refinementLevel);
            coarsenCondition    = @(e,refinementLevel) activeSettings.getCoarseningCriteria(s,e,refinementLevel);
            updatePhysics       = @() s.meshUpdateFields;
            
            s.mesh.meshUpdate(maxLevel,refineCondition,coarsenCondition,updatePhysics,s.name);
        end
        
        function meshUpdateFields(s)
            for n=1:numel(s.fields)
                s.fields{n}.meshUpdate;
            end
        end
        
        function out = mergedDoFHandler(s,var)
            % Merges the DoFHandler for all variables (fields)
            
            if (nargin==1)
                var = s.variables;
            end
            
            % qDoF numbering
            for n=1:numel(var)
                %dofHandler = s.mesh.getDoFHandler( s.periodic(n) );
                
                dofHandler = s.fields{ var(n) }.getDoFHandler;

                if (n==1)
                    for e=1:numel(dofHandler.dofs)
                        out.qdofs{e} = dofHandler.qdofs{e}(:);
                    end
                    out.numqDof = dofHandler.numqDof;
                else
                    for e=1:numel(dofHandler.qdofs)
                        out.qdofs{e} = [ out.qdofs{e}; out.numqDof+dofHandler.qdofs{e}(:)];
                    end
                    out.numqDof = out.numqDof + dofHandler.numqDof;
                end
            end
            
            % DoF numbering
            for n=1:numel(var)
                %dofHandler = s.mesh.getDoFHandler( s.periodic(n) );
                
                dofHandler = s.fields{ var(n) }.getDoFHandler;

                if (n==1)
                    for e=1:numel(dofHandler.dofs)
                        out.dofs{e} = dofHandler.dofs{e}(:);
                    end
                    out.numDof = dofHandler.numDof;
                else
                    for e=1:numel(dofHandler.dofs)
                        out.dofs{e} = [ out.dofs{e}; out.numDof+dofHandler.dofs{e}(:)];
                    end
                    out.numDof = out.numDof + dofHandler.numDof;
                end
            end
            
            % Local DoF numbering
            for n=1:numel(var)
%                 dofHandler = s.mesh.getDoFHandler( s.periodic(n) );
                dofHandler = s.fields{ var(n) }.getDoFHandler;
                
                if (n==1)
                    for e=1:numel(dofHandler.localDofs)
                        out.localDofs{e} = dofHandler.localDofs{e}(:);
                    end
                    out.numLocalDof = dofHandler.numLocalDof;
                else
                    for e=1:numel(dofHandler.localDofs)
                        out.localDofs{e} = [ out.localDofs{e}; out.numLocalDof+dofHandler.localDofs{e}(:)];
                    end
                    out.numLocalDof = out.numLocalDof + dofHandler.numLocalDof;
                end
            end
            
            % Shared DoFs
            for n=1:numel(var)
%                 dofHandler = s.mesh.getDoFHandler( s.periodic(n) );
                dofHandler = s.fields{ var(n) }.getDoFHandler;
                
                if (n==1)
                    out.sharedDoFs = dofHandler.sharedDoFs;
                else
                    for i=1:numel(out.sharedDoFs)
                        out.sharedDoFs{i} = [ out.sharedDoFs{i}; dofHandler.sharedDoFs{i}];
                    end
                end
            end
            
            % DoFRanks
            for n=1:numel(var)
%                 dofHandler = s.mesh.getDoFHandler( s.periodic(n) );
                dofHandler = s.fields{ var(n) }.getDoFHandler;
                
                if (n==1)
                    out.dofRanks = dofHandler.dofRanks;
                else
                    out.dofRanks = [ out.dofRanks; dofHandler.dofRanks];
                end
            end
            
            % eRanks
            for n=1:numel(var)
%                 dofHandler = s.mesh.getDoFHandler( s.periodic(n) );
                dofHandler = s.fields{ var(n) }.getDoFHandler;
                
                if (n==1)
                    out.eRanks = dofHandler.eRanks;
                else
                    out.eRanks = [ out.eRanks dofHandler.eRanks];
                end
            end
            
        end

        function copyBoundarySolution(s,source,destination,varargin)
            % Function to move the solution from one side of the mesh to the
            % opposite side. By default this is done for all variables, however it 
            % is possible to request it for a single variable. The scaling is
            % by default 1, but it can be useful if a mean value must be
            % preserved (for a temperature field for example).
        
            % Process the input
            [variable,scaling] = VerifyInput(varargin,{1:s.Nvar,1});
            
            % Extract the patches
            src = source.patchList{1};
            dst = destination.patchList{1};
            
            try
                for n=variable
                    tmpField = s.fields{n};

                    tmpField.active.needsMeshUpdate;
                    if (tmpField.active.needsMeshUpdate)
                        tmpField.active.meshUpdate;
                    end

                    %srcElementList = [src.elements.localID];
                    %dstElementList = [dst.elements.localID];
                    
                    srcElementList = [src.elements.id];
                    dstElementList = [dst.elements.id];
                    
                    if (numel(srcElementList)~=numel(dstElementList))
                        error('LSQDIM:physics:Physics.m - copyBoundarySolution error : unequal number of elements between src and dst patches')
                    end
                    %tmpField.copyBoundarySolution(srcElementList,dstElementList,src.dir,scaling);
                    
                    if (src.spaceDimension==1)
                        tmpField.copyBoundarySolutionValue(srcElementList,dstElementList,src.normal,scaling);
                    elseif (src.spaceDimension==2)
                        tmpField.copyBoundarySolutionValue(srcElementList,dstElementList,src.dir,scaling);
                    end
                end
                
                if (System.rank==0 && System.settings.outp.debug)
                    fprintf('PHYSICS    : Time slab has been updated for %s\n',s.name)
                end
            catch
                fprintf('Fatal error : there is an issue with Physics.copyBoundarySolution, nothing has been updated!\n')
                save(['copyBoundarySolution.' int2str(System.rank) '.mat']);
                if (System.nproc>1)
                    exit
                end
            end
        end

        function updateTimeSlab(s,variable)
            % Function to move the solution at the end of a time slab to the
            % beginning. By default this is done for all variables, however it 
            % is possible to request it for a single variable.
            if nargin==1
                variable = 1:s.Nvar; % work on all variables
            end
            
            %boundary = s.mesh.getBoundary(s.mesh.dim,0);             %#ok<PROPLC>
            
            %patch0 = s.getMesh.getPatch(s.mesh.dim,0);
            patch1 = s.getMesh.getPatch(s.mesh.dim,1);
            
            try
                for n=variable
                    tmpField = s.fields{n};
                    
                    % Create an elementList
%                     elementList = [];
%                     for be=1:boundary.getNumElements
%                         e = boundary.getElementNumbers(be);
%                         if ismember(e,s.getMesh.eRange)
%                             elementList = [elementList e];
%                         end
%                     end
                    
                    % Create the (global) elementList, which can be
                    % converted in a local list by using an arrayfun().
%                    elementList = intersect(boundary.getElementNumbers',s.getMesh.eRange); %#ok<PROPLC>
%                    localElementList = arrayfun(@(x) s.getMesh.getElement(x).localID,elementList);

                    tmpField.active.needsMeshUpdate;
                    if (tmpField.active.needsMeshUpdate)
                        tmpField.active.meshUpdate;
                    end

                    localElementList = [patch1.elements.localID];
                    tmpField.moveSolution(localElementList,s.mesh.dim); % 3 = z-direction
                end
                
                if (System.rank==0 && System.settings.outp.debug)
                    fprintf('PHYSICS    : Time slab has been updated for %s\n',s.name)
                end
            catch
                fprintf('Fatal error : there is an issue with Physics.updateTimeSlab, nothing has been updated!\n')
                save(['timeslab.' int2str(System.rank) '.mat']);
                if (System.nproc>1)
                    exit
                end
            end
        end
        
        function setPenalty(s,activeFEBoundary,field,value,bcType,type)
            
            
            % The 'field' input parameter can be a Field object, or just a
            % field number. If it is a number, select the requested Field
            % object from the fields inside this Physics
            if (~isa(field,'Field'))
                field = s.fields{field};
            end
            
            % For a strong bc only prescribed flags are set to true, for a
            % weak bc 
            switch bcType
                case BCType.Strong
                    field.current.setAlpha( activeFEBoundary.side, 0, value );     % 0 means the value is NOT a derivative 
                    for be=1:activeFEBoundary.getNumLocalElements
                        e = activeFEBoundary.getElementNumbers(be);
                        if inrange(e,1,s.getNumLocalElements)
                            field.setPrescribed(e,activeFEBoundary.Hpos);
                        end
                    end

                case BCType.Weak

                    % Initialize the LGW cell arrays of this weak FEBoundary
                    activeFEBoundary.init(s.Nvar);

                    for be=1:activeFEBoundary.getNumElements

                            % Convert the boundary element number (be) to a
                            % mesh element number (e).
                            e = activeFEBoundary.getElementNumbers(be);
                            
                            % Check if this element number is present on this
                            % processor, and process if that's the case.
                            if ismember(e,s.mesh.eRange)

                                element = s.mesh.getElement(e); 

                                m = element.finite.numQuadratureNodes;                        % integration points (quadrature)
                                n = element.finite.numDof;                         % mode points (DOFs)    

                                % Initialize the sparse element arrays
                                activeFEBoundary.initElementArray(be,m,n);                           

                                % Determine the valueVector
                                if (~isa(value,'function_handle'))
                                    if ~isa(value,'Field')
                                        valueVector = value*ones( m, 1 );
                                    else
                                        try
                                            tmp_value   = value.getValue(e);        % NB: e is a global element number
                                            valueVector = tmp_value(:);
                                        catch
                                            save(['setBC.' int2str(System.rank) '.mat']);
                                            disp('problem in setBC')
                                            if (System.nproc>1)
                                                exit
                                            end
                                        end
                                    end
                                else
                                    [Xc,Yc,Zc,Tc] = element.getNodes();
                                    Xc = squeeze(Xc); Yc = squeeze(Yc);
                                    Zc = squeeze(Zc); Tc = squeeze(Tc);
                                    
                                    valueVector = value(Xc,Yc,Zc,Tc);                                
                                end

                                if (type==0)
                                    activeFEBoundary.setValue(be,element,field.getVariable,valueVector,s.mesh,e);
                                elseif (type==1)
                                    activeFEBoundary.setDerivative(be,element,field.getVariable,valueVector,s.mesh,e);
                                elseif (type==2)
                                    activeFEBoundary.setDerivative2(be,element,field.getVariable,valueVector,s.mesh,e);
                                end
                            end
                    end
            end
        end
        
        %% Boundary condition functions
        function setBC(s,activeFEBoundary,field,value,bcType,type,weight,direction,angle,singleL)
            % Function to set a boundary condition.
            %   activeFEBoundary        The boundary to work on [FEBoundary]
            %   field                   The field to apply to bc to
            %   value                   The value to assign to the boundary
            %   bcType                  Type of bc [BCType]
            %   weight                  Weight for the bc (optional)                    
            
            % Assign the boundary condition weight
            if nargin<7
                weight = System.settings.comp.boundary.weight;
            end
            if ( isa(activeFEBoundary,'FEBoundary') )
                % nothing to do
            elseif (isa(activeFEBoundary,'Patch') )
                activeFEBoundary = s.initFEBoundary( activeFEBoundary );
            else
                activeFEBoundary = s.initFEBoundary( PatchCollection.get( activeFEBoundary ) );
            end
            activeFEBoundary.weight = weight;
            
            if nargin<8 || isempty(direction)
                direction = Direction.normal;
            end
            
            if (nargin<9)
                angle = [];
            end
            
            if (nargin<10)
                singleL = false;
            end
            
            % The 'field' input parameter can be a Field object, or just a
            % field number. If it is a number, select the requested Field
            % object from the fields inside this Physics
            if (~isa(field,'Field') && ~isempty(field))
                field = s.fields{field};
            end

            % For a strong bc only prescribed flags are set to true, for a
            % weak bc 
            switch bcType
                case BCType.Strong
                    field.current.setAlpha( activeFEBoundary.side, 0, value );     % 0 means the value is NOT a derivative 
                    for be=1:activeFEBoundary.getNumLocalElements
                        e = activeFEBoundary.getElementNumbers(be);
                        if inrange(e,1,s.getNumLocalElements)
                            field.setPrescribed(e,activeFEBoundary.Hpos);
                        end
                    end

                case BCType.Weak

                    % Initialize the LGW cell arrays of this weak FEBoundary
                    if (singleL)
                        activeFEBoundary.init(1);
                        activeFEBoundary.singleL = singleL;
                    else
                        activeFEBoundary.init(s.Nvar);
                    end

                    for be=1:activeFEBoundary.getNumElements

                        % Convert the boundary element number (be) to a
                        % mesh element number (e).
                        e = activeFEBoundary.getElementNumbers(be);

                        % Check if this element number is present on this
                        % processor, and process if that's the case.
                        if ismember(e,s.mesh.eRange)

                            element = s.mesh.getElement(e); 

                            m = element.finite.numQuadratureNodes;                        % integration points (quadrature)
                            n = element.finite.numDof;                         % mode points (DOFs)    

                            % Initialize the sparse element arrays
                            activeFEBoundary.initElementArray(be,m,n);                           

                            % Determine the valueVector
                            if (~isa(value,'function_handle'))
                                if ~isa(value,'Field')
                                    %valueVector = value*ones( nnz(indq), 1 );
                                    valueVector = value*ones( m, 1 );
                                else
                                    try
                                        %tmp_value = reshape(value.getValue(e),size(indq));
%                                             tmp_value   = value.getValue(e);
                                        if (type==BCType.Dirichlet)
                                            switch direction
                                                case Direction.gradC
                                                    %CH = PhysicsCollection.get('CH');
                                                    tmp_value   = value.x2(e);              % NB: e is a global element number; gradC
                                                otherwise
                                                    tmp_value   = value.getValue(e);        % NB: e is a global element number
                                            end
                                        elseif (type==BCType.Neumann)
                                            switch direction
                                                case Direction.normal
                                                    tmp_value   = value.x2(e);              % NB: e is a global element number
                                                case Direction.tangential
                                                    tmp_value   = value.x2(e);              % NB: e is a global element number
                                                case Direction.dynamicX
                                                    %tmp_value   = value.y2(e);              % NB: e is a global element number
                                                    tmp_value   = sqrt(3)/3 * value.y2(e);
                                                otherwise
                                                    error('Undefined boundary condition direction')
                                            end
                                        end
                                        valueVector = tmp_value(:);
                                        
%                                         if ( isequal(field.name,'omg') || isequal(field.name,'c') )
%                                             valueVector = 0.5*sqrt(3) * valueVector;
%                                         end
                                    catch
                                        save(['setBC.' int2str(System.rank) '.mat']);
                                        disp('problem in setBC')
                                        if (System.nproc>1)
                                            exit
                                        end
                                    end
                                end
                            else
                                % To me in the future: Why haven't you finished your own code? 14.10.2021
                                [Xc,Yc,Zc,Tc] = element.getNodes();
                                Xc = squeeze(Xc); Yc = squeeze(Yc);
                                Zc = squeeze(Zc); Tc = squeeze(Tc);


                                valueVector = value(Xc,Yc,Zc,Tc,e);
                            end

                            if (type==BCType.Dirichlet)
                                activeFEBoundary.setValue(be,element,field.getVariable,valueVector,s.mesh,e);
                                
                            elseif (type==BCType.Neumann)
                                switch direction
                                    case Direction.normal
                                        activeFEBoundary.setDerivative(be,element,field.getVariable,valueVector,s.mesh,e);
                                    case Direction.tangential
                                        activeFEBoundary.setDerivativeTangential(be,element,field.getVariable,valueVector,s.mesh,e);
                                    case Direction.dynamicAngle
                                        if ( ~isempty(angle) )
                                            activeFEBoundary.setDerivativeStaticAngle(be,element,field.getVariable,valueVector,s.mesh,e,angle);
                                        else
                                            activeFEBoundary.setDerivativeDynamicAngle(be,element,field.getVariable,valueVector,s.mesh,e);
                                        end
                                    case Direction.dynamicX
                                        %activeFEBoundary.setDerivative(be,element,field.getVariable,valueVector,s.mesh,e);
                                        activeFEBoundary.setDerivativeDynamicAngleX(be,element,field.getVariable,valueVector,s.mesh,e);
                                    case Direction.dynamicY
                                        %activeFEBoundary.setDerivative(be,element,field.getVariable,valueVector,s.mesh,e);
                                        activeFEBoundary.setDerivativeDynamicAngleY(be,element,field.getVariable,valueVector,s.mesh,e);  
                                    case Direction.gradC
                                        activeFEBoundary.setGradC(be,element,field.getVariable,valueVector,s.mesh,e);
                                    otherwise
                                        error('Undefined boundary condition direction')
                                end
                                
                            elseif (type==BCType.Neumann2)
                                activeFEBoundary.setDerivative2(be,element,field.getVariable,valueVector,s.mesh,e);
                                
                            elseif (type==BCType.GNBC)
                                activeFEBoundary.setGNBC(be,element,field,valueVector,s.mesh,e,s.timeMethod);
                                
                            elseif (type==BCType.WallPotential)
                                activeFEBoundary.setWallPotential(be,element,field,valueVector,s.mesh,e,s.timeMethod);
                                
                            elseif (type==BCType.CH1D)
                                activeFEBoundary.setCH1D(be,element,field,valueVector,s.mesh,e,s.timeMethod);
                                
                            elseif (type==BCType.CH0D)
                                activeFEBoundary.setCH0D(be,element,field,valueVector,s.mesh,e,s.timeMethod);                                
                                
                            elseif (type==BCType.AC1D)
                                activeFEBoundary.setAC1D(be,element,field,valueVector,s.mesh,e,s.timeMethod);
                            elseif (type==BCType.noslip)
                                activeFEBoundary.setSlipBC(be, element, field.getVariable, valueVector, s.mesh, e);
                            elseif (type==BCType.specialFWC)
                                activeFEBoundary.setPartialDirichletPartialNeumann(be, element, field.getVariable, valueVector, s.mesh, e);
                            elseif (type==BCType.testBCX)
                                activeFEBoundary.testBCX(be, element, field.getVariable, valueVector, s.mesh, e);
                            elseif (type==BCType.testBCY)
                                activeFEBoundary.testBCY(be, element, field.getVariable, valueVector, s.mesh, e);
                            elseif (type==BCType.testBCT)
                                activeFEBoundary.testBCT(be, element, field.getVariable, valueVector, s.mesh, e);
                            end
                        end
                    end            
            end
        end
        
        function updateBC(s) %#ok<MANU>
            % no update by default, but you can overload this function in a 
            % derived Physics if required
        end
        
        function time = processStrongBC(s)
            % This function reduces opL and moves the prescribed strong
            % solution to the rhs opG.
            
            t1 = toc;

            s.syncPrescribed;
            
            for e=s.mesh.eRange
                
                prescribed = s.getPrescribed(e);
                
                % Prescribed vector
                numPrescribed = nnz(prescribed);
                if (numPrescribed>0)
                                       
                    prescribedVector 	= s.getPrescribedValues(e);

                    % opL
                    L_tmp            	= full(s.opL{ g2l(e) });                             % copy current opL
                    opLprescribed      	= L_tmp(:,prescribed);            % prescribed section of opL
                    L_tmp(:,prescribed) = [];                                   % remove prescribed columns

                    % opG
                    G_tmp               = s.opG{ g2l(e) };                             % copy current opG
                    G_tmp             	= G_tmp - opLprescribed*prescribedVector; % move prescribed part to the rhs

                    % Save new operators
                    s.opL{e}                = L_tmp;
                    s.opG{e}                = G_tmp;
                end
            end
            
            t2 = toc;
            time = t2-t1;
        end
        
        %% Solve functions
        function solveNonLinear(s,couplingIteration)
            % Function to solve a nonlinear loop

            if nargin==1
                couplingIteration = 0;
            end
            
            % If the mesh has changed outside the current object, the fields and arrays need to be updated. 
            s.update();
            s.couplingIteration = couplingIteration;
            
            % Loop as long as requested, or until convergence
            for nonlinearIteration = 1 : s.maxNonlinear
                s.nonlinearIteration = nonlinearIteration;
                
%                 if nonlinearIteration>1
%                     s.coupled = true;
%                 end
                
                tStart = toc;
                
               	if (System.rank==0 && System.settings.outp.debug) 
                    fprintf(' Non linear step %d\n',s.nonlinearIteration)
                end

                % Copy the 'current' subField to the 'nonLinear' subField 
                % as a backup (used after the solve again to see if 
                % nonLinear convergence has occured already
                if (s.maxNonlinear>1)
                    s.copySolution(FieldType.Nonlinear);
                    if (System.rank==0 && System.settings.outp.debug)
                        fprintf('PHYSICS    : Nonlinear fields have been updated\n')
                    end
                end
                
                isConverged = false( s.numStages,1 );
                
                for stage=1:s.numStages
                
                    tStartStep = toc;
                
                    s.activeStage = stage;
                    
                    if ( s.nonlinearIteration==1 && s.timeMethod~=TimeMethod.SpaceTime && s.timeMethod~=TimeMethod.Steady)
                        % Extrapolate fields in time if no space-time
                        s.extrapolateFieldsInTime;
                        
                        if (System.rank==0 && System.settings.outp.debug)
                            fprintf('PHYSICS    : Fields have been extrapolated in time\n')
                        end
                    end
                    
                   % If multiple stages are used, each stage needs its
                    % own boundary conditions
                    if (s.numStages>1)
                        s.resetBoundaryConditions();
                        activeSettings = System.settings;
                        activeSettings.setBoundaryConditions( s );
                    end

                    if ( s.nonlinearIteration>1 || couplingIteration>1 )
                        s.updateBC();
                    end
                    
%                     t2 = toc;
%                     timing = t2-tStartStep;
%                     fprintf('Setup bc in %f seconds\n',timing)
                    
                    if (System.rank==0 && System.settings.outp.debug)
                        fprintf('PHYSICS    : Boundary conditions have been setup for step %d\n',s.activeStage)
                    end

                    % Setup the equations to solve
                    s.code_times.non_linear.time_opL = s.setupEquations();
                    if (System.rank==0 && System.settings.outp.debug)
                        fprintf('PHYSICS    : Equations have been setup for step %d\n',s.activeStage)
                    end
%{
Julián ADDED: 'time_opL', 'time_setup', 'time_solve' and 'time_total' will hold
the times that I need to measure for the training I am currently doing. The
idea is to get an idea of how different parameters of the solver affect
the time that the code spends on different parts.
%}
                    % Solve the system of equations
                    [s.code_times.non_linear.time_setup, s.code_times.non_linear.time_solve] = ...
                        s.solve( System.settings.comp.CG.method, s.activeVariables );
                    if (System.rank==0 && System.settings.outp.debug)
                        fprintf('PHYSICS    : Solve done for step %d\n',s.activeStage)
                    end
                    
%                     t2 = toc;
%                     fprintf('Solve equations in %f seconds\n',t2-t1)
                    
%                     isConverged = s.isConverged(FieldType.Nonlinear,s.nonlinearIteration);
%                     
                    tEndStep = toc;
                    timeStep = tEndStep - tStartStep;
                    
                    if (s.numStages>1)
                        %fprintf('Stage %d done in %f seconds\n',step,timeStep)
                    end
                    
                    s.computeH1Norm;
                    
                    if (s.maxNonlinear>1)
                        isConverged(stage) = s.isConverged(FieldType.Nonlinear,s.nonlinearIteration,s.maxNonlinear);
                        System.writeToScreen('nonlinear',s.name,stage,s.numStages,s.nonlinearIteration,timeStep,s.solverInfo,s.variables,s.norms{1},s.norms{2},s.activeVariables);
                    else
                        isConverged(stage) = s.isConverged(FieldType.Nonlinear,s.nonlinearIteration,s.maxNonlinear);
                        System.writeToScreen('nonlinear',s.name,stage,s.numStages,s.nonlinearIteration,timeStep,s.solverInfo,s.variables,s.norms{1},s.norms{2},s.activeVariables);
                    end
%                     
                    %isConverged = s.isConverged(FieldType.Nonlinear,nonlinearIteration);
%                     System.writeToScreen('nonlinear',s.name,nonlinearIteration,timeStep,s.solverInfo,s.norms{1},s.norms{2});                    
                end

%{
  Julián MOD: Total time is stored in
  'physics.code_times.non_linear.time_total' to be able to compute each
  fraction of the time spent on different parts of the code.
%}
                s.code_times.non_linear.time_total = timeStep;
                
                if (System.settings.outp.writeEveryNL)
                    s.write_vtu;
                end
                
                % Callback call
                activeSettings = System.settings;
                activeSettings.nonLinearCallback( s );
                
                % Check convergence
                if (s.maxNonlinear>1)
%                     isConverged = s.isConverged(FieldType.Nonlinear,nonlinearIteration);
%                     System.writeToScreen('nonlinear',s.name,nonlinearIteration,time,s.solverInfo,s.norms{1},s.norms{2});
                    
                    if ( all( isConverged(s.needsConvergence) ) )
                        break;
                    end
                else
%                     System.writeToScreen('nonlinear',s.name,nonlinearIteration,time,s.solverInfo,[],[]);
                    break
                end
            end
            
        end % function solveNonLinear
   
        %% Post processing functions
        function computeError(s,position)
            cellfun( @(x) x.computeError(position), s.fields );
        end
        
        function computeRelativeError(s,position)
            cellfun( @(x) x.computeRelativeError(position), s.fields );
        end
   
        % Function to update/assign the activeField. This field can be a
        % combination of the 'current', 'nonlinear' and 'coupled'
        % subFields. For the physics group that is being solved, the
        % combination of 'current' and 'nonlinear' is used. For physics
        % that only provides some values a combination of 'current' and 
        % 'coupled' is used. Since the meshes between physics might differ,
        % this field might be interpolated.
        function updateActiveField(s,physics)
            for p=1:numel(physics)
                if (isequal(s,physics{p}))
                    for n=1:physics{p}.Nvar
                        physics{p}.fields{n}.updateActiveField(FieldType.Nonlinear);
                    end
                else
                    for n=1:physics{p}.Nvar
                        physics{p}.fields{n}.updateActiveField(FieldType.Coupled,s.getMesh);
                    end
                end
            end
        end
        
        function updateFields(s)
            if (~System.settings.anal.useCellFun)
                for i=1:numel(s.fields)
                    s.fields{i}.update;
                end
            else
                cellfun(@(x) x.update, s.fields);
            end
        end
        
        function copySolution(s,destination,source)
            if nargin==2
                % Move the current solution for all fields of this Physics to 'destination'
                for n=1:s.Nvar
                    s.fields{n}.copySolution(destination);
                end
            else
                % Move the solution for all fields of this Physics from 'source' to 'destination'
                for n=1:s.Nvar
                    s.fields{n}.copySolution(destination,source);
                end
            end
        end
        
        function shiftTimeSolutions(s,timeMethod,extrapolationOrder)
            % Function to shift the solution one time step down
            
            % If no additional arguments are passed, use the default
            % timeMethod. Otherwise use the passed timeMethod for once.
            if (nargin==1)
                timeMethod = s.timeMethod;
            else
                s.timeMethod = timeMethod;
            end
            
            nVar = s.Nvar;
            
            if (isa(s,'NavierStokes3D'))
                nVar = nVar-1;
                s.fields{ 4 }.shiftTimeSolutions( 1 , true );
            end
            
            switch timeMethod
                case TimeMethod.SpaceTime
                    % to add...
                    
                case TimeMethod.BDF2
                    for i=1:nVar
                        s.fields{ i }.shiftTimeSolutions( 2, extrapolationOrder );      % shift 2 time level
                    end
                    
                case TimeMethod.EulerImplicit
                    for i=1:nVar
                        s.fields{ i }.shiftTimeSolutions( 1, 1 );      % shift 1 time level
                    end
            end
        end
        
        % Function to return the 'variable' field of class 's', which is to
        % be used in the coupled class 'physics'. Within the Field class it
        % is checked if the physics.mesh corresponds to the coupled.mesh.
        % If this is not the case, the field values for the physics.mesh
        % are determined.
        function out = getField(s,variable)            
            out = s.fields{variable};
        end
        
        function setActiveField(s,fieldType,mesh)
            
            if (System.settings.anal.debug)
                fprintf('Set active field for %s to %s\n',s.name,char(fieldType))
            end
            
            if (nargin==2)
                mesh = [];
            end
            if (~System.settings.anal.useCellFun)
                for i=1:numel(s.fields)
                    s.fields{i}.setFieldMesh(fieldType,mesh);
                end 
            else
                cellfun(@(x) x.setFieldMesh(fieldType,mesh), s.fields);
            end
        end
        
        function syncPrescribed(s)
            % Used for strong BC, not tested yet.
            % Synchronize the 'prescribed' flag for all fields. This flags
            % indicates if a DOF is prescribed or not.
            for n=1:s.Nvar
                selectedField = s.getField( n );
                selectedField.syncPrescribed;
            end
        end
        
        function prescribed = getPrescribed(s,e)
            % Used for strong BC, not tested yet.
            
            prescribed = [];
            
            if nargin==2
                % OLD CODE
                %prescribed = [s.uField.getPrescribed(e); s.vField.getPrescribed(e); s.pField.getPrescribed(e)];
                
                % NEW CODE
                for n=s.activeVariables
                    selectedField = s.getField( n );
                    prescribed = [prescribed ; selectedField.getPrescribed(e) ]; %#ok<AGROW>
                end
            else
                prescribed = cell(s.mesh.numLocalElements,1);
                for e=1:numel(prescribed)
                    element = s.mesh.getLocalElement(e);
                    prescribed{e} = s.getPrescribed( element.globalID );
                end
            end
        end
        
        function out = getPrescribedValues(s,e)
            % Used for strong BC, not tested yet.
            
            % OLD CODE
            %out = [s.uField.getPrescribedValues(e); s.vField.getPrescribedValues(e); s.pField.getPrescribedValues(e)];
            
            % NEW CODE
            for n=1:s.Nvar
                selectedField = s.getField( n );
                out = [out ; selectedField.getPrescribedValues(e) ]; %#ok<AGROW>
            end
        end
        
%         function out = getTime(s,element,field)
%             if nargin==1
%                 switch s.timeMethod
%                     case TimeMethod.SpaceTime
%                         out = 0;
% 
%                     case TimeMethod.BDF2
%                         out = 3/2 / System.settings.time.step;
% 
%                     case TimeMethod.EulerImplicit
%                         out = 1 / System.settings.time.step;
%                         
%                     case TimeMethod.Steady
%                         out = 1; 
%                 end
%             elseif nargin==2
%                 switch s.timeMethod
%                     case TimeMethod.SpaceTime
%                         out = element.getDt;
% 
%                     case TimeMethod.BDF2
%                         out = 3/2 * element.getH / System.settings.time.step;
% 
%                     case TimeMethod.EulerImplicit
%                         out = element.getH / System.settings.time.step;
%                         
%                     case TimeMethod.Steady
%                         out = element.getH; 
%                 end
%             elseif nargin==3
%                 switch s.timeMethod
%                     case TimeMethod.SpaceTime
%                         out = element.getVecZ;
% 
%                     case TimeMethod.BDF2                    
%                         v1  = field.oldVal(element.id,s.mesh,1);
%                         v2  = field.oldVal(element.id,s.mesh,2);
%                         out = (2*v1 - 0.5*v2) / System.settings.time.step;
% 
%                     case TimeMethod.EulerImplicit
%                         v1  = field.oldVal(element.id,s.mesh,1);
%                         out = v1 / System.settings.time.step;
%                         
%                     case TimeMethod.Steady
%                         %v1  = field.oldVal(element.id,s.mesh,1);
%                         v1  = field.val2(element.id,s.mesh);
%                         out = v1;%/ System.settings.time.step;
%                 end
%             end
%         end
        
        function out = getTime(s,element,field)
            if nargin==1
                out = TimeIntegration.getTime(s.timeMethod);
            elseif nargin==2
                out = TimeIntegration.getTime(s.timeMethod,s.mesh,element);
            elseif nargin==3
                out = TimeIntegration.getTime(s.timeMethod,s.mesh,element,field);
            end
        end
        
        function [x,y,z] = getOldValue(s,element,field,level)
            if (nargin==3)
                level = 1;
            end
            x = field.oldVal_x(element.id,s.mesh,level);
            y = field.oldVal_y(element.id,s.mesh,level);
            z = field.oldVal_z(element.id,s.mesh,level);
        end
        
%         function out = getOldValue(s,element,field,level)
%             if (nargin==3)
%                 level = 1;
%             end
%             out = field.oldVal(element.id,s.mesh,level);
%         end
        
        function [ ddt_rhs ] = getTimeRHS(s,element,field,timeMethod)
            if nargin==3
                timeMethod = s.timeMethod;
            end
            
            % Verify the
            
            switch timeMethod
                case TimeMethod.SpaceTime
                    ddt_rhs = element.vecZ;
                    
                case TimeMethod.BDF2                    
                    v1 = field.oldVal(element.id,s.mesh,1);
                    v2 = field.oldVal(element.id,s.mesh,2);
                    
                    ddt_rhs = (2*v1 - 0.5*v2) / System.settings.time.step;
                    
                case TimeMethod.EulerImplicit
                    v1 = field.oldVal(element.id,s.mesh,1);
                    ddt_rhs = v1 / System.settings.time.step;
            end
        end
        
        % Serial direct solver, where the global quadrature numbering is
        % used to merge/sum identical quadrature points.
        function solveDirect(s)
            
            numDof = s.Nvar * s.mesh.numLocalDof;
            
            Lg = sparse(numDof,numDof);
            Wg = sparse(numDof,1);
            Gg = sparse(numDof,1);
            
            s.mesh.createGMq;
            
            % Add local L to global Lg
            for e=1:s.mesh.numLocalElements
                element = s.mesh.getElement(e);
                
                % Optimized L'WL through a mex function (BLAS routine)
                locp = [element.GM(:)  ; s.mesh.dofPerVariable + element.GM(:)];
                locq = [element.GMq(:) ; s.mesh.qpPerVariable + element.GMq(:)];
                Lg(locq,locp) = Lg(locq,locp) + s.opL{e};

                % Original L'WL through default Matlab multiplication
                % s.K{e} = s.opL{e}' * (s.W{e} .* s.opL{e});

                Wg(loc)     = s.W{e};
                
                %s.G{e} = s.opL{e}' * ( (s.W{e}) .* s.opG{e});
                Gg(loc)     = Gg(loc) + s.opG{e};
            end
            
            % Compute K
            % Optimized L'WL through a mex function (BLAS routine)
            Kg = Lg' .* Wg .* Lg;
            
            solution = Gg\Kg;
            
        end
        
        % Julián ADDED: Now the function 'solve' returns an array with two
        % values where the first one is the time of the setup of the
        % equations (computation of A and G matrices) and the second
        % element is the time spent on solving the linear problem.
        function [time_setup, time_solve] = solve( s, method, variable )
            
            % Some settings / parameters
            withStrongBC = false;
            
            if( nargin == 2 )
                variable = 0;   % 0 == all variables
                alphaTmp = s.getAlpha(variable);
            elseif (nargin==3)
                if isa(variable,'Field')
                    alphaTmp = variable.getAlphaArray;
                else
                    alphaTmp = s.getAlpha(variable);
                end
            end
            
            % Assign a dofHandler
            if (variable==0)
                dofHandler = s.mergedDoFHandler;
            elseif (numel(variable)==1)
                dofHandler = s.fields{variable}.getDoFHandler;
            else
                dofHandler = s.mergedDoFHandler(variable);
            end
            
            % Assign the strong BC
            if ( withStrongBC )
                time1 = s.processStrongBC(); %#ok<UNRCH>
                prescribed = s.getPrescribed;
            else
                prescribed = 0;
            end
               
            % Setup normal equations (includes the weak BC)
            time_setup = s.setupNormalEquations( variable );
            %fprintf('Time for setting up NE = %f\n',time2);
                        
            switch method
                case SolverMethod.hybridCG
                    % Iterative solver, using BLAS mex routines for matrix-vector operations
                    t1 = toc;

                    epsilon = System.settings.comp.CG.convergence;
                    tag     = [ s.name '-t' int2str(System.settings.time.iter) '-c' int2str(System.settings.comp.coupling.iter) '-nl' int2str(System.settings.comp.nonlinear.iter) ];
                    
                    % Solve the system of equations
                    %[alpha,s.solverInfo] = PCG_hybrid(s.K,s.G,dofHandler,epsilon,s.silent,alphaTmp,tag,s.opL,s.opG,s.W);    % used to test global residual
                    [alpha,s.solverInfo] = PCG_hybrid(s.K,s.G,dofHandler,epsilon,s.silent,alphaTmp,tag);

                    
%                     %% Bisymmetric matrix for Poisson equation (test)
%                     for e=1:numel(s.K)
%                         s.K{e} = full( s.K{e} + tril(s.K{e},-1)' );
%                     end
%                     K1 = s.K{1};
%                     issymmetric(K1) 
%                     K1 = K1([1,2,3,4,5,6,7,8,16,15,14,13,12,11,10,9],[1,2,3,4,5,6,7,8,16,15,14,13,12,11,10,9]);     % permute columns and rows
%                     dofs1 = dofHandler.dofs{1};
%                     scaler=[ 1 1; 1 -1];
%                     temp = kron(scaler,ones(2,2));
%                     K1 = diag(temp(dofs1)) * K1 * diag(temp(dofs1));                                                % create bisymmetric K
%                     
%                     issymmetric(K1) 
%                     isbisymmetric(K1)
                    
                    % JULIAN ADDED: variable CG tolerance
                    if System.settings.comp.CG.variable_convergence
                        system_instance = System.instance;
                        if s.solverInfo.iter == System.settings.comp.CG.maxIter && false
                            if s.solverInfo.finalRes > epsilon * 100
                                system_instance.settings.comp.CG.convergence = epsilon * 10;
                                system_instance.settings.comp.CG.hit_counter = 0;
                            end % if
                        elseif s.solverInfo.iter == 1
                            if System.settings.comp.CG.convergence > 10E-15 && System.settings.comp.CG.hit_counter == 2
                                system_instance.settings.comp.CG.convergence = System.settings.comp.CG.convergence / 50;
                                system_instance.settings.comp.CG.hit_counter = 0;
                            elseif System.settings.comp.CG.convergence > 10E-15 && System.settings.comp.CG.hit_counter < 2
                                system_instance.settings.comp.CG.hit_counter = system_instance.settings.comp.CG.hit_counter + 1;
                            end % if
                        else
                            system_instance.settings.comp.CG.hit_counter = 0;
                        end % if
                    end % if
                    t2 = toc;
                    %fprintf('Time in Matlab solver    = %f\n',t2-t1);

                    %System.setItem(10,'time',t2-t1,'%10.2f');
                    %System.setItem(11,'iter',s.iter_cg,'%8d');

                    % Set the alphas and values 'current' subFields for all variables
                    if ( isa(variable,'Field') )
                        variable.setAlphaArray(alpha);
                    else
                        s.setAlpha(alpha,variable);
                    end

                case SolverMethod.matlabCG
                    % Iterative solver, using Matlab only routines for matrix-vector operations                
                    epsilon = System.settings.comp.CG.convergence;

                    if (variable==0)
                        dofHandler = s.mergedDoFHandler;
                    else
                        dofHandler = s.fields{variable}.getDoFHandler; %s.mesh.getDoFHandler( s.periodic(variable) );
                    end

                    t1 = toc;

                    % Ensure the matrices are full (not triangular)
                    if ( issymmetric(s.K{1}) )
                        % do nothing
                    elseif ( istril(s.K{1}) )
                        for e=1:numel(s.K)
                            s.K{e} = sparse( s.K{e} + tril(s.K{e},-1)' );
                        end
                    elseif ( istriu(s.K{1}) )
                        for e=1:numel(s.K)
                            s.K{e} = sparse( s.K{e} + triu(s.K{e},1)' );
                        end
                    else
                        error('The provided matrix is neither symmetric or upper/lower triangular, this is not supported')
                    end

                    [alpha,s.solverInfo]=PCG_matlab(s.K,s.G,dofHandler.dofs,dofHandler.numDof,epsilon);

                    t2 = toc;
                    % fprintf('Time in Matlab solver    = %f\n',t2-t1);

                    %System.setItem(10,'time',t2-t1,'%10.2f');
                    %System.setItem(11,'iter',s.iter_cg,'%8d');

                    % Set the alphas and values 'current' subFields for all variables
                    if ( isa(variable,'Field') )
                        variable.setAlphaArray(alpha);
                    else
                        s.setAlpha(alpha,variable);
                    end
                    
                case SolverMethod.mexCG
                    % not implemented and tested yet
    %                 t3 = toc;
    %                 [s.alpha,s.iter_cg] = PrecElemCG_par_mex(s.K,s.G,s.getGMFree,s.prescribed,s.getNumDof,s.epsilon,alphaTmp);
    %                 t4 = toc;
    %                 fprintf('Time in Mex solver    = %f\n',t4-t3);
                
                case SolverMethod.direct
                    % Direct solver (matlab)
                
                    s.solverInfo = [];  % this line ensures no output is generated wrt residuals / number of iterations
                    
                    if (NMPI.instance.nproc>1)
                        error('No parallel direct solver available, use an iterative solver or run the code in serial')
                    end

                    dh = dofHandler;

                    t1 = toc;
                    
                    if (~s.globalL) 
                        if (s.useInverse)
                            if (isempty(s.stageSolver))
                                s.stageSolver = Solver();
                            end
                            if (s.mesh.isChanged && s.nonlinearIteration==1 && s.couplingIteration==1 )
                                s.stageSolver.reset;
                            end
                            s.stageSolver.update(s.K,s.G,dh,s.getPrescribed,alphaTmp,s.useInverse);
                            %s.stageSolver.solve;
                            s.setAlpha(s.stageSolver.x,variable);
                        else
                            temp = Solver(s.K,s.G,dh,prescribed,alphaTmp,s.useInverse);
                            %temp = Solver(s.K,s.G,dh,s.getPrescribed,alphaTmp,s.useInverse);
                            s.setAlpha(temp.x,variable);
                        end
                    else
                        temp = Solver(s.A,s.f,dh,0,alphaTmp);
                        s.setAlpha(temp.x,variable);
                    end
                    t2 = toc;
            end % switch
            
            time_solve = t2 - t1;
            % Julián ADDED: Now the function returns and array with the
            % time used to setup the equations and to solve the linear
            % problem.
        end % function solve
        
        function out = getNumDof(s)
            out = 0;
            for n=1:s.Nvar
                dofHandler = s.fields{n}.getDoFHandler;
                out = out + dofHandler.numDof;
            end
        end
        
        function out = getNumLocalDof(s)
            if (isempty(s.mesh.getNumLocalDof))
                s.mesh.setDofRank();
            end
            out = s.mesh.getNumLocalDof * s.Nvar;
        end
        
        function out = getVariableNames(s,prefix,suffix)
            out = cell(s.Nvar,1);
            if (nargin==1)
                prefix = '';
                suffix = '';
            elseif (nargin==2)
                suffix = '';
            end
            
            for i=1:s.Nvar
                out{i} = [prefix s.getField(i).name suffix];
            end
        end
        
        function out = getMaxIter(s,fieldType)
            switch fieldType
                case FieldType.Nonlinear
                    out = s.maxNonlinear;
                case FieldType.Coupled
                    out = s.maxCoupling;
            end
        end
        
        function out = getFields(s,fieldType)
            if (nargin==1 || isempty(fieldType))
                fieldType = []; %FieldType.Current;
            elseif nargin == 2
                disp('')
            end
          
            out = cell(s.Nvar,1);
            for n=1:s.Nvar
                out{n} = s.getField( n );
            end
        end
        
        function out = computeSquaredNorm(s,element,vector)
            disc = s.getMesh.disc;

            % Set the integration weights and Jacobian for 2D spatial planes
%             wxy  = kron( disc.wy, disc.wx );
            wxy  = element.disc.Wspatial;
            
            J    = element.J(1)*element.J(2);

            % Reset the mass for each time level
            Qt   = disc.Q(3);
            mass = zeros(Qt,1);
            
            vector = reshape(vector,disc.Q);
            
            for t=1:Qt
                xyValues = vector(:,:,t);
                mass(t) = mass(t) + xyValues(:)' * wxy * J;
            end
            
            out = max( mass.^2 );
        end
        
%         function L2Norm = getL2Norm(s,fieldType)
%             % Initialize the L2Norm array, and get the fieldType norm 
%             % for each field
%             L2Norm = zeros(s.Nvar,1);
%             for n=1:s.Nvar
%                 L2Norm(n) = s.getField(n).getL2Norm(fieldType);
%             end
%         end

        function out = isConverged( s, fieldType, iter, maxIter )
            
            if nargin == 3
                maxIter = 2; % ensure both the variable and equations norms are computed (old code)
            end
            
            % JULIAN ADDED
            % DATE: 23.01.2020 22:10
            % Checks for which type of converge is going to check
            % This determines which values are taken from the settings file
            switch fieldType
                case FieldType.Nonlinear
                    convType = s.nConvType;
                case FieldType.Coupled
                    convType = s.cConvType;
            end % switch
            
            s.converged = cell( numel(convType), 1 );
            s.norms     = cell( numel(convType), 1 );
            
            % Value convergence
            if (maxIter > 1)
                if (s.numStages>1 && fieldType==FieldType.Coupled)
                    [s.converged{1}, s.norms{1}] = s.isConvergedVariable(fieldType,iter);       % do not compute the equation residual if coupled AND multiple stages are used 
                else
                    [s.converged{1}, s.norms{1}] = s.isConvergedVariable(fieldType,iter);
                    [s.converged{2}, s.norms{2}] = s.isConvergedEquation(fieldType,iter);
                end % if
            else
                [s.converged{2}, s.norms{2}] = s.isConvergedEquation(FieldType.Current,iter);
            end % if
            
            
%                     [isConvergedValue, maxValueNorm]    = s.isConvergedValue(fieldType);
%                     isConvergedResidual = s.isConvergedResidual(fieldType);
%                     
%                     if ( ( isConvergedValue     && System.settings.comp.nonlinear.convType(1) ) || ...
%                          ( isConvergedResidual  && System.settings.comp.nonlinear.convType(2) ) )
%                         break
%                     end
        	%out = all( converged( convType ) );
            
            out = cellfun( @(x) all(x), s.converged );
            out = all( out( convType ) );
        end

        % Check if the nonlinear variables have converged
        function [isConverged,norms] = isConvergedVariable(s,fieldType,iter)
            
            % 1) Compute the L2Norm for all fields
            norms = s.computeL2Norm(fieldType);
            
            % 2) Check if converged
            isConverged = s.checkConvergence(fieldType);
            %isConverged     = L2Norm < s.getConvergence(fieldType);
        end
        
        function [ isConverged, norms, relativeRes ] = isConvergedEquation( s, fieldType, iter )
            s.computeResidual( fieldType );

            % Set 'residualOld'
            if (iter==1)
                residualL2Old = 0;
                residualNormOld = zeros( numel( s.residualNorm ), 1 );
            else
                switch fieldType
                    case FieldType.Nonlinear
                        residualL2Old = s.residualL2N{ s.activeStage };
                        residualNormOld = s.residualNormN{ s.activeStage };
                    case FieldType.Coupled
                        residualL2Old = s.residualL2C{ s.activeStage };
                        residualNormOld = s.residualNormC{ s.activeStage };
                end
            end
            
            % Compute the relative residual norm
            resImprovement = abs(s.residualL2-residualL2Old) ./ abs(s.residualL2);
            
            resImprovementEqs = zeros(numel(s.residualNorm),1);
            resDeltaEqs       = zeros(numel(s.residualNorm),1);
            
            norms = cell(numel(s.residualNorm),1);
            
            for i=1:numel(s.residualNorm)
                if (abs(s.residualNorm(i))~=0)
                    resDeltaEqs(i)       = abs(s.residualNorm(i)-residualNormOld(i));
                    resImprovementEqs(i) = abs(s.residualNorm(i)-residualNormOld(i)) ./ abs(s.residualNorm(i));
                else
                    resImprovementEqs(i) = 0;
                end
                norms{i}.deltaL2rel = resImprovementEqs(i);
                norms{i}.L2         = s.residualNorm(i);
            end
            
            % Store the current residual for future use
            switch fieldType
                case FieldType.Nonlinear
                    s.residualL2N{ s.activeStage }   = s.residualL2;
                    s.residualNormN{ s.activeStage } = s.residualNorm;
                case FieldType.Coupled
                    s.residualL2C{ s.activeStage }   = s.residualL2;
                    s.residualNormC{ s.activeStage } = s.residualNorm;
            end % switch

            %if (NMPI.instance.rank==0)
            %    fprintf('Residual improvement : %e (residual = %e - delta = %e)\n',resImprovement,s.residualL2,abs(s.residualL2-residualL2Old))
            %end

            % 2) Check if converged
            %isConvergedFlag = all( s.getL2Norm(fieldType) < s.getConvergence(fieldType) );
            %% 3) Assign the norm values for output
            %s.setNormItems(fieldType,iter)

            if s.residualL2 == 0 && residualL2Old == 0
                resImprovement = 0;
            end

            convergences = s.getConvergence(fieldType,true);

            % JULIAN MOD
            % DATE: 23.01.2020 22:25
            % Added checking of number of iteration to evade instant
            % convergence criteria on 0 initialized codes
            try
                isConverged = or( ( resImprovementEqs < convergences ), ( resDeltaEqs < convergences ) ) & iter ~= 1;
            catch
                isConverged = or( ( resImprovementEqs < convergences(1) ), ( resDeltaEqs < convergences(1) ) ) & iter ~= 1;
            end % try
            
            L2norm = resDeltaEqs;
        end % function isConvergedEquation
        
        % Compute the Norms for all fields
        function norms = computeL2Norm(s,fieldType)
            norms = cell(s.Nvar,1);
            %for n=1:s.Nvar
            for n=s.activeVariables
                norms{n} = s.fields{n}.computeL2Norm(fieldType);
                norms{n}.name = s.fields{n}.name;
            end
        end
        
        function norms = computeH1Norm(s,fieldType)
            norms = cell(s.Nvar,1);
            for n=s.activeVariables
                norms{n}.value = s.fields{n}.H1;
                norms{n}.name  = ['H1(',s.fields{n}.name,')'];
            end
        end
        
        function isConverged = checkConvergence(s,fieldType)
            % Function to verify if the norms of all fields have reached
            % their convergence already. For a single stage equation all 
            % flags needs to be true to ensure convergence. However, the 
            % last statement (with activeVariables) ensures only the 
            % activeVariables of this stage are verified for their 
            % convergence. In such cases not all flags needs to be true to
            % return isConverged = true for this function.
            
            isConverged(s.Nvar,1) = false;
            
            for n=s.activeVariables
                norms = s.fields{n}.getNorms(fieldType);
                limit = s.getConvergence(fieldType);
                limit = limit(n);
                isConverged(n) = ( norms.L2rel < limit || norms.L1 < limit );
            end
            
            isConverged = all( isConverged(s.activeVariables) );
        end
        
        function out = getMaxValues(s)
            out = [];
            
            %disp('Physics.getMaxValues')
            
            for i=1:s.Nvar
                out = [out s.getField(i).getMaxValue];
            end
        end
        
        function out = getErrors(s)
            out = [];
            for i=1:s.Nvar
                out = [out s.getField(i).getError];
            end
            %out (2) = s.fields{1}.omegaError;
        end
        
        function out = getRelativeErrors(s)
            out = [];
            for i=1:s.Nvar
                out = [out s.getField(i).getRelativeError];
            end
        end
        
        %% Plot functions
        function plotMesh(s,fig,position,title)
            if (nargin==1)
                s.mesh.plot();
            else
                s.mesh.plot(fig,0,title);
            end
        end
        
        function plotResidualNormEquation(s,n)

            if nargin==1
                n=1;
            end
            
            % Loop over the elements to plot the residual for this equation 
            for e=1:s.mesh.numElements
                element     = s.mesh.getElement(e);
                %layer = element.finite.Q(3);
                delta = element.finite.numQuadratureNodes;

                
                
                nStart = (n-1)*delta + 1;
                nEnd   = n*delta;
            
                [X,Y] = element.getNodes;
                
                element     = s.mesh.getElement(e);
                residual    = reshape(s.residual{e}(nStart:nEnd),element.finite.qSize);
                
                if (element.finite.spaceTime)
                    layer = element.finite.Q(end);
                    surf(X(:,:,layer),Y(:,:,layer),residual(:,:,layer)); % residual
                else
                    surf(X,Y,residual);                                 % residual for time stepping
                end
                hold on;
            end
            view([0,90])
            pbaspect([1 1 1])
        end
        
        function plotResidualNorm(s,layer)
            if nargin==1
                layer = s.getMesh.disc.Q(3);
            end
            
            delta = s.getMesh.disc.dofevq;
            
            nStart = 1;
            nEnd   = delta;
            
            for n=1:s.nEq
                
                % Select the active subfigure for an equation
                subplot(2,s.nEq,n);
                
                % Loop over the elements to plot the residual for this equation 
                for e=1:s.mesh.numElements
                    element     = s.mesh.getElement(e);
                    residual    = reshape(s.residual{e}(nStart:nEnd),element.disc.Q);
                    surf(element.nodes{1},element.nodes{2},residual(:,:,layer)'); % residual
                    hold on;
                end
                view([0,90])
                
                pbaspect([1 1 1])
                
                nStart = nStart + delta;
                nEnd   = nEnd   + delta;
            end
            
            % Plot the elemental residuals (integral values)
            for n=1:s.nEq
                
                % Select the active subfigure for an equation
                subplot(2,s.nEq,s.nEq+n);
                
                % Loop over the elements to plot the residual for this equation 
                for e=1:s.mesh.numElements
                    element     = s.mesh.getElement(e);
                    surf(element.xelem,element.yelem,s.residualIntegral(e,n).*ones(numel(element.xelem),numel(element.yelem))); % residual
                    hold on;
                end
                view([0,90])
                
                shading interp
                
                pbaspect([1 1 1])
            end
        end
        
        function plotError(s,fig,position)
            if nargin==2
                position = 1;
            end
            
            s.plotMode = -1;
            s.plot(fig,position);
            s.plotMode = 0;
        end
        
        function plot(s,varargin)
            % General function to plot the variables of a Physics
            clf;
            
            if (System.settings.time.spaceTime)
                timeLevels = [0,1];
            else
                timeLevels = 0;
            end
            
            % varargin = variable,derivative,time
            defaultInput = {[1:s.Nvar],0,timeLevels};
            [ variables,derivative,time ] = VerifyInput( varargin, defaultInput );
            
            % Determine the plot size
            plotNumber = 0;
            for n=variables
                for t=time
                    plotNumber = plotNumber+1;
                    subplot(numel(variables),numel(time),plotNumber);
                    s.fields{n}.plot2(derivative,t);
                end
            end
        end
    
        function plotVar(s,variable,interface,derivative)
            
            if (nargin==2)
                interface = s.interface;
                derivative = Derivative.value;
            end
            
            if (nargin==3)
                derivative = Derivative.value;
            end
            
            if (nargin==4 && isempty(interface))
                interface = s.interface;
            end
                        
            verbose         = false;
            t1              = toc;
            
            defaultProps    = {'FaceColor','interp','edgecolor','none'};
            field           = s.getField(variable,s);
            
            switch derivative
                case Derivative.value
                    value           = field.value([],[],3);   
                case Derivative.x
                    value           = field.xValue();
                case Derivative.y
                    value           = field.yValue();
            end
            
            minValue        = min( cellfun(@(x) min(x(:)),value) );
            maxValue        = max( cellfun(@(x) max(x(:)),value) );
         
            xmin = field.mesh.X(1,1);
            xmax = field.mesh.X(1,2);
            ymin = field.mesh.X(2,1);
            ymax = field.mesh.X(2,2);
            zmin = minValue;
            zmax = maxValue;
            
            %% Create meshgrid
            t1  = toc;
            X   = cell(field.mesh.numLocalElements,1);
            Y   = cell(field.mesh.numLocalElements,1);
            
            for e=1:field.mesh.numLocalElements
                element = field.mesh.getLocalElement(e);
%                 [X{e},Y{e}] = meshgrid(element.xelem,element.yelem);
                [X{e},Y{e}] = element.getNodes;
            end      
            
            % plot settings
            xlim( field.mesh.X(1,:) )
            ylim( field.mesh.X(2,:) )
            
            if (zmin==0 && zmax==0) || (zmax-zmin)<1 || zmax-zmin>max(xmax-xmin,ymax-ymin)
                pbaspect([xmax-xmin ymax-ymin 1])
            else
                pbaspect([xmax-xmin ymax-ymin zmax-zmin])
            end
            view([0,90])
            grid off;
            hold on;
                        
            for e=1:field.mesh.numLocalElements
                surface(X{e},Y{e},value{e}(:,:,end),defaultProps{:});
            end
            if interface.active
                for e=1:numel(interface.x)
                    if ~isempty(interface.x{e})
                        line(interface.x{e}, interface.y{e}, repmat(maxValue,size(interface.x{e})), interface.props{:});
                    end
                end
            end
            
            if (verbose)
                fprintf('Total time %s   : %f\n',field.name,toc-t1)
            end
        end
        
        function plotVarHalf(s,variable,interface)
            
            if (nargin==2)
                interface = s.interface;
            end
            
            verbose         = false;
            t1              = toc;
            
            defaultProps    = {'FaceColor','interp','edgecolor','none'};
            field           = s.getField(variable,s);
            value           = field.value([],[],3);   
            minValue        = min( cellfun(@(x) min(x(:)),value) );
            maxValue        = max( cellfun(@(x) max(x(:)),value) );
         
            xmin = field.mesh.x0;
            xmax = field.mesh.x1;
            ymin = field.mesh.y0;
            ymax = field.mesh.y1;
            zmin = minValue;
            zmax = maxValue;
            
            %% Create meshgrid
            t1  = toc;
            X   = cell(field.mesh.numLocalElements,1);
            Y   = cell(field.mesh.numLocalElements,1);
            
            for e=1:field.mesh.numLocalElements
                element = field.mesh.getLocalElement(e);
                [X{e},Y{e}] = meshgrid(element.xelem,element.yelem);
            end      
            
            % plot settings
            xlim([-xmax xmax])
            ylim([ymin ymax])
            if (zmin==0 && zmax==0) || (zmax-zmin)<1 || zmax-zmin>max(xmax-xmin,ymax-ymin)
                pbaspect([2*(xmax-xmin) (ymax-ymin) 1])
            else
                pbaspect([2*(xmax-xmin) (ymax-ymin) zmax-zmin])
            end
            view([0,90])
            grid off;
            hold on;
            
            % First half
            for e=1:field.mesh.numLocalElements
                surface(X{e},Y{e},value{e}(:,:,end)',defaultProps{:});
            end
            if interface.active
                for e=1:numel(interface.x)
                    if ~isempty(interface.x{e})
                        line(interface.x{e}, interface.y{e}, repmat(maxValue,size(interface.x{e})), interface.props{:});
                    end
                end
            end
            
            % Second half
            for e=1:field.mesh.numLocalElements
                surface(-X{e},Y{e},value{e}(:,:,end)',defaultProps{:});
            end
            if interface.active
                for e=1:numel(interface.x)
                    if ~isempty(interface.x{e})
                        line(-interface.x{e}, interface.y{e}, repmat(maxValue,size(interface.x{e})), interface.props{:});
                    end
                end
            end
            
            if (verbose)
                fprintf('Total time %s   : %f\n',field.name,toc-t1)
            end
        end
        
        function plotVariable(s,fig,position,variable,titleString,layer)
            if (nargin<6)
                layer = 1;
            end
            
            if (position<=1)
                
                if (position==1)
                    clf(fig)
                end
                screensize = get( groot, 'Screensize' );
                sizex = 1000;
                sizey = 1000;
                
%                 if (s.problemCase==ProblemCase.LidDrivenCavity)
%                     sizex = 1200;
%                     sizey = 1000;   
%                 end
                
                set(fig,'position',[0.5*(screensize(3)-sizex) 0.5*(screensize(4)-sizey) sizex sizey]); 
            end
            
            field = s.getField(variable);
            data = field.current;
            
%             data = s.getValues(variable);
%             data = s.c;
             
            if (position>0)
                subplot(2,2,position);
            end

            dir = 0;

            for e=1:s.mesh.getNumLocalElements
                if (size(data{e},2)==1)
                    data{e} = reshape(data{e},s.mesh.getElement(e).disc.Q);
        %             c{e}(1:end-1:end,1:end-1:end,1)
                    [X,Y] = meshgrid( s.mesh.getElement(e).xelem,s.mesh.getElement(e).yelem );
                    if (layer==1)
                        surf(X,Y,data{e}(:,:,s.mesh.getElement(e).disc.Qz)');
                    elseif (layer==0)
                        surf(X,Y,data{e}(:,:,1)');
                    end
                else
                    [X,Y] = meshgrid( s.mesh.getElement(e).xelem,s.mesh.getElement(e).yelem );
        %             surf(X,Y,c{e}(1:5,1:5,5)');
%                     surf(X,Y,data(1:size(data,1),1:size(data,2),1)');
                    if (layer==1)
                        surf(X,Y,data{e}(:,:,s.mesh.getElement(e).disc.Qz)');
                    elseif (layer==0)
                        surf(X,Y,data{e}(:,:,1)');
                    end
        %             surf(mesh.xgbl{e}(1:end-1:end),mesh.ygbl{e}(1:end-1:end),c{e}(1:end-1:end,1:end-1:end,1));
                end

            %             surf(mesh.xgbl{e},mesh.ygbl{e},c{e}(:,:,1));
            %             surf(mesh.xgbl{e}(1:end-1:end),mesh.ygbl{e}(1:end-1:end),c{e}(1:end-1:end,1:end-1:end,1));

                hold on;
            end
            
            colorbar;
%             if (s.problemCase==ProblemCase.LidDrivenCavity)
%                 view([0,90]) 
%             else
                view([25,25]) 
%             end
            
%             for e=1:mesh.getNumElements
%             %             surf(mesh.xgbl{e},mesh.ygbl{e},c{e}(:,:,1));
%             %             surf(mesh.xgbl{e}(1:end-1:end),mesh.ygbl{e}(1:end-1:end),c{e}(1:end-1:end,1:end-1:end,1));
%         %         surf(mesh.xgbl{e}(1:end-1:end),mesh.ygbl{e}(1:end-1:end),0*c{e}(1:end-1:end,1:end-1:end,1));
%                 plotPlane(e,dir,mesh)
%                 hold on;
%             end
            title(titleString)
%             colorbar off
%             hold off
%             if (dir==3)
%                 view([0,90])
%                 pbaspect([(mesh.x1-mesh.x0) (mesh.y1-mesh.y0) 1])
%             elseif (dir==2)
%                 view([90,0])
%                 pbaspect([1 (mesh.y1-mesh.y0) 100*(mesh.t1-mesh.t0)])
%             elseif (dir==1)
%                 view([0,0])
%                 pbaspect([(mesh.x1-mesh.x0) 1 100*(mesh.t1-mesh.t0)])
%             elseif (dir==0)
%                 view([0,90])
%                 pbaspect([(mesh.x1-mesh.x0) (mesh.y1-mesh.y0) 10*(mesh.t1-mesh.t0)])
%             end

%             xlabel('x') ;
%             ylabel('y') ;
%             zlabel('t') ;   
        end
        
        function plotStreamline(s,fig,position,variable,variable2,title)
            if (position==1)
                clf(fig)
                screensize = get( groot, 'Screensize' );
                sizex = 1000;
                sizey = 1000;
                
%                 if (s.problemCase==ProblemCase.LidDrivenCavity)
%                     sizex = 1200;
%                     sizey = 1000;   
%                 end
                
                set(fig,'position',[0.5*(screensize(3)-sizex) 0.5*(screensize(4)-sizey) sizex sizey]); 
            end

            if (variable==4)
                variable = 3;
            end
            
            data = s.getValues(variable);
            data2 = s.getValues(variable2);
            
            subplot(2,2,position);

            for e=1:s.mesh.getNumElements
                if (size(data{e},2)==1)
                    data{e} = reshape(data{e},s.mesh.getElement(e).disc.Q);
        %             c{e}(1:end-1:end,1:end-1:end,1)
                    [X,Y] = meshgrid( s.mesh.getElement(e).xelem,s.mesh.getElement(e).yelem );
                    data2{e} = reshape(data2{e},s.mesh.getElement(e).disc.Q);
                    [sx,sy] = meshgrid(0:0.1:1,0:0.1:1);
                    u = data{e}(:,:,end)';
                    v = data2{e}(:,:,end)';
                    streamData = stream2(X,Y,u,v,sx,sy);
                    streamline( streamData );
%                     test = streamslice(X,Y,u,v);
                else
                    error('Unsupported call in Physics::plotStreamline')
                    %[X,Y] = meshgrid(s.mesh.xgbl{e},s.mesh.ygbl{e});
                    %surf(X,Y,c{e}(1:5,1:5,5)');
                    %surf(X,Y,c{e}(1:size(c{e},1),1:size(c{e},2),1)');
                    %surf(mesh.xgbl{e}(1:end-1:end),mesh.ygbl{e}(1:end-1:end),c{e}(1:end-1:end,1:end-1:end,1));
                end

            %             surf(mesh.xgbl{e},mesh.ygbl{e},c{e}(:,:,1));
            %             surf(mesh.xgbl{e}(1:end-1:end),mesh.ygbl{e}(1:end-1:end),c{e}(1:end-1:end,1:end-1:end,1));

                hold on;
            end
            
            colorbar;
%             if (s.problemCase==ProblemCase.LidDrivenCavity)
%                 view([0,90]) 
%             else
                view([25,25]) 
%             end  
        end
        
        function outputTimings(s)
            nTimings = length(s.timings);
            
            if (System.settings.anal.debug)
                fprintf('Before NMPI.Allreduce in Physics.outputTimings\n')
            end
            
            tAll = NMPI.Allreduce(s.timings,nTimings,'+','Physics.outputTimings') ;
            if (NMPI.instance.rank==0)
                fprintf('Total time : %f\n',tAll(1))
            end 
        end
        
        % Compute the residual for the passed 'fieldType'. If none is
        % provided, the 'current' field will be used.
        function computeResidual(s,fieldType)
            if (nargin==1)
                fieldType = FieldType.Current;
            end
            
            if (System.settings.anal.debug)
                %save(['computeResidual.' int2str(System.rank) '.mat']);
            end
            
            % Get the cell array with alpha for each element
            alphaElements = s.getElementalAlpha(fieldType);

            % Compute the squared residualNorm for each element
            norm = 0;
            
            if (isempty(s.nEq))
                s.nEq = s.Nvar;
            end
            
            s.residualNorm = zeros(s.nEq+1,1);
            
            s.residualIntegral = zeros(s.getMesh.numElements,s.nEq); % NB: global size
            
            if (isempty(s.opL{1})) % || s.getMesh.isChanged ) %numel(s.opL)~=s.getMesh.numLocalElements )
                if (System.settings.anal.debug)
                    fprintf('Setting up equations (Physics.computeResidual) on rank %d\n',System.rank)
                end
                if System.rank==0
                    fprintf('Setting up equations for Physics.computeResidual; is this really necessary?\n')
                end
                s.setupEquations(); %s.allPhysics);                
            end
            
            s.residual = cell( numel(s.mesh.eRange), 1);
            
            numDof = s.residualField{1}.mesh.getDoFHandler(1).numDof;
            
            if (s.globalL)
                alpha = s.getAlpha();
                residual = s.f - s.A * alpha;
                
                for i=1:s.nEq
                    s.residualField{i}.setAlphaArray( residual( numDof*(i-1) + [1:numDof] ) );
                end
                
                norm = 0;
                
%                 dofs = s.mesh.getDoFHandler(1).dofs;
%                 
%                 for e=s.mesh.eRange
%                     element = s.mesh.getElement(e);
%                     el = element.localID;
% 
%     %                 if (isempty(s.opL{el}) || s.getMesh.isChanged ) %numel(s.opL)~=s.getMesh.numLocalElements )
%     %                     if (System.settings.anal.debug)
%     %                         fprintf('Setting up equations (Physics.computeResidual) on rank %d\n',System.rank)
%     %                     end
%     %                     s.setupEquations(s.allPhysics);
%     %                 end
% 
%                     %Ze = kron(eye(s.Nvar),s.mesh.Z{e});
%                     % NB: Z is already included in opL !
% 
%                     try
%                         s.residual{el} = residual( dofs{e}(:) );
%                     catch
%                         disp('error in physics residual')
%                     end
% 
%                     norm = 0; %norm + s.residual{el}' * (s.W{el} .* s.residual{el});
% 
%                     for i=1:s.nEq
%                         selection = (i-1)*s.mesh.getLocalElement(el).finite.numQuadratureNodes+1:i*s.mesh.getLocalElement(el).finite.numQuadratureNodes;
%                         s.residualField{i}.setValue(el,s.residual{el}(selection)./(s.activeEqWeights(i))^2);
% 
%                         s.residualField{i}.alpha{ el } = s.residual{ el };
%                         
%                         elementalResidual = s.residual{el}(selection)' * (s.W{el}(selection) .* s.residual{el}(selection));
% 
%                         s.residualNorm(i) = s.residualNorm(i) + elementalResidual;
% 
%                         s.residualIntegral(e,i) = elementalResidual;
%                     end
%                 end
                
            else
                for e=s.mesh.eRange

                    element = s.mesh.getElement(e);
                    el = element.localID;

    %                 if (isempty(s.opL{el}) || s.getMesh.isChanged ) %numel(s.opL)~=s.getMesh.numLocalElements )
    %                     if (System.settings.anal.debug)
    %                         fprintf('Setting up equations (Physics.computeResidual) on rank %d\n',System.rank)
    %                     end
    %                     s.setupEquations(s.allPhysics);
    %                 end

                    %Ze = kron(eye(s.Nvar),s.mesh.Z{e});
                    % NB: Z is already included in opL !

                    try
                        s.residual{el} = ( s.opG{el} - s.opL{el} * alphaElements{el} ); %./ s.opG{el};
                    catch
                        disp('error in physics residual')
                    end

                    norm = norm + s.residual{el}' * (s.W{el} .* s.residual{el});

                    for i=1:s.nEq
                        selection = (i-1)*s.mesh.getLocalElement(el).finite.numQuadratureNodes+1:i*s.mesh.getLocalElement(el).finite.numQuadratureNodes;
                        s.residualField{i}.setValue(el,s.residual{el}(selection)./(s.activeEqWeights(i))^2);

                        elementalResidual = s.residual{el}(selection)' * (s.W{el}(selection) .* s.residual{el}(selection));

                        s.residualNorm(i) = s.residualNorm(i) + elementalResidual;

                        s.residualIntegral(e,i) = elementalResidual;
                    end
                end
            end
            
%             for i=1:s.nEq
%                 s.residualField{i}.computeAlpha;
%             end
            
            for i=1:s.nEq
                s.residualNorm(i) = s.residualNorm(i) / (s.activeEqWeights(i))^2;
            end
            
            s.residualNorm(s.nEq+1) = sum( s.residualNorm(1:s.nEq) );
            
            if (System.settings.anal.debug)
                fprintf('Before NMPI.Allreduce in Physics.computeResidual on rank %d\n',System.rank)                
                %save(['computeResidual.' int2str(System.rank) '.mat']);
            end
            
            % Parallel sum of all the norms
            if (numel(norm)==1 && ~isempty(norm))
                norm                = NMPI.Allreduce(norm,1,'+','Physics.computeResidual.norm') ;
                s.residualNorm      = NMPI.Allreduce(s.residualNorm,numel(s.residualNorm),'+','Physics.computeResidual.residualNorm');
                s.residualIntegral  = NMPI.Allreduce(s.residualIntegral,numel(s.residualIntegral),'+','Physics.computeResidual.residualIntegral');
            else
                if isempty(norm)
                    error('LSQDIM:Norm','There is no norm, it is an empty variable');
                else
                    error('LSQDIM:Norm','Wrong number of elements in norm (%d elements where 1 is expected)',numel(maxValue));
                end
            end
            
            s.residualIntegral = reshape(s.residualIntegral,[s.getMesh.numElements,s.nEq]);
            
            if (System.settings.anal.debug)
                fprintf('After NMPI.Allreduce in Physics.computeResidual on rank %d\n',System.rank)
            end
            
            % Finally save the norm
            s.residualL2 = sqrt(norm);
            for i=1:numel(s.residualNorm)
                s.residualNorm(i) = sqrt(s.residualNorm(i));
            end
%             
%             if System.rank==0
%                 for i=1:numel(s.residualNorm)-1
%                     fprintf('%d : %f  ',i,s.residualNorm(i))
%                 end
%                 fprintf('total : %f\n',s.residualNorm(end))
%             end

%             % Find the values and positions of the
%             % 'nResidualRefine' largest residuals
%             nResidualRefine = ceil(0.1*s.getMesh.numElements);
%             [B,I] = maxk(s.residualIntegral,nResidualRefine);
%             
%             s.residualRefine = false( s.getMesh.numElements,1 );
%             s.residualRefine(I(:)) = true;
%             
%             nResidualCoarsen = ceil(0.1*s.getMesh.numElements);
%             [B,I] = mink(s.residualIntegral,nResidualCoarsen);
%             s.residualCoarsen = false( s.getMesh.numElements,1 );
%             s.residualCoarsen(I(:)) = true;
        end
        
        % This is an expensive function!
        function updateResidualField(s)
            
            disp('WARNING : the function Physics.updateResidualField is not very efficient')
            
            % Loop over the variables
            for n=1:s.Nvar
                tmpField = s.residualField{n};            
                for e=1:s.mesh.getNumLocalElements
                    tmpField.value{e} = s.residual{e}( (n-1)*s.getMesh.disc.dofevq+1 : n*s.getMesh.disc.dofevq );
                end
            end
        end
        
        function out = getConvergence(s,fieldType,residual)
            if nargin==2
                switch fieldType
                    case FieldType.Nonlinear
                        out = reshape( s.nConvergence, [numel(s.nConvergence),1] );
                    case FieldType.Coupled
                        out = reshape( s.cConvergence, [numel(s.cConvergence),1] );
                end
            elseif (residual)
                out = reshape( s.rConvergence, [numel(s.rConvergence),1] );
            else
                error('Physics.getConvergence :: unsupported request')
            end
        end
        
        function out = getMesh(s)
            out = s.mesh;
        end
        
        function setExactSolution(s,activate)
            activeSettings = System.settings;
            for n=1:s.Nvar
                fh = @(x,y,z,t) activeSettings.analyticalSolution(s,n,x,y,z,t);
                field = s.getField(n);
                field.addSubField(FieldType.Exact,true,[],[],fh);
                
                if (~System.settings.comp.manufacturedSolution)
                    % If no manufactured solution is used, the fields have
                    % to be computed
                    % MMKK, 31/05/2019 : is this really necessary?
                    s.setupValueEquation( fh );
                    s.resetBoundaryConditions();
                    activeSettings.setBoundaryConditions( s );
                    s.solve( activeSettings.comp.CG.method ,n);
                end
                
                if (~activate)
                    field.setActive(FieldType.Current);
                end
            end
        end

        function computeForcing(s)
            % Function used to compute the forcing required to solve the
            % problem with a manufactured solution. To obtain the
            % prescribed solution a forcing term has to be added to the rhs
            % of the equations. 
            
            if (s.manufacturedSolution)
                % Temporary disable the forcing in the system of equations
                s.withForcing = false;
                
                % Ensure the 'exact' field is 'true' for the physics under
                % consideration, if not throw an error
                if (~s.exact)
                    error('Please enable "exact" in settings')
                end
                
                % Assign the exact solution as the activeField
                backupActive = s.selectActiveField( FieldType.Exact );
                
                %s.steady = true;
                
                if (s.numStages==1)
                    % Setup the system of equations
                    s.setupEquations();

                    %s.steady = false;

                    % Compute the residual based wrt the exact solution
                    s.computeResidual(FieldType.Exact);

                    % Enable the forcing again, and assign the residual as forcing 
                    % term to ensure the manufactured solution will be obtained
                    s.withForcing = true;
                    s.forcing = s.residual;

                    s.residualField{1}.reset;
                    
                elseif (s.numStages>1)
                    s.stageForcing = cell(s.numStages,1);
                    
                    activeSettings = System.settings;
                    tmpWeight = activeSettings.comp.boundary.eqWeight;
                    activeSettings.setValue('comp.boundary.eqWeight',1);
                    
                    % Loop over the stages
                    for stage=1:s.numStages
                        s.activeStage = stage;
                        
                        % Setup the system of equations
                        s.setupEquations();

                        %s.steady = false;

                        % Compute the residual based wrt the exact solution
                        s.computeResidual(FieldType.Exact);

                        % Enable the forcing again, and assign the residual as forcing 
                        % term to ensure the manufactured solution will be obtained
                        s.stageForcing{stage} = s.residual;

                        s.residualField{1}.reset;
                    end
                    s.withForcing = true;           % Reenable forcing
                    
                    activeSettings.setValue('comp.boundary.eqWeight',tmpWeight);
                else
                    error('Unsupported number of stages in Physics.m\n')
                end
                
                backupActive = s.selectActiveField( backupActive ); %#ok<NASGU>
                
            elseif (System.settings.comp.exactRHS)
                % Enable the forcing again, and assign the residual as forcing 
                % term to ensure the manufactured solution will be obtained
                s.withForcing = true;
                
                for e=s.mesh.eRange
                    element = s.mesh.getElement(e);         % Select the active element                    
                    el = element.localID;                   % Get the local element number

                    [X,Y,Z,T] = element.getNodes();
                    
                    s.forcing{el} = System.settings.phys.forcingFunction(X,Y,Z,T);
                end
                
            else
                % Ensure no forcing is applied if no manufactured solution
                % is being simulated for testing purposes
                s.forcing     = 0;
                s.withForcing = false;
            end
            
            if (System.rank==0 && System.settings.outp.debug)
                fprintf('PHYSICS    : Forcing has been computed\n')
            end
        end
        
        function backup = selectActiveField(s,fieldType)
            for n=1:s.Nvar
                % Select the main field
                selectedField = s.fields{n};
                
                % For the first variable store the currently active field
                % as backup, which is returned by this function. By doing
                % this, it is possible to set the same field back after
                % doing some operations on another field
                if (n==1)
                    backup = selectedField.active.type;
                end
                
                % Activate the requested SubField
                selectedField.setActive(fieldType);
            end
        end

        function out = getElementalAlpha(s,fieldType)
            out = cell(s.mesh.numLocalElements,1);
            
            if ( isempty(s.mesh.numLocalElements) || s.mesh.numLocalElements ~= numel(s.mesh.eRange))
                physics = cell(PhysicsCollection.size,1);
                for i=1:numel(physics)
                    physics{i} = PhysicsCollection.get(i);
                end
                filename = ['physics_getElementalAlpha1.' int2str(System.rank) '.mat'];
                save(filename);
                error('The number of localElements and the size of eRange are not equal!\n')
            end
            
            try
                for n=s.activeVariables
                    % Select the main field
                    selectedField = s.fields{n};

                    backupActive = selectedField.active.type;

                    % Activate the requested SubField
                    selectedField.setActive(fieldType);

                    id = 0;
                    for e=s.mesh.eRange
                        id = id+1;
                        tmp = selectedField.alp(fieldType,e);
                        out{id} = [ out{id}; tmp(:) ];
                    end

                    selectedField.setActive(backupActive);
                end
            
            catch MExp
            	physics = cell(PhysicsCollection.size,1);
                for i=1:numel(physics)
                    physics{i} = PhysicsCollection.get(i);
                end
                filename = ['physics_getElementalAlpha2.' int2str(System.rank) '.mat'];
                save(filename);
                error('The number of localElements and the size of eRange are not equal!\n')
            end
        end
        
        function out = getElementalAlphaFree(s)
            out = cell(s.mesh.numLocalElements,1);
            
            for n=1:s.Nvar
                % Select the main field
                selectedField = s.fields{n};
                
                % Activate the requested SubField
                selectedField.setActive(fieldType);
                
                for e=1:numel(out)
                    out{e} = [ out{e}; selectedField.alpha{e}( ~selectedField.prescribed{e} ) ];
                end
            end
        end

        function out = getAlpha(s,var)
            if (nargin==1)
                var = 0;
            end
            
            if (var==0)
%                 out = [];
%                 for n=1:s.Nvar
%                     out = [out; s.fields{n}.getAlphaArray ];      % growing out
%                 end
                
                alphaCellArray  = cell(s.Nvar,1);
                alphaSize       = 0;
                count           = 0;
                for n=s.activeVariables
                    alphaCellArray{ n } = s.fields{n}.getAlphaArray;
                    alphaSize = alphaSize + numel( alphaCellArray{ n } );
                end
                
                out = zeros( alphaSize,1 );     % allocate out
                
                for n=s.activeVariables
                    alphaSize = numel( alphaCellArray{ n } );
                    out( count+1 : count+alphaSize ) = alphaCellArray{ n };
                    count = count + alphaSize;
                end
            elseif (numel(var)==1)
                out = s.fields{var}.getAlphaArray;
            else
                nVar = numel(var);
                alphaCellArray  = cell(nVar,1);
                alphaSize       = 0;
                count           = 0;
                for n=var
                    alphaCellArray{ n } = s.fields{n}.getAlphaArray;
                    alphaSize = alphaSize + numel( alphaCellArray{ n } );
                end
                
                out = zeros( alphaSize,1 );     % allocate out
                
                for n=var
                    alphaSize = numel( alphaCellArray{ n } );
                    out( count+1 : count+alphaSize ) = alphaCellArray{ n };
                    count = count + alphaSize;
                end
            end
        end
        
        function setAlpha(s,alpha,var)
            if (nargin==2)
                var = 0;
            end
            
            if (var==0)
                
                endIndex   = 0;
                
                for n=1:s.Nvar
                    dofHandler = s.fields{n}.getDoFHandler;
                    startIndex = endIndex + 1;
                    endIndex   = endIndex + dofHandler.numLocalDof;
                    s.fields{n}.setAlphaArray( alpha(startIndex:endIndex) );
                end
            elseif (numel(var)==1)
                s.fields{var}.setAlphaArray(alpha);
            else
                endIndex   = 0;                
                for n=var
                    dofHandler = s.fields{n}.getDoFHandler;
                    startIndex = endIndex + 1;
                    endIndex   = endIndex + dofHandler.numLocalDof;
                    s.fields{n}.setAlphaArray( alpha(startIndex:endIndex) );
                end
            end
        end 
        
        function write_vtu(s)
            % Function to write a fieldGroup to Paraview vtu format
            if (~isempty(s.fieldGroup))
                write_vtu( s.fieldGroup, System.settings.time.iter, System.settings.time.current, System.rank, System.nproc );
            end
        end

        function writeError(s)
            if (System.rank==0)
                currentStep = round( System.settings.time.current / System.settings.time.step );
                filename    = [System.settings.outp.directory s.shortName '_error.txt'];
                
                if (currentStep==1 || exist(filename,'file')~=2)
                    header = {'iter';'time'};
                    header = [header; s.getVariableNames('','Error')];
                    header = [header; s.getVariableNames('','RelError')];
                    System.writeHeader(filename,header);
                end
                
                values = [currentStep, System.settings.time.current];
                
                errors    = s.getErrors;                                    % For spaceTime there are multiple errors levels
                relErrors = s.getRelativeErrors;                            % For spaceTime there are multiple errors levels
                
                values = [values, errors(end,:), relErrors(end,:)];         % But only the last one is added to the file
                System.appendToFile(filename,values');
            end
        end
        
        function setupValueEquation(s,value)
            for e=s.mesh.eRange
                % Select the active element
                element = s.mesh.getElement(e);
                
                % Get the local element number
                el = element.localID;
                
                % Get all elemental operators/arrays
                H       = element.getH;
                Wel     = element.getW;
                vecZ    = element.getVecZ;

                [Xc,Yc,Zc,Tc] = element.getNodes();
                Xc = squeeze(Xc); Yc = squeeze(Yc);
                Zc = squeeze(Zc); Tc = squeeze(Tc);
                
                if ( isa(value,'function_handle') )
                    b = value(Xc,Yc,Zc,Tc);
                elseif ( numel(value)==numel(vecZ) )
                    b = value;
                else
                    b = vecZ;
                end
                
                s.opL{el} = H;                    
                s.opG{el} = b(:);
                
                Ze = s.mesh.Z{e};
                         
                s.opL{el}	= s.opL{el} * Ze;
                s.W{el}   	= Wel;
                s.sqrtW{el}	= sqrt(Wel);
            end
        end
        
        % Dummies for functions than can be overload 
        function preTimeLoop(s) %#ok<MANU>
            % no update by default, but you can overload this function in a 
            % derived Physics if required
        end
        
        function preTimeStep(s) %#ok<MANU>
            % no update by default, but you can overload this function in a 
            % derived Physics if required
        end
        
        function writeValues(s) %#ok<MANU>
            % no update by default, but you can overload this function in a 
            % derived Physics if required
        end
        
        function out = save(s)
            % Merge solution from all processors on the master rank
            % Initialize the default output from the settings of the current object            
            physics  = struct;
            
            % First synchronize the fields in parallel
            physics.fields = cell(s.Nvar,1);
            for n=1:s.Nvar
                physics.fields{n} = save( s.fields{n} );
            end
            
            if (NMPI.instance.rank==0 || System.settings.outp.parallelWrite )
                physics.class        = class(s);
                physics.name         = s.name;
                physics.mesh         = s.mesh.save;
                physics.settings     = s.settings;
            end
            
            out = physics;
        end
    end
    
    methods (Access=protected)
        
        function out = getOpL(s,e,equation,variable)
            eqRange     = size(s.opL{e},1) / s.nEq;
            varRange    = size(s.opL{e},2) / s.Nvar;
            out = s.opL{e}( (equation-1)*eqRange +1:equation*eqRange , ...
                            (variable-1)*varRange+1:variable*varRange );
        end
        
        function out = getOpG(s,e,equation)
            eqRange     = size(s.opL{e},1) / s.nEq;
            out = s.opG{e}( (equation-1)*eqRange +1:equation*eqRange , 1 );
        end 

        function potentialCoarsening = checkCoarsening(s,maxLevel)
            % Check which elements need refinement. This is determined by
            % checking if the element needs refinement based on the provided
            % refinementCriteria, and if the level of refinement is equal or
            % lower than the maxLevel.
            potentialCoarsening = true(s.mesh.getNumLocalElements,1);

            activeSettings = System.settings;
            
            for e=1:s.mesh.getNumLocalElements    
                refinementCriteria = activeSettings.getRefinementCriteria(s,e);                
                element = s.mesh.octTree.getElement(e);
                
                if (any(refinementCriteria) && element.getLv<=maxLevel || element.getLv==0 || element.needsRefinement)
                    potentialCoarsening(e) = false;
                end
            end
            
            % Convert from local to global and sync between processors
            potentialCoarseningAll = true(s.mesh.getNumGlobalElements,1);
            potentialCoarseningAll(s.mesh.getElementRank) = potentialCoarsening;            
            potentialCoarsening = NMPI.AllreduceLogical(potentialCoarseningAll,s.mesh.getNumGlobalElements,'|','Physics.checkCoarsening');
        end  

        function setFields(s,fields)
            if ( numel(s.fields)~=numel(fields) )
                error('The number of fields of this physics (%d) does not match the number of fields provided (%d)\n',numel(s.fields),numel(fields));
            end
            
            s.fieldGroup = FieldGroup(s.Nvar,s.shortName,s.longName);
            
            for i=1:numel(fields)
                s.fields{i} = fields{i};
                s.fieldGroup.add(s.fields{i},0,s.variables{i},i);              % 0 means scalar field
            end
            
            %FieldCollection.add( s.fields{n} );
        end
    end
    
    methods (Static)
        function out = load(data,mesh)
            if (~isstruct(data))
                error('%s :: No valid data provided\n',data.class)
            end
            
            % Create the mesh if none supplied
            if (nargin==1)
                fh = str2func([data.mesh.class '.load']);
                mesh = fh(data.mesh);
                %mesh = OctreeMesh();
            end
            
            % Create the settings, and add the current fields. These fields
            % will be removed from the settings in physics after reloading
            % their data.
            settings = data.settings;
            settings.fields = data.fields;
            
            % Create the physics
            fh = str2func(data.class);
            out = fh( mesh, settings );
        end
    end
    
%     methods (Abstract)
%         % These functions need to be implemented in a physics derived class
%         result = setupEquations(s);
%     end
end

