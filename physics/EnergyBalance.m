classdef EnergyBalance
    %ENERGYBALANCE Used to compute the energy balance of the system
    %   From the Physics cell array, all contributions to the total energy
    %   are computed within this class. 
    
    properties
        instance;
        
        energyTable;
        
        kinetic;
        gravitational;
        surface;
        pressure;
        dissipation;
        total;
    end
    
    methods
        function s = EnergyBalance(table)
            %EnergyBalance Constructor
            %   If no table is provided, a new instance is constructed. If 
            %   a table is provided, the energyBalance will start from
            %   there and add new entries at the bottom of this table
            if nargin==1 && istable(table)
                s.energyTable = table;
            elseif nargin==1
                error('Unexpected behavior: the passed table is not a table')
            else
                obj.Property1 = inputArg1 + inputArg2;
            end
        end
        
        function outputArg = method1(obj,inputArg)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            outputArg = obj.Property1 + inputArg;
        end
        
        % The pressure energy is defined by the integration of the pressure
        % over the full domain. Initially the pressure is zero everywhere,
        % so any change from zero will contribute to the potential.
        function [out1] = computePressureEnergy(s)

            % Set the Fields
            C   = s.CH.getField(1,s.CH);
            P   = s.NS.getField(3,s.NS);
            
            % Get the mesh (should be same for both fields!)
            mesh = P.mesh;
            
            numElements = mesh.numLocalElements;
           
            % Initialize the pressure for all elements
            pressure = cell(numElements,1);
             
            % After loading a solution the field also use their own meshes
            % for coupled field. By selecting the coupled field (and
            % passing the coupled mesh), it is ensured that both fields
            % provide the same elements.
            C.selectCoupled(mesh);
            
            for el=1:numElements
                e = mesh.getElement(el).globalID;
                
                Pk          = P.val(e);
                Ck          = C.val(e);
                
                pressure{e} = (Pk); %.* (1-Ck);
            end

            totalPressureEnergy = 0;
            
            for e=1:numElements
                data1 = pressure{e};
                
                element = mesh.getLocalElement(e);
                wxy = kron(element.disc.wy,element.disc.wx);
                totalPressureEnergy = totalPressureEnergy + data1((element.disc.Qz-1)*(element.disc.Qx*element.disc.Qy)+1:element.disc.Qz*(element.disc.Qx*element.disc.Qy))' * wxy / (element.J(1)*element.J(2)) ;
            end

            if (System.settings.anal.debug)
                fprintf('Before NMPI_Allreduce in Solution.computePressureEnergy\n')
            end
            
            totalPressureEnergy = NMPI_Allreduce(totalPressureEnergy,1,'+',Parallel3D.MPI.mpi_comm_world);
%             if (Parallel3D.MPI.rank==0)
%                 fprintf('Total kinetic energy  : %f\n',totalPressureEnergy)
%             end
            
            out1 = totalPressureEnergy;
        end
    end
end

