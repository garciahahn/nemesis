classdef FluidBox < NavierStokes
    %BOX 2D Steady Navier-Stokes benchmark
    %   Detailed explanation goes here
    
    properties
        % none
    end
    
    methods
        function s = FluidBox(mesh,Re,We,Fr,g,lamRho,lamMu,maxNIter) 
            if (nargin==0)
                return;
            end
            
            s.name = 'FluidBox';
            s.isSteady = false;
            s.initialize(mesh,Re,We,Fr,g,lamRho,lamMu,maxNIter);
        end
        
        function initial(s)
            % do nothing
        end
        
        function applyConditions(s,bcType)
            %s.applyInitialConditions(bcType);
            s.applyBoundaryConditions(bcType);
        end
        
        function applyInitialConditions(s,bcType)
%             bcType = BCType.Strong;
            applyInitialConditions@NavierStokes(s,bcType);
            %fprintf('%s initial conditions applied\n',s.name)
        end
        
        % Boundary conditions:
        %
        % Dirichlet on all walls, where the solution is obtained from the
        % analytical solution
        %
        function applyBoundaryConditions(s,bcType)
             
            % Velocity u: 
            s.applyNeumannField( Neighbor.Left    , s.uField,  0, bcType );     % fixedValue: u=0 (free slip)
            s.applyDirichletField( Neighbor.Right   , s.uField,  0, bcType );     % fixedValue: u=0 (free slip)
            s.applyDirichletField( Neighbor.Bottom  , s.uField,  0, bcType );     % fixedValue: u=0 (no slip)
            s.applyDirichletField( Neighbor.Top     , s.uField,  0, bcType );     % zeroGradient: u=0 (no slip)

            % Velocity v:
%             s.applyNeumannField(   Neighbor.Left    , s.vField,  0, bcType );     % zeroGradient: dv/dx=0 (free slip)
%             s.applyNeumannField(   Neighbor.Right   , s.vField,  0, bcType );     % zeroGradient: dv/dx=0 (free slip)
            s.applyDirichletField(   Neighbor.Left    , s.vField,  0, bcType );     % zeroGradient: dv/dx=0 (free slip)
            s.applyDirichletField(   Neighbor.Right   , s.vField,  0, bcType );     % zeroGradient: dv/dx=0 (free slip)
            s.applyDirichletField( Neighbor.Bottom  , s.vField,  0, bcType );     % fixedValue: v=0 (no slip)
            s.applyNeumannField( Neighbor.Top     , s.vField,  0, bcType );     % fixedValue: v=0 (no slip)

            % Pressure: p=0 on left/right (in-/outflow), dp/dn=0 on bottom/top
            s.applyDirichletField( Neighbor.Left    , s.pField,  0, bcType );     % fixedValue: uniform 0
            s.applyDirichletField( Neighbor.Right   , s.pField,  0, bcType );     % fixedValue: uniform 0
            s.applyNeumannField(   Neighbor.Bottom  , s.pField,  0, bcType );     % zeroGradient: dp/dy=0
            s.applyNeumannField(   Neighbor.Top     , s.pField,  0, bcType );     % zeroGradient: dp/dy=0

%             s.applyDirichletLine( Neighbor.Left, Neighbor.Top, s.pField,  0, BCType.Strong );     % fixedValue: p=0
            
%             fprintf('%s boundary conditions applied\n',s.name)
        end
        
%         function criteria = getRefinementCriteria(s,e)
% %             criteria(1) = (s.residualNorm(e)>50);                            % Refinement based on residual 
%             criteria = false;
%         end
        
        function plot(s,fig,position)
%             u = s.getValues(Variable.u);
%             v = s.getValues(Variable.v);
%             p = s.getValues(Variable.p);
%             
            uField = s.uField; %s.getValues(Variable.u);
            vField = s.vField; %s.getValues(Variable.v);
            pField = s.pField; %s.getValues(Variable.p);
            
            if nargin==2
                position = 1;
            end
            
            u = cell(s.mesh.getNumLocalElements,1);
            v = cell(s.mesh.getNumLocalElements,1);
            p = cell(s.mesh.getNumLocalElements,1);
            
            if (position==0)
                for e=1:s.mesh.getNumLocalElements
                    u{e} = reshape(uField{e},s.mesh.getLocalElement(e).disc.Q);
                    u{e} = u{e}(:,:,1)';

                    v{e} = reshape(vField{e},s.mesh.getLocalElement(e).disc.Q);
                    v{e} = v{e}(:,:,1)';

                    p{e} = reshape(pField{e},s.mesh.getLocalElement(e).disc.Q);
                    p{e} = p{e}(:,:,1)';
                end
            else
                for e=1:s.mesh.getNumLocalElements
                    u{e} = reshape(uField{e},s.mesh.getLocalElement(e).disc.Q);
                    u{e} = u{e}(:,:,end)';

                    v{e} = reshape(vField{e},s.mesh.getLocalElement(e).disc.Q);
                    v{e} = v{e}(:,:,end)';

                    p{e} = reshape(pField{e},s.mesh.getLocalElement(e).disc.Q);
                    p{e} = p{e}(:,:,end)';
                end
            end
            
            s.plotAll(fig,u,v,p);
%             s.plotReference();
            
            drawnow;
        end
%         
%         function plotAnalyticalSolution(s,fig)
%             for e=1:s.mesh.getNumElements
%                 element = s.mesh.getElement(e);
%                 u{e} = s.analyticalSolution(Variable.u,element.xelem,element.yelem)';
%                 v{e} = s.analyticalSolution(Variable.v,element.xelem,element.yelem)';
%                 p{e} = s.analyticalSolution(Variable.p,element.xelem,element.yelem)';
%             end
%             
%             s.plotAll(fig,u,v,p);
%         end
%            
        function plotAll(s,fig,u,v,p)
            screensize = get( groot, 'Screensize' );
            sizex = 625;
            sizey = 1000;
            set(fig,'position',[0.5*(screensize(3)-sizex) 0.5*(screensize(4)-sizey) sizex sizey]); 
            
            % U-plot
            delete(subplot(3,2,1))
            subplot(3,2,1)
            for e=1:s.mesh.getNumLocalElements
                element = s.mesh.getLocalElement(e);
                surf(element.xelem,element.yelem,u{e});
                hold on;
            end
            xlim([s.mesh.x0 s.mesh.x1])
            view([30,30])
            %view([0,90])
            
            % V-plot
            delete(subplot(3,2,3))
            subplot(3,2,3)
            for e=1:s.mesh.getNumLocalElements
                element = s.mesh.getLocalElement(e);
                surf(element.xelem,element.yelem,v{e});
                hold on;
            end
            view([30,30])
            %view([0,90])
            
            % P-plot
            delete(subplot(3,2,5))
            subplot(3,2,5)
            for e=1:s.mesh.getNumLocalElements
                element = s.mesh.getLocalElement(e);
                surf(element.xelem,element.yelem,p{e});
                hold on;
            end
            view([30,30])
            %view([0,90])
            
            % U-velocity at x=0.5
            delete(subplot(3,2,2))
            subplot(3,2,2)
            for e=1:s.mesh.getNumLocalElements
                element = s.mesh.getLocalElement(e);
                z=interp2(element.xelem,element.yelem,u{e},0.5,element.yelem);
                plot(element.yelem,z,'k');
                hold on;
            end
            xlim([0 1])
            view([90,-90])
            
            % V-velocity at x=0.5
            delete(subplot(3,2,4))
            subplot(3,2,4);
            for e=1:s.mesh.getNumLocalElements
                element = s.mesh.getLocalElement(e);
                z=interp2(element.xelem,element.yelem,v{e},0.5,element.yelem);
                plot(element.yelem,z,'k');
                hold on;
            end
            ylim([-1e-3 1e-3])
            view([90,-90]) 
            
%             % Velocity contours
%             delete(subplot(3,2,6))
%             subplot(3,2,6)
%             for e=1:s.mesh.getNumElements
%                 element = s.mesh.getElement(e);
%                 z=interp2(element.xelem,element.yelem,p{e},0.5,element.yelem);
%                 plot(element.yelem,z,'k');
% %                 contourf(element.xelem,element.yelem,hypot(u{e},v{e}),[0:0.25:10]);
%                 hold on;
%             end
%             view([0,90])
        end
%         
%         function plotReference(s)
%             for e=1:s.mesh.getNumElements
%                 element = s.mesh.getElement(e);
%                 u{e} = s.analyticalSolution(Variable.u,element.xelem,element.yelem)';
%                 v{e} = s.analyticalSolution(Variable.v,element.xelem,element.yelem)';
%                 p{e} = s.analyticalSolution(Variable.p,element.xelem,element.yelem)';
%             end
%             
%             subplot(3,2,2);
%             for e=1:s.mesh.getNumElements
%                 element = s.mesh.getElement(e);
%                 z=interp2(element.xelem,element.yelem,u{e},0.5,element.yelem);
%                 plot(element.yelem,z,'r');
%                 hold on;
%             end
%             view([90,-90]) 
%             
%             subplot(3,2,4);
%             for e=1:s.mesh.getNumElements
%                 element = s.mesh.getElement(e);
%                 z=interp2(element.xelem,element.yelem,v{e},0.5,element.yelem);
%                 plot(element.yelem,z,'r');
%                 hold on;
%             end
%             view([90,-90])
%             
%             subplot(3,2,6);
%             for e=1:s.mesh.getNumElements
%                 element = s.mesh.getElement(e);
%                 z=interp2(element.xelem,element.yelem,p{e},0.5,element.yelem);
%                 plot(element.yelem,z,'r');
%                 hold on;
%             end
%             view([90,-90]) 
%         end
%         
%         function out = analyticalSolution(s,variable,x,y,z)
%             switch variable
%                 case Variable.u
%                     out = 1 - exp(s.lambda.*x) .* cos(2*pi*y');
%                 case Variable.v
%                     out = s.lambda/(2*pi) * exp(s.lambda.*x) .* sin(2*pi*y');
%                 case Variable.p
%                     out = s.P0 - 1/2 * exp(2*s.lambda.*x) .* exp(0*y');
%             end
%             if ~exist('z','var') || isempty(z)
%                 z=1;
%             end
%             out = repmat(out,1,1,length(z));
%         end
    end
    
    methods (Access=private)

    end
    
    methods (Static)
        function out = load(s)
            out = FluidBox();
            out.loadObject(s);
        end
    end
    
end

