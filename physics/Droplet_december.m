classdef Droplet < CahnHilliard
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods        
        function addDroplet(s,center,radius)
%             fh = @(x,y,z) s.initialCircle(x,y,z,center,radius);
%             s.addC( fh );
            
            fh = @(x,y,z) s.initialCircle(x,y,z,center,radius);
            s.cField.add( fh );
            
            fh = @(x,y,z) s.initialOmg(x,y,z,center,radius);
            s.omgField.add( fh );
        end
        
        % Function which defines an circle through a specified center with 
        % a given radius. The x,y,z data are the locations where the C
        % data needs to be evaluated. The center is a vector of length 3,
        % which contains its x,y,z data. Inside the circle C=1, outside C=0
        function c = initialCircle(s,x,y,z,center,radius)
            if length(center)~=3
                error('The center should have length == 3')
            end
            
            c = zeros(length(x),length(y),length(z));
            
            denominator = (2*sqrt(2)*s.Cn);

            xc = center(1);
            yc = center(2);
%             zc = center(3);
            
            for j=1:length(y)
                for i=1:length(x)
                    c(i,j,:) = 0.5 + 0.5 * tanh( (radius - sqrt( (x(i)-xc)^2 + (y(j)-yc)^2) ) / denominator );
                end
            end
        end
        
        % Analytical solution for omega, which complies with the solution for
        % the variable c (see above). Omega is defined by:
        % 
        %   omega = 1/eps * (C^3 - 3/2*C^2 + 1/2*C) - eps * laplacian(C)
        %
        function omg = initialOmg(s,x,y,z,center,radius)
            if length(center)~=3
                error('The center should have length == 3')
            end
            
            omg = zeros(length(x),length(y),length(z));
            omg2 = zeros(length(x),length(y),length(z));
            
            xc = center(1);
            yc = center(2);
            % zc = center(3);
                        
            %factor = s.alp/(s.We);
            denominator = (2*sqrt(2)*s.Cn);
            
            for j=1:length(y)
                for i=1:length(x)
                    r = sqrt( (x(i)-xc)^2 + (y(j)-yc)^2 );
%                     if (r~=0)
%                         omg(i,j,:) = factor * sqrt(2) / ( 8 * r * cosh( ( (sqrt(2)*(r-radius)) / (4*s.Cn) ) )^2 );
%                     else
%                         omg(i,j,:) = 0; % to prevent 'inf' solution
%                     end
                    
                    % Only c
% %                     c = 0.5 + 0.5 * tanh( (radius - r ) / denominator );
% %                     omg(i,j,:) = (4*sqrt(2) / s.Cn) * ( c.^3 - 3/2 * c.^2 + 1/2 * c ); %
                    
                    omg(i,j,:) = 0.5*sqrt(2) * sinh( (r-radius)/denominator ) / ( s.Cn * cosh( (r-radius) / denominator )^3);

                    % Only laplacian
                    if (r~=0)
                        omg(i,j,:) = omg(i,j,:) - ( 0.5*sqrt(2) * sinh( (r-radius)/denominator ) * r - s.Cn*cosh( (r-1)/denominator ) ) / ...
                                                                                            ( r * s.Cn*cosh( (r-1)/denominator )^3 );
                    else
                        omg(i,j,:) = omg(i,j,:) + 0;
                    end
                    
%                     epsilon = s.Cn;
%                     if (r~=0)
%                         omg(i,j,:) = omg(i,j,:) + 3*sqrt(2)*(cosh((-radius+r)*sqrt(2)/(4*epsilon))*sqrt(2)*epsilon-sinh((-radius+r)*sqrt(2)/(4*epsilon))*r) / (4*epsilon*r*cosh((-radius+r)*sqrt(2)/(4*epsilon))^3);
%                     else
%                         omg(i,j,:) = omg(i,j,:) + 0; % to prevent 'inf' solution
%                     end
                end
            end
            
            omg = omg / radius * 3/2;
            
%             omg = omg2;
        end  
    end
    
end

