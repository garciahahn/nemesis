classdef CahnHilliard_temp < Physics
    %CahnHilliard Implementation of the Cahn-Hilliard equation
    %   Detailed explanation goes here
    
    properties
        % CH properties
        % Refinement settings (min/max values for C)
        Pe; Cn; M; We; alpha; Cn_L;
        Pr; Re; Ja;
        H;
        lamRho; 
        
        massTotal   , massLossTotal;
        massEnclosed, massLossEnclosed;
        
        meanOmega;
        totalVolume;
        intGradC;
        kappa;
        cm_total, cm_enclosed;        % Center of mass
        cmv_total, cmv_enclosed; 
        
        % Fields
        %cField;
        %omgField;
        
        %cConvergence = 1e-4;
        %omgConvergence = 1e-4;
        
        mobilityType;
        
        interface;
        stabilize;
        useMeanCurvature = false;
    end
    
    methods
        
        function s = CahnHilliard_temp(mesh,settings)            
            s = s@Physics(mesh);
            
            s.shortName = 'CH';
            s.longName  = 'CahnHilliard';
            s.name      = 'CH';
            s.nEq       = 2;
            s.variables = {'c','omg'};
            
            s.initialize(settings);       
        end
        
        function initialize(s,settings)
            initialize@Physics(s,settings);
            
            % General physical settings
            s.alpha     = System.settings.phys.alpha;
            s.Pe        = System.settings.phys.Pe;   % Peclet number
            s.Cn        = System.settings.phys.Cn;   % Cahn number
            s.Cn_L      = System.settings.phys.Cn * System.settings.phys.radius;   % Cahn number
            s.M         = System.settings.phys.M;    % mobility    
            s.We        = System.settings.phys.We;   % Weber number
            s.Pr        = System.settings.phys.Pr;
            s.Ja        = System.settings.phys.Ja;
            s.Re        = System.settings.phys.Re;
            s.lamRho    = System.settings.phys.lamRho;
            
            % Cahn-Hilliard specific settings
            if (isfield(settings,'mobilityType'))
                s.mobilityType = settings.mobilityType;
            end
            
            % Cahn-Hilliard specific settings
            if (isfield(settings,'stabilize'))
                s.stabilize = settings.stabilize;
            end
            
            % Cahn-Hilliard specific settings
            if (isfield(settings,'useMeanCurvature'))
                s.useMeanCurvature = settings.useMeanCurvature;
            end
        end
        
        function time = setupEquations(s) %,physics)
            t1 = toc;
            
            %s.eqWeights = System.settings.comp.CH.equationWeights;
            
            U   = FieldCollection.get('u');
            V   = FieldCollection.get('v');
            C   = FieldCollection.get('c');
            Omg = FieldCollection.get('omg');
            
            if (~System.settings.comp.coupling.enabled || ~s.coupled)
                U = NaN;
                V = NaN;
                Uk = 0;
                Vk = 0;
            %else
            %    fprintf('WARNING : NO COUPLING OF CH WITH ANY OTHER PHYSICS')
            end
            
            % Compute the mean curvature (if requested, otherwise just a cell array with zeros)
            s.computeH;
            
            % Droplet/bubble center of gravity velocity at temporal
            % quadrature points
            if (System.settings.phys.dynamicReferenceFrame)
                s.computeCenterOfMassVelocity(true,false);       % determine the center-of-mass velocity: compute the enclosed mass, and without overIntegration
                uc = s.cmv_total(:,1) - System.settings.phys.bubbleVelocity(1);
                vc = s.cmv_total(:,2) - System.settings.phys.bubbleVelocity(2);
                %fprintf('WARNING : USING DYNAMIC REFERENCE FRAME')
            end

            %if (System.rank==0)
            %    fprintf('CH coupling : %d\n',s.coupled)
            %end
                
%              	%U = FieldCollection.get('u');
%               	%V = FieldCollection.get('v');
% %                 defaultUVP = false;
%                 
%                 for i=1:numel(physics)
%                     if isa(physics{i},'NavierStokes')
%                         %U = FieldCollection.get('u');
%                         %V = FieldCollection.get('v');
%                         %U = physics{i}.getField( Variable.u, s ); %, physics{i}.activeField );
%                         %V = physics{i}.getField( Variable.v, s ); %, physics{i}.activeField );
%                         %defaultUVP = false;
%                     elseif isa(physics{i},'EnergyEquation')
%                         T = physics{i}.getField( Variable.t, s ); %, physics{i}.activeField );
%                         defaultT = false;
%                     end
%                 end
%             end
                                    
%             if (exist('NS','var') && ~isempty(NS))
%                 [U,V,~] = NS.getFields(NS.activeField);
%             end            
%             [C,Omg] = s.getFields(s.activeField);
%             C   = s.getField( Variable.c  , s ); %, s.activeField );
%             Omg = s.getField( Variable.omg, s ); %, s.activeField );
            
            %s.Gi = zeros(s.mesh.numElements,1);
            
            %% CH) Setup operators
            for e=s.mesh.eRange
                
                % Set active element
                element = s.mesh.getElement(e);
                
                Ck          = C.val(FieldType.Current,e);
                dCkdt       = C.t(FieldType.Current,e);
                dCkdx       = C.x(FieldType.Current,e);
                dCkdy       = C.y(FieldType.Current,e);
                dCkdxx      = C.xx(FieldType.Current,e);
                dCkdyy      = C.yy(FieldType.Current,e);
%                 Omgk        = Omg.val(FieldType.Current,e);
%                 dOmgkdx     = Omg.x(FieldType.Current,e);
%                 dOmgkdy     = Omg.y(FieldType.Current,e);

                gradC2 = dCkdx.^2 + dCkdy.^2;
                gradC  = sqrt(gradC2);
                m2 = Ck.^2 .* (Ck-1).^2;
                
                dOmgkdxx    = [];
                %dOmgkdxx    = Omg.xx(FieldType.Exact,e);
                %dOmgkdyy    = Omg.yy(FieldType.Exact,e);
                
                if (isempty(dOmgkdxx))
                    f1 = 1;
                    dCkdxx   = 0;
                    dCkdyy   = 0;
                    dOmgkdxx = 0;
                    dOmgkdyy = 0;
                else
                    %fprintf('f1 is set to 0\n')
                    f1 = 0;
                end
                
                scaleFactor = 1/(4*sqrt(2)*s.Cn);
                %scaleFactor = 1;
                
                % Elemental operators
                H   = element.getH;
                Dx  = scaleFactor * element.getDx;
                Dy  = scaleFactor * element.getDy;
                
%                 gradC = sqrt( dCkdx.^2 + dCkdy.^2 );
                
                %absGradient = sqrt( Dx.^2 + Dy.^2 );
                
%                 [X,Y] = meshgrid(element.xelem,element.yelem);
                %[X,Y] = meshgrid( element.nodes{1:end-1} );
                
%                 [X,Y,Z,T]=element.getNodes;
%                 X = squeeze(X);
%                 Y = squeeze(Y);
%                 Z = squeeze(Z);
%                 T = squeeze(T);
                
                %dist  = sqrt(X.^2 + Y.^2 + Z.^2);
                %%dist2 = repmat(dist1(:),element.disc.Q(3),1);
                %meanCurv = 0*absGradient./dist(:);
                % 
                %% Remove NaN values
                %meanCurv( meanCurv~=meanCurv ) = 0;                
                
                
                if (s.steady)                    
                    %fprintf('WARNING : STEADY CH')
                    Dt = 0; %element.getH;
                    steady = 0; %Ck;
                else
                    Dt = scaleFactor * element.getDt;
                    steady = 0;
                end
                
                Dxx  = scaleFactor^2 * element.getDxx;
                Dyy  = scaleFactor^2 * element.getDyy;
                Dxy  = scaleFactor^2 * element.getDxy;
                Dxx_t = element.getDxx_t;
                Dyy_t = element.getDyy_t;
                %Dxy  = element.getDxy;
                W    = element.getW;% .* 4.^s.mesh.dh.dofLevels{e}(:);
                matZ = element.getMatZ;
                vecZ = element.getVecZ;      % gzero vector
                Ze   = s.mesh.Z{e};
                
                % Variables
                if ( U==U )
                    Uk = U.val2(e,s.mesh);
                else
                    Uk = 0;
                    %if (s.coupled)
                    %    fprintf('WARNING : U = 0\n')
                    %end
                end
                
                if ( V==V )
                    Vk = V.val2(e,s.mesh);
                else
                    Vk = 0;
                    %if (s.coupled)
                    %    fprintf('WARNING : V = 0\n')
                    %end
                end
                
                % Moving reference frame
                if (System.settings.phys.dynamicReferenceFrame && s.coupled && System.settings.comp.coupling.enabled)
                    %fprintf('WARNING : USING DYNAMIC REFERENCE FRAME')
                    
                    % x-direction
                    if ( ~isempty(uc) )
                        Uk = reshape( Uk, element.finite.qSize );
                        for i=1:numel(uc)
                            Uk(:,:,i) = Uk(:,:,i) - uc(i);
                        end
                        Uk = Uk(:);
                    end
                    
                    % y-direction
                    if ( ~isempty(vc) )
                        Vk = reshape( Vk, element.finite.qSize );
                        for i=1:numel(vc)
                            Vk(:,:,i) = Vk(:,:,i) - vc(i);
                        end
                        Vk = Vk(:);
                    end
                end
                
                
%                 if (~defaultT)
%                     dTkdx = T.x(e);
%                     dTkdy = T.y(e);
%                 else
%                     dTkdx = vecZ;
%                     dTkdy = vecZ;
%                 end   
                
                %rho     = (1-s.lamRho)*Ck + s.lamRho;
                
                % Below are 2 scaling factors for the 2 equations. Together
                % they scale the omega value by 6*sqrt(2)/(Cn*We). Use 
                % 4*sqrt(2)/Cn to make the maximum omega equal to unity, 
                % but then 3/2 should be added to f1. Furthermore, in that 
                % case the initial condition for omega should be changed as 
                % well! And the addition of the surface tension to the NS
                % should be modified.
%                 if (System.settings.time.current == 0)
%                     f1 = 1; %s.alpha / s.We;
%                 else
%                     f1 = 0;
%                 end
                
                %f2 = s.alpha *s.Cn / (s.Cn_L.^2);
                
                f1 = 1;
                f2 = s.alpha / s.Cn;
%                 f1 = s.alpha * s.Re / s.We; %s.alp / s.We;
%                 f2 = 1 / s.Cn;
                
                % Keunsoo
%                 f1 = 1;
%                 f2 = 1;
                
%                 f1 =    1  / s.We;
%                 f2 = s.alp / s.Cn;
%                 f2 = sqrt(2)/s.Cn;
                
                % Parts of equations
                Conv    = (Uk.*Dx + Vk.*Dy);    % + 0*(Ukx + Vky).*H);
                Nabla2  = Dxx + Dyy;            % + 0*Dxy;
                Nabla2t = Dxx_t + Dyy_t;
%                 Nabla2  = Dy;

%                 meanCurv    = sqrt(2)*s.Cn * ( ( gradC ./ ( (1-Ck).*Ck ) ) .* Nabla2 + 1*(2*gradC.^2 .* (Ck-0.5)) ./ ((Ck-1).^2 .* Ck.^2) .* absGradient);
%                 meanCurv( meanCurv~=meanCurv ) = 0;   % remove NaN
%                 meanCurv( ~isfinite(meanCurv) ) = 0;
%                 meanCurv(Ck<0.001) = 0;
%                 meanCurv(Ck>0.999) = 0;
%                 
%                 meanCurvrhs = sqrt(2)*s.Cn^3 * ( 0*( gradC.*(dCkdxx+dCkdyy) ./ ( (1-Ck).*Ck ) ) + 0*(2*gradC.^3 .* (Ck-0.5)) ./ ((Ck-1).^2 .* Ck.^2));
%                 meanCurvrhs( meanCurvrhs~=meanCurvrhs ) = 0;   % remove NaN
%                 meanCurvrhs( ~isfinite(meanCurvrhs) ) = 0;
%                 meanCurvrhs(Ck<0.001) = 0;
%                 meanCurvrhs(Ck>0.999) = 0;
%                                 
%                 Fenergy = (3*Ck.^2 - 3*Ck + 0.5).*H - s.Cn^2 * ( Nabla2 - 1*meanCurv );
                
                
                
%                 Fenergy = -s.Cn^2*Nabla2;
%                 Fenergy = (Ck.^2-3/2*Ck+0.5).*H - 0*s.Cn^2*Nabla2;
                
                % Get the alpha values for C and Omg
                Cs   = C.alp(FieldType.Current,e);
                Omgs = Omg.alp(FieldType.Current,e);    %(SolutionMode.Nonlinear);

                if (s.mobilityType==2)
                    Mobil = 4*(Ck - Ck.^2);
                    auxi  = 4*( 1 - 2*Ck );
                else
                    % By default mobilityType == 1 is selected
                    Mobil = 1;
                    auxi = 0;
                end                

                theta = 0.5;
                
                Mphi = - (1/s.Pe) * ( auxi * (  theta) .*((Dx*Omgs).*Dx + (Dy*Omgs).*Dy)                 );
                Momg = - (1/s.Pe) * ( auxi * (1-theta) .*((Dx*Cs  ).*Dx + (Dy*Cs  ).*Dy) + Mobil.*Nabla2 );
                
                %Momg = matZ;
                %Momgrhs = (1/s.Pe) * ( auxi * 0.5.*((Dx*Cs  ).* dOmgkdx + (Dy*Cs  ).*dOmgkdy) + Mobil.* (dOmgkdxx+dOmgkdyy) );
                
%                 weight = 1; 
                
                el = element.localID;
                
                weight1 = s.eqWeights(1);
                weight2 = s.eqWeights(2);
                
                %weight1 = 10^element.lvl;
                %weight2 = 10^element.lvl;
                
                %W = W*0.25^element.lvl;
                
%                 M = 0.00;
%                 
%                 %evaporation = (1./rho) .* ( (s.Ja/(s.Re*s.Pr)) * ( dTkdx .* Dx + dTkdy .* Dy ) );
%                 evaporation = M * (1./rho) .* 6/2 .* Ck .* (1-Ck) .* sqrt( Dx.^2 + Dy.^2 );
                
                %Ck = 0*Ck + 1.5;

                % Options for CH equations:
                %   1) normal use
                %   2) extra Dt for omega equation (ensure that the initial condition for omega is also set every timestep!)
                %   3) split scheme
                CH_option = 1;
                
                % Option 1
                if (CH_option == 1)
                    f1      = 1;
                    lapC    = 0;
                    L22     = H;
                    %Fenergy_impl = (3*Ck.^2 - 3*Ck + 0.5).*H - f1 * s.Cn_L^2 * ( Nabla2 .*( 1 - 2*s.Cn_L*gradC./(sqrt(2*s.Cn_L^2*gradC2+m2)) )  );         % 3*C^3 - 3*C^2 + 0.5*C - Cn^2 * (Cxx+Cyy)
                    Fenergy_impl = (3*Ck.^2 - 3*Ck + 0.5).*H - f1 * s.Cn_L^2 * ( Nabla2 );
                    Fenergy_expl = (2*Ck.^3 - 1.5*Ck.^2) + 0*(1-f1) * s.Cn_L^2 * ( dCkdxx + dCkdyy );

                elseif (CH_option == 2)
                    f1      = 1;
                    lapC    = 0;
                    L22     = Dt;
                    Fenergy_impl = (3*Ck.^2 - 3*Ck + 0.5).*Dt - f1 * s.Cn_L^2 * ( Nabla2t );         % 3*C^3 - 3*C^2 + 0.5*C - Cn^2 * (Cxx+Cyy)
                    Fenergy_expl = 0*(2*Ck.^2 - 1.5*Ck.^1) .* dCkdt + (1-f1) * s.Cn_L^2 * ( dCkdxx + dCkdyy );
                
                elseif (CH_option == 3)
                    f1      = 1;
                    lapC    = 0;
                    L22     = H;
%                     Fenergy_impl = (3*Ck.^2 - 3*Ck + 0.5).*H - 2* f1 * s.Cn_L^2 * ( dCkdx.^2 .* Dxx + 2*dCkdx.*dCkdy.* Dxy + dCkdy.^2 .* Dyy ) ./ (gradC2);
                    Fenergy_impl = (Ck.^2 - 3/2*Ck + 0.5).*H - 2* f1 * s.Cn_L^2 * ( dCkdx.^2 .* Dxx + 2*dCkdx.*dCkdy.* Dxy + dCkdy.^2 .* Dyy ) ./ (gradC2);
                    Fenergy_expl = 0*(2*Ck.^3 - 1.5*Ck.^2);
                    
                else 
                    Fenergy_expl = 0*Ck;
                    Fenergy_impl = -s.Cn_L^2 * Nabla2;
                    lapC = -(1/s.Pe) * 6*(Ck-0.5) .* Nabla2;
                    L22 = H;
                end
                
                % Option 2
                %Fenergy_impl = 0*(-s.Cn^2 * ( Nabla2 ));
                %Fenergy_expl = (-Ck.^3 + 1.5*Ck.^2 - 0.5*Ck + s.Cn^2 * (dCkdxx+dCkdyy));

                %try
                % Operators L & G
                %               C                 Omega

                              
                if (CH_option==3)
                    s.opL{el} = [ weight1.*(Dt+Conv+Mphi+ lapC )               weight1.*(s.Cn/s.Cn_L)*(f1*Momg)       ; ...
                                  weight2.*f2*(Fenergy_impl)                   -weight2.*L22              ];

                    s.opG{el} = [ weight1 .* (vecZ+steady + 0*(1/s.Pe)* (1-f1) * Mobil*(dOmgkdxx+dOmgkdyy) )          ; ...
                                  weight2 .* f2*(Fenergy_expl) ];             
                else
                    s.opL{el} = [ weight1.*(Dt+Conv+Mphi+ lapC )               weight1.*(s.Cn/s.Cn_L)*(f1*Momg)       ; ...
                                  weight2.*f2*(Fenergy_impl)                   -weight2.*L22              ];

                    s.opG{el} = [ weight1 .* (vecZ+steady + 0*(1/s.Pe)* (1-f1) * Mobil*(dOmgkdxx+dOmgkdyy) )          ; ...
                                  weight2 .* f2*(Fenergy_expl) + s.Cn_L/s.Cn * s.H{el}(:) - 0];
                                  %weight2 .* f2*(Fenergy_expl) ]; %+ 0*(2*sqrt(2))/s.alpha*s.H{el}(:) ];
                end
                                            
                if (s.withForcing)
                    s.opG{el} = s.opG{el} - s.forcing{el};
                end

                s.opL{el} = s.opL{el} * superkron(eye(s.Nvar),Ze);
                         
                % Weights
                s.W{el}      = repmat(W,s.nEq,1);
                s.sqrtW{el}  = repmat(sqrt(W),s.nEq,1);
                
                %s.Gi(e) = 0;
                %s.Gi(e) = s.Gi(e) + s.computeSquaredNorm(element,dCkdx);
                %s.Gi(e) = s.Gi(e) + s.computeSquaredNorm(element,dCkdy);
                %s.Gi(e) = s.Gi(e) + s.computeSquaredNorm(element,dOmgkdx);
                %s.Gi(e) = s.Gi(e) + s.computeSquaredNorm(element,dOmgkdy);
                %s.Gi(e) = sqrt( s.Gi(e) );
            end 
            
            t2 = toc;
            time = t2-t1;
        end
                  
        function setSTField(s)
            disp('setting surface tension field')
            
            
            s.omgField * s.cField.x;
            
            dCkdx   = C{e}.x;
            dCkdy   = C{e}.y;
            Omgk    = Omg{e};
            
            s.WEI * Omgk.*dCkdx;
        end
                
        function updateInterpolated(s,physics)
            if (nargin==1)
                mesh=s.getMesh;
            else
                for i=1:numel(physics)
                    if ~isa(physics{i},class(s))
                        mesh = physics{i}.getMesh;
                    end
                end
            end
            s.cField.updateInterpolated(mesh);
            s.omgField.updateInterpolated(mesh);
        end           
        
        function updateCoupling(s)
            s.cField.update(SolutionMode.Coupling)
            s.omgField.update(SolutionMode.Coupling)
        end

        % See Ding & Spelt (2007)
        %     Yue & Feng (2011)
        %     Dong (2012)
        %     Yue et al (2010)
        function applyDynamicContactAngle_v1(s,boundaryNumber,field,theta_s,bcType,coupledPhysics)
            boundary = s.mesh.getBoundary(boundaryNumber);
            activeFEBoundary = s.boundary_v1{boundaryNumber};   
                        
            if (isempty(theta_s))
                theta_s = System.settings.phys.ContactAngle;
            end
            
            switch bcType
                case BCType.Strong
                    error('A strong version of the dynamic contact angle bc has not been implemented')
                
                case BCType.Weak
                    
                    %U = coupledPhysics{2}.getField(1,s);
                    %V = coupledPhysics{2}.getField(2,s);
                    
                    U = FieldCollection.get('u');
                    V = FieldCollection.get('v');
                    
                    for be=1:boundary.getNumElements
                        
                        %indp = (1:boundary.getElements(be).disc.dofev) + (variable-1)*boundary.getElements(be).disc.dofev;
                        
                        e = boundary.getElementNumbers(be);
                                                
                        if ismember(e,s.mesh.eRange)
                            
                            element = s.mesh.getElement(e);
                            indq    = boundary.getHposQ(element.disc.dofeq);        % these are logicals

                            indqD   = boundary.getDposQ(element.disc.dofeq);        % these are logicals
                            
                            if (isempty(activeFEBoundary.opL{be,field.getVariable}))
                                activeFEBoundary.opL{be,field.getVariable} = sparse(element.disc.dofevq,element.disc.dofev);
                            end
                            if (isempty(activeFEBoundary.opG{be,field.getVariable}))
                                activeFEBoundary.opG{be,field.getVariable} = sparse(element.disc.dofevq,1);
                            end
                            if (isempty(activeFEBoundary.W{be}))
                                activeFEBoundary.W{be}                     = sparse(element.disc.dofevq,1);
                            end
                            
                            Dw = System.settings.phys.Dw;
                            
                            Uk      = U.val(e);
                            Vk      = V.val(e);
                            Conv    = ( Uk(indq).*boundary.Dx + Vk(indq).*boundary.Dy );
                            
%                             Ck = wi*C(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' + (1-wi)*Ci(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' ;

                            % NB: theta is in radians
                            %
                            % fw' = 6 * (C^2-C) * sigma * cos(theta)
                            % lambda = 6*sqrt(2) * sigma / Cn
                            % fw' / lambda = 0.5*sqrt(2) * (C^2-C) / Cn * cos(theta)

                            Ck = s.cField.val(e); %getC(e,SolutionMode.Nonlinear);
                            
                            factor      = -1/(sqrt(2)*System.settings.phys.Cn) * cos(theta_s/180*pi) .* (Ck(indq)-1); % 0 degrees: hydrophillic, 180 degrees: hydrophobic
                            factorrhs   = -1/(sqrt(2)*System.settings.phys.Cn) * cos(theta_s/180*pi) .* Ck(indq) .* (Ck(indq)-1); % 0 degrees: hydrophillic, 180 degrees: hydrophobic
                            
                            % the additional 'Ck(indq)' is included by H
                            
                            %boundary.opL{be}( indq(:), indp(:) ) = ( -Dw*(boundary.Dz+Conv) + boundary.Dn + factor.*boundary.H )* s.mesh.getZ(e);
                            % L = ( -Dw*(disc.By0.Dz*mesh.Jz(elem)+Conv) + disc.By0.D*mesh.Jy(elem) + (disc.By0.H)*0.5*sqrt(2)/Cn*cos(theta_s/180*pi)*sparse(diag(1-Ck')))

                            %mixingEnergy = System.settings.phys.alpha * System.settings.phys.Cn * System.settings.phys.sigma;
                            
                            valueVector = -1*factorrhs + 0*ones( nnz(indqD), 1 );

                            activeFEBoundary.opL{be,field.getVariable}( indqD(:), : ) = ( -Dw*(boundary.Dz+Conv) + boundary.Dn + 0*factor.*boundary.H )* s.mesh.getZ(e);
                                                                                                                  %boundary.Dn * s.mesh.getZ(e);      % unit operator
                            activeFEBoundary.W{be}( indqD(:) )                        = (boundary.Wdisc)./(boundary.J(be));
                            activeFEBoundary.opG{be,field.getVariable}( indqD(:) )    = valueVector(:) / boundary.Jn(be); 
                        end
                    end
            end
        end

        % See Ding & Spelt (2007)
        %     Yue & Feng (2011)
        %     Dong (2012)
        %     Yue et al (2010)
        function applyDynamicContactAngle(s,bcID,boundaryNumber,field,theta_s,bcType,coupledPhysics)
%             boundary = s.mesh.getBoundary(boundaryNumber);
            activeFEBoundary = s.boundary{bcID};   
                        
            if (isempty(theta_s))
                theta_s = System.settings.phys.ContactAngle;
            end
            
            switch bcType
                case BCType.Strong
                    error('A strong version of the dynamic contact angle bc has not been implemented')
                
                case BCType.Weak
                    
                    % Initialize the LGW cell arrays of this weak FEBoundary
                    activeFEBoundary.init(s.Nvar);
                    
%                     U = coupledPhysics{2}.getField(1,s);
%                     V = coupledPhysics{2}.getField(2,s);
                    U = FieldCollection.get('u');
                    V = FieldCollection.get('v');
                    
                    for be=1:activeFEBoundary.getNumElements
                        
                        %indp = (1:boundary.getElements(be).disc.dofev) + (variable-1)*boundary.getElements(be).disc.dofev;
                        
                        e = activeFEBoundary.getElementNumbers(be);
                                                
                        if ismember(e,s.mesh.eRange)
                            
                            element = s.mesh.getElement(e);
                            indq    = activeFEBoundary.getHposQ(element.disc.dofeq);        % these are logicals
                            indqD   = activeFEBoundary.getDposQ(element.disc.dofeq);        % these are logicals
                            
                            if (isempty(activeFEBoundary.opL{be,field.getVariable}))
                                activeFEBoundary.opL{be,field.getVariable} = sparse(element.disc.dofevq,element.disc.dofev);
                            end
                            if (isempty(activeFEBoundary.opG{be,field.getVariable}))
                                activeFEBoundary.opG{be,field.getVariable} = sparse(element.disc.dofevq,1);
                            end
                            if (isempty(activeFEBoundary.W{be}))
                                activeFEBoundary.W{be}                     = sparse(element.disc.dofevq,1);
                            end
                            
                            Dw = System.settings.phys.Dw;
                            
                            Uk      = U.val(e);
                            Vk      = V.val(e);
                            Conv    = ( Uk(indq).*activeFEBoundary.Dx + Vk(indq).*activeFEBoundary.Dy );
                            
%                             Ck = wi*C(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' + (1-wi)*Ci(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' ;

                            % NB: theta is in radians
                            %
                            % fw' = 6 * (C^2-C) * sisogma * cos(theta)
                            % lambda = 6*sqrt(2) * sigma / Cn
                            % fw' / lambda = 0.5*sqrt(2) * (C^2-C) / Cn * cos(theta)

                            Ck = s.cField.val(e); %getC(e,SolutionMode.Nonlinear);
                            
                            factor      = -1/(sqrt(2)*System.settings.phys.Cn) * cos(theta_s/180*pi) .* (Ck(indq)-1); % 0 degrees: hydrophillic, 180 degrees: hydrophobic
                            factorrhs   = -1/(sqrt(2)*System.settings.phys.Cn) * cos(theta_s/180*pi) .* Ck(indq) .* (Ck(indq)-1); % 0 degrees: hydrophillic, 180 degrees: hydrophobic
                            
                            dCkdx = s.cField.x(e);
                            dCkdy = s.cField.y(e);
                            
                            gradC = sqrt(dCkdx.^2 + dCkdy.^2);
                            
                            %factorrhs   = cos(theta_s/180*pi) .* gradC(indq);
                            
                            % the additional 'Ck(indq)' is included by H
                            
                            %boundary.opL{be}( indq(:), indp(:) ) = ( -Dw*(boundary.Dz+Conv) + boundary.Dn + factor.*boundary.H )* s.mesh.getZ(e);
                            % L = ( -Dw*(disc.By0.Dz*mesh.Jz(elem)+Conv) + disc.By0.D*mesh.Jy(elem) + (disc.By0.H)*0.5*sqrt(2)/Cn*cos(theta_s/180*pi)*sparse(diag(1-Ck')))

                            %mixingEnergy = System.settings.phys.alpha * System.settings.phys.Cn * System.settings.phys.sigma;
                            
                            valueVector = -1*factorrhs + 0*ones( nnz(indqD), 1 );

                            activeFEBoundary.opL{be,field.getVariable}( indqD(:), : ) = ( -Dw*(activeFEBoundary.Dz+Conv) + activeFEBoundary.Dn + 0*factor.*activeFEBoundary.H )* s.mesh.getZ(e);
                                                                                                                  %boundary.Dn * s.mesh.getZ(e);      % unit operator
                            activeFEBoundary.W{be}( indqD(:) )                        = (activeFEBoundary.Wdisc)./(activeFEBoundary.J(be));
                            activeFEBoundary.opG{be,field.getVariable}( indqD(:) )    = valueVector(:) / activeFEBoundary.Jn(be); 
                            
%                             value = 0;
                            
%                             if (~iscell(value) && length(value)==1)
%                                 boundary.opG{be}( indq(:) ) = value*ones(length(boundary.Wdisc),1);
%                             elseif (iscell(value) && numel(value{1})==element.disc.dofevq)
%                                 values = value{e}(:,:,end);
%                                 boundary.opG{be}( indq(:) ) = values(:);
%                             elseif (iscell(value) && numel(value{1})==numel(indq))
%                                 boundary.opG{be}( indq(:) ) = value{e}(:);
%                             else
%                                 disp('check dirichlet in CH')
%                             end
                            
%                             s.K{e}( ind,ind ) = s.K{e}(ind,ind) + weight * L' * W * L;
%                             if length(value)==1
%                                 s.G{e}(   ind   ) = s.G{e}(   ind   ) + weight * L' * W * value*ones(length(boundary.W),1);
%                             else
%                                 s.G{e}(   ind   ) = s.G{e}(   ind   ) + weight * L' * W * value{be}(:);
%                             end
                        end
                    end
                end
% % %             
% % %             %- dPhi/dx=dOmg/dx=0 at y=y0(bottom)
% % %             for ebc = 1:length(mesh.BCy0.elem)
% % %                 elem = mesh.BCy0.elem(ebc) ;
% % %                 dCbc = zeros(Qx*Qz,1) ;
% % %                 dOmgbc = zeros(Qx*Qz,1) ;
% % %                 indC = [1:mesh.dofev] ;
% % %                 indOmg = [(mesh.dofev+1):2*mesh.dofev] ;
% % %                 if elem>=e_start && elem<=e_end
% % %                     Ck = wi*C(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' + (1-wi)*Ci(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' ;
% % % 
% % %                     Dw = 0.9;
% % %                     %Uk = 0; % wc*U(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' + (1-wc)*Uc(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' ;
% % %                     %Vk = 0; % wc*V(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' + (1-wc)*Vc(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' ;
% % %                     %Conv    = ( diag(Uk)*disc.Dx*mesh.Jx(elem) + diag(Vk)*disc.Dy*mesh.Jy(elem) );
% % %                     Conv = 0;
% % % 
% % %                     CH.Ke{elem-e_start+1}(indC,indC) = CH.Ke{elem-e_start+1}(indC,indC) + wbc* ( ( -Dw*(disc.By0.Dz'*mesh.Jz(elem)+Conv') + disc.By0.D'*mesh.Jy(elem) + 0.5*sqrt(2)/Cn*cos(theta_s/180*pi)*sparse(diag(1-Ck'))*disc.By0.H') * ...
% % %                                                                                                       diag(disc.By0.p.w)./(mesh.Jx(elem)*mesh.Jz(elem))*...
% % %                                                                                                      ( -Dw*(disc.By0.Dz*mesh.Jz(elem)+Conv) + disc.By0.D*mesh.Jy(elem) + (disc.By0.H)*0.5*sqrt(2)/Cn*cos(theta_s/180*pi)*sparse(diag(1-Ck'))) );                                                                                             
% % % 
% % % 
% % %     %                 CH.Ke{elem-e_start+1}(indC,indC) = CH.Ke{elem-e_start+1}(indC,indC) + wbc* ( (disc.By0.D'*mesh.Jy(elem) + 0.5*sqrt(2)/Cn*cos(theta_s/180*pi)*sparse(diag(1-Ck'))*disc.By0.H') * ...
% % %     %                                                                                               diag(disc.By0.p.w)./(mesh.Jx(elem)*mesh.Jz(elem))*...
% % %     %                                                                                              (disc.By0.D*mesh.Jy(elem) + (disc.By0.H)*0.5*sqrt(2)/Cn*cos(theta_s/180*pi)*sparse(diag(1-Ck'))) );                                                                                             
% % % 
% % %     %                 CH.Ke{elem-e_start+1}(indC,indC)=CH.Ke{elem-e_start+1}(indC,indC) + (disc.By0.D'*mesh.Jy(elem))*diag(disc.By0.p.w)./(mesh.Jx(elem)*mesh.Jz(elem))*(disc.By0.D*mesh.Jy(elem))*wbc ;
% % %                     CH.Ge{elem-e_start+1}(indC)=CH.Ge{elem-e_start+1}(indC) + (disc.By0.D'*mesh.Jy(elem))*diag(disc.By0.p.w)./(mesh.Jx(elem)*mesh.Jz(elem))*dCbc*wbc ;
% % % 
% % %     %                 Ck = wi*C(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' + (1-wi)*Ci(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' ;
% % %     %                 CH.Ke{elem-e_start+1}(indC,indC) = CH.Ke{elem-e_start+1}(indC,indC) + wbc* ( (disc.By0.D'*mesh.Jy(elem) + 2*sqrt(2)/Cn*cos(theta_s/180*pi)*sparse(diag(1-Ck'))*disc.By0.H') * ...
% % %     %                                                                                               diag(disc.By0.p.w)./(mesh.Jx(elem)*mesh.Jz(elem))*...
% % %     %                                                                                              (disc.By0.D*mesh.Jy(elem) + (disc.By0.H)*2*sqrt(2)/Cn*cos(theta_s/180*pi)*sparse(diag(1-Ck'))) );
% % %     %                 CH.Ge{elem-e_start+1}(indC)=CH.Ge{elem-e_start+1}(indC) + (disc.By0.D'*mesh.Jy(elem))*diag(disc.By0.p.w)./(mesh.Jx(elem)*mesh.Jz(elem))*dCbc*wbc ;
% % % 
% % %                     CH.Ke{elem-e_start+1}(indOmg,indOmg)=CH.Ke{elem-e_start+1}(indOmg,indOmg) + (disc.By0.D'*mesh.Jy(elem))*diag(disc.By0.p.w)./(mesh.Jx(elem)*mesh.Jz(elem))*(disc.By0.D*mesh.Jy(elem))*wbc ;
% % %                     CH.Ge{elem-e_start+1}(indOmg)=CH.Ge{elem-e_start+1}(indOmg) + (disc.By0.D'*mesh.Jy(elem))*diag(disc.By0.p.w)./(mesh.Jx(elem)*mesh.Jz(elem))*dOmgbc*wbc ;
% % %                 end
% % %             end
        end
        
        function applyDirichletContactAngle(s,boundaryNumber,field,theta_s,bcType,coupledPhysics)
            boundary = s.mesh.getBoundary(boundaryNumber);
            activeFEBoundary = s.boundary{boundaryNumber};   
                        
            if (isempty(theta_s))
                theta_s = System.settings.phys.ContactAngle;
            end
            
            switch bcType
                case BCType.Strong
                    error('A strong version of the dynamic contact angle bc has not been implemented')
                
                case BCType.Weak
                    
                    %C = coupledPhysics{1}.getField(1,s);
                    
                    %U = coupledPhysics{2}.getField(1,s);
                    %V = coupledPhysics{2}.getField(2,s);
                    
                    C = FieldCollection.get('c');
                    U = FieldCollection.get('u');
                    V = FieldCollection.get('v');
                    
                    for be=1:boundary.getNumElements
                        
                        %indp = (1:boundary.getElements(be).disc.dofev) + (variable-1)*boundary.getElements(be).disc.dofev;
                        
                        e = boundary.getElementNumbers(be);
                                                
                        if ismember(e,s.mesh.eRange)
                            
                            element = s.mesh.getElement(e);
                            indq    = boundary.getHposQ(element.disc.dofeq);        % these are logicals

                            indqD   = boundary.getDposQ(element.disc.dofeq);        % these are logicals
                            
                            if (isempty(activeFEBoundary.opL{be,field.getVariable}))
                                activeFEBoundary.opL{be,field.getVariable} = sparse(element.disc.dofevq,element.disc.dofev);
                            end
                            if (isempty(activeFEBoundary.opG{be,field.getVariable}))
                                activeFEBoundary.opG{be,field.getVariable} = sparse(element.disc.dofevq,1);
                            end
                            if (isempty(activeFEBoundary.W{be}))
                                activeFEBoundary.W{be}                     = sparse(element.disc.dofevq,1);
                            end
                            
                            Dw = System.settings.phys.Dw;
                            
                            Uk      = U.val(e);
                            Vk      = V.val(e);
                            Conv    = ( Uk(indq).*boundary.Dx + Vk(indq).*boundary.Dy );
                            
%                             Ck = wi*C(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' + (1-wi)*Ci(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' ;

                            % NB: theta is in radians
                            %
                            % fw' = 6 * (C^2-C) * sigma * cos(theta)
                            % lambda = 6*sqrt(2) * sigma / Cn
                            % fw' / lambda = 0.5*sqrt(2) * (C^2-C) / Cn * cos(theta)

                            Ck = s.cField.val(e); %getC(e,SolutionMode.Nonlinear);
                            Cxx = s.cField.xx(e);
                            Cyy = s.cField.yy(e);
                            
                            %factor      = -1/(sqrt(2)*System.settings.phys.Cn) * cos(theta_s/180*pi) .* (Ck(indq)-1); % 0 degrees: hydrophillic, 180 degrees: hydrophobic
                            %factorrhs   = -1/(sqrt(2)*System.settings.phys.Cn) * cos(theta_s/180*pi) .* Ck(indq) .* (Ck(indq)-1); % 0 degrees: hydrophillic, 180 degrees: hydrophobic
                            
                            % the additional 'Ck(indq)' is included by H
                            
                            %boundary.opL{be}( indq(:), indp(:) ) = ( -Dw*(boundary.Dz+Conv) + boundary.Dn + factor.*boundary.H )* s.mesh.getZ(e);
                            % L = ( -Dw*(disc.By0.Dz*mesh.Jz(elem)+Conv) + disc.By0.D*mesh.Jy(elem) + (disc.By0.H)*0.5*sqrt(2)/Cn*cos(theta_s/180*pi)*sparse(diag(1-Ck')))

                            %mixingEnergy = System.settings.phys.alpha * System.settings.phys.Cn * System.settings.phys.sigma;
                            
                            %valueVector = -1*factorrhs + 0*ones( nnz(indqD), 1 );
                            
                            C   = Ck(indq);
                            Cxx = Cxx(indq);
                            Cyy = Cyy(indq);
                            
                            valueVector = System.settings.phys.alpha/System.settings.phys.Cn * ( (3*C.^3-3/2*C.^2+0.5.*C) - System.settings.phys.Cn^2 .* ( Cxx + Cyy ) );

                            activeFEBoundary.opL{be,field.getVariable}( indqD(:), : ) = ( -0*Dw*(boundary.Dz+Conv) + 0*boundary.Dn + boundary.H )* s.mesh.getZ(e);
                                                                                                                  %boundary.Dn * s.mesh.getZ(e);      % unit operator
                            activeFEBoundary.W{be}( indqD(:) )                        = (boundary.Wdisc)./(boundary.J(be));
                            activeFEBoundary.opG{be,field.getVariable}( indqD(:) )    = valueVector(:) / boundary.Jn(be); 
                            
%                             value = 0;
                            
%                             if (~iscell(value) && length(value)==1)
%                                 boundary.opG{be}( indq(:) ) = value*ones(length(boundary.Wdisc),1);
%                             elseif (iscell(value) && numel(value{1})==element.disc.dofevq)
%                                 values = value{e}(:,:,end);
%                                 boundary.opG{be}( indq(:) ) = values(:);
%                             elseif (iscell(value) && numel(value{1})==numel(indq))
%                                 boundary.opG{be}( indq(:) ) = value{e}(:);
%                             else
%                                 disp('check dirichlet in CH')
%                             end
                            
%                             s.K{e}( ind,ind ) = s.K{e}(ind,ind) + weight * L' * W * L;
%                             if length(value)==1
%                                 s.G{e}(   ind   ) = s.G{e}(   ind   ) + weight * L' * W * value*ones(length(boundary.W),1);
%                             else
%                                 s.G{e}(   ind   ) = s.G{e}(   ind   ) + weight * L' * W * value{be}(:);
%                             end
                        end
                    end
            end          
        end

        function out = getMassTotal(s)            
            out(1) = s.massTotal(end);
            out(2) = s.massLossTotal;
        end
        
        function out = getMassEnclosed(s)            
            out(1) = s.massEnclosed(end);
            out(2) = s.massLossEnclosed;
        end
        
        function out = getCM(s)
            out(1) = s.cm(end,1);       % cmx at end of time slab
            out(2) = s.cm(end,2);       % cmy at end of time slab
        end
        
        % Function to get the velocity of the center of mass
        function out = getCMV(s)
            out(1) = s.cmv_total(end,1);       % cmx at end of time slab
            out(2) = s.cmv_total(end,2);       % cmy at end of time slab
        end
        
%         function out = getConvergence(s,fieldType,residual)
%             if nargin==2
%                 switch fieldType
%                     case FieldType.Nonlinear
%                         out = reshape( System.settings.comp.CH.nConvergence, [numel(System.settings.comp.CH.nConvergence),1] );
%                     case FieldType.Coupled
%                         out = reshape( System.settings.comp.CH.cConvergence, [numel(System.settings.comp.CH.cConvergence),1] );
%                 end
%             else
%                 out = reshape( System.settings.comp.CH.rConvergence, [numel(System.settings.comp.CH.rConvergence),1] );
%             end
%         end
        
        function setNormItems(s,fieldType,iter)
            L2Norm = s.getL2Norm(fieldType);
%             System.setItem(3,'  #',iter,'%4d');
%             System.setItem(4,'  |dC|/|C|',L2Norm(1),'%10.2e');
%             System.setItem(5,'  |dw|/|w|',L2Norm(2),'%10.2e');
            System.setItems(s,iter,L2Norm);
        end
                
        function syncPrescribed(s)
            s.cField.syncPrescribed;
            s.omgField.syncPrescribed;
        end
        
        function prescribed = getPrescribed(s,e)
            if (nargin==2)
                prescribed = [s.cField.getPrescribed(e); s.omgField.getPrescribed(e)];
            else
                prescribed = cell(s.mesh.numLocalElements,1);
                for e=1:numel(prescribed)
                    element = s.mesh.getLocalElement(e);
                    prescribed{e} = s.getPrescribed( element.globalID );
                end
            end
        end
        
        function createInterface(s)

            cField        = s.getField(Variable.c,s);
            mesh          = cField.mesh;
            cData         = cField.value([],[],3);
            C             = cell(mesh.numLocalElements,1);
            
            %% Interface settings
            s.interface         = struct();
            s.interface.value   = 0.5;
            s.interface.active  = true;
            s.interface.props   = {'Color', 'k', 'LineWidth', 2};
            s.interface.x       = cell(mesh.numLocalElements,1);
            s.interface.y       = cell(mesh.numLocalElements,1);
            %interface.z        = cell(mesh.numLocalElements,1);                     
            
            %% Extract interface 
            % Find the contour where the C surface is equal to the
            % requested interface value. Next, extract the x- and y-locations 
            % from the contour matrix C. Interpolate on the C surface
            % to find z-locations for the intersection. Finally plot the
            % line with some properties.
            for e=1:mesh.numLocalElements
                element = mesh.getLocalElement(e);
                
%                 [X,Y] = meshgrid( element.nodes{1:end-1} );
                [X,Y] = element.getNodes;
                X = squeeze(X);
                Y = squeeze(Y);
                
                C{e} = contourc(X,Y,cData{e}(:,:,end)',[s.interface.value s.interface.value]);
                if (~isempty(C{e}))
%                     s.interface.x{e} = C{e}(1, 2:1+C{e}(2,1));
%                     s.interface.y{e} = C{e}(2, 2:1+C{e}(2,1));
                    s.interface.x{e} = C{e}(1, 2:end);
                    s.interface.y{e} = C{e}(2, 2:end);
                    if C{e}(2,1)~=size(C{e},2)-1
                       s.interface.x{e}(:, C{e}(2,1)+1 ) = [];
                       s.interface.y{e}(:, C{e}(2,1)+1 ) = [];
                       s.interface.x{e}(:, 1:C{e}(2,1) ) = fliplr( s.interface.x{e}(:, 1:C{e}(2,1) ) );
                       s.interface.y{e}(:, 1:C{e}(2,1) ) = fliplr( s.interface.y{e}(:, 1:C{e}(2,1) ) );
                    end
                    %interface.z{e} = interp2(X{e}(1,:)', Y{e}(:,1), dataVar{e}, interface.x{e}, interface.y{e});
                    %line(s.interface.x{e}, s.interface.y{e}, repmat(s.interface.value,size(s.interface.x{e})), s.interface.props{:});
                end
            end
        end
        
        % Function to plot the mean curvature as computed from the C field
        function plotH(s)
            % The C function is defined by: C = 0.5 + 0.5 * tanh( z / (2*sqrt(2)*Cn) )
            % From this function the half width parameter w is defined as w = 2*sqrt(2)*Cn
            % Furthermore, we define m = C * (C-1)
            
            s.computeH(s.mesh,true);
            
            for e=s.mesh.eRange                
                element = s.mesh.getElement(e);
                el = element.localID;
                
                [X,Y] = element.getNodes;
                
                X = squeeze(X); Y = squeeze(Y);
                
                meanCurvature = reshape( s.H{el}, element.finite.qSize );
                
                surface(X(:,:,end),Y(:,:,end),meanCurvature(:,:,end));
                hold on;
            end
        end
        
        % Function to plot the mean curvature as computed from the C field
        function plotDelta(s)
            % The C function is defined by: C = 0.5 + 0.5 * tanh( z / (2*sqrt(2)*Cn) )
            % From this function the half width parameter w is defined as w = 2*sqrt(2)*Cn
            % Furthermore, we define m = C * (C-1)
            
            s.computeH(s.mesh,true);
            
            C = s.fields{1};
            
            for e=s.mesh.eRange                
                element = s.mesh.getElement(e);
                el = element.localID;
                
                [X,Y] = element.getNodes;
                
                X = squeeze(X); Y = squeeze(Y);
                
                c      = reshape(C.getValue(el),element.finite.qSize);
                cxx    = reshape(C.xx(FieldType.Current,el),element.finite.qSize);
                cyy    = reshape(C.yy(FieldType.Current,el),element.finite.qSize);
                
                [X,Y]    = element.getNodes;
                                
                c = c(:,:,end);
                
                %potential = s.alpha/s.Cn * s.Cn/s.Cn_L * ( 1*(c.^3-3/2*c.^2+1/2*c) - 1*s.Cn_L^2*(cxx+cyy) );
                potential = s.alpha/s.Cn * ( 1*(c.^3-3/2*c.^2+1/2*c) - 1*s.Cn_L^2*(cxx+cyy) );
                
                lhs = potential;
                rhs = s.Cn_L/s.Cn * s.H{el};
                
                delta = lhs-rhs;
                
                surface(X(:,:,end),Y(:,:,end),delta(:,:,end));
                hold on;
            end
        end
        
        % Function to plot the mean curvature as computed from the C field
        function computeH(s,mesh,useMeanCurvature)
            % The C function is defined by: C = 0.5 + 0.5 * tanh( z / (2*sqrt(2)*Cn) )
            % From this function the half width parameter w is defined as w = 2*sqrt(2)*Cn
            % Furthermore, we define m = C * (C-1)
            
            if (nargin==1)
                mesh = s.mesh;
                useMeanCurvature = s.useMeanCurvature;
            elseif (nargin==2)
                useMeanCurvature = s.useMeanCurvature;
            end
            
            s.H = cell( numel(mesh.eRange),1 );
            
            if (useMeanCurvature)
                w = 2*sqrt(2) * s.Cn_L;
                C = s.fields{1};

                for e=mesh.eRange
                    element = mesh.getElement(e);
                    el = element.localID;

                    c   = C.val2(e,mesh);
                    cx  = C.x2(e,mesh);
                    cy  = C.y2(e,mesh);
                    cxx = C.xx2(e,mesh);
                    cyy = C.yy2(e,mesh);
                    cxy = C.xy2(e,mesh);

                    gradC = sqrt( cx.^2 + cy.^2 );

                    m = c .* (c-1);

                    H_enum  = -0.5*w * ( (2*s.Cn_L^2 * (cy).^2 + m.^2).*cxx ...
                                       + (2*s.Cn_L^2 * (cx).^2 + m.^2).*cyy ...
                                       -  4*s.Cn_L^2 * cx.*cy.*cxy ...
                                       -  2*m.*(c-0.5) .* (cx.^2+cy.^2) );
%                     H_enum  = -0.5*w * ( -(2*s.Cn_L^2 * (cx).^2).*cxx ...
%                                        - (2*s.Cn_L^2 * (cy).^2).*cyy ...
%                                        -  4*s.Cn_L^2 * cx.*cy.*cxy ...
%                                        -  2*m.*(c-0.5) .* (cx.^2+cy.^2) );
                    H_denom = ( 2*s.Cn_L^2 * ( cx.^2 + cy.^2 ) + m.^2 ) .^(3/2);

                    H = H_enum ./ H_denom;

                    meanCurvature = s.alpha * sqrt(2) * s.Cn_L .* gradC .* H;

                    s.H{el} = reshape( meanCurvature, element.finite.qSize );
                end
                
            else
                for e=1:numel(mesh.eRange)
                    s.H{e} = 0;
                end
            end
        end
        
        function computeG(s)
            % The C function is defined by: C = 0.5 + 0.5 * tanh( z / (2*sqrt(2)*Cn) )
            % From this function the half width parameter w is defined as w = 2*sqrt(2)*Cn
            % Furthermore, we define m = C * (C-1)
            
            w = 2*sqrt(2) * s.Cn_L;
            C = s.fields{1};
            
            s.G = cell( numel(s.mesh.eRange),1 );
            
            for e=s.mesh.eRange
                element = s.mesh.getElement(e);
                el = element.localID;
                
                c   = C.val('Current',e);                
                cx  = C.x('Current',e);
                cy  = C.y('Current',e);
                cxx = C.xx('Current',e);
                cyy = C.yy('Current',e);
                cxy = C.xy('Current',e);
                
                m = c .* (1-c);
%                 
%                 G_enum  = -4*w * ( (2*s.Cn_L^2 * ( cx.^2 .* cxx + cy.^2.*cyy + 2*cx.*cy.*cxy...
%                                    -  2*m.*(c-0.5) .* (cx.^2+cy.^2) );
%                 G_denom = ( 2*s.Cn_L^2 * ( cx.^2 + cy.^2 ) + m.^2 );
                
                G = G_enum ./ G_denom;
                
                meanCurvature = G_enum ./ G_denom;
                
                s.G{el} = reshape( meanCurvature, element.finite.qSize );
                
                %surface(X(:,:,end),Y(:,:,end),meanCurvature(:,:,end));
                %hold on;
            end
        end
        
        function out = getMeanKappa(s)
            s.computeH();
            
            out = cell(numel(s.H),1);
            
            for i=1:numel(s.H)
                out{i} = s.H{i}(:);
                %out{i} = s.alpha/(4*sqrt(2)) * s.Cn_L/s.Cn * s.H{i}(:);
            end
        end
        
        function [kx,ky] = getCurvature(s,mesh)
            s.computeH(mesh);
            
            kx = cell(numel(s.H),1);
            ky = cell(numel(s.H),1);
            
            C   = s.fields{1};
            Omg = s.fields{2};
            
            for e=mesh.eRange
                element = mesh.getElement(e);
                el = element.localID;
                
                Omgk    = Omg.val2(e,mesh);
                dCkdx   = C.x2(e,mesh);
                dCkdy   = C.y2(e,mesh);
                    
                kx{el} = (s.Cn/s.Cn_L * Omgk + s.H{el}(:)).*dCkdx;
                ky{el} = (s.Cn/s.Cn_L * Omgk + s.H{el}(:)).*dCkdy;
            end
        end
        
        function [dfr] = getDiffusiveFlowRate(s,mesh)
            if nargin==1
                mesh = s.mesh;
            end
            
            dfr = cell(numel(mesh.eRange),1);
            Omg = s.fields{2};
            
            for e=mesh.eRange
                element = mesh.getElement(e);
                el = element.localID;
                dfr{el} = 1/s.Pe * (s.Cn/s.Cn_L)*( Omg.xx2(e,mesh) + Omg.yy2(e,mesh) );
            end
        end
        
        function plotAll(s,fig,~,mesh)
            verbose = false;
            
            if nargin==3
                mesh = s.mesh;
            end
            
            %screensize = get( groot, 'Screensize' );
            %sizex = 1600;
            %sizey = 478;
            %set(fig,'position',[0.5*(screensize(3)-sizex) 0.5*(screensize(4)-sizey) sizex sizey]); 
            
            rows = 1;
            cols = s.Nvar+1;
            
            s.createInterface;

            
            defaultProps = {'FaceColor','interp','edgecolor','none'};
            %shading interp
                        
            %% Get the meshgrid for each element
            [xGrids,yGrids] = mesh.getMeshgrids;
%             
%             for e=1:mesh.numLocalElements
%                 element = mesh.getLocalElement(e);
%                 [X{e},Y{e}] = meshgrid(element.xelem,element.yelem);
%             end            
%             if (verbose)
%                 fprintf('Total time meshgrid   : %f\n',toc-t1)
%             end
            
            %% Extract interface     
%             t1          = toc;
%             var         = Variable.c;
%             dataVar     = data{var};
                        
%             C             = cell(mesh.numLocalElements,1);
%             s.interface.x = cell(mesh.numLocalElements,1);
%             s.interface.y = cell(mesh.numLocalElements,1);
%             %interface.z = cell(mesh.numLocalElements,1);
%             
%             % Find the contour where the C surface is equal to the
%             % requested interface value. Next, extract the x- and y-locations 
%             % from the contour matrix C. Interpolate on the C surface
%             % to find z-locations for the intersection. Finally plot the
%             % line with some properties.
%             for e=1:mesh.numLocalElements
%                 element = mesh.getLocalElement(e);
%                 C{e} = contourc(element.xelem,element.yelem,dataVar{e},[s.interface.value s.interface.value]);
%                 if (~isempty(C{e}))
% %                     s.interface.x{e} = C{e}(1, 2:1+C{e}(2,1));
% %                     s.interface.y{e} = C{e}(2, 2:1+C{e}(2,1));
%                     s.interface.x{e} = C{e}(1, 2:end);
%                     s.interface.y{e} = C{e}(2, 2:end);
%                     if C{e}(2,1)~=size(C{e},2)-1
%                        s.interface.x{e}(:, C{e}(2,1)+1 ) = [];
%                        s.interface.y{e}(:, C{e}(2,1)+1 ) = [];
%                        s.interface.x{e}(:, 1:C{e}(2,1) ) = fliplr( s.interface.x{e}(:, 1:C{e}(2,1) ) );
%                        s.interface.y{e}(:, 1:C{e}(2,1) ) = fliplr( s.interface.y{e}(:, 1:C{e}(2,1) ) );
%                     end
%                     %interface.z{e} = interp2(X{e}(1,:)', Y{e}(:,1), dataVar{e}, interface.x{e}, interface.y{e});
%                     %line(s.interface.x{e}, s.interface.y{e}, repmat(s.interface.value,size(s.interface.x{e})), s.interface.props{:});
%                 end
%             end
%             if (verbose)
%                 fprintf('Total time interface : %f\n',toc-t1)
%             end
            
            %% C plot
            position    = 1;
            sub         = subplot(rows,cols,position);
            cla(sub);
            s.plotVar(Variable.c);
            
%             t1          = toc;
%             var         = Variable.c;
%             dataVar     = data{var};
%             minC        = min( cellfun(@(x) min(x(:)),dataVar) );
%             maxC        = max( cellfun(@(x) max(x(:)),dataVar) );
%             
%             % plot settings
%             delete(subplot(rows,cols,var))
%             subplot(rows,cols,var)
%             xlim([mesh.x0 mesh.x1])
%             ylim([mesh.y0 mesh.y1])
%             pbaspect([pbx pby 1])
%             view([0,90])
%             grid off;
%             hold on;
%             
%             for e=1:mesh.numLocalElements
%                 surface(X{e},Y{e},dataVar{e},defaultProps{:});
%                 if s.interface.active && ~isempty(s.interface.x{e})
%                     line(s.interface.x{e}, s.interface.y{e}, repmat(maxC,size(s.interface.x{e})), s.interface.props{:});
%                 end
%             end
%             if (verbose)
%                 fprintf('Total time C   : %f\n',toc-t1)
%             end

            %% Omega plot
            position    = 2;
            sub         = subplot(rows,cols,position);
            cla(sub);
            s.plotVar(Variable.omg);
            
            
%             t1          = toc; 
%             var         = Variable.omg;
%             dataVar     = data{var};
%             maxOmega    = max( cellfun(@(x) max(x(:)),dataVar) );
%             
%             delete(subplot(rows,cols,var))
%             subplot(rows,cols,var)            
%             xlim([mesh.x0 mesh.x1])
%             ylim([mesh.y0 mesh.y1])
%             grid off;
%             pbaspect([pbx pby 1])
%             view([0,90])
%             hold on;
%             
%             for e=1:mesh.numLocalElements
%                 surface(X{e},Y{e},dataVar{e},defaultProps{:});                
%                 if interface.active && ~isempty(interface.x{e})
%                     line(interface.x{e}, interface.y{e}, repmat(maxOmega,size(interface.x{e})),interface.props{:});
%                 end
%             end
%             if (verbose)
%                 fprintf('Total time Omg : %f\n',toc-t1)
%             end
            
            %% Surface tension plot
            t1      = toc;
            var     = 3;
            delete(subplot(rows,cols,var))
            s.plotSurfaceTension( subplot(rows,cols,var), s.interface );
            if (verbose)
                fprintf('Total time ST  : %f\n',toc-t1)
            end
            
%             dataVar = data{var};
%             for e=1:s.mesh.getNumLocalElements
%                 element = s.mesh.getLocalElement(e);
%                 surf(element.xelem,element.yelem,dataVar{e});
%                 hold on;
%             end
%             shading interp
%             xlim([s.mesh.x0 s.mesh.x1])
%             ylim([s.mesh.y0 s.mesh.y1])
%             grid off;
%             pbaspect([pbx pby 1])
%             view([0,90])
            
%             % c-plot
%             delete(subplot(2,1,1))
%             subplot(2,1,1)
%             c = data{1};
%             for e=1:s.mesh.getNumLocalElements
%                 element = s.mesh.getLocalElement(e);
%                 surf(element.xelem,element.yelem,c{e});
%                 hold on;
%             end
%             xlim([s.mesh.x0 s.mesh.x1])
%             view([30,30])
%             %view([0,90])
            
%             % omg-plot
%             delete(subplot(2,1,2))
%             subplot(2,1,2)
%             omg = data{2};
%             for e=1:s.mesh.getNumLocalElements
%                 element = s.mesh.getLocalElement(e);
%                 surf(element.xelem,element.yelem,omg{e});
%                 hold on;
%             end
%             view([30,30])
%             %view([0,90])
            
%             % P-plot
%             delete(subplot(3,2,5))
%             subplot(3,2,5)
%             p = data{3};
%             for e=1:s.mesh.getNumLocalElements
%                 element = s.mesh.getLocalElement(e);
%                 surf(element.xelem,element.yelem,p{e});
%                 hold on;
%             end
%             view([30,30])
%             %view([0,90])
%             
%             % U-velocity at x=0.5
%             delete(subplot(3,2,2))
%             subplot(3,2,2)
%             for e=1:s.mesh.getNumLocalElements
%                 element = s.mesh.getLocalElement(e);
%                 z=interp2(element.xelem,element.yelem,u{e},0.5,element.yelem);
%                 plot(element.yelem,z,'k');
%                 hold on;
%             end
%             xlim([0 1])
%             view([90,-90])
%             
%             % V-velocity at x=0.5
%             delete(subplot(3,2,4))
%             subplot(3,2,4);
%             for e=1:s.mesh.getNumLocalElements
%                 element = s.mesh.getLocalElement(e);
%                 z=interp2(element.xelem,element.yelem,v{e},0.5,element.yelem);
%                 plot(element.yelem,z,'k');
%                 hold on;
%             end
%             ylim([-1e-3 1e-3])
%             view([90,-90]) 

            %% Position subfigures
            d = 0.01;
            
            width  = (1-(cols+1)*2*d)/cols;
            height = 1;
            
            s1 = subplot(rows,cols,1);
            set(s1,'position',[2*d 2*d width height])
            
            s2 = subplot(rows,cols,2);
            set(s2,'position',[4*d+width 0.02 width height])
            
            s3 = subplot(rows,cols,3);
            set(s3,'position',[6*d+2*width 0.02 width height])
        end
        
        % Main function which calls other functions
        function writeValues(s)
            filename = [ System.settings.outp.directory s.name '-' System.settings.outp.valueFilename '.txt' ];
            
            perimeter = s.computePerimeter2D;
            s.computeMass(System.getIntegrationDisc);
            s.computeCenterOfMass(false);
            
            s.computeCenterOfMassVelocity(false);
            
            if (System.rank==0)
                if (System.settings.time.current==0)
                    header = {'iter','time','totalMass','massLossTotal','cmx_total','cmy_total','cmu_total','cmv_total',...
                                            'enclosedMass','massLossEnclosed','cmx_enclosed','cmy_enclosed','cmu_enclosed','cmv_enclosed',...
                                            'perimeter'};
                    System.writeHeader(filename,header)
                end

                values = zeros(15,1);
                values(1)  = System.settings.time.iter; 
                values(2)  = System.settings.time.current;
                values(3)  = s.massTotal(end);
                values(4)  = s.massLossTotal;
                values(5)  = s.cm_total(end,1);
                values(6)  = s.cm_total(end,2);
                values(7)  = s.cmv_total(end,1);
                values(8)  = s.cmv_total(end,2);
                values(9)  = s.massEnclosed(end);
                values(10) = s.massLossEnclosed;
            	values(11) = s.cm_enclosed(end,1);
                values(12) = s.cm_enclosed(end,2);
                values(13) = s.cmv_enclosed(end,1);
                values(14) = s.cmv_enclosed(end,2);
                values(15) = perimeter;

                System.appendToFile(filename,values)
            end
        end
        
        function out = computeOmega(s)
            % Fenergy = (3*Ck.^2 - 3*Ck + 0.5).*H - s.Cn^2 * ( Nabla2 );
            clf;
                        
            layer = 1;
            
            alphaC = s.fields{1}.active.alpha;
            
            out = cell( numel(s.mesh.eRange),1 );
            
            for e=s.mesh.eRange
                element = s.mesh.getElement(e);
                el = element.localID;
                
                %Ck = C.val(FieldType.Current,e);
                Ck = s.fields{1}.value{el}(:);
                Ze = s.mesh.Z{e};
                Nabla2 = element.getDxx + element.getDyy;
                data = (Ck.^3 - 3/2*Ck.^2 + 0.5.*Ck) - s.Cn_L^2 * ( Nabla2 * Ze * alphaC{el}(:) );
                data = s.alpha/s.Cn * data;
                
                out{el} = reshape(data,element.finite.qSize);
                
                %[X,Y] = element.getNodes;
                %if (layer==1)
                %    plotData = data(:,:,end);
                %elseif (layer==0)
                %    plotData = data(:,:,1);
                %end
                %surf(X(:,:,:,end),Y(:,:,:,end),plotData,'FaceColor','interp');
                %hold on;
            end
            
%             x0 = s.mesh.X(1,1);
%             x1 = s.mesh.X(2,1);
%             y0 = s.mesh.X(1,2);
%             y1 = s.mesh.X(2,2);
%             
%             pbx = x1 - x0;
%             pby = y1 - y0;
%             pbaspect([pbx pby 1])
%                
%             colorbar;
%             view([25,25]);
        end
        
        % Function to compute the perimeter of the C=0.5 patch
        function out = computePerimeter2D(s)
            
            length = zeros(2,1);
            
            for e=s.mesh.eRange
                
                % Set the element
                element = s.mesh.getElement(e);
                
                [x,y,z,t] = element.getNodes();
                
                x = squeeze(x);
                y = squeeze(y);
                t = squeeze(t);
                
                c = s.fields{1};
                
                try
                    cValues = reshape( c.val(FieldType.Current,e), size(x) );
                catch
                    disp('')
                end
                
                for l=1:2
                    
                    if l==1
                        pos = 1;
                    else
                        pos = size(x,3);
                    end
                    
                    % 3D
                    %p = patch(isosurface(x(:,:,l),y(:,:,l),t(:,:,l),cValues(:,:,l),0.5));
                    %verts = get(p, 'Vertices');
                    %faces = get(p, 'Faces');
                    
                    % 2D
                    % create a contour object for C = 0.5
                    %p = contour(x(:,:,pos),y(:,:,pos),cValues(:,:,pos),[0.5 0.5]);
                    
                    p = contourc(x(:,1,pos),y(1,:,pos),cValues(:,:,pos),[0.5 0.5]);
                    
                    % use the contour object to compute the linear line length 
                    % for each segment (p(1,1)==0.5 and p(2,1)=number of segments)
                    if (~isempty(p))
                        diff = zeros(2,p(2,1)-1);
                        for n=1:p(2,1)-1
                            diff(1,n) = p(1,n+2) - p(1,n+1);
                            diff(2,n) = p(2,n+2) - p(2,n+1);
                            diff(3,n) = sqrt(diff(1,n)^2 + diff(2,n)^2);
                        end
                        length(l) = length(l) + sum(diff(3,:));
                    end
                end
            end
            
            
            
            length = NMPI_Allreduce(length,numel(length),'+',Parallel3D.MPI.mpi_comm_world);
            
            out = length(end);
            
            %fprintf('perimeter start / end : %e / %e\n',length)
                
%             [x,y,z,v] = flow;
%             p = patch(isosurface(x,y,z,v,-3));
%             isonormals(x,y,z,v,p)
%             set(p,'FaceColor','red','EdgeColor','none');
%             daspect([1 1 1])
%             view(3); 
%             camlight 
%             lighting gouraud
%             verts = get(p, 'Vertices');
%             faces = get(p, 'Faces');
%             a = verts(faces(:, 2), :) - verts(faces(:, 1), :);
%             b = verts(faces(:, 3), :) - verts(faces(:, 1), :);
%             c = cross(a, b, 2);
%             area = 1/2 * sum(sqrt(sum(c.^2, 2)));
%             fprintf('\nThe surface area is %f\n\n', area);
        end
        
        % Function to compute the total and enclosed mass.
        % The total mass is a simple summation over all elements, and
        % should remain constant in time. The enclosed mass is the mass
        % where C >= 0.5, and its value can change in time.
        function computeMass(s,finiteElement)
            if (nargin==1)
                finiteElement = FECollection.get(1);
            end
            
            if (finiteElement.spaceTime)
                Qt = finiteElement.Q( end );
            end
            
            % Reset the total/enclosed mass for each time level
            massTotal    = zeros(Qt,1);
            massEnclosed = zeros(Qt,1);
            check        = zeros(Qt,1);
            
            for e=s.mesh.eRange
                
                % Set the element
                element = s.mesh.getElement(e);
                
                %[X,Y,Z,T] = element.getNodes;
                
                % Construct the total and enclosed c arrays
                cTotal    = s.fields{1}.getValueInterpolated(e,finiteElement,s.mesh);
                
                cEnclosed = cTotal;
                cEnclosed(cEnclosed< 0.5) = 0;
                cEnclosed(cEnclosed>=0.5) = 1;
                
                % Set the integration weights and Jacobian for 2D spatial planes
                w = finiteElement.Wspatial;
                J = element.JSpatial;
      
                for t=1:Qt
                    sliceValues     = cTotal(:,:,t);
                    massTotal(t)    = massTotal(t)    + sliceValues(:)' * w * J;
                    
                    sliceValues     = cEnclosed(:,:,t);
                    massEnclosed(t) = massEnclosed(t) + sliceValues(:)' * w * J;
                    
                    %sliceValues     = ones( size(cEnclosed(:,:,t)) );
                    %check(t)        = check(t) + sliceValues(:)' * w * J;
                end
            end
            
            if (System.settings.anal.debug)
                fprintf('Before NMPI_Allreduce in CahnHilliard.computeMass\n')
            end
            
            s.massTotal     = NMPI_Allreduce(massTotal   ,Qt,'+',Parallel3D.MPI.mpi_comm_world);
            s.massEnclosed  = NMPI_Allreduce(massEnclosed,Qt,'+',Parallel3D.MPI.mpi_comm_world);
            checkGlobal     = NMPI_Allreduce(check,Qt,'+',Parallel3D.MPI.mpi_comm_world);
            
            s.massLossTotal     = s.massTotal(1)   -s.massTotal(end);
            s.massLossEnclosed  = s.massEnclosed(1)-s.massEnclosed(end);
            checkLoss           = check(1)-check(end);
            
            %if (Parallel3D.MPI.rank==0)
            %    fprintf('Total mass (begin/end)    : %f / %f (mass loss = %8.2e)\n',s.massTotal(1),s.massTotal(end),s.massLossTotal);
            %    fprintf('Enclosed mass (begin/end) : %f / %f (mass loss = %8.2e)\n',s.massEnclosed(1),s.massEnclosed(end),s.massLossEnclosed);
            %    %fprintf('Enclosed mass (begin/end) : %f / %f (mass loss = %8.2e)\n',check(1),check(end),checkLoss);
            %end
        end
        
        % The center of mass (CoM) can be determined by:
        %   CoM = sum( C * distance ) / sum( C )
        % for each spatial direction. Here 'distance' is a value wrt the
        % origin of the 
        function computeCenterOfMass(s,computeMass,Q)
            if (nargin==2)
                disc = System.getIntegrationDisc;
            else
                error('unsupported option')
                %disc = LSQdisc_5(System.settings.stde.Pn,Q,System.settings.stde.C,System.settings.stde.sDim,System.settings.time.spaceTime);
            end
            
            Qs = disc.Q(1)*disc.Q(2);       % Spatial Q
            Qt = disc.Q(3);                 % Temporal Q
            
            sDim = 2;
            
            % Reset the cm(x,y) for each time level
            cm_total    = zeros(Qt,sDim);
            cm_enclosed = zeros(Qt,sDim);

            for e=s.mesh.eRange
                % Set the element & locations of the quadrature points
                element     = s.mesh.getElement(e);
                locations   = element.getLocations(disc);
                
                xLoc = superkron( ones(numel(locations{3}),1), ones(numel(locations{2}),1), locations{1} );
                yLoc = superkron( ones(numel(locations{3}),1), locations{2}, ones(numel(locations{1}),1) );
                
                % Get the values for the modified integration order
                cTotal = s.fields{1}.getValueInterpolated( e ,disc, s.mesh);
                
                cEnclosed = cTotal; 
                cEnclosed(cEnclosed<0.5)  = 0;
                cEnclosed(cEnclosed>=0.5) = 1;
                
                % Set the integration weights and Jacobian for 2D spatial planes
                w = disc.Wspatial;
                J = element.J(1)*element.J(2);
      
                % Compute the mass * distance
                cmx_total = cTotal(:) .* xLoc; cmx_total = reshape(cmx_total,[Qs,Qt]);
                cmy_total = cTotal(:) .* yLoc; cmy_total = reshape(cmy_total,[Qs,Qt]);
                
                % Compute the mass * distance
                cmx_enclosed = cEnclosed(:) .* xLoc; cmx_enclosed = reshape(cmx_enclosed,[Qs,Qt]);
                cmy_enclosed = cEnclosed(:) .* yLoc; cmy_enclosed = reshape(cmy_enclosed,[Qs,Qt]);
                
                % Integrate the values for each element and sum to get the
                % total values on this rank
                for t=1:Qt
                    cm_total(t,1) = cm_total(t,1) + cmx_total(:,t)' * w * J;               % x-direction
                    cm_total(t,2) = cm_total(t,2) + cmy_total(:,t)' * w * J;               % y-direction
                    
                    cm_enclosed(t,1) = cm_enclosed(t,1) + cmx_enclosed(:,t)' * w * J;               % x-direction
                    cm_enclosed(t,2) = cm_enclosed(t,2) + cmy_enclosed(:,t)' * w * J;               % y-direction
                end
            end
            
            if (System.settings.anal.debug)
                fprintf('Before NMPI_Allreduce in CahnHilliard.computeCenterOfMass\n')
            end
            
            cm_total = NMPI_Allreduce(cm_total,numel(cm_total),'+',Parallel3D.MPI.mpi_comm_world);
            cm_total = reshape(cm_total,[Qt,sDim]);
            
            cm_enclosed = NMPI_Allreduce(cm_enclosed,numel(cm_enclosed),'+',Parallel3D.MPI.mpi_comm_world);
            cm_enclosed = reshape(cm_enclosed,[Qt,sDim]);            
            
            % Compute the mass for each time plane
            if (computeMass)
                s.computeMass(disc);
            end
            
            % Finally compute the cm(x,y) for each time plane
            s.cm_total = cm_total ./ s.massTotal;
            
            % Finally compute the cm(x,y) for each time plane
            s.cm_enclosed = cm_enclosed ./ s.massEnclosed;
            
%             if (Parallel3D.MPI.rank==0)
%                 %fprintf('Total mass : %f (effective radius : %f)\n',s.mass,sqrt(s.mass/pi))
%                 fprintf('Center of mass (begin/end) : (%f,%f / %f,%f) -- delta : (%8.2e,%8.2e) \n',s.cm(1,1),s.cm(1,2),s.cm(end,1),s.cm(end,2),s.cm(end,1)-s.cm(1,1),s.cm(end,2)-s.cm(1,2))
%             end
        end
        
        % The center of mass velocity (CoM_vel) can be determined by:
        %   CoM_vel = sum( C * U ) / sum( C )
        % for each spatial direction. Here 'U' is the velocity vector
        function computeCenterOfMassVelocity(s,varargin)
            
            [computeMass,overIntegration] = VerifyInput(varargin,{true,true});
            
            U = FieldCollection.get('u');
            V = FieldCollection.get('v');
            %save(['com1.' int2str(System.rank) '.mat']);
            
            remesh = ~isequal(U.mesh.Lv,s.mesh.Lv);
            
            if (remesh)
                U.initSubField(FieldType.Interpolated,s.getMesh)
                V.initSubField(FieldType.Interpolated,s.getMesh)
            end
                        
            if (overIntegration)
                disc = System.getIntegrationDisc;
            %else
            %    error('unsupported option')
            %    %disc = LSQdisc_5(System.settings.stde.Pn,Q,System.settings.stde.C,System.settings.stde.sDim,System.settings.time.spaceTime);
            end
            
            sDim = 2;
            count = 0;
            
            %maxV = 0;
            
            for e=s.mesh.eRange
                count = count+1;
                                
                % Set the element & locations of the quadrature points
                element     = s.mesh.getElement(e);

                if (~overIntegration)
                    disc = element.finite;
                end
                
                Qs = disc.Q(1)*disc.Q(2);       % Spatial Q
                Qt = disc.Q(3);                 % Temporal Q
                
                if count==1
                    % Reset the cm(x,y) for each time level
                    cm_total    = zeros(Qt,sDim);
                    cm_enclosed = zeros(Qt,sDim);
                end
                
                if (remesh)
                    udata = U.getValueInterpolated(e,disc,s.mesh);
                    vdata = V.getValueInterpolated(e,disc,s.mesh);
                else
                    udata = U.getValueInterpolated(e,disc);
                    vdata = V.getValueInterpolated(e,disc);
                end
                
                % Get the values for the modified integration order
                cTotal = s.fields{1}.getValueInterpolated(e,disc);
                
                cEnclosed = cTotal;
                cEnclosed(cEnclosed<0.5)  = 0;
                cEnclosed(cEnclosed>=0.5) = 1;
                
                % Set the integration weights and Jacobian for 2D spatial planes
                w = disc.Wspatial;
                J = element.J(1)*element.J(2);
                
                % Compute the mass * distance
                cmx_total = cTotal(:) .* udata(:); cmx_total = reshape(cmx_total,[Qs,Qt]);
                cmy_total = cTotal(:) .* vdata(:); cmy_total = reshape(cmy_total,[Qs,Qt]);
                
                cmx_enclosed = cEnclosed(:) .* udata(:); cmx_enclosed = reshape(cmx_enclosed,[Qs,Qt]);
                cmy_enclosed = cEnclosed(:) .* vdata(:); cmy_enclosed = reshape(cmy_enclosed,[Qs,Qt]);
                
                %maxV = max(maxV,max(vdata(:)));
                
                % Integrate the values for each element and sum to get the
                % total values on this rank
                for t=1:Qt
                    cm_total(t,1) = cm_total(t,1) + cmx_total(:,t)' * w * J;               % x-direction
                    cm_total(t,2) = cm_total(t,2) + cmy_total(:,t)' * w * J;               % y-direction
                    
                    cm_enclosed(t,1) = cm_enclosed(t,1) + cmx_enclosed(:,t)' * w * J;               % x-direction
                    cm_enclosed(t,2) = cm_enclosed(t,2) + cmy_enclosed(:,t)' * w * J;               % y-direction
                end
            end
            
            if (System.settings.anal.debug)
                fprintf('Before NMPI_Allreduce in CahnHilliard.computeCenterOfMassVelocity\n')
            end
            
            cm_total = NMPI_Allreduce(cm_total,numel(cm_total),'+',Parallel3D.MPI.mpi_comm_world);
            cm_total = reshape(cm_total,[Qt,sDim]);
            
            cm_enclosed = NMPI_Allreduce(cm_enclosed,numel(cm_enclosed),'+',Parallel3D.MPI.mpi_comm_world);
            cm_enclosed = reshape(cm_enclosed,[Qt,sDim]);
            
            % Compute the mass for each time plane
            if (computeMass)
                s.computeMass(disc);
            end
            
            if (isfield(System.settings.phys,'bubbleVelocity'))
                goalVelocity = repmat(System.settings.phys.bubbleVelocity,size(cm_total,1),1);
            else
                goalVelocity = 0;
            end
            
            % Finally compute the cm(x,y) for each time plane
            s.cmv_total     = cm_total    ./ s.massTotal    + goalVelocity;
            s.cmv_enclosed  = cm_enclosed ./ s.massEnclosed + goalVelocity;
            
%             if (Parallel3D.MPI.rank==0)
%                 %fprintf('Total mass : %f (effective radius : %f)\n',s.mass,sqrt(s.mass/pi))
%                 fprintf('rank %d : %f %f\n',Parallel3D.MPI.rank,max(s.cmv_total(:)),max(s.cmv_enclosed(:)))
%                 if (Parallel3D.MPI.rank==0)
%                     fprintf('=======================\n')
%                 end
                %fprintf('Center of mass velocity (begin/end) : (%f,%f / %f,%f) -- delta : (%8.2e,%8.2e) \n',s.cmv(1,1),s.cmv(1,2),s.cmv(end,1),s.cmv(end,2),s.cmv(end,1)-s.cmv(1,1),s.cmv(end,2)-s.cmv(1,2))
%             end
        end
        
        function computeMeanOmega(s,disc)
            if (nargin==1)
                disc = s.getMesh.getElement(1).finite;
            end
            
            Qt = disc.Q(3);
            
            % Reset the mass for each time level
            meanValues  = zeros(Qt,1);
            volume      = zeros(Qt,1);
            intGradC    = zeros(Qt,1);
               
            for e=s.mesh.eRange
                
                % Set the element
                element = s.mesh.getElement(e);
                
                % Get the values for the modified integration order
                values = s.omgField.getValueInterpolated(e,disc,s.mesh);
                
                Cx = s.cField.getDxInterpolated(e,disc,s.mesh);
                Cy = s.cField.getDyInterpolated(e,disc,s.mesh);
                
                gradC = sqrt( Cx.^2 + Cy.^2 );
                
%                 values(values<0.5) = 0;
                
                % Set the integration weights and Jacobian for 2D spatial planes
                wxy  = element.disc.Wspatial;
                J    = element.J(1)*element.J(2);
      
                for t=1:Qt
                    xyValues        = values(:,:,t);
                    meanValues(t)	= meanValues(t) + xyValues(:)' * wxy / J;
                    volume(t)       = volume(t) + sum(wxy) / J;
                    
                    gradCValues     = gradC(:,:,t);
                    intGradC(t)     = intGradC(t) + gradCValues(:)' * wxy / J;
                end
                
%                 % Space
% %                 data = C{e};
%                 data = C.getValueInterpolated(e,disc);
%                 element = s.mesh.getLocalElement(e);
%                 wxy = kron(element.disc.wy,element.disc.wx);
%                 if position == 0
%                     % time level 0 of element
%                     mass(1) = mass(1) + data(1:(element.disc.Qx*element.disc.Qy))' * wxy / (element.J(1)*element.J(2)) ;
%                 else
%                     mass(1) = mass(1) + data((element.disc.Qz-1)*(element.disc.Qx*element.disc.Qy)+1:element.disc.Qz*(element.disc.Qx*element.disc.Qy))' * wxy / (element.J(1)*element.J(2)) ;
%                 end
%                 
%                 % SpaceTime
%                 data = C{e};                
%                 element = s.mesh.getLocalElement(e);
%                 J = prod( element.J );
%                 wxyz = superkron(element.disc.wz,element.disc.wy,element.disc.wx);
%                 mass(2) = mass(2) + data' * wxyz / J;
            end
            
            if (System.settings.anal.debug)
                fprintf('Before NMPI_Allreduce in CahnHilliard.computeMass\n')
            end
            
            s.meanOmega     = NMPI_Allreduce(meanValues,Qt,'+',Parallel3D.MPI.mpi_comm_world);
            s.totalVolume   = NMPI_Allreduce(volume,Qt,'+',Parallel3D.MPI.mpi_comm_world);
            s.intGradC      = NMPI_Allreduce(intGradC,Qt,'+',Parallel3D.MPI.mpi_comm_world);
            
%             s.massLoss = s.mass(1)-s.mass(end);
            
%             if (Parallel3D.MPI.rank==0)
%                 fprintf('Mean omega (begin/end)             : %f / %f\n',s.meanOmega(1)/s.totalVolume(1),s.meanOmega(end)/s.totalVolume(end));
%                 fprintf('Mean curvature (begin/end)         : %f / %f\n',s.meanOmega(1)/s.intGradC(1),s.meanOmega(end)/s.intGradC(end));
%                 fprintf('Mean curvature - 2*pi (begin/end)  : %f / %f\n',2*pi/s.intGradC(1),2*pi/s.intGradC(end));
%             end
            
            s.kappa = (2*pi) / mean(s.intGradC);
        end
        
        % Only useful for cField
        function plotPotential(s)
            C = s.fields{1};
            
            for e=1:s.mesh.numElements
                element = s.mesh.getElement(e);
                c      = reshape(C.getValue(e),element.finite.qSize);
                cxx    = reshape(C.xx(FieldType.Current,e),element.finite.qSize);
                cyy    = reshape(C.yy(FieldType.Current,e),element.finite.qSize);
                
                [X,Y]    = element.getNodes;
                                
                c = c(:,:,end);
                
                %potential = s.alpha/s.Cn * s.Cn/s.Cn_L * ( 1*(c.^3-3/2*c.^2+1/2*c) - 1*s.Cn_L^2*(cxx+cyy) );
                potential = (s.alpha/s.Cn+1) * ( 1*(c.^3-3/2*c.^2+1/2*c) - 0*s.Cn_L^2*(cxx+cyy) );
                
                surf(X(:,:,end),Y(:,:,end), potential(:,:,end));
                
                hold on;
            end
                        
            colorbar;
            view([25,25]);
%             title(titleString);
        end
        
        % Only useful for cField
        function plotMeanOmega(s)
            C = s.fields{1};
            
            for e=1:s.mesh.numElements
                element = s.mesh.getElement(e);
                c      = reshape(C.getValue(e),element.finite.qSize);
                cx     = reshape(C.x(FieldType.Current,e),element.finite.qSize);
                cy     = reshape(C.y(FieldType.Current,e),element.finite.qSize);
                cxx    = reshape(C.xx(FieldType.Current,e),element.finite.qSize);
                cyy    = reshape(C.yy(FieldType.Current,e),element.finite.qSize);
                cxy    = reshape(C.xy(FieldType.Current,e),element.finite.qSize);
                
                gradC2 = cx.^2+cy.^2;
                
                [X,Y]    = element.getNodes;
                                
                %c = c(:,:,end);
                
                %potential = s.alpha/s.Cn * s.Cn/s.Cn_L * ( 1*(c.^3-3/2*c.^2+1/2*c) - 1*s.Cn_L^2*(cxx+cyy) );
                
                %meanOmega = (s.alpha/s.Cn) * ( (c.^3-3/2*c.^2+1/2*c).*sqrt(gradC2) - 1*s.Cn_L^2.*(cx.^2.*cxx + 2*cx.*cy.*cxy + cy.^2.*cyy)./sqrt(gradC2) );
                meanOmega = (s.alpha/s.Cn) * ( (c.^3-3/2*c.^2+1/2*c) - 1*s.Cn_L^2.*(cx.^2.*cxx + 2*cx.*cy.*cxy + cy.^2.*cyy)./(gradC2) );
                
                surf(X(:,:,end),Y(:,:,end), meanOmega(:,:,end));
                
                hold on;
            end
                        
            colorbar;
            view([25,25]);
%             title(titleString);
        end
        
        function plotLS(s)
            C = FieldCollection.get('c');
            
            for e=s.mesh.eRange
                element = s.mesh.getElement(e);

                Ck      = C.val(FieldType.Current,e);
                
                [x,y]   = element.getNodes;
                
                x = squeeze(x);
                y = squeeze(y);
                 
                LS = 2*s.alpha/3 * s.Cn_L * log( abs( Ck./(1-Ck) ) );
                
                LS = reshape( LS, size(x) );
                
                LS_exact = 0 * (0.5 - sqrt( (x-1).^2 + (y-1).^2 ) );
                
                hold on;
                surface(x(:,:,end),y(:,:,end),LS(:,:,end)-LS_exact(:,:,end))
            end
        end
        
        function plotCurvature(s)
            
%             if nargin<3
%                 s.createInterface;
%                 interface = s.interface;
%             end

            curvature = cell(s.mesh.numLocalElements,1);
            
            C   = s.getField(1,s); %.current;
            Omg = s.getField(2,s);  %.current;
            
            mesh = C.mesh;
            
            [kx,ky] = s.getCurvature(s.mesh);
            
            for el=1:mesh.numLocalElements
                
                %e = mesh.getLocalElement(el).globalID;
                %Ck      = C.val(FieldType.Current,e);
                %dCkdx   = C.x(FieldType.Current,e);
                %dCkdy   = C.y(FieldType.Current,e);
                %Omgk    = Omg.val(FieldType.Current,e);
                %curvature{el} = Omgk./ (s.Cn * s.alpha * sqrt(dCkdx.^2+dCkdy.^2));
                %curvature{el}( (abs(curvature{el})>100) ) = 0;
                
                curvature{el} = sqrt( kx{el}.^2 + ky.^2 );
            end
                        
            %pbx = mesh.x1 - mesh.x0;
            %pby = mesh.y1 - mesh.y0;

            %xlim([mesh.x0 mesh.x1])
            %ylim([mesh.y0 mesh.y1])
            
            grid off;
            %pbaspect([pbx pby 1])

            shading interp
            view([0,90])
            
            hold on;
            
            X = cell(mesh.numLocalElements,1);
            Y = cell(mesh.numLocalElements,1);
            curv = cell(mesh.numLocalElements,1);

            %% Create a meshgrid
            %for e=1:mesh.numLocalElements
            %    [X{e},Y{e}] = meshgrid( mesh.getLocalElement(e).xelem,mesh.getLocalElement(e).yelem );
            %end
            
            %% Compute the surface tension data
            for e=1:mesh.numLocalElements
                temp = curvature{e};
                temp = reshape(temp,mesh.getLocalElement(e).finite.qSize);
                curv{e} = temp; %(:,:,end);
            end
            
            maxST = max( cellfun(@(x) max(x(:)),curv) );
            
            % Integration of the surface tension
            for e=1:mesh.numLocalElements
                element = mesh.getElement(e);
                %wxy = kron(element.disc.wy,element.disc.wx);
                %intST = intST + data((element.disc.Qz-1)*(element.disc.Qx*element.disc.Qy)+1:element.disc.Qz*(element.disc.Qx*element.disc.Qy))' * wxy / (element.J(1)*element.J(2)) ;
                %intST = intST + surfaceTension{e}(:) .* wxy/(element.J(1)*element.J(2));

                [X,Y] = element.getNodes;
                X = squeeze(X);
                Y = squeeze(Y);
                
                surface(X(:,:,end),Y(:,:,end),curv{e}(:,:,end));
                
                %if interface.active && ~isempty(interface.x{e})
                %    line(interface.x{e}, interface.y{e}, repmat(maxST,size(interface.x{e})), interface.props{:});
                %end
            end
        end
        
        % The surface energy can be determined from the Helmholtz free 
        % energy density model, which in the Cahn-Hilliard model is the 
        % sum of the local Helmholtz energy and surface tension effects.
        function out = computeSurfaceEnergy(s)

            
            
%             if (nargin==1)
%                 activeField = FieldType.Current;
%             end

            % Set the Fields
            C   = s.getField(1,s);
            
            % Get the mesh (should be same for both fields!)
            mesh = C.mesh;
            
            numElements = mesh.numLocalElements;
           
            % Initialize psi and gradC for all elements
            psi   = cell(numElements,1);
            gradC = cell(numElements,1);
           
            for el=1:numElements
                e = mesh.getElement(el).globalID;
                
                Ck          = C.val(e);
                dCkdx       = C.x(e);
                dCkdy       = C.y(e);
                
                psi{e}      = 0.25 * Ck.^2 .* (1-Ck).^2;
                gradC{e}    = dCkdx.^2+dCkdy.^2;
            end

            surfaceEnergy = 0;
            
            for e=1:numElements
                element = mesh.getLocalElement(e);
                
                data = 0.5*s.Cn .* gradC{e} + psi{e} ./ s.Cn;
                
                % Set the integration weights and Jacobian for 2D spatial planes
                w = finiteElement.Wspatial;
                J = element.JSpatial;
                
                surfaceEnergy = surfaceEnergy + data((element.disc.Qz-1)*(element.disc.Qx*element.disc.Qy)+1:element.disc.Qz*(element.disc.Qx*element.disc.Qy))' * wxy / (element.J(1)*element.J(2)) ;
            end
            
            surfaceEnergy = surfaceEnergy * s.alpha;

            if (System.settings.anal.debug)
                fprintf('Before NMPI_Allreduce in CahnHilliard.computeSurfaceEnergy\n')
            end
            
%             fprintf('Max of surface tension : %f\n',stMax)
            surfaceEnergy = NMPI_Allreduce(surfaceEnergy,1,'+',Parallel3D.MPI.mpi_comm_world);
%             if (Parallel3D.MPI.rank==0)
%                 fprintf('Total surface energy  : %f\n',surfaceEnergy)
%             end
            
            out = surfaceEnergy;
        end
        
        function computeSurfaceTension(s,activeField)

            %if (nargin==1)
                finiteElement = FECollection.get(1);
            %end
            
            if (nargin==1)
                activeField = FieldType.Current;
            end

            % Set the Fields
            C   = s.getField(1,s);
            Omg = s.getField(2,s);
            
            % Get the mesh (should be same for both fields!)
            mesh = C.mesh;
            
            numElements = mesh.numLocalElements;
            
            fx = cell(numElements,1);
            fy = cell(numElements,1);
            
            if (finiteElement.spaceTime)
                Qt = finiteElement.Q( end );
            else
                Qt = 1;
            end
            
            % Compute the surface tension per element
            for el=1:numElements
                e = mesh.getElement(el).globalID;
                
                Ck      = C.val(FieldType.Current,e);
                dCkdx   = C.x(FieldType.Current,e);
                dCkdy   = C.y(FieldType.Current,e);
                Omgk    = Omg.val(FieldType.Current,e);

                fx{e} = Omgk.*dCkdx;
                fy{e} = Omgk.*dCkdy;
            end

            % Reset the total/enclosed mass for each time level
            surfaceTension = zeros(Qt,1);
            
            for e=1:numElements
                element = mesh.getLocalElement(e);
                
                data = sqrt(fx{e}.^2+fy{e}.^2);
                data = reshape( data, element.finite.qSize );
                
                % Set the integration weights and Jacobian for 2D spatial planes
                w = finiteElement.Wspatial;
                J = element.JSpatial;

                for t=1:Qt
                    sliceValues = data(:,:,t);
                    surfaceTension(t) = surfaceTension(t) + dot( sliceValues(:),w) * J;
                end
            end
            
            surfaceTension = surfaceTension / s.We;

            if (System.settings.anal.debug)
                fprintf('Before NMPI_Allreduce in CahnHilliard.computeSurfaceTension\n')
            end
            
%             fprintf('Max of surface tension : %f\n',stMax)
            surfaceTension = NMPI_Allreduce(surfaceTension,numel(surfaceTension),'+',Parallel3D.MPI.mpi_comm_world);
            if (Parallel3D.MPI.rank==0)
                for i=1:Qt
                    fprintf('Total surface tension (t=%d) : %f (%f pi)\n',i,surfaceTension(i),surfaceTension(i)/pi)
                end
            end
        end
        
        function plotSurfaceTension(s,fig,interface)
            
%             if nargin<3
%                 s.createInterface;
%                 interface = s.interface;
%             end
            
            fx = cell(s.mesh.numLocalElements,1);
            fy = cell(s.mesh.numLocalElements,1);
            
            %Omega = FieldCollection.get('omg');
            C     = FieldCollection.get('c');
            Omg   = FieldCollection.get('omg');
            
            %C   = s.getField(1,s); %.current;
            %Omg = s.getField(2,s);  %.current;
            
            %activeField = FieldType.Current;
            % 
            %C.setActive(activeField);
            %Omg.setActive(activeField);
            
            mesh = C.mesh;
            
            Cn      = System.settings.phys.Cn;
            alpha   = System.settings.phys.alpha;
            
%             meanOmg = 0;
            
            for el=1:mesh.numLocalElements
%                 Ck      = s.getC(e,SolutionMode.Normal);
%                 dCkdx   = s.getCx(e,SolutionMode.Normal);
%                 dCkdy   = s.getCy(e,SolutionMode.Normal);
%                 Omgk    = s.getOmg(e,SolutionMode.Normal);
                
                element = s.mesh.getElement(el);

                e = mesh.getLocalElement(el).globalID;

                Ck      = C.val(FieldType.Current,e);
                dCkdx   = C.x(FieldType.Current,e);
                dCkdy   = C.y(FieldType.Current,e);
                Omgk    = Omg.val(FieldType.Current,e); % - s.meanOmega(1);

                %Ze = s.mesh.Z{e};
                %Nabla2 = element.getDxx + element.getDyy;
                %data = (Ck.^3 - 3/2*Ck.^2 + 0.5.*Ck) - Cn^2 * ( Nabla2 * Ze * C.alpha{e}(:) );
                %Omgk = alpha/Cn * data;
                
                
                fx{el} = Omgk.*dCkdx;
                fy{el} = Omgk.*dCkdy;
                
%                 meanOmg = meanOmg+mean(Omgk);
                
%                 fx{e} = Omgk;
%                 fy{e} = Omgk;
            end
            
%             meanOmg = meanOmg / mesh.numLocalElements*4
%             
%             for el=1:mesh.numLocalElements
%                 e = mesh.getLocalElement(el).globalID;
%                 dCkdx   = C.x(e);
%                 dCkdy   = C.y(e);
%                 Omgk    = Omg.val(e);
%                 fx{el} = (Omgk-meanOmg).*dCkdx;
%                 fy{el} = (Omgk-meanOmg).*dCkdy;
%             end
            
%             subplot(3,1,1)
%             data = fx;
%             for e=1:s.mesh.getNumElements
%                 if (size(data{e},2)==1)
%                     data{e} = reshape(data{e},s.mesh.getElement(e).disc.Q);
%                     [X,Y] = meshgrid( s.mesh.getElement(e).xelem,s.mesh.getElement(e).yelem );
%                     surf(X,Y,data{e}(:,:,end)');
%                 else
%                     [X,Y] = meshgrid( s.mesh.getElement(e).xelem,s.mesh.getElement(e).yelem );
%                     surf(X,Y,data{e}(:,:,end)');
%                 end
%                 hold on;
%             end
%             
%             subplot(3,1,2)
%             data = fy;
%             for e=1:s.mesh.getNumElements
%                 if (size(data{e},2)==1)
%                     data{e} = reshape(data{e},s.mesh.getElement(e).disc.Q);
%                     [X,Y] = meshgrid( s.mesh.getElement(e).xelem,s.mesh.getElement(e).yelem );
%                     surf(X,Y,data{e}(:,:,end)');
%                 else
%                     [X,Y] = meshgrid( s.mesh.getElement(e).xelem,s.mesh.getElement(e).yelem );
%                     surf(X,Y,data{e}(:,:,end)');
%                 end
%                 hold on;
%             end
%             
%             subplot(3,1,3)

            intST = 0;
            
            pbx = mesh.X(1,2) - mesh.X(1,1);
            pby = mesh.X(2,2) - mesh.X(2,1);

            %xlim( mesh.X(1,:) );
            %ylim( mesh.X(2,:) )
            
            grid off;
            %pbaspect([pbx pby 1])

            shading interp
            view([0,90])
            
            hold on;
            
            X = cell(mesh.numLocalElements,1);
            Y = cell(mesh.numLocalElements,1);
            surfaceTension = cell(mesh.numLocalElements,1);

            %% Create a meshgrid
            for e=1:mesh.numLocalElements
                [X1,Y1] = mesh.getLocalElement(e).getNodes;
                X{e} = X1(:,:,end);
                Y{e} = Y1(:,:,end);
            end
            
            %% Compute the surface tension data
            for e=1:mesh.numLocalElements
                temp = sqrt(fx{e}.^2+fy{e}.^2);
                temp = reshape(temp,mesh.getLocalElement(e).finite.qSize);
                surfaceTension{e} = temp(:,:,1);
            end
            
            maxST = max( cellfun(@(x) max(x(:)),surfaceTension) );
            
            % Integration of the surface tension
            for e=1:mesh.numLocalElements
                element = mesh.getLocalElement(e);
%                 wxy = kron(element.disc.wy,element.disc.wx);
                
                wxy = element.getW_spatial;
                
                %intST = intST + data((element.disc.Qz-1)*(element.disc.Qx*element.disc.Qy)+1:element.disc.Qz*(element.disc.Qx*element.disc.Qy))' * wxy / (element.J(1)*element.J(2)) ;
                intST = intST + surfaceTension{e}(:) .* wxy(1:element.finite.numSpatialPoints); %*(element.J(1)*element.J(2));

                surface(X{e},Y{e},surfaceTension{e});
                
%                 if interface.active && ~isempty(interface.x{e})
%                     line(interface.x{e}, interface.y{e}, repmat(maxST,size(interface.x{e})), interface.props{:});
%                 end
            end
            
            if (System.settings.anal.debug)
                fprintf('Before NMPI_Allreduce in CahnHilliard.plotSurfaceTension\n')
            end
            
%             fprintf('Max of surface tension : %f\n',stMax)

            intST = NMPI_Allreduce(intST,1,'+',Parallel3D.MPI.mpi_comm_world);
            
%             if (Parallel3D.MPI.rank==0)
%                 fprintf('Total surface tension  : %f (%f pi)\n',surfaceTension,surfaceTension/pi)
%             end



        end
        
        function plotSurfaceTensionHalf(s,fig,interface)
            
            if nargin<3
                s.createInterface;
                interface = s.interface;
            end
            
            fx = cell(s.mesh.numLocalElements,1);
            fy = cell(s.mesh.numLocalElements,1);
            
            C   = s.getField(1,s); %.current;
            Omg = s.getField(2,s);  %.current;
            
            %activeField = FieldType.Current;
            % 
            %C.setActive(activeField);
            %Omg.setActive(activeField);
            
            mesh = C.mesh;
            
%             meanOmg = 0;
            
            for el=1:mesh.numLocalElements
%                 Ck      = s.getC(e,SolutionMode.Normal);
%                 dCkdx   = s.getCx(e,SolutionMode.Normal);
%                 dCkdy   = s.getCy(e,SolutionMode.Normal);
%                 Omgk    = s.getOmg(e,SolutionMode.Normal);
                
                e = mesh.getLocalElement(el).globalID;

                Ck      = C.val(e);
                dCkdx   = C.x(e);
                dCkdy   = C.y(e);
                Omgk    = Omg.val(e); % - s.meanOmega(1);

                fx{el} = Omgk.*dCkdx;
                fy{el} = Omgk.*dCkdy;
                
%                 meanOmg = meanOmg+mean(Omgk);
                
%                 fx{e} = Omgk;
%                 fy{e} = Omgk;
            end
            
%             meanOmg = meanOmg / mesh.numLocalElements*4
%             
%             for el=1:mesh.numLocalElements
%                 e = mesh.getLocalElement(el).globalID;
%                 dCkdx   = C.x(e);
%                 dCkdy   = C.y(e);
%                 Omgk    = Omg.val(e);
%                 fx{el} = (Omgk-meanOmg).*dCkdx;
%                 fy{el} = (Omgk-meanOmg).*dCkdy;
%             end
            
%             subplot(3,1,1)
%             data = fx;
%             for e=1:s.mesh.getNumElements
%                 if (size(data{e},2)==1)
%                     data{e} = reshape(data{e},s.mesh.getElement(e).disc.Q);
%                     [X,Y] = meshgrid( s.mesh.getElement(e).xelem,s.mesh.getElement(e).yelem );
%                     surf(X,Y,data{e}(:,:,end)');
%                 else
%                     [X,Y] = meshgrid( s.mesh.getElement(e).xelem,s.mesh.getElement(e).yelem );
%                     surf(X,Y,data{e}(:,:,end)');
%                 end
%                 hold on;
%             end
%             
%             subplot(3,1,2)
%             data = fy;
%             for e=1:s.mesh.getNumElements
%                 if (size(data{e},2)==1)
%                     data{e} = reshape(data{e},s.mesh.getElement(e).disc.Q);
%                     [X,Y] = meshgrid( s.mesh.getElement(e).xelem,s.mesh.getElement(e).yelem );
%                     surf(X,Y,data{e}(:,:,end)');
%                 else
%                     [X,Y] = meshgrid( s.mesh.getElement(e).xelem,s.mesh.getElement(e).yelem );
%                     surf(X,Y,data{e}(:,:,end)');
%                 end
%                 hold on;
%             end
%             
%             subplot(3,1,3)

            intST = 0;
            
            pbx = 2*(mesh.x1 - mesh.x0);
            pby = mesh.y1 - mesh.y0;

            xlim([-mesh.x1 mesh.x1])
            ylim([mesh.y0 mesh.y1])
            
            grid off;
            pbaspect([pbx pby 1])

            shading interp
            view([0,90])
            
            hold on;
            
            X = cell(mesh.numLocalElements,1);
            Y = cell(mesh.numLocalElements,1);
            surfaceTension = cell(mesh.numLocalElements,1);

            %% Create a meshgrid
            for e=1:mesh.numLocalElements
                [X{e},Y{e}] = meshgrid( mesh.getLocalElement(e).xelem,mesh.getLocalElement(e).yelem );
            end
            
            %% Compute the surface tension data
            for e=1:mesh.numLocalElements
                temp = sqrt(fx{e}.^2+fy{e}.^2);
                temp = reshape(temp,mesh.getLocalElement(e).disc.Q);
                surfaceTension{e} = temp(:,:,1);
            end
            
            maxST = max( cellfun(@(x) max(x(:)),surfaceTension) );
            
            % Integration of the surface tension
            for e=1:mesh.numLocalElements
                element = mesh.getLocalElement(e);
                wxy = kron(element.disc.wy,element.disc.wx);
                %intST = intST + data((element.disc.Qz-1)*(element.disc.Qx*element.disc.Qy)+1:element.disc.Qz*(element.disc.Qx*element.disc.Qy))' * wxy / (element.J(1)*element.J(2)) ;
                intST = intST + surfaceTension{e}(:) .* wxy/(element.J(1)*element.J(2));

                surface(X{e},Y{e},surfaceTension{e}');
                
                if interface.active && ~isempty(interface.x{e})
                    line(interface.x{e}, interface.y{e}, repmat(maxST,size(interface.x{e})), interface.props{:});
                end
                
                % second half
                surface(-X{e},Y{e},surfaceTension{e}');
                
                if interface.active && ~isempty(interface.x{e})
                    line(-interface.x{e}, interface.y{e}, repmat(maxST,size(interface.x{e})), interface.props{:});
                end
            end
            
            if (System.settings.anal.debug)
                fprintf('Before NMPI_Allreduce in CahnHilliard.plotSurfaceTension\n')
            end
            
%             fprintf('Max of surface tension : %f\n',stMax)

            intST = NMPI_Allreduce(intST,1,'+',Parallel3D.MPI.mpi_comm_world);
            
%             if (Parallel3D.MPI.rank==0)
%                 fprintf('Total surface tension  : %f (%f pi)\n',surfaceTension,surfaceTension/pi)
%             end



        end
       
        function out = save(s,filename)
            out = [];
            
            % Merge solution from all processors on the master rank
            %c   = s.fields{1}.getGlobalAlpha;
            %omg = s.fields{2}.getGlobalAlpha;
            
            if (Parallel3D.MPI.rank==0 || System.settings.outp.parallelWrite)
                CH                = struct;
                CH.class          = class(s);
                CH.name           = s.name;
                CH.mesh           = s.mesh.save;
                %CH.alpha          = s.alpha;
                %CH.Pe             = s.Pe;
                %CH.Cn             = s.Cn;
                %CH.M              = s.M;
                %CH.We             = s.We;
                %CH.cConvergence	  = s.cConvergence;
                %CH.omgConvergence = s.omgConvergence;
                %CH.c              = c;
                %CH.omg            = omg;
                %CH.fields         = s.fields;
                
                CH.fields       = cell(s.Nvar,1);
                for n=1:s.Nvar
                    CH.fields{n} = save( s.fields{n} );
                end
                
                % Additional NS settings
                CH.steady     	= s.steady;                 % True if the system of equations is steady
                CH.nonlinear   	= s.nonlinear;            	% True if the equations are nonlinear
                CH.coupled      = s.coupled;                % True if variables of other physics are to be used in the equations
                CH.fdata        = s.fdata;
                CH.exact        = s.exact;                  % True if this physics has an exact solution
                CH.maxLevel     = s.maxLevel;               % Maximum level for AMR
                CH.eqWeights    = s.eqWeights;           	% Weights for every equation
                CH.nConvType    = s.nConvType;              % Residual convergence criteria based on [ value; residual ]
                CH.cConvType    = s.cConvType;              % Coupling convergence criteria based on [ value; residual ]
                CH.nMaxIter     = s.nMaxIter;           	% Maximum number of nonlinear iterations
                CH.cMaxIter     = s.cMaxIter;           	% Maximum number of coupling iterations
                CH.nConvergence = s.nConvergence;       	% Nonlinear convergence for each variable
                CH.cConvergence = s.cConvergence;        	% Coupling convergence for each variable
                CH.rConvergence = s.rConvergence;           % Residual convergence for each equation and the whole system
                CH.mobilityType = s.mobilityType;
                CH.stabilize    = s.stabilize;
                
                out = CH;
                %save(filename,'CH','-append');
            end
        end
        
        %%%%%%%%%%% DROPLET
        function [varargout] = addDroplet(s,variable,x,y,z,t,center,radius,radius2)
            
            if (s.mesh.sDim==2)
                center(3:end) = 0;
            end
            
            switch variable
                case Variable.c
                    if (nargin==8 || radius==radius2 )
                        varargout = cell(1,3);
                        [varargout{1:3}] = s.initialCircle(x,y,z,t,center,radius);
                    else
                        varargout = cell(1,3);
                        [varargout{1:3}] = s.initialEllipse(x,y,z,t,center,radius,radius2);
                    end
                case Variable.omg
                    if (nargin==8)
                        varargout = cell(1,3);
                        [varargout{1:3}] = s.initialOmg(x,y,z,t,center,radius);
                    else
                        varargout = cell(1,3);
                        [varargout{1:3}] = s.initialEllipseOmg(x,y,z,t,center,radius,radius2);
                    end
            end
        end
        
        function [varargout] = addTaylorDroplet(s,variable,x,y,z,t,center,radius,length)
            
            if (s.mesh.sDim==2)
                center(3:end) = 0;
            end
            
            switch variable
                case Variable.c
                    varargout = cell(1,3);
                    [varargout{1:3}] = s.initialTaylorC(x,y,z,t,center,radius,length);
                case Variable.omg
                    varargout = cell(1,3);
                    [varargout{1:3}] = 0*s.initialTaylorC(x,y,z,t,center,radius,length);
            end
        end
        
%         function [varargout] = addTaylorDroplet(s,variable,x,y,z,t,center,radius,radius2)
%             
%             if (s.mesh.sDim==2)
%                 center(3:end) = 0;
%             end
%             
%             switch variable
%                 case Variable.c
%                     if (nargin==8 || radius==radius2 )
%                         varargout = cell(1,3);
%                         [varargout{1:3}] = s.initialCircle(x,y,z,t,center,radius);
%                     else
%                         varargout = s.initialEllipse(x,y,z,t,center,radius,radius2);
%                     end
%                 case Variable.omg
%                     if (nargin==8)
%                         varargout = cell(1,3);
%                         [varargout{1:3}] = s.initialOmg(x,y,z,t,center,radius);
%                     else
%                         varargout = s.initialEllipseOmg(x,y,z,t,center,radius,radius2);
%                     end
%             end
%         end
        
        function out = addInterface(s,variable,x,y,z,t,center,radius,radius2)
            switch variable
                case Variable.c
                    if (nargin==8)
                        out = s.initialCircle(x,y,z,t,center,radius);
                    end
                case Variable.omg
                    if (nargin==8)
                        out = s.initialOmg(x,y,z,t,center,radius);
                    end
            end
        end
        
       	function preTimeLoop(s)
            if (s.stabilize)
                s.stabilizeInterface;
            end
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    end
    
    methods(Access=protected)
        function setC(s,value)
%             c.value = value;
            s.c = cell(s.mesh.getNumLocalElements,1);
            for e=1:s.mesh.getNumLocalElements
                element = s.mesh.getLocalElement(e);
                s.c{e} = value*ones(element.disc.dofeq);
            end
        end
        
        function setOmg(s,value)
            s.omg = cell(s.mesh.getNumLocalElements,1);
            for e=1:s.mesh.getNumLocalElements
                element = s.mesh.getLocalElement(e);
                s.omg{e} = value*ones(element.disc.dofeq);
            end
        end
        
        function addC(s,value)
            if (isa(value,'function_handle'))
                for e=1:s.mesh.getNumLocalElements
                    element = s.mesh.getLocalElement(e);

                    x = element.xelem;
                    y = element.yelem;
                    z = element.zelem;
                    
                    s.c{e} = max( s.c{e} , value(x,y,z) );
                end
            else
                for e=1:s.mesh.getNe
                    s.c{e} = max( s.c{e} , value );
                end
            end
        end
        
        % Function which defines an circle through a specified center with 
        % a given radius. The x,y,z data are the locations where the C
        % data needs to be evaluated. The center is a vector of length 3,
        % which contains its x,y,z data. Inside the circle C=1, outside C=0
        function [c,Dc,DDc] = initialCircle(s,x,y,z,t,center,radius)
            if length(center)~=3
                center( numel(center)+1:4 ) = 0;
            end
            
            elementSize = size(x);
            
            % VALUES
            c = zeros( elementSize );
                    
            % D VALUES
            Dc = cell(1,s.mesh.sDim);
            for i=1:s.mesh.sDim
                Dc{i} = zeros( elementSize );
            end
            
            % DD VALUES
            DDc = cell(1,s.mesh.sDim);
            for i=1:s.mesh.sDim
                DDc{i} = zeros( elementSize );
            end
            
            % Element nodes
            if (isempty(z))
                z = 0;
            end
            
            % Center location
            xc = center(1);
            yc = center(2);
            zc = center(3);
            
            denominator = (2*sqrt(2)*s.Cn_L);
            
            for i=1:numel(x)
                dx2 = (x(i)-xc)^2;
                dy2 = (y(i)-yc)^2;
                dz2 = (z(i)-zc)^2;
                
                r = sqrt( dx2 + dy2 + dz2 );
                
                %% VALUE
                %c(i) = 0.5 + 0.5 * tanh( (radius - r) / (denominator) );
                c(i) = 0.5 + 0.5 * tanh( (radius - r) / (denominator) );
                
                %% D VALUE   
                if (r~=0)
                    factor = sqrt(2) * (1-tanh( (radius-r)/denominator )^2) / (8*r*s.Cn_L);
                    %factor = (1-tanh( (1-r/radius)/(denominator*radius) )^2) / (2*denominator*r*s.Cn_L);
                else
                    factor = 0;
                end
                Dc{1}(i) = -x(i) * factor;
                Dc{2}(i) = -y(i) * factor; 
            end
        end
        
        % Function which defines an circle through a specified center with 
        % a given radius. The x,y,z data are the locations where the C
        % data needs to be evaluated. The center is a vector of length 3,
        % which contains its x,y,z data. Inside the circle C=1, outside C=0
        function [c,Dc,DDc] = initialTaylorC(s,x,y,z,t,center,radius,stretch)
            if length(center)~=3
                center( numel(center)+1:4 ) = 0;
            end
            
            elementSize = size(x);
            
            % VALUES
            c = zeros( elementSize );
                    
            % D VALUES
            Dc = cell(1,s.mesh.sDim);
            for i=1:s.mesh.sDim
                Dc{i} = zeros( elementSize );
            end
            
            % DD VALUES
            DDc = cell(1,s.mesh.sDim);
            for i=1:s.mesh.sDim
                DDc{i} = zeros( elementSize );
            end
            
            % Element nodes
            if (isempty(z))
                z = 0;
            end
            
            % Center location
            xc = center(1);
            yc = center(2);
            zc = center(3);
            
            denominator = (2*sqrt(2)*s.Cn_L);
            
            for i=1:numel(x)
                
                
                if (abs(y(i)-yc) < stretch/2)
                    %% linear zone
                    dx = radius - abs(x(i)-xc);
                    
                    %% VALUE
                    c(i) = 0.5 + 0.5 * tanh( dx / (denominator) );
% 
%                     %% D VALUE   
%                     if (r~=0)
%                         factor = sqrt(2) * (1-tanh( (radius-r)/denominator )^2) / (8*r*s.Cn_L);
%                         %factor = (1-tanh( (1-r/radius)/(denominator*radius) )^2) / (2*denominator*r*s.Cn_L);
%                     else
%                         factor = 0;
%                     end
%                     Dc{1}(i) = -x(i) * factor;
%                     Dc{2}(i) = -y(i) * factor; 
                    
                else
                    % circular zone
                    dx2 = (x(i)-xc)^2;
                    dy2 = (abs(y(i)-yc)-stretch/2)^2;
                    dz2 = (z(i)-zc)^2;
                    
                    r = sqrt( dx2 + dy2 + dz2 );
                
                    %% VALUE
                    %c(i) = 0.5 + 0.5 * tanh( (radius - r) / (denominator) );
                    c(i) = 0.5 + 0.5 * tanh( (radius - r) / (denominator) );

%                     %% D VALUE   
%                     if (r~=0)
%                         factor = sqrt(2) * (1-tanh( (radius-r)/denominator )^2) / (8*r*s.Cn_L);
%                         %factor = (1-tanh( (1-r/radius)/(denominator*radius) )^2) / (2*denominator*r*s.Cn_L);
%                     else
%                         factor = 0;
%                     end
%                     Dc{1}(i) = -x(i) * factor;
%                     Dc{2}(i) = -y(i) * factor; 
                end
            end
        end
        
        function [c,Dc,DDc] = initialEllipse(s,x,y,z,t,center,xRadius,yRadius)
            if length(center)~=3
                error('The center should have length == 3')
            end
            
            Dc = []; DDc = [];
            
            elementSize = size(x);
            c = zeros( elementSize );
            
            eqRadius = sqrt(xRadius*yRadius);
            
            denominator = (2*sqrt(2)*s.Cn_L)/eqRadius;

            xc = center(1);
            yc = center(2);
            zc = center(3);
            
            for i=1:numel(x)
                dx = (x(i)-xc);
                dy = (y(i)-yc);
                dz = (z(i)-zc);

                c(i) = 0.5 + 0.5 * tanh( (1 - sqrt( ((dx)/xRadius)^2 + ((dy)/yRadius)^2) ) / denominator );
            end
        end
        
        % Analytical solution for omega, which complies with the solution for
        % the variable c (see above). Omega is defined by:
        % 
        %   omega = 1/eps * (C^3 - 3/2*C^2 + 1/2*C) - eps * laplacian(C)
        %
        function [omg,Domg,DDomg] = initialOmg(s,x,y,z,t,center,radius)
            if length(center)~=3
                center( numel(center)+1:4 ) = 0;
            end
            
            elementSize = size(x); %[ max(1,length(x)) max(1,length(y)) max(1,length(z))];
            
            % VALUES
            omg = zeros( elementSize );
                    
            % D VALUES
            Domg = cell(1,s.mesh.sDim);
            for i=1:s.mesh.sDim
                Domg{i} = zeros( elementSize );
            end
            
            % DD VALUES
            DDomg = cell(1,s.mesh.sDim);
            for i=1:s.mesh.sDim
                DDomg{i} = zeros( elementSize );
            end
            
            % Element nodes
            if (isempty(z))
                z = 0;
            end
            
            % Center location
            xc = center(1);
            yc = center(2);
            zc = center(3);
                        
            denominator = (2*sqrt(2)*s.Cn_L);
            
            for i=1:numel(x)
                dx2 = (x(i)-xc)^2;
                dy2 = (y(i)-yc)^2;
                dz2 = (z(i)-zc)^2;

                r  = sqrt( dx2 + dy2 + dz2 );
                r2 = r;                        

                if (r<radius)
                    r = 2*radius-r;
                end 
                
                % METHOD 2 : 1 equation ( NB: s.alpha/s.Cn is a scaling )
                if (r~=0)
                    omg(i) = s.Cn_L/s.Cn * s.alpha/s.Cn_L * (s.Cn_L*sqrt(2)) / (8*r*cosh( (r-radius)/denominator )^2);
                    %omg(i) = s.alpha/s.Cn * (s.Cn*sqrt(2)) / (8*r/radius*cosh( (r/radius-1)/denominator )^2);
                else
                    omg(i) = omg(i) + 0;
                end

                %% D VALUE
                if (r2>0.1)
                    factor = s.alpha/s.Cn * (s.Cn*sqrt(2) * cosh( (r2-radius)/denominator ) + r2 * sinh( (r2-radius)/denominator ) ) / ( 8* (r2*cosh( (r2-radius)/denominator ))^3 );
                else
                    factor = 0;
                end
                Domg{1}(i) = -(x(i)-xc) * factor;
                Domg{2}(i) = -(y(i)-yc) * factor;
                Domg{3}(i) = -(z(i)-zc) * factor;
            end

            if (nnz(any(omg<0))>0)
                omg(omg<0) = min(min(min(omg(omg>0))));
            end
            
            %omg = omg*0 + 1.5;
            %omg = 0*omg;
        end
        
        function [omg,Domg,DDomg] = initialEllipseOmg(s,x,y,z,t,center,xRadius,yRadius,zRadius)
            if length(center)~=3
                error('The center should have length == 3')
            end
            
            if nargin<9
                zRadius = 1;
            end
            
            elementSize = size(x); %[ max(1,length(x)) max(1,length(y)) max(1,length(z))];
            
            % VALUES
            omg = zeros( elementSize );
                    
            % D VALUES
            Domg = cell(1,s.mesh.sDim);
            for i=1:s.mesh.sDim
                Domg{i} = zeros( elementSize );
            end
            
            % DD VALUES
            DDomg = cell(1,s.mesh.sDim);
            for i=1:s.mesh.sDim
                DDomg{i} = zeros( elementSize );
            end
            
            % Element nodes
            if (isempty(z))
                z = 0;
            end
            
            eqRadius = sqrt(xRadius*yRadius*zRadius);
            
            xc = center(1);
            yc = center(2);
            zc = center(3);
                        
            %factor = s.alp/(s.We);
            denominator = (2*sqrt(2)*s.Cn_L); %/ eqRadius;
            
            for i=1:numel(x)
                dx2 = ((x(i)-xc)/xRadius)^2;
                dy2 = ((y(i)-yc)/yRadius)^2;
                dz2 = ((z(i)-zc)/zRadius)^2;
                
                r  = eqRadius*sqrt( dx2 + dy2 + dz2 );
                r2 = r;                        

                if (r<eqRadius)
                    r = 2*eqRadius-r;
                end 
                
                % METHOD 2 : 1 equation ( NB: s.alpha/s.Cn is a scaling )
                if (r~=0)
                    omg(i) = s.Cn_L/s.Cn * s.alpha/s.Cn_L * (s.Cn_L*sqrt(2)) / (8*r*cosh( (r-eqRadius)/denominator )^2);
                    %omg(i) = s.alpha/s.Cn * (s.Cn*sqrt(2)) / (8*r/radius*cosh( (r/radius-1)/denominator )^2);
                else
                    omg(i) = omg(i) + 0;
                end

%                 %% D VALUE
%                 if (r2>0.1)
%                     factor = s.alpha/s.Cn * (s.Cn*sqrt(2) * cosh( (r2-radius)/denominator ) + r2 * sinh( (r2-radius)/denominator ) ) / ( 8* (r2*cosh( (r2-radius)/denominator ))^3 );
%                 else
%                     factor = 0;
%                 end
%                 Domg{1}(i) = -(x(i)-xc) * factor;
%                 Domg{2}(i) = -(y(i)-yc) * factor;
%                 Domg{3}(i) = -(z(i)-zc) * factor;
            end

            if (nnz(any(omg<0))>0)
                omg(omg<0) = min(min(min(omg(omg>0))));
            end
            
%             
%             
%             
%             for j=1:length(y)
%                 for i=1:length(x)
%                     r = sqrt( ((x(i)-xc)/xRadius)^2 + ((y(j)-yc)/yRadius)^2 );
% %                     if (r~=0)
% %                         omg(i,j,:) = factor * sqrt(2) / ( 8 * r * cosh( ( (sqrt(2)*(r-radius)) / (4*s.Cn) ) )^2 );
% %                     else
% %                         omg(i,j,:) = 0; % to prevent 'inf' solution
% %                     end
%                     
%                     % Only c
% % %                     c = 0.5 + 0.5 * tanh( (radius - r ) / denominator );
% % %                     omg(i,j,:) = (4*sqrt(2) / s.Cn) * ( c.^3 - 3/2 * c.^2 + 1/2 * c ); %
%                         
%                     %if (r<radius)
%                     %    r = 2*radius-r;
%                     %end
%                     
%                     %omg(i,j,:) = 0.75*sqrt(2) * sinh( (r-1)/denominator ) / ( s.Cn * cosh( (r-1) / denominator )^3);
% 
%                     % Only laplacian
%                     if (r~=0)
%                         %omg(i,j,:) = omg(i,j,:) - ( 0.75*sqrt(2) * sinh( (r-1)/denominator ) * eqRadius * r - 1.5 * s.Cn*cosh( (r-1)/denominator ) ) / ...
%                         %                                                                           ( eqRadius * r * s.Cn*cosh( (r-1)/denominator )^3 );
%                         omg(i) = s.Cn_L/s.Cn * s.alpha/s.Cn_L * (s.Cn_L*sqrt(2)) / (8*r*cosh( (r-radius)/denominator )^2);
%                     else
%                         %omg(i,j,:) = omg(i,j,:) + 0;
%                     end
%                     
%                     
%                     
% %                     epsilon = s.Cn;
% %                     if (r~=0)
% %                         omg(i,j,:) = omg(i,j,:) + 3*sqrt(2)*(cosh((-radius+r)*sqrt(2)/(4*epsilon))*sqrt(2)*epsilon-sinh((-radius+r)*sqrt(2)/(4*epsilon))*r) / (4*epsilon*r*cosh((-radius+r)*sqrt(2)/(4*epsilon))^3);
% %                     else
% %                         omg(i,j,:) = omg(i,j,:) + 0; % to prevent 'inf' solution
% %                     end
%                 end
%             end
%                         
%             if (nnz(any(omg<0))>0)
%                 omg(omg<0) = min(min(min(omg(omg>0))));
%             end
%             
%             omg = 1+0*omg / (System.settings.phys.alpha);
        end
        
        %% Stabilize the interfaces
        % By the discretization, the interface fields (C and omega) are not 
        % in balance yet (even when the exact initial solution for both are
        % used). This function solves the system of a steady and uncoupled 
        % (velocities are zero, so no interface advection) system of
        % equations. This will only slighlty adjust both C and omega
        % fields, and ensure there is no interface movement due to
        % discretization limitations.
        function stabilizeInterface(s)
            if (System.rank==0 && System.settings.outp.verbose)
                System.newLine();
                fprintf('PHYSICS    : Interface stabilization started\n')
            end
            
            %System.setVariableOffsets(Physics);
            %System.setItem(1,'timestep',0,'%8d');
            %System.setItem(2,'time level',0,'%14.2e');
            
            temp = s.nMaxIter;
            
            s.nMaxIter = 5;
            
            s.coupled = false;
            
            for t=1:1

                %% Apply boundary conditions
                s.resetBoundaryConditions();
                Settings.setBoundaryConditions( s );

                if (System.settings.time.spaceTime)
                    s.updateTimeSlab();
                    if (~s.steady)
                        %% Apply initial conditions
                        s.applyInitialConditions();
                    end
                elseif ~isempty(System.settings.time.steppingMethod)
                    physics.copySolution(FieldType.Timelevel1);
                end
            
            %% Solve nonlinear equations
%             settings = System.settings;
%             couplingSetting = settings.getCoupling;
%             settings.setConvergence([true; false],[true; false]);
            
%             for cIter=1:System.settings.comp.coupling.maxIter                       
                time1 = toc;
% 
%                 System.setIter('coupling',cIter);
% 
%                 isConvergedValue    = false(numel(System.settings.phys.classes),1);
%                 isConvergedResidual = false(numel(System.settings.phys.classes),1);
                
                % Solve the nonlinear equations for each element in the
                % Physics cell array.
                

                
                %for i=1:numel(System.settings.phys.classes)                
%                     if ( isa(Physics{i},'NavierStokes') )
%                         Physics{i}.isSteady = true;
%                         Physics{i}.setZeroGravity(true);
%                         settings.setCoupling(true);
% %                         settings.setInitialWeight(0);
%                     elseif ( isa(Physics{i},'CahnHilliard') )
%                         Physics{i}.isSteady = false;
%                         settings.setCoupling(false);
% %                         settings.setInitialWeight(100);
%                     end
% 
% %                     for j=1:numel(System.settings.phys.classes)
% %                         Physics{j}.activeField = FieldType.Coupled;
% %                     end
%                     
%                     for j=1:numel(System.settings.phys.classes)
%                         Physics{j}.activeField = FieldType.Interpolated;
%                         Physics{j}.updateInterpolated(Physics)
%                     end
%                     Physics{i}.solveNonLinear(Physics);

                %if (~System.settings.anal.useCellFun)
                 %   for i=1:numel(System.settings.phys.classes)  
                try
                    %s.steady = true;

                    %s.Pe = 1;
                    s.solveNonLinear();

                    %s.Pe = System.settings.phys.Pe;

                    %s.steady = false;
                catch Mexp
                    disp('ERROR in solveNonLinear')
                    save(['solveNonlinear.' int2str(System.rank) '.mat']);
                end   
                    
            end
            
            s.nMaxIter = temp;
            s.coupled = s.settings.coupled;
            
                %end
                                
%                 time2 = toc;
% 
%                 % Output the time of the coupling step, and disable the
%                 % iteration output
%                 System.setItem(3,'iter',cIter,'%4d');
%                 System.setItem(10,'time',time2-time1,'%10.2f');
%                 System.setItemFlags(11,false);
%                 System.outputNonLinear(Physics);
%                 
%                 % Update the mesh
% %                 for i=1:numel(System.settings.phys.classes) 
% %                     Physics{i}.meshUpdate(System.settings.mesh.maxLevel(i));
% %                 end
%                 if ( ( all(isConvergedValue)     && System.settings.comp.coupling.convType(1) ) || ...
%                      ( all(isConvergedResidual)  && System.settings.comp.coupling.convType(2) ) )
%                     break
%                 end
%                 
%                 %if ( all(isConvergedValue) )
%                 %    break
%                 %end
%             end
            
%             % Reset some values for further computation
%             for i=1:numel(System.settings.phys.classes)                
%                 if ( isa(s,'NavierStokes') )
%                     s.isSteady = false;
%                     s.setZeroGravity(false);
%                 end
%             end

%             settings.setCoupling(couplingSetting);
%             settings.setConvergence([true; false],[true; false]);
            
            if (System.rank==0 && System.settings.outp.verbose)
                fprintf('PHYSICS    : Interface stabilization finished, wall clock time : %f\n',toc-time1)
            end
        end

%         for i=1:numel(System.settings.phys.classes)                
%             if ( isa(Physics{i},'NavierStokes') )
%                 NS = Physics{i};
%             elseif ( isa(Physics{i},'CahnHilliard') )
%                 CH = Physics{i};
%             end
%         end
        
%*        CH.computeCenterOfMass;
%*        CH.computeCenterOfMassVelocity(NS);
%*        CH.computeMeanOmega;

%*        % If requested, an energy balance table is created
%*       if (System.settings.comp.energyBalance)
%*            energyBalance = integral.energyBalance(Physics);
%*            System.saveEnergy( energyBalance );
%*        end
        
%*        System.saveSolution(standardElement,baseMesh,Physics)
%*        System.saveValues(Physics)        
        
%*        NS.pField.zero;
%*        NS.uField.zero;
%*        NS.vField.zero;
        
        %rank = System.rank;
        %save(['parallel.' int2str(System.rank) '.mat']);
        %end
        
        function loadObject(s,solution)
            t1 = toc;
            
            CH = solution.data.CH;
            
            s.name = CH.name;        
            %s.cConvergence = CH.cConvergence;
            %s.omgConvergence = CH.omgConvergence;
            
            if (nargin>0)
                
%                 fprintf('physics (1)        : %f\n',toc-t1); t1=toc;
                
                if (isa(CH.mesh,class(MeshRefineMultiD)))
                    mesh = CH.mesh;
                    mesh.old.Lv = mesh.currentLv;
                    meshStruct = mesh.save;
                elseif (isstruct(CH.mesh))
                    meshStruct = CH.mesh;
                else
                    error('Undefined mesh type')
                end

                stde = LSQdisc_5.load(solution.data.standardElement);
%                 fprintf('physics (2)        : %f\n',toc-t1); t1=toc;
                
                mesh = MeshRefineMultiD.load(stde,meshStruct);
                
                if (isfield(CH.mesh,'name'))
                    mesh.setName(CH.mesh.name);
                else
                    mesh.setName(CH.name);
                end
                
%                 fprintf('physics (3)        : %f\n',toc-t1); t1=toc;
                
%                 mesh = CH.mesh;  
%                 mesh.isReady = false;
%                 mesh.update;
                
                s.initialize(mesh,CH.Pe,CH.Cn,CH.M,CH.We,CH.alpha,15,15);
                
%                 fprintf('physics (4)        : %f\n',toc-t1); t1=toc;
            end
            
            s.cField.current.loadAlphaArray(CH.c);
            s.omgField.current.loadAlphaArray(CH.omg);
            
            s.cField.coupled{1}.loadAlphaArray(CH.c);
            s.omgField.coupled{1}.loadAlphaArray(CH.omg);
        end
    end
    
    methods (Static)
        function out = load(solution)
            if (~isa(solution,'Solution'))
                error('The first argument of load should be a Solution object')
            end
            out = CahnHilliard_temp;
            out.loadObject(solution);
        end 
    end
    
%     methods (Abstract)
%         result = initial(s);
%     end
    
end

