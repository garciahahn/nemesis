classdef InterfaceObjects < handle
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
%     methods
%         function addInterface(s,point,normal)            
%             fh = @(x,y,z,t) s.initialInterface(x,y,z,t,point,normal);
%             s.cField.add( fh );
%             
%             fh = @(x,y,z,t) s.initialOmg(x,y,z,t,point,normal);
%             s.omgField.add( fh );
%         end
%     end
    
    methods (Access=protected)
        % Function which defines an interface through a specified point with 
        % a given normal. The x,y,z data are the locations where the C
        % data needs to be evaluated. The center is a vector of length 3,
        % which contains its x,y,z data. On the left of the interface C=1, 
        % and on the right side C=0
        function c = initialFlatInterface(s,x,y,z,t,point,normal)
            if length(point)~=3
                error('The point should have length == 3')
            end
            if length(normal)~=3
                error('The normal vector should have length == 3')
            end       
            
            %elementSize = size(x);
            %c = zeros(elementSize);
            
            denominator = (2*sqrt(2)*s.Cn_L);

            xp = point(1);
            yp = point(2);
            %zp = center(3);
            
            if ( all( abs(normal)==[1,0,0]) )
                c = 0.5 + 0.5 * tanh( sign(normal(1))*( xp - x ) / denominator );
                
            elseif ( all( abs(normal)==[0,1,0]) )
                c = 0.5 + 0.5 * tanh( sign(normal(2))*( yp - y ) / denominator );
                
            else
                % ensure the normal is normalized
                normal = normal / norm(normal);
                
                if (all(z(:)==0))
                    distance = normal(1) * (xp-x) + normal(2) * (yp-y);
                else
                    error('Only 2D initialFlatInterface is implemented')
                end
                
                c = 0.5 + 0.5 * tanh( distance / denominator );
                
                %error('This normal has not been implemented yet')
            end
        end
        
        % Function which defines an interface through a specified point with 
        % a given normal. The x,y,z data are the locations where the C
        % data needs to be evaluated. The center is a vector of length 3,
        % which contains its x,y,z data. On the left of the interface C=1, 
        % and on the right side C=0
        function c = initialFlatInterfaceStretch(s,x,y,z,t,point,normal,stretch)
            if length(point)~=3
                error('The point should have length == 3')
            end
            if length(normal)~=3
                error('The normal vector should have length == 3')
            end       
            
            elementSize = size(x);
            
            c = zeros(elementSize);
            
            denominator = (2*sqrt(2)*s.Cn_L*stretch);

            xp = point(1);
            yp = point(2);
            %zp = center(3);
            
            if ( all( abs(normal)==[1,0,0]) )
                for i=1:numel(x)
                    c(i) = 0.5 + 0.5 * tanh( sign(normal(1))*( xp - x(i) ) / denominator );
                end
                
            elseif ( all( abs(normal)==[0,1,0]) )
                for i=1:numel(y)
                    c(i) = 0.5 + 0.5 * tanh( sign(normal(2))*( yp - y(i) ) / denominator );
                end
                
            elseif ( abs(normal(1)) - abs(normal(2)) < 1e-13 )  % the interface normal points mainly in y-direction
                for i=1:numel(y)
                    distance = normal(1) * x(i) + normal(2) * y(i);
                    c(i) = 0.5 + 0.5 * tanh( -distance / denominator );
                    %c(i) = 0.5 + 0.5 * sin( sign(distance) * min(1,abs(distance)) * 0.5 * pi) ;
                end
                
            else
                for i=1:numel(y)
                    distance = normal(1) * x(i) + normal(2) * y(i);
                    c(i) = 0.5 + 0.5 * tanh( -distance / denominator );
                end
                
%                 error('This normal has not been implemented yet')
            end
        end
        
        % Function which defines a flat layer through a specified point with 
        % a given normal and width. The x,y,z,t data are the locations where 
        % the C data needs to be evaluated. The options are:
        %       centerPoint     : Point object
        %       normalVector    : Vector object
        %       width           : width of the layer
        %       stretch         : interface at equilibrium if set to 1.0
        function c = initializeFlatLayer_c(s,x,y,z,t,options)
            centerPoint     = options.centerPoint;
            normalVector    = options.normalVector;
            width           = options.thickness;
            
            % The flat layer is constructed by two flat interfaces, which
            % distances are combined to compute the final values for C. It 
            % is assumed that the width is di
            distancePositiveSide = ...
                ((centerPoint.x + normalVector.x * width/2) - x) * normalVector.x + ...
                ((centerPoint.y + normalVector.y * width/2) - y) * normalVector.y + ...
                ((centerPoint.z + normalVector.z * width/2) - z) * normalVector.z;
            
            distanceNegativeSide = ...
                ((centerPoint.x - normalVector.x * width/2) - x) * normalVector.x + ...
                ((centerPoint.y - normalVector.y * width/2) - y) * normalVector.y + ...
                ((centerPoint.z - normalVector.z * width/2) - z) * normalVector.z;
            
            combinedDistance = min(distancePositiveSide,-distanceNegativeSide);
            
            denominator = (2*sqrt(2)*s.Cn_L* options.stretch);
            c = 0.5 + 0.5 * tanh( combinedDistance / denominator );
        end
        
        
        % Function which defines an circle through a specified center with 
        % a given radius. The x,y,z data are the locations where the C
        % data needs to be evaluated. The center is a vector of length 3,
        % which contains its x,y,z data. Inside the circle C=1, outside C=0
        function [c,Dc,DDc] = initialCircle(s,x,y,z,t,center,radius,stretch)
            if length(center)~=3
                center( numel(center)+1:4 ) = 0;
            end
            
            if (nargin<8)
                stretch = 1;
            end
            
            elementSize = size(x);
            
            % VALUES
            c = zeros( elementSize );
                    
            % D VALUES
            Dc = cell(1,s.mesh.sDim);
            for i=1:s.mesh.sDim
                Dc{i} = zeros( elementSize );
            end
            
            % DD VALUES
            DDc = cell(1,s.mesh.sDim);
            for i=1:s.mesh.sDim
                DDc{i} = zeros( elementSize );
            end
            
            % Element nodes
            if (numel(y)==1)
                y = y * ones(size(x));
            end
            
            if (isempty(z))
                z = 0;
            end
            
            % Center location
            xc = center(1);
            yc = center(2);
            zc = center(3);
            
            % The interface thickness is scaled with Cn_L, which is equal
            % to eps/L. In the settings Cn = eps/R, so here the radius
            % (scaled by L) can be used.
            % Through this the interface thickness is proportional to the
            % radius, and not to the length scale that is used to
            % nondimensionalize the equations.
            denominator = (2*sqrt(2)*s.Cn_L*stretch);
            
            for i=1:numel(x)
                dx2 = (x(i)-xc)^2;
                dy2 = (y(i)-yc)^2;
                dz2 = (z(i)-zc)^2;
                
                r = sqrt( dx2 + dy2 + dz2 );
                
                %% VALUE
                %c(i) = 0.5 + 0.5 * tanh( (radius - r) / (denominator) );
                if (denominator~=0)
                    c(i) = 0.5 + 0.5 * tanh( (radius - r) / (denominator) );
                else
                    c(i) = 0.5 + 0.5 * sign(radius - r);
                end
                
                %% D VALUE   
                %if (r~=0)
                %    factor = sqrt(2) * (1-tanh( (radius-r)/denominator )^2) / (8*r*s.Cn_L);
                %    %factor = (1-tanh( (1-r/radius)/(denominator*radius) )^2) / (2*denominator*r*s.Cn_L);
                %else
                %    factor = 0;
                %end
                %Dc{1}(i) = -x(i) * factor;
                %Dc{2}(i) = -y(i) * factor; 
            end
        end
        
        % Function which defines an circle through a specified center with 
        % a given radius. The x,y,z data are the locations where the C
        % data needs to be evaluated. The center is a vector of length 3,
        % which contains its x,y,z data. Inside the circle C=1, outside C=0
        function [c,Dc,DDc] = initialTaylorC(s,x,y,z,t,center,radius,stretch)
            if length(center)~=3
                center( numel(center)+1:4 ) = 0;
            end
            
            elementSize = size(x);
            
            % VALUES
            c = zeros( elementSize );
                    
            % D VALUES
            Dc = cell(1,s.mesh.sDim);
            for i=1:s.mesh.sDim
                Dc{i} = zeros( elementSize );
            end
            
            % DD VALUES
            DDc = cell(1,s.mesh.sDim);
            for i=1:s.mesh.sDim
                DDc{i} = zeros( elementSize );
            end
            
            % Element nodes
            if (isempty(z))
                z = 0;
            end
            
            % Center location
            xc = center(1);
            yc = center(2);
            zc = center(3);
            
            denominator = (2*sqrt(2)*s.Cn_L);
            
            for i=1:numel(x)
                
                
                if (abs(y(i)-yc) < stretch/2)
                    %% linear zone
                    dx = radius - abs(x(i)-xc);
                    
                    %% VALUE
                    c(i) = 0.5 + 0.5 * tanh( dx / (denominator) );
% 
%                     %% D VALUE   
%                     if (r~=0)
%                         factor = sqrt(2) * (1-tanh( (radius-r)/denominator )^2) / (8*r*s.Cn_L);
%                         %factor = (1-tanh( (1-r/radius)/(denominator*radius) )^2) / (2*denominator*r*s.Cn_L);
%                     else
%                         factor = 0;
%                     end
%                     Dc{1}(i) = -x(i) * factor;
%                     Dc{2}(i) = -y(i) * factor; 
                    
                else
                    % circular zone
                    dx2 = (x(i)-xc)^2;
                    dy2 = (abs(y(i)-yc)-stretch/2)^2;
                    dz2 = (z(i)-zc)^2;
                    
                    r = sqrt( dx2 + dy2 + dz2 );
                
                    %% VALUE
                    %c(i) = 0.5 + 0.5 * tanh( (radius - r) / (denominator) );
                    c(i) = 0.5 + 0.5 * tanh( (radius - r) / (denominator) );

%                     %% D VALUE   
%                     if (r~=0)
%                         factor = sqrt(2) * (1-tanh( (radius-r)/denominator )^2) / (8*r*s.Cn_L);
%                         %factor = (1-tanh( (1-r/radius)/(denominator*radius) )^2) / (2*denominator*r*s.Cn_L);
%                     else
%                         factor = 0;
%                     end
%                     Dc{1}(i) = -x(i) * factor;
%                     Dc{2}(i) = -y(i) * factor; 
                end
            end
        end
        
        function [c,Dc,DDc] = initialEllipse(s,x,y,z,t,center,xRadius,yRadius)
            if length(center)~=3
                error('The center should have length == 3')
            end
            
            Dc = []; DDc = [];
            
            elementSize = size(x);
            c = zeros( elementSize );
            
            eqRadius = sqrt(xRadius*yRadius);

            denominator = (2*sqrt(2)*s.Cn_L)/eqRadius;

            xc = center(1);
            yc = center(2);
            zc = center(3);

            for i=1:numel(x)
                dx = (x(i)-xc);
                dy = (y(i)-yc);
                dz = (z(i)-zc);

                c(i) = 0.5 + 0.5 * tanh( (1 - sqrt( ((dx)/xRadius)^2 + ((dy)/yRadius)^2) ) / denominator );
            end
        end
        
        % Function which defines a curved interface through a specified point with 
        % a given normal. The x,y,z data are the locations where the C
        % data needs to be evaluated. The center is a vector of length 3,
        % which contains its x,y,z data. Furthermore, W is the channel width,
        % and the theta is the angle the interface makes with the
        % wall.
        %     
        % ----------------
        %   / theta
        %  (          W
        %   \ theta
        % ----------------
        function c = initialCurvedInterface(s,x,y,z,t,point,normal,W,theta)
            if length(point)~=3
                error('The point should have length == 3')
            end
            if length(normal)~=3
                error('The normal vector should have length == 3')
            end       
            
            elementSize = size(x);
            
            c = zeros(elementSize);
            
            denominator = (2*sqrt(2)*s.Cn_L);

            xp = point(1);
            yp = point(2);
            %zp = center(3);
           
            % ensure the normal is normalized
            normal = normal / norm(normal);
                
            dir = sign(normal(1));
            
            if (all(z(:)==0))
                radius      = (W/2) / sin(theta-pi/2);  	% compute the radius of the curved interface
                alphaLimit  = asin( (W/2) / radius );   	% compute the angle between the centerline of the channel and the along the curved interface up to the wall
                %alphaLimit  = pi/2;                         % max alpha is 90 degrees
                
                center  = point  - normal * radius;         % compute the center coordinates of the curved interface
                xc = center(1); yc = center(2);

                
                % The curved interface intersects the walls in points A and B. 
                % Use the local angle of the interface to compute a first
                % approximation for the variable C.
                perpDistance = radius * sin(alphaLimit);
                tangDistance = radius * cos(alphaLimit);
                
                % Compute the location of A and B
                pointA = center; pointB = center;                           % Initialize the points A and B from the center
                if ( abs(normal(1)) == 1 )
                    % x-direction is the tangential direction
                    pointA(1) = pointA(1) + normal(1) * tangDistance;
                    pointA(2) = pointA(2) + perpDistance;                   % upper point
                    pointB(1) = pointB(1) + normal(1) * tangDistance;
                    pointB(2) = pointB(2) - perpDistance;                   % lower point
                elseif ( abs(normal(2)) == 1 )
                    % y-direction is the tangential direction
                    pointA(1) = pointA(1) + perpDistance;                   % right point
                    pointA(2) = pointA(2) + normal(2) * tangDistance;
                    pointB(1) = pointB(1) - perpDistance;                   % left point
                    pointB(2) = pointB(2) + normal(2) * tangDistance;
                else
                    error('unsupported direction')
                end
                
                if ( abs(normal(1)) == 1 )
                    thetaA  = theta;
                    Cm = s.initialFlatInterfaceAngle(x,y,z,t,pointA,thetaA);
                    thetaB  = pi - theta;
                    Cp = s.initialFlatInterfaceAngle(x,y,z,t,pointB,thetaB);
                    dx = (x-xc); dy = (y-yc);                              	% compute the distance between the center and the grid locations
                    flag = (dy<=0);
                    alpha  = atan( dy ./ dx ); 
                    dir = sign(normal(1));
                    pattern = and( abs(alpha)<=alphaLimit, dir*dx>0 );
                elseif ( abs(normal(2)) == 1 )
                    thetaA  = theta-pi/2;
                    Cp = s.initialFlatInterfaceAngle(x,y,z,t,pointA,thetaA);
                    thetaB  = -thetaA;
                    Cm = s.initialFlatInterfaceAngle(x,y,z,t,pointB,thetaB);
                    dx = (x-xc); dy = (y-yc);                               % compute the distance between the center and the grid locations
                    flag = (dx<=0);
                    alpha  = atan( dx ./ dy ); 
                    r = 1 ./ cos(pi/2 * alpha/alphaLimit); 
                    dir = sign(normal(2));
                    pattern = and( abs(alpha)<=alphaLimit, dir*dy>0 );                    
                else
                    error('unsupported direction')
                end
                

                c = (Cm .* (flag) + Cp .* (~flag));
                
                
                
                % Replace curved region within alphaLimit
                distance = radius - sqrt(dx(pattern).^2 + dy(pattern).^2);
                c( pattern ) = 0.5 + dir * 0.5 * tanh( distance / denominator );
                
                %distance = radius + sign(normal(2)) * sqrt(dx.^2 + dy.^2);
                %c = 0.5 + 0.5 * tanh( distance / denominator );
                       
            else
                error('Only 2D initialCurvedInterface is implemented')
            end
        end
        
        % Function which defines an interface through a specified point with 
        % a given normal. The x,y,z data are the locations where the C
        % data needs to be evaluated. The center is a vector of length 3,
        % which contains its x,y,z data. On the left of the interface C=1, 
        % and on the right side C=0
        function c = initialFlatInterfaceAngle(s,x,y,z,t,point,theta)
            normal = [ -sin(theta), cos(theta), 0 ];
            c = initialFlatInterface(s,x,y,z,t,point,normal);
        end
        
        % Analytical solution for omega, which complies with the solution for
        % the variable c (see above). Omega is defined by:
        % 
        %   omega = 1/eps * (C^3 - 3/2*C^2 + 1/2*C) - eps * laplacian(C)
        %
        function omega = initialOmg(s,x,y,z,t,center,radius)
            if length(center)~=3
                center( numel(center)+1:4 ) = 0;
            end

            omega = zeros( size(x) );
            
            % Element nodes
            if (isempty(z))
                z = 0;
            end
            
            % Center location
            xc = center(1);
            yc = center(2);
            zc = center(3);
                        
            denominator = (2*sqrt(2)*s.Cn_L);
            
            for i=1:numel(x)
                dx2 = (x(i)-xc)^2;
                dy2 = (y(i)-yc)^2;
                dz2 = (z(i)-zc)^2;

                r  = sqrt( dx2 + dy2 + dz2 );
                r2 = r;                        

                if (r<radius)
                    r = 2*radius-r;
                end 
                
                % METHOD 2 : 1 equation ( NB: s.alpha/s.Cn is a scaling )
                if (r~=0)
                    omega(i) = s.Cn_L/s.Cn * s.alpha/s.Cn_L * (s.Cn_L*sqrt(2)) / (8*r*cosh( (r-radius)/denominator )^2);
                    %omega(i) = s.alpha/s.Cn * (s.Cn*sqrt(2)) / (8*r/radius*cosh( (r/radius-1)/denominator )^2);
                else
                    omega(i) = omega(i) + 0;
                end
            end

            if ( nnz( any(omega<0) ) > 0 )
                omega( omega<0 ) = min( min( min( omega(omega>0) ) ) );
            end
        end
        
        function omega = initialEllipseOmg(s,x,y,z,t,center,xRadius,yRadius,zRadius)
            if length(center)~=3
                error('The center should have length == 3')
            end
            
            if nargin<9
                zRadius = 1;
            end

            omega = zeros( size(x) );
            
            % Element nodes
            if (isempty(z))
                z = 0;
            end
            
            equivalentRadius = sqrt(xRadius*yRadius*zRadius);
            
            xc = center(1);
            yc = center(2);
            zc = center(3);
                        
            %factor = s.alp/(s.We);
            denominator = (2*sqrt(2)*s.Cn_L);
            
            for i=1:numel(x)
                dx2 = ((x(i)-xc)/xRadius)^2;
                dy2 = ((y(i)-yc)/yRadius)^2;
                dz2 = ((z(i)-zc)/zRadius)^2;
                
                r  = equivalentRadius*sqrt( dx2 + dy2 + dz2 );                        

                if (r<eqRadius)
                    r = 2*eqRadius-r;
                end 
                
                % METHOD 2 : 1 equation ( NB: s.alpha/s.Cn is a scaling )
                if (r~=0)
                    omega(i) = s.Cn_L/s.Cn * s.alpha/s.Cn_L * (s.Cn_L*sqrt(2)) / (8*r*cosh( (r-equivalentRadius)/denominator )^2);
                    %omega(i) = s.alpha/s.Cn * (s.Cn*sqrt(2)) / (8*r/radius*cosh( (r/radius-1)/denominator )^2);
                else
                    omega(i) = omega(i) + 0;
                end
            end

            if (nnz(any(omega<0))>0)
                omega(omega<0) = min(min(min(omega(omega>0))));
            end
        end
    end
    
    methods (Access=public)

        %%%%%%%%%%% CURVED INTERFACE
        function out = addCurvedInterface(s,variable,x,y,z,t,center,normal,width,theta)
            switch variable
                case Variable.c
                    out = s.initialCurvedInterface(x,y,z,t,center,normal,width,theta);
                case Variable.omg
                    out = zeros( size(x) );
            end
        end
        
        
        %%%%%%%%%%% FLAT INTERFACE
        function out = addFlatInterface(s,variable,x,y,z,t,center,normal)
            switch variable
                case Variable.c
                    out = s.initialFlatInterface(x,y,z,t,center,normal);
                case Variable.omg
                    out = zeros( size(x) );
            end
        end
        
        function out = addFlatInterfaceStretch(variable,x,y,z,t,center,normal,stretch)
            switch variable
                case Variable.c
                    out = s.initialFlatInterfaceStretch(x,y,z,t,center,normal,stretch);
                case Variable.omg
                    out = 0 * s.initialFlatInterfaceStretch(x,y,z,t,center,normal,stretch);
            end
        end
        
        %% FLAT LAYER
        function out = addFlatLayer(s,variable,x,y,z,t,options)
            
            % Options can contain [default value]:
            %   centerPoint     : center point of the layer [(0,0,0)]
            %   normalVector    : normal direction of the layer [(1,0,0)]
            %   thickness       : thickness of the layer [1.0]
            %   stretch         : initialize a stretched layer [default: 1.0, which indicates no stretch]
            
            if ( ~isfield(options,'centerPoint') )
                options.centerPoint = Point( [0,0,0] );
            end
            if ( ~isfield(options,'normalVector') )
                options.normalVector = Vector( [1,0,0] );
            end
            if ( ~isfield(options,'thickness') )
                options.thickness = 1.0;
            end
            if ( ~isfield(options,'stretch') )
                options.stretch = 1.0;
            end
            
            switch variable
                case Variable.c
                    out = s.initializeFlatLayer_c(x,y,z,t,options);
                case Variable.omg
                    out = zeros( size(x) );
            end
        end
        
        %%%%%%%%%%% DROPLET
        function [varargout] = addDroplet(s,variable,x,y,z,t,center,radius,radius2)
            
            if (s.mesh.sDim==2)
                center(3:end) = 0;
            end
            
            switch variable 
                case Variable.c
                    if (nargin==8)% || radius==radius2 )
                        varargout = cell(1,3);
                        [varargout{1}] = s.initialCircle(x,y,z,t,center,radius);
                    else
                        varargout = cell(1,3);
                        [varargout{1:3}] = s.initialEllipse(x,y,z,t,center,radius,radius2);
                    end
                case Variable.omg
                    if (nargin==8)
                        varargout = cell(1,3);
                        [varargout{1:3}] = s.initialOmg(x,y,z,t,center,radius);
                    else
                        varargout = cell(1,3);
                        [varargout{1:3}] = s.initialEllipseOmg(x,y,z,t,center,radius,radius2);
                    end
            end
        end
        
        function [varargout] = addDropletStretch(s,variable,x,y,z,t,center,radius,radius2,stretch)
            
            if (s.mesh.sDim==2)
                center(3:end) = 0;
            end
            
            switch variable
                case Variable.c
%                     if (nargin==8 || radius==radius2 )
                        varargout = cell(1,3);
                        [varargout{1:3}] = s.initialCircle(x,y,z,t,center,radius,stretch);
%                     else
%                         varargout = cell(1,3);
%                         [varargout{1:3}] = s.initialEllipse(x,y,z,t,center,radius,radius2);
%                     end
                case Variable.omg
%                     if (nargin==8)
                        varargout = cell(1,3);
                        [varargout{1:3}] = s.initialOmg(x,y,z,t,center,radius);
%                     else
%                         varargout = cell(1,3);
%                         [varargout{1:3}] = s.initialEllipseOmg(x,y,z,t,center,radius,radius2);
%                     end
            end
        end
        
        function out = addTaylorDroplet(s,variable,x,y,z,t,center,radius,length)
            
            if (s.mesh.sDim==2)
                center(3:end) = 0;
            end
            
            switch variable
                case Variable.c
                    out = s.initialTaylorC(x,y,z,t,center,radius,length);
                case Variable.omg
                    out = zeros( size(x) );
            end
        end
        
        % Function to initialize a spinodal decompostion
        %   Based on the equation used in Yang et al. (2017)
        %
        %       C = 0.5 + delta * rand(x,y,z)
        %   
        %   where rand is a random number from a normal random 
        %   distribution with zero mean. This is slightly different
        %   from Yang et al. (2017), as not all random values are in 
        %   the [-1,+1] domain.
        %
        function out = addSpinodal(s,variable,x,y,z,t,average,delta,box)
            rng shuffle
            
            if nargin==8
                randomValues = randn( size(x) );
                randomValues = randomValues - mean(randomValues);

                out = average + delta * randomValues;
                
            else
                randomValues = randn( size(x) );
                randomValues = randomValues - mean(randomValues);

                average = average / ( prod(box(:,2)-box(:,1)) );
                
                out = average + delta * randomValues;
                
                flags = false( numel(out),1 );
                flags( logical( ( (x(:)>box(1,1)) .* (x(:)<box(1,2))) .* ( (y(:)>box(2,1)) .* (y(:)<box(2,2)))) ) = true;
                
                out( ~flags ) = 0;
            end
        end
        
        function out = addDroplet3D(s,variable,x,y,z,t,center,radius)
            if (variable==1)
                c = zeros( size(x) );
            
                % Center location
                xc = center(1);
                yc = center(2);
                zc = center(3);

                % The interface thickness is scaled with Cn_L, which is equal
                % to eps/L. In the settings Cn = eps/R, so here the radius
                % (scaled by L) can be used.
                % Through this the interface thickness is proportional to the
                % radius, and not to the length scale that is used to
                % nondimensionalize the equations.
                denominator = (2*sqrt(2)*s.Cn_L);

                for i=1:numel(x)
                    dx2 = (x(i)-xc)^2;
                    dy2 = (y(i)-yc)^2;
                    dz2 = (z(i)-zc)^2;

                    r = sqrt( dx2 + dy2 + dz2 );

                    %% VALUE
                    c(i) = 0.5 + 0.5 * tanh( (radius - r) / (denominator) ); 
                end

                out = c;
            else
                out = zeros(size(x));
            end
        end
        
        function out = addTorus3D(s,variable,x,y,z,t,center,R,r)
            % Function to create a torus shaped initial condition. The
            % major radius is given by R, the minor radius by r. The aspect
            % ratio is defined as R/r, and is typically a value between 2 and 3.
                        
            if (variable==1)                
                % This LS function defines phi>0 inside the torus, and phi<0 
                % outside. The interface itself is defined by phi=0
                phi = -( (sqrt(x.^2 + y.^2) - R).^2 + z.^2 - r.^2 );
                        
                % Convert phi (LS value) to C (phase-field value). The
                % interface is now defined by C=0.5, C>0.5 is inside and 
                % C<0.5 is outside. The denominator is to have control over
                % the thickness of the diffuse interface.
                denominator = (2*sqrt(2)*s.Cn_L);
                out = 0.5 + 0.5 * tanh( phi / denominator);
                  
            else
                out = zeros(size(x));              
            end
        end
        
        function out = addComplex3D(s,variable,x,y,z,t,center,D)
            % Function to return the initial condition as used by Zhang and
            % Ye (2016), see equation 43 of their article
            
            if (variable==1)                
                % This LS function defines phi>0 inside the complex structure, and
                % phi<0 outside. The interface itself is defined by phi=0
                phi = 1.0 - ( (x.^2+y.^2-D^2).^2 + (z-1).^2 .* (z+1).^2 ) .* ...
                            ( (y.^2+z.^2-D^2).^2 + (x-1).^2 .* (x+1).^2 ) .* ...
                            ( (z.^2+x.^2-D^2).^2 + (y-1).^2 .* (y+1).^2 );

                % Convert phi (LS value) to C (phase-field value). The
                % interface is now defined by C=0.5, C>0.5 is inside and 
                % C<0.5 is outside. The denominator is to have control over
                % the thickness of the diffuse interface.
                denominator = (2*sqrt(2)*s.Cn_L);
                out = 0.5 + 0.5 * tanh( phi / denominator);
                  
            else
                out = zeros(size(x));              
            end
        end
    end
    
end

