classdef NSK_E_Cart < Physics
    %NSK_energy_reduced_model Implementation of the energy equation for NSK 
    properties (Access=private)
        REI; WEI;
    end
    
    properties (Constant)
        % Variable ids for this physics
        T = 1;
    end
    
    properties
        Re; We; k; Cv;
    end
    
    methods
        function s = NSK_E_Cart(mesh,settings)
            s = s@Physics(mesh);
            
            s.shortName = 'NSK_E_Cart';
            s.longName  = 'NSK_Energy_Cartesian';
            s.name      = 'NSK_E_Cart';
            
            s.nEq       = 1;
            s.variables = {'T'};
            
            s.initialize(settings);
        end
        
        function initialize(s,settings)
            initialize@Physics(s,settings);

            s.steady    = settings.steady;
            
            s.Re        = System.settings.phys.Re;
            s.We        = System.settings.phys.We;
            s.k         = System.settings.phys.k;
            s.Cv        = System.settings.phys.cv;
            
            s.REI       = 1.0/s.Re;
            s.WEI       = 1.0/s.We;
        end

        function time = setupEquations(s)
            % Function to setup the energy equation for the NSK. These
            % equations can be found in the following article of Keunsoo 
            % (at the end of section 2):
            %   Thermal two-phase flow with a phase-field method
            %   Int. Journal of Multiphase Flow, 2019
            %
            t1 = toc;
            
            weight1 = s.eqWeights(1);
            
            U	= FieldCollection.get('u');
            V   = FieldCollection.get('v');
            R   = FieldCollection.get('R');
            q   = FieldCollection.get('q');
                        
            for e=s.mesh.eRange
                element	= s.mesh.getElement(e);         % Select the active element
                el      = element.localID;              % Get the local element number
                
                % elemental operators
                H       = element.getH;
                Dx      = element.getDx;
                Dy      = element.getDy;
                if (s.steady)
                    Dt = 0;
                else
                    Dt = element.getDt;
                end
                Dxx     = element.getDxx;
                Dyy     = element.getDyy;
                Wel     = element.getW;
                matZ    = element.getMatZ;              %#ok<NASGU>
                vecZ    = element.getVecZ;              %#ok<NASGU>
                
                
                % u-velocity derivatives
                Uk      = U.val2(e,s.mesh);
                Ux      = U.x(FieldType.Current,e);
                Uy      = U.y(FieldType.Current,e);
                
                % v-velocity derivatives
                Vk      = V.val2(e,s.mesh);
                Vx      = V.x(FieldType.Current,e);
                Vy      = V.y(FieldType.Current,e);
                
                % density derivatives
                Rk      = R.val2(e,s.mesh);
                
                % auxiliary terms
                divU    = Ux + Vy;
                Conv    = Dt + Uk.*Dx + Vk.*Dy; 
                Lap     = Dxx + Dyy;
                
                PT      = 8*Rk ./ (3-Rk);
                
                % RHS terms
             	rhs = s.REI * ( 2*Ux.^2 + Uy.^2 + Vx.^2 + 2 * Vy.^2 + 2 * Uy .* Vx - (2/3) * (divU .* divU));

                % Assign L and G
                s.opL{el} = weight1 * [ s.Cv * Rk .* Conv - s.k * Lap + PT .* divU .* H ]; %#ok<NBRAK>

                s.opG{el} = weight1 * [ rhs ]; %#ok<NBRAK>

                s.opL{el}   = s.opL{el} * superkron(eye(s.Nvar),s.mesh.Z{e});
                s.W{el}     = repmat(Wel,s.Nvar,1);
                s.sqrtW{el} = repmat(sqrt(Wel),s.Nvar,1);
            end

            t2 = toc;
            time = t2-t1;
        end              

        % Default refinement criteria (false)
        function criteria = getRefinementCriteria(s,e)
            criteria(1) = false;
        end
    end
    
    methods (Static)
        function out = load(solution)
            if (~isa(solution,'Solution'))
                error('The first argument of load should be a Solution object')
            end
            
            out = julianE_NSK;
            out.loadObject(solution);
        end 
    end
    
end

