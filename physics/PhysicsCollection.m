%PHYSICSCOLLECTION Collection of the (coupled) physics
%
%   The PhysicsCollection is a container for all (coupled) physics required for a problem. There can be only one 
%   PhysicsCollection, which is ensured by the Singleton pattern. Important static functions are the following:
%       add                     Adds a single physics
%       remove                  Removes a single physics
%       info                    Prints information on the collection
%       size                    Returns the number of physics
%       reset                   Clears the collection
%
%   The 'ALL PHYSICS OPERATIONS' functions loop over all physics in the container. It will call the function with the 
%   same name within each single physics, so these should be present there. This is ensure by their presence in the 
%   Physics.m class, which forms the blueprint for each physics.
%
% AUTHOR: Marcel Kwakkel, 2019
%
classdef PhysicsCollection < Singleton
    properties (Access=private)
        physics;
        numPhysics;

        maxNumVariables;
        maxNumEquations;
    end
    
    % Private initialization of this object
    methods(Access=private)
        function s = PhysicsCollection()
            s.physics    = cell(0);
            s.numPhysics = 0;
        end
    end    

    methods(Static)
        
        %% CONSTRUCTOR
        function s = instance()
            % Allow only 1 object to exist
            persistent uniqueInstance
            if isempty(uniqueInstance)
                s = PhysicsCollection();
                uniqueInstance = s;
            else
                s = uniqueInstance;
            end
        end
        
        %% CONTAINER OPERATIONS
        function add(id,physics)
            %ADD Adds a physics to the collection
            s = PhysicsCollection.instance();
            
            if ( numel(s.physics)<id || isempty(s.physics{id}) )
                s.numPhysics    = s.numPhysics + 1;
                s.physics{id}   = physics;
            else
                error('There is already a physics with id=%d\n',id)
            end
        end
        
        function remove(id)
            %REMOVE Removes a physics from the collection
            s = PhysicsCollection.instance();
            if ~isempty(s.physics{id})
                s.numPhysics = s.numPhysics - 1;
                s.physics{id} = [];
            else
                %error('There is no finite element with id=%d\n',id)
            end
        end
        
        function out = get(id)
            % Returns a specific physics
            
            if (isempty(id))
                error('Please provide a valid physics id, this one is empty')
            end
            s = PhysicsCollection.instance();
            
            out = [];
            
            if (isnumeric(id))
                if ( inrange(id,1,s.numPhysics) )
                    out = s.physics{id};
                else
                    error('No physics known with id=%d\n',id)
                end
                
            elseif (ischar(id))
                for n=1:s.numPhysics
                    if ( strcmp(s.physics{n}.shortName,id) )
                        out = s.physics{n};
                    end
                end
                
                if ( isempty(out) )
                    error('No physics known with id=%s\n',id)
                end
                
            else
                error('The provided id is not recognized')
            end
        end
        
        function info()
            % Provides information about the physics within the container
            
            s = PhysicsCollection.instance();
            fprintf('    PhysicsCollection with %d physics\n',s.numPhysics);
            for i=1:s.numPhysics
                fprintf('       Physics %d : %s\n',i,s.physics{i}.name);
            end
        end
        
        function out = size()
            % Returns the number of physics in the container
            s = PhysicsCollection.instance();
            out = s.numPhysics;
        end
        
        function reset()
            % Clears all currently present physics
            s = PhysicsCollection.instance();
            s.physics    = cell(0);
            s.numPhysics = 0;
        end
        
        %% ALL PHYSICS OPERATIONS
        function setInitialSolution()
            % Set the initial solution for all physics in the container
            
            s = PhysicsCollection.instance();
            
            % Loop over all physics to perform AMR
            for i=1:s.numPhysics
                physics = s.physics{i};
                physics.setInitial( physics.maxLevel );             % set initial solution, and refine mesh accordingly
                
                if ( isfield(physics.settings,'hasExactSolution') )
                    if (physics.settings.hasExactSolution)
                        physics.setExactSolution(false);	% Assign the exact solution
                    end
                end
            end

            % Next initialize the physics again without any AMR. 
            % There was a bug that the 2nd physics would change AMR, but 
            % the fields of the 1st physics were not changed accordingly 
            for i=1:s.numPhysics
                physics = s.physics{i};
                physics.initial;             % set initial solution, and refine mesh accordingly
                physics.init;
            end
        end
        
        function solveTimeStep(timeIteration,maxCoupling)
            
            % Function to solve a time step for all physics. Internally a
            % (coupled) set of equations are solved iteratively. The iterations
            % are stopped if all coupled fields have converged (or the provided
            % maxIter has been reached).
            
            % Julian:
            % Inputs:
            %     - timeIteration: It seems obsolete since the main loop of
            %     the Nemesis script doesnt give any input to this
            %     function. This can be seen in first set of instructions
            %     in the following lines.
            %     - maxCoupling: ???
            
            if nargin==0
                timeIteration = System.settings.time.iter;
            end
            
            % Julian: Grabs input to bypass some MATLAB issues with long 
            % access trees to objects.
            s = PhysicsCollection.instance();
            
            time = zeros(2,1);
            
            % 1) Prepare all physics for a new time step
            for i=1:s.numPhysics

                tStart = toc;
                
                % Select the active Physics
                physics = s.physics{i};
                
                % Run the preTimeStep function for the current physics
                physics.preTimeStep;
                
                % Check if AMR needs to be updated
                if (System.settings.mesh.updateMoment(1) && ( physics.maxLevel>0 || max(physics.mesh.getLv)>physics.maxLevel ) )
                    if (i==1 || ~System.settings.mesh.matching)
                        physics.meshUpdate( physics.maxLevel );
                    end
                end
                
%                 % Save the 'current' solution to a previous time level
%                 if (~System.settings.time.spaceTime && ~isempty(System.settings.time.method))
%                     physics.copySolution(FieldType.Timelevel1);
%                 end

                %Physics{i}.computeResidual;

                if (physics.exact)
                    % Assign the exact solution
                    physics.setExactSolution(false);
                end

                % Clear any existing boundary conditions 
                physics.resetBoundaryConditions();

                % Time boundary
                %   spaceTime : Move solution from end to beginning of time 
                %               slab and applyInitialCondition if unsteady
                %   stepping  : Copy current solution to new time level subField
                                
%                 isSpaceTime = (System.settings.time.method==TimeMethod.SpaceTime);
%                 
%                 if (isSpaceTime)
%                     physics.updateTimeSlab();
%                     if (~physics.steady)
%                         physics.applyInitialConditions();
%                     end
%                 elseif ~isempty(System.settings.time.method)

                    % Julian: performs different actions depending on which
                    % time method is selected.
                    switch System.settings.time.method
                        case TimeMethod.SpaceTime
                            physics.updateTimeSlab();
                            if (~physics.steady)
                                physics.applyInitialConditions();
                            end
                            
                        case TimeMethod.Steady
                            % do nothing
                            
                        otherwise
                            if (timeIteration==1)
                                physics.shiftTimeSolutions(TimeMethod.EulerImplicit,System.settings.comp.extrapolationOrder);
                            else
                                physics.shiftTimeSolutions(System.settings.time.method,System.settings.comp.extrapolationOrder);
                            end
                    end
%                 elseif ~physics.steady
%                     error('The problem is unsteady but no proper time advancing method has been selected')
%                 end
                
                % Setup boundary conditions, only for single stage
                % equations, since for multi stage equations the bc will be
                % set up during the non-linear solves. Without this
                % limitation to single stage equations, the code will crash
                % due to incompatible number of elements (after refinement)
                % Changed by MMKK on 04/06/2019
                if (physics.numStages==1)
                    activeSettings = System.settings;
                    activeSettings.setBoundaryConditions(physics);
                end

                %physics.updateFields(FieldType.Coupled);                     % Update the values of the 'coupled' subField
                
                % If a manufactured solution is requested, this step will 
                % compute the required forcing terms
                physics.computeForcing();
                
                tEnd = toc;
                time(i) = tEnd-tStart;
            end % for - Preparation cycle

            if (nargin<2)
                activeSettings = System.settings;
                maxCoupling = activeSettings.comp.maxCoupling;
            end % if
            
            if s.numPhysics==1
                maxCoupling = 1;
            end % if
            
            % 2) Solve a timestep for all the (coupled) physics
            for couplingIteration=1:maxCoupling
                
                % Solve the nonlinear equations for each physics
                for i = 1 : s.numPhysics
                    tStart = toc;
                    
                    % a) Select the active Physics
                    physics = s.physics{i};
                    
                    % b) Save the 'current' subFields as the 'coupled' subField 
                    %    to allow a coupled convergence check later
                    if (System.settings.comp.coupling.enabled && maxCoupling>1)
                        physics.copySolution(FieldType.Coupled);
                    end
                    
                    % c) Solve the nonlinear equations for the active physics
                    physics.solveNonLinear(couplingIteration);
                    
                    tEnd = toc;
                    % JULIAN: time includes everything necessary in the
                    % coupling cycle to solve the equations
                    if (couplingIteration==1)
                        time(i) = time(i) + tEnd-tStart;        % include initialization in the first coupling step
                    else
                        time(i) = tEnd-tStart;                  % after the first just time only the coupling step
                    end
                end % for - cycle through the number of physics
                
                % Check if the (new) current field has converged by 
                % comparing it with the previously stored Coupled field
                if (System.settings.comp.coupling.enabled && maxCoupling>1)
                    
                    isConverged = PhysicsCollection.isConverged(FieldType.Coupled,couplingIteration);
                    
                    for i=1:s.numPhysics
                        % a) Select the active Physics
                        physics = s.physics{i};
                        
                        System.writeToScreen('coupling',i,physics.name,couplingIteration,time(i),physics.norms{1},physics.norms{2});
                    end
                    
                    if ( isConverged )
                        break
                    end
                end % if - Check coupled convergence
            end % for - Coupling cycle
        end
        
        function out = isConverged(fieldType,iter)
            % Verify if all physics have reached convergence.
            
            s = PhysicsCollection.instance();
            
            converged = false(s.numPhysics,1);
            
            for i=1:s.numPhysics
                physics = s.physics{i};
                
                if (fieldType==FieldType.Coupled)
                    physics.activeVariables = 1:physics.Nvar;
                end
                
                converged(i) = physics.isConverged(fieldType,iter);
            end
            
            out = all(converged);
        end
    
        function dofInfo()
            % Outputs the number of DOFs for each physics
            s = PhysicsCollection.instance();
            for i=1:s.numPhysics
                if (System.rank==0)
                    fprintf('SIMULATION : %s has %d DOF\n',s.physics{i}.name,s.physics{i}.getNumDof) 
                end
            end
        end
        
        function out = getMaxNumVariables()
            s = PhysicsCollection.instance();
            %if (isempty(s.maxNumVariables) ||s.maxNumVariables == 0)
                s.maxNumVariables = 0;
                for i=1:s.numPhysics
                    s.maxNumVariables = max( s.maxNumVariables, s.physics{i}.Nvar );
                end
            %end
            out = s.maxNumVariables;
        end
        
        function out = getMaxNumEquations()
            s = PhysicsCollection.instance();
            if (isempty(s.maxNumEquations) ||s.maxNumEquations == 0)
                s.maxNumEquations = 0;
                for i=1:s.numPhysics
                    s.maxNumEquations = max( s.maxNumEquations, s.physics{i}.nEq );
                end
            end
            out = s.maxNumEquations;
            
            out = 3; % #FIX: find a proper way to determine the maximum number of equations of all physics
        end
        
        function write_vtu()
            % Write the physics data to Paraview
            s = PhysicsCollection.instance();
            for i=1:s.numPhysics
                s.physics{i}.write_vtu;
            end
            
            if (System.rank==0 && System.settings.outp.debug)
                disp('All physics written to Paraview files') 
            end
        end
        
        function writeValues()
            % Write some values to a text file for all physics
            s = PhysicsCollection.instance();
            for i=1:s.numPhysics
                s.physics{i}.writeValues;
            end
        end
        
        function preTimeLoop()
            s = PhysicsCollection.instance();
            for i=1:s.numPhysics
                s.physics{i}.preTimeLoop;
            end
        end        

        function out = save()
            % Function to save a PhysicsCollection to a struct. The number of  
            % physics and structs of the physics are saved (by calling
            % the save(physics) function (see Physics.save).
            s = PhysicsCollection.instance();
            
            out = cell(s.numPhysics,1);
            for i=1:s.numPhysics
                physics = PhysicsCollection.get(i);
                out{i} = save(physics);
            end
        end

        function load(physics)
            % Function to load a PhysicsCollection from a saved struct. First the
            % collection is reset, and next the physics are loaded and
            % added to the collection (see Physics.load).
            PhysicsCollection.reset;
            FieldCollection.reset;
            for i=1:numel(physics)                
                if (System.settings.mesh.matching)
                    if (i==1)
                        temp = Physics.load(physics{i});
                        mesh = temp.mesh;
                    else
                        temp = Physics.load(physics{i},mesh);
                    end
                else
                    temp = Physics.load(physics{i});
                end
                
                PhysicsCollection.add(i,temp);
            end
        end
    end
end
