classdef WaveEquation < Physics
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here    
    properties (Access=protected)
        gx; gy;
        REI; FRI2; WEI;
        
        % NS properties
        Ja; Pr; Re; We; Fr; g; alpha;
        lamRho, lamMu;
        
        gradP = zeros(3,1);
        
        % Default values
        sharpRho    = false;
        sharpMu     = false;
        eps         = 0.1;
                
        maxVelocityNorm;
    end
    
    properties 
        % Fields
        stField;
        
        dynamicWeight = false;
        computeDynamicWeight = false;
        weight = 1;
        
        withInterfaceForcing = true;
        zeroGravity = false;
    end
    
    methods
        % Class constructor
        function s = WaveEquation(mesh,settings)
            s = s@Physics(mesh);
                        
            if nargin>0
                s.shortName = 'WE';
                s.longName  = 'Wave Equation';
                s.name = 'WE';

                s.nEq = 2;
                s.variables = {'u','rho'};
                
                s.initialize(settings);
            end
        end
        
        % Function to set all properties 
        function initialize(s,settings)
            initialize@Physics(s,settings);
            
            s.Re        = System.settings.phys.Re;   
            s.We        = System.settings.phys.We;
            s.Fr        = System.settings.phys.Fr;
            s.Pr        = System.settings.phys.Pr;
            s.Ja        = System.settings.phys.Ja;
            s.g         = System.settings.phys.g;
            s.alpha     = System.settings.phys.alpha;
            s.lamRho    = System.settings.phys.lamRho;
            s.lamMu     = System.settings.phys.lamMu;
            
           
            s.REI  = 1.0/s.Re;

            if (s.We>0)
                s.WEI = 1.0/s.We;
            else
                s.WEI = 0;
            end

            if (s.Fr>0)
                s.FRI2 = 1.0/s.Fr;
            else 
                s.FRI2 = 0;
            end                

            if (s.g==0)
                s.gx = 0;
                s.gy = 0;
            else
                s.gx = s.g(1);
                s.gy = s.g(2);
            end
            
            % Navier-Stokes specific settings
            if (isfield(settings,'eps'))
                s.eps = settings.eps;
            end
            
            if (isfield(settings,'sharpRho'))
                s.sharpRho = settings.sharpRho;
            end
            
            if (isfield(settings,'sharpMu'))
                s.sharpMu = settings.sharpMu;
            end
            
            if (isfield(settings,'pressureGradient'))
                s.gradP   = settings.pressureGradient; 
            end
        end
                
        % Setup the NS equations
        function time = setupEquations(s) %,physics)
            t1 = toc;

            %s.allPhysics = physics;
            
            %s.eqWeights = System.settings.comp.NS.equationWeights;
            
%             kappa = 0;
%                         
%             if ( System.settings.comp.coupling.enabled && s.coupled)
%                 C   = FieldCollection.get('c');
%                 Omg = FieldCollection.get('omg');
%                 
% %                 for i=1:numel(physics)
% %                     if isa(physics{i},'CahnHilliard')
% %                         %C   = physics{i}.getField( Variable.c   , s );
% %                         %Omg = physics{i}.getField( Variable.omg , s );
% %                         % ST  = coupledPhysics{i}.getField( Variable.st , coupledPhysics{i}.activeField );
% %                         
% % %                         physics{i}.computeMeanOmega;
% % %                         if (System.settings.comp.fixedKappa)
% % %                             kappa = System.settings.comp.meanKappa;
% % %                         else
% % %                             kappa = physics{i}.kappa;
% % %                         end
% %                         
% %                         kappa = 0;
% %                     elseif isa(physics{i},'EnergyEquation')
% %                         T = physics{i}.getField( Variable.t, s ); %, physics{i}.activeField );
% %                     end
% %                 end
% 
% %                 CH = PhysicsCollection.get(2);
% %                 %Omega = CH.computeOmega;
% % 
% %                 % Get the curvature from the CH simulation, use the NS.mesh as
% %                 % the base mesh for the data
% %                 [kx,ky] = CH.getCurvature(s.mesh);
% %                 diffusiveFlowRate = CH.getDiffusiveFlowRate(s.mesh);
% 
%                 % Droplet/bubble center of gravity velocity at temporal
%                 % quadrature points
%                 if (System.settings.phys.dynamicReferenceFrame)
%                     CH.computeCenterOfMassVelocity(true,false);       % determine the center-of-mass velocity: compute the enclosed mass, and without overIntegration
%                     uc = CH.cmv_total(:,1);
%                     vc = CH.cmv_total(:,2);
%                     
%                     absCenterVelocity = sqrt( uc.^2 + vc.^2 );
%                 else
%                     uc  = [];
%                     vc  = [];
%                     absCenterVelocity = [];
%                 end
%             else
%                 C   = NaN; %FieldCollection.get('c');
%                 Omg = NaN; %FieldCollection.get('omg');
%                 kx  = [];
%                 ky  = [];
%                 uc  = [];
%                 vc  = [];
%                 absCenterVelocity = [];
%             end
            
            U   = s.getField( Variable.u );      
            rho = s.getField( Variable.rho );
            
            %s.Gi = zeros(s.mesh.numElements,1);
            
            for e=s.mesh.eRange
                %t(1) = toc;
                
                % Select the active element
                element = s.mesh.getElement(e);
                el = element.localID;
                                
                Uk      = U.val(FieldType.Current,e);
                dUkdx   = U.x(FieldType.Current,e);
                dUkdy   = U.y(FieldType.Current,e);
                               
                rhok    = rho.val(FieldType.Current,e);
                drhokdx = rho.x(FieldType.Current,e);
                drhokdy = rho.y(FieldType.Current,e);

                % Get all elemental operators/arrays
                H       = element.getH;
                Dx      = element.getDx;
                Dy      = element.getDy;
                if (s.steady)
                    Dt = 0;
                else
                    Dt = element.getDt;
                end
                Dxx     = element.getDxx;
                %Dyy     = element.getDyy;
                %Dxy     = element.getDxy;
                Wel     = element.getW;
                matZ    = element.getMatZ;
                vecZ    = element.getVecZ;
                                                
%                 dVisUU  = s.REI*( 2*dmudx.*Dx + dmudy.*Dy );
%                 
% %                 simplifyViscousTerm = 1; % see Kang (2000)
% %                 dVisUV  = s.REI*(   dmudy.*Dx + (1-simplifyViscousTerm)*mu.*Dxy );
% %                 dVisVU  = s.REI*(   dmudx.*Dy + (1-simplifyViscousTerm)*mu.*Dxy );
%                 
%                 dVisUV  = s.REI*(   dmudy.*Dx );
%                 dVisVU  = s.REI*(   dmudx.*Dy );
%                 
%                 dVisVV  = s.REI*(   dmudx.*Dx + 2*dmudy.*Dy ) ;
%                 
%                 linearization = Linearization.Newton;
%                 
%                 Comm    = Uk.*Dx + Vk.*Dy;          % Used for both Picard and Newton linearization
%                 
%                 switch linearization
%                     case Linearization.Newton
%                         ConvUU  = Comm + dUkdx .* H;
%                         ConvUV  =        dUkdy .* H;
%                         ConvVU  =        dVkdx .* H;
%                         ConvVV  = Comm + dVkdy .* H;
%                         ConvGx  = Uk.*dUkdx + Vk.*dUkdy ;
%                         ConvGy  = Uk.*dVkdx + Vk.*dVkdy ; 
%                     case Linearization.Picard
%                         ConvUU  = Comm;
%                         ConvUV  = matZ;
%                         ConvVU  = matZ;
%                         ConvVV  = Comm;
%                         ConvGx  = vecZ;
%                         ConvGy  = vecZ;
%                 end
                
                weight1 = s.eqWeights(1); %(s.eqWeights(1)*Ck + 1) * 0.005;
                weight2 = s.eqWeights(2); %(s.eqWeights(2)*Ck + 1) * 0.005;
                
%                 ustar   = 10;
                c       = 6; %360 / ustar / 6;
%                 L       = 30;
%                 rhostar = 2;
%                 epsilon = 1e-11; %
%                 epsilon = 2.025e-5 / (rhostar*ustar*L);
                epsilon = 1/(2*50);
                
                St = 1;
                
%                 epsilon = 1/40;
                
                s.opL{el} = [ weight1.*(rhok.*Dt*St + 2*rhok.*(Uk.*Dx) - epsilon*Dxx)          weight1.*(Uk.*Dt + (Uk.^2+c^2) .* Dx)   ; 
                              weight2.*(rhok.*Dx)                                           weight2.*(    St*Dt + Uk .* Dx)               ]; ...
                              
                              
                fvx = vecZ;
                fvy = vecZ;
                 
                s.opG{el} = [ weight1.*(fvx) ; ...
                              weight2.*(fvy) ];
                          
                if (s.withForcing)
                    s.opG{el} = s.opG{el} - s.forcing{el};
                end
                
                Ze = s.mesh.Z{e};
                         
                s.opL{el} = s.opL{el} * superkron(eye(s.Nvar),Ze);
                
                s.W{el}      = repmat(Wel,s.nEq,1);
                s.sqrtW{el}  = repmat(sqrt(Wel),s.nEq,1);
            end
            
            t2 = toc;
            time = t2-t1;
        end
        
        function updateCoupled(s,physics)
            if (nargin==1)
                mesh=s.getMesh;
            else
                for i=1:numel(physics)
                    if isa(physics{i},class(s))
                        % do nothing, own mesh
                    else
                        mesh = physics{i}.getMesh;
                    end
                end
            end
            s.uField.updateCoupled(mesh);
            s.vField.updateCoupled(mesh);
            s.pField.updateCoupled(mesh);
        end
    
        function updateCoupling(s)
            s.uField.update(SolutionMode.Coupling)
            s.vField.update(SolutionMode.Coupling)
            s.pField.update(SolutionMode.Coupling)
        end               

%         function setNormItems(s,fieldType,iter)
%             L2Norm = s.getL2Norm(fieldType);
% %             System.setItem(3,'  #',iter,'%4d');
% %             System.setItem(6,'  |dU|/|U|',L2Norm(1),'%10.2e');
% %             System.setItem(7,'  |dV|/|V|',L2Norm(2),'%10.2e');
% %             System.setItem(8,'  |dP|/|P|',L2Norm(3),'%10.2e');
%             System.setItems(s,iter,L2Norm);
%         end
  
        function syncPrescribed(s)
            s.uField.syncPrescribed;
            s.vField.syncPrescribed;
            s.pField.syncPrescribed;
        end
        
        function prescribed = getPrescribed(s,e)
            if nargin==2
                prescribed = [s.uField.getPrescribed(e); s.vField.getPrescribed(e); s.pField.getPrescribed(e)];
            else
                prescribed = cell(s.mesh.numLocalElements,1);
                for e=1:numel(prescribed)
                    element = s.mesh.getLocalElement(e);
                    prescribed{e} = s.getPrescribed( element.globalID );
                end
            end
        end
        
        function out = getPrescribedValues(s,e)
            out = [s.uField.getPrescribedValues(e); s.vField.getPrescribedValues(e); s.pField.getPrescribedValues(e)];
        end

        function output(s,outputDir,id)
            data.u = s.getU( 1, s.getElementalAlpha(Variable.u) );
            for e=Parallel3D.MPI.eStart:Parallel3D.MPI.eEnd
                data.u{e} = reshape(data.u{e},s.mesh.getElement(e).disc.Q);
                data.u{e} = data.u{e}(:,:,end);
            end
            
            data.v = s.getU( 1, s.getElementalAlpha(Variable.v) );
            for e=Parallel3D.MPI.eStart:Parallel3D.MPI.eEnd
                data.v{e} = reshape(data.v{e},s.mesh.getElement(e).disc.Q);
                data.v{e} = data.v{e}(:,:,end);
            end
            
            data.p = s.getU( 1, s.getElementalAlpha(Variable.p) );
            for e=Parallel3D.MPI.eStart:Parallel3D.MPI.eEnd
                data.p{e} = reshape(data.p{e},s.mesh.getElement(e).disc.Q);
                data.p{e} = data.p{e}(:,:,end);
            end
            
            for e=1:s.mesh.getNumLocalElements
                data.x{e,1} = s.mesh.getLocalElement(e).xelem;
                data.y{e,1} = s.mesh.getLocalElement(e).yelem;
            end
            
            data.eStart = Parallel3D.MPI.eStart;
            data.eEnd   = Parallel3D.MPI.eEnd;
            
            save([outputDir 'info-' num2str(id,'%.4d')  '--' num2str(Parallel3D.MPI.rank,'%.3d') '.mat'],'data') ;
        end   

        function plotExact(s,fig,position)
            if nargin==2
                position = 1;
            end
            
            s.plotMode = 1;
            s.plot(fig,position);
            s.plotMode = 0;
        end
        
        function plotInterpolated(s,fig,position)
            if nargin==2
                position = 1;
            end
            
            s.plotMode = 2;
            s.plot(fig,position);
            s.plotMode = 0;
        end
                
        function plotAll(s,fig,data,mesh)
            
            if nargin==3
                mesh = s.mesh;
            end
            
            screensize = get( groot, 'Screensize' );
            sizex = 1600;
            sizey = 600;
            set(fig,'position',[0.5*(screensize(3)-sizex) 0.5*(screensize(4)-sizey) sizex sizey]); 
            
            rows = 1;
            cols = 3;
            
            pbx = s.mesh.x1 - s.mesh.x0;
            pby = s.mesh.y1 - s.mesh.y0;
            
            
            % U-plot
            u = data{1};
            minU    = min( cellfun(@(x) min(x(:)),u) );
            maxU    = max( cellfun(@(x) max(x(:)),u) );
            
            %if minU==0 && maxU==0
                maxU = 1;
            %end
            
            pos = 1;
            delete(subplot(rows,cols,pos))
            subplot(rows,cols,pos)
            xlim([mesh.x0 mesh.x1])
            ylim([mesh.y0 mesh.y1])
            %zlim([minU    maxU   ])
            pbz = maxU - minU;
            pbaspect([pbx pby pbz])
            view([0,90])
            grid off;
            hold on;
            
            for e=1:mesh.numLocalElements
                element = mesh.getLocalElement(e);
                surf(element.xelem,element.yelem,u{e},'FaceColor','interp');
            end
            
            % V-plot
            v = data{2};
            minV    = min( cellfun(@(x) min(x(:)),v) );
            maxV    = max( cellfun(@(x) max(x(:)),v) );
            
            %if minV==0 && maxV==0
                maxV = 1;
            %end
            
            pos = 2;
            delete(subplot(rows,cols,pos))
            subplot(rows,cols,pos)
            xlim([mesh.x0 mesh.x1])
            ylim([mesh.y0 mesh.y1])
            %zlim([minV    maxV   ])
            pbz = maxV - minV;
            pbaspect([pbx pby pbz])
            view([0,90])
            grid off;
            hold on;
            
            v = data{2};
            for e=1:mesh.numLocalElements
                element = mesh.getLocalElement(e);
                surf(element.xelem,element.yelem,v{e},'FaceColor','interp');
            end

            
            % P-plot
            pos = 3;
            delete(subplot(rows,cols,pos))
            subplot(rows,cols,pos)
            p = data{3};
            for e=1:mesh.numLocalElements
                element = mesh.getLocalElement(e);
                surf(element.xelem,element.yelem,p{e},'FaceColor','interp');
                hold on;
            end
%             shading interp
            xlim([mesh.x0 mesh.x1])
            ylim([mesh.y0 mesh.y1])
            grid off;
            pbaspect([pbx pby 1])
            view([0,90])
            
%             % U-velocity at x=0.5
%             pos = 4;
%             delete(subplot(rows,cols,pos))
%             subplot(rows,cols,pos)
%             for e=1:mesh.numLocalElements
%                 element = mesh.getLocalElement(e);
%                 z=interp2(element.xelem,element.yelem,u{e},0.5,element.yelem);
%                 plot(element.yelem,z,'k');
%                 hold on;
%             end
%             xlim([0 1])
%             view([90,-90])
%             
%             % V-velocity at x=0.5
%             pos = 5;
%             delete(subplot(rows,cols,pos))
%             subplot(rows,cols,pos);
%             for e=1:mesh.numLocalElements
%                 element = mesh.getLocalElement(e);
%                 z=interp2(element.xelem,element.yelem,v{e},0.5,element.yelem);
%                 plot(element.yelem,z,'k');
%                 hold on;
%             end
%             ylim([-1e-3 1e-3])
%             view([90,-90]) 
        end
        
        % Main function which calls other functions
        function writeValues(s)
            filename = [ System.settings.outp.directory s.name '-' System.settings.outp.valueFilename '.txt' ];
            
            s.computeMaxVelocityNorm();
            
            if (System.rank==0)
                if (System.settings.time.current==0)
                    header = {'iter','time','maxVelocityNorm'};
                    System.writeHeader(filename,header)
                end

                values = zeros(11,1);
                values(1) = System.settings.time.iter; 
                values(2) = System.settings.time.current;
                values(3) = s.maxVelocityNorm;

                System.appendToFile(filename,values)
            end
        end
        
        % Default refinement criteria (false)
        function criteria = getRefinementCriteria(s,e)
            criteria(1) = false;
        end
        
        function out = save(s,filename)
            % Merge solution from all processors on the master rank
            
            out = [];
            
            %fprintf('Saving the NS to a restart file on rank %d\n',System.rank)
            
            %u = s.fields{1}.getGlobalAlpha;
            %v = s.fields{2}.getGlobalAlpha;
            %p = s.fields{3}.getGlobalAlpha;
            
            NS  = struct;
            
            % First synchronize the fields in parallel
            NS.fields = cell(s.Nvar,1);
            for n=1:s.Nvar
                NS.fields{n} = save( s.fields{n} );
            end
            
            if (Parallel3D.MPI.rank==0 || System.settings.outp.parallelWrite )
                NS.class        = class(s);
                NS.name         = s.name;
                NS.mesh         = s.mesh.save;
                %NS.Re           = s.Re;
                %NS.We           = s.We;
                %NS.Fr           = s.Fr;
                %NS.g            = s.g;
                %NS.alpha        = s.alpha;
                %NS.lamRho       = s.lamRho;
                %NS.lamMu        = s.lamMu;
                %NS.uConvergence	= s.uConvergence;
                %NS.vConvergence = s.vConvergence;
                %NS.pConvergence = s.pConvergence;
                %NS.u            = u;
                %NS.v            = v;
                %NS.p            = p;
                
%                 NS.fields       = cell(s.Nvar,1);
%                 for n=1:s.Nvar
%                     NS.fields{n} = save( s.fields{n} );
%                 end
                
                % Additional NS settings
                NS.steady     	= s.steady;                 % True if the system of equations is steady
                NS.nonlinear   	= s.nonlinear;            	% True if the equations are nonlinear
                NS.coupled      = s.coupled;                % True if variables of other physics are to be used in the equations
                NS.fdata        = s.fdata;
                NS.exact        = s.exact;                  % True if this physics has an exact solution
                NS.maxLevel     = s.maxLevel;               % Maximum level for AMR
                NS.eqWeights    = s.eqWeights;           	% Weights for every equation
                NS.nConvType    = s.nConvType;              % Residual convergence criteria based on [ value; residual ]
                NS.cConvType    = s.cConvType;              % Coupling convergence criteria based on [ value; residual ]
                NS.nMaxIter     = s.nMaxIter;           	% Maximum number of nonlinear iterations
                NS.cMaxIter     = s.cMaxIter;           	% Maximum number of coupling iterations
                NS.nConvergence = s.nConvergence;       	% Nonlinear convergence for each variable
                NS.cConvergence = s.cConvergence;        	% Coupling convergence for each variable
                NS.rConvergence = s.rConvergence;           % Residual convergence for each equation and the whole system
                NS.eps          = s.eps;         
                
                %save(filename,'NS','-append');
            end
            
            out = NS;
        end
        
        function setZeroGravity(s,flag)
            s.zeroGravity = flag;
        end
        
        function computeMaxVelocityNorm(s)
            U = s.getField( Variable.u );
            V = s.getField( Variable.v );
            
            maxVelocityNorm = 0;
            
            for e=s.mesh.eRange                
                % Get dUkdx and dVkdy
                Uk = U.val(FieldType.Current,e);
                Vk = V.val(FieldType.Current,e);
                
                maxU = max(abs(Uk(:)));
                maxV = max(abs(Vk(:)));
                
                maxLocal = max(maxU,maxV);
                
                maxVelocityNorm = max(maxVelocityNorm,maxLocal);
            end
            
            s.maxVelocityNorm = NMPI_Allreduce(maxVelocityNorm,1,'M',Parallel3D.MPI.mpi_comm_world);
            
            %if (Parallel3D.MPI.rank==0)
            %    fprintf('Maximum velocity norm (defined as: max(|U|,|V|)) : %e\n',maxVelocityNorm);
            %end
        end
        
        function out = computeDivergence(s)
            U = s.getField( Variable.u );
            V = s.getField( Variable.v );
            
            L2divTotal = 0;
            check = 0;
            
            for e=s.mesh.eRange
                
                % Set the element
                element = s.mesh.getElement(e);
                
                %[X,Y,Z,T] = element.getNodes;
                
                % Get dUkdx and dVkdy
                dUkdx = U.x(FieldType.Current,e);
                dVkdy = V.y(FieldType.Current,e);
                
                div   = dUkdx + dVkdy;
                
                w     = element.getW;
                
                % Set the integration weights and Jacobian for 2D spatial planes
                %w = finiteElement.W;
                %J = element.J;
      
                L2divTotal = L2divTotal + dot( div(:).^2, w);
                check = check + dot( (1+0*div(:)).^2, w);
            end
            
            %s.massTotal     = NMPI_Allreduce(massTotal   ,Qt,'+',Parallel3D.MPI.mpi_comm_world);
            %s.massEnclosed  = NMPI_Allreduce(massEnclosed,Qt,'+',Parallel3D.MPI.mpi_comm_world);
            %checkGlobal     = NMPI_Allreduce(check,Qt,'+',Parallel3D.MPI.mpi_comm_world);
            
            %s.massLossTotal     = s.massTotal(1)   -s.massTotal(end);
            %s.massLossEnclosed  = s.massEnclosed(1)-s.massEnclosed(end);
            %checkLoss           = check(1)-check(end);
            
            if (Parallel3D.MPI.rank==0)
                fprintf('Total divergence error (L2) : %e (volume check : %e)\n',sqrt(L2divTotal),check);
            end
        end
        
       	function preTimeLoop(s)
            if (s.settings.usePreTimeLoop)
                
                s.preTimeStep();
                
                %% Apply boundary conditions
                s.resetBoundaryConditions();
                Settings.setBoundaryConditions( s );

                % By solving the steady equations, the solution will be
                % obtained faster
                s.steady    = true;
                s.coupled   = false; 
                %s.eps       = 0.05;
                %s.eqWeights = s.settings.eqWeights;
                %s.eqWeights(1) = 0.01;
                %s.eqWeights(2) = 0.01;

    %             %% Apply temporal conditions
    %             if (System.settings.time.spaceTime)
    %                 s.updateTimeSlab();
    %                 if (~s.steady)
    %                     s.applyInitialConditions();
    %                 end
    %             elseif ~isempty(System.settings.time.steppingMethod)
    %                 physics.copySolution(FieldType.Timelevel1);
    %             end

                %% Solve nonlinear equations                
                s.solveNonLinear();

                s.steady    = s.settings.steady;
                s.coupled   = s.settings.coupled;
                %s.eps       = s.settings.eps;
                %s.eqWeights = s.settings.eqWeights;
            end
        end
        
        function preTimeStep(s)
            if (s.settings.usePreTimeLoop)
                % Compute the new the gradP
                CH = PhysicsCollection.get(2);

                CH.computeCenterOfMassVelocity(true,false);       % determine the center-of-mass velocity: compute the enclosed mass, and without overIntegration
                %uc = CH.cmv(:,1);
                vc = CH.cmv_total(:,2);

                denominator = mean(vc);

                % Set the adaptive pressure gradient
                if ( denominator ~= 0)
                    s.gradP(2) = s.gradP(2) ./ denominator;
                end

                if (System.rank==0)
                    fprintf('Current pressure gradient : %e [ abs(mean(vc)) : %e ]\n',s.gradP(2),abs(mean(vc)));
                end
            end
        end
    end
    
    methods (Static)
        function plotParallel(nproc,variable)
            
            fig = figure(1);
            clf
            
            for i=1:nproc
                disp('NEW PROC')
                temp = load(['NS_' int2str(i-1) '.mat']);
                temp.NS.plotVariable(fig,0,variable,'')
                hold on
            end
            
            drawnow;            
        end 
    end
    
end

