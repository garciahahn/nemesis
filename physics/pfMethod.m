classdef pfMethod < int32
   enumeration
      classicCH(0), classicAC(1), classicACH(2), balancedCH(3), balancedAC(4), balancedACH(5), meanKwakkel(6), kappa(7)
   end
end