classdef Stokes3D < Physics
    %STOKES3D Physics file for the steady 3D Stokes problem
    
    properties (Access=protected)
        REI; FRI2; WEI;
        
        % NS properties
        Ja; Pr; Re; We; Fr; g; gx; gy;
    end
    
    methods
        % Class constructor
        function s = Stokes3D(mesh,settings)
            s = s@Physics(mesh);
                        
            if nargin>0
                s.shortName = 'St';
                s.longName  = 'Stokes3D';
                s.name = 'St';

                s.nEq = 8;
                s.variables = {'vU','vV','vW','Wx','Wy','Wz','P'};
                
                s.initialize(settings);
            end
        end
        
        % Function to set all properties 
        function initialize(s,settings)
            % First initialize the basic settings
            initialize@Physics(s,settings);
            
            % Additional settings (physics dependent)
            s.Re        = System.settings.phys.Re;   
            s.We        = System.settings.phys.We;
            s.Fr        = System.settings.phys.Fr;
            s.Pr        = System.settings.phys.Pr;
            s.Ja        = System.settings.phys.Ja;
            s.g         = System.settings.phys.g;
            
            s.REI  = 1.0/s.Re;

            if (s.We>0)
                s.WEI = 1.0/s.We;
            else
                s.WEI = 0;
            end

            if (s.Fr>0)
                s.FRI2 = 1.0/s.Fr;
            else 
                s.FRI2 = 0;
            end                

            if (s.g==0)
                s.gx = 0;
                s.gy = 0;
            else
                s.gx = s.g(1);
                s.gy = s.g(2);
            end
        end
                
        % Setup the Stokes3D equations
        function time = setupEquations(s) %,physics)
            t1 = toc;
            
            for e=s.mesh.eRange
                %t(1) = toc;
                
                %% Select the active element and assing the local element number
                element = s.mesh.getElement(e);
                el = element.localID;
                                
                %% Get all elemental operators/arrays
                H       = element.getH;
                Dx      = element.getDx;
                Dy      = element.getDy;
                Dz      = element.getDz;
                
%                 if (s.steady)
%                     Dt = 0;
%                 else
%                     Dt = element.getDt;
%                 end
%                 Dxx     = element.getDxx;
%                 Dyy     = element.getDyy;
%                 Dxy     = element.getDxy;

                Wel     = element.getW;
                matZ    = element.getMatZ;
                vecZ    = element.getVecZ;
                
                %% Set weights for equations
                %weight1 = s.eqWeights(1)               

                %% Setup L
                TNL= matZ; %STOKES
                %TNL=(S.Dz*Jz+diag(S.var.U_it_e)*S.Dx*Jx+diag(S.var.V_it_e)*S.Dy*Jy); 

                
                %             U         V       W       Wx           Wy           Wz           P                
                s.opL{el} = [ Dx        Dy      Dz      matZ        matZ         matZ       matZ; ...
                              TNL       matZ    matZ    matZ        -s.REI*Dz   s.REI*Dy    Dx; ...
                              matZ      TNL     matZ    s.REI*Dz    matZ        -s.REI*Dx   Dy; ...
                              matZ      matZ    TNL     -s.REI*Dy   s.REI*Dx    matZ        Dz; ...
                              matZ       Dz     -Dy      H          matZ        matZ        matZ; ...
                              -Dz       matZ     Dx     matZ         H          matZ        matZ; ...
                               Dy       -Dx     matZ    matZ        matZ         H          matZ; ...
                              matZ      matZ    matZ     Dx          Dy          Dz         matZ];     

                %% Setup G
                s.opG{el} = [vecZ+1; vecZ; vecZ; vecZ; vecZ; vecZ; vecZ; vecZ];
                
                %% AMR scaling
                Ze = s.mesh.Z{e};
                s.opL{el} = s.opL{el} * superkron(eye(s.Nvar),Ze);
                
                %% Weights for finite elements
                s.W{el}      = repmat(Wel,s.nEq,1);
                s.sqrtW{el}  = repmat(sqrt(Wel),s.nEq,1);
            end
            
            t2 = toc;
            time = t2-t1;
        end
        
        function preTimeStep(s)
        end
        
        function writeValues(s)
        end
        
        function out = save(s)
            % Merge solution from all processors on the master rank
            
            % Initialize the default output from the settings of the
            % current object as a struct
            out  = struct;
            
            % First synchronize the fields in parallel
            out.fields = cell(s.Nvar,1);
            for n=1:s.Nvar
                out.fields{n} = save( s.fields{n} );
            end
            
            if (Parallel3D.MPI.rank==0 || System.settings.outp.parallelWrite )
                out.class        = class(s);
                out.name         = s.name;
                out.mesh         = s.mesh.save;
                
                out.settings     = s.settings;

                % Additional NS settings
                out.steady     	= s.steady;                 % True if the system of equations is steady
                out.nonlinear   	= s.nonlinear;            	% True if the equations are nonlinear
                out.coupled      = s.coupled;                % True if variables of other physics are to be used in the equations
                out.fdata        = s.fdata;
                out.exact        = s.exact;                  % True if this physics has an exact solution
                out.maxLevel     = s.maxLevel;               % Maximum level for AMR
                out.eqWeights    = s.eqWeights;           	% Weights for every equation
                out.nConvType    = s.nConvType;              % Residual convergence criteria based on [ value; residual ]
                out.cConvType    = s.cConvType;              % Coupling convergence criteria based on [ value; residual ]
                out.nMaxIter     = s.nMaxIter;           	% Maximum number of nonlinear iterations
                out.cMaxIter     = s.cMaxIter;           	% Maximum number of coupling iterations
                out.nConvergence = s.nConvergence;       	% Nonlinear convergence for each variable
                out.cConvergence = s.cConvergence;        	% Coupling convergence for each variable
                out.rConvergence = s.rConvergence;           % Residual convergence for each equation and the whole system
                
                out.usePreTimeLoop     = s.usePreTimeLoop;
                out.usePreTimeStep     = s.usePreTimeStep;
            end
        end
        
    end
    
end