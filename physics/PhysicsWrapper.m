classdef PhysicsWrapper
    %PHYSICSWRAPPER Collection of all available physics
    %   The PhysicsWrapper provides easy access to all available variables,
    %   without knowledge of the underlying system.
    
    properties
        allPhysics
    end
    
    methods
        function obj = PhysicsWrapper(inputArg1,inputArg2)
            %UNTITLED6 Construct an instance of this class
            %   Detailed explanation goes here
            obj.Property1 = inputArg1 + inputArg2;
        end
        
        function outputArg = getField(obj,inputArg)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            outputArg = obj.Property1 + inputArg;
        end
    end
end

