classdef CahnHilliard3D < CahnHilliard
    %CahnHilliard2D Implementation of the 2D Cahn-Hilliard equation
    %   Detailed explanation goes here

    properties
        % no extra properties required
    end
    
    methods
        
        function s = CahnHilliard3D(mesh,settings)
            s = s@CahnHilliard(mesh);
            
            s.shortName = 'CH';
            s.longName  = 'CahnHilliard';
            s.name      = 'CH';
            s.nEq       = 2;
            s.variables = {'c','omg'};
            
            s.initialize(settings);       
        end
        
        function initialize(s,settings)
            initialize@CahnHilliard(s,settings);
            
            activeSettings = System.settings;
           
            % General physical settings
            s.alpha     = activeSettings.phys.alpha;
            s.Pe        = activeSettings.phys.Pe;   % Peclet number
            s.Ca        = activeSettings.phys.Ca;   % Capillary number
            s.Cn        = activeSettings.phys.Cn;   % Cahn number
            s.Cn_L      = activeSettings.phys.Cn * activeSettings.phys.radius;   % Cahn number
            s.M         = activeSettings.phys.M;    % mobility    
            s.We        = activeSettings.phys.We;   % Weber number
            s.Pr        = activeSettings.phys.Pr;
            s.Ja        = activeSettings.phys.Ja;
            s.Re        = activeSettings.phys.Re;
            s.lamRho    = activeSettings.phys.lamRho;
            
            if (s.We>0)
                s.WEI = 1.0/s.We;
            else
                s.WEI = 0;
            end
            
            if (s.Pe>0)
                s.PEI = 1.0/s.Pe;
            else
                s.PEI = 0;
            end
            
            % Cahn-Hilliard specific settings
            if (isfield(settings,'mobilityType'))
                s.mobilityType = settings.mobilityType;
            end
            
            % Cahn-Hilliard specific settings
            if (isfield(settings,'stabilize'))
                s.stabilize = settings.stabilize;
            end
            
            % Cahn-Hilliard specific settings
            if (isfield(settings,'correctionMethod'))
                s.correctionMethod = settings.correctionMethod;
            end
            
            % Cahn-Hilliard specific settings
            if (isfield(settings,'explicitDivergence'))
                s.explicitDivergence = settings.explicitDivergence;
            end
            
            % Cahn-Hilliard specific settings
            if (isfield(settings,'pfMethod'))
                s.pfMethod = settings.pfMethod;
            elseif (isfield(settings,'settings') && isfield(settings.settings,'pfMethod'))
                s.pfMethod = settings.settings.pfMethod;
            end
            
            % Cahn-Hilliard specific settings
            if (isfield(settings,'meanKappa'))
                s.meanKappa = settings.meanKappa;
            else
                s.meanKappa = 0;
            end
            
            % Cahn-Hilliard specific settings
            if (s.correctionMethod==1)
                s.useMeanCurvature = true;
            end
        end

        function time = setupEquations_3D(s)
            t1 = toc;
            
            U   = FieldCollection.get('u');
            V   = FieldCollection.get('v');
            W   = FieldCollection.get('w');
            C   = FieldCollection.get('c');
            Omg = FieldCollection.get('omg');
            
            if (~System.settings.comp.coupling.enabled || ~s.coupled)
                U = NaN;
                V = NaN;
                W = NaN;
                Uk = 0;
                Vk = 0;
                Wk = 0;
            %else
            %    fprintf('WARNING : NO COUPLING OF CH WITH ANY OTHER PHYSICS')
            end
            
            %% Curvature
            %k = s.getCurvature_3D(s.mesh);
            
            % Droplet/bubble center of gravity velocity at temporal
            % quadrature points
            if (System.settings.phys.dynamicReferenceFrame)
                s.computeCenterOfMassVelocity(true,false);       % determine the center-of-mass velocity: compute the enclosed mass, and without overIntegration
                uc = s.cmv_total(:,1) - System.settings.phys.bubbleVelocity(1);
                vc = s.cmv_total(:,2) - System.settings.phys.bubbleVelocity(2);
                %fprintf('WARNING : USING DYNAMIC REFERENCE FRAME')
            end
            
            %% CH) Setup operators
            for e=s.mesh.eRange
                
                % Set active element
                element = s.mesh.getElement(e);
                
                Ck          = C.val(FieldType.Current,e);
                dCkdt       = C.t(FieldType.Current,e);
                dCkdx       = C.x(FieldType.Current,e);
                dCkdy       = C.y(FieldType.Current,e);
                dCkdz       = C.z(FieldType.Current,e);
                dCkdxx      = C.xx(FieldType.Current,e);
                dCkdyy      = C.yy(FieldType.Current,e);
                dCkdzz      = C.zz(FieldType.Current,e);
                
                Omgk        = Omg.val(FieldType.Current,e);
                dOmgkdx     = Omg.x(FieldType.Current,e);
                dOmgkdy     = Omg.y(FieldType.Current,e);

%                 gradC2 = dCkdx.^2 + dCkdy.^2;
%                 gradC  = sqrt(gradC2);
%                 m2 = Ck.^2 .* (Ck-1).^2;
                
%                 dOmgkdxx    = [];
%                 
%                 if (isempty(dOmgkdxx))
%                     f1 = 1;
%                     dCkdxx   = 0;
%                     dCkdyy   = 0;
%                     dOmgkdxx = 0;
%                     dOmgkdyy = 0;
%                 else
%                     %fprintf('f1 is set to 0\n')
%                     f1 = 0;
%                 end
                
                % Elemental operators
                H   = element.getH;
                Dx  = element.getDx;
                Dy  = element.getDy;
                Dz  = element.getDz;
                                
                if (s.steady)                    
                    %fprintf('WARNING : STEADY CH')
                    Dt = 0; %element.getH;
                    steady = 0; %Ck;
                else
                    Dt       = s.getTime( element );
                    dCdt_rhs = s.getTime( element, C );
                    steady = 0;
                end
                
                Dxx  = element.getDxx;
                Dyy  = element.getDyy;
                Dzz  = element.getDzz;
                Dxy  = element.getDxy;
                Dxz  = element.getDxz;
                Dyz  = element.getDyz;
                
                Wel  = element.getW;
                matZ = element.getMatZ;
                vecZ = element.getVecZ;
                Ze   = s.mesh.Z{e};
                
                % Variables
                if ( U==U )
                    Uk = U.val2(e,s.mesh);
                    ux = U.x2(e,s.mesh);
                else
                    Uk = 0;
                    ux = 0;
                end
                
                if ( V==V )
                    Vk = V.val2(e,s.mesh);
                    vy = V.y2(e,s.mesh);
                else
                    Vk = 0;
                    vy = 0;
                end
                
                if ( W==W )
                    Wk = W.val2(e,s.mesh);
                    wz = W.z2(e,s.mesh);
                else
                    Wk = 0;
                    wz = 0;
                end
                
                % Moving reference frame
                if (System.settings.phys.dynamicReferenceFrame && s.coupled && System.settings.comp.coupling.enabled)
                    %fprintf('WARNING : USING DYNAMIC REFERENCE FRAME')
                    
                    % x-direction
                    if ( ~isempty(uc) )
                        Uk = reshape( Uk, element.finite.qSize );
                        for i=1:numel(uc)
                            Uk(:,:,i) = Uk(:,:,i) - uc(i);
                        end
                        Uk = Uk(:);
                    end
                    
                    % y-direction
                    if ( ~isempty(vc) )
                        Vk = reshape( Vk, element.finite.qSize );
                        for i=1:numel(vc)
                            Vk(:,:,i) = Vk(:,:,i) - vc(i);
                        end
                        Vk = Vk(:);
                    end
                end
                
                f1 = 1;
                
                % Parts of equations
                Conv    = (Uk.*Dx + Vk.*Dy + Wk.*Dz);
                Nabla2  = Dxx + Dyy + Dzz;
                                
                % Get the alpha values for C and Omg
                Cs   = C.alp(FieldType.Current,e);
                Omgs = Omg.alp(FieldType.Current,e);    %(SolutionMode.Nonlinear);

                switch s.mobilityType
                    case 1
                        % By default mobilityType == 1 is selected
                        Mobil = 1;
                        auxi = 0;
                    case 2
                        % Mobility type as used by Keunsoo in one of his articles
                        Mobil = (Ck - Ck.^2);
                        auxi  = ( 1 - 2*Ck );
                    case 3
                        % Enclosed mass conerving mobility, it must be a function of (C-0.5)
                        Mobil = (Ck - 0.5);
                        auxi  = 1;
                end
               
                theta = 0.5;
                
                Mphi = - (1/s.Pe) * ( auxi * (  theta) .*((Dx*Omgs).*Dx + (Dy*Omgs).*Dy)                 );
                Momg = - (1/s.Pe) * ( auxi * (1-theta) .*((Dx*Cs  ).*Dx + (Dy*Cs  ).*Dy) + Mobil.*Nabla2 );
                
%                 if (s.correctionMethod==3)
%                     Mphi = - (1/s.Pe) * ( auxi * (  theta) .*((Dx*Omgs).*Dx + (Dy*Omgs).*Dy) - 0*(dCkdx.*dOmgkdx + dCkdy.*dOmgkdy).*(Dxx+Dyy)./gradC2 );
%                     Momg = - (1/s.Pe) * ( auxi * (1-theta) .*((Dx*Cs  ).*Dx + (Dy*Cs  ).*Dy) + Mobil.*( dCkdy.^2 .* Dxx - 2*dCkdx.*dCkdy.*Dxy + dCkdx.^2 .* Dyy )./(gradC2) );
%                 end
                                
                el = element.localID;
                
                weight1 = s.eqWeights(1);
                weight2 = s.eqWeights(2);

                % Options for CH equations:
                %   1) normal use
                %   2) extra Dt for omega equation (ensure that the initial condition for omega is also set every timestep!)
                %   3) split scheme
%                 CH_option = 4;
                
%                 % Option 1
%                 if (CH_option == 1)
%                     f1      = 1;
%                     lapC    = 0;
%                     L22     = H;
%                     %Fenergy_impl = (3*Ck.^2 - 3*Ck + 0.5).*H - f1 * s.Cn_L^2 * ( Nabla2 .*( 1 - 2*s.Cn_L*gradC./(sqrt(2*s.Cn_L^2*gradC2+m2)) )  );         % 3*C^3 - 3*C^2 + 0.5*C - Cn^2 * (Cxx+Cyy)
%                     Fenergy_impl = (3*Ck.^2 - 3*Ck + 0.5).*H - f1 * s.Cn_L^2 .* ( Nabla2 );
%                     Fenergy_expl = (2*Ck.^3 - 1.5*Ck.^2) + 0*(1-f1) * s.Cn_L^2 * ( dCkdxx + dCkdyy );
% 
%                 elseif (CH_option == 2)
%                     f1      = 1;
%                     lapC    = 0;
%                     L22     = Dt;
%                     Fenergy_impl = (3*Ck.^2 - 3*Ck + 0.5).*Dt - f1 * s.Cn_L^2 * ( Nabla2t );         % 3*C^3 - 3*C^2 + 0.5*C - Cn^2 * (Cxx+Cyy)
%                     Fenergy_expl = 0*(2*Ck.^2 - 1.5*Ck.^1) .* dCkdt + (1-f1) * s.Cn_L^2 * ( dCkdxx + dCkdyy );
%                 
%                 elseif (CH_option == 3)
%                     f1      = 1;
%                     lapC    = 0;
%                     L22     = H;
% %                     Fenergy_impl = (3*Ck.^2 - 3*Ck + 0.5).*H - 2* f1 * s.Cn_L^2 * ( dCkdx.^2 .* Dxx + 2*dCkdx.*dCkdy.* Dxy + dCkdy.^2 .* Dyy ) ./ (gradC2);
%                     Fenergy_impl = (Ck.^2 - 3/2*Ck + 0.5).*H - 2* f1 * s.Cn_L^2 * ( dCkdx.^2 .* Dxx + 2*dCkdx.*dCkdy.* Dxy + dCkdy.^2 .* Dyy ) ./ (gradC2);
%                     Fenergy_expl = 0*(2*Ck.^3 - 1.5*Ck.^2);
%                     
%                 else 
%                    Fenergy_expl = 0*Ck;
%                    Fenergy_impl = -s.Cn_L^2 * Nabla2;
%                    lapC = -(1/s.Pe) * 6*(Ck-0.5) .* Nabla2;
%                    L22 = H;
%                 end
                
%                 f2 = 1/s.Cn_L;
                
                % Option 2
                %Fenergy_impl = 0*(-s.Cn^2 * ( Nabla2 ));
                %Fenergy_expl = (-Ck.^3 + 1.5*Ck.^2 - 0.5*Ck + s.Cn^2 * (dCkdxx+dCkdyy));

                %try
                % Operators L & G
                %               C                 Omega

%                 normC = dCkdx.^2+dCkdy.^2;
                              
%                 if (CH_option==3)
%                     s.opL{el} = [ weight1.*(Dt+Conv+Mphi+ lapC )               weight1.* f1 * (Momg)       ; ...  % (s.Cn/s.Cn_L)
%                                   weight2.*f2*(Fenergy_impl)                   -weight2.*L22              ];
% 
%                     s.opG{el} = [ weight1 .* (vecZ+steady + 0*(1/s.Pe)* (1-f1) * Mobil*(dOmgkdxx+dOmgkdyy) )          ; ...
%                                   weight2 .* f2*(Fenergy_expl) ];             
%                               
%                 elseif (CH_option==4)
                    % New approach where the normal second derivative is
                    % the additional variable instead of the classical
                    % omega.
                    
                    normC1 = dCkdx.^2 + dCkdy.^2 + dCkdz.^2;        % + 1e-10;
                    %normC2 = Ck.^2.*(1-Ck).^2 / (2*s.Cn_L.^2);
                    %normOmg1 = dOmgkdx.^2+dOmgkdy.^2;
                    
                    %normU = sqrt( Uk.^2 + Vk.^2 );
                    %normU( normU < 1e-1 ) = 1e-1;                    
                    %L11 = normU .* (Dt + Conv + Mphi);
                    
                    if (s.explicitDivergence)
                        divU = (ux + vy + wz) .* H;
                    else
                        divU = 0;
                    end
                    
                    L11 = Dt + Conv + Mphi + divU; % + k{el}.*(Dx+Dy+Dz); % - 1/s.We * s.alpha * sqrt(normOmg1) .* H;
                    %L12 = Momg;
                    
                    % Classic CH
                    switch s.pfMethod
                        case pfMethod.classicCH
                            L12 = -1/s.Pe * (Dxx + Dyy + Dzz);
                            L21 = 1/s.Cn_L * (3*Ck.^2 - 3*Ck + 0.5).*H - s.Cn_L * (Dxx + Dyy + Dzz);
                            
                        case pfMethod.classicAC
                            L12 = 1/s.We * s.alpha .* H;
                            L21 = 1/s.Cn_L * (3*Ck.^2 - 3*Ck + 0.5).*H - s.Cn_L * (Dxx + Dyy + Dzz);
                            
                        case pfMethod.balancedCH    % with directional second derivative
                            L12 = -1/s.Pe * (Dxx + Dyy + Dzz);
                            L21 = 1/s.Cn_L * (3*Ck.^2 - 3*Ck + 0.5).*H - s.Cn_L * ( (   dCkdx.^2 .* Dxx   +   dCkdy.^2.*Dyy     +   dCkdz.^2.*Dzz     + ...
                                                                                      2*dCkdx.*dCkdy.*Dxy + 2*dCkdx.*dCkdz.*Dxz + 2*dCkdy.*dCkdz.*Dyz )./ normC1);
                            
                        case pfMethod.balancedAC    % with directional second derivative
                            L12 = 1/s.We * s.alpha .* H;
                            L21 = 1/s.Cn_L * (3*Ck.^2 - 3*Ck + 0.5).*H - s.Cn_L * ( (   dCkdx.^2 .* Dxx   +   dCkdy.^2.*Dyy     +   dCkdz.^2.*Dzz     + ...
                                                                                      2*dCkdx.*dCkdy.*Dxy + 2*dCkdx.*dCkdz.*Dxz + 2*dCkdy.*dCkdz.*Dyz )./ normC1);
                            
                        case pfMethod.balancedACH 	% with directional second derivative
                            L12 = 1/s.We * s.alpha .* H - 1/s.Pe * (Dxx + Dyy + Dzz);
                            L21 = 1/s.Cn_L * (3*Ck.^2 - 3*Ck + 0.5).*H - s.Cn_L * ( (   dCkdx.^2 .* Dxx   +   dCkdy.^2.*Dyy     +   dCkdz.^2.*Dzz     + ...
                                                                                      2*dCkdx.*dCkdy.*Dxy + 2*dCkdx.*dCkdz.*Dxz + 2*dCkdy.*dCkdz.*Dyz )./ normC1);
                        
                        case pfMethod.classicACH
                            L12 = 1/s.We * s.alpha .* H - 1/s.Pe * (Dxx + Dyy + Dzz);
                            L21 = 1/s.Cn_L * (3*Ck.^2 - 3*Ck + 0.5).*H - s.Cn_L * (Dxx + Dyy + Dzz);
                    end
                    
                    
                    %factor = Ck.*(1-Ck)./(s.Cn_L*sqrt(2)*sqrt(normC));
                    %limit = 2;
                    
                    L22 = -H;
                    
                    G1  = dCdt_rhs + vecZ;
                    
                    % without correction:
                    G2  = 1/s.Cn_L * (2*Ck.^3 - 1.5*Ck.^2);
                    
%                     % Dynamic bc
%                     if (isempty(element.neighbor{1,2}))                        
%                         qLocations = (element.finite.logicalCoordinates(2,:)==-1);
%                          
%                         L12( qLocations, : ) = L12( qLocations, : ) - 1/s.Pe * Dy( qLocations, : );
%                         L21( qLocations, : ) = L21( qLocations, : ) - s.Cn_L * Dy( qLocations, : );        % adds eps * grad_n(C) * cos(theta) to L21 at the lower wall
%                     end
                    
                    % with correction:
                    %G2  = -1 + 1/s.Cn_L * (2*Ck.^3 - 1.5*Ck.^2) .* (1-1./factor);
                    
                    s.opL{el} = [ L11	L12  ; ...
                                  L21	L22 ];

                    s.opG{el} = [ G1  ; ...
                                  G2 ];
                    
%                 end
                                            
                if (s.withForcing)
                    s.opG{el} = s.opG{el} - s.forcing{el};
                end
                
                %% Set quadrature weights, apply all scaling to L and G
                s.setWeightAndScale( element );
            end 
            
            t2 = toc;
            time = t2-t1;
        end

        function out = getMassTotal(s)            
            out(1) = s.massTotal(end);
            out(2) = s.massLossTotal;
        end
        
        function out = getMassEnclosed(s)            
            out(1) = s.massEnclosed(end);
            out(2) = s.massLossEnclosed;
        end
        
        function out = getCM(s)
            out(1) = s.cm(end,1);       % cmx at end of time slab
            out(2) = s.cm(end,2);       % cmy at end of time slab
        end
        
        % Function to get the velocity of the center of mass
        function out = getCMV(s)
            out(1) = s.cmv_total(end,1);       % cmx at end of time slab
            out(2) = s.cmv_total(end,2);       % cmy at end of time slab
        end

        function createInterface(s)

            cField        = s.getField(Variable.c,s);
            mesh          = cField.mesh;
            cData         = cField.value([],[],3);
            C             = cell(mesh.numLocalElements,1);
            
            %% Interface settings
            s.interface         = struct();
            s.interface.value   = 0.5;
            s.interface.active  = true;
            s.interface.props   = {'Color', 'k', 'LineWidth', 2};
            s.interface.x       = cell(mesh.numLocalElements,1);
            s.interface.y       = cell(mesh.numLocalElements,1);
            %interface.z        = cell(mesh.numLocalElements,1);                     
            
            %% Extract interface 
            % Find the contour where the C surface is equal to the
            % requested interface value. Next, extract the x- and y-locations 
            % from the contour matrix C. Interpolate on the C surface
            % to find z-locations for the intersection. Finally plot the
            % line with some properties.
            for e=1:mesh.numLocalElements
                element = mesh.getLocalElement(e);
                
%                 [X,Y] = meshgrid( element.nodes{1:end-1} );
                [X,Y] = element.getNodes;
                X = squeeze(X);
                Y = squeeze(Y);
                
                C{e} = contourc(X,Y,cData{e}(:,:,end)',[s.interface.value s.interface.value]);
                if (~isempty(C{e}))
%                     s.interface.x{e} = C{e}(1, 2:1+C{e}(2,1));
%                     s.interface.y{e} = C{e}(2, 2:1+C{e}(2,1));
                    s.interface.x{e} = C{e}(1, 2:end);
                    s.interface.y{e} = C{e}(2, 2:end);
                    if C{e}(2,1)~=size(C{e},2)-1
                       s.interface.x{e}(:, C{e}(2,1)+1 ) = [];
                       s.interface.y{e}(:, C{e}(2,1)+1 ) = [];
                       s.interface.x{e}(:, 1:C{e}(2,1) ) = fliplr( s.interface.x{e}(:, 1:C{e}(2,1) ) );
                       s.interface.y{e}(:, 1:C{e}(2,1) ) = fliplr( s.interface.y{e}(:, 1:C{e}(2,1) ) );
                    end
                    %interface.z{e} = interp2(X{e}(1,:)', Y{e}(:,1), dataVar{e}, interface.x{e}, interface.y{e});
                    %line(s.interface.x{e}, s.interface.y{e}, repmat(s.interface.value,size(s.interface.x{e})), s.interface.props{:});
                end
            end
        end
        
        %% PLOT FUNCTIONS
        % Function to plot the mean curvature as computed from the C field
        function plotH(s)
            % The C function is defined by: C = 0.5 + 0.5 * tanh( z / (2*sqrt(2)*Cn) )
            % From this function the half width parameter w is defined as w = 2*sqrt(2)*Cn
            % Furthermore, we define m = C * (C-1)
            
            s.computeH2(s.mesh,true);
            
            for e=s.mesh.eRange                
                element = s.mesh.getElement(e);
                el = element.localID;
                
                [X,Y] = element.getNodes;
                
                X = squeeze(X); Y = squeeze(Y);
                
                meanCurvature = reshape( s.H{el}, element.finite.qSize );
                
                surface(X(:,:,end),Y(:,:,end),meanCurvature(:,:,end));
                hold on;
            end
            
            x0 = s.mesh.X(1,1);
            x1 = s.mesh.X(2,1);
            y0 = s.mesh.X(1,2);
            y1 = s.mesh.X(2,2);
            
            pbx = x1 - x0;
            pby = y1 - y0;
            
            grid off;
            pbaspect([pbx pby 1])

            zlim([-2 2])
            
            %shading interp
            view([0,90])
        end
        
        % Function to plot the mean curvature as computed from the C field
        function plotHGradC(s)
            % The C function is defined by: C = 0.5 + 0.5 * tanh( z / (2*sqrt(2)*Cn) )
            % From this function the half width parameter w is defined as w = 2*sqrt(2)*Cn
            % Furthermore, we define m = C * (C-1)
            
            s.computeH2(s.mesh,true);
            
            C = s.fields{1};
            
            for e=s.mesh.eRange                
                element = s.mesh.getElement(e);
                el = element.localID;
                
                [X,Y] = element.getNodes;
                
                cx  = C.x2(e);
                cy  = C.y2(e);
                gradC = sqrt( cx.^2 + cy.^2 );
                
                X = squeeze(X); Y = squeeze(Y);
                
                meanCurvature = reshape( s.H{el}, element.finite.qSize );
                gradC         = reshape( gradC  , element.finite.qSize );
                
                surface(X(:,:,end),Y(:,:,end),meanCurvature(:,:,end).*gradC);
                hold on;
            end
            
            x0 = s.mesh.X(1,1);
            x1 = s.mesh.X(2,1);
            y0 = s.mesh.X(1,2);
            y1 = s.mesh.X(2,2);
            
            pbx = x1 - x0;
            pby = y1 - y0;
            
            grid off;
            pbaspect([pbx pby 1])

            zlim([-2 20])
            
            %shading interp
            view([0,90])
        end
        
        % Function to plot the mean curvature as computed from the C field
        function plotH2_av(s)
            % The C function is defined by: C = 0.5 + 0.5 * tanh( z / (2*sqrt(2)*Cn) )
            % From this function the half width parameter w is defined as w = 2*sqrt(2)*Cn
            % Furthermore, we define m = C * (C-1)
            
            s.computeH2_av(s.mesh,true);
            
            for e=s.mesh.eRange                
                element = s.mesh.getElement(e);
                el = element.localID;
                
                [X,Y] = element.getNodes;
                
                X = squeeze(X); Y = squeeze(Y);
                
                meanCurvature = reshape( s.H{el}, element.finite.qSize );
                
                surface(X(:,:,end),Y(:,:,end),meanCurvature(:,:,end));
                hold on;
            end
            
            zlim([-2 2])
        end
        
        % Function to plot the mean curvature as computed from the C field
        function plotDelta(s)
            % The C function is defined by: C = 0.5 + 0.5 * tanh( z / (2*sqrt(2)*Cn) )
            % From this function the half width parameter w is defined as w = 2*sqrt(2)*Cn
            % Furthermore, we define m = C * (C-1)
            
            s.computeH(s.mesh,true);
            
            C = s.fields{1};
            
            for e=s.mesh.eRange                
                element = s.mesh.getElement(e);
                el = element.localID;
                
                [X,Y] = element.getNodes;
                
                X = squeeze(X); Y = squeeze(Y);
                
                c      = reshape(C.getValue(el),element.finite.qSize);
                cxx    = reshape(C.xx(FieldType.Current,el),element.finite.qSize);
                cyy    = reshape(C.yy(FieldType.Current,el),element.finite.qSize);
                
                [X,Y]    = element.getNodes;
                                
                c = c(:,:,end);
                
                %potential = s.alpha/s.Cn * s.Cn/s.Cn_L * ( 1*(c.^3-3/2*c.^2+1/2*c) - 1*s.Cn_L^2*(cxx+cyy) );
                potential = s.alpha/s.Cn * ( 1*(c.^3-3/2*c.^2+1/2*c) - 1*s.Cn_L^2*(cxx+cyy) );
                
                lhs = potential;
                rhs = s.Cn_L/s.Cn * s.H{el};
                
                delta = lhs-rhs;
                
                surface(X(:,:,end),Y(:,:,end),delta(:,:,end));
                hold on;
            end
        end
        
        function plotInterface(s,plotCurv)
            % Function to plot the interface, which is defined by C=1/2.
            % For a 2D setup it will be a curve, and for a 3D setup a
            % surface
            
            if nargin==1
                plotCurv = false;
            end
            
            scale = 5;
            
            integrateHO = false;
            if (integrateHO)
                plotOrder = 10;
                plotFE = FiniteElement(System.settings.stde.Pn,plotOrder*ones(size(System.settings.stde.Q)),System.settings.stde.C,s.mesh.sDim,s.mesh.spaceTime);
            end
            
            s.computeH2_av;
%             s.computeH2;
            
            hold on

            xL_all = [];
            yL_all = [];
            val_all = [];
            
            for e=s.mesh.eRange
                
                % Set the element
                element = s.mesh.getElement(e);
                
                if (integrateHO)
                    element.setFinite( plotFE );
                end
                
                [x,y,z,t] = element.getNodes();
                
                x = squeeze(x);
                y = squeeze(y);
                t = squeeze(t);
                
                c = s.fields{1};
                
                cValues = reshape( c.val(FieldType.Current,e), size(x) );
                
%                 if ( s.timeMethod == TimeMethod.SpaceTime )
%                     lEnd = 2;
%                 else
%                     lEnd = 1;
%                 end
%                 
%                 if l==1
%                     pos = 1;
%                 else
%                     pos = size(x,3);
%                 end

                % 3D
                %p = patch(isosurface(x(:,:,l),y(:,:,l),t(:,:,l),cValues(:,:,l),0.5));
                %verts = get(p, 'Vertices');
                %faces = get(p, 'Faces');

                % 2D
                % create a contour object for C = 0.5
                %p = contour(x(:,:,pos),y(:,:,pos),cValues(:,:,pos),[0.5 0.5]);

                %p = contourc(x(:,1),y(1,:),cValues(:,:)',[0.5 0.5]);
                p = contours(x, y, cValues, [0.5 0.5]);
                
%                 Iblur = 3 * imgaussfilt( 1.0*(cValues>0.5) , 2.0);
%                 Iblur = 3 * (1.0*(cValues>=0.5));
%                 Iblur = cValues;
                
                if (~isempty(p))
                    % Extract the x- and y-locations from the contour matrix C.
                    xL = p(1, 2:end);
                    yL = p(2, 2:end);
                    
                    if isempty(xL_all)
                        xL_all = xL(1:end)';
                        yL_all = yL(1:end)';
                    else
                        xL_all = [ xL_all ; NaN; xL(1:end)' ];
                        yL_all = [ yL_all ; NaN; yL(1:end)' ];
                    end
                    
                    if (plotCurv)
                        values = plotCurv*scale* 2*s.H{e};
%                         values = imgaussfilt( values, 0.2);
                        
                    else
                        values = scale * ones( size(x) );
                    end
                    
                    % Interpolate on the first surface to find z-locations for the intersection line.
                    zL = interp2(x', y', values', xL, yL,'linear');
                    
%                     val_all = [val_all zL];
                    
                    % Visualize the line.
%                     line(xL, yL, zL, 'Marker','s','Color', 'r', 'LineWidth', 3);
                    
                    Idx = knnsearch([x(:) y(:)],[xL' yL'],'K',2); % nearest neighbor indices
                    
                    xN = x(Idx);
                    yN = y(Idx);
                    zN = ones( size(xN) );
                   
                    scatter3(xN(:),yN(:),zN(:),[],'g','Marker','s','LineWidth', 3);
                    
                    F1 = scatteredInterpolant(xL(:),yL(:),zL(:));
                    
                    if numel(zL)>2
                        F1.ExtrapolationMethod = 'nearest';
                        curv = F1(x,y);
                        curv = mean(curv(:)) * ones( size(x)) ;
                    else
                        curv = mean(xL) * ones( size(x)) ;
                    end
%                     z  = interp2(xL,yL,zL,x,y);

                    surf(x,y,curv)
                end
                
%                 surf(x,y,Iblur)
                
                surface(x,y,zeros(size(x)));
                
                
                if (integrateHO)
                    element.setFinite(); % reset to computation finite element
                end
                
%                     % use the contour object to compute the linear line length 
%                     % for each segment (p(1,1)==0.5 and p(2,1)=number of segments)
%                     if (~isempty(p))
%                         diff = zeros(2,p(2,1)-1);
%                         for n=1:p(2,1)-1
%                             diff(1,n) = p(1,n+2) - p(1,n+1);
%                             diff(2,n) = p(2,n+2) - p(2,n+1);
%                             diff(3,n) = sqrt(diff(1,n)^2 + diff(2,n)^2);
%                         end
%                         length(l) = length(l) + sum(diff(3,:));
%                     end
            end
            
%             [xL_all yL_all] = polymerge(xL_all,yL_all,1e-5);
%             p=[xL_all yL_all];
%             
% %             for i=2:size(p,1)
% %                 if ( norm(p(i-1,:)-p(i,:)) < 1e-4 )
% %                     p(i,:) = ( p(i-1,:) + p(i,:) ) /2;
% %                     p(i-1,:) = NaN; 
% %                 end
% %             end
%             
%             % remove NaN
%             p(any(isnan(p), 2), :) = [];
%             
%             for i=2:size(p,1)
%                 if ( norm(p(i-1,:)-p(i,:)) < 1e-4 )
%                     p(i,:) = ( p(i-1,:) + p(i,:) ) /2;
%                     p(i-1,:) = NaN; 
%                 end
%             end
%             
%             % compare start/end
%             if ( norm(p(1,:)-p(end,:)) < 1e-4 )
%                 p(end,:) = ( p(1,:) + p(end,:) ) /2;
%                 p(1,:) = NaN; 
%             end
%             
%             % remove NaN
%             p(any(isnan(p), 2), :) = [];
%             
%             p2 = interparc(1000,p(:,1),p(:,2));
%             p2 = p;
%             
%             curv = LineCurvature2D(p2);
%             line(p2(:,1), p2(:,2), abs(curv), 'Marker','s','Color', 'b', 'LineWidth', 3);
            
%             clf;plot(val_all,'Marker','s'); hold on;test = imgaussfilt3( val_all, 5,'Padding','circular');plot(test,'r')

            x0 = s.mesh.X(1,1);
            x1 = s.mesh.X(2,1);
            y0 = s.mesh.X(1,2);
            y1 = s.mesh.X(2,2);
            
            pbx = x1 - x0;
            pby = y1 - y0;
            
            grid off;
            pbaspect([pbx pby 1])

            xlim([x0 x1])
            ylim([y0 y1])
            zlim([-1 6])
            
            %shading interp
            view([30,30])
        end
        
        %% COMPUTE FUNCTIONS
        % Function to plot the mean curvature as computed from the C field
        function computeH(s,mesh,useMeanCurvature)
            % The C function is defined by: C = 0.5 + 0.5 * tanh( z / (2*sqrt(2)*Cn) )
            % From this function the half width parameter w is defined as w = 2*sqrt(2)*Cn
            % Furthermore, we define m = C * (C-1)
            
            if (nargin==1)
                mesh = s.mesh;
                useMeanCurvature = s.useMeanCurvature;
            elseif (nargin==2)
                useMeanCurvature = s.useMeanCurvature;
            end
            
            s.H = cell( numel(mesh.eRange),1 );
            
            if (useMeanCurvature)
                w = 2*sqrt(2) * s.Cn_L;
                C = s.fields{1};

                for e=mesh.eRange
                    element = mesh.getElement(e);
                    el = element.localID;

                    c   = C.val2(e,mesh);
                    cx  = C.x2(e,mesh);
                    cy  = C.y2(e,mesh);
                    cxx = C.xx2(e,mesh);
                    cyy = C.yy2(e,mesh);
                    cxy = C.xy2(e,mesh);

                    gradC = sqrt( cx.^2 + cy.^2 );

                    m = c .* (c-1);

%                     H_enum  = -0.5*w * ( (2*s.Cn_L^2 * (cy).^2 + m.^2).*cxx ...
%                                        + (2*s.Cn_L^2 * (cx).^2 + m.^2).*cyy ...
%                                        -  4*s.Cn_L^2 * cx.*cy.*cxy ...
%                                        -  2*m.*(c-0.5) .* (cx.^2+cy.^2) );
% %                     H_enum  = -0.5*w * ( -(2*s.Cn_L^2 * (cx).^2).*cxx ...
% %                                        - (2*s.Cn_L^2 * (cy).^2).*cyy ...
% %                                        -  4*s.Cn_L^2 * cx.*cy.*cxy ...
% %                                        -  2*m.*(c-0.5) .* (cx.^2+cy.^2) );
%                     H_denom = ( 2*s.Cn_L^2 * ( cx.^2 + cy.^2 ) + m.^2 ) .^(3/2);
% 
%                     H = H_enum ./ H_denom;
%                     meanCurvature = (1 / sqrt(2)) .* H; %* s.Cn_L^2 .* H;
                    
                    H_enum  = -0.5*s.Cn_L * ( (0.25*m.^2 + 0.5*s.Cn_L^2 * (cy).^2).*cxx ...
                                            + (0.25*m.^2 + 0.5*s.Cn_L^2 * (cx).^2).*cyy ...
                                            -  s.Cn_L^2 * cx.*cy.*cxy ...
                                            -  0.5*m.*(c-0.5) .* (cx.^2+cy.^2) );
                    H_denom = ( 0.25*m.^2 + 0.5*s.Cn_L^2 * (cx.^2+cy.^2) ) .^(3/2);
                    
                    H = H_enum ./ H_denom;
                    meanCurvature = H;

                    s.H{el} = reshape( meanCurvature, element.finite.qSize );
                end
                
            else
                for e=1:numel(mesh.eRange)
                    s.H{e} = 0;
                end
            end
        end
        
        % Function to plot the mean curvature as computed from the C field
        function computeH2(s,mesh,useMeanCurvature)
            % The C function is defined by: C = 0.5 + 0.5 * tanh( z / (2*sqrt(2)*Cn) )
            % From this function the half width parameter w is defined as w = 2*sqrt(2)*Cn
            % Furthermore, we define m = C * (C-1)
            
            if (nargin==1)
                mesh = s.mesh;
                useMeanCurvature = s.useMeanCurvature;
            elseif (nargin==2)
                useMeanCurvature = s.useMeanCurvature;
            end
            
            useMeanCurvature = true;
            
            s.H = cell( numel(mesh.eRange),1 );
            
            if (useMeanCurvature)
                C = s.fields{1};

                for e=mesh.eRange
                    element = mesh.getElement(e);
%                     element.setFinite();
                    
                    el = element.localID;

                    %c   = C.val2(e,mesh);
                    cx  = C.x2(e,mesh);
                    cy  = C.y2(e,mesh);
                    cxx = C.xx2(e,mesh);
                    cyy = C.yy2(e,mesh);
                    cxy = C.xy2(e,mesh);  
                                        
                    gradC = sqrt( cx.^2 + cy.^2 );
                    
                    %H_enum  = gradC.^2 .* (cxx+cyy) - (cx.^2.*cxx + 2*cx.*cy.*cxy + cy.^2.*cyy );
                    H_enum  = -cy.^2.*cxx + 2*cx.*cy.*cxy - cx.^2.*cyy;     % equal to: gradC^T * H(C) * gradC - gradC.^2 * Lap(C)
                    H_denom = gradC.^3;
                    
                    H = H_enum ./ H_denom;
                    
                    H( abs(H_denom)<1e-8 ) = 0;
                    meanCurvature = 0.5 * H;    % NB: the last term is a Jacobian
                    
                    s.H{el} = reshape( meanCurvature, element.finite.qSize );
                end
                
            else
                for e=1:numel(mesh.eRange)
                    s.H{e} = 0;
                end
            end
        end
        
        % Function to plot the mean curvature as computed from the C field
        function computeH2_av(s,mesh,useMeanCurvature)
            % The C function is defined by: C = 0.5 + 0.5 * tanh( z / (2*sqrt(2)*Cn) )
            % From this function the half width parameter w is defined as w = 2*sqrt(2)*Cn
            % Furthermore, we define m = C * (C-1)
            
            if (nargin==1)
                mesh = s.mesh;
                useMeanCurvature = s.useMeanCurvature;
            elseif (nargin==2)
                useMeanCurvature = s.useMeanCurvature;
            end
            
            useMeanCurvature = true;
            
            s.H = cell( numel(mesh.eRange),1 );
            
            if (useMeanCurvature)
                C = s.fields{1};

                % Compute the mean for Cxx
                C.xx2_av_init;
                C.yy2_av_init;
                
                for e=mesh.eRange
                    element = mesh.getElement(e);
                    el = element.localID;
                    
%                     element.setFinite();

                    c   = C.val2(e,mesh);
                    cx  = C.x2(e,mesh);
                    cy  = C.y2(e,mesh);

                    cxx = C.xx2_mean(e);
                    cyy = C.yy2_mean(e);
                    
                    %cxx = C.xx2(e,mesh);
                    %cyy = C.yy2(e,mesh);
                    
                    cxy = C.xy2(e,mesh);  
                                        
                    normC = sqrt( cx.^2 + cy.^2 );
                    
                    %H_enum  = gradC.^2 .* (cxx+cyy) - (cx.^2.*cxx + 2*cx.*cy.*cxy + cy.^2.*cyy );
                    H_enum  = -cy.^2.*cxx + 2*cx.*cy.*cxy - cx.^2.*cyy;     % equal to: gradC^T * H(C) * gradC - gradC.^2 * Lap(C)
                    H_denom = normC.^3;
                    
                    H = H_enum ./ H_denom;
                    
                    H( abs(H_denom)<1e-8 ) = 0;
                    meanCurvature = 0.5 * H;    % NB: the last term is a Jacobian
                    
                    s.H{el} = meanCurvature;
                    %s.H{el}( abs(c-0.5)>0.499 ) = 0;
                    
                    s.H{el} = reshape( s.H{el}, element.finite.qSize );
                end
                
            else
                for e=1:numel(mesh.eRange)
                    s.H{e} = 0;
                end
            end
        end

        % Function to plot the mean curvature as computed from the C field
        function computeH2_3D(s,mesh,useMeanCurvature)
            % The C function is defined by: C = 0.5 + 0.5 * tanh( z / (2*sqrt(2)*Cn) )
            % From this function the half width parameter w is defined as w = 2*sqrt(2)*Cn
            % Furthermore, we define m = C * (C-1)
            
            if (nargin==1)
                mesh = s.mesh;
%                 useMeanCurvature = s.useMeanCurvature;
            elseif (nargin==2)
%                 useMeanCurvature = s.useMeanCurvature;
            end
            
            useMeanCurvature = true;
            
            s.H = cell( numel(mesh.eRange),1 );
            
            if (useMeanCurvature)
                C = s.fields{1};

                for e=mesh.eRange
                    element = mesh.getElement(e);
                    el = element.localID;

                    c   = C.val2(e,mesh);
                    cx  = C.x2(e,mesh);
                    cy  = C.y2(e,mesh);
                    cz  = C.z2(e,mesh);
                    cxx = C.xx2(e,mesh);
                    cyy = C.yy2(e,mesh);
                    czz = C.zz2(e,mesh);
                    cxy = C.xy2(e,mesh);
                    cxz = C.xz2(e,mesh);
                    cyz = C.yz2(e,mesh);
                                        
                    gradC = sqrt( cx.^2 + cy.^2 + cz.^2 );
                    
                    %H_enum  = gradC.^2 .* (cxx+cyy) - (cx.^2.*cxx + 2*cx.*cy.*cxy + cy.^2.*cyy );
                    %H_enum  = -cy.^2.*cxx + 2*cx.*cy.*cxy - cx.^2.*cyy;     
                    
                    % H_enum = gradC^T * H(C) * gradC - gradC.^2 * Lap(C)
                    H_enum  =   cx.^2.*cxx  +   cy.^2.*cyy  +   cz.^2.*czz  + ...
                              2*cx.*cy.*cxy + 2*cx.*cz.*cxz + 2*cy.*cz.*cyz - ...
                              gradC.^2 .* ( cxx + cyy + czz );
                    H_denom = gradC.^3;
                    
                    H = H_enum ./ H_denom;
                    
                    H( abs(H_denom)<1e-8 ) = 0;
                    meanCurvature = 0.5 * H;
                    
                    s.H{el} = reshape( meanCurvature, element.finite.qSize );
                end
                
            else
                for e=1:numel(mesh.eRange)
                    s.H{e} = 0;
                end
            end
        end
        
        function computeG(s)
            % The C function is defined by: C = 0.5 + 0.5 * tanh( z / (2*sqrt(2)*Cn) )
            % From this function the half width parameter w is defined as w = 2*sqrt(2)*Cn
            % Furthermore, we define m = C * (C-1)
            
            w = 2*sqrt(2) * s.Cn_L;
            C = s.fields{1};
            
            s.G = cell( numel(s.mesh.eRange),1 );
            
            for e=s.mesh.eRange
                element = s.mesh.getElement(e);
                el = element.localID;
                
                c   = C.val('Current',e);                
                cx  = C.x('Current',e);
                cy  = C.y('Current',e);
                cxx = C.xx('Current',e);
                cyy = C.yy('Current',e);
                cxy = C.xy('Current',e);
                
                m = c .* (1-c);
%                 
%                 G_enum  = -4*w * ( (2*s.Cn_L^2 * ( cx.^2 .* cxx + cy.^2.*cyy + 2*cx.*cy.*cxy...
%                                    -  2*m.*(c-0.5) .* (cx.^2+cy.^2) );
%                 G_denom = ( 2*s.Cn_L^2 * ( cx.^2 + cy.^2 ) + m.^2 );
                
                G = G_enum ./ G_denom;
                
                meanCurvature = G_enum ./ G_denom;
                
                s.G{el} = reshape( meanCurvature, element.finite.qSize );
                
                %surface(X(:,:,end),Y(:,:,end),meanCurvature(:,:,end));
                %hold on;
            end
        end
        
        function out = getMeanKappa(s)
            s.computeH();
            
            out = cell(numel(s.H),1);
            
            for i=1:numel(s.H)
                out{i} = s.H{i}(:);
                %out{i} = s.alpha/(4*sqrt(2)) * s.Cn_L/s.Cn * s.H{i}(:);
            end
        end
        
        function kappa = getKappa(s)
            switch s.curvatureMethod
                case CurvatureMethod.divNormal
                    kappa = s.computeKappa;
                case CurvatureMethod.LScorrected 
                    kappa = s.computeKappaGamma;
                case CurvatureMethod.classicCH
%                     kappa = s.fields{2}.subFieldList{1}.val;                % omega values
%                     for e=1:numel(kappa)                                    % scale with alpha
%                         kappa{e} = s.alpha * kappa{e};
%                     end
                    kappa = s.computeKappa;
                otherwise
                    error('This curvatureMethod is not supported!\n')
            end 
        end
        
        
        function [kx,ky] = getCurvature(s,mesh)
            if (nargin==1)
                mesh = s.mesh;
            end
            
            kx = cell(numel(s.H),1);
            ky = cell(numel(s.H),1);
            
            C   = s.fields{1};
            
            kappa = s.getKappa;
            
            for e=mesh.eRange
                element = mesh.getElement(e);
                el = element.localID;

                dCkdx   = C.x2(e,mesh);
                dCkdy   = C.y2(e,mesh);

                switch s.curvatureSharpening
                    case CurvatureSharpening.gradC0
                        % The norm of the curvature becomes equal to |kappa|
                        gradC  = sqrt( dCkdx.^2 + dCkdy.^2 );
                        kx{el} = kappa{el}(:) .* dCkdx ./ gradC;
                        ky{el} = kappa{el}(:) .* dCkdy ./ gradC;
                        
                    case CurvatureSharpening.gradC1
                        % The norm of the curvature becomes equal to |kappa| * |gradC|
                        kx{el} = kappa{el}(:) .* dCkdx;
                        ky{el} = kappa{el}(:) .* dCkdy;
                        
                    case CurvatureSharpening.gradC2
                        % The curvature (kappa) is defined as 2*H, where H is the mean curvature. Therefore, 
                        % the norm of the curvature of this method is equal to |kappa| * |gradC|^2
                        gradC  = sqrt( dCkdx.^2 + dCkdy.^2 );
                        scaler = s.alpha * s.Cn_L * gradC;
                        kx{el} = scaler .* kappa{el}(:) .* dCkdx;
                        ky{el} = scaler .* kappa{el}(:) .* dCkdy;
                        
                    case CurvatureSharpening.smoothHeaviside
                        Ck     = C.val2(e,mesh);
                        [Ds,Dx,Dy] = Delta(Ck,s.heavisideThickness);
                        kx{el} = kappa{el}(:) .* Ds .* dCkdx;
                        ky{el} = kappa{el}(:) .* Ds .* dCkdy;

                        %kx{el} = kappa{el}(:) .* Dx(:) * 50;
                        %ky{el} = kappa{el}(:) .* Dy(:) * 50;
                        
                        % Is the scaling here correct? Check for stationary
                        % droplet first before using this option for
                        % unsteady problems!
                end
            end
        end
        
        function [kLapC] = getCurvatureLapC(s,mesh)
            if (nargin==1)
                mesh = s.mesh;
            end
            
            kLapC = cell(numel(s.H),1);
            
            C   = s.fields{1};
            
            kappa = s.getKappa;
            
            for e=mesh.eRange
                element = mesh.getElement(e);
                el = element.localID;

                dCkdx   = C.x2(e,mesh);
                dCkdy   = C.y2(e,mesh);
                Cxx   = C.xx2(e,mesh);
                Cyy   = C.yy2(e,mesh);

                switch s.curvatureSharpening
                    case CurvatureSharpening.gradC0
                        % The norm of the curvature becomes equal to |kappa|
                        gradC  = sqrt( dCkdx.^2 + dCkdy.^2 );
                        kx{el} = kappa{el}(:) .* dCkdx ./ gradC;
                        ky{el} = kappa{el}(:) .* dCkdy ./ gradC;
                        
                    case CurvatureSharpening.gradC1
                        % The norm of the curvature becomes equal to |kappa| * |gradC|
                        kx{el} = kappa{el}(:) .* dCkdx;
                        ky{el} = kappa{el}(:) .* dCkdy;
                        
                    case CurvatureSharpening.gradC2
                        % The curvature (kappa) is defined as 2*H, where H is the mean curvature. Therefore, 
                        % the norm of the curvature of this method is equal to |kappa| * |gradC|^2
                        gradC  = sqrt( dCkdx.^2 + dCkdy.^2 );
                        scaler = s.alpha * s.Cn_L * gradC;
                        kx{el} = scaler .* kappa{el}(:) .* dCkdx;
                        ky{el} = scaler .* kappa{el}(:) .* dCkdy;
                        
                    case CurvatureSharpening.smoothHeaviside
                        Ck     = C.val2(e,mesh);
                        [Ds] = Delta(Ck,s.heavisideThickness);
                        kLapC{el} = kappa{el}(:) .* Ds .* (Cxx+Cyy);
                        
                        %ky{el} = kappa{el}(:) .* Ds .* dCkdy;

                        %kx{el} = kappa{el}(:) .* Dx(:) * 50;
                        %ky{el} = kappa{el}(:) .* Dy(:) * 50;
                        
                        % Is the scaling here correct? Check for stationary
                        % droplet first before using this option for
                        % unsteady problems!
                end
            end
        end
        
        
%         function out = getKappa(s)
%             mesh = s.mesh;
%             
%             %% Step A : Initialize/update the kappa Field
%             kappaField = FieldCollection.get('kappa');
%             
%             if (~isa(kappaField,'Field'))
%                 fieldID  = 6;
%                 activate = true;
%                 periodic = [ false false  ;...
%                              false false ];
%                 kappaField = Field(s.mesh,fieldID,'kappa',periodic);
%                 FieldCollection.add( kappaField );
%                 kappaField.addSubField(FieldType.Current,activate,[],[],[],true);
%             else
%                 % if kappaField exists, verify if the mesh has been updated
%                 kappaField.meshUpdate;
%             end
%             
%             %save(['getSurfaceTension.' int2str(System.rank) '.mat']);
% 
%             % Load fields to be used
%             C = s.fields{1};
%             
%             % Compute the mean curvature
%             s.computeH2_av(mesh,1);
% 
%             % Compute kappa and store as a field
%             for e=mesh.eRange
%                 element = mesh.getElement(e);
%                 el      = element.localID;
% 
%                 dCkdx   = C.x2(e,mesh);
%                 dCkdy   = C.y2(e,mesh);
%                 normC   = sqrt( dCkdx.^2 + dCkdy.^2 );
% 
% %                 value = 2 * s.H{el}(:); %.* normC ) );           % both alpha and Cn_L are used for scaling
%                 %value = (s.alpha) * ( s.Cn_L * ( 2 * s.H{el}(:) .* normC ) );           % both alpha and Cn_L are used for scaling
%                 value = ( 2 * s.H{el}(:) .* normC );           % both alpha and Cn_L are used for scaling
%                 
%                 kappaField.setValue(el,value);
%             end
% 
%             % Ensure the alpha values are computed for the field
%             kappaField.computeAlpha;
%             
%             out = kappaField;
%         end
        
        function [st] = getSurfaceTension(s,mesh)
            % Function to compute the surface tension term as required for
            % the 3 staged NS equations. This term is the divergence of the
            % surface tension term used in the single staged NS equations.
            
            if (nargin==1)
                mesh = s.mesh;
            end
                        
            %% Step A : Initialize/update the kappa Field
            kappaField = FieldCollection.get('kappa');
            
            if (~isa(kappaField,'Field'))
                fieldID  = 6;
                activate = true;
                periodic = [ false false  ;...
                             false false ];
                kappaField = Field(s.mesh,fieldID,'kappa',periodic);
                FieldCollection.add( kappaField );
                kappaField.addSubField(FieldType.Current,activate,[],[],[],true);
            else
                % if kappaField exists, verify if the mesh has been updated
                kappaField.meshUpdate;
            end
            
            %save(['getSurfaceTension.' int2str(System.rank) '.mat']);

            % Load fields to be used
            C = s.fields{1};
            
            % Compute the mean curvature
            s.computeH2_av(mesh,1);

            % Compute kappa and store as a field
            for e=mesh.eRange
                element = mesh.getElement(e);
                el      = element.localID;

                dCkdx   = C.x2(e,mesh);
                dCkdy   = C.y2(e,mesh);
                normC   = sqrt( dCkdx.^2 + dCkdy.^2 );

%                 value = 2 * s.H{el}(:); %.* normC ) );           % both alpha and Cn_L are used for scaling
                %value = (s.alpha) * ( s.Cn_L * ( 2 * s.H{el}(:) .* normC ) );           % both alpha and Cn_L are used for scaling
                value = ( 2 * s.H{el}(:) .* normC );           % both alpha and Cn_L are used for scaling
                
                kappaField.setValue(el,value);
            end

            % Ensure the alpha values are computed for the field
            kappaField.computeAlpha;

            %% Step B : Compute the surface tension term for a staged NS
            st = cell(numel(s.H),1);
            
            % Compute the mean for Cxx
            C.xx2_av_init;
            C.yy2_av_init;
            
            for e=mesh.eRange
                element = mesh.getElement(e);
                el      = element.localID;
                
                Cx  = C.x2(e);
                Cy  = C.y2(e);
                Cxx = C.xx2_mean(e);
                Cyy = C.yy2_mean(e);
                
                k   = kappaField.val2(e);
                kx  = kappaField.x2(e);
                ky  = kappaField.y2(e);
                
                % By setting kappa = 2*H*|grad(C)|, the surface tension
                % contribution is given by: 
                %   st = div( kappa * grad(C) )
                %      = ( kappa_x * cx + kappa_y * cy ) + kappa * Lapl(C)
                st{el} = kx.*Cx + ky.*Cy + k.*(Cxx+Cyy);
            end
        end
        
        function [ssfx,ssfy] = getSSF(s,mesh)
            if (nargin==1)
                mesh = s.mesh;
            end
            
            s.computeH2_av(mesh,1);
            
            ssfx = cell(numel(s.H),1);
            ssfy = cell(numel(s.H),1);
            
            C   = s.fields{1};
            Omg = s.fields{2};

            % Compute the mean for Cxx
            C.xx2_av_init;
            C.yy2_av_init;

            v1 = [0.5;ones(6,1);0.5];
            scaler = kron(v1,v1);
            
            % Mean curvature correction
            for e=mesh.eRange
                element = mesh.getElement(e);
                el = element.localID;

                Ck      = C.val2(e,mesh);
                Omgk    = Omg.val2(e,mesh);
                dCkdx   = C.x2(e,mesh);
                dCkdy   = C.y2(e,mesh);
                %Cxx     = C.xx2(e,mesh);
                %Cyy     = C.yy2(e,mesh);

                Cxx     = C.xx2_mean(e);
                Cyy     = C.yy2_mean(e);

                gradC   = sqrt( dCkdx.^2 + dCkdy.^2 );

%                         kxx = 2 * (s.alpha) * ( 0*Omgk + 1*s.Cn_L * ( 2 * s.H{el}(:) .* gradC ) ) .* Cxx;
%                         kyy = 2 * (s.alpha) * ( 0*Omgk + 1*s.Cn_L * ( 2 * s.H{el}(:) .* gradC ) ) .* Cyy;
                kxx = (s.alpha) *( s.Cn_L * ( 2 * s.H{el}(:) ) ) .* Cxx;
                kyy = (s.alpha) *( s.Cn_L * ( 2 * s.H{el}(:) ) ) .* Cyy;

%                         kxx = Cxx;
%                         kyy = Cyy;

                %ssf{el} = kxx + kyy;

                ssfx{el} = reshape(2.0*(Ck>=0.5),element.finite.qSize);
                ssfy{el} = reshape(2.0*(Ck>=0.5),element.finite.qSize);

                imax = element.finite.qSize(1);
                jmax = element.finite.qSize(2);

                for j=1:jmax
                    for i=1:imax
                        if (i<imax)
                            if ssfx{el}(i,j)<=0 && ssfx{el}(i+1,j)>0
                                ssfx{el}(i  ,j)=-1;
                                ssfx{el}(i+1,j)= 1;
                            end
                        end
                        if j<jmax
                            if ssfy{el}(i,j)<=0 && ssfy{el}(i,j+1)>0
                                ssfy{el}(i,j  )=-1;
                                ssfy{el}(i,j+1)= 1;
                            end
                        end
                        if (i>1)
                            if ssfx{el}(i,j)<=0 && ssfx{el}(i-1,j)>0
                                ssfx{el}(i  ,j)=-1;
                                ssfx{el}(i-1,j)= 1;
                            end
                        end
                        if j>1
                            if ssfy{el}(i,j)<=0 && ssfy{el}(i,j-1)>0
                                ssfy{el}(i,j  )=-1;
                                ssfy{el}(i,j-1)= 1;
                            end
                        end
                    end
                end

                ssfx{el}( ssfx{el}==2 ) = 0;
                ssfy{el}( ssfy{el}==2 ) = 0;
                
%                 ssf{el} = -scaler .* ssf{el}(:);
                ssfx{el} = -ssfx{el}(:);
                ssfy{el} = -ssfy{el}(:);
            end

            
        end
        
        function [kx,ky,kz] = getCurvature_3D(s,mesh)
            if (nargin==1)
                mesh = s.mesh;
            end
            
            s.computeH2_3D(mesh,1);
            
            kx = cell(numel(s.H),1);
            ky = cell(numel(s.H),1);
            kz = cell(numel(s.H),1);
            
            C   = s.fields{1};
            Omg = s.fields{2};
            
%             selected = s.correctionMethod;
            
            selected = 1;
            
            switch selected
                case 1
                    % Mean curvature correction
                    for e=mesh.eRange
                        element = mesh.getElement(e);
                        el = element.localID;

                        Omgk    = Omg.val2(e,mesh);
                        dCkdx   = C.x2(e,mesh);
                        dCkdy   = C.y2(e,mesh);
                        dCkdz   = C.z2(e,mesh);
                        
                        gradC   = sqrt( dCkdx.^2 + dCkdy.^2 + dCkdz.^2 );
                        
                        kx{el} = (s.alpha) * ( 0*Omgk + 1*s.Cn_L * ( 2 * s.H{el}(:) .* gradC ) ) .* dCkdx;
                        ky{el} = (s.alpha) * ( 0*Omgk + 1*s.Cn_L * ( 2 * s.H{el}(:) .* gradC ) ) .* dCkdy;
                        kz{el} = (s.alpha) * ( 0*Omgk + 1*s.Cn_L * ( 2 * s.H{el}(:) .* gradC ) ) .* dCkdz;
                    end
                    
%                 case 2
%                     % Curvature from : -alpha * (C-1/2) * grad( omega )
%                     for e=mesh.eRange
%                         element = mesh.getElement(e);
%                         el = element.localID;
% 
%                         Ck        = C.val2(e,mesh);
%                         dOmgkdx   = Omg.x2(e,mesh);
%                         dOmgkdy   = Omg.y2(e,mesh);
%                         
%                         kx{el} = -s.alpha * (Ck-0.5).*dOmgkdx;
%                         ky{el} = -s.alpha * (Ck-0.5).*dOmgkdy;
%                     end
%                     
%                 otherwise
%                     % Any other correction
%                     for e=mesh.eRange
%                         element = mesh.getElement(e);
%                         el = element.localID;
% 
%                         Omgk    = Omg.val2(e,mesh);
%                         dCkdx   = C.x2(e,mesh);
%                         dCkdy   = C.y2(e,mesh);
%                         
%                         gradC   = sqrt( dCkdx.^2 + dCkdy.^2 );
%                         
%                         kx{el} = s.alpha * Omgk.*dCkdx;
%                         ky{el} = s.alpha * Omgk.*dCkdy;
%                         %kx{el} = (s.alpha) * ( Omgk + s.Cn_L^2 * ( 2 * s.H{el}(:) .* gradC ) ) .* dCkdx;
%                         %ky{el} = (s.alpha) * ( Omgk + s.Cn_L^2 * ( 2 * s.H{el}(:) .* gradC ) ) .* dCkdy;
%                     end
            end
        end

        function [kxx,kyy,kzz] = getCurvature2_3D(s,mesh)
            if (nargin==1)
                mesh = s.mesh;
            end
            
            s.computeH2_3D(mesh,1);
            
            kxx = cell(numel(s.H),1);
            kyy = cell(numel(s.H),1);
            kzz = cell(numel(s.H),1);
            
            C   = s.fields{1};
            Omg = s.fields{2};
            
%             selected = s.correctionMethod;
            
            selected = 1;
            
            switch selected
                case 1
                    % Mean curvature correction
                    for e=mesh.eRange
                        element = mesh.getElement(e);
                        el = element.localID;

                        Omgk  = Omg.val2(e,mesh);
                        dCkdx = C.x2(e,mesh);
                        dCkdy = C.y2(e,mesh);
                        dCkdz = C.z2(e,mesh);
                        cxx   = C.xx2(e,mesh);
                        cyy   = C.yy2(e,mesh);
                        czz   = C.zz2(e,mesh);
                        
                        gradC   = sqrt( dCkdx.^2 + dCkdy.^2 + dCkdz.^2 );
                        
                        kxx{el} = (s.alpha) * ( 0*Omgk + 1*s.Cn_L * ( 2 * s.H{el}(:) .* gradC ) ) .* cxx;
                        kyy{el} = (s.alpha) * ( 0*Omgk + 1*s.Cn_L * ( 2 * s.H{el}(:) .* gradC ) ) .* cyy;
                        kzz{el} = (s.alpha) * ( 0*Omgk + 1*s.Cn_L * ( 2 * s.H{el}(:) .* gradC ) ) .* czz;
                    end
            end
        end

        function [dfr] = getDiffusiveFlowRate(s,mesh)
            if nargin==1
                mesh = s.mesh;
            end
            
            dfr = cell(numel(mesh.eRange),1);
            Omg = s.fields{2};
            
            for e=mesh.eRange
                element = mesh.getElement(e);
                el = element.localID;
                dfr{el} = 1/s.Pe * (s.Cn/s.Cn_L)*( Omg.xx2(e,mesh) + Omg.yy2(e,mesh) );
            end
        end
        
        % Returns the rhs of the phase-field equation (scalar value in each
        % quadrature point)
        function [mf] = getMassFlux(s,mesh)
            if nargin==1
                mesh = s.mesh;
            end
            
            mf  = cell(numel(mesh.eRange),1);
            C   = s.fields{1};
            Omg = s.fields{2};
            
            for e=mesh.eRange
                element = mesh.getElement(e);
                el = element.localID;

                Omgk    = Omg.val2(e,mesh);
                dCkdx   = C.x2(e,mesh);
                dCkdy   = C.y2(e,mesh);
                Cxx     = C.xx2(e,mesh);
                Cxy     = C.xy2(e,mesh);
                Cyy     = C.yy2(e,mesh);
                
                normC2  = sqrt( dCkdx.^2 + dCkdy.^2 );
                
                %mf{el} = -1/s.We * s.alpha .* Omgk + 1/s.Pe * (dCkdx.^2 .* Cxx + 2*dCkdx.*dCkdy.*Cxy + dCkdy.^2.*Cyy) ./ normC2;
                mf{el} = -1/s.We * s.alpha .* Omgk + 1/s.Pe * (Cxx + Cyy);
            end
        end
        
        function plotAll(s,fig,~,mesh)
            verbose = false;
            
            if nargin==3
                mesh = s.mesh;
            end
            
            %screensize = get( groot, 'Screensize' );
            %sizex = 1600;
            %sizey = 478;
            %set(fig,'position',[0.5*(screensize(3)-sizex) 0.5*(screensize(4)-sizey) sizex sizey]); 
            
            rows = 1;
            cols = s.Nvar+1;
            
            s.createInterface;

            
            defaultProps = {'FaceColor','interp','edgecolor','none'};
            %shading interp
                        
            %% Get the meshgrid for each element
            [xGrids,yGrids] = mesh.getMeshgrids;
%             
%             for e=1:mesh.numLocalElements
%                 element = mesh.getLocalElement(e);
%                 [X{e},Y{e}] = meshgrid(element.xelem,element.yelem);
%             end            
%             if (verbose)
%                 fprintf('Total time meshgrid   : %f\n',toc-t1)
%             end
            
            %% Extract interface     
%             t1          = toc;
%             var         = Variable.c;
%             dataVar     = data{var};
                        
%             C             = cell(mesh.numLocalElements,1);
%             s.interface.x = cell(mesh.numLocalElements,1);
%             s.interface.y = cell(mesh.numLocalElements,1);
%             %interface.z = cell(mesh.numLocalElements,1);
%             
%             % Find the contour where the C surface is equal to the
%             % requested interface value. Next, extract the x- and y-locations 
%             % from the contour matrix C. Interpolate on the C surface
%             % to find z-locations for the intersection. Finally plot the
%             % line with some properties.
%             for e=1:mesh.numLocalElements
%                 element = mesh.getLocalElement(e);
%                 C{e} = contourc(element.xelem,element.yelem,dataVar{e},[s.interface.value s.interface.value]);
%                 if (~isempty(C{e}))
% %                     s.interface.x{e} = C{e}(1, 2:1+C{e}(2,1));
% %                     s.interface.y{e} = C{e}(2, 2:1+C{e}(2,1));
%                     s.interface.x{e} = C{e}(1, 2:end);
%                     s.interface.y{e} = C{e}(2, 2:end);
%                     if C{e}(2,1)~=size(C{e},2)-1
%                        s.interface.x{e}(:, C{e}(2,1)+1 ) = [];
%                        s.interface.y{e}(:, C{e}(2,1)+1 ) = [];
%                        s.interface.x{e}(:, 1:C{e}(2,1) ) = fliplr( s.interface.x{e}(:, 1:C{e}(2,1) ) );
%                        s.interface.y{e}(:, 1:C{e}(2,1) ) = fliplr( s.interface.y{e}(:, 1:C{e}(2,1) ) );
%                     end
%                     %interface.z{e} = interp2(X{e}(1,:)', Y{e}(:,1), dataVar{e}, interface.x{e}, interface.y{e});
%                     %line(s.interface.x{e}, s.interface.y{e}, repmat(s.interface.value,size(s.interface.x{e})), s.interface.props{:});
%                 end
%             end
%             if (verbose)
%                 fprintf('Total time interface : %f\n',toc-t1)
%             end
            
            %% C plot
            position    = 1;
            sub         = subplot(rows,cols,position);
            cla(sub);
            s.plotVar(Variable.c);
            
%             t1          = toc;
%             var         = Variable.c;
%             dataVar     = data{var};
%             minC        = min( cellfun(@(x) min(x(:)),dataVar) );
%             maxC        = max( cellfun(@(x) max(x(:)),dataVar) );
%             
%             % plot settings
%             delete(subplot(rows,cols,var))
%             subplot(rows,cols,var)
%             xlim([mesh.x0 mesh.x1])
%             ylim([mesh.y0 mesh.y1])
%             pbaspect([pbx pby 1])
%             view([0,90])
%             grid off;
%             hold on;
%             
%             for e=1:mesh.numLocalElements
%                 surface(X{e},Y{e},dataVar{e},defaultProps{:});
%                 if s.interface.active && ~isempty(s.interface.x{e})
%                     line(s.interface.x{e}, s.interface.y{e}, repmat(maxC,size(s.interface.x{e})), s.interface.props{:});
%                 end
%             end
%             if (verbose)
%                 fprintf('Total time C   : %f\n',toc-t1)
%             end

            %% Omega plot
            position    = 2;
            sub         = subplot(rows,cols,position);
            cla(sub);
            s.plotVar(Variable.omg);
            
            
%             t1          = toc; 
%             var         = Variable.omg;
%             dataVar     = data{var};
%             maxOmega    = max( cellfun(@(x) max(x(:)),dataVar) );
%             
%             delete(subplot(rows,cols,var))
%             subplot(rows,cols,var)            
%             xlim([mesh.x0 mesh.x1])
%             ylim([mesh.y0 mesh.y1])
%             grid off;
%             pbaspect([pbx pby 1])
%             view([0,90])
%             hold on;
%             
%             for e=1:mesh.numLocalElements
%                 surface(X{e},Y{e},dataVar{e},defaultProps{:});                
%                 if interface.active && ~isempty(interface.x{e})
%                     line(interface.x{e}, interface.y{e}, repmat(maxOmega,size(interface.x{e})),interface.props{:});
%                 end
%             end
%             if (verbose)
%                 fprintf('Total time Omg : %f\n',toc-t1)
%             end
            
            %% Surface tension plot
            t1      = toc;
            var     = 3;
            delete(subplot(rows,cols,var))
            s.plotSurfaceTension( subplot(rows,cols,var), s.interface );
            if (verbose)
                fprintf('Total time ST  : %f\n',toc-t1)
            end
            
%             dataVar = data{var};
%             for e=1:s.mesh.getNumLocalElements
%                 element = s.mesh.getLocalElement(e);
%                 surf(element.xelem,element.yelem,dataVar{e});
%                 hold on;
%             end
%             shading interp
%             xlim([s.mesh.x0 s.mesh.x1])
%             ylim([s.mesh.y0 s.mesh.y1])
%             grid off;
%             pbaspect([pbx pby 1])
%             view([0,90])
            
%             % c-plot
%             delete(subplot(2,1,1))
%             subplot(2,1,1)
%             c = data{1};
%             for e=1:s.mesh.getNumLocalElements
%                 element = s.mesh.getLocalElement(e);
%                 surf(element.xelem,element.yelem,c{e});
%                 hold on;
%             end
%             xlim([s.mesh.x0 s.mesh.x1])
%             view([30,30])
%             %view([0,90])
            
%             % omg-plot
%             delete(subplot(2,1,2))
%             subplot(2,1,2)
%             omg = data{2};
%             for e=1:s.mesh.getNumLocalElements
%                 element = s.mesh.getLocalElement(e);
%                 surf(element.xelem,element.yelem,omg{e});
%                 hold on;
%             end
%             view([30,30])
%             %view([0,90])
            
%             % P-plot
%             delete(subplot(3,2,5))
%             subplot(3,2,5)
%             p = data{3};
%             for e=1:s.mesh.getNumLocalElements
%                 element = s.mesh.getLocalElement(e);
%                 surf(element.xelem,element.yelem,p{e});
%                 hold on;
%             end
%             view([30,30])
%             %view([0,90])
%             
%             % U-velocity at x=0.5
%             delete(subplot(3,2,2))
%             subplot(3,2,2)
%             for e=1:s.mesh.getNumLocalElements
%                 element = s.mesh.getLocalElement(e);
%                 z=interp2(element.xelem,element.yelem,u{e},0.5,element.yelem);
%                 plot(element.yelem,z,'k');
%                 hold on;
%             end
%             xlim([0 1])
%             view([90,-90])
%             
%             % V-velocity at x=0.5
%             delete(subplot(3,2,4))
%             subplot(3,2,4);
%             for e=1:s.mesh.getNumLocalElements
%                 element = s.mesh.getLocalElement(e);
%                 z=interp2(element.xelem,element.yelem,v{e},0.5,element.yelem);
%                 plot(element.yelem,z,'k');
%                 hold on;
%             end
%             ylim([-1e-3 1e-3])
%             view([90,-90]) 

            %% Position subfigures
            d = 0.01;
            
            width  = (1-(cols+1)*2*d)/cols;
            height = 1;
            
            s1 = subplot(rows,cols,1);
            set(s1,'position',[2*d 2*d width height])
            
            s2 = subplot(rows,cols,2);
            set(s2,'position',[4*d+width 0.02 width height])
            
            s3 = subplot(rows,cols,3);
            set(s3,'position',[6*d+2*width 0.02 width height])
        end
        
        % Main function which calls other functions
        function writeValues(s)
            filename = [ System.settings.outp.directory s.name '-' System.settings.outp.valueFilename '.txt' ];
            
            perimeter = s.computePerimeter2D;
            s.computeMass(System.getIntegrationDisc);
            s.computeCenterOfMass(false);
            
            s.computeCenterOfMassVelocity(false);
            
            if (System.rank==0)
                if (System.settings.time.current==0)
                    header = {'iter','time','totalMass','massLossTotal','cmx_total','cmy_total','cmu_total','cmv_total',...
                                            'enclosedMass','massLossEnclosed','cmx_enclosed','cmy_enclosed','cmu_enclosed','cmv_enclosed',...
                                            'perimeter'};
                    System.writeHeader(filename,header)
                end

                values = zeros(15,1);
                values(1)  = System.settings.time.iter; 
                values(2)  = System.settings.time.current;
                values(3)  = s.massTotal(end);
                values(4)  = s.massLossTotal;
                values(5)  = s.cm_total(end,1);
                values(6)  = s.cm_total(end,2);
                values(7)  = s.cmv_total(end,1);
                values(8)  = s.cmv_total(end,2);
                values(9)  = s.massEnclosed(end);
                values(10) = s.massLossEnclosed;
            	values(11) = s.cm_enclosed(end,1);
                values(12) = s.cm_enclosed(end,2);
                values(13) = s.cmv_enclosed(end,1);
                values(14) = s.cmv_enclosed(end,2);
                values(15) = perimeter;

                System.appendToFile(filename,values)
            end
        end
        
        function out = computeOmega(s)
            % Fenergy = (3*Ck.^2 - 3*Ck + 0.5).*H - s.Cn^2 * ( Nabla2 );
            clf;
                        
            layer = 1;
            
            alphaC = s.fields{1}.active.alpha;
            
            out = cell( numel(s.mesh.eRange),1 );
            
            for e=s.mesh.eRange
                element = s.mesh.getElement(e);
                el = element.localID;
                
                %Ck = C.val(FieldType.Current,e);
                Ck = s.fields{1}.value{el}(:);
                Ze = s.mesh.Z{e};
                Nabla2 = element.getDxx + element.getDyy;
                data = (Ck.^3 - 3/2*Ck.^2 + 0.5.*Ck) - s.Cn_L^2 * ( Nabla2 * Ze * alphaC{el}(:) );
                data = s.alpha/s.Cn * data;
                
                out{el} = reshape(data,element.finite.qSize);
                
                %[X,Y] = element.getNodes;
                %if (layer==1)
                %    plotData = data(:,:,end);
                %elseif (layer==0)
                %    plotData = data(:,:,1);
                %end
                %surf(X(:,:,:,end),Y(:,:,:,end),plotData,'FaceColor','interp');
                %hold on;
            end
            
%             x0 = s.mesh.X(1,1);
%             x1 = s.mesh.X(2,1);
%             y0 = s.mesh.X(1,2);
%             y1 = s.mesh.X(2,2);
%             
%             pbx = x1 - x0;
%             pby = y1 - y0;
%             pbaspect([pbx pby 1])
%                
%             colorbar;
%             view([25,25]);
        end
        
        % Function to compute the perimeter of the C=0.5 patch
        function out = computePerimeter2D(s)
            
            length = zeros(2,1);
            
            for e=s.mesh.eRange
                
                % Set the element
                element = s.mesh.getElement(e);
                
                [x,y,z,t] = element.getNodes();
                
                x = squeeze(x);
                y = squeeze(y);
                t = squeeze(t);
                
                c = s.fields{1};
                
                try
                    cValues = reshape( c.val(FieldType.Current,e), size(x) );
                catch
                    disp('')
                end
                
                if ( s.timeMethod == TimeMethod.SpaceTime )
                    lEnd = 2;
                else
                    lEnd = 1;
                end
                
                for l=1:lEnd
                    
                    if l==1
                        pos = 1;
                    else
                        pos = size(x,3);
                    end
                    
                    % 3D
                    %p = patch(isosurface(x(:,:,l),y(:,:,l),t(:,:,l),cValues(:,:,l),0.5));
                    %verts = get(p, 'Vertices');
                    %faces = get(p, 'Faces');
                    
                    % 2D
                    % create a contour object for C = 0.5
                    %p = contour(x(:,:,pos),y(:,:,pos),cValues(:,:,pos),[0.5 0.5]);
                    
                    p = contourc(x(:,1,pos),y(1,:,pos),cValues(:,:,pos)',[0.5 0.5]);
                    
                    % use the contour object to compute the linear line length 
                    % for each segment (p(1,1)==0.5 and p(2,1)=number of segments)
                    if (~isempty(p))
                        diff = zeros(3,p(2,1)-1);
                        for n=1:p(2,1)-1
                            diff(1,n) = p(1,n+2) - p(1,n+1);
                            diff(2,n) = p(2,n+2) - p(2,n+1);
                            diff(3,n) = sqrt(diff(1,n)^2 + diff(2,n)^2);
                        end
                        length(l) = length(l) + sum(diff(3,:));
                    end
                end
            end
            
            
            
            length = NMPI.Allreduce(length,numel(length),'+','computePerimeter2D');
            
            out = length(lEnd);
            
            %fprintf('perimeter start / end : %e / %e\n',length)
                
%             [x,y,z,v] = flow;
%             p = patch(isosurface(x,y,z,v,-3));
%             isonormals(x,y,z,v,p)
%             set(p,'FaceColor','red','EdgeColor','none');
%             daspect([1 1 1])
%             view(3); 
%             camlight 
%             lighting gouraud
%             verts = get(p, 'Vertices');
%             faces = get(p, 'Faces');
%             a = verts(faces(:, 2), :) - verts(faces(:, 1), :);
%             b = verts(faces(:, 3), :) - verts(faces(:, 1), :);
%             c = cross(a, b, 2);
%             area = 1/2 * sum(sqrt(sum(c.^2, 2)));
%             fprintf('\nThe surface area is %f\n\n', area);
        end
        
        % Function to compute the total and enclosed mass.
        % The total mass is a simple summation over all elements, and
        % should remain constant in time. The enclosed mass is the mass
        % where C >= 0.5, and its value can change in time.
        function computeMass(s,finiteElement)
            if (nargin==1)
                finiteElement = FECollection.get(1);
            end
            
            if (finiteElement.spaceTime)
                Qt = finiteElement.Q( end );
            else
                Qt = 1;
            end
            
            % Reset the total/enclosed mass for each time level
            massTotal    = zeros(Qt,1);
            massEnclosed = zeros(Qt,1);
            check        = zeros(Qt,1);
            
            for e=s.mesh.eRange
                
                % Set the element
                element = s.mesh.getElement(e);
                
                %[X,Y,Z,T] = element.getNodes;
                
                % Construct the total and enclosed c arrays
                cTotal    = s.fields{1}.getValueInterpolated(e,finiteElement,s.mesh);
                
                %cEnclosed = cTotal;
                %cEnclosed(cEnclosed< 0.5) = 0;
                %cEnclosed(cEnclosed>=0.5) = 1;
                cEnclosed = Heaviside(cTotal,s.heavisideThickness);
                
                % Set the integration weights and Jacobian for 2D spatial planes
                w = finiteElement.Wspatial;
                J = element.JSpatial;
      
                if (finiteElement.spaceTime)
                    for t=1:Qt
                        sliceValues     = cTotal(:,:,t);
                        massTotal(t)    = massTotal(t)    + sliceValues(:)' * w * J;

                        sliceValues     = cEnclosed(:,:,t);
                        massEnclosed(t) = massEnclosed(t) + sliceValues(:)' * w * J;

                        %sliceValues     = ones( size(cEnclosed(:,:,t)) );
                        %check(t)        = check(t) + sliceValues(:)' * w * J;
                    end
                else
                    %if element.dimension==2
                        sliceValues     = cTotal(:);
                        massTotal       = massTotal + sliceValues(:)' * w * J;

                        sliceValues     = cEnclosed(:);
                        massEnclosed    = massEnclosed + sliceValues(:)' * w * J;
                        
%                     elseif (element.dimension==3)
%                         sliceValues     = cTotal(:);
%                         massTotal       = massTotal + sliceValues(:)' * w * J;
% 
%                         sliceValues     = cEnclosed(:);
%                         massEnclosed    = massEnclosed + sliceValues(:)' * w * J;
%                     end
                end
            end
            
            if (System.settings.anal.debug)
                fprintf('Before NMPI_Allreduce in CahnHilliard.computeMass\n')
            end
            
            s.massTotal     = NMPI.Allreduce(massTotal   ,Qt,'+','massTotal');
            s.massEnclosed  = NMPI.Allreduce(massEnclosed,Qt,'+','massEnclosed');
            checkGlobal     = NMPI.Allreduce(check,Qt,'+','checkGlobal (mass)');
            
            s.massLossTotal     = s.massTotal(1)   -s.massTotal(end);
            s.massLossEnclosed  = s.massEnclosed(1)-s.massEnclosed(end);
            checkLoss           = check(1)-check(end);
            
            %if (NMPI.instance.rank==0)
            %    fprintf('Total mass (begin/end)    : %f / %f (mass loss = %8.2e)\n',s.massTotal(1),s.massTotal(end),s.massLossTotal);
            %    fprintf('Enclosed mass (begin/end) : %f / %f (mass loss = %8.2e)\n',s.massEnclosed(1),s.massEnclosed(end),s.massLossEnclosed);
            %    %fprintf('Enclosed mass (begin/end) : %f / %f (mass loss = %8.2e)\n',check(1),check(end),checkLoss);
            %end
        end
        
        function computeCenterOfMass(s,varargin)
            switch System.settings.mesh.sdim
                case 2
                    s.computeCenterOfMass_2D(varargin{:});
                case 3
                    s.computeCenterOfMass_3D(varargin{:});
            end
        end
        
        function computeCenterOfMassVelocity(s,varargin)
            switch System.settings.mesh.sdim
                case 2
                    s.computeCenterOfMassVelocity_2D(varargin{:});
                case 3
                    s.computeCenterOfMassVelocity_3D(varargin{:});
            end
        end
        
        % The center of mass (CoM) can be determined by:
        %   CoM = sum( C * distance ) / sum( C )
        % for each spatial direction. Here 'distance' is a value wrt the
        % origin of the 
        function computeCenterOfMass_2D(s,computeMass,Q)
            if (nargin==2)
                disc = System.getIntegrationDisc;
            else
                error('unsupported option')
                %disc = LSQdisc_5(System.settings.stde.Pn,Q,System.settings.stde.C,System.settings.stde.sDim,System.settings.time.spaceTime);
            end
            
            Qs = disc.Q(1)*disc.Q(2);       % Spatial Q
            
            if (System.settings.time.method==TimeMethod.SpaceTime)
                Qt = disc.Q(3);                 % Temporal Q
            else
                Qt = 1;
            end
            
            sDim = 2;
            
            % Reset the cm(x,y) for each time level
            cm_total    = zeros(Qt,sDim);
            cm_enclosed = zeros(Qt,sDim);

            for e=s.mesh.eRange
                % Set the element & locations of the quadrature points
                element     = s.mesh.getElement(e);
                locations   = element.getLocations(disc);
                
                if (System.settings.time.method==TimeMethod.SpaceTime)
                    xLoc = superkron( ones(numel(locations{3}),1), ones(numel(locations{2}),1), locations{1} );
                    yLoc = superkron( ones(numel(locations{3}),1), locations{2}, ones(numel(locations{1}),1) );
                else
                    xLoc = superkron( ones(numel(locations{2}),1), locations{1} );
                    yLoc = superkron( locations{2}, ones(numel(locations{1}),1) );
                end
                
                % Get the values for the modified integration order
                cTotal = s.fields{1}.getValueInterpolated( e ,disc, s.mesh);
                
                % Truely sharp 
                %cEnclosed = cTotal; 
                %cEnclosed(cEnclosed<0.5)  = 0;
                %cEnclosed(cEnclosed>=0.5) = 1;
                % Smoothened Heaviside function
                cEnclosed = Heaviside(cTotal,s.heavisideThickness);
                
                % Set the integration weights and Jacobian for 2D spatial planes
                w = disc.Wspatial;
                J = element.J(1)*element.J(2);
      
                % Compute the mass * distance
                cmx_total = cTotal(:) .* xLoc; cmx_total = reshape(cmx_total,[Qs,Qt]);
                cmy_total = cTotal(:) .* yLoc; cmy_total = reshape(cmy_total,[Qs,Qt]);
                
                
                
                % Compute the mass * distance
                cmx_enclosed = cEnclosed(:) .* xLoc; cmx_enclosed = reshape(cmx_enclosed,[Qs,Qt]);
                cmy_enclosed = cEnclosed(:) .* yLoc; cmy_enclosed = reshape(cmy_enclosed,[Qs,Qt]);
                
                % Integrate the values for each element and sum to get the
                % total values on this rank
                for t=1:Qt
                    cm_total(t,1) = cm_total(t,1) + cmx_total(:,t)' * w * J;               % x-direction
                    cm_total(t,2) = cm_total(t,2) + cmy_total(:,t)' * w * J;               % y-direction
                    
                    cm_enclosed(t,1) = cm_enclosed(t,1) + cmx_enclosed(:,t)' * w * J;               % x-direction
                    cm_enclosed(t,2) = cm_enclosed(t,2) + cmy_enclosed(:,t)' * w * J;               % y-direction
                end
            end
            
            if (System.settings.anal.debug)
                fprintf('Before NMPI_Allreduce in CahnHilliard.computeCenterOfMass\n')
            end
            
            cm_total = NMPI.Allreduce(cm_total,numel(cm_total),'+','cm_total');
            cm_total = reshape(cm_total,[Qt,sDim]);
            
            cm_enclosed = NMPI.Allreduce(cm_enclosed,numel(cm_enclosed),'+','cm_enclosed');
            cm_enclosed = reshape(cm_enclosed,[Qt,sDim]);            
            
            % Compute the mass for each time plane
            if (computeMass)
                s.computeMass(disc);
            end
            
            % Finally compute the cm(x,y) for each time plane
            s.cm_total = cm_total ./ s.massTotal;
            
            % Finally compute the cm(x,y) for each time plane
            s.cm_enclosed = cm_enclosed ./ s.massEnclosed;
            
%             if (NMPI.instance.rank==0)
%                 %fprintf('Total mass : %f (effective radius : %f)\n',s.mass,sqrt(s.mass/pi))
%                 fprintf('Center of mass (begin/end) : (%f,%f / %f,%f) -- delta : (%8.2e,%8.2e) \n',s.cm(1,1),s.cm(1,2),s.cm(end,1),s.cm(end,2),s.cm(end,1)-s.cm(1,1),s.cm(end,2)-s.cm(1,2))
%             end
        end
        
        function computeCenterOfMass_3D(s,computeMass,Q)
            if (nargin==2)
                disc = System.getIntegrationDisc;
            else
                error('unsupported option')
            end
            
            sDim = 3;
            
            if (System.settings.mesh.sdim~=sDim)
                error('Please set System.settings.mesh.sdim == %d\n',sDim)
            end
            
            Qs = prod(disc.Q(1:sDim));                                      % Spatial Q
            if (System.settings.time.method==TimeMethod.SpaceTime)
                Qt = disc.Q(sDim+1);                                        % Temporal Q
            else
                Qt = 1;
            end
            
            % Reset the cm(x,y,z) for each time level
            cm_total    = zeros(Qt,sDim);
            cm_enclosed = zeros(Qt,sDim);

            for e=s.mesh.eRange
                % Set the element & locations of the quadrature points
                element     = s.mesh.getElement(e);
                locations   = element.getLocations(disc);
                
                if (System.settings.time.method==TimeMethod.SpaceTime)
                    error('3D space + time is not supported yet')
                else
                    xLoc = superkron( ones(numel(locations{3}),1), ones(numel(locations{2}),1), locations{1} );
                    yLoc = superkron( ones(numel(locations{3}),1), locations{2}, ones(numel(locations{1}),1) );
                    zLoc = superkron( locations{3}, ones(numel(locations{2}),1), ones(numel(locations{1}),1) );
                end
                
                % Get the values for the modified integration order
                cTotal = s.fields{1}.getValueInterpolated( e ,disc, s.mesh);
                
                %cEnclosed = cTotal; 
                %cEnclosed(cEnclosed<0.5)  = 0;
                %cEnclosed(cEnclosed>=0.5) = 1;
                cEnclosed = Heaviside(cTotal,s.heavisideThickness);
                
                % Set the integration weights and Jacobian for 2D spatial planes
                w = disc.Wspatial;
                J = element.J(1) * element.J(2) * element.J(3);
      
                % Compute the mass * distance
                cmx_total = cTotal(:) .* xLoc; cmx_total = reshape(cmx_total,[Qs,Qt]);
                cmy_total = cTotal(:) .* yLoc; cmy_total = reshape(cmy_total,[Qs,Qt]);
                cmz_total = cTotal(:) .* zLoc; cmz_total = reshape(cmz_total,[Qs,Qt]);
                
                % Compute the mass * distance
                cmx_enclosed = cEnclosed(:) .* xLoc; cmx_enclosed = reshape(cmx_enclosed,[Qs,Qt]);
                cmy_enclosed = cEnclosed(:) .* yLoc; cmy_enclosed = reshape(cmy_enclosed,[Qs,Qt]);
                cmz_enclosed = cEnclosed(:) .* zLoc; cmz_enclosed = reshape(cmz_enclosed,[Qs,Qt]);
                
                % Integrate the values for each element and sum to get the
                % total values on this rank
                for t=1:Qt
                    cm_total(t,1) = cm_total(t,1) + cmx_total(:,t)' * w * J;               % x-direction
                    cm_total(t,2) = cm_total(t,2) + cmy_total(:,t)' * w * J;               % y-direction
                    cm_total(t,3) = cm_total(t,3) + cmz_total(:,t)' * w * J;               % z-direction
                    
                    cm_enclosed(t,1) = cm_enclosed(t,1) + cmx_enclosed(:,t)' * w * J;               % x-direction
                    cm_enclosed(t,2) = cm_enclosed(t,2) + cmy_enclosed(:,t)' * w * J;               % y-direction
                    cm_enclosed(t,3) = cm_enclosed(t,3) + cmz_enclosed(:,t)' * w * J;               % z-direction
                end
            end
            
            if (System.settings.anal.debug)
                fprintf('Before NMPI_Allreduce in CahnHilliard.computeCenterOfMass\n')
            end
            
            cm_total = NMPI.Allreduce(cm_total,numel(cm_total),'+','cm_total');
            cm_total = reshape(cm_total,[Qt,sDim]);
            
            cm_enclosed = NMPI.Allreduce(cm_enclosed,numel(cm_enclosed),'+','cm_enclosed');
            cm_enclosed = reshape(cm_enclosed,[Qt,sDim]);            
            
            % Compute the mass for each time plane
            if (computeMass)
                s.computeMass(disc);
            end
            
            % Finally compute the cm(x,y) for each time plane
            s.cm_total = cm_total ./ s.massTotal;
            
            % Finally compute the cm(x,y) for each time plane
            s.cm_enclosed = cm_enclosed ./ s.massEnclosed;
            
%             if (NMPI.instance.rank==0)
%                 %fprintf('Total mass : %f (effective radius : %f)\n',s.mass,sqrt(s.mass/pi))
%                 fprintf('Center of mass (begin/end) : (%f,%f / %f,%f) -- delta : (%8.2e,%8.2e) \n',s.cm(1,1),s.cm(1,2),s.cm(end,1),s.cm(end,2),s.cm(end,1)-s.cm(1,1),s.cm(end,2)-s.cm(1,2))
%             end
        end
        
        % The center of mass velocity (CoM_vel) can be determined by:
        %   CoM_vel = sum( C * U ) / sum( C )
        % for each spatial direction. Here 'U' is the velocity vector
        function computeCenterOfMassVelocity_2D(s,varargin)
            
            [computeMass,overIntegration] = VerifyInput(varargin,{true,true});
            
            U = FieldCollection.get('u');
            V = FieldCollection.get('v');
            %save(['com1.' int2str(System.rank) '.mat']);
            
            if (U==U && V==V)
                remesh = ~isequal(U.mesh.Lv,s.mesh.Lv);

                if (remesh)
                    U.initSubField(FieldType.Interpolated,s.getMesh)
                    V.initSubField(FieldType.Interpolated,s.getMesh)
                end

                if (overIntegration)
                    disc = System.getIntegrationDisc;
                %else
                %    error('unsupported option')
                %    %disc = LSQdisc_5(System.settings.stde.Pn,Q,System.settings.stde.C,System.settings.stde.sDim,System.settings.time.spaceTime);
                end

                sDim = 2;
                count = 0;

                %maxV = 0;

                for e=s.mesh.eRange
                    count = count+1;

                    % Set the element & locations of the quadrature points
                    element     = s.mesh.getElement(e);

                    if (~overIntegration)
                        disc = element.finite;
                    end

                    Qs = disc.Q(1)*disc.Q(2);       % Spatial Q

                    if (System.settings.time.method==TimeMethod.SpaceTime)
                        Qt = disc.Q(3);                 % Temporal Q
                    else
                        Qt = 1;
                    end

                    if count==1
                        % Reset the cm(x,y) for each time level
                        cm_total    = zeros(Qt,sDim);
                        cm_enclosed = zeros(Qt,sDim);
                    end

                    if (remesh)
                        udata = U.getValueInterpolated(e,disc,s.mesh);
                        vdata = V.getValueInterpolated(e,disc,s.mesh);
                    else
                        udata = U.getValueInterpolated(e,disc);
                        vdata = V.getValueInterpolated(e,disc);
                    end

                    % Get the values for the modified integration order
                    cTotal = s.fields{1}.getValueInterpolated(e,disc);

                    %cEnclosed = cTotal;
                    %cEnclosed(cEnclosed<0.5)  = 0;
                    %cEnclosed(cEnclosed>=0.5) = 1;
                    cEnclosed = Heaviside(cTotal,s.heavisideThickness);

                    % Set the integration weights and Jacobian for 2D spatial planes
                    w = disc.Wspatial;
                    J = element.J(1)*element.J(2);

                    % Compute the mass * distance
                    cmx_total = cTotal(:) .* udata(:); cmx_total = reshape(cmx_total,[Qs,Qt]);
                    cmy_total = cTotal(:) .* vdata(:); cmy_total = reshape(cmy_total,[Qs,Qt]);

                    cmx_enclosed = cEnclosed(:) .* udata(:); cmx_enclosed = reshape(cmx_enclosed,[Qs,Qt]);
                    cmy_enclosed = cEnclosed(:) .* vdata(:); cmy_enclosed = reshape(cmy_enclosed,[Qs,Qt]);

                    %maxV = max(maxV,max(vdata(:)));

                    % Integrate the values for each element and sum to get the
                    % total values on this rank
                    for t=1:Qt
                        cm_total(t,1) = cm_total(t,1) + cmx_total(:,t)' * w * J;               % x-direction
                        cm_total(t,2) = cm_total(t,2) + cmy_total(:,t)' * w * J;               % y-direction

                        cm_enclosed(t,1) = cm_enclosed(t,1) + cmx_enclosed(:,t)' * w * J;               % x-direction
                        cm_enclosed(t,2) = cm_enclosed(t,2) + cmy_enclosed(:,t)' * w * J;               % y-direction
                    end
                end

                if (System.settings.anal.debug)
                    fprintf('Before NMPI_Allreduce in CahnHilliard.computeCenterOfMassVelocity\n')
                end

                cm_total = NMPI.Allreduce(cm_total,numel(cm_total),'+','cm_total');
                cm_total = reshape(cm_total,[Qt,sDim]);

                cm_enclosed = NMPI.Allreduce(cm_enclosed,numel(cm_enclosed),'+','cm_enclosed');
                cm_enclosed = reshape(cm_enclosed,[Qt,sDim]);

                % Compute the mass for each time plane
                if (computeMass)
                    s.computeMass(disc);
                end

%                 if (isfield(System.settings.phys,'bubbleVelocity'))
%                     goalVelocity = repmat(System.settings.phys.bubbleVelocity,size(cm_total,1),1);
%                 else
                    goalVelocity = 0;
%                 end

                % Finally compute the cm(x,y) for each time plane
                s.cmv_total     = cm_total    ./ s.massTotal    + goalVelocity;
                s.cmv_enclosed  = cm_enclosed ./ s.massEnclosed + goalVelocity;

            else
                s.cmv_total     = zeros( 1, System.settings.mesh.sdim );
                s.cmv_enclosed  = zeros( 1, System.settings.mesh.sdim );
            end
        end
        
        % The center of mass velocity (CoM_vel) can be determined by:
        %   CoM_vel = sum( C * U ) / sum( C )
        % for each spatial direction. Here 'U' is the velocity vector
        function computeCenterOfMassVelocity_3D(s,varargin)
            
            [computeMass,overIntegration] = VerifyInput(varargin,{true,true});
            
            U = FieldCollection.get('u');
            V = FieldCollection.get('v');
            W = FieldCollection.get('w');
            %save(['com1.' int2str(System.rank) '.mat']);
            
            if (U==U && V==V && W==W)
                remesh = ~isequal(U.mesh.Lv,s.mesh.Lv);

                if (remesh)
                    U.initSubField(FieldType.Interpolated,s.getMesh)
                    V.initSubField(FieldType.Interpolated,s.getMesh)
                    W.initSubField(FieldType.Interpolated,s.getMesh)
                end

                if (overIntegration)
                    disc = System.getIntegrationDisc;
                %else
                %    error('unsupported option')
                %    %disc = LSQdisc_5(System.settings.stde.Pn,Q,System.settings.stde.C,System.settings.stde.sDim,System.settings.time.spaceTime);
                end

                sDim = 3;
                count = 0;

                %maxV = 0;

                for e=s.mesh.eRange
                    count = count+1;

                    % Set the element & locations of the quadrature points
                    element     = s.mesh.getElement(e);

                    if (~overIntegration)
                        disc = element.finite;
                    end

                    Qs = disc.Q(1)*disc.Q(2)*disc.Q(3);       % Spatial Q

                    if (System.settings.time.method==TimeMethod.SpaceTime)
                        Qt = disc.Q(4);                 % Temporal Q
                    else
                        Qt = 1;
                    end

                    if count==1
                        % Reset the cm(x,y) for each time level
                        cm_total    = zeros(Qt,sDim);
                        cm_enclosed = zeros(Qt,sDim);
                    end

                    if (remesh)
                        udata = U.getValueInterpolated(e,disc,s.mesh);
                        vdata = V.getValueInterpolated(e,disc,s.mesh);
                        wdata = W.getValueInterpolated(e,disc,s.mesh);
                    else
                        udata = U.getValueInterpolated(e,disc);
                        vdata = V.getValueInterpolated(e,disc);
                        wdata = W.getValueInterpolated(e,disc);
                    end

                    % Get the values for the modified integration order
                    cTotal = s.fields{1}.getValueInterpolated(e,disc);

                    %cEnclosed = cTotal;
                    %cEnclosed(cEnclosed<0.5)  = 0;
                    %cEnclosed(cEnclosed>=0.5) = 1;
                    cEnclosed = Heaviside(cTotal,s.heavisideThickness);

                    % Set the integration weights and Jacobian for 2D spatial planes
                    w = disc.Wspatial;
                    J = element.J(1)*element.J(2)*element.J(3);

                    % Compute the mass * distance
                    cmx_total = cTotal(:) .* udata(:); cmx_total = reshape(cmx_total,[Qs,Qt]);
                    cmy_total = cTotal(:) .* vdata(:); cmy_total = reshape(cmy_total,[Qs,Qt]);
                    cmz_total = cTotal(:) .* wdata(:); cmz_total = reshape(cmz_total,[Qs,Qt]);

                    cmx_enclosed = cEnclosed(:) .* udata(:); cmx_enclosed = reshape(cmx_enclosed,[Qs,Qt]);
                    cmy_enclosed = cEnclosed(:) .* vdata(:); cmy_enclosed = reshape(cmy_enclosed,[Qs,Qt]);
                    cmz_enclosed = cEnclosed(:) .* wdata(:); cmz_enclosed = reshape(cmz_enclosed,[Qs,Qt]);

                    %maxV = max(maxV,max(vdata(:)));

                    % Integrate the values for each element and sum to get the
                    % total values on this rank
                    for t=1:Qt
                        cm_total(t,1) = cm_total(t,1) + cmx_total(:,t)' * w * J;               % x-direction
                        cm_total(t,2) = cm_total(t,2) + cmy_total(:,t)' * w * J;               % y-direction
                        cm_total(t,3) = cm_total(t,3) + cmy_total(:,t)' * w * J;               % z-direction

                        cm_enclosed(t,1) = cm_enclosed(t,1) + cmx_enclosed(:,t)' * w * J;               % x-direction
                        cm_enclosed(t,2) = cm_enclosed(t,2) + cmy_enclosed(:,t)' * w * J;               % y-direction
                        cm_enclosed(t,3) = cm_enclosed(t,3) + cmz_enclosed(:,t)' * w * J;               % z-direction
                    end
                end

                if (System.settings.anal.debug)
                    fprintf('Before NMPI_Allreduce in CahnHilliard.computeCenterOfMassVelocity\n')
                end

                cm_total = NMPI.Allreduce(cm_total,numel(cm_total),'+','cm_total');
                cm_total = reshape(cm_total,[Qt,sDim]);

                cm_enclosed = NMPI.Allreduce(cm_enclosed,numel(cm_enclosed),'+','cm_enclosed');
                cm_enclosed = reshape(cm_enclosed,[Qt,sDim]);

                % Compute the mass for each time plane
                if (computeMass)
                    s.computeMass(disc);
                end

                if (isfield(System.settings.phys,'bubbleVelocity'))
                    goalVelocity = repmat(System.settings.phys.bubbleVelocity,size(cm_total,1),1);
                else
                    goalVelocity = 0;
                end

                % Finally compute the cm(x,y) for each time plane
                s.cmv_total     = cm_total    ./ s.massTotal    + goalVelocity;
                s.cmv_enclosed  = cm_enclosed ./ s.massEnclosed + goalVelocity;

            else
                s.cmv_total     = zeros( 1, System.settings.mesh.sdim );
                s.cmv_enclosed  = zeros( 1, System.settings.mesh.sdim );
            end
        end
        
        
        function computeMeanOmega(s,disc)
            if (nargin==1)
                disc = s.getMesh.getElement(1).finite;
            end
            
            Qt = disc.Q(3);
            
            % Reset the mass for each time level
            meanValues  = zeros(Qt,1);
            volume      = zeros(Qt,1);
            intGradC    = zeros(Qt,1);
               
            for e=s.mesh.eRange
                
                % Set the element
                element = s.mesh.getElement(e);
                
                % Get the values for the modified integration order
                values = s.omgField.getValueInterpolated(e,disc,s.mesh);
                
                Cx = s.cField.getDxInterpolated(e,disc,s.mesh);
                Cy = s.cField.getDyInterpolated(e,disc,s.mesh);
                
                gradC = sqrt( Cx.^2 + Cy.^2 );
                
%                 values(values<0.5) = 0;
                
                % Set the integration weights and Jacobian for 2D spatial planes
                wxy  = element.disc.Wspatial;
                J    = element.J(1)*element.J(2);
      
                for t=1:Qt
                    xyValues        = values(:,:,t);
                    meanValues(t)	= meanValues(t) + xyValues(:)' * wxy / J;
                    volume(t)       = volume(t) + sum(wxy) / J;
                    
                    gradCValues     = gradC(:,:,t);
                    intGradC(t)     = intGradC(t) + gradCValues(:)' * wxy / J;
                end
                
%                 % Space
% %                 data = C{e};
%                 data = C.getValueInterpolated(e,disc);
%                 element = s.mesh.getLocalElement(e);
%                 wxy = kron(element.disc.wy,element.disc.wx);
%                 if position == 0
%                     % time level 0 of element
%                     mass(1) = mass(1) + data(1:(element.disc.Qx*element.disc.Qy))' * wxy / (element.J(1)*element.J(2)) ;
%                 else
%                     mass(1) = mass(1) + data((element.disc.Qz-1)*(element.disc.Qx*element.disc.Qy)+1:element.disc.Qz*(element.disc.Qx*element.disc.Qy))' * wxy / (element.J(1)*element.J(2)) ;
%                 end
%                 
%                 % SpaceTime
%                 data = C{e};                
%                 element = s.mesh.getLocalElement(e);
%                 J = prod( element.J );
%                 wxyz = superkron(element.disc.wz,element.disc.wy,element.disc.wx);
%                 mass(2) = mass(2) + data' * wxyz / J;
            end
            
            if (System.settings.anal.debug)
                fprintf('Before NMPI_Allreduce in CahnHilliard.computeMass\n')
            end
            
            s.meanOmega     = NMPI.Allreduce(meanValues,Qt,'+','meanOmega');
            s.totalVolume   = NMPI.Allreduce(volume,Qt,'+','totalVolume');
            s.intGradC      = NMPI.Allreduce(intGradC,Qt,'+','intGradC');
            
%             s.massLoss = s.mass(1)-s.mass(end);
            
%             if (NMPI.instance.rank==0)
%                 fprintf('Mean omega (begin/end)             : %f / %f\n',s.meanOmega(1)/s.totalVolume(1),s.meanOmega(end)/s.totalVolume(end));
%                 fprintf('Mean curvature (begin/end)         : %f / %f\n',s.meanOmega(1)/s.intGradC(1),s.meanOmega(end)/s.intGradC(end));
%                 fprintf('Mean curvature - 2*pi (begin/end)  : %f / %f\n',2*pi/s.intGradC(1),2*pi/s.intGradC(end));
%             end
            
            s.kappa = (2*pi) / mean(s.intGradC);
        end
        
        function computeLS(s)
            
            % check if init is required
            if (isempty(s.LS))
                s.LS = cell( s.mesh.numElements, 1);
                init = true;
            else
                init = false;
            end
            
            % compute LS
            for e=1:s.mesh.numElements
                element = s.mesh.getElement(e);
                el = element.localID;
                
                %Ck = C.val(FieldType.Current,e);
                if (init)
                    Ck = s.fields{1}.value{el}(:);
                else
                    Ck = s.fields{1}.value{el}(:) + (0.5 + 0.5*tanh( s.LS{e} / (2*sqrt(2)*s.Cn_L) ));
                end
                
                s.LS{e} = sqrt(2) * s.Cn_L * log( abs( Ck ./ (1-Ck) ));
                
                delta = 0.00001;
                s.LS{e}( abs(Ck)>1-delta ) = sqrt(2) * s.Cn_L * log( abs( (1-delta) ./ (  delta) ) );
                s.LS{e}( abs(Ck)<  delta ) = sqrt(2) * s.Cn_L * log( abs(    delta  ./ (1-delta) ) );
            end
            
            % update C_SEM
            for e=1:s.mesh.numElements
                element = s.mesh.getElement(e);
                el = element.localID;
                
                [X,Y]    = element.getNodes;
                     
                X = squeeze(X); Y = squeeze(Y);
                
                Ck = s.fields{1}.value{el}(:);
                
                %data = reshape( full(s.LS{e}), element.finite.qSize );
                Cext = Ck(:) - (0.5 + 0.5*tanh( s.LS{e} / (2*sqrt(2)*s.Cn_L) ));
                data = reshape( full(Cext), element.finite.qSize );
                
                %potential = s.alpha/s.Cn * s.Cn/s.Cn_L * ( 1*(c.^3-3/2*c.^2+1/2*c) - 1*s.Cn_L^2*(cxx+cyy) );
                %potential = (s.alpha/s.Cn_L) * ( 1*(c.^3-3/2*c.^2+1/2*c) - 1*s.Cn_L^2*(cxx+cyy) );
                
                surf(X(:,:,end),Y(:,:,end), data(:,:,end));
                
                hold on;
            end
            
            clf;
            for e=1:s.mesh.numElements
                element = s.mesh.getElement(e);
                el = element.localID;
                
                [X,Y]    = element.getNodes;
                     
                X = squeeze(X); Y = squeeze(Y);
                
                Ck = s.fields{1}.value{el}(:);
                
                data = reshape( full(s.LS{e}), element.finite.qSize );
                [Fx, Fy] = gradient(data,X(:,1,1),X(1,:,1),X(1,1,:));
                
                %Cext = Ck(:) - (0.5 + 0.5*tanh( s.LS{e} / (2*sqrt(2)*s.Cn_L) ));
                %data = reshape( full(Cext), element.finite.qSize );
                
                data = Fy(:); % sqrt(2) / (8*s.Cn_L) * Fy(:) .* (1 - (tanh( s.LS{e} / (2*sqrt(2)*s.Cn_L) ) ).^2 );
                data = reshape( full(data), element.finite.qSize );
                
                %potential = s.alpha/s.Cn * s.Cn/s.Cn_L * ( 1*(c.^3-3/2*c.^2+1/2*c) - 1*s.Cn_L^2*(cxx+cyy) );
                %potential = (s.alpha/s.Cn_L) * ( 1*(c.^3-3/2*c.^2+1/2*c) - 1*s.Cn_L^2*(cxx+cyy) );
                
                surf(X(:,:,end),Y(:,:,end), data(:,:,end));
                
                hold on;
            end
            
            disp('')
            
        end
        
        % Only useful for cField
        function plotPotential(s)
            C = s.fields{1};
            
            P = FieldCollection.get('p');
            
            for e=1:s.mesh.numElements
                element = s.mesh.getElement(e);
                c      = reshape(C.getValue(e),element.finite.qSize);
                cx = reshape(C.x(FieldType.Current,e),element.finite.qSize);
                cy = reshape(C.y(FieldType.Current,e),element.finite.qSize);
                cxx    = reshape(C.xx(FieldType.Current,e),element.finite.qSize);
                cyy    = reshape(C.yy(FieldType.Current,e),element.finite.qSize);
                
                px = reshape(P.x(FieldType.Current,e),element.finite.qSize);
                py = reshape(P.y(FieldType.Current,e),element.finite.qSize);
                
                [X,Y]    = element.getNodes;
                                
                c = c(:,:,end);
                
                %potential = s.alpha/s.Cn * s.Cn/s.Cn_L * ( 1*(c.^3-3/2*c.^2+1/2*c) - 1*s.Cn_L^2*(cxx+cyy) );
%                 potential = (s.alpha) * ( 1/s.Cn_L*(c.^3-3/2*c.^2+1/2*c) - s.Cn_L*(cxx+cyy) );
                potential = (1) * ( 0/s.Cn_L*(c.^3-3/2*c.^2+1/2*c) - 1*s.Cn_L*(cxx+cyy) ) - 0*s.Cn_L * sqrt(px.^2+py.^2);
                
                surf(X(:,:,end),Y(:,:,end), potential(:,:,end));
                
                hold on;
            end
                        
            colorbar;
            view([25,25]);
%             title(titleString);
        end
        
        % Only useful for cField
        function plotKappaCorrection(s)
            C = s.fields{1};
            kappa = s.getKappa;
            
            for e=1:s.mesh.numElements
                element = s.mesh.getElement(e);
                
                id = element.id;
                
                %c      = reshape(C.getValue(e),element.finite.qSize);
                cx = reshape(C.x(FieldType.Current,e),element.finite.qSize);
                cy = reshape(C.y(FieldType.Current,e),element.finite.qSize);
                cxy = C.xy2(id);
                
                gradc = sqrt(cx.^2+cy.^2);
                
                %cxx    = reshape(C.xx(FieldType.Current,e),element.finite.qSize);
                %cyy    = reshape(C.yy(FieldType.Current,e),element.finite.qSize);
                
                kappax = kappa.x2(id);
                kappay = kappa.y2(id);
                
                %px = reshape(P.x(FieldType.Current,e),element.finite.qSize);
                %py = reshape(P.y(FieldType.Current,e),element.finite.qSize);
                
                [X,Y]    = element.getNodes;
                                
                %c = c(:,:,end);
                
                %potential = s.alpha/s.Cn * s.Cn/s.Cn_L * ( 1*(c.^3-3/2*c.^2+1/2*c) - 1*s.Cn_L^2*(cxx+cyy) );
%                 potential = (s.alpha) * ( 1/s.Cn_L*(c.^3-3/2*c.^2+1/2*c) - s.Cn_L*(cxx+cyy) );
%                 potential = ( -sign(cx(:)) .* kappax .* cx(:) + 0*sign(cy(:)) .* kappay .* cy(:) ) ; %.* cxy ; %.* ( 0*cy(:) - cx(:) ); % ./ gradc(:);
                potential = ( kappax .* cy(:) - kappay .* cx(:) ) .* (cx(:)-cy(:)) ; %.* cxy ; %.* ( 0*cy(:) - cx(:) ); % ./ gradc(:);
                
                surf(X,Y,reshape(potential,size(X)));
                
                hold on;
            end
                        
            colorbar;
            view([25,25]);
%             title(titleString);
        end
        
       function plotKappa(s)
            kappa = s.getKappa;
            
            for e=1:s.mesh.numElements
                element = s.mesh.getElement(e);
                id      = element.id;
                [X,Y]    = element.getNodes;
                
                surf(X,Y,reshape(kappa{id},size(X)));
                
                hold on;
            end
                        
            colorbar;
            view([25,25]);
%             title(titleString);

            zlim([-5 5])
        end
        
       function plotKappaGradient(s)
            C = s.fields{1};
            kappa = s.getKappa;
            
            for e=1:s.mesh.numElements
                element = s.mesh.getElement(e);
                
                id = element.id;
                
                %c      = reshape(C.getValue(e),element.finite.qSize);
                cx = reshape(C.x(FieldType.Current,e),element.finite.qSize);
                cy = reshape(C.y(FieldType.Current,e),element.finite.qSize);
                cxy = C.xy2(id);
                
                gradc = sqrt(cx.^2+cy.^2);
                
                %cxx    = reshape(C.xx(FieldType.Current,e),element.finite.qSize);
                %cyy    = reshape(C.yy(FieldType.Current,e),element.finite.qSize);
                
                kappaVal  = kappa.val2(id);
%                 kappax = kappa.x2(id);
%                 kappay = kappa.y2(id);
                
                %px = reshape(P.x(FieldType.Current,e),element.finite.qSize);
                %py = reshape(P.y(FieldType.Current,e),element.finite.qSize);
                
                [X,Y]    = element.getNodes;
                                
                %c = c(:,:,end);
                
                %potential = s.alpha/s.Cn * s.Cn/s.Cn_L * ( 1*(c.^3-3/2*c.^2+1/2*c) - 1*s.Cn_L^2*(cxx+cyy) );
%                 potential = (s.alpha) * ( 1/s.Cn_L*(c.^3-3/2*c.^2+1/2*c) - s.Cn_L*(cxx+cyy) );
                potential = (kappaVal -2 * gradc(:)) .* cxy ; %.* (cy(:)-cx(:));
                
                surf(X,Y,reshape(potential,size(X)));
                
                hold on;
            end
                        
            colorbar;
            view([25,25]);
%             title(titleString);
        end
        
        % Only useful for cField
        function plotMeanOmega(s)
            C = s.fields{1};
            
            for e=1:s.mesh.numElements
                element = s.mesh.getElement(e);
                c      = reshape(C.getValue(e),element.finite.qSize);
                cx     = reshape(C.x(FieldType.Current,e),element.finite.qSize);
                cy     = reshape(C.y(FieldType.Current,e),element.finite.qSize);
                cxx    = reshape(C.xx(FieldType.Current,e),element.finite.qSize);
                cyy    = reshape(C.yy(FieldType.Current,e),element.finite.qSize);
                cxy    = reshape(C.xy(FieldType.Current,e),element.finite.qSize);
                
                gradC2 = cx.^2+cy.^2;
                
                [X,Y]    = element.getNodes;
                                
                %c = c(:,:,end);
                
                %potential = s.alpha/s.Cn * s.Cn/s.Cn_L * ( 1*(c.^3-3/2*c.^2+1/2*c) - 1*s.Cn_L^2*(cxx+cyy) );
                
                %meanOmega = (s.alpha/s.Cn) * ( (c.^3-3/2*c.^2+1/2*c).*sqrt(gradC2) - 1*s.Cn_L^2.*(cx.^2.*cxx + 2*cx.*cy.*cxy + cy.^2.*cyy)./sqrt(gradC2) );
                %meanOmega = (s.alpha/s.Cn_L) * ( 1*(c.^3-3/2*c.^2+1/2*c) - 1*s.Cn_L^2.*(cx.^2.*cxx + 2*cx.*cy.*cxy + cy.^2.*cyy)./(gradC2) );
                meanOmega = (1/s.Cn_L) * ( 1*(c.^3-3/2*c.^2+1/2*c) - 1*s.Cn_L^2.*(cx.^2.*cxx + 2*cx.*cy.*cxy + cy.^2.*cyy)./(gradC2) );
                
                surf(X(:,:,end),Y(:,:,end), meanOmega(:,:,end));
                
                hold on;
            end
                        
            colorbar;
            view([25,25]);
%             title(titleString);
        end
        
        function plotLS(s)
            C = FieldCollection.get('c');
            
            for e=s.mesh.eRange
                element = s.mesh.getElement(e);

                Ck      = C.val(FieldType.Current,e);
                
                [x,y]   = element.getNodes;
                
                x = squeeze(x);
                y = squeeze(y);
                 
                LS = 2*s.alpha/3 * s.Cn_L * log( abs( Ck./(1-Ck) ) );
                
                LS = reshape( LS, size(x) );
                
                LS_exact = 0 * (0.5 - sqrt( (x-1).^2 + (y-1).^2 ) );
                
                hold on;
                surface(x(:,:,end),y(:,:,end),LS(:,:,end)-LS_exact(:,:,end))
            end
        end
        

        
        function plotCurvature(s)
            
%             if nargin<3
%                 s.createInterface;
%                 interface = s.interface;
%             end

            curvature = cell(s.mesh.numLocalElements,1);
            
            C   = s.getField(1); %.current;
            Omg = s.getField(2);  %.current;
            
            mesh = C.mesh;
            
            [kx,ky] = s.getCurvature(s.mesh);
            
            for el=1:mesh.numLocalElements
                
                e = mesh.getLocalElement(el).globalID;
                %Ck      = C.val(FieldType.Current,e);
                dCkdx   = C.x(FieldType.Current,e);
                dCkdy   = C.y(FieldType.Current,e);
                %Omgk    = Omg.val(FieldType.Current,e);
                %curvature{el} = Omgk./ (s.Cn * s.alpha * sqrt(dCkdx.^2+dCkdy.^2));
                %curvature{el}( (abs(curvature{el})>100) ) = 0;
                %Cxx      = C.xx2(el);
                %Cxy      = C.xy2(el);
                %Cyy      = C.yy2(el);
                
                %curvature{el} = (Cxx+Cyy) - 0*sqrt( kx{el}.^2 + ky{el}.^2 );
                curvature{el} = sqrt( kx{el}.^2 + ky{el}.^2 ); % ./ sqrt( dCkdx.^2 + dCkdy.^2 );
                
%                 curvature{el} = kx{el};
            end
                        
            x0 = s.mesh.X(1,1);
            x1 = s.mesh.X(2,1);
            y0 = s.mesh.X(1,2);
            y1 = s.mesh.X(2,2);
            
            pbx = x1 - x0;
            pby = y1 - y0;

            %xlim([mesh.x0 mesh.x1])
            %ylim([mesh.y0 mesh.y1])
            
            zlim([0 6])
            
            grid off;
            pbaspect([pbx pby 1])

            shading interp
            view([0,90])
            
            hold on;
            
%             X = cell(mesh.numLocalElements,1);
%             Y = cell(mesh.numLocalElements,1);
            curv = cell(mesh.numLocalElements,1);

            %% Create a meshgrid
            %for e=1:mesh.numLocalElements
            %    [X{e},Y{e}] = meshgrid( mesh.getLocalElement(e).xelem,mesh.getLocalElement(e).yelem );
            %end
            
            %% Compute the surface tension data
            for e=1:mesh.numLocalElements
                temp = curvature{e};
                temp = reshape(temp,mesh.getLocalElement(e).finite.qSize);
                curv{e} = temp; %(:,:,end);
            end
            
            maxST = max( cellfun(@(x) max(x(:)),curv) );
            
            % Integration of the surface tension
            for e=1:mesh.numLocalElements
                element = mesh.getElement(e);
                %wxy = kron(element.disc.wy,element.disc.wx);
                %intST = intST + data((element.disc.Qz-1)*(element.disc.Qx*element.disc.Qy)+1:element.disc.Qz*(element.disc.Qx*element.disc.Qy))' * wxy / (element.J(1)*element.J(2)) ;
                %intST = intST + surfaceTension{e}(:) .* wxy/(element.J(1)*element.J(2));

                [X,Y] = element.getNodes;
                X = squeeze(X);
                Y = squeeze(Y);
                
                surface(X(:,:,end),Y(:,:,end),curv{e}(:,:,end));
                
                %if interface.active && ~isempty(interface.x{e})
                %    line(interface.x{e}, interface.y{e}, repmat(maxST,size(interface.x{e})), interface.props{:});
                %end
            end
        end
        
        function plotDivCurvature(s)
            
%             if nargin<3
%                 s.createInterface;
%                 interface = s.interface;
%             end

            curvature = cell(s.mesh.numLocalElements,1);
            
            C   = s.getField(1); %.current;
            Omg = s.getField(2);  %.current;
            
            mesh = C.mesh;
            
            %H = s.getH2(s.mesh);
            
            for el=1:mesh.numLocalElements
                
                e = mesh.getLocalElement(el).globalID;
                Ck      = C.val(FieldType.Current,e);
                %cx   = C.x(FieldType.Current,e);
                %cy   = C.y(FieldType.Current,e);
                %Omgk    = Omg.val(FieldType.Current,e);
                %curvature{el} = Omgk./ (s.Cn * s.alpha * sqrt(dCkdx.^2+dCkdy.^2));
                %curvature{el}( (abs(curvature{el})>100) ) = 0;
                
                %curvature{el} = 2 * s.H{el}(:) kx{el} .* cy - ky{el} .* cx; %sqrt( kx{el}.^2 + ky{el}.^2 );
                
                advTerm = sign(Ck-0.5) .* (Ck.*(1-Ck)-0.25);            
                curvature{el} = advTerm .* 2 .* s.H{el}(:);
            end
                        
            x0 = s.mesh.X(1,1);
            x1 = s.mesh.X(2,1);
            y0 = s.mesh.X(1,2);
            y1 = s.mesh.X(2,2);
            
            pbx = x1 - x0;
            pby = y1 - y0;

            %xlim([mesh.x0 mesh.x1])
            %ylim([mesh.y0 mesh.y1])
            
            grid off;
            pbaspect([pbx pby 1])

            shading interp
            view([0,90])
            
            hold on;
            
%             X = cell(mesh.numLocalElements,1);
%             Y = cell(mesh.numLocalElements,1);
            curv = cell(mesh.numLocalElements,1);

            %% Create a meshgrid
            %for e=1:mesh.numLocalElements
            %    [X{e},Y{e}] = meshgrid( mesh.getLocalElement(e).xelem,mesh.getLocalElement(e).yelem );
            %end
            
            %% Compute the surface tension data
            for e=1:mesh.numLocalElements
                temp = curvature{e};
                temp = reshape(temp,mesh.getLocalElement(e).finite.qSize);
                curv{e} = temp; %(:,:,end);
            end
            
            maxST = max( cellfun(@(x) max(x(:)),curv) );
            
            % Integration of the surface tension
            for e=1:mesh.numLocalElements
                element = mesh.getElement(e);
                %wxy = kron(element.disc.wy,element.disc.wx);
                %intST = intST + data((element.disc.Qz-1)*(element.disc.Qx*element.disc.Qy)+1:element.disc.Qz*(element.disc.Qx*element.disc.Qy))' * wxy / (element.J(1)*element.J(2)) ;
                %intST = intST + surfaceTension{e}(:) .* wxy/(element.J(1)*element.J(2));

                [X,Y] = element.getNodes;
                X = squeeze(X);
                Y = squeeze(Y);
                
                surface(X(:,:,end),Y(:,:,end),curv{e}(:,:,end));
                
                %if interface.active && ~isempty(interface.x{e})
                %    line(interface.x{e}, interface.y{e}, repmat(maxST,size(interface.x{e})), interface.props{:});
                %end
            end
        end
        
        
        % The surface energy can be determined from the Helmholtz free 
        % energy density model, which in the Cahn-Hilliard model is the 
        % sum of the local Helmholtz energy and surface tension effects.
        function out = computeSurfaceEnergy(s)

            
            
%             if (nargin==1)
%                 activeField = FieldType.Current;
%             end

            % Set the Fields
            C   = s.getField(1,s);
            
            % Get the mesh (should be same for both fields!)
            mesh = C.mesh;
            
            numElements = mesh.numLocalElements;
           
            % Initialize psi and gradC for all elements
            psi   = cell(numElements,1);
            gradC = cell(numElements,1);
           
            for el=1:numElements
                e = mesh.getElement(el).globalID;
                
                Ck          = C.val(e);
                dCkdx       = C.x(e);
                dCkdy       = C.y(e);
                
                psi{e}      = 0.25 * Ck.^2 .* (1-Ck).^2;
                gradC{e}    = dCkdx.^2+dCkdy.^2;
            end

            surfaceEnergy = 0;
            
            for e=1:numElements
                element = mesh.getLocalElement(e);
                
                data = 0.5*s.Cn .* gradC{e} + psi{e} ./ s.Cn;
                
                % Set the integration weights and Jacobian for 2D spatial planes
                w = finiteElement.Wspatial;
                J = element.JSpatial;
                
                surfaceEnergy = surfaceEnergy + data((element.disc.Qz-1)*(element.disc.Qx*element.disc.Qy)+1:element.disc.Qz*(element.disc.Qx*element.disc.Qy))' * wxy / (element.J(1)*element.J(2)) ;
            end
            
            surfaceEnergy = surfaceEnergy * s.alpha;

            if (System.settings.anal.debug)
                fprintf('Before NMPI_Allreduce in CahnHilliard.computeSurfaceEnergy\n')
            end
            
%             fprintf('Max of surface tension : %f\n',stMax)
            surfaceEnergy = NMPI.Allreduce(surfaceEnergy,1,'+','surfaceEnergy');
%             if (NMPI.instance.rank==0)
%                 fprintf('Total surface energy  : %f\n',surfaceEnergy)
%             end
            
            out = surfaceEnergy;
        end
        
        function computeSurfaceTension(s,activeField)

            %if (nargin==1)
                finiteElement = FECollection.get(1);
            %end
            
            if (nargin==1)
                activeField = FieldType.Current;
            end

            % Set the Fields
            C   = s.getField(1,s);
            Omg = s.getField(2,s);
            
            % Get the mesh (should be same for both fields!)
            mesh = C.mesh;
            
            numElements = mesh.numLocalElements;
            
            fx = cell(numElements,1);
            fy = cell(numElements,1);
            
            if (finiteElement.spaceTime)
                Qt = finiteElement.Q( end );
            else
                Qt = 1;
            end
            
            % Compute the surface tension per element
%             for el=1:numElements
%                 e = mesh.getElement(el).globalID;
%                 
%                 Ck      = C.val(FieldType.Current,e);
%                 dCkdx   = C.x(FieldType.Current,e);
%                 dCkdy   = C.y(FieldType.Current,e);
%                 Omgk    = Omg.val(FieldType.Current,e);
% 
%                 fx{e} = (Omgk).*dCkdx;
%                 fy{e} = (Omgk).*dCkdy;
%             end
            
            [fx,fy] = s.getCurvature;

            % Reset the total/enclosed mass for each time level
            surfaceTension = zeros(Qt,1);
            
            for e=1:numElements
                element = mesh.getLocalElement(e);
                
                data = sqrt(fx{e}.^2+fy{e}.^2);
                data = reshape( data, element.finite.qSize );
                
                % Set the integration weights and Jacobian for 2D spatial planes
                w = finiteElement.Wspatial;
                J = element.JSpatial;

                for t=1:Qt
                    sliceValues = data(:,:,t);
                    surfaceTension(t) = surfaceTension(t) + dot( sliceValues(:),w) * J;
                end
            end
            
            surfaceTension = surfaceTension / s.We;

            if (System.settings.anal.debug)
                fprintf('Before NMPI_Allreduce in CahnHilliard.computeSurfaceTension\n')
            end
            
%             fprintf('Max of surface tension : %f\n',stMax)
            surfaceTension = NMPI.Allreduce(surfaceTension,numel(surfaceTension),'+','surfaceTension');
            if (NMPI.instance.rank==0)
                for i=1:Qt
                    fprintf('Total surface tension (t=%d) : %f (%f pi)\n',i,surfaceTension(i),surfaceTension(i)/pi)
                end
            end
        end
        
        function plotSurfaceTension(s,fig,interface)
            
%             if nargin<3
%                 s.createInterface;
%                 interface = s.interface;
%             end
            
%             fx = cell(s.mesh.numLocalElements,1);
%             fy = cell(s.mesh.numLocalElements,1);
            
            %Omega = FieldCollection.get('omg');
%             C     = FieldCollection.get('c');
%             Omg   = FieldCollection.get('omg');
            
            cField   = s.fields{1};
            
            %Omg = s.getField(2,s);  %.current;
            
            %activeField = FieldType.Current;
            % 
            %C.setActive(activeField);
            %Omg.setActive(activeField);
            
            mesh = s.mesh;
            
%             Cn      = System.settings.phys.Cn;
%             alpha   = System.settings.phys.alpha;
            
%             meanOmg = 0;
            
%             for el=1:mesh.numLocalElements
% %                 Ck      = s.getC(e,SolutionMode.Normal);
% %                 dCkdx   = s.getCx(e,SolutionMode.Normal);
% %                 dCkdy   = s.getCy(e,SolutionMode.Normal);
% %                 Omgk    = s.getOmg(e,SolutionMode.Normal);
%                 
%                 element = s.mesh.getElement(el);
% 
%                 e = mesh.getLocalElement(el).globalID;
% 
%                 Ck      = C.val(FieldType.Current,e);
%                 dCkdx   = C.x(FieldType.Current,e);
%                 dCkdy   = C.y(FieldType.Current,e);
%                 Omgk    = Omg.val(FieldType.Current,e); % - s.meanOmega(1);
% 
%                 %Ze = s.mesh.Z{e};
%                 %Nabla2 = element.getDxx + element.getDyy;
%                 %data = (Ck.^3 - 3/2*Ck.^2 + 0.5.*Ck) - Cn^2 * ( Nabla2 * Ze * C.alpha{e}(:) );
%                 %Omgk = alpha/Cn * data;
%                 
%                 
%                 fx{el} = Omgk.*dCkdx;
%                 fy{el} = Omgk.*dCkdy;
%                 
% %                 meanOmg = meanOmg+mean(Omgk);
%                 
% %                 fx{e} = Omgk;
% %                 fy{e} = Omgk;
%             end
            
            [kx,ky] = s.getCurvature;
            
%             meanOmg = meanOmg / mesh.numLocalElements*4
%             
%             meanOmg = 4;
% 
%             for el=1:mesh.numLocalElements
%                 e = mesh.getLocalElement(el).globalID;
%                 dCkdx   = C.x(e);
%                 dCkdy   = C.y(e);
%                 Omgk    = Omg.val(e);
%                 fx{el} = (Omgk-meanOmg).*dCkdx;
%                 fy{el} = (Omgk-meanOmg).*dCkdy;
%             end
            
%             subplot(3,1,1)
%             data = fx;
%             for e=1:s.mesh.getNumElements
%                 if (size(data{e},2)==1)
%                     data{e} = reshape(data{e},s.mesh.getElement(e).disc.Q);
%                     [X,Y] = meshgrid( s.mesh.getElement(e).xelem,s.mesh.getElement(e).yelem );
%                     surf(X,Y,data{e}(:,:,end)');
%                 else
%                     [X,Y] = meshgrid( s.mesh.getElement(e).xelem,s.mesh.getElement(e).yelem );
%                     surf(X,Y,data{e}(:,:,end)');
%                 end
%                 hold on;
%             end
%             
%             subplot(3,1,2)
%             data = fy;
%             for e=1:s.mesh.getNumElements
%                 if (size(data{e},2)==1)
%                     data{e} = reshape(data{e},s.mesh.getElement(e).disc.Q);
%                     [X,Y] = meshgrid( s.mesh.getElement(e).xelem,s.mesh.getElement(e).yelem );
%                     surf(X,Y,data{e}(:,:,end)');
%                 else
%                     [X,Y] = meshgrid( s.mesh.getElement(e).xelem,s.mesh.getElement(e).yelem );
%                     surf(X,Y,data{e}(:,:,end)');
%                 end
%                 hold on;
%             end
%             
%             subplot(3,1,3)

            intST = 0;
            
            pbx = mesh.X(2,1) - mesh.X(1,1);
            pby = mesh.X(2,2) - mesh.X(1,2);

            %xlim( mesh.X(1,:) );
            %ylim( mesh.X(2,:) )
            
            grid off;
            pbaspect([pbx pby 1])

            shading interp
            %view([0,90])
            colorbar;
            view([25,25]); 
            
            hold on;
            
            X = cell(mesh.numLocalElements,1);
            Y = cell(mesh.numLocalElements,1);
            surfaceTension = cell(mesh.numLocalElements,1);

            %% Create a meshgrid
            for e=1:mesh.numLocalElements
                [X1,Y1] = mesh.getLocalElement(e).getNodes;
                X{e} = X1(:,:,end);
                Y{e} = Y1(:,:,end);
            end
            
            %% Compute the surface tension data
            for e=1:mesh.numLocalElements
                
                c   = cField.val2(e);
                cx  = cField.x2(e);
                cy  = cField.y2(e);
                cxx = cField.xx2(e);
                cyy = cField.yy2(e);
                
%                 temp = - s.alpha * s.Cn_L * (cxx+cyy) .* sqrt(cx.^2+cy.^2);
                %temp = ((c.^3-3/2*c.^2+c/2)/s.Cn_L - s.Cn_L * (cxx+cyy));
                
                temp = sqrt(kx{e}.^2+ky{e}.^2);
                temp = reshape(temp,mesh.getLocalElement(e).finite.qSize);
                surfaceTension{e} = s.WEI * temp(:,:,1);
            end
            
            maxST = max( cellfun(@(x) max(x(:)),surfaceTension) );
            
            % Integration of the surface tension
            for e=1:mesh.numLocalElements
                element = mesh.getLocalElement(e);
%                 wxy = kron(element.disc.wy,element.disc.wx);
                
                wxy = element.getW_spatial;
                
                %intST = intST + data((element.disc.Qz-1)*(element.disc.Qx*element.disc.Qy)+1:element.disc.Qz*(element.disc.Qx*element.disc.Qy))' * wxy / (element.J(1)*element.J(2)) ;
                intST = intST + surfaceTension{e}(:) .* wxy(1:element.finite.numSpatialPoints); %*(element.J(1)*element.J(2));

                surface(X{e},Y{e},surfaceTension{e});
                
%                 if interface.active && ~isempty(interface.x{e})
%                     line(interface.x{e}, interface.y{e}, repmat(maxST,size(interface.x{e})), interface.props{:});
%                 end
            end
            
            if (System.settings.anal.debug)
                fprintf('Before NMPI_Allreduce in CahnHilliard.plotSurfaceTension\n')
            end
            
%             fprintf('Max of surface tension : %f\n',stMax)

            intST = NMPI.Allreduce(intST,1,'+','intST');
            
%             if (NMPI.instance.rank==0)
%                 fprintf('Total surface tension  : %f (%f pi)\n',surfaceTension,surfaceTension/pi)
%             end



        end
        
        function plotSurfaceTensionHalf(s,fig,interface)
            
            if nargin<3
                s.createInterface;
                interface = s.interface;
            end
            
            fx = cell(s.mesh.numLocalElements,1);
            fy = cell(s.mesh.numLocalElements,1);
            
            C   = s.getField(1,s); %.current;
            Omg = s.getField(2,s);  %.current;
            
            %activeField = FieldType.Current;
            % 
            %C.setActive(activeField);
            %Omg.setActive(activeField);
            
            mesh = C.mesh;
            
%             meanOmg = 0;
            
            for el=1:mesh.numLocalElements
%                 Ck      = s.getC(e,SolutionMode.Normal);
%                 dCkdx   = s.getCx(e,SolutionMode.Normal);
%                 dCkdy   = s.getCy(e,SolutionMode.Normal);
%                 Omgk    = s.getOmg(e,SolutionMode.Normal);
                
                e = mesh.getLocalElement(el).globalID;

                Ck      = C.val(e);
                dCkdx   = C.x(e);
                dCkdy   = C.y(e);
                Omgk    = Omg.val(e); % - s.meanOmega(1);

                fx{el} = Omgk.*dCkdx;
                fy{el} = Omgk.*dCkdy;
                
%                 meanOmg = meanOmg+mean(Omgk);
                
%                 fx{e} = Omgk;
%                 fy{e} = Omgk;
            end
            
%             meanOmg = meanOmg / mesh.numLocalElements*4
%             
%             for el=1:mesh.numLocalElements
%                 e = mesh.getLocalElement(el).globalID;
%                 dCkdx   = C.x(e);
%                 dCkdy   = C.y(e);
%                 Omgk    = Omg.val(e);
%                 fx{el} = (Omgk-meanOmg).*dCkdx;
%                 fy{el} = (Omgk-meanOmg).*dCkdy;
%             end
            
%             subplot(3,1,1)
%             data = fx;
%             for e=1:s.mesh.getNumElements
%                 if (size(data{e},2)==1)
%                     data{e} = reshape(data{e},s.mesh.getElement(e).disc.Q);
%                     [X,Y] = meshgrid( s.mesh.getElement(e).xelem,s.mesh.getElement(e).yelem );
%                     surf(X,Y,data{e}(:,:,end)');
%                 else
%                     [X,Y] = meshgrid( s.mesh.getElement(e).xelem,s.mesh.getElement(e).yelem );
%                     surf(X,Y,data{e}(:,:,end)');
%                 end
%                 hold on;
%             end
%             
%             subplot(3,1,2)
%             data = fy;
%             for e=1:s.mesh.getNumElements
%                 if (size(data{e},2)==1)
%                     data{e} = reshape(data{e},s.mesh.getElement(e).disc.Q);
%                     [X,Y] = meshgrid( s.mesh.getElement(e).xelem,s.mesh.getElement(e).yelem );
%                     surf(X,Y,data{e}(:,:,end)');
%                 else
%                     [X,Y] = meshgrid( s.mesh.getElement(e).xelem,s.mesh.getElement(e).yelem );
%                     surf(X,Y,data{e}(:,:,end)');
%                 end
%                 hold on;
%             end
%             
%             subplot(3,1,3)

            intST = 0;
            
            pbx = 2*(mesh.x1 - mesh.x0);
            pby = mesh.y1 - mesh.y0;

            xlim([-mesh.x1 mesh.x1])
            ylim([mesh.y0 mesh.y1])
            
            grid off;
            pbaspect([pbx pby 1])

            shading interp
            view([0,90])
            
            hold on;
            
            X = cell(mesh.numLocalElements,1);
            Y = cell(mesh.numLocalElements,1);
            surfaceTension = cell(mesh.numLocalElements,1);

            %% Create a meshgrid
            for e=1:mesh.numLocalElements
                [X{e},Y{e}] = meshgrid( mesh.getLocalElement(e).xelem,mesh.getLocalElement(e).yelem );
            end
            
            %% Compute the surface tension data
            for e=1:mesh.numLocalElements
                temp = sqrt(fx{e}.^2+fy{e}.^2);
                temp = reshape(temp,mesh.getLocalElement(e).disc.Q);
                surfaceTension{e} = temp(:,:,1);
            end
            
            maxST = max( cellfun(@(x) max(x(:)),surfaceTension) );
            
            % Integration of the surface tension
            for e=1:mesh.numLocalElements
                element = mesh.getLocalElement(e);
                wxy = kron(element.disc.wy,element.disc.wx);
                %intST = intST + data((element.disc.Qz-1)*(element.disc.Qx*element.disc.Qy)+1:element.disc.Qz*(element.disc.Qx*element.disc.Qy))' * wxy / (element.J(1)*element.J(2)) ;
                intST = intST + surfaceTension{e}(:) .* wxy/(element.J(1)*element.J(2));

                surface(X{e},Y{e},surfaceTension{e}');
                
                if interface.active && ~isempty(interface.x{e})
                    line(interface.x{e}, interface.y{e}, repmat(maxST,size(interface.x{e})), interface.props{:});
                end
                
                % second half
                surface(-X{e},Y{e},surfaceTension{e}');
                
                if interface.active && ~isempty(interface.x{e})
                    line(-interface.x{e}, interface.y{e}, repmat(maxST,size(interface.x{e})), interface.props{:});
                end
            end
            
            if (System.settings.anal.debug)
                fprintf('Before NMPI_Allreduce in CahnHilliard.plotSurfaceTension\n')
            end
            
%             fprintf('Max of surface tension : %f\n',stMax)

            intST = NMPI.Allreduce(intST,1,'+','intST');
            
%             if (NMPI.instance.rank==0)
%                 fprintf('Total surface tension  : %f (%f pi)\n',surfaceTension,surfaceTension/pi)
%             end



        end
       
        function out = save(s)
            out = [];
            
            % Merge solution from all processors on the master rank
            %c   = s.fields{1}.getGlobalAlpha;
            %omg = s.fields{2}.getGlobalAlpha;
            
            if (NMPI.instance.rank==0 || System.settings.outp.parallelWrite)
                CH                  = struct;
                CH.class            = class(s);
                CH.name             = s.name;
                CH.mesh             = s.mesh.save;
                
                CH.settings         = s.settings;
                
                CH.fields       = cell(s.Nvar,1);
                for n=1:s.Nvar
                    CH.fields{n} = save( s.fields{n} );
                end
                
                % Additional NS settings
                CH.steady     	= s.steady;                 % True if the system of equations is steady
                CH.nonlinear   	= s.nonlinear;            	% True if the equations are nonlinear
                CH.coupled      = s.coupled;                % True if variables of other physics are to be used in the equations
                CH.fdata        = s.fdata;
                CH.exact        = s.exact;                  % True if this physics has an exact solution
                CH.maxLevel     = s.maxLevel;               % Maximum level for AMR
                CH.eqWeights    = s.eqWeights;           	% Weights for every equation
                CH.nConvType    = s.nConvType;              % Residual convergence criteria based on [ value; residual ]
                CH.cConvType    = s.cConvType;              % Coupling convergence criteria based on [ value; residual ]
                CH.maxNonlinear     = s.maxNonlinear;           	% Maximum number of nonlinear iterations
                CH.maxCoupling     = s.maxCoupling;           	% Maximum number of coupling iterations
                CH.nConvergence = s.nConvergence;       	% Nonlinear convergence for each variable
                CH.cConvergence = s.cConvergence;        	% Coupling convergence for each variable
                CH.rConvergence = s.rConvergence;           % Residual convergence for each equation and the whole system
                CH.mobilityType = s.mobilityType;
                CH.stabilize    = s.stabilize;
                CH.pfMethod     = s.pfMethod;

                out = CH;
                %save(filename,'CH','-append');
            end
        end
        
        % Function to derive the omega field from the C field
        function deriveOmega(s,variableNumber)
            omgField = s.fields{variableNumber};
            
            switch s.pfMethod
                case {pfMethod.balancedCH,pfMethod.balancedAC,pfMethod.balancedACH,pfMethod.meanKwakkel}
                    for e=s.mesh.eRange
                        element = s.mesh.getElement(e);
                        el = element.localID;
                        
                        cField = s.fields{1};
                        
                        C   = cField.val2( e );
                        Cx  = cField.x2( e );
                        Cy  = cField.y2( e );
                        Cxx = cField.xx2( e );
                        Cxy = cField.xy2( e );
                        Cyy = cField.yy2( e );
                        
                        normC1 = Cx.^2 + Cy.^2;
                        
                        omgValues = 1/s.Cn_L * ( C.^3 - 3/2 * C.^2 + 1/2 * C ) ...
                                    - s.Cn_L * ( ( Cx.^2 .* Cxx   +   Cy.^2.*Cyy  + 2*Cx.*Cy.*Cxy )./ normC1);
                                
%                          Cnn = ( ( Cx.^2 .* Cxx   +   Cy.^2.*Cyy  + 2*Cx.*Cy.*Cxy )./ normC1);
%                          omgValues = -sqrt( 2 * normC1 ) .* sign(Cnn) - s.Cn_L * Cnn;
%                          omgValues = -sqrt( 2 * normC1 ) - s.Cn_L * Cnn;
                        
                        omgField.setValue(el,omgValues);
                    end
                    
                case {pfMethod.classicCH,pfMethod.classicAC,pfMethod.classicACH}
                    
                    [kx,ky] = s.getCurvature;
                    
                    for e=s.mesh.eRange
                        element = s.mesh.getElement(e);
                        el = element.localID;
                        
                        cField = s.fields{1};
                        
                        C   = cField.val2( e );
                        Cx  = cField.x2( e );
                        Cy  = cField.y2( e );
                        Cxx = cField.xx2( e );
                        Cxy = cField.xy2( e );
                        Cyy = cField.yy2( e );
                        
                        k = sqrt(kx{el}.^2+ky{el}.^2);
                        
                        omgValues = 1/s.Cn_L * ( C.^3 - 3/2 * C.^2 + 1/2 * C ) ...
                                    - s.Cn_L * ( Cxx + Cyy ) - s.Cn_L * k(:);
                                
                        %omgValues = omgValues / s.Cn_L;
                                
                        %omgValues = omgValues * 0;
                        
                        omgField.setValue(el,omgValues);
                    end
                    
                case {pfMethod.kappa}
                    for e=s.mesh.eRange
                        element = s.mesh.getElement(e);
                        el = element.localID;
                        
                        cField = s.fields{1};
                        
                        C   = cField.val2( e );
                        Cx  = cField.x2( e );
                        Cy  = cField.y2( e );
                        Cxx = cField.xx2( e );
                        Cxy = cField.xy2( e );
                        Cyy = cField.yy2( e );
                        
                        normC1 = Cx.^2 + Cy.^2;
                        
                        omgValues = ( ( Cx.^2 .* Cxx   +   Cy.^2.*Cyy  + 2*Cx.*Cy.*Cxy )./ normC1);
                        
                        omgField.setValue(el,omgValues);
                    end
                    
                otherwise 
                    error('Only balancedCH is supported in deriveOmega')
            end
            
            omgField.computeAlpha;
        end
        
        %%%%%%%%%%% FLAT INTERFACE
        function out = addFlatInterface(s,variable,x,y,z,t,center,normal)
            switch variable
                case Variable.c
                    out = s.initialFlatInterface(x,y,z,t,center,normal);
                case Variable.omg
                    out = 0 * s.initialFlatInterface(x,y,z,t,center,normal);
            end
        end
        
        function out = addFlatInterfaceStretch(s,variable,x,y,z,t,center,normal,stretch)
            switch variable
                case Variable.c
                    out = s.initialFlatInterfaceStretch(x,y,z,t,center,normal,stretch);
                case Variable.omg
                    out = 0 * s.initialFlatInterfaceStretch(x,y,z,t,center,normal,stretch);
            end
        end
        
        %%%%%%%%%%% DROPLET
        function [varargout] = addDroplet(s,variable,x,y,z,t,center,radius,radius2)
            
            if (s.mesh.sDim==2)
                center(3:end) = 0;
            end
            
            switch variable
                case Variable.c
                    if (nargin==8)% || radius==radius2 )
                        varargout = cell(1,3);
                        [varargout{1:3}] = s.initialCircle(x,y,z,t,center,radius);
                    else
                        varargout = cell(1,3);
                        [varargout{1:3}] = s.initialEllipse(x,y,z,t,center,radius,radius2);
                    end
                case Variable.omg
                    if (nargin==8)
                        varargout = cell(1,3);
                        [varargout{1:3}] = s.initialOmg(x,y,z,t,center,radius);
                    else
                        varargout = cell(1,3);
                        [varargout{1:3}] = s.initialEllipseOmg(x,y,z,t,center,radius,radius2);
                    end
            end
        end
        
%         function [varargout] = addDropletStretch(s,variable,x,y,z,t,center,radius,radius2,stretch)
%             
%             if (s.mesh.sDim==2)
%                 center(3:end) = 0;
%             end
%             
%             switch variable
%                 case Variable.c
% %                     if (nargin==8 || radius==radius2 )
%                         varargout = cell(1,3);
%                         [varargout{1:3}] = s.initialCircle(x,y,z,t,center,radius,stretch);
% %                     else
% %                         varargout = cell(1,3);
% %                         [varargout{1:3}] = s.initialEllipse(x,y,z,t,center,radius,radius2);
% %                     end
%                 case Variable.omg
% %                     if (nargin==8)
%                         varargout = cell(1,3);
%                         [varargout{1:3}] = s.initialOmg(x,y,z,t,center,radius);
% %                     else
% %                         varargout = cell(1,3);
% %                         [varargout{1:3}] = s.initialEllipseOmg(x,y,z,t,center,radius,radius2);
% %                     end
%             end
%         end
        
        function [varargout] = addTaylorDroplet(s,variable,x,y,z,t,center,radius,length)
            
            if (s.mesh.sDim==2)
                center(3:end) = 0;
            end
            
            switch variable
                case Variable.c
                    varargout = cell(1,3);
                    [varargout{1:3}] = s.initialTaylorC(x,y,z,t,center,radius,length);
                case Variable.omg
                    varargout = cell(1,3);
                    [varargout{1:3}] = 0*s.initialTaylorC(x,y,z,t,center,radius,length);
            end
        end
        
        % Function to initialize a spinodal decompostion
        %   Based on the equation used in Yang et al. (2017)
        %
        %       C = 0.5 + delta * rand(x,y,z)
        %   
        %   where rand is a random number from a normal random 
        %   distribution with zero mean. This is slightly different
        %   from Yang et al. (2017), as not all random values are in 
        %   the [-1,+1] domain.
        %
        function out = addSpinodal(s,variable,x,y,z,t,average,delta,box)
            rng shuffle
            
            if nargin==8
                randomValues = randn( size(x) );
                randomValues = randomValues - mean(randomValues);

                out = average + delta * randomValues;
                
            else
                randomValues = randn( size(x) );
                randomValues = randomValues - mean(randomValues);

                average = average / ( prod(box(:,2)-box(:,1)) );
                
                out = average + delta * randomValues;
                
                flags = false( numel(out),1 );
                flags( logical( ( (x(:)>box(1,1)) .* (x(:)<box(1,2))) .* ( (y(:)>box(2,1)) .* (y(:)<box(2,2)))) ) = true;
                
                out( ~flags ) = 0;
            end
        end
        
        function out = addDroplet3D(s,variable,x,y,z,t,center,radius)
            if (variable==1)
                c = zeros( size(x) );
            
                % Center location
                xc = center(1);
                yc = center(2);
                zc = center(3);

                % The interface thickness is scaled with Cn_L, which is equal
                % to eps/L. In the settings Cn = eps/R, so here the radius
                % (scaled by L) can be used.
                % Through this the interface thickness is proportional to the
                % radius, and not to the length scale that is used to
                % nondimensionalize the equations.
                denominator = (2*sqrt(2)*s.Cn_L);

                for i=1:numel(x)
                    dx2 = (x(i)-xc)^2;
                    dy2 = (y(i)-yc)^2;
                    dz2 = (z(i)-zc)^2;

                    r = sqrt( dx2 + dy2 + dz2 );

                    %% VALUE
                    c(i) = 0.5 + 0.5 * tanh( (radius - r) / (denominator) ); 
                end

                out = c;
            else
                out = zeros(size(x));
            end
        end
        
        function out = addTorus3D(s,variable,x,y,z,t,center,R,r)
            % Function to create a torus shaped initial condition. The
            % major radius is given by R, the minor radius by r. The aspect
            % ratio is defined as R/r, and is typically a value between 2 and 3.
                        
            if (variable==1)                
                % This LS function defines phi>0 inside the torus, and phi<0 
                % outside. The interface itself is defined by phi=0
                phi = -( (sqrt(x.^2 + y.^2) - R).^2 + z.^2 - r.^2 );
                        
                % Convert phi (LS value) to C (phase-field value). The
                % interface is now defined by C=0.5, C>0.5 is inside and 
                % C<0.5 is outside. The denominator is to have control over
                % the thickness of the diffuse interface.
                denominator = (2*sqrt(2)*s.Cn_L);
                out = 0.5 + 0.5 * tanh( phi / denominator);
                  
            else
                out = zeros(size(x));              
            end
        end
        
        function out = addComplex3D(s,variable,x,y,z,t,center,D)
            % Function to return the initial condition as used by Zhang and
            % Ye (2016), see equation 43 of their article
            
            if (variable==1)                
                % This LS function defines phi>0 inside the complex structure, and
                % phi<0 outside. The interface itself is defined by phi=0
                phi = 1.0 - ( (x.^2+y.^2-D^2).^2 + (z-1).^2 .* (z+1).^2 ) .* ...
                            ( (y.^2+z.^2-D^2).^2 + (x-1).^2 .* (x+1).^2 ) .* ...
                            ( (z.^2+x.^2-D^2).^2 + (y-1).^2 .* (y+1).^2 );

                % Convert phi (LS value) to C (phase-field value). The
                % interface is now defined by C=0.5, C>0.5 is inside and 
                % C<0.5 is outside. The denominator is to have control over
                % the thickness of the diffuse interface.
                denominator = (2*sqrt(2)*s.Cn_L);
                out = 0.5 + 0.5 * tanh( phi / denominator);
                  
            else
                out = zeros(size(x));              
            end
        end
        
       	function preTimeLoop1(s)
            if (s.usePreTimeLoop)
                
                s.preTimeStep();
                
                %% Apply boundary conditions
                s.resetBoundaryConditions();
                activeSettings = System.settings;
                activeSettings.setBoundaryConditions( s );

                timeMethod = s.timeMethod;
                
                % By solving the steady equations, the solution will be
                % obtained faster
                s.Pe            = 0;
                s.PEI           = 0;
                s.steady        = false;
                
                %s.timeMethod    = TimeMethod.EulerImplicit;
                s.coupled       = false; 
                s.maxNonlinear  = 5;
                
                s.timeMethod = TimeMethod.Steady;
                
                eqWeights_tmp = s.eqWeights;
                
                if (System.settings.time.method==TimeMethod.SpaceTime)
                    s.eqWeights(1) = 1;
                    s.eqWeights(2) = 1;
                else
                    s.eqWeights(1) = 1;
                    s.eqWeights(2) = 1;
                end

                %% Solve nonlinear equations 
%                 s.shiftTimeSolutions(TimeMethod.EulerImplicit);
                s.solveNonLinear(1);

                s.steady    = s.settings.steady;
                s.coupled   = s.settings.coupled;
                s.maxNonlinear  = s.settings.maxNonlinear;
                s.Pe        = System.settings.phys.Pe;
                s.PEI       = 1 / s.Pe;
                %s.eps       = s.settings.eps;
                %s.eqWeights = s.settings.eqWeights;
                s.eqWeights = eqWeights_tmp;
                s.timeMethod    = timeMethod;
            end
        end
        
       	function preTimeLoop(s)
            if (s.stabilize)
                s.stabilizeInterface;
            end
        end

        function updateBC(s)
            s.resetBoundaryConditions();
            activeSettings = System.settings;
            activeSettings.setBoundaryConditions( s );
        end
        
        function plotRatio(s)
            cField   = s.fields{1};
            omgField = s.fields{2};
            
            scaled = 1; %1/s.Cn_L;
            
            for e=1:s.mesh.numLocalElements
                element = s.mesh.getElement(e);
                dataCurrent = reshape(cField.val(FieldType.Current,e),element.finite.qSize);
                dataExact   = reshape(cField.val(FieldType.Exact,e),element.finite.qSize);
                dataDiff    = dataCurrent-dataExact;
                dataOmg     = reshape(omgField.val(FieldType.Current,e),element.finite.qSize);
                [X,Y,Z] = element.getNodes;
                
                Ck = cField.val2(e);
                Cx = cField.x2(e);
                Cy = cField.y2(e);
                
                gradC = sqrt( Cx.^2 + Cy.^2 );
                %gradC = reshape( 1./( 2*abs(Ck-0.5)), element.finite.qSize);
                %gradC = abs(Ck-0.5);
                %gradC = ones( size(gradC) );
                
                gradC = reshape( gradC, element.finite.qSize);
                
                %surf(X,Y,scaled*dataDiff ./ (s.Cn_L * 4*sqrt(2) * dataOmg .* gradC ));
                surf(X,Y,dataDiff ./ (s.Cn_L * sqrt(2) * dataOmg .* ( 1 + gradC) ) ); %./ (s.Cn_L * 2*sqrt(2) * dataOmg ));
                
                hold on;
            end
            
            zlim([-0.5 2.5])
            
            %axis([-1.5 1.5 -0.5 0.5 -5 5])
                        
            colorbar;
            view([25,25]);
        end
        
        function plotOmegaExtra(s)
            cField   = s.fields{1};
            omgField = s.fields{2};
            
            scaled = 1; %1/s.Cn_L;
            
            for e=1:s.mesh.numLocalElements
                element = s.mesh.getElement(e);
                %dataCurrent = reshape(cField.val(FieldType.Current,e),element.finite.qSize);
                %dataExact   = reshape(cField.val(FieldType.Exact,e),element.finite.qSize);
                %dataDiff    = dataCurrent-dataExact;
                %dataOmg     = reshape(omgField.val(FieldType.Current,e),element.finite.qSize);
                [X,Y,Z] = element.getNodes;
                
                Ck = cField.val2(e);
                Cx = cField.x(FieldType.Current,e);
                Cy = cField.y(FieldType.Current,e);
                Cnn = cField.nn(FieldType.Current,e);
                lapC = cField.xx(FieldType.Current,e) + cField.yy(FieldType.Current,e);
                
                gradC = sqrt( Cx.^2 + Cy.^2 );
                %gradC = reshape( 1./( 2*abs(Ck-0.5)), element.finite.qSize);
                %gradC = abs(Ck-0.5);
                %gradC = ones( size(gradC) );
                
                %gradC = reshape( gradC, element.finite.qSize);
                
                local    = 1/s.Cn_L * (Ck.^3 - 3/2*Ck.^2 + Ck/2);
                gradient = s.Cn_L * gradC .* ( Cnn );
                cross    = 1/sqrt(2) * (2*(0.5-Ck) .* gradC + Ck.*(1-Ck).*Cnn);
                
                values = (local + gradient) - cross;
                %values = gradC;
                
                values = reshape( values, element.finite.qSize);
                
                %surf(X,Y,scaled*dataDiff ./ (s.Cn_L * 4*sqrt(2) * dataOmg .* gradC ));
                surf(X,Y,values); %./ (s.Cn_L * 2*sqrt(2) * dataOmg ));
                
                hold on;
            end
            
            %zlim([0 8])
            
            %axis([-1.5 1.5 -0.5 0.5 -5 5])
                        
            colorbar;
            view([25,25]);
        end
        
        function plotHelmholtzFreeEnergy(s)
            cField   = s.fields{1};
            omgField = s.fields{2};
            
            scaled = 1; %1/s.Cn_L;
            
            for e=1:s.mesh.numLocalElements
                element = s.mesh.getElement(e);
                %dataCurrent = reshape(cField.val(FieldType.Current,e),element.finite.qSize);
                %dataExact   = reshape(cField.val(FieldType.Exact,e),element.finite.qSize);
                %dataDiff    = dataCurrent-dataExact;
                %dataOmg     = reshape(omgField.val(FieldType.Current,e),element.finite.qSize);
                [X,Y,Z] = element.getNodes;
                
                Ck = cField.val2(e);
                Cx = cField.x(FieldType.Current,e);
                Cy = cField.y(FieldType.Current,e);
                Cnn = cField.nn(FieldType.Current,e);
                
                gradC = sqrt( Cx.^2 + Cy.^2 );
                %gradC = reshape( 1./( 2*abs(Ck-0.5)), element.finite.qSize);
                %gradC = abs(Ck-0.5);
                %gradC = ones( size(gradC) );
                
                %gradC = reshape( gradC, element.finite.qSize);
                
                local    = 0.25 * Ck.^2 .* (1-Ck).^2;
                gradient = 0.5 * s.Cn_L^2 * gradC.^2;
                cross    = 0.5*Ck.*(1-Ck) .* s.Cn_L/sqrt(2) .* gradC;
                
                values = local(:) + gradient(:) - 2*cross(:);
                %values = gradC;
                
                values = reshape( values, element.finite.qSize);
                
                %surf(X,Y,scaled*dataDiff ./ (s.Cn_L * 4*sqrt(2) * dataOmg .* gradC ));
                surf(X,Y,values); %./ (s.Cn_L * 2*sqrt(2) * dataOmg ));
                
                hold on;
            end
            
            %zlim([0 8])
            
            %axis([-1.5 1.5 -0.5 0.5 -5 5])
                        
            colorbar;
            view([25,25]);
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    end
    
    methods(Access=protected)
        function setC(s,value)
%             c.value = value;
            s.c = cell(s.mesh.getNumLocalElements,1);
            for e=1:s.mesh.getNumLocalElements
                element = s.mesh.getLocalElement(e);
                s.c{e} = value*ones(element.disc.dofeq);
            end
        end
        
        function setOmg(s,value)
            s.omg = cell(s.mesh.getNumLocalElements,1);
            for e=1:s.mesh.getNumLocalElements
                element = s.mesh.getLocalElement(e);
                s.omg{e} = value*ones(element.disc.dofeq);
            end
        end
        
        function addC(s,value)
            if (isa(value,'function_handle'))
                for e=1:s.mesh.getNumLocalElements
                    element = s.mesh.getLocalElement(e);

                    x = element.xelem;
                    y = element.yelem;
                    z = element.zelem;
                    
                    s.c{e} = max( s.c{e} , value(x,y,z) );
                end
            else
                for e=1:s.mesh.getNe
                    s.c{e} = max( s.c{e} , value );
                end
            end
        end
        
        % Function which defines an interface through a specified point with 
        % a given normal. The x,y,z data are the locations where the C
        % data needs to be evaluated. The center is a vector of length 3,
        % which contains its x,y,z data. On the left of the interface C=1, 
        % and on the right side C=0
        function c = initialFlatInterface(s,x,y,z,t,point,normal)
            if length(point)~=3
                error('The point should have length == 3')
            end
            if length(normal)~=3
                error('The normal vector should have length == 3')
            end       
            
            elementSize = size(x);
            
            c = zeros(elementSize);
            
            denominator = (2*sqrt(2)*s.Cn_L);

            xp = point(1);
            yp = point(2);
            %zp = center(3);
            
            if ( all( abs(normal)==[1,0,0]) )
                for i=1:numel(x)
                    c(i) = 0.5 + 0.5 * tanh( sign(normal(1))*( xp - x(i) ) / denominator );
                end
                
            elseif ( all( abs(normal)==[0,1,0]) )
                for i=1:numel(y)
                    c(i) = 0.5 + 0.5 * tanh( sign(normal(2))*( yp - y(i) ) / denominator );
                end
                
            else
                error('This normal has not been implemented yet')
            end
        end
        
        % Function which defines an interface through a specified point with 
        % a given normal. The x,y,z data are the locations where the C
        % data needs to be evaluated. The center is a vector of length 3,
        % which contains its x,y,z data. On the left of the interface C=1, 
        % and on the right side C=0
        function c = initialFlatInterfaceStretch(s,x,y,z,t,point,normal,stretch)
            if length(point)<s.mesh.dim
                error('The point should have length == %d',s.mesh.dim)
            end
            if length(normal)<s.mesh.dim
                error('The normal vector should have length == %d',s.mesh.dim)
            end       
            
            elementSize = size(x);
            
            c = zeros(elementSize);
            
            denominator = (2*sqrt(2)*s.Cn_L*stretch);

            xDir = [1,0,0];
            yDir = [0,1,0];
            
            xp = point(1);
            yp = point(2);
            %zp = center(3);
            
            if ( all( abs(normal(1:s.mesh.dim))==xDir(1:s.mesh.dim)) )
                for i=1:numel(x)
                    c(i) = 0.5 + 0.5 * tanh( sign(normal(1))*( xp - x(i) ) / denominator );
                end
                
            elseif ( all( abs(normal(1:s.mesh.dim))==yDir(1:s.mesh.dim)) )
                for i=1:numel(y)
                    c(i) = 0.5 + 0.5 * tanh( sign(normal(2))*( yp - y(i) ) / denominator );
                end
                
            elseif ( abs(normal(1)) - abs(normal(2)) < 1e-13 )  % the interface normal points mainly in y-direction
                for i=1:numel(y)
                    distance = normal(1) * ( xp - x(i) ) + normal(2) * ( yp - y(i) );
                    c(i) = 0.5 + 0.5 * tanh( -distance / denominator );
                    %c(i) = 0.5 + 0.5 * sin( sign(distance) * min(1,abs(distance)) * 0.5 * pi) ;
                end
                
            else
                for i=1:numel(y)
                    distance = normal(1) * ( xp - x(i) ) + normal(2) * ( yp - y(i) );
                    c(i) = 0.5 + 0.5 * tanh( -distance / denominator );
                end
                
%                 error('This normal has not been implemented yet')
            end
        end

        %% Stabilize the interfaces
        % By the discretization, the interface fields (C and omega) are not 
        % in balance yet (even when the exact initial solution for both are
        % used). This function solves the system of a steady and uncoupled 
        % (velocities are zero, so no interface advection) system of
        % equations. This will only slighlty adjust both C and omega
        % fields, and ensure there is no interface movement due to
        % discretization limitations.
        function stabilizeInterface(s)
            if (System.rank==0 && System.settings.outp.verbose)
                System.newLine();
                fprintf('PHYSICS    : Interface stabilization started\n')
            end
            
            time1 = toc;
            
            %System.setVariableOffsets(Physics);
            %System.setItem(1,'timestep',0,'%8d');
            %System.setItem(2,'time level',0,'%14.2e');
            
            tempNL = s.maxNonlinear;
            tempC  = s.maxCoupling;
            
            s.maxNonlinear = 2;
            %s.maxCoupling  = 1;
            %s.coupled = false;
            
            temp = s.PEI;
            s.PEI = 1;
            
            activeSettings = System.settings;
            tempTime = activeSettings.time.step;
            activeSettings.setValue('time.step',5);
            
            for t=1:5
                PhysicsCollection.solveTimeStep(t,1); 
            end
            
            s.PEI = temp;
            activeSettings.setValue('time.step',tempTime);
            s.maxNonlinear = tempNL;
%             
%             for t=1:1000
%                 %% Apply boundary conditions
%                 s.resetBoundaryConditions();
%                 activeSettings = System.settings;
%                 activeSettings.setBoundaryConditions( s );
% 
%                 if (System.settings.time.spaceTime)
%                     s.updateTimeSlab();
%                     if (~s.steady)
%                         %% Apply initial conditions
%                         s.applyInitialConditions();
%                     end
%                 elseif ~isempty(System.settings.time.method)
%                     s.copySolution(FieldType.Timelevel1);
%                 else
%                     s.copySolution(FieldType.Timelevel1);
%                 end
%             
%             %% Solve nonlinear equations
% %             settings = System.settings;
% %             couplingSetting = settings.getCoupling;
% %             settings.setConvergence([true; false],[true; false]);
%             
% %             for cIter=1:System.settings.comp.coupling.maxIter                       
%                 time1 = toc;
% % 
% %                 System.setIter('coupling',cIter);
% % 
% %                 isConvergedValue    = false(numel(System.settings.phys.classes),1);
% %                 isConvergedResidual = false(numel(System.settings.phys.classes),1);
%                 
%                 % Solve the nonlinear equations for each element in the
%                 % Physics cell array.
%                 
% 
%                 
%                 %for i=1:numel(System.settings.phys.classes)                
% %                     if ( isa(Physics{i},'NavierStokes') )
% %                         Physics{i}.isSteady = true;
% %                         Physics{i}.setZeroGravity(true);
% %                         settings.setCoupling(true);
% % %                         settings.setInitialWeight(0);
% %                     elseif ( isa(Physics{i},'CahnHilliard') )
% %                         Physics{i}.isSteady = false;
% %                         settings.setCoupling(false);
% % %                         settings.setInitialWeight(100);
% %                     end
% % 
% % %                     for j=1:numel(System.settings.phys.classes)
% % %                         Physics{j}.activeField = FieldType.Coupled;
% % %                     end
% %                     
% %                     for j=1:numel(System.settings.phys.classes)
% %                         Physics{j}.activeField = FieldType.Interpolated;
% %                         Physics{j}.updateInterpolated(Physics)
% %                     end
% %                     Physics{i}.solveNonLinear(Physics);
% 
%                 %if (~System.settings.anal.useCellFun)
%                  %   for i=1:numel(System.settings.phys.classes)  
%                 try
%                     %s.steady = true;
% 
%                     %s.Pe = 1;
%                     s.solveNonLinear(0);
% 
%                     %s.Pe = System.settings.phys.Pe;
% 
%                     %s.steady = false;
%                 catch Mexp
%                     disp('ERROR in solveNonLinear (CahnHilliard.m)')
%                     save(['solveNonlinear.' int2str(System.rank) '.mat']);
%                 end   
%                     
%             end
%             
%             s.maxNonlinear = temp;
%             s.coupled = s.settings.coupled;
%             
                %end
                                
%                 time2 = toc;
% 
%                 % Output the time of the coupling step, and disable the
%                 % iteration output
%                 System.setItem(3,'iter',cIter,'%4d');
%                 System.setItem(10,'time',time2-time1,'%10.2f');
%                 System.setItemFlags(11,false);
%                 System.outputNonLinear(Physics);
%                 
%                 % Update the mesh
% %                 for i=1:numel(System.settings.phys.classes) 
% %                     Physics{i}.meshUpdate(System.settings.mesh.maxLevel(i));
% %                 end
%                 if ( ( all(isConvergedValue)     && System.settings.comp.coupling.convType(1) ) || ...
%                      ( all(isConvergedResidual)  && System.settings.comp.coupling.convType(2) ) )
%                     break
%                 end
%                 
%                 %if ( all(isConvergedValue) )
%                 %    break
%                 %end
%             end
            
%             % Reset some values for further computation
%             for i=1:numel(System.settings.phys.classes)                
%                 if ( isa(s,'NavierStokes') )
%                     s.isSteady = false;
%                     s.setZeroGravity(false);
%                 end
%             end

%             settings.setCoupling(couplingSetting);
%             settings.setConvergence([true; false],[true; false]);
            
            if (System.rank==0 && System.settings.outp.verbose)
                fprintf('PHYSICS    : Interface stabilization finished, wall clock time : %f\n',toc-time1)
            end
        end

    end
    
    methods (Static)
        function out = load(solution)
            if (~isa(solution,'Solution'))
                error('The first argument of load should be a Solution object')
            end
            out = CahnHilliard;
            out.loadObject(solution);
        end 
    end    
end

