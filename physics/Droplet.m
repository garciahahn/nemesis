classdef Droplet < CahnHilliard
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods        
        function addDroplet(s,center,radius)            
            fh = @(x,y,z,t) s.initialCircle(x,y,z,t,center,radius);
            %s.cField.add( fh );
            s.initField(1,FieldType.Current,fh)
            
            fh = @(x,y,z,t) s.initialOmg(x,y,z,t,center,radius);
            %s.omgField.add( fh );
            s.initField(2,FieldType.Current,fh)
        end
        
        function out = exactC(s,x,y,z,t,center,radius)
            out = squeeze( s.initialCircle(x,y,z,t,center,radius) );
        end
        
        function out = exactOmg(s,x,y,z,t,center,radius)
            out = squeeze( s.initialOmg(x,y,z,t,center,radius) );
        end
    end
    
%     methods (Access=protected)
%         % Function which defines an circle through a specified center with 
%         % a given radius. The x,y,z data are the locations where the C
%         % data needs to be evaluated. The center is a vector of length 3,
%         % which contains its x,y,z data. Inside the circle C=1, outside C=0
%         function c = initialCircle(s,x,y,z,t,center,radius)
%             if length(center)~=3
%                 center( numel(center)+1:4 ) = 0;
%             end
%             
%             denominator = (2*sqrt(2)*s.Cn);
% 
%             xc = center(1);
%             yc = center(2);
%             zc = center(3);
%             
%             c = 0.5 + 0.5 * tanh( (radius - sqrt( (x-xc).^2 + (y-yc).^2 + (z-zc).^2) ) / denominator );
%         end
%         
%         % Analytical solution for omega, which complies with the solution for
%         % the variable c (see above). Omega is defined by:
%         % 
%         %   omega = 1/eps * (C^3 - 3/2*C^2 + 1/2*C) - eps * laplacian(C)
%         %
%         function omg = initialOmg(s,x,y,z,t,center,radius)
%             if length(center)~=3
%                 center( numel(center)+1:4 ) = 0;
%             end
%             
%             xc = center(1);
%             yc = center(2);
%             zc = center(3);
%                         
%             %factor = s.alp/(s.We);
%             denominator = (2*sqrt(2)*s.Cn);
%             
%             % Compute r, and adjust if r is smaller than the radius
%             r = sqrt( (x-xc).^2 + (y-yc).^2 + (z-zc).^2 );
%             r( r<radius ) = 2*radius - r( r<radius );
%             
%             %omg = 0.5*sqrt(2) * sinh( (r-radius)/denominator ) ./ ( s.Cn * cosh( (r-radius) / denominator ).^3);
%             
%             % Only laplacian
% %             omg( r~=0 ) = omg( r~=0 ) - ( 0.5*sqrt(2) * sinh( (r( r~=0 )-radius)/denominator ) .* r( r~=0 ) - s.Cn*cosh( (r( r~=0 )-radius)/denominator ) ) ./ ...
% %                                                                                         ( s.Cn*r( r~=0 ).*cosh( (r( r~=0 )-radius)/denominator ).^3 );
%             omg = zeros( size(r) );
%             omg( r~=0 ) = s.alpha/s.Cn * (s.Cn*sqrt(2)) / (8*r( r~=0 ).*cosh( (r( r~=0 )-radius)./denominator ).^2);
%             omg( r==0 ) = omg( r==0 ) + 0;
%             
%             if (nnz(any(omg<0))>0)
%                 omg(omg<0) = min(min(min(omg(omg>0))));
%             end
%             
%             %omg = omg * 3/2;
%         end
%     end
    
end

