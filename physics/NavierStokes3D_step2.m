classdef NavierStokes3D_step2 < Physics
    %NAVIERSTOKES3D Physics file for the steady 3D Navier-Stokes problem
    
    properties (Access=protected)
        REI; FRI2; WEI;
        
        % NS properties
        Ja; Pr; Re; We; Fr; 
        gx; gy;
        
        g               = 0;
        lamRho          = 1;
        lamMu           = 1;
        eps             = 0;
        sharpRho        = false;
        sharpMu         = false;
        linearization   = Linearization.Newton;
    end
    
    methods
        % Class constructor
        function s = NavierStokes3D_step2(mesh,settings)
            s = s@Physics(mesh);
                        
            if nargin>0
                s.shortName = 'N2';
                s.longName  = 'NavierStokes3D_step2';
                s.name = 'N2';

                s.nEq = 4;
                s.variables = {'u','v','w'};
                
                s.initialize(settings);
            end
        end
        
        % Function to set all properties 
        function initialize(s,settings)
            % First initialize the basic settings
            initialize@Physics(s,settings);
            
            % Additional settings (physics dependent)
            s.Re        = System.settings.phys.Re;   
            s.We        = System.settings.phys.We;
            s.Fr        = System.settings.phys.Fr;
            s.Pr        = System.settings.phys.Pr;
            s.Ja        = System.settings.phys.Ja;
            s.g         = System.settings.phys.g;
            s.lamRho    = System.settings.phys.lamRho;
            s.lamMu     = System.settings.phys.lamMu;
            
            s.REI  = 1.0/s.Re;

            if (s.We>0)
                s.WEI = 1.0/s.We;
            else
                s.WEI = 0;
            end

            if (s.Fr>0)
                s.FRI2 = 1.0/s.Fr;
            else 
                s.FRI2 = 0;
            end                

            if (s.g==0)
                s.gx = 0;
                s.gy = 0;
            else
                s.gx = s.g(1);
                s.gy = s.g(2);
            end
            
            % Navier-Stokes specific settings
            if (isfield(settings,'eps'))
                s.eps = settings.eps;
            end
            
            if (isfield(settings,'sharpRho'))
                s.sharpRho = settings.sharpRho;
            end
            
            if (isfield(settings,'sharpMu'))
                s.sharpMu = settings.sharpMu;
            end
            
            if (isfield(settings,'pressureGradient'))
                s.gradP   = settings.pressureGradient;
            end
            
            if (isfield(settings,'usePreTimeLoop'))
                s.usePreTimeLoop   = settings.usePreTimeLoop; 
            end
            
            if (isfield(settings,'usePreTimeStep'))
                s.usePreTimeStep   = settings.usePreTimeStep; 
            end
            
            if (isfield(settings,'useMassFlux'))
                s.useMassFlux = settings.useMassFlux;
            end
        end
                
        % Setup the Stokes3D equations
        function time = setupEquations(s) %,physics)
            t1 = toc;
            
            U	= FieldCollection.get('u');
            V   = FieldCollection.get('v');
            W   = FieldCollection.get('w');
            C   = FieldCollection.get('c');
            
            % Get the curvature from the CH simulation, use the NS.mesh as
            % the base mesh for the data
            %CH = PhysicsCollection.get('CH');
            %[kx,ky,kz] = CH.getCurvature_3D(s.mesh);
            kx = []; ky = []; kz = []; 
            
            for e=s.mesh.eRange
                %t(1) = toc;
                
                %% Select the active element and assing the local element number
                element = s.mesh.getElement(e);
                el = element.localID;
                                
                %% Get all elemental operators/arrays
                H       = element.getH;
                Dx      = element.getDx;
                Dy      = element.getDy;
                Dz      = element.getDz;
                if (s.steady)
                    Dt = 0;
                else
                    Dt       = s.getTime( element );
                    dUdt_rhs = s.getTime( element, U );
                    dVdt_rhs = s.getTime( element, V );
                    dWdt_rhs = s.getTime( element, W );
                end
                Dxx     = element.getDxx;
                Dyy     = element.getDyy;
                Dzz     = element.getDzz;
                Dxy     = element.getDxy;
                Dxz     = element.getDxz;
                Dyz     = element.getDyz;

                Wel     = element.getW;
                matZ    = element.getMatZ;
                vecZ    = element.getVecZ;
                
                Uk    = U.val(FieldType.Current,e);
                Vk    = V.val(FieldType.Current,e);
                Wk    = W.val(FieldType.Current,e);
                dUkdx = U.x(FieldType.Current,e);
                dUkdy = U.y(FieldType.Current,e);
                dUkdz = U.z(FieldType.Current,e);
                dVkdx = V.x(FieldType.Current,e);
                dVkdy = V.y(FieldType.Current,e);
                dVkdz = V.z(FieldType.Current,e);
                dWkdx = W.x(FieldType.Current,e);
                dWkdy = W.y(FieldType.Current,e);
                dWkdz = W.z(FieldType.Current,e);
                
                %[px1,py1,pz1] = s.getOldValue(element,P,1);
                
                % Try some old values
                %Ck_1  = s.getOldValue(element,C,1);
                %rho_1 = (1-s.lamRho)*Ck_1 + s.lamRho;
                
                if (isempty(C) || C~=C)
                    Ck      = 1;
                    dCkdx   = 0;
                    dCkdy   = 0;
                    dCkdz   = 0;

                else
                    Ck      = C.val2(e,s.mesh);
                    dCkdx   = C.x2(e,s.mesh);
                    dCkdy   = C.y2(e,s.mesh);
                    dCkdz   = C.z2(e,s.mesh);
                end
                
                %% Density
                if (~s.sharpRho)
                    rho     = (1-s.lamRho)*Ck + s.lamRho ;
                    drhodx  = (1-s.lamRho )*dCkdx ;
                    drhody  = (1-s.lamRho )*dCkdy ;
                    drhodz  = (1-s.lamRho )*dCkdz ;
                else
                    Hs = Heaviside(Ck);
                    rho     = (1-s.lamRho)*Hs + s.lamRho ;
                end
                
                %% Dynamic viscosity
                if (~s.sharpMu)
                    mu      = (1-s.lamMu )*Ck + s.lamMu ;
                    dmudx   = (1-s.lamMu )*dCkdx ;
                    dmudy   = (1-s.lamMu )*dCkdy ;
                    dmudz   = (1-s.lamMu )*dCkdz ;
                else
                    Hs = Heaviside(Ck);
                    mu     = (1-s.lamMu)*Hs + s.lamMu ;
                    
                    Ds = Delta(Ck);
                    dmudx = (1-s.lamMu )*Ds.*dCkdx ;
                    dmudy = (1-s.lamMu )*Ds.*dCkdy ;
                    dmudz = (1-s.lamMu )*Ds.*dCkdz ;
                end
                
                %% Advection terms
                Comm    = Dt + Uk.*Dx + Vk.*Dy + Wk.*Dz;          % Used for both Picard and Newton linearization
                
                switch s.linearization
                    case Linearization.Newton
                        ConvUU  = Comm + dUkdx .* H;
                        ConvUV  =        dUkdy .* H;
                        ConvUW  =        dUkdz .* H;
                        ConvVU  =        dVkdx .* H;
                        ConvVV  = Comm + dVkdy .* H;
                        ConvVW  =        dVkdz .* H;
                        ConvWU  =        dWkdx .* H;
                        ConvWV  =        dWkdy .* H;
                        ConvWW  = Comm + dWkdz .* H;
                        %ConvGx  = Uk.*dUkdx + Vk.*dUkdy ;
                        %ConvGy  = Uk.*dVkdx + Vk.*dVkdy ; 
                    case Linearization.Picard
                        ConvUU  = Comm;
                        ConvUV  = matZ;
                        ConvVU  = matZ;
                        ConvVV  = Comm;
                        ConvGx  = vecZ;
                        ConvGy  = vecZ;
                end
                
                %% Diffusion terms
                Lapl   = Dxx + Dyy + Dzz;
                Nabla2 = s.REI* mu.*( Dxx + Dyy + Dzz );
                comp   = s.REI/3 * mu;
                    
                % x-momentum
                dVisUU  = s.REI*( 2*dmudx.*Dx +   dmudy.*Dy +   dmudz.*Dz ) + Nabla2 + comp.*Dxx;
                dVisUV  = s.REI*(   dmudy.*Dx                             ) +          comp.*Dxy;
                dVisUW  = s.REI*(   dmudz.*Dx                             ) +          comp.*Dxz;
                
                % y-momentum
                dVisVU  = s.REI*(                 dmudx.*Dy               ) +          comp.*Dxy;
                dVisVV  = s.REI*(   dmudx.*Dx + 2*dmudy.*Dy +   dmudz.*Dz ) + Nabla2 + comp.*Dyy;
                dVisVW  = s.REI*(                 dmudz.*Dy               ) +          comp.*Dyz;
                
                % z-momentum
                dVisWU  = s.REI*(                               dmudx.*Dz ) +          comp.*Dxz;
                dVisWV  = s.REI*(                               dmudx.*Dz ) +          comp.*Dyz;
                dVisWW  = s.REI*(   dmudx.*Dx +   dmudy.*Dy + 2*dmudz.*Dz ) + Nabla2 + comp.*Dzz;
                
                %% Setup L
                %Conv = Dt + Uk.*Dx + Vk.*Dy + Wk.*Dz;
                %Lap  = Dxx + Dyy + Dzz;
                
                dt = System.settings.time.step;
                
                % Vorticities:
                %   omega_x = dVdz - dWdy
                %   omega_y = dWdx - dUdz 
                %   omega_z = dUdy - dVdx
                % 
                %   Lap(omega) = omega_xx + omega_yy + omega_zz = 0
                %
                %   DUDT = dUdt + U*dUdx + V*dUdy + W*dUdz
                %
                %   dUdt + U*dUdx + V*dUdy + W*dUdz + s.REI * ( (omega_y)_z - (omega_z)_y )
                %                                   + s.REI * ( (Wxz-Uzz) - (Uyy-Vxy)   )
                %                                   + s.REI * ( (Ux+Vy+Wz)_x - Lap(U) )
                
                s.opL{el} = [  Dx     Dy     Dz     ; ...   % continuity
                              matZ   -Dz     Dy     ; ...   % omega_x
                               Dz    matZ   -Dx     ; ...   % omega_y
                              -Dy     Dx    matZ    ];      % omega_z

                %% Setup G
                fv1 = vecZ;
                fv2 = dWkdy - dVkdz;
                fv3 = dUkdz - dWkdx;
                fv4 = dVkdx - dUkdy;
                                    
                s.opG{el} = [  fv1  ; ...
                               fv2  ; ...
                               fv3  ;
                               fv4 ];
                
                %% Scale equations with weights (can differ between elements)
                eqW    = repmat(s.eqWeights',numel(vecZ),1);
%                 scaling = [     1./rho        ; ...
%                                 1./rho        ; ...
%                                 1./rho        ; ...
%                             ones(size(vecZ)) ];
%                 eqW    = scaling .* eqW(:);
                s.opL{el} = eqW(:) .* s.opL{el};
                s.opG{el} = eqW(:) .* s.opG{el};
                          
                %% AMR scaling
                Ze = s.mesh.Z{e};
                s.opL{el} = s.opL{el} * superkron(eye(s.Nvar),Ze);
                
                %% Weights for finite elements
                s.W{el}      = repmat(Wel,s.nEq,1);
                s.sqrtW{el}  = repmat(sqrt(Wel),s.nEq,1);
            end
            
            t2 = toc;
            time = t2-t1;
        end
        
        function preTimeStep(s)
        end
        
        function writeValues(s)
        end
                
        function out = save(s)
            % Merge solution from all processors on the master rank
            
            % Initialize the default output from the settings of the
            % current object as a struct
            out  = struct;
            
            % First synchronize the fields in parallel
            out.fields = cell(s.Nvar,1);
            for n=1:s.Nvar
                out.fields{n} = save( s.fields{n} );
            end
            
            if (Parallel3D.MPI.rank==0 || System.settings.outp.parallelWrite )
                out.class        = class(s);
                out.name         = s.name;
                out.mesh         = s.mesh.save;
                
                out.settings     = s.settings;

                % Additional NS settings
                out.steady     	 = s.steady;                 % True if the system of equations is steady
                out.nonlinear    = s.nonlinear;            	% True if the equations are nonlinear
                out.coupled      = s.coupled;                % True if variables of other physics are to be used in the equations
                out.fdata        = s.fdata;
                out.exact        = s.exact;                  % True if this physics has an exact solution
                out.maxLevel     = s.maxLevel;               % Maximum level for AMR
                out.eqWeights    = s.eqWeights;           	% Weights for every equation
                out.nConvType    = s.nConvType;              % Residual convergence criteria based on [ value; residual ]
                out.cConvType    = s.cConvType;              % Coupling convergence criteria based on [ value; residual ]
                out.nMaxIter     = s.nMaxIter;           	% Maximum number of nonlinear iterations
                out.cMaxIter     = s.cMaxIter;           	% Maximum number of coupling iterations
                out.nConvergence = s.nConvergence;       	% Nonlinear convergence for each variable
                out.cConvergence = s.cConvergence;        	% Coupling convergence for each variable
                out.rConvergence = s.rConvergence;           % Residual convergence for each equation and the whole system
                
                out.usePreTimeLoop     = s.usePreTimeLoop;
                out.usePreTimeStep     = s.usePreTimeStep;
            end
        end
        
    end
    
end