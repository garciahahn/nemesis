classdef LinearAdvection < Physics
    %LINEARADVECTION 1D and 2D LinearAdvection equation
    %   As the exact solution is known, the LinearAdvection equation can be 
    %   used to verify the implementation and determine the accuracy of the 
    %   method. 
    
    properties (Access=private)
    end
    
    properties
        % none
    end
    
    methods
        function s = LinearAdvection(mesh,settings)
            s = s@Physics(mesh);
                        
            if nargin>0
                s.name          = 'LA';
                s.shortName     = 'LA';
                s.longName      = 'LinearAdvection';

                s.nEq = 1;
                s.variables = {'u'};
                
                s.initialize(settings);
            end
        end

        function time = setupEquations(s,physics)
            t1 = toc;

            if (nargin==2)
                s.allPhysics = physics;
            end
                                                
%             s.Gi = zeros(s.mesh.numElements,1);
            
            %s.eqWeights = System.settings.comp.LA.equationWeights;
            
            U = FieldCollection.get('u');
            
            for e=s.mesh.eRange
                % Select the active element
                element = s.mesh.getElement(e);
                
                % Get the local element number
                el = element.localID;
                
                % Get all elemental operators/arrays
                vecZ    = element.getVecZ;
                Dx      = element.getDx;
                if (s.mesh.sDim==2)
                    Dy = element.getDy;
                end
                
                Dt       = s.getTime( element );
                dUdt_rhs = s.getTime( element, U );                
                
%                 if (System.settings.time.spaceTime)
%                     %Dt  = element.getDt;
%                     %Dt1 = 0;
%                     theta = 1;
%                     dUdx1 = 0;
%                     dUdy1 = 0;
%                 else
%                     %Dt      = element.getH / System.settings.time.step;
%                     %U       = FieldCollection.get('u');
%                     %Dt1     = U.val(FieldType.Timelevel1,e) / System.settings.time.step; % timelevel n-1
%                     dUdx1   = U.x(FieldType.Timelevel1,e);        % timelevel n-1
%                     dUdy1   = U.y(FieldType.Timelevel1,e);        % timelevel n-1
%                     theta   = 0.5;
%                 end
                
                if (s.mesh.sDim==1)
                    a = 1;
                    s.opL{el} = Dt + a*Dx;                    
                    s.opG{el} = vecZ + dUdt_rhs;
                    
                elseif (s.mesh.sDim==2)
                    nodes = element.getLocations;
                    [X,Y,T] = ndgrid( nodes{:} );
                                        
                    ax =  2*pi*Y(:);
                    ay = -2*pi*X(:);
                    
%                     s.opL{el} = Dt + theta*(ax.*Dx + ay.*Dy);                    
%                     s.opG{el} = vecZ + dUdt_rhs - (1-theta)*(ax.*dUdx1 + ay.*dUdy1);
                    
                    s.opL{el} = Dt + (ax.*Dx + ay.*Dy);                    
                    s.opG{el} = vecZ + dUdt_rhs;
                end
                
                %Z = s.mesh.Z{e};        
                %s.opL{el} = s.opL{el} * superkron(eye(s.Nvar),Z);
                %s.W{el}      = repmat(Wel,s.Nvar,1);
                %s.sqrtW{el}  = repmat(sqrt(Wel),s.Nvar,1);
                
                %% Set quadrature weights, apply all scaling to L and G
                s.setWeightAndScale( element );
            end
            
            t2 = toc;
            time = t2-t1;
        end             
                
        function out = getConvergence(s,fieldType,residual)
            if nargin==2
                switch fieldType
                    case FieldType.Nonlinear
                        out = reshape( System.settings.comp.NS.nConvergence, [numel(System.settings.comp.NS.nConvergence),1] );
                    case FieldType.Coupled
                        out = reshape( System.settings.comp.NS.cConvergence, [numel(System.settings.comp.NS.cConvergence),1] );
                end
            else
                out = reshape( System.settings.comp.NS.rConvergence, [numel(System.settings.comp.NS.rConvergence),1] );
            end
        end
        
%         function save(s,filename)
%             % Merge solution from all processors on the master rank
%             u = s.uField.getGlobalAlpha;
%             
%             if (NMPI.instance.rank==0)
%                 LA              = struct;
%                 LA.class        = class(s);
%                 LA.name         = s.name;
%                 LA.mesh         = s.mesh.save;
%                 LA.uConvergence	= s.uConvergence;
%                 LA.u            = u;
%                 
%                 save(filename,'LA','-append');
%             end
%             
%             
%             
%             
%         end
        
        
        
        function out = save(s)
            % Merge solution from all processors on the master rank
            
            % Initialize the default output from the settings of the
            % current object
            out = [];
            
            %fprintf('Saving the NS to a restart file on rank %d\n',System.rank)
            
            out  = struct;
            
            % First synchronize the fields in parallel
            out.fields = cell(s.Nvar,1);
            for n=1:s.Nvar
                out.fields{n} = save( s.fields{n} );
            end
            
            if (NMPI.instance.rank==0 || System.settings.outp.parallelWrite )
                out.class        = class(s);
                out.name         = s.name;
                out.mesh         = s.mesh.save;
                
                out.settings     = s.settings;

                % Additional NS settings
                out.steady     	= s.steady;                 % True if the system of equations is steady
                out.nonlinear   	= s.nonlinear;            	% True if the equations are nonlinear
                out.fdata        = s.fdata;
                out.exact        = s.exact;                  % True if this physics has an exact solution
                out.maxLevel     = s.maxLevel;               % Maximum level for AMR
                out.eqWeights    = s.eqWeights;           	% Weights for every equation
                out.nConvType    = s.nConvType;              % Residual convergence criteria based on [ value; residual ]
                out.cConvType    = s.cConvType;              % Coupling convergence criteria based on [ value; residual ]
                out.nMaxIter     = s.nMaxIter;           	% Maximum number of nonlinear iterations
                out.cMaxIter     = s.cMaxIter;           	% Maximum number of coupling iterations
                out.nConvergence = s.nConvergence;       	% Nonlinear convergence for each variable
                out.cConvergence = s.cConvergence;        	% Coupling convergence for each variable
                out.rConvergence = s.rConvergence;           % Residual convergence for each equation and the whole system
                out.usePreTimeLoop     = s.usePreTimeLoop;
                out.usePreTimeStep     = s.usePreTimeStep;
            end
        end
        
        
        
        
        function out = uField(s)
            out = s.fields{1};
        end
    end
        
    methods (Access=protected)
        function loadObject(s,solution)
%             NS = solution.data.NS;
%             
%             s.name = NS.name;        
%             s.uConvergence = NS.uConvergence;
%             s.vConvergence = NS.vConvergence;
%             s.pConvergence = NS.pConvergence;
%                        
%             if (isa(NS.mesh,class(MeshRefineMultiD)))
%                 fprintf('Please save the mesh as a struct\n')
%                 mesh = NS.mesh;
%                 meshStruct = mesh.save;
%             elseif (isstruct(NS.mesh))
%                 meshStruct = NS.mesh;
%             else
%                 error('Undefined mesh type')
%             end
%             
%             stde = LSQdisc_5.load(solution.data.standardElement);
%             mesh = MeshRefineMultiD.load(stde,meshStruct);
% 
%             if (isfield(NS.mesh,'name'))
%                 mesh.setName(NS.mesh.name);
%             else
%                 mesh.setName(NS.name);
%             end
%             
%             if (nargin>0)
%                 s.initialize(mesh,NS.Re,NS.We,NS.Fr,NS.g,NS.alpha,NS.lamRho,NS.lamMu,15,15);
%             end
%             
%             s.uField.current.loadAlphaArray(NS.u);
%             
%             s.uField.coupled{1}.loadAlphaArray(NS.u);
        end        
    end
    
    methods (Static)
        function out = load(solution)
            if (~isa(solution,'Solution'))
                error('The first argument of load should be a Solution object')
            end
            out = NavierStokes;
            out.loadObject(solution);
        end
        
        function plotParallel(nproc,variable)
            
            fig = figure(1);
            clf
            
            for i=1:nproc
                disp('NEW PROC')
                temp = load(['NS_' int2str(i-1) '.mat']);
                temp.NS.plotVariable(fig,0,variable,'')
                hold on
            end
            
            drawnow;            
        end 
    end
    
end

