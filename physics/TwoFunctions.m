classdef TwoFunctions < Physics
    %POISSON
    %   Solve the Poisson equation: u_xx + u_yy = f;
    
    properties
    end
    
    methods
        % Class constructor
        function s = TwoFunctions(mesh,settings)
            s = s@Physics(mesh);
                        
            if nargin>0
                s.shortName = '2F';
                s.longName  = 'TwoFunctions';
                s.name = '2F';

                s.nEq = 2;
                s.variables = {'f','g'};
                
                s.initialize(settings);
            end
        end
        
        function time = setupEquations(s)
            t1 = toc;
            
            s.eqWeights = s.settings.eqWeights;
            
            for e=s.mesh.eRange
                
                % Select the active element
                element = s.mesh.getElement(e);
                
                % Get all elemental operators/arrays
                H       = element.getH;
                Dx      = element.getDx;
                Dy      = element.getDy;
                vecZ    = element.getVecZ;
                               
                % Get the local element number
                el = element.localID;
                                
                s.opL{el} = [ Dx  Dx  ; ...
                              H  -Dy ];

                s.opG{el} = [ vecZ ; ...
                              vecZ ];

                if (s.withForcing)
                    s.opG{el} = s.opG{el} + s.forcing{el};
                end
                
                %% Set quadrature weights, apply all scaling to L and G
                s.setWeightAndScale( element );
            end
            
            t2 = toc;
            time = t2-t1;
        end
        
%         function out = getConvergence(s,fieldType,residual)
%             if nargin==2
%                 switch fieldType
%                     case FieldType.Nonlinear
%                         out = reshape( System.settings.comp.nConvergence, [numel(System.settings.comp.nConvergence),1] );
%                     case FieldType.Coupled
%                         out = reshape( System.settings.comp.cConvergence, [numel(System.settings.comp.cConvergence),1] );
%                 end
%             else
%                 out = reshape( System.settings.comp.rConvergence, [numel(System.settings.comp.rConvergence),1] );
%             end
%         end
        
        function data = save(s)
            % Merge solution from all processors on the master rank
            
            data  = struct;
            
            % First synchronize the fields in parallel
            data.fields = cell(s.Nvar,1);
            for n=1:s.Nvar
                data.fields{n} = save( s.fields{n} );
            end
            
            if (NMPI.instance.rank==0 || System.settings.outp.parallelWrite )
                data.class        = class(s);
                data.name         = s.name;
                data.mesh         = s.mesh.save;
                               
                data.settings     = s.settings;
            end
        end
    end
    
    methods (Access=protected)
        function loadObject(s,solution)
            % load from parent
            loadObject@Poisson(s,solution)
            
            % specific loads for the Poisson object
            % none
        end
    end
    
    methods (Static)
        function out = load(s)
            out = Poisson();
            out.loadObject(s);
        end
    end
    
end

