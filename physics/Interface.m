classdef Interface < CahnHilliard
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function addInterface(s,point,normal)            
            fh = @(x,y,z,t) s.initialInterface(x,y,z,t,point,normal);
            s.cField.add( fh );
            
            fh = @(x,y,z,t) s.initialOmg(x,y,z,t,point,normal);
            s.omgField.add( fh );
        end
    end
    
    methods (Access=protected)
        % Function which defines an interface through a specified point with 
        % a given normal. The x,y,z data are the locations where the C
        % data needs to be evaluated. The center is a vector of length 3,
        % which contains its x,y,z data. On the left of the interface C=1, 
        % and on the right side C=0
        function c = initialInterface(s,x,y,z,t,point,normal)
            if length(point)~=3
                error('The point should have length == 3')
            end
            if length(normal)~=3
                error('The normal vector should have length == 3')
            end       
            
            c = zeros(length(x),length(y),length(z));
            
            denominator = (2*sqrt(2)*s.Cn);

            xp = point(1);
            yp = point(2);
%             zp = center(3);
            
            if ( all( abs(normal)==[1,0,0]) )
                for j=1:length(y)
                    for i=1:length(x)
                        c(i,j,:) = 0.5 + 0.5 * tanh( sign(normal(1))*( xp - x(i) ) / denominator );
                    end
                end
            else
                error('This normal has not been implemented yet')
            end
        end
        
        % Analytical solution for omega, which complies with the solution for
        % the variable c (see above). Omega is defined by:
        % 
        %   omega = 1/eps * (C^3 - 3/2*C^2 + 1/2*C) - eps * laplacian(C)
        %
        function omg = initialOmg(s,x,y,z,t,point,normal)
            if length(point)~=3
                error('The point should have length == 3')
            end
            if length(normal)~=3
                error('The normal vector should have length == 3')
            end   
            
            omg = zeros(length(x),length(y),length(z));
%             omg2 = zeros(length(x),length(y),length(z));
%             
%             xp = point(1);
%             yp = point(2);
            % zp = point(3);

%             denominator = (2*sqrt(2)*s.Cn);

            if ( all( abs(normal)==[1,0,0]) )
                for j=1:length(y)
                    for i=1:length(x)
%                         distance = x-xp;
                        
                        omg(i,j,:) = 0;
                        %0.5*sqrt(2) * sinh( distance./denominator ) ./ ( s.Cn * cosh( distance./denominator )^3);

                        % Only laplacian
                        %omg(i,j,:) = omg(i,j,:) - ( 0.5*sqrt(2) * sinh( distance/denominator ) * r - s.Cn*cosh( distance/denominator ) ) / ...
                        %                                                                       ( r * s.Cn*cosh( distance/denominator )^3 );
                    end
                end
            else
                error('This normal has not been implemented yet')
            end

            omg = omg * 3/2;
        end  
    end
    
end

