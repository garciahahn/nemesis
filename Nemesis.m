function Nemesis(varargin)
%Nemesis - Main function to start any simulation
%
%Nemesis is a multi-physics least-squares (LS) spectral element solver. 
%It can be run in parallel and supports adaptive grid refinement 
%(h-refinement). Time is treated as an additional dimension, i.e. problems
%are solved as space-time (time stepping is not supported yet). 
%The physics can be non-linear, and each time step a coupled problem is
%solved up to both non-linear and coupling has converged.
%
% Syntax:  Nemesis(restartID,settings)
%
% Inputs:
%    restartID - Possible values: 
%                   -1 : only initialize the Nemesis system
%                    0 : start a new simulation from scratch
%                   >0 : restart a simulation from a specific time level
%    settings  - String containing the settings m-file to load (w/o .m)
%
% Example usage: 
%    Nemesis(0,'Settings')   	: Start a new simulation with Settings.m 
%                                 as a source file
%    Nemesis(-1)                : Only initialize the system, do not start
%                                 any simulation
%    Nemesis(-2)                : Used for batch runs
%    Nemesis(99,'Settings_BDF') : Restart a simulation from time level 99 
%                                 (the restart file of this time level
%                                 should be available), use Settings_BDF.m
%                                 as a source file
%
% Author: Marcel Kwakkel
% Work address: Thermal Two-Phase Flow Laboratory, EPT, NTNU, Kolbjørn Hejes vei 2A, NO-7491 Trondheim
% email: marcel.kwakkel@ntnu.no
% Website: https://www.ntnu.edu/employees/marcel.kwakkel
% 2017 - 2019

%------------- BEGIN CODE --------------

    %% To ensure all global variables are reset
    if (System.nproc==1)
        clear global;
        clearvars -except varargin;
    end

    %% Initialization
    % Set the additional library directories, initialize the parallel system, 
    % load settings from Settings.m. Next, the finite elements, meshes and 
    % physics are initialized. The restartID can be a location (wrt the
    % current folder) or just an integer indicating the time step to load
    % (in that case it is assumed that the restart file is location in the
    % folder 'outp.directory' with filename 'outp.solutionFilename').
    
    defaultValues = {0,'Settings'};
    [restartID,settingsFileName] = VerifyInput(varargin,defaultValues);
    System.init(restartID,settingsFileName);
    
    
    %% Perform some pre computation operations
    if (restartID<=0)
        PhysicsCollection.preTimeLoop;
    end
    
    %% Output: write the physics to a Paraview files
    PhysicsCollection.write_vtu;
    
    %% Output number of DOF
    PhysicsCollection.dofInfo;
    
    %% Output some values of interest for all physics
    PhysicsCollection.writeValues;
        
    %% Perform time stepping
    for tt = System.settings.time.nStart : System.settings.time.nEnd
        t1 = toc;
                
     	% Advance time globally
        System.advanceTime(tt);

        % Solve the time step (iterates over all physics until coupling convergence has been reached)
        PhysicsCollection.solveTimeStep(); 
        
        % Output info on this time step to the screen
        if (System.rank==0)
          time_step_wall_clock_time = toc - t1;
          System.setTimeStepWallClockTime( time_step_wall_clock_time );
          fprintf('Done! Wall clock time of this time step: %f\n', time_step_wall_clock_time )
        end
        System.newLine();

        % If requested, an energy balance table is created
        if (System.settings.comp.energyBalance)
            energyBalance = integral.energyBalance(Physics);
            System.saveEnergy( energyBalance );
        end
                
        % Output a restart file, the Paraview data files, and the txt output files
        System.writeRestart;
        PhysicsCollection.write_vtu;
        PhysicsCollection.writeValues;

        if (System.rank==0 && System.settings.outp.debug)
            disp('physics written') 
        end
    
        % Postprocessing functions (error calculation, energy balance, etc.)
        activeSettings = System.settings;
        breakLoop = activeSettings.postTimeStep(activeSettings);
        
        if (breakLoop)
            break;
        end
        
        System.printMemory;
        System.writeValues;
        
    end  % end of time step loop

    %% Deinitialization / termination of the program
    % Switch off diary output
    if (System.rank==0)
        diary off;
    end
    
    if (System.rank==0 && System.settings.anal.profiling)
        profile off;
        % Path where you want to store the HTML profiler results
        profsave(profile('info'), System.settings.outp.directory)
    end

    if (System.rank==0)
        time = toc;
        fprintf('Success! Total wall clock time : %5.2e seconds\n',time)
    end

    if (System.nproc>1)
        exit;
    end
    try
        [ret, name] = system('hostname');
        sendmail('garciahahn@gmail.com', 'e-mail from matlab', ['Simulation running in ', pwd, ' in computer ', name, ' finished!']);
    end
end
%------------- END OF CODE --------------