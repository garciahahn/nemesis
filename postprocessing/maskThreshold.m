function out = maskThreshold(field, condition)
  out = cell(numel(field.mesh.eRange), 1);
  for e = field.mesh.eRange
    out{e} = condition(field.getValue(e));
  end % for
end % maskThreshold