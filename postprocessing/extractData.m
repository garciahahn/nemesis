function extractData(number, name)
    % EXTRACTDATA Extract data from a Nemesis simulation
    %   extractData(number, name)
    %
    %     To use the data extraction follow these steps:
    %
    %     Step 1: Move to the folder where the "Settings.m" file of the simulation is.
    %  
    %     Step 2: Call the 'extractData' function with the number of the timestep file (for
    %     example: 1000.m) and a 'name' (string) that will be used to name the comma
    %     separated value (.csv) file where the data will be stored.
    System.createAnalysisFramework('Settings', number);

    fe_info = FECollection.get(1);
    spatial_dofs = fe_info.numSpatialDofs;

    R = FieldCollection.get('R');
    u = FieldCollection.get('u');
    v = FieldCollection.get('v');
    q = FieldCollection.get('q');
    T = FieldCollection.get('T');

    mesh = MeshCollection.get(1);
    num_el = numel(mesh.elements);
    
    X_data = zeros(num_el * spatial_dofs, 1);
    Y_data = zeros(size(X_data));
    time_data = zeros(size(X_data));

    R_data = zeros(size(X_data));
    Rx_data = zeros(size(X_data));
    Ry_data = zeros(size(X_data));
    Rxx_data = zeros(size(X_data));
    Ryy_data = zeros(size(X_data));
    Rxy_data = zeros(size(X_data));
    Ryx_data = zeros(size(X_data));
    Rt_data = zeros(size(X_data));
    Rxt_data = zeros(size(X_data));
    Ryt_data = zeros(size(X_data));
    u_data = zeros(size(X_data));
    ux_data = zeros(size(X_data));
    uy_data = zeros(size(X_data));
    uxx_data = zeros(size(X_data));
    uyy_data = zeros(size(X_data));
    uxy_data = zeros(size(X_data));
    uyx_data = zeros(size(X_data));
    ut_data = zeros(size(X_data));
    uxt_data = zeros(size(X_data));
    uyt_data = zeros(size(X_data));
    v_data = zeros(size(X_data));
    vx_data = zeros(size(X_data));
    vy_data = zeros(size(X_data));
    vxx_data = zeros(size(X_data));
    vyy_data = zeros(size(X_data));
    vxy_data = zeros(size(X_data));
    vyx_data = zeros(size(X_data));
    vt_data = zeros(size(X_data));
    vxt_data = zeros(size(X_data));
    vyt_data = zeros(size(X_data));
    q_data = zeros(size(X_data));
    qx_data = zeros(size(X_data));
    qy_data = zeros(size(X_data));
    qxx_data = zeros(size(X_data));
    qyy_data = zeros(size(X_data));
    qxy_data = zeros(size(X_data));
    qyx_data = zeros(size(X_data));
    qt_data = zeros(size(X_data));
    qxt_data = zeros(size(X_data));
    qyt_data = zeros(size(X_data));
    T_data = zeros(size(X_data));
    Tx_data = zeros(size(X_data));
    Ty_data = zeros(size(X_data));
    Txx_data = zeros(size(X_data));
    Tyy_data = zeros(size(X_data));
    Txy_data = zeros(size(X_data));
    Tyx_data = zeros(size(X_data));
    Tt_data = zeros(size(X_data));
    Txt_data = zeros(size(X_data));
    Tyt_data = zeros(size(X_data));
    list_of_el = 1;
    fields = [R, u, v, q, T];
    for e = mesh.eRange
        el = mesh.getElement(e);

        [X, Y, ~, time] = el.getNodes();
        R_temp = reshape(R.getValue(el.id, Derivative.value), size(X));
        Rx_temp = reshape(R.getValue(el.id, Derivative.x), size(X));
        Ry_temp = reshape(R.getValue(el.id, Derivative.y), size(X));
        Rt_temp = reshape(R.getValue(el.id, Derivative.t), size(X));
        Rxx_temp = reshape(R.getValue(el.id, Derivative.xx), size(X));
        Ryy_temp = reshape(R.getValue(el.id, Derivative.yy), size(X));
        Rxy_temp = reshape(R.getValue(el.id, Derivative.xy), size(X));
        Ryx_temp = reshape(R.getValue(el.id, Derivative.yx), size(X));
        Rxt_temp = reshape(R.getValue(el.id, Derivative.xt), size(X));
        Ryt_temp = reshape(R.getValue(el.id, Derivative.yt), size(X));
        u_temp = reshape(u.getValue(el.id, Derivative.value), size(X));
        ux_temp = reshape(u.getValue(el.id, Derivative.x), size(X));
        uy_temp = reshape(u.getValue(el.id, Derivative.y), size(X));
        ut_temp = reshape(u.getValue(el.id, Derivative.t), size(X));
        uxx_temp = reshape(u.getValue(el.id, Derivative.xx), size(X));
        uyy_temp = reshape(u.getValue(el.id, Derivative.yy), size(X));
        uxy_temp = reshape(u.getValue(el.id, Derivative.xy), size(X));
        uyx_temp = reshape(u.getValue(el.id, Derivative.yx), size(X));
        uxt_temp = reshape(u.getValue(el.id, Derivative.xt), size(X));
        uyt_temp = reshape(u.getValue(el.id, Derivative.yt), size(X));
        v_temp = reshape(v.getValue(el.id, Derivative.value), size(X));
        vx_temp = reshape(v.getValue(el.id, Derivative.x), size(X));
        vy_temp = reshape(v.getValue(el.id, Derivative.y), size(X));
        vt_temp = reshape(v.getValue(el.id, Derivative.t), size(X));
        vxx_temp = reshape(v.getValue(el.id, Derivative.xx), size(X));
        vyy_temp = reshape(v.getValue(el.id, Derivative.yy), size(X));
        vxy_temp = reshape(v.getValue(el.id, Derivative.xy), size(X));
        vyx_temp = reshape(v.getValue(el.id, Derivative.yx), size(X));
        vxt_temp = reshape(v.getValue(el.id, Derivative.xt), size(X));
        vyt_temp = reshape(v.getValue(el.id, Derivative.yt), size(X));
        q_temp = reshape(q.getValue(el.id, Derivative.value), size(X));
        qx_temp = reshape(q.getValue(el.id, Derivative.x), size(X));
        qy_temp = reshape(q.getValue(el.id, Derivative.y), size(X));
        qt_temp = reshape(q.getValue(el.id, Derivative.t), size(X));
        qxx_temp = reshape(q.getValue(el.id, Derivative.xx), size(X));
        qyy_temp = reshape(q.getValue(el.id, Derivative.yy), size(X));
        qxy_temp = reshape(q.getValue(el.id, Derivative.xy), size(X));
        qyx_temp = reshape(q.getValue(el.id, Derivative.yx), size(X));
        qxt_temp = reshape(q.getValue(el.id, Derivative.xt), size(X));
        qyt_temp = reshape(q.getValue(el.id, Derivative.yt), size(X));
        T_temp = reshape(T.getValue(el.id, Derivative.value), size(X));
        Tx_temp = reshape(T.getValue(el.id, Derivative.x), size(X));
        Ty_temp = reshape(T.getValue(el.id, Derivative.y), size(X));
        Tt_temp = reshape(T.getValue(el.id, Derivative.t), size(X));
        Txx_temp = reshape(T.getValue(el.id, Derivative.xx), size(X));
        Tyy_temp = reshape(T.getValue(el.id, Derivative.yy), size(X));
        Txy_temp = reshape(T.getValue(el.id, Derivative.xy), size(X));
        Tyx_temp = reshape(T.getValue(el.id, Derivative.yx), size(X));
        Txt_temp = reshape(T.getValue(el.id, Derivative.xt), size(X));
        Tyt_temp = reshape(T.getValue(el.id, Derivative.yt), size(X));
        

        %u_temp = reshape(u.getValue(Derivative.value), size(X));
        R_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(R_temp(:, :, 1, end));
        Rx_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(Rx_temp(:, :, 1, end));
        Ry_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(Ry_temp(:, :, 1, end));
        Rt_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(Rt_temp(:, :, 1, end));
        Rxx_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(Rxx_temp(:, :, 1, end));
        Ryy_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(Ryy_temp(:, :, 1, end));
        Rxy_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(Rxy_temp(:, :, 1, end));
        Ryx_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(Ryx_temp(:, :, 1, end));
        Rxt_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(Rxt_temp(:, :, 1, end));
        Ryt_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(Ryt_temp(:, :, 1, end));
        u_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(u_temp(:, :, 1, end));
        ux_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(ux_temp(:, :, 1, end));
        uy_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(uy_temp(:, :, 1, end));
        ut_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(ut_temp(:, :, 1, end));
        uxx_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(uxx_temp(:, :, 1, end));
        uyy_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(uyy_temp(:, :, 1, end));
        uxy_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(uxy_temp(:, :, 1, end));
        uyx_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(uyx_temp(:, :, 1, end));
        uxt_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(uxt_temp(:, :, 1, end));
        uyt_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(uyt_temp(:, :, 1, end));
        v_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(v_temp(:, :, 1, end));
        vx_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(vx_temp(:, :, 1, end));
        vy_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(vy_temp(:, :, 1, end));
        vt_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(vt_temp(:, :, 1, end));
        vxx_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(vxx_temp(:, :, 1, end));
        vyy_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(vyy_temp(:, :, 1, end));
        vxy_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(vxy_temp(:, :, 1, end));
        vyx_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(vyx_temp(:, :, 1, end));
        vxt_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(vxt_temp(:, :, 1, end));
        vyt_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(vyt_temp(:, :, 1, end));
        q_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(q_temp(:, :, 1, end));
        qx_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(qx_temp(:, :, 1, end));
        qy_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(qy_temp(:, :, 1, end));
        qt_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(qt_temp(:, :, 1, end));
        qxx_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(qxx_temp(:, :, 1, end));
        qyy_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(qyy_temp(:, :, 1, end));
        qxy_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(qxy_temp(:, :, 1, end));
        qyx_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(qyx_temp(:, :, 1, end));
        qxt_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(qxt_temp(:, :, 1, end));
        qyt_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(qyt_temp(:, :, 1, end));
        T_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(T_temp(:, :, 1, end));
        Tx_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(Tx_temp(:, :, 1, end));
        Ty_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(Ty_temp(:, :, 1, end));
        Tt_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(Tt_temp(:, :, 1, end));
        Txx_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(Txx_temp(:, :, 1, end));
        Tyy_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(Tyy_temp(:, :, 1, end));
        Txy_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(Txy_temp(:, :, 1, end));
        Tyx_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(Tyx_temp(:, :, 1, end));
        Txt_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(Txt_temp(:, :, 1, end));
        Tyt_data(list_of_el:list_of_el+spatial_dofs-1) = squeeze(Tyt_temp(:, :, 1, end));

        X = squeeze(X(:, :, 1, end)); Y = squeeze(Y(:, :, 1, end));
        time = squeeze(time(:, :, 1, end));

        

        X_data(list_of_el:list_of_el+spatial_dofs-1) = X(:);
        Y_data(list_of_el:list_of_el+spatial_dofs-1) = Y(:);
        time_data(list_of_el:list_of_el+spatial_dofs-1) = time(:);
        list_of_el = list_of_el + spatial_dofs;
    end % Loop for extracting all the data
    
    var_names = {'x', 'y', 'time', 'R', 'Rx', 'Ry', 'Rt', 'Rxx', 'Ryy', 'Rxy', 'Ryx', 'Rxt', 'Ryt',...
        'u', 'ux', 'uy', 'ut', 'uxx', 'uyy', 'uxy', 'uyx', 'uxt', 'uyt',...
        'v', 'vx', 'vy', 'vt', 'vxx', 'vyy', 'vxy', 'vyx', 'vxt', 'vyt',...
        'q', 'qx', 'qy', 'qt', 'qxx', 'qyy', 'qxy', 'qyx', 'qxt', 'qyt',...
        'T', 'Tx', 'Ty', 'Tt', 'Txx', 'Tyy', 'Txy', 'Tyx', 'Txt', 'Tyt'};
    table = array2table([X_data, Y_data, time_data, R_data, Rx_data, Ry_data, Rt_data, Rxx_data, Ryy_data, Rxy_data, Ryx_data, Rxt_data, Ryt_data,...
         u_data, ux_data, uy_data, ut_data, uxx_data, uyy_data, uxy_data, uyx_data, uxt_data, uyt_data,...
         v_data, vx_data, vy_data, vt_data, vxx_data, vyy_data, vxy_data, vyx_data, vxt_data, vyt_data,...
         q_data, qx_data, qy_data, qt_data, qxx_data, qyy_data, qxy_data, qyx_data, qxt_data, qyt_data,...
         T_data, Tx_data, Ty_data, Tt_data, Txx_data, Tyy_data, Txy_data, Tyx_data, Txt_data, Tyt_data],...
        'VariableNames', var_names);

    writetable(table, strcat(name, '.csv'));
end