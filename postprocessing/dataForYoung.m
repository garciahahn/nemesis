function [p_i, p_o, radius, timestep] = dataForYoung(input)
% #########################################################################
%                                WARNING:
% #########################################################################
% This function serves a very specific purpose of separing a two-phase flow
% of Navier-Stokes-Korteweg nature into liquid and vapor phases and takes
% SPECIFIC data from this fields. It is inteded for collecting data for a
% SPECIFIC report. It can't be guaranteed to be used on more general cases
% even though some effort was put into not making it more hardcoded than
% necessary.
% #########################################################################
if ~ischar(input)
  ME = MException('dataForYoung:IncorrectInputType', 'Input is not a proper folder''s name (string)');
  throw(ME);
end % if
listing = dir(['./', input]);
number = -1;
logi = cell(numel(listing), 1);
[logi{:}] = listing.name;
logi = cellfun(@(x) split(x, '.'), logi, 'UniformOutput', false);
logi = cellfun(@(x) strcmp(x{1}, 'restart'), logi, 'UniformOutput', false);
logi = cell2mat(logi);
logi = find(logi);
for i = 1:numel(find(logi))
  l = listing(logi(i));
  n = split(l.name, '.');
  n = str2double(n{2});
  if number < n
    number = n;
  end % if
end %for

System.createAnalysisFramework('Settings', number);
mesh = MeshCollection.get(1);
R = FieldCollection.get('R');
T = FieldCollection.get('T');
rho_l= findNearestQuadValue(R, 0.5, 0.5);
temp_center = findNearestQuadValue(T, 0.5, 0.5);
rho_v = findNearestQuadValue(R, 0.01, 0.01);
temp_outside = findNearestQuadValue(T, 0.01, 0.01);
cond = @(val) condition(val, mean([rho_l, rho_v]));

mask = maskThreshold(R, cond);
x_nodes = [];
y_nodes = [];
for e = mesh.eRange
    element = mesh.getElement(e);
    mask{e} = reshape(mask{e}, element.finite.qSize);
    [X, Y, Z, T] = mesh.elements(e).getNodes;
    X = squeeze(X); Y = squeeze(Y);
    Z = squeeze(Z); T = squeeze(T);
    X = X(:, :, end);
    Y = Y(:, :, end);
    idx = find(mask{e}(:, :, end));
    x_nodes = [x_nodes; X(idx)];
    y_nodes = [y_nodes; Y(idx)];
end % for

c = smallestCircle([x_nodes, y_nodes]);

p_i = pressure(rho_l, temp_center);
p_o = pressure(rho_v, temp_outside);
radius = c(3);
timestep = number;
save('young', 'p_i', 'p_o', 'radius', 'timestep');

end % function dataForYoung

function out = condition(val, threshold)
  out = val > threshold;
end

function out = pressure(rho, temp)
  out = ((8 * rho * temp)/(3 - rho)) - 3 * rho^2;
end % function pressure