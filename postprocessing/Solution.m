classdef Solution < handle %< NavierStokes
    
    properties (Access=private)
        baseFile;
        
%         NS;
%         CH;
        
        numElements;
        GM;
                
        interpolQ;
        
        mesh;
        standardElement;
    end
    
    properties
        filename;
        status;
        data;
        
        energy;
        y0;
        
        NS;
        CH;
    end
    
    methods 
        function s = Solution(baseFile,Q)
            s.baseFile  = baseFile;
            if (nargin==2)
                s.interpolQ = Q;
            end
        end
        
        %% Load functions
        % Function to load the solution of iteration n
        function    load(s,n)
            loadFromFile = false;
            filename = [];
                
            verbose = false;
            
            System.setVerbose( verbose );
            System.loadLibraries; 
            System.startParallel();
            System.loadSettings(loadFromFile,filename);
            System.createIntegrationDisc(9);
            
            s.loadData(n);
            
            %fields = fieldnames(s.data);
            
            if (s.status)
                s.loadStandardElement;
                s.loadMesh;
                s.loadNS;
                s.loadCH;
            end
            
            Physics         = cell(numel(System.settings.phys.classes),1);  

            Physics{1}      = s.getCH;
            Physics{2}      = s.getNS;
            
            Physics{1}.init(Physics);
            Physics{2}.init(Physics);
        end
        
        function loadData(s,n)
            if nargin==2
                s.filename = sprintf('%s.%.7d.00.mat',s.baseFile,n);
                if (exist(s.filename,'file')==2)
                    s.data      = load(s.filename);
                    s.status    = true;
                    
                    if (Parallel3D.MPI.rank==0)
                        fprintf('Loading from %s\n',s.filename)
                    end
                else
                    fprintf('%s does not exist\n',s.filename)
                    s.status    = false;
                end                
            end
        end
        
        function loadStandardElement(s)
            s.checkData;
            s.standardElement = LSQdisc_5.load(s.data.standardElement,s.interpolQ);%  (s.data.Pn,s.interpolQ,s.data.C,s.data.SDIM,s.data.spaceTime);
            if (System.getVerbose && Parallel3D.MPI.rank==0)
                fprintf('StandardElement has been set with Q=%d\n',s.standardElement.Q(1))
            end
        end
        
        function loadMesh(s)
            s.checkMesh;
            s.mesh = MeshRefineMultiD.load(s.getStandardElement,s.data.mesh);
            if (System.getVerbose && Parallel3D.MPI.rank==0)
                disp('Mesh has been set')
            end
        end
        
        function loadNS(s)
            s.checkNS;
            s.NS = eval([s.data.NS.class '.load(s)']);
            if (System.getVerbose && Parallel3D.MPI.rank==0)
                disp('NS has been set')
            end
        end
        
        function loadCH(s)
            s.checkCH;
            s.CH = eval([s.data.CH.class '.load(s)']);
            if (System.getVerbose && Parallel3D.MPI.rank==0)
                disp('CH has been set')
            end
        end
        
        %% Plot functions
        function plotMesh(s)
            s.mesh.plot;
        end
        
        function plotNS(s,fig)
            if (nargin==1)
                fig = figure;
            end
            s.NS.plot(fig);
        end
        
        function plotCH(s,fig)
            if (nargin==1)
                fig = figure;
            end
            s.CH.createInterface;
            s.CH.plot(fig);
        end
        
        function plotST(s)
            s.CH.plotSurfaceTension(figure);
        end
        
        function computeST(s)
            s.CH.computeSurfaceTension();
        end
        
        function computeMass(s)
            s.CH.computeMass();
        end
        
        function computeCenterOfMass(s)
            s.CH.computeCenterOfMass();
        end
        
        function resetEnergy(s)
            s.energy = [];
        end
        
        function computeEnergy(s,timeLevel)
            if nargin==1
                timeLevel = System.settings.time.current;
            end
            
            if (isempty(s.energy) || timeLevel == 0)
                timelevel           = timeLevel;
                surfaceEnergy       = s.computeSurfaceEnergy;
                [totalKineticEnergy,transKineticEnergy] = s.computeKineticEnergy;
                pressureEnergy      = s.computePressureEnergy;
                [gravitationalEnergy,cmy] = s.computeGravitationalEnergy(1);
                [dis,div] = s.computeDissipationEnergy;  
                dissipationEnergy   = dis;
                divergenceEnergy    = div;
                totalEnergy         = surfaceEnergy+totalKineticEnergy+gravitationalEnergy+dissipationEnergy;
                s.energy = table(timelevel,surfaceEnergy,pressureEnergy,totalKineticEnergy,transKineticEnergy,cmy,gravitationalEnergy,dissipationEnergy,divergenceEnergy,totalEnergy);
            else
                surfaceEnergy       = s.computeSurfaceEnergy;
                [totalKineticEnergy,transKineticEnergy] = s.computeKineticEnergy;
                pressureEnergy      = s.computePressureEnergy;
                [gravitationalEnergy,cmy] = s.computeGravitationalEnergy;
                [dis,div] = s.computeDissipationEnergy;                
                dissipationEnergy   = s.energy.dissipationEnergy(end) + dis;
                divergenceEnergy    = s.energy.divergenceEnergy(end) + div;
                totalEnergy         = surfaceEnergy+totalKineticEnergy+gravitationalEnergy+dissipationEnergy;
                s.energy = [s.energy; {timeLevel,surfaceEnergy,pressureEnergy,totalKineticEnergy,transKineticEnergy,cmy,gravitationalEnergy,dissipationEnergy,divergenceEnergy,totalEnergy}];
            end
        end
        
        function out = computeSurfaceEnergy(s)
            out = s.CH.computeSurfaceEnergy();
        end
        
        % The pressure energy is defined by the integration of the pressure
        % over the full domain. Initially the pressure is zero everywhere,
        % so any change from zero will contribute to the potential.
        function [out1] = computePressureEnergy(s)

            % Set the Fields
            C   = s.CH.getField(1,s.NS);
            P   = s.NS.getField(3,s.NS);
            
            % Get the mesh (should be same for both fields!)
            mesh = P.mesh;
            
            numElements = mesh.numLocalElements;
           
            % Initialize the pressure for all elements
            pressure = cell(numElements,1);
             
            % After loading a solution the field also use their own meshes
            % for coupled field. By selecting the coupled field (and
            % passing the coupled mesh), it is ensured that both fields
            % provide the same elements.
            %C.selectCoupled(mesh);
            
            for el=1:numElements
                e = mesh.getElement(el).globalID;
                
                Pk          = P.val(e);
                Ck          = C.val(e);
                
                pressure{e} = (Pk); %.* (1-Ck);
            end

            totalPressureEnergy = 0;
            
            for e=1:numElements
                data1 = pressure{e};
                
                element = mesh.getLocalElement(e);
                wxy = kron(element.disc.wy,element.disc.wx);
                totalPressureEnergy = totalPressureEnergy + data1((element.disc.Qz-1)*(element.disc.Qx*element.disc.Qy)+1:element.disc.Qz*(element.disc.Qx*element.disc.Qy))' * wxy / (element.J(1)*element.J(2)) ;
            end

            if (System.settings.anal.debug)
                fprintf('Before NMPI_Allreduce in Solution.computePressureEnergy\n')
            end
            
            totalPressureEnergy = NMPI_Allreduce(totalPressureEnergy,1,'+',Parallel3D.MPI.mpi_comm_world);
%             if (Parallel3D.MPI.rank==0)
%                 fprintf('Total kinetic energy  : %f\n',totalPressureEnergy)
%             end
            
            out1 = totalPressureEnergy;
        end
        
        % The total kinetic energy is defined by the integration of 
        % 0.5*rho*U^2 over the full domain. Here rho is a function of C,
        % so both fluid as interface solutions are required.
        function [out1,out2] = computeKineticEnergy(s)

            % Set the Fields
            C   = s.CH.getField(1,s.NS);
            U   = s.NS.getField(1,s.NS);
            V   = s.NS.getField(2,s.NS);
            
            % Get the mesh (should be same for both fields!)
            mesh = C.mesh;
            
            numElements = mesh.numLocalElements;
           
            % Initialize rho and the squared velocity for all elements
            rho   	= cell(numElements,1);
            absVel2	= cell(numElements,1);
            absV2	= cell(numElements,1);
           
            for el=1:numElements
                e = mesh.getElement(el).globalID;
                
                Ck          = C.val(e);
                Uk          = U.val(e);
                Vk          = V.val(e);
                
                rho{e}      = Ck + (1-Ck) .* System.settings.phys.lamRho;
                absVel2{e}  = Uk.^2+Vk.^2;
                absV2{e}    = Vk.^2;
            end

            totalKineticEnergy = 0;
            transKineticEnergy = 0;
            
            for e=1:numElements
                data1 = 0.5 .* rho{e} .* absVel2{e};
                data2 = 0.5 .* rho{e} .* absV2{e};
                
                element = mesh.getLocalElement(e);
                wxy = kron(element.disc.wy,element.disc.wx);
                totalKineticEnergy = totalKineticEnergy + data1((element.disc.Qz-1)*(element.disc.Qx*element.disc.Qy)+1:element.disc.Qz*(element.disc.Qx*element.disc.Qy))' * wxy / (element.J(1)*element.J(2)) ;
                transKineticEnergy = transKineticEnergy + data2((element.disc.Qz-1)*(element.disc.Qx*element.disc.Qy)+1:element.disc.Qz*(element.disc.Qx*element.disc.Qy))' * wxy / (element.J(1)*element.J(2)) ;
            end

            if (System.settings.anal.debug)
                fprintf('Before NMPI_Allreduce in Solution.computeKineticEnergy\n')
            end
            
%             fprintf('Max of surface tension : %f\n',stMax)
            totalKineticEnergy = NMPI_Allreduce(totalKineticEnergy,1,'+',Parallel3D.MPI.mpi_comm_world);
            transKineticEnergy = NMPI_Allreduce(transKineticEnergy,1,'+',Parallel3D.MPI.mpi_comm_world);
%             if (Parallel3D.MPI.rank==0)
%                 fprintf('Total kinetic energy  : %f\n',totalKineticEnergy)
%                 fprintf('Trans kinetic energy  : %f\n',transKineticEnergy)
%             end
            
            out1 = totalKineticEnergy;
            out2 = transKineticEnergy;
        end
        
        function [out1,out2] = computeGravitationalEnergy(s,tag)
            s.CH.computeCenterOfMass();
            if nargin==2
                s.y0 = s.CH.cm(1,2);
                out1 = 0;
                out2 = s.CH.cm(1,2);
            else
                out1 = System.settings.phys.Bo * ( s.CH.cm(end,2) - s.y0 );
                out2 = s.CH.cm(end,2);
            end
        end
        
        function [out1,out2] = computeDissipationEnergy(s)

            % Set the Fields
            C   = s.CH.getField(1,s.NS);
            U   = s.NS.getField(1,s.NS);
            V   = s.NS.getField(2,s.NS);
            
            % Get the mesh (should be same for both fields!)
            mesh = C.mesh;
            
            numElements = mesh.numLocalElements;
           
            % Initialize rho and the squared velocity for all elements
            mu    	= cell(numElements,1);
            Phi     = cell(numElements,1);
            divVel  = cell(numElements,1);
           
            for el=1:numElements
                e = mesh.getElement(el).globalID;
                
                Ck          = C.val(e);
                dUkdx       = U.x(e);
                dVkdx       = V.x(e);
                dUkdy       = U.y(e);
                dVkdy       = V.y(e);
                
                mu{e}       = Ck + (1-Ck) .* System.settings.phys.lamMu;
                Phi{e}      = 2*dUkdx.^2 + 2*dVkdy.^2 + (dVkdx+dUkdy).^2;
                divVel{e}   = ( dUkdx + dVkdy ).^2;
            end

            dissipationEnergy = 0;
            divergenceEnergy = 0;
            
            for e=1:numElements
                data1 = mu{e} .* System.settings.phys.Oh .* Phi{e};
                data2 = -2/3 * mu{e} .* System.settings.phys.Oh .* divVel{e};
                
                element = mesh.getLocalElement(e);
                wxyz    = superkron(element.disc.wz,element.disc.wy,element.disc.wx);
                dissipationEnergy = dissipationEnergy + data1(:)' * wxyz / (element.J(1)*element.J(2)*element.J(3)) ;
                divergenceEnergy  = divergenceEnergy  + data2(:)' * wxyz / (element.J(1)*element.J(2)*element.J(3)) ;
            end

            if (System.settings.anal.debug)
                fprintf('Before NMPI_Allreduce in Solution.computeKineticEnergy\n')
            end
            
            dissipationEnergy = NMPI_Allreduce(dissipationEnergy,1,'+',Parallel3D.MPI.mpi_comm_world);
            divergenceEnergy  = NMPI_Allreduce(divergenceEnergy ,1,'+',Parallel3D.MPI.mpi_comm_world);
%             if (Parallel3D.MPI.rank==0)
%                 fprintf('Dissipation energy  : %f\n',dissipationEnergy)
%             end
            
            out1 = dissipationEnergy;
            out2 = divergenceEnergy;
        end
        
        function energies(s,nStart,nEnd)
                        
            loadFromFile = false;
            filename = [];
                
            verbose = false;
            
            System.setVerbose( verbose );
            System.loadLibraries; 
            System.startParallel();
            System.loadSettings(loadFromFile,filename);
            System.createIntegrationDisc(9);
            
            if nStart~=0
               s.resetEnergy;
            end
            
            
            for n=nStart:1:nEnd
                t1 = toc;
                
                s.loadData(n);
                t2 = toc;
                
                %fprintf('data loaded\n')
                
                s.loadStandardElement;                
                
                %fprintf('stde loaded\n')
                
                t3 = toc;
                
                s.loadMesh;
                
                %fprintf('mesh loaded\n')
                
                %if System.rank==0
                %    fprintf('number of proc : %d\n',System.nproc)
                %end
                
                t4 = toc;
                s.loadNS;
                
                %fprintf('NS loaded\n')
                
                t5 = toc;
                s.loadCH;
                
                t6 = toc;
                s.computeEnergy(n);
                
                t7 = toc;
                
                fprintf('time = %f %f %f %f %f %f\n',t2-t1,t3-t2,t4-t3,t5-t4,t6-t5,t7-t6);
            end
            
            s.energy;
            
            writetable(s.energy,['energyBalance-' num2str(nStart) '-' num2str(nEnd) '.txt'],'Delimiter','tab');
        end
        
        function plotEnergy(s,filename,angle)
            figure(33);
            clf;
            hold on;
            
            if nargin>2
                s.energy = readtable(filename);
%                 values   = readtable('./jump-3-results-N008x012-P04x04x04-0g-Cn0.03-mob2-1-180-kappa1/values.txt');
                values   = readtable('./jump-3-results-N008x012-P04x04x04-0g-Cn0.01-mob1-1/values.txt');
            end
            
            time = s.energy.timelevel*0.01;
            
            if (angle==180)
                % theta=180
                zeroEnergy      = 6.283185307;
                totalSurface    = 8.885765876;
            elseif (angle==150)
                % theta=150
                zeroEnergy      = 5.235987756;
                totalSurface    = 8.756720782;
            end
            
            cmEnergy = 0.5*pi*(values.cmvx.^2+values.cmvy.^2);
            
            pFactor = 100;
                        
            plot(time,zeroEnergy*ones(size(s.energy.gravitationalEnergy)),'LineStyle','--','Color',[0.5 0.5 0.0]);
            plot(time,(totalSurface/2)*ones(size(s.energy.gravitationalEnergy)),'LineStyle',':','Color',[0.5 0.0 0.5]);
            
            plot(time,s.energy.totalEnergy+0*(pi+pFactor*s.energy.pressureEnergy),'-k');
            plot(time,s.energy.surfaceEnergy,'--r');
            plot(time,s.energy.totalKineticEnergy,':b');
            plot(time,s.energy.dissipationEnergy,'-.g');
            plot(time,s.energy.gravitationalEnergy,'-..c');
            plot(time,pi+pFactor*s.energy.pressureEnergy,'--k');
            
            plot(time,((zeroEnergy)*ones(size(s.energy.gravitationalEnergy))-s.energy.totalEnergy-pFactor*s.energy.pressureEnergy),'-..m');
            plot(time,(s.energy.dissipationEnergy+(zeroEnergy)*ones(size(s.energy.gravitationalEnergy))-s.energy.totalEnergy-pFactor*s.energy.pressureEnergy),'-m');
            
            plot(values.time,cmEnergy,'-c');
            
            plot(time,(3-3/2*sqrt(2))*pi*ones(size(s.energy.gravitationalEnergy)),'LineStyle','-.','Color',[0.5 0.0 0.5]);
            plot(time,3*pi*ones(size(s.energy.gravitationalEnergy)),'LineStyle','--','Color',[0.5 0.0 0.5]);
            plot(time,pi*ones(size(s.energy.gravitationalEnergy)),'LineStyle','--','Color',[0.5 0.0 0.5]);
            plot(time,(1/sqrt(2))*pi*ones(size(s.energy.gravitationalEnergy)),'LineStyle',':','Color',[0.5 0.0 0.5]);
            plot(time,(3/2*sqrt(2))*pi*ones(size(s.energy.gravitationalEnergy)),'LineStyle',':','Color',[0.5 0.0 0.5]);
            
            ylim([-1,10]);
            
            grid on;
            
            title({['energy balance ' num2str(angle) ' degrees' ]});
            legend( 'initial surface energy','final surface energy','total energy', 'surface energy', 'total kinetic energy', 'viscous dissipation', 'gravitational energy','pressure energy','unaccounted', 'Location', 'East');
            
%             figure(34)
%             clf
%             hold on;
%             plot(time,s.energy.totalKineticEnergy,'-r');
%             plot(time,s.energy.transKineticEnergy,'-b');            
%             
%             grid on;
%             title({['Kinetic energy : ' num2str(angle) ' degrees' ]});
%             legend( 'total kinetic energy', 'trans kinetic energy', 'Location', 'SouthEast');
        end
        
        function animate(s,nStart,nEnd,nStep,half)
            
            if nargin==3
                nStep = 1;
                half = false;
            end
            
            if nargin==4
                half = false;
            end
            
            loadFromFile = false;
            filename = [];
                
            verbose = false;
            
            System.setVerbose( verbose );
            System.loadLibraries; 
            System.startParallel();
            System.loadSettings(loadFromFile,filename);
            System.createIntegrationDisc(9);
            
            fig = figure(1);
            
            screensize = get( groot, 'Screensize' );
            sizex = 1600;
            sizey = 900;
            
            %sizex = 1600*3/4;
            %sizey = 900*3/4;
            
            set(fig,'position',[0.5*(screensize(3)-sizex) 0.5*(screensize(4)-sizey) sizex sizey]); 
            
            v = VideoWriter('video');
            open(v);

            for n=nStart:nStep:nEnd
                t1=toc;
                s.loadData(n);

%                 fprintf('loading data       : %f\n',toc-t1); t1=toc;
                
                %fields = fieldnames(s.data);

                s.loadStandardElement;
                
%                 fprintf('loading stde       : %f\n',toc-t1); t1=toc;
                
                s.loadMesh;
                s.loadNS;
%                 fprintf('loading mesh       : %f\n',toc-t1); t1=toc;
                
                s.loadCH;
                
%                 fprintf('loading physics    : %f\n',toc-t1); t1=toc;
                
                tag = ['time = ' num2str(System.settings.time.step*s.data.iter,'%5.2f')];

                if (half)
                    s.plotFrameHalf(fig,tag);
                else
                    s.plotFrame(fig,tag);
                end
                
%                 fprintf('plotting           : %f\n',toc-t1); t1=toc;
                
                % Store the frame
                frame=getframe(fig); % leaving gcf out crops the frame in the movie.
                writeVideo(v,frame);
                
                %s.computeEnergy(n);
                            
%                 fprintf('writing video      : %f\n',toc-t1);
            end
            
            %writetable(s.energy);
            
            close(v);
        end
        
        function plotFrame(s,fig,tag)
            
            if nargin<3
                tag = '';
            end
            
            rows    = 2;
            cols    = 3;
            offset  = 0.03;
            
            clf;
            t = annotation('textbox', [1-3.5*offset, 1-1.5*offset, 2.5*offset, 1*offset], 'string', tag, 'EdgeColor',[0 0 0],'LineStyle','-');
            t.FontSize = 12;
            t.FontWeight = 'Bold';            
            
            s.CH.createInterface;
            
            %% C plot
            position    = 1;
            sub         = subplot(rows,cols,position);
            cla(sub);
            s.CH.plotVar(Variable.c);
            title('c')  
            
            %% Omega plot
            position    = 2;
            sub         = subplot(rows,cols,position);
            cla(sub);
            s.CH.plotVar(Variable.omg);
            title('omega')  
            
            %% Surface tension plot
            position    = 3;
            sub         = subplot(rows,cols,position);
            cla(sub);
            s.CH.plotSurfaceTension( sub, s.CH.interface );
            title('surface tension')
            
            %% U velocity
            position    = 4;
            sub         = subplot(rows,cols,position);
            cla(sub);
            s.NS.plotVar(Variable.u,s.CH.interface);
            title('u velocity')
            
            %% V velocity
            position    = 5;
            sub         = subplot(rows,cols,position);
            cla(sub);
            s.NS.plotVar(Variable.v,s.CH.interface);
            title('v velocity')
            
            %% P pressure
            position    = 6;
            sub         = subplot(rows,cols,position);
            cla(sub);
            s.NS.plotVar(Variable.p,s.CH.interface);
            title('pressure')
            
            %% Position subfigures
            d = offset;
            
            width  = (1-(cols+1)*d)/cols;
            height = (1-(rows+1)*d)/rows;
            
            s1 = subplot(rows,cols,1);
            set(s1,'position',[   d         2*d+height width height])
            
            s2 = subplot(rows,cols,2);
            set(s2,'position',[ 2*d+  width 2*d+height width height])
            
            s3 = subplot(rows,cols,3);
            set(s3,'position',[ 3*d+2*width 2*d+height width height])
            
            s4 = subplot(rows,cols,4);
            set(s4,'position',[   d           d        width height])
            
            s5 = subplot(rows,cols,5);
            set(s5,'position',[ 2*d+  width   d        width height])
            
            s6 = subplot(rows,cols,6);
            set(s6,'position',[ 3*d+2*width   d        width height])
        end

        function plotFrameHalf(s,fig,tag)
            
            %sizex = 1600*3/4;
            %sizey = 900*3/4;
            %screensize = get( groot, 'Screensize' );
            
            %if nargin==1
            %    fig = gcf;
            %end
            
            %set(fig,'position',[0.5*(screensize(3)-sizex) 0.5*(screensize(4)-sizey) sizex sizey]); 
            
            if nargin<3
                try
                    tag = ['time = ' num2str(System.settings.time.step * s.data.iter)];
                catch
                    tag = '';
                end
            end
            
            rows    = 2;
            cols    = 3;
            offset  = 0.03;
            
            clf;
            t = annotation('textbox', [1-3.5*offset, 1-1.5*offset, 2.5*offset, 1*offset], 'string', tag, 'EdgeColor',[0 0 0],'LineStyle','-');
            t.FontSize = 12;
            t.FontWeight = 'Bold';            
            
            s.CH.createInterface;
            
            %% C plot
            position    = 1;
            sub         = subplot(rows,cols,position);
            cla(sub);
            s.CH.plotVarHalf(Variable.c);
            title('c')  
            
            %% Omega plot
            position    = 2;
            sub         = subplot(rows,cols,position);
            cla(sub);
            s.CH.plotVarHalf(Variable.omg);
            title('omega')  
            
            %% Surface tension plot
            position    = 3;
            sub         = subplot(rows,cols,position);
            cla(sub);
            s.CH.plotSurfaceTensionHalf( sub, s.CH.interface );
            title('surface tension')
            
            %% U velocity
            position    = 4;
            sub         = subplot(rows,cols,position);
            cla(sub);
            s.NS.plotVarHalf(Variable.u,s.CH.interface);
            title('u velocity')
            
            %% V velocity
            position    = 5;
            sub         = subplot(rows,cols,position);
            cla(sub);
            s.NS.plotVarHalf(Variable.v,s.CH.interface);
            title('v velocity')
            
            %% P pressure
            position    = 6;
            sub         = subplot(rows,cols,position);
            cla(sub);
            s.NS.plotVarHalf(Variable.p,s.CH.interface);
            title('pressure')
            
            %% Position subfigures
            d = offset;
            
            width  = (1-(cols+1)*d)/cols;
            height = (1-(rows+1)*d)/rows;
            
            s1 = subplot(rows,cols,1);
            set(s1,'position',[   d         2*d+height width height])
            
            s2 = subplot(rows,cols,2);
            set(s2,'position',[ 2*d+  width 2*d+height width height])
            
            s3 = subplot(rows,cols,3);
            set(s3,'position',[ 3*d+2*width 2*d+height width height])
            
            s4 = subplot(rows,cols,4);
            set(s4,'position',[   d           d        width height])
            
            s5 = subplot(rows,cols,5);
            set(s5,'position',[ 2*d+  width   d        width height])
            
            s6 = subplot(rows,cols,6);
            set(s6,'position',[ 3*d+2*width   d        width height])
        end

        function animateDerivative(s,nStart,nEnd,nStep,half)
            
            if nargin==3
                nStep = 1;
                half = false;
            end
            
            if nargin==4
                half = false;
            end
            
            loadFromFile = false;
            filename = [];
                
            verbose = false;
            
            System.setVerbose( verbose );
            System.loadLibraries; 
            System.startParallel();
            System.loadSettings(loadFromFile,filename);
            System.createIntegrationDisc(9);
            
            fig = figure(1);
            
            screensize = get( groot, 'Screensize' );
            sizex = 1600;
            sizey = 900;
            
            %sizex = 1600*3/4;
            %sizey = 900*3/4;
            
            set(fig,'position',[0.5*(screensize(3)-sizex) 0.5*(screensize(4)-sizey) sizex sizey]); 
            
            v = VideoWriter('video');
            open(v);

            for n=nStart:nStep:nEnd
                t1=toc;
                s.loadData(n);

%                 fprintf('loading data       : %f\n',toc-t1); t1=toc;
                
                %fields = fieldnames(s.data);

                s.loadStandardElement;
                
%                 fprintf('loading stde       : %f\n',toc-t1); t1=toc;
                
                s.loadMesh;
                s.loadNS;
%                 fprintf('loading mesh       : %f\n',toc-t1); t1=toc;
                
                s.loadCH;
                
%                 fprintf('loading physics    : %f\n',toc-t1); t1=toc;
                
                tag = ['time = ' num2str(System.settings.time.step*s.data.iter,'%5.2f')];

                if (half)
                    s.plotFrameHalf(fig,tag);
                else
                    s.plotFrameDerivative(fig,tag);
                end
                
%                 fprintf('plotting           : %f\n',toc-t1); t1=toc;
                
                % Store the frame
                frame=getframe(fig); % leaving gcf out crops the frame in the movie.
                writeVideo(v,frame);
                
                %s.computeEnergy(n);
                            
%                 fprintf('writing video      : %f\n',toc-t1);
            end
            
            %writetable(s.energy);
            
            close(v);
        end
        
        function plotFrameDerivative(s,fig,tag)
            
            if nargin<3
                tag = '';
            end
            
            rows    = 2;
            cols    = 3;
            offset  = 0.03;
            
            clf;
            t = annotation('textbox', [1-3.5*offset, 1-1.5*offset, 2.5*offset, 1*offset], 'string', tag, 'EdgeColor',[0 0 0],'LineStyle','-');
            t.FontSize = 12;
            t.FontWeight = 'Bold';            
            
            s.CH.createInterface;
            
            %% C plot
            position    = 1;
            sub         = subplot(rows,cols,position);
            cla(sub);
            s.CH.plotVar(Variable.c,[],Derivative.y);
            title('c')  
            
            %% Omega plot
            position    = 2;
            sub         = subplot(rows,cols,position);
            cla(sub);
            s.CH.plotVar(Variable.omg,[],Derivative.y);
            title('omega')  
            
            %% Surface tension plot
            position    = 3;
            sub         = subplot(rows,cols,position);
            cla(sub);
            s.CH.plotSurfaceTension( sub, s.CH.interface );
            title('surface tension')
            
            %% U velocity
            position    = 4;
            sub         = subplot(rows,cols,position);
            cla(sub);
            s.NS.plotVar(Variable.u,s.CH.interface,Derivative.y);
            title('u velocity')
            
            %% V velocity
            position    = 5;
            sub         = subplot(rows,cols,position);
            cla(sub);
            s.NS.plotVar(Variable.v,s.CH.interface,Derivative.y);
            title('v velocity')
            
            %% P pressure
            position    = 6;
            sub         = subplot(rows,cols,position);
            cla(sub);
            s.NS.plotVar(Variable.p,s.CH.interface,Derivative.y);
            title('pressure')
            
            %% Position subfigures
            d = offset;
            
            width  = (1-(cols+1)*d)/cols;
            height = (1-(rows+1)*d)/rows;
            
            s1 = subplot(rows,cols,1);
            set(s1,'position',[   d         2*d+height width height])
            
            s2 = subplot(rows,cols,2);
            set(s2,'position',[ 2*d+  width 2*d+height width height])
            
            s3 = subplot(rows,cols,3);
            set(s3,'position',[ 3*d+2*width 2*d+height width height])
            
            s4 = subplot(rows,cols,4);
            set(s4,'position',[   d           d        width height])
            
            s5 = subplot(rows,cols,5);
            set(s5,'position',[ 2*d+  width   d        width height])
            
            s6 = subplot(rows,cols,6);
            set(s6,'position',[ 3*d+2*width   d        width height])
        end
        
        %% Get functions
        function out = getStandardElement(s)
            if (~isobject(s.standardElement))
                s.loadStandardElement;
            end
            out = s.standardElement;
        end

        function out = getMesh(s)
            if (~isobject(s.mesh))
                s.loadMesh;
            end
            out = s.mesh;
        end
        
        function out = getNS(s)
            if (~isobject(s.NS))
                s.loadNS;
            end
            out = s.NS;
        end
        
        function out = getCH(s)
            if (~isobject(s.CH))
                s.loadCH;
            end
            out = s.CH;
        end
        
        function out = getPhysics(s)
            out = cell(2,1);
            
            out{1} = s.getCH;
            out{2} = s.getNS;
        end
        
        function out = getIter(s)
            out = s.data.iter;
        end
        
        function checkAMR(s,physicsID)            
            if (nargin==1 || physicsID==1 )
                physics = s.CH;
            else
                physics = s.NS;
            end
            
            physics.computeResidual;
            physics.meshUpdate( physics.getMesh.maxLevel );
        end
    end
    
    methods (Access=private)        
        function checkData(s)
            if (~isstruct(s.data))
                error('There is no data present, please load data first')
            end
        end
        
        function checkNS(s)
            s.checkData;
            if (~isstruct(s.data.NS))
                error('There is no NS data present')
            end
        end
        
        function checkCH(s)
            s.checkData;
            if (~isstruct(s.data.CH))
                error('There is no CH data present')
            end
        end
        
        function checkMesh(s)
            s.checkData;
            if (~isstruct(s.data.mesh))
                error('There is no mesh data present')
            end
        end
    end
    
    methods (Static)
        function out = loadRestart(restartFile,n,restart)
            if (restart)
                out = Solution(restartFile);
                out.loadData(n);
            else
                out.status = false;
            end
        end
        
        function plot(baseFile,iter,Q)
            if (nargin==2)
                sol = Solution(baseFile);
            else
                sol = Solution(baseFile,Q);
            end
            sol.load(iter);
            sol.plotNS;
        end
    end
end