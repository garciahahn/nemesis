function c = smallestCircle(points)
  num_points = size(points, 1);
  c = [];
  shuffled_points = reshape(points(randperm(num_points), :), size(points));
  
  for i = 1:num_points
    p = shuffled_points(i, :);
    if(isempty(c) | ~is_in_circle(c, p))
      c = make_circle_one_point(shuffled_points(1:i, :), p);
    end % if
  end % for
end

function c = make_circle_one_point(points, p)
  c = [p(1), p(2), 0];
  for i = 1:size(points, 1)
    if ~is_in_circle(c, points(i, :))
      if c(3) == 0
        c = make_diameter(p, points(i, :));
      else
        c = make_circle_two_points(points(1:i, :), p, points(i, :));
      end % if
    end % if
  end % for
end % function make_circle_one_point

function out = make_circle_two_points(points, p, q)
  circ = make_diameter(p, q);
  left = [];
  right = [];
  
  for i = 1 : size(points, 1)
    if is_in_circle(circ, points(i, :))
      continue 
    end % if
    
    cross_var = cross_product(p, q, points(i, :));
    c = make_circumcircle(p, q, points(i, :));
    if isempty(c)
      continue
    elseif cross_var > 0 & (isempty(left) | cross_product(p, q, [c(1), c(2)]) > cross_product(p, q, [left(1), left(2)]))
      left = c;
    elseif cross_var < 0 & (isempty(right) | cross_product(p, q, [c(1), c(2)]) < cross_product(p, q, [right(1), right(2)]))
      right = c;
    end % if
  end % for
  
  if (isempty(left) && isempty(right))
    out = circ;
  elseif (isempty(left))
    out = right;
  elseif (isempty(right))
    out = left;
  else
    if (left(3) <= right(3))
      out = left;
    else
      out = right;
    end % if
  end % if
end % function make_circle_two_points

function out = is_in_circle(c, p)
  out = ~isempty(c) & (sqrt((p(1) - c(1)) ^ 2 + (p(2) - c(2)) ^ 2) <= (c(3) * (1 + 1e-14)));
end % function is_in_circle

function out = make_diameter(a, b)
  cx = (a(1) + b(1)) / 2;
  cy = (a(2) + b(2)) / 2;
  r0 = sqrt((cx - a(1))^2 + (cy - a(2))^2);
  r1 = sqrt((cx - b(1))^2 + (cy - b(2))^2);
  out = [cx, cy, max(r0, r1)];
end % function make_diameter

function out = make_circumcircle(a, b, c)
  % Mathematical algorithm from Wikipedia: Circumscribed circle
  ox = (min([a(1), b(1), c(1)]) + max([a(1), b(1), c(1)])) / 2;
  oy = (min([a(2), b(2), c(2)]) + max([a(2), b(2), c(2)])) / 2;
  ax = a(1) - ox;  ay = a(2) - oy;
  bx = b(1) - ox;  by = b(2) - oy;
  cx = c(1) - ox;  cy = c(2) - oy;
  d = (ax * (by - cy) + bx * (cy - ay) + cx * (ay - by)) * 2;
  if (d == 0.0)
    out = [];
  else % if
    x = ox + ((ax*ax + ay*ay) * (by - cy) + (bx*bx + by*by) * (cy - ay) + (cx*cx + cy*cy) * (ay - by)) / d;
    y = oy + ((ax*ax + ay*ay) * (cx - bx) + (bx*bx + by*by) * (ax - cx) + (cx*cx + cy*cy) * (bx - ax)) / d;
    ra = sqrt((x - a(1))^2 + (y - a(2))^2);
    rb = sqrt((x - b(1))^2 + (y - b(2))^2);
    rc = sqrt((x - c(1))^2 + (y - c(2))^2);
    r = max([ra, rb, rc]);
    out = [x, y, r];
  end % if
end % function make_circumcircle
    
function out = cross_product(p, q, r)
  aux = [p;q;r];
  out = sum(cross(aux(:, 1), aux(:, 2)));
end % function cross_product