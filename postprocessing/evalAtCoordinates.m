function out = evalAtCoordinates(field, x, derivative_grade)
% EVALATCOORDINATES Evaluates a set of coordinates for a given timestep in the
% Nemesis software package.
%   A = EVALATCOORDINATES(FIELD, X) Writes in A the given values for a FIELD at
%   coordinates X, re constructing the solution using the modular weights
%   and the correspondant basis functions.

elements = findElementByPosition(field.mesh, x(:, 1), x(:, 2));
unique_elements = unique(elements);
track = 1;
logical_coordinates = zeros(size(x));
out = zeros(size(x, 1), 1);

for i = 1 : numel(unique_elements)
  element = field.mesh.getElement(unique_elements(i));
  vertices = element.geometric.element(0);
  offset = vertices(1).X;
  idx = elements == unique_elements(i);
  for j = 1 : 2
    logical_coordinates(idx, j) = (x(idx, j) - offset(j)) / element.geometric.J(j, j) - 1;
  end % forcounts_sum = cum_sum(counts);
  switch derivative_grade
  
      case 'v'
          [M_x, ~, ~] = element.finite.basis{1}.GetCustomQuadraturePoints(logical_coordinates(idx, 1));
          [M_y, ~, ~] = element.finite.basis{2}.GetCustomQuadraturePoints(logical_coordinates(idx, 2));
      case 'x'
          [~, M_x, ~] = element.finite.basis{1}.GetCustomQuadraturePoints(logical_coordinates(idx, 1));
          M_x = M_x / element.J(1);
          [M_y, ~, ~] = element.finite.basis{2}.GetCustomQuadraturePoints(logical_coordinates(idx, 2));
      case 'y'
          [M_x, ~, ~] = element.finite.basis{1}.GetCustomQuadraturePoints(logical_coordinates(idx, 1));
          [~, M_y, ~] = element.finite.basis{2}.GetCustomQuadraturePoints(logical_coordinates(idx, 2));
          M_y = M_y / element.J(2);
      case 'xx'
          [~, ~, M_x] = element.finite.basis{1}.GetCustomQuadraturePoints(logical_coordinates(idx, 1));
          M_x = M_x / element.J(1)^2;
          [M_y, ~, ~] = element.finite.basis{2}.GetCustomQuadraturePoints(logical_coordinates(idx, 2));
      case 'yy'
          [M_x, ~, ~] = element.finite.basis{1}.GetCustomQuadraturePoints(logical_coordinates(idx, 1));
          [~, ~, M_y] = element.finite.basis{2}.GetCustomQuadraturePoints(logical_coordinates(idx, 2));
          M_y = M_y / element.J(2)^2;
      case {'xy', 'yx'}
          [~, M_x, ~] = element.finite.basis{1}.GetCustomQuadraturePoints(logical_coordinates(idx, 1));
          M_x = M_x / element.J(1);
          [~, M_y, ~] = element.finite.basis{2}.GetCustomQuadraturePoints(logical_coordinates(idx, 2));
          M_y = M_y / element.J(2);
  end
  
  points = size(M_x, 1);
  H = zeros(points, size(M_x, 2) * size(M_y, 2));
  for j = 1 : points
    H(j,:) = kron(M_y(j, :), M_x(j, :));
  end % for
  
  alpha = field.active.alpha(unique_elements(i));
  alpha = alpha{1};
  Z = field.mesh.Z{unique_elements(i)};
  alpha = Z * alpha(:);
  alpha = reshape(alpha, [], 2);
  
  val = H * alpha(:, 2);
  
  out(track:track-1+numel(val)) = val;
  track = track + numel(val);
end % for

end % function evalAtCoordinates