function [val, q_x, q_y] = findNearestQuadValue(field, x, y)
  element = findElementByPosition(field.mesh, x, y);
  loc = field.mesh.getElement(element).nodes;
  x_dist = (loc{1} - x) .^ 2;
  y_dist = (loc{2} - y) .^ 2;
  M = repmat(x_dist, 1, numel(y_dist)) + y_dist';
  [~, I] = min(M, [], 'all', 'linear');
  data = reshape(field.getValue(element), field.mesh.getElement(element).finite.qSize);
  data = data(:, :, end);
  idy = ceil(I / numel(loc{1}));
  idx = mod(I-1, numel(loc{1})) + 1;
  q_x = loc{1}(idx);
  q_y = loc{2}(idy);
  val = data(I);
end % findNearestQuad


%System.createAnalysisFramework('Settings', 1);

% mesh = MeshCollection.get(1);
% R = FieldCollection.get('R');
% mass = 0;
% 
% for e = mesh.eRange
%   element = mesh.getElement(e);
%   data = reshape(R.getValue(Derivative.value, e), element.finite.qSize);
%   data = data(:, :, end);
%   mass = mass + data(:)' * element.finite.Wspatial * element.J(1) * element.J(2);
% end % for