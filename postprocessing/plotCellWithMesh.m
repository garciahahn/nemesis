function plotCellWithMesh(value_cell, mesh)
  clf;
  for e = mesh.eRange
    
    [X, Y, Z, T] = mesh.elements(e).getNodes;
    X = squeeze(X); Y = squeeze(Y);
    Z = squeeze(Z); T = squeeze(T);
    X = X(:, :, end);
    Y = Y(:, :, end);
    surf(X, Y, double(value_cell{e}), 'FaceColor', 'interp');
    hold on
  end % for
  xlabel('X');
  ylabel('Y');
  zlabel('Value');
end