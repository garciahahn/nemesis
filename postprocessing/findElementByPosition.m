function out = findElementByPosition(mesh, x, y)
  s = numel(mesh.eRange);
  x_boundaries = zeros(s, 2);
  y_boundaries = zeros(s, 2);
  for e = mesh.eRange
    element = mesh.getElement(e);
    [x_b, y_b] = findBoundaryNodes(element);
    x_boundaries(e, :) = x_b;
    y_boundaries(e, :) = y_b;
  end % for
  
  for i = 1 : numel(x)
    candidates_x_inf = abs(diff(x_boundaries < x(i), 1, 2));
    candidates_y_inf = abs(diff(y_boundaries < y(i), 1, 2));
    candidates_x_sup = abs(diff(x_boundaries > x(i), 1, 2));
    candidates_y_sup = abs(diff(y_boundaries > y(i), 1, 2));
    
    temp_inf = find(candidates_x_inf & candidates_y_inf);
    temp_sup = find(candidates_x_sup & candidates_y_sup);
    
    if (~isempty(temp_inf))
        out(i) = temp_inf;
    elseif (~isempty(temp_sup))
        out(i) = temp_sup;
    else
        ME = MException("findElementByPosition:no_candidate", "There are no admisible candidates for position x = %.4f, y = %.4f\nPossibly, the point is not in the domain.\n", x(i), y(i));
        throw(ME);
    end % if
  end % for
  out = out';
end % function findElementByPosition

function [x_b, y_b] = findBoundaryNodes(e)

  loc = e.nodes;
  
  x = loc{1};
  y = loc{2};
  
  x_b = [x(1), x(end)];
  y_b = [y(1), y(end)];

end 