classdef FieldGroup < handle
    %FIELDGROUP is a wrapper for multiple physics fields. It defines the 
    %   type of the field (scalar, vector or tensor), and it's name. This 
    %   object forms the base for the Paraview export (VTU format).
    
    properties (Access = private)
        out_fieldNameWidth = -15;
        out_fieldTypeWidth = -10;
    end
    
    properties
        numFields;              % number of fields inside this group
        shortTitle; title;      % short & normal title
        
        field;                  % cell array with all fields of this group
        mesh;                   % base mesh for this group of fields
        type;                   % Possible types : scalar(0), vector(1), tensor(2)
        name;                   % name of the fields
        nComp;                  % number of components (can be equal to 2)
        plotComp;               % number of components to plot (always 1 or 3)
    end
    
    methods
        function s = FieldGroup(numFields,varargin)
            %FIELDGROUP Construct an instance of this class
            %   Once initialized, the number
            %   of fields is fixed. The mesh must be shared between all
            %   fields inside this FIELDGROUP.
            
            s.numFields = numFields;
            
            % only want 2 optional inputs at most
            numvarargs = length(varargin);
            if numvarargs > 2
                error('LSQDIM:FieldGroup:TooManyInputs','requires at most 2 optional inputs');
            end
            
            % set defaults for optional inputs
            optargs = {'fg' 'fieldGroup'};

            % now put these defaults into the valuesToUse cell array, 
            % and overwrite the ones specified in varargin.
            optargs(1:numvarargs) = varargin;

            % Place optional args in memorable variable names
            [s.shortTitle, s.title] = optargs{:};
                        
            s.field     = cell(1,numFields);
            s.name      = cell(1,numFields);
            s.type      = zeros(1,numFields);
            s.nComp     = zeros(1,numFields);
            s.plotComp  = zeros(1,numFields);
        end
        
        function disp(s)
            if s.numFields==1
                fprintf('  FieldGroup %s (%s) with %d field:\n',s.title,s.shortTitle,s.numFields)
            else
                fprintf('  FieldGroup %s (%s) with %d fields:\n',s.title,s.shortTitle,s.numFields)
            end
            fprintf('\n')
            fprintf('         %*s  %*s  %s\n',s.out_fieldNameWidth,'Field name',s.out_fieldTypeWidth,'Field type','nComp');
            fprintf('         %s  %s  %s\n',repmat('=',1,abs(s.out_fieldNameWidth)),repmat('=',1,abs(s.out_fieldTypeWidth)),repmat('=',1,5));
            for i=1:s.numFields
                if ~isempty(s.name{i})
                    fprintf('     %-4d%*s  %*s  %-5d\n',i,s.out_fieldNameWidth,s.name{i},s.out_fieldTypeWidth,FieldGroup.getTypeName(s.type(i)),s.nComp(i))
                else
                    fprintf('     %-4d%*s  %*s  %-5d\n',i,s.out_fieldNameWidth,'empty',s.out_fieldTypeWidth,'empty',0)
                end
            end
            fprintf('\n')
        end
        
        function add(s,field,varargin)
            %ADD Function to ADD a fieldgroup 
            %   varargin can contain: type, name, position
            
            % only want 3 optional inputs at most
            numvarargs = length(varargin);
            if numvarargs > 3
                error('LSQDIM:FieldGroup:TooManyInputs','requires at most 3 optional inputs');
            end
                        
            if (numel(field)>1 && numvarargs<2)
                error('LSQDIM:FieldGroup:NotEnoughInputs','requires at least 2 additional inputs (type & name) if more than 1 field is provided');
            end
            
            % set defaults for optional inputs
            optargs = {0 'none' s.getAvailablePosition};

            % now put these defaults into the valuesToUse cell array, 
            % and overwrite the ones specified in varargin.
            optargs(1:numvarargs) = varargin;

            % Place optional args in memorable variable names
            [type, name, position] = optargs{:};
                        
            % 
            if (numel(field)==1 && numvarargs<2)
                name = field.name;
            end
                        
            % verify position value, and throw an error if not valid
            FieldGroup.verifyType(type);
            s.verifyPosition(position);
            
            s.field{position} = field;
            s.type(position)  = type;
            s.name{position}  = name;
            s.nComp(position) = numel(field);
            
            if s.nComp(position)==1
                s.plotComp(position) = 1;
            else
                s.plotComp(position) = 3;
            end
            
            if (isempty(s.mesh))
                if iscell(field)
                    s.mesh = field{1}.mesh;
                else
                    s.mesh = field.mesh;
                end
            end
        end
        
        function writeVTU(s,varargin)
            % only want 2 optional inputs at most
            numvarargs = length(varargin);
            if numvarargs > 2
                error('LSQDIM:FieldGroup:writeVTU:TooManyInputs','requires at most 3 optional inputs');
            end
            
            % set defaults for optional inputs
            optargs = {0 0};

            % now put these defaults into the valuesToUse cell array, 
            % and overwrite the ones specified in varargin.
            optargs(1:numvarargs) = varargin;

            % Place optional args in memorable variable names
            [iter,time] = optargs{:};
            write_vtu(s,iter,time,System.parallel.rank,System.parallel.nproc);
        end
        
        function unifyMesh(s)
            for i=1:numel(s.field)
                selectedField = s.field{i};
                
                if ~iscell(selectedField)
                    selectedField.unifyMesh;
                else
                    for v=1:numel(selectedField)
                        selectedField{v}.unifyMesh;
                    end
                end
                
            end
        end
    end
    
    methods (Access = private)
        
        function out = getAvailablePosition(s)
            % start from i=1 and loop as long as s.field is not empty, but
            % break the loop if i>numFields. In that case -1 is returned
            i = 1;
            
            while ~isempty(s.field{i})
                i=i+1;
                if (i>s.numFields)
                    i = -1;
                    break
                end
            end
            
            out = i;
        end
        
        function verifyPosition(s,position)
            if (~inrange(position,1,s.numFields))
                error('There is no open position available in the FieldGroup, please provide a position')
            end
        end
        
    end
    
    methods (Access = private, Static)
        function verifyType(type)
            if (~inrange(type,0,2))
                error('There is no type %d available in the FieldGroup, please provide type 0, 1 or 2\n',type)
            end
        end
        
        function out = getTypeName(typeNumber)
            switch typeNumber
                case 0
                    out = 'scalar';
                case 1
                    out = 'vector';
                case 2
                    out = 'tensor';
                otherwise
                    error('A typeNumber %d is not defined\n',typeNumber)
            end
        end
    end
end

