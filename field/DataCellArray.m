classdef DataCellArray < handle
    %DATACELLARRAY Data structure based on cell arrays
    %   The data of each element is stored in a cell array format, which
    %   can be stored internally as a flat cell array or in a tree format.
    
    properties
        format;      % 0: flat; 1: tree
        data;
        
        baseSize;
        leafSize;
        levels;
    end
    
    methods
        function s = DataCellArray(data,format,baseSize,leafSize,levels)
            %DATACELLARRAY Construct an instance of this class
            %   An optional format sets the initial format, and data can be
            %   in flat or tree format
            s.data   = data;
            
            if (nargin==1)
                s.detectFormat;
            elseif (nargin==2)
                s.format   = format;
            else
                s.format   = format;
                s.baseSize = baseSize;
                s.leafSize = leafSize;
                s.levels   = levels;
            end
        end
                
        function toTree(s)
            %TOTREE Converts the data into tree format
            if (s.format==0)
                s.check;
                s.data = s.makeTree();
                s.format = DataFormat.Tree;
            end
        end
        
        function toFlat(s)
            %TOFLAT Convert the data into flat format
            if (s.format==1)
                s.check;
                s.data = s.makeFlat();
                %s.data = cell2mat(s.data);
                s.format = DataFormat.Flat;
            end
        end
        
        function count = cellCount(s,maxDepth)
            if (nargin==1)
                maxDepth = 0;
            end
            count = DataCellArray.countCells( s.data, 0, maxDepth );
        end
        
        % Return the data of this DataCellArray
        function out = getData(s,format)
            % Convert to flat / tree format
            if nargin==2
                if (format==DataFormat.Flat)
                    s.toFlat;
                elseif (format==DataFormat.Tree)
                    s.toTree;
                end
            end
            out = s.data;
        end
        
        function check(s)
            if (isempty(s.baseSize))
                error('DataCellArray :: Please set the baseSize first\n')
            end
            if (isempty(s.leafSize))
                error('DataCellArray :: Please set the leafSize first\n')
            end
            if (isempty(s.levels))
                error('DataCellArray :: Please set the levels first\n')
            end
            if (s.format==DataFormat.Tree)
                if ( s.cellCount(-1) ~= numel( s.levels ) )
                    error('The number of levels does not match the number of cells in the data variable (Tree)\n')
                end
            elseif (s.format==DataFormat.Flat)
                if ( numel(s.data) ~= numel( s.levels ) )
                    error('The number of levels does not match the number of cells in the data variable (Flat)\n')
                end
            else
                error('This data format is not supported\n')
            end
        end
        
        function set.format(s,format)
            if (~isa(format,'DataFormat'))
                error('Please use a valid DataFormat')
            end
            s.format = format;
        end
    end
    
    methods (Access = protected)
        
        % If the cell count differs for various levels, the data should be
        % in tree format.
        function detectFormat(s)
            if (iscell(s.data))
                if ( s.countCells(1) ~= s.countCells(0) )
                    s.format = DataFormat.Tree;
                else
                    s.format = DataFormat.Flat;
                end
            else
                s.format = DataFormat.Flat;
            end
        end
        
        % Recursive function to create a tree structure
        function out = makeTree(s,array,numBaseElements,levels,currentLevel)
            % Use the Material.All Lv array if none is given
            if (nargin==1)
                array = s.data;
            end

            % Initialization settings, so the function can be called by
            % just a single argument 'array'.
            if (nargin<3)
                numBaseElements = s.baseSize;                               % Extract the number of baseElements from s.data
                levels          = s.levels;                                 % Get the current Lv array for all materials
                currentLevel    = 0;                                        % Start from level 0
            else
                if (isempty(numBaseElements))
                    numBaseElements = s.baseSize;
                end
                if (nargin<5 || isempty(currentLevel) )
                    currentLevel = 0;
                end
            end

            out = cell(numBaseElements,1);
            e = 1;

            % Determine the volume of the current children
            numLeafs        = prod(s.leafSize);
            leafVolume      = (1/numLeafs).^(levels-currentLevel);

            % Loop over the baseElements until the summed volume is equal
            % to 1. That indicates that the range (eStart:e-1) represents
            % a full L0 element.
            for n=1:numBaseElements
                eStart = e;
                volume = 0;

                while volume < 1
                   volume = volume + leafVolume(e);
                   e = e+1;
                end

                if max(levels(eStart:e-1))>currentLevel
                    % move one level deeper in the octree structure, this
                    % will create a nested cell array
                    out{n} = s.makeTree( array(eStart:e-1), numLeafs, levels(eStart:e-1), currentLevel+1 );
                else
                    % return the 
                    out{n} = array(eStart:e-1);
                end
            end
        end
        
        function out = makeFlat(s,array)
            if (nargin==1)
                array = s.data;
            end
            
            numCells = DataCellArray.countCells(array);
            out = cell( numCells, 1 );
               
            % Extract the child cell arrays 
            position = 0;
            for e=1:numel(array)
                if (iscell(array{e}))
                    flatData = s.makeFlat( array{e} );
                    flatSize = numel(flatData);
                    out(position+1:position+flatSize) = s.makeFlat( array{e} );
                    position = position + flatSize;
                else
                    out{position+1} = array{e};
                    position = position + 1;
                end
            end
        end
        

    end
    
    methods (Static)
        function out = convertToFlat(data)
            temp = DataCellArray( data, 1 );
            out = temp.getData(0);
        end
        
        function out = convertToTree(data)
            temp = DataCellArray( data, 0 );
            out = temp.getData(1);
        end
        
        function cellCount = countCells(array,currentDepth,maxDepth)
            if nargin==1
                currentDepth = -2;
                maxDepth     = -1;
            end
            
            cellCount = 0;
            for e=1:numel(array)
                if (iscell(array))
                    if (iscell(array{e}) && (currentDepth<maxDepth || maxDepth==-1))
                        cellCount = cellCount + DataCellArray.countCells(array{e},currentDepth+1,maxDepth);
                    else
                        cellCount = cellCount + 1;
                    end
                else
                    cellCount = cellCount + DataCellArray.countCells(array(e),currentDepth+1,maxDepth);
                end
            end
        end
        
        function maxDepth = maxDepth(array,depth)
            maxDepth = 0;
%             if (nargin==1)
%                 depth = 0;
%             end
%             for e=1:numel(array)
%                 if (iscell(array{e}))
%                     depth = depth+1;
%                     maxDepth = DataCellArray.maxDepth(array{e},depth);
%                 end
%             end
        end
    end
end

