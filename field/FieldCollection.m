classdef FieldCollection < Singleton
    %FIELDCOLLECTION Collection of the fields of all physics
    %   Every Physics has it's own fields, which are only accessible from
    %   itself. By putting all these fields also in a collection, every
    %   physics can use any fields provided by other Physics
    
    properties (Access=private)
        fields;
        numFields = 0;
    end
    
    methods(Access=private)
        % Guard the constructor against external invocation.  We only want
        % to allow a single instance of this class.  See description in
        % Singleton superclass.
        function s = FieldCollection()
        end
    end
    
    methods (Static)
        % Concrete implementation.  See Singleton superclass.
        function s = instance()
            persistent uniqueInstance
            if isempty(uniqueInstance)
                s = FieldCollection();
                uniqueInstance = s;
            else
                s = uniqueInstance;
            end
        end
        
        % Function to add a field to the collection
        function add(field_in)
            s = FieldCollection.instance;
            s.fields{s.numFields+1} = field_in;
            s.numFields = numel(s.fields);
        end
        
        function remove(variable)
            fc = FieldCollection.instance;
            
            % If the variable passed is numeric, just use it as an index,
            % otherwise find the corresponding
            if isnumeric(variable)
                idx = variable;
            else
                fieldNames = cellfun( @(x) x.name, fc.fields, 'UniformOutput', false );
                idx = ismember(fieldNames,variable);
            end
            if (nnz(idx)==1)
                fc.fields( idx ) = [];      % completely remove the cell element
            elseif (nnz(idx)>1)
                warning('LSQDIM:field:FieldCollection :: the requested variable is multiple times present in the FieldCollection')
            else
                error('LSQDIM:field:FieldCollection :: the passed id is not understood')
            end
        end
        
        % Function to get a field from the collection (by variable name)
        function out = get(variable)
            fc = FieldCollection.instance;
            
            % If the variable passed is numeric, just use it as an index,
            % otherwise find the corresponding
            if isnumeric(variable)
                idx = variable;
            else
                fieldNames = cellfun( @(x) x.name, fc.fields, 'UniformOutput', false );
                idx = ismember(fieldNames,variable);
            end
            if (nnz(idx)==1)
                out = fc.fields{ idx };
            elseif (nnz(idx)>1)
                error('LSQDIM:field:FieldCollection :: the requested variable is multiple times present in the FieldCollection')
            else
                out = NaN;
                %error('LSQDIM:field:FieldCollection :: the requested variable is not present in the FieldCollection')
            end
        end
        
        function info()
            s = FieldCollection.instance();
            numFields = numel(s.fields);
            fprintf('    FieldCollection with %d fields\n',numFields);
            for i=1:numFields
                fprintf('       Field %d : %s\n',i,s.fields{i}.name);
            end
        end
        
        function reset()
            s = FieldCollection.instance();
            s.fields = cell(0);
            s.numFields = 0;
        end
        
        function update()
            s = FieldCollection.instance();
            for i=1:s.numFields
                s.fields{i}.meshUpdate;
            end
        end
        
        % Function to save a PhysicsCollection to a struct. The number of  
        % physics and structs of the physics are saved (by calling
        % the save(physics) function (see Physics.save).
        function out = save()
            s = FieldCollection.instance();            
            out = cell(s.numFields,1);
            for i=1:s.numFields
                field = FieldCollection.get(i);
                out{i} = save(field);
            end
        end
        
        % Function to load a PhysicsCollection from a saved struct. First the
        % collection is reset, and next the physics are loaded and
        % added to the collection (see Physics.load).
        function load(fields)
            FieldCollection.reset;
            for i=1:numel(fields)
                temp = Field.load(fields{i});
                FieldCollection.add(i,temp);
            end
        end
        
        % Julian ADDED. Function to save fields to the output folder of the
        % simulation. This is intended to work with the values using matlab
        % functions.
        
    end
    
    methods

    end
end

