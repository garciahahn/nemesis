classdef Field < handle
    %Field Container for subfields
    %   A Field is a 
    
    properties (Access=public)
        active;        
        activeType;
        
        mesh;
        dh;
        
        name;
        periodic;
        numDof;             % Modes / polynomials
        
        subFieldList;
        
        oldTimeLevels;
        
        prescribed;
        
        L2norm; H1norm;
        
        plotOrder = 0;
        
        relaxation;
        
        activeMaterial;
    end
    
    properties(Access=private)
        variable;        
        
        useMean = false;
        
        numElements;        % Number of (local) elements
%         numDof;             % Modes / polynomials
        numEqs;             % Quadrature
        
%         prescribed;
        prescribedSynced;
        
        isEmpty;
        
        error       = 0;
        relError    = 0;
        
        n = 0;
        
        numInternal;
        numInterface;
        numPrescribed;
        numSuppressed;
                
        numPrevious;        % for time stepping multiple previous time levels might be required
        alphaArray;        % FE solution at time level n
        
        globalAlphaArray;
        
        isNonlinear;
        isCoupled;
        
        nWeight, cWeight, tWeight;
        
        errorNorm;
        errorMax;
        
        xx2_av;
        yy2_av;
        
        meshID;
    end
     
    methods
        function s = Field(mesh,varNumber,varargin)
            
            % Process required inputs
            s.mesh      = mesh;
            
            s.variable  = varNumber;
            
            [s.name,s.periodic,s.relaxation] = VerifyInput(varargin,{'variableName',true,1});
            
            s.dh = mesh.getDoFHandler(s.periodic);
            
            s.subFieldList = cell(1,numel( enumeration('FieldType') ));
            
            s.activeMaterial = Material.Fluid;
            
            s.meshID = s.mesh.Lv;
            
            s.resetPrescribed;
        end
        
        function addSubField(s,fieldType,activate,varargin)
            
            if (nargin==2 || isempty(activate) )
                if (~isempty(s.subFieldList{fieldType}))
                    activate = (s.active==s.subFieldList{fieldType});
                else
                    activate = true;
                end
            end
            
            % set defaults for optional inputs and verify the input
            default = {s.mesh,s.dh,0,true};
            [mesh,dh,definedFunction,useAlphaSolver] = VerifyInput(varargin,default);
            
            s.subFieldList{fieldType} = SubField(s,fieldType,mesh,dh,definedFunction,useAlphaSolver);
            s.subFieldList{fieldType}.relaxation = s.relaxation;
            if (activate)
                s.active = s.subFieldList{fieldType};
            end
        end
        
        function initSubField(s,varargin)
            subFieldType = varargin{1};
            
            switch subFieldType
                case FieldType.Current
                    s.current = SubField(s,subFieldType,'current');
                    s.current.weight(1);
%                 case FieldType.Previous
%                     if (nargin==2)
%                         s.numPrevious = 1;
%                     else
%                         s.numPrevious = varargin{2};
%                     end
%                     s.previous = cell(s.numPrevious,1);
%                     for i=1:s.numPrevious
%                         s.previous{i} = SubField(s,subFieldType,['previous' num2str(i)]);
%                     end
                case FieldType.Nonlinear
                    s.nonlinear = SubField(s,subFieldType,'nonlinear');
                    %s.nonlinear.weight = s.wi;
                case FieldType.Coupled
                    s.coupled = SubField(s,subFieldType,'coupled');
                    %s.coupled.weight = s.wc;
                case FieldType.Exact
                    s.exact = SubField(s,subFieldType,'exact');
                case FieldType.Interpolated
                    s.updateInterpolated(varargin{2});       
            end
            
            % Initialize the active field if it's still empty
            if (isempty(s.activeType))
                s.activeType = FieldType.Current;
            end
        end
        
        function out = getDoFHandler(s)
            out = DoFHandlerCollection.getByID( s.mesh, s.periodic );
        end
        
        function setActive(s,fieldType)
            if (isa(fieldType,'FieldType'))
                s.active = s.subFieldList{ fieldType };
            else
                error('Please provide a valid FieldType');
            end
        end
        
        function setFieldMesh(s,fieldType,mesh)
            switch fieldType
                case FieldType.Current
                    s.active = s.current;
                case FieldType.Nonlinear
                    s.active = s.nonlinear;
                case FieldType.Coupled
                    s.active = s.coupled;
                    s.active.mesh = mesh;
                    s.active.setSubField(s.current);
                case FieldType.Exact
                    s.active = s.exact;
                case FieldType.Interpolated
                    s.active = s.interpolated;
                    s.active.mesh = mesh;
                    s.active.setSubField(s.current);
                case FieldType.Previous
                    s.active = s.previous;
            end
        end
        
        % Function to set the active subField
        function updateField(s)
            if (s.active.type==FieldType.Coupled && s.active.isInterpolated)
                %s.interpolated.setSubField(s.current);
                s.initSubField(FieldType.Interpolated,s.active.mesh);
            end
%                 case FieldType.Nonlinear
%                     s.nonlinear.setSubField(s.current);
%                 case FieldType.Coupled
%                     s.coupled.setSubField(s.current);
%                 case FieldType.Interpolated
%                     s.interpolated.setSubField(s.current);
% %                 case FieldType.Previous
% %                     s.active = s.previous;
%                 otherwise
%                     error('Field:updateField','Unsupported fieldType');
%             end
        end
        
        function extrapolateInTime(s)
            disp('')
            if (numel(s.oldTimeLevels)==1)
                %s.subFieldList{1}.alpha = s.oldTimeLevels{1}.alpha;
            elseif (numel(s.oldTimeLevels)==2)
                %s.subFieldList{1} = 2 * s.oldTimeLevels{1} - s.oldTimeLevels{2};
            else
                error('No oldTimeLevels available');
            end
        end
                
%         function checkCoupledMesh(s)
%             if (~isempty(s.interpolated) && s.interpolated.needsMeshUpdate)
%                 s.interpolated.meshUpdate(false);
%             end
%             if (~isempty(s.coupled) && s.coupled.needsMeshUpdate)
%                 s.coupled.meshUpdate(false);
%             end
%         end
        
        function selectNonlinear(s)           
            s.activeType = FieldType.Nonlinear;
            
            s.setActive(s.activeType);
            
            if (s.mesh.isChanged)
                s.meshUpdate();
            end
        end
                
        function selectCoupled(s,mesh)           
            s.activeType = FieldType.Coupled;
            s.setActive(s.activeType);
            
            for i=1:numel(s.coupled)
                %if ~isequal(s.coupled{i}.Lv,mesh.octTree.getLv)
                if (~isequal(s.coupled{i}.mesh.octTree.getLv,mesh.octTree.getLv) || s.coupled{i}.mesh.isChanged )
                    s.coupled{i}.meshUpdate(mesh);
                end
            end
        end
        
        function selectTimelevel(s)
            s.activeType = FieldType.TimeLevel;
            s.setActive(s.activeType);
        end
        
        function selectCurrent(s)
            s.activeType = FieldType.Current;
            s.setActive(s.activeType);
        end
        
%         function shiftCoupled(s)
%             %s.coupled{i}.copy( s.current );
%             
%             % Rotate the coupled cell array
%             temp = s.coupled{end};
%             for i=numel(s.coupled):-1:2
%                 s.coupled{i} = s.coupled{i-1};
%             end
%             s.coupled{1} = temp;
%             
%             % Copy current to coupled{1}
%             s.coupled{1}.copy( s.current );
%             s.coupled{1}.Z      = s.mesh.Z;
%             s.coupled{1}.eRange = s.mesh.eRange;
%             s.coupled{1}.Lv     = s.mesh.octTree.getLv; 
%                 
%             if ~isequal(s.coupled{1}.mesh,s.mesh)
%                 s.coupled{1}.meshUpdate();
%             end
%         end
        
%         function init(s)
%             for i=1:numel(s.nonlinear)
%                 s.nonlinear{i}.Z = s.mesh.Z;
%             end
%             
%             s.current.computeAlpha();
%             
%             for i=1:numel(s.coupled)
%                 s.coupled{i}.copy( s.current );
%                 s.coupled{i}.Z = s.mesh.Z;
%             end
%         end
        
        function out = get.mesh(s)
            out = s.mesh;
        end
        
%         function out = getCurrent(s)
%             out = s.current;
%         end
        
        function out = getSize(s)
            out = size(s.alphaArray,1);
        end
        
        function update(s,fieldType)
            if (nargin==1)
                s.numDof      = s.dh.numLocalDof;            
%                 s.GM          = s.mesh.octTree.getGM;
            else
                switch fieldType
                    case FieldType.Nonlinear
                        s.copyCurrentToNonLinear;
                    case FieldType.Coupled
                        s.copyCoupling;
                end
            end
        end
        
        function meshUpdate(s)
            
            current = s.subFieldList{1};
            
            if current.needsMeshUpdate
                current.meshUpdate();
            
                %             try
                %                 for i=1:numel(s.nonlinear)
                %                     %if ( ~isequal(s.nonlinear{i}.mesh,s.mesh) || ~all(s.nonlinear{i}.mesh.Lv{1}==s.mesh.Lv{1}) || numel(s.nonlinear{i}.alpha)~=s.mesh.numLocalElements )
                %                         s.nonlinear{i}.meshUpdate();
                %                     %end
                %                 end
                %                 
                % %                 for i=1:numel(s.timeLevel)
                % %                     %if ( ~isequal(s.timeLevel{i}.mesh,s.mesh) || ~all(s.timeLevel{i}.mesh.Lv{1}==s.mesh.Lv{1}) || numel(s.timeLevel{i}.alpha)~=s.mesh.numLocalElements )
                % %                         s.timeLevel{i}.meshUpdate( s.timeLevel{i}.mesh );
                % %                     %end
                % %                 end
                %             catch MExp
                %                 save(['Field.meshUpdate.' int2str(System.rank) '.mat']);
                %                 error('Problem in Field.meshUpdate');
                %             end

                %             for i=1:numel(s.coupled)
                %                 s.coupled{i}.meshUpdate(true);
                %             end
                s.numElements = s.mesh.numLocalElements;

                s.resetPrescribed;
            end
            
%             if ~isequal( current.meshID, current.mesh.Lv )
%                 
%             end
        end
        
        function resetPrescribed(s)
            % Initialize the prescribed cell array after changing the mesh
            s.prescribed = cell(s.mesh.numLocalElements,1);            
            for e=1:numel(s.prescribed)
                element = s.mesh.getLocalElement(e);
                s.prescribed{e} = false(element.finite.dSize);
            end
        end
        
        function updateInterpolated(s,interpolationMesh)
            if ( isempty(s.subFieldList{ FieldType.Interpolated }) )
                %s.interpolated = SubField(s,FieldType.Interpolated,'interpolated');
                s.addSubField( FieldType.Interpolated,false,interpolationMesh);
            end
            if (nargin==2 && ~isempty(interpolationMesh))
                s.subFieldList{ FieldType.Interpolated }.mesh = interpolationMesh;
            end
            if (s.subFieldList{ FieldType.Interpolated }.needsMeshUpdate)
                s.subFieldList{ FieldType.Interpolated }.meshUpdate;
            end
        end
        
        function out = computeValue(s)
            out = cell(s.mesh.numLocalElements,1);
            for e=1:s.mesh.numLocalElements
                element = s.mesh.getLocalElement(e);
                Z = s.mesh.Z{e};
                out{e} = element.getH * (Z * s.alpha{e}(:) );
            end
        end

        function computeAlpha(s,elementNumber)
            if (nargin==1)
                s.active.computeAlpha();
            else
                s.checkElementNumber(elementNumber);
                s.active.computeAlpha(elementNumber);
            end
            
%             s.copySubField(s.nonlinear,s.current);
%             s.copySubField(s.coupled  ,s.current);
        end
        
        function out = getNumElements(s)
            % LOCAL ELEMENTS
            %out = s.mesh.getNumLocalElements;
            out = s.mesh.numElements;
        end
        
        function out = getValue(s, e, value_type)
            
            s.checkElementNumber(e);
            if (nargin < 3)
              out = s.active.getValue(Derivative.value, e);
            else
              out = s.active.getValue(value_type, e);
            end % if
        end
        function out = getValueInterpolated(s,e,finiteElement,mesh)
            if (nargin==3 || isequal(s.mesh.Lv,mesh.Lv))
                current = s.subFieldList{1};
                current.checkElementNumber(e);
                element = current.mesh.getElement(e);
                try
                    Z = current.mesh.Z{e};
                    out = finiteElement.getH * (Z * current.alpha{ element.localID }(:));
                catch MExp
                    save(['getValueInterpolated_n3.' int2str(System.rank) '.mat']);
                    error('Problem in Field.getValueInterpolated') 
                end
                out = reshape(out,finiteElement.qSize);
            else
                save(['getValueInterpolated.' int2str(System.rank) '.mat']);
                error('Field.getValueInterpolated problem!')
%                 if (~isequal(s.coupled{1}.mesh,mesh))
%                     s.coupled{1}.meshUpdate(mesh);
%                 end
%                 s.coupled{1}.checkElementNumber(e);
%                 element = s.coupled{1}.mesh.getElement(e);
%                 out = disc.H * s.coupled{1}.alpha{ element.localID }(:);
%                 out = reshape(out,disc.Q);
            end
        end
        
        function out = getDxInterpolated(s,e,finiteElement,mesh)
            if (nargin==3 || isequal(s.mesh.Lv,mesh.Lv))
                current = s.subFieldList{1};
                current.checkElementNumber(e);
                element = current.mesh.getElement(e);
                try
                    Z = current.mesh.Z{e};
                    out = (finiteElement.D{1}/element.J(1)) * (Z * current.alpha{ element.localID }(:));
                catch MExp
                    save(['getDxInterpolated_n3.' int2str(System.rank) '.mat']);
                    error('Problem in Field.getDxInterpolated') 
                end
                out = reshape(out,finiteElement.qSize);
            else
                save(['getDxInterpolated.' int2str(System.rank) '.mat']);
                error('Field.getDxInterpolated problem!')
            end
        end
        
        function out = getDyInterpolated(s,e,finiteElement,mesh)
            if (nargin==3 || isequal(s.mesh.Lv,mesh.Lv))
                current = s.subFieldList{1};
                current.checkElementNumber(e);
                element = current.mesh.getElement(e);
                try
                    Z = current.mesh.Z{e};
                    out = (finiteElement.D{2}/element.J(2)) * (Z * current.alpha{ element.localID }(:));
                catch MExp
                    save(['getDyInterpolated_n3.' int2str(System.rank) '.mat']);
                    error('Problem in Field.getDyInterpolated') 
                end
                out = reshape(out,finiteElement.qSize);
            else
                save(['getDyInterpolated.' int2str(System.rank) '.mat']);
                error('Field.getDyInterpolated problem!')
            end
        end
        
%         function out = getDxInterpolated(s,e,disc,mesh)
%             if (isequal(s.mesh,mesh))
%                 s.current.checkElementNumber(e);
%                 element = s.current.mesh.getElement(e);
%                 out = disc.D{1} * element.J(1) * s.current.alpha{ element.localID }(:);
%                 out = reshape(out,disc.Q);
%             else
%                 if (~isequal(s.coupled{1}.mesh,mesh))
%                     s.coupled{1}.meshUpdate(mesh);
%                 end
%                 s.coupled{1}.checkElementNumber(e);
%                 element = s.coupled{1}.mesh.getElement(e);
%                 out = disc.D{1} * element.J(1) * s.coupled{1}.alpha{ element.localID }(:);
%                 out = reshape(out,disc.Q);
%             end
%         end
%         
%         function out = getDyInterpolated(s,e,disc,mesh)
%             if (isequal(s.mesh,mesh))
%                 s.current.checkElementNumber(e);
%                 element = s.current.mesh.getElement(e);
%                 out = disc.D{2} * element.J(2) * s.current.alpha{ element.localID }(:);
%                 out = reshape(out,disc.Q);
%             else
%                 if (~isequal(s.coupled{1}.mesh,mesh))
%                     s.coupled{1}.meshUpdate(mesh);
%                 end
%                 s.coupled{1}.checkElementNumber(e);
%                 element = s.coupled{1}.mesh.getElement(e);
%                 out = disc.D{2} * element.J(2) * s.coupled{1}.alpha{ element.localID }(:);
%                 out = reshape(out,disc.Q);
%             end
%         end
%         
        function out = getValueArrayFormat(s,e,selection)
            s.current.checkElementNumber(e);
            
            element = s.mesh.getElement(e);
            values = reshape( s.current.value{ element.localID }, element.disc.dofeq);
            out = values(selection{:});
        end
        
        function out = getAlphaArrayFormat(s,e,selection)
            s.checkElementNumber(e);
            
            element = s.mesh.getElement(e);
            values = reshape( s.alpha{ element.localID }, element.disc.dofe);
            out = values(selection{:});
        end
        
        function out = getAlpha(s,e,solutionMode)
            if (nargin==2)
                solutionMode = SolutionMode.Normal;
            end
            
            if (nargin==3)
%                 switch solutionMode
%                     case SolutionMode.Nonlinear
%                         out = ( s.wi ) * s.alpha{e}(:) + (1-s.wi) * s.alphai{e}(:);
%                     case SolutionMode.Coupling
%                         out = ( s.wc ) * s.alpha{e}(:) + (1-s.wc) * s.alphac{e}(:);
%                     case SolutionMode.Normal
%                         out = s.alpha{e}(:);
%                 end
                error('Error in Field.getAlpha')
            elseif (nargin==1)
                out = s.alpha;
            end
        end
        
        function out = getAlphaArray(s)
            out = s.active.getAlphaArray;
        end
        
        function setAlphaArray(s,array)
            s.active.setAlphaArray(array);
            
%             if (isempty(s.numDof))
%                 s.update;
%             end
%             
%             s.copyNonLinear;
%             if (s.numDof==numel(array))
%                 s.current.alphaArray = array( 1 : s.numDof );
%             else
%                 s.current.alphaArray = array( (s.variable-1)*s.numDof+1 : s.variable*s.numDof );
%             end
%             s.current.convertAlpha(1);
%             s.value = s.computeValue;           
        end
        
        function loadAlphaArray(s,array)
            if (isempty(s.numDof))
                s.update;
            end
            
            s.copyNonLinear;
%             if (s.numDof==numel(array))
%                 s.alphaArray = array( 1 : s.numDof );
%             else

            eStart = s.mesh.octTree.eStart;
            eEnd   = s.mesh.octTree.eEnd;

                for e=eStart:eEnd
%                     element = s.mesh.getLocalElement(e);
                    s.alpha{ e-eStart+1 } = array( s.mesh.getGlobalGM{e} );
                end
%             end
            s.convertAlpha(0);
            s.value = s.computeValue;
        end
        
        function copyCurrentToNonLinear(s)
            s.subFieldList{ FieldType.Nonlinear }.copy( s.subFieldList{ FieldType.Current } );
        end
        
        % Copy from 'source' to 'destination' (default source = current)
        function copySolution(s,destination)
            if (isa(destination,'FieldType'))
                subField = s.subField( destination );
                
                % Check if the 'current' subField needs a meshUpdate
                if (s.subFieldList{1}.needsMeshUpdate)
                    s.subFieldList{1}.meshUpdate;
                end
                
                subField.copy( s.subFieldList{1} );
            else
                error('LSQDIM:field:Field.copySolution :: The destination must be a FieldType object');
            end
        end
        
        % Copy from 'source' to 'destination' (default source = current)
        function shiftTimeSolutions(s,numTimeLevels,extrapolationOrder)
            
            if nargin==3
                try
                    if ( isempty(s.oldTimeLevels) && numTimeLevels==1 )

                        if ( numTimeLevels==1 )
                            s.oldTimeLevels = cell(numTimeLevels,1);
                        else
                            error('The number of time levels should be equal to one here'); %#ok<CPROPLC>
                        end

                    else
                        for i = numTimeLevels:-1:2
                            s.oldTimeLevels{ i } = s.oldTimeLevels{ i-1 };
                            
                            % Check if oldTimeLevels{ i } needs a meshUpdate
                            if ( s.oldTimeLevels{ i }.needsMeshUpdate )
                                s.oldTimeLevels{ i }.meshUpdate();
                            end
                        end
                    end

                    s.oldTimeLevels{ 1 } = SubField(s,'oldTimeLevel',s.mesh,s.dh,0);

                    % Check if the 'current' subField needs a meshUpdate
                    s.meshUpdate;
%                     if (s.subFieldList{ 1 }.needsMeshUpdate)
%                         s.subFieldList{ 1 }.meshUpdate;
%                     end

                    s.oldTimeLevels{ 1 }.copy( s.subFieldList{1} ); 
                    
                    % Check if oldTimeLevels{1} needs a meshUpdate
                    if ( s.oldTimeLevels{ 1 }.needsMeshUpdate )
                        s.oldTimeLevels{ 1 }.meshUpdate();
                    end
                    
                    if (extrapolationOrder==2)
                        % extrapolate the solution of the 'n' and 'n-1' time levels to obtain 'n+1'
                        s.subFieldList{1}.extrapolateSolution( s.oldTimeLevels, extrapolationOrder ) 
                    end

                catch MExp
                    error('LSQDIM:field:Field.shiftTimeSolutions :: Fatal error');
                end
                
            else
                error('Should not be here: unsupported Field.shiftTimeSolutions')
                
                s.oldTimeLevels{ 1 } = SubField(s,'oldTimeLevel',s.mesh,s.dh,0);

                % Check if the 'current' subField needs a meshUpdate
                if (s.subFieldList{ 1 }.needsMeshUpdate)
                    s.subFieldList{ 1 }.meshUpdate;
                end

                s.oldTimeLevels{ 1 }.copy_add( s.subFieldList{1} );
                    
            end
        end
        
        % Return a requested subField, and initialize it if it does not
        % exist yet (but do not activate it)
        function out = subField(s,fieldType)
            if (isempty( s.subFieldList{fieldType} ))
                s.addSubField( fieldType, false );
            end
            out = s.subFieldList{fieldType};
        end
        
%         function copyCoupling(s)
%             if s.coupled.isInterpolated
%                 % Update the interpolated value
%                 s.coupled.meshUpdate();
%             else
%                 % Simple copy
%                 s.coupled.copy(s.current);
%             end
%         end
        
        function convertAlpha(s,type)
            if (type==1)
                % alphaArray --> alpha
                for e=1:s.mesh.numLocalElements
                    element = s.mesh.getLocalElement(e);
                    s.active.alpha{ e } = s.active.alphaArray( element.GMlocal );
                end
            else 
                % alpha --> alphaArray
                s.active.alphaArray = zeros(s.mesh.getNumDof,1);
                for e=1:s.mesh.numLocalElements
                    element = s.mesh.getLocalElement(e);
                    s.active.alphaArray( element.GMlocal ) = s.active.alpha{ e }; 
                end
            end
        end
        
        function moveSolution(s,elementList,direction)
            s.active.moveSolution(elementList,direction);
        end
        
        function copyBoundarySolution(s,srcElements,dstElements,direction,scaling)
            s.active.copyBoundarySolution(srcElements,dstElements,direction,scaling);
        end
        
        function copyBoundarySolutionValue(s,srcElements,dstElements,direction,scaling)
            s.active.copyBoundarySolutionValue(srcElements,dstElements,direction,scaling);
        end
        
%         function temporalExpansion(s)
%             for e=1:s.mesh.getNumLocalElements
%                 element = s.mesh.getLocalElement(e);
%                 solution = s.alphaArray( element.GM(:,:,1) );
%                 s.alpha{ e } = repmat(solution(:),size( element.GM,3),1);
%             end
%         end
        
        function plotValue(s)
            for e=s.mesh.eRange
                element = s.mesh.getElement(e);
                
                [X,Y] = element.getNodes;
                
                X = squeeze(X);
                Y = squeeze(Y);
                
                dataCurrent = reshape(s.active.value{e},element.finite.qSize);
%                 dataExact   = reshape(s.exact.getValue(Derivative.value,e),element.disc.Q);
                dataDiff    = dataCurrent;
%                 [X,Y] = meshgrid( element.xelem,element.yelem );
%                 if (layer==1)
                    surf(X(:,:,end),Y(:,:,end),dataDiff(:,:,end));
%                 elseif (layer==0)
%                     surf(X,Y,data{e}(:,:,1)');
%                 end
                hold on;
            end
                        
            colorbar;
            view([25,25]);
%             title(titleString);  
        end
                
        function plotComputedValue(s)
            data = s.current.computeValue;
            
            for e=1:s.mesh.numLocalElements
                element = s.mesh.getLocalElement(e);
                
                dataElement = reshape( data{e}, element.disc.Q );
                
%                 dataCurrent = reshape(s.current.getValue(Derivative.value,e),element.disc.Q);
%                 dataDiff    = dataCurrent-dataExact;
                [X,Y] = meshgrid( element.xelem,element.yelem );
%                 if (layer==1)
                    surf(X,Y,dataElement(:,:,5)');
%                 elseif (layer==0)
%                     surf(X,Y,data{e}(:,:,1)');
%                 end
                hold on;
            end
                        
            colorbar;
            view([25,25]);
%             title(titleString);  
        end
        
        function plotError(s,scaled)
            if (nargin==1)
                scaled = 1;
            end
            
            for e=1:s.mesh.numLocalElements
                element = s.mesh.getElement(e);
                dataCurrent = reshape(s.val(FieldType.Current,e),element.finite.qSize);
                dataExact   = reshape(s.val(FieldType.Exact,e),element.finite.qSize);
                dataDiff    = dataCurrent-dataExact;
                [X,Y,Z] = element.getNodes;
                surf(X,Y,scaled*dataDiff);
                hold on;
            end
                        
            colorbar;
            view([25,25]);
%             title(titleString);  
        end
        
        function plotErrorComputedValue(s)
            data = s.current.computeValue;
            
            for e=1:s.mesh.numLocalElements
                element = s.mesh.getLocalElement(e);
                dataCurrent = reshape( data{e}, element.disc.Q );
                dataExact   = reshape(s.exact.getValue(Derivative.value,e),element.disc.Q);
                dataDiff    = dataCurrent-dataExact;
                [X,Y] = meshgrid( element.xelem,element.yelem );
%                 if (layer==1)
                    surf(X,Y,dataDiff(:,:,5)');
%                 elseif (layer==0)
%                     surf(X,Y,data{e}(:,:,1)');
%                 end
                hold on;
            end
                        
            colorbar;
            view([25,25]);
%             title(titleString);  
        end
        
        function plotExact(s)
            s.subFieldList{4}.plot;
%             
%             for e=1:s.mesh.numLocalElements
%                 element = s.mesh.getLocalElement(e);
%                 dataExact   = reshape(s.subFieldList{4}.getValue(Derivative.value,e),element.finite.Q);
%                 [X,Y] = meshgrid( element.xelem,element.yelem );
% %                 if (layer==1)
%                     surf(X,Y,dataExact(:,:,5)');
% %                 elseif (layer==0)
% %                     surf(X,Y,data{e}(:,:,1)');
% %                 end
%                 hold on;
%             end
%                         
%             colorbar;
%             view([25,25]);
% %             title(titleString);  
        end
        
        function plotTheta(s)
                        
            clf;
            
            isSpaceTime = (System.settings.time.method==TimeMethod.SpaceTime);
            
            switch s.mesh.dim
                case 1
%                     for e=1:s.mesh.numLocalElements
%                         element = s.mesh.getLocalElement(e);
%                         X = element.getNodes;
%                         data = s.active.getValue(Derivative.x,e);
%                         line(X,data);
%                         hold on;
%                     end
%                     colorbar;
                    
                case 2            
                    for e=1:s.mesh.numLocalElements
                        element = s.mesh.getLocalElement(e);
                        [X,Y] = element.getNodes;
                        datax = reshape(s.active.getValue(Derivative.x,e),element.finite.qSize);
                        datay = reshape(s.active.getValue(Derivative.y,e),element.finite.qSize);
                        ratio = datay./datax;
                        surf(X,Y,180/pi * atan(ratio(:,:,end)));
                        hold on;
                    end
                    colorbar;
                    view([25,25]);
                    
                case 3
%                     if (isSpaceTime)
%                         for e=1:s.mesh.numLocalElements
%                             element = s.mesh.getLocalElement(e);
%                             [X,Y] = element.getNodes;
%                             data = reshape(s.active.getValue(Derivative.x,e),element.finite.qSize);
%                             surf(X(:,:,end),Y(:,:,end),data(:,:,end));
%                             hold on;
%                         end
%                         colorbar;
%                         view([25,25]);
%                     end
            end    

            %title(titleString);            
        end

        function plotTangentialInterfaceGradient(s)
                        
            clf;
            
            isSpaceTime = (System.settings.time.method==TimeMethod.SpaceTime);
            
            switch s.mesh.dim
                case 1
%                     for e=1:s.mesh.numLocalElements
%                         element = s.mesh.getLocalElement(e);
%                         X = element.getNodes;
%                         data = s.active.getValue(Derivative.x,e);
%                         line(X,data);
%                         hold on;
%                     end
%                     colorbar;
                    
                case 2            
                    for e=1:s.mesh.numLocalElements
                        element = s.mesh.getLocalElement(e);
                        [X,Y] = element.getNodes;
                        datax = reshape(s.active.getValue(Derivative.x,e),element.finite.qSize);
                        datay = reshape(s.active.getValue(Derivative.y,e),element.finite.qSize);
                        norm = sqrt( datax.^2 + datay.^2 );
                        nx = -datax ./ norm;
                        ny = -datay ./ norm;
                        dtang = -datax .* ny + datay .* nx;
                        surf(X,Y, dtang(:,:,end) );
%                         surf(X,Y, ny(:,:,end) );
                        hold on;
                    end
                    colorbar;
                    view([25,25]);
                    
                case 3
%                     if (isSpaceTime)
%                         for e=1:s.mesh.numLocalElements
%                             element = s.mesh.getLocalElement(e);
%                             [X,Y] = element.getNodes;
%                             data = reshape(s.active.getValue(Derivative.x,e),element.finite.qSize);
%                             surf(X(:,:,end),Y(:,:,end),data(:,:,end));
%                             hold on;
%                         end
%                         colorbar;
%                         view([25,25]);
%                     end
            end    

            %title(titleString);            
        end

        
        function plotx(s)
                        
            clf;
            
            isSpaceTime = (System.settings.time.method==TimeMethod.SpaceTime);
            
            switch s.mesh.dim
                case 1
                    for e=1:s.mesh.numLocalElements
                        element = s.mesh.getLocalElement(e);
                        X = element.getNodes;
                        data = s.active.getValue(Derivative.x,e);
                        line(X,data);
                        hold on;
                    end
                    colorbar;
                    
                case 2            
                    for e=1:s.mesh.numLocalElements
                        element = s.mesh.getLocalElement(e);
                        [X,Y] = element.getNodes;
                        data = reshape(s.active.getValue(Derivative.x,e),element.finite.qSize);
                        surf(X,Y,data(:,:,end));
                        hold on;
                    end
                    colorbar;
                    view([25,25]);
                    
                case 3
                    if (isSpaceTime)
                        for e=1:s.mesh.numLocalElements
                            element = s.mesh.getLocalElement(e);
                            [X,Y] = element.getNodes;
                            data = reshape(s.active.getValue(Derivative.x,e),element.finite.qSize);
                            surf(X(:,:,end),Y(:,:,end),data(:,:,end));
                            hold on;
                        end
                        colorbar;
                        view([25,25]);
                    end
            end    

            %title(titleString);            
        end
        
        function plott(s)
                        
            clf;
%             
%             if ( ~isa(s.active,'SubField') )
%                 s.setActive(s.current);
%             end
            
            switch s.mesh.dim
                case 1
                    for e=1:s.mesh.numLocalElements
                        element = s.mesh.getLocalElement(e);
                        X = element.getNodes;
                        data = s.active.getValue(Derivative.x,e);
                        line(X,data);
                        hold on;
                    end
                    colorbar;
                    
                case 2            
                    for e=1:s.mesh.numLocalElements
                        element = s.mesh.getLocalElement(e);
                        [X,Y] = element.getNodes;
                        data = reshape(s.active.getValue(Derivative.y,e),element.finite.qSize);
                        surf(X,Y,data(:,:,end));
                        hold on;
                    end
                    colorbar;
                    view([25,25]);
                    
                case 3
                    if (s.mesh.disc.spaceTime)
                        for e=1:s.mesh.numLocalElements
                            element = s.mesh.getLocalElement(e);
                            [X,Y] = element.getNodes;
                            data = reshape(s.active.getValue(Derivative.x,e),element.disc.Q);
                            surf(X(:,:,end),Y(:,:,end),data(:,:,end));
                            hold on;
                        end
                        colorbar;
                        view([25,25]);
                    end
            end    

            %title(titleString);            
        end
        
        function plotGradientNorm(s)          
            for e=1:s.mesh.numLocalElements
                element = s.mesh.getLocalElement(e);
                datax = reshape(s.active.getValue(Derivative.x,e),element.finite.qSize);
                datay = reshape(s.active.getValue(Derivative.y,e),element.finite.qSize);
                
                data = sqrt(datax.^2 + datay.^2);
                
                [X,Y] = element.getNodes;
                X = squeeze(X);
                Y = squeeze(Y);
                
%                 if (layer==1)
                    surf(X(:,:,end),Y(:,:,end),data(:,:,end));
%                 elseif (layer==0)
%                     surf(X,Y,data{e}(:,:,1)');
%                 end
                hold on;
            end
                        
            colorbar;
            view([25,25]);
%             title(titleString);            
        end
        
        function plotInterfaceForcingX(s)          
            for e=1:s.mesh.numLocalElements
                element = s.mesh.getLocalElement(e);
                value = reshape(s.current.getValue(Derivative.value,e),element.disc.Q);
                
                datax = reshape(s.current.getValue(Derivative.x,e),element.disc.Q);
                dataxx = reshape(s.current.getValue(Derivative.xx,e),element.disc.Q);
                datay = reshape(s.current.getValue(Derivative.y,e),element.disc.Q);
                dataz = reshape(s.current.getValue(Derivative.z,e),element.disc.Q);
                
                gradientNorm = (datax.^2 + datay.^2);
                
                %datax(value<0.1) = 0;
                
                data = gradientNorm;
%                 data = 1./data;
%                 data(data~=data) = 0;
%                 data(abs(data)>1000) = 0;

                plotValue = element.disc.Dz*( element.disc.H \ dataz(:) ) .* datax(:);
                
                plotValue = reshape(plotValue,size(dataz));
                                
                [X,Y] = meshgrid( element.xelem,element.yelem );
%                 if (layer==1)
%                     surf(X,Y,dataz(:,:,end)');
                    surf(X,Y,plotValue(:,:,end)');
                    %surf(X,Y,(dataz(:,:,end)./data(:,:,end))');
%                 elseif (layer==0)
%                     surf(X,Y,data{e}(:,:,1)');
%                 end
                hold on;
            end
                        
            colorbar;
            view([25,25]);
%             title(titleString);            
        end
                
        function plotxxdiff(s)
            dataAll = zeros(s.numDof,1);
            counter = zeros(s.numDof,1);
            for e=1:s.mesh.numLocalElements
                element = s.mesh.getLocalElement(e);
                data = s.xxDerivative(e);
                dataAll(element.getGM) = dataAll(element.getGM) + data;
                counter(element.getGM) = counter(element.getGM) + 1;
            end
            
            % Compute the averaged field
            dataAveragedAll = dataAll ./ counter;
                
            % Put the averaged field back to the elements
            dataAveraged = cell(s.mesh.numLocalElements,1);            
            for e=1:s.mesh.numLocalElements
                element = s.mesh.getLocalElement(e);
                dataAveraged{e} = dataAveragedAll(element.getGM);
            end
               
            % Plot the averaged field
            for e=1:s.mesh.numLocalElements
                data = reshape(dataAveraged{e},element.disc.Q);
                [X,Y] = meshgrid( element.xelem,element.yelem );
                surf(X,Y,data(:,:,end)');
                hold on;
            end
                        
            colorbar;
            view([25,25]);
        end
        
        function ploty(s)
            
            clf;
            
%             if ( ~isa(s.active,'SubField') )
%                 s.setActive(s.current);
%             end
            
            isSpaceTime = (System.settings.time.method==TimeMethod.SpaceTime);

            switch s.mesh.dim
                case 1
                    for e=1:s.mesh.numLocalElements
                        element = s.mesh.getLocalElement(e);
                        X = element.getNodes;
                        data = s.active.getValue(Derivative.y,e);
                        line(X,data);
                        hold on;
                    end
                    colorbar;
                    
                case 2            
                    for e=1:s.mesh.numLocalElements
                        element = s.mesh.getLocalElement(e);
                        [X,Y] = element.getNodes;
                        data = reshape(s.active.getValue(Derivative.y,e),element.finite.qSize);
                        surf(X,Y,data(:,:,end));
                        hold on;
                    end
                    colorbar;
                    view([25,25]);
                    
                case 3
                    if (isSpaceTime)
                        for e=1:s.mesh.numLocalElements
                            element = s.mesh.getLocalElement(e);
                            [X,Y] = element.getNodes;
                            data = reshape(s.active.getValue(Derivative.y,e),element.finite.qSize);
                            %data = s.active.valuey{e};
                            surf(X(:,:,end),Y(:,:,end),data(:,:,end));
                            hold on;
                        end
                        colorbar;
                        view([25,25]);
                    end
            end    
            
%             clf
%             
%             for e=1:s.numElements
%                 element = s.mesh.getLocalElement(e);
%                 
% %                 [X,Y] = element.getNodes;
%                 
%                     if (~isempty(element.xelem))
%                         [X,Y] = meshgrid( element.xelem,element.yelem );
%                     else
%                         if (s.mesh.dim==1)
%                             X = element.nodes{:};
%                         elseif (s.mesh.dim==2 && ~s.mesh.disc.spaceTime)
%                             [X,Y] = meshgrid( element.nodes{:} );
%                         elseif (s.mesh.dim==3 && s.mesh.disc.spaceTime )
%                             [X,Y] = meshgrid( element.nodes{1:end-1} );
%                         else
%                             error('fix this please')
%                             [X,Y] = meshgrid( element.nodes{:} );
%                         end
%                     end
%                 
%                 data = reshape(s.active.getValue(Derivative.y,e),element.disc.Q);
%                 %[X,Y] = meshgrid( element.xelem,element.yelem );
% %                 if (layer==1)
%                     surf(X,Y,data(:,:,end)');
% %                 elseif (layer==0)
% %                     surf(X,Y,data{e}(:,:,1)');
% %                 end
%                 hold on;
%             end
%                         
%             colorbar;
%             view([25,25]);
% %             title(titleString);            
        end
                
        function plotxx(s)
            s.plotFunction( Derivative.xx );
        end
        
        function plotyy(s)
            s.plotFunction( Derivative.yy );
        end
        
        function plotxy(s)
            s.plotFunction( Derivative.xy );
        end
        
        function plotxxx(s)
            s.plotFunction( Derivative.xxx );
        end
        
        function plotyyy(s)
            s.plotFunction( Derivative.yyy );
        end
        
        function plotFunction(s,varargin)
            clf;
            
            default = {Derivative.value};
            [ selected ] = VerifyInput(varargin,default);
            
            if ( ~isa(s.active,'SubField') )
                s.setActive(FieldType.Current);
            end
            
            switch s.mesh.dim
                case 1
                    for e=1:s.mesh.numLocalElements
                        element = s.mesh.getLocalElement(e);
                        X       = element.getNodes;
                        data    = s.active.getValue(selected,e);
                        line(X,data);
                        hold on;
                    end
                    colorbar;
                    
                case 2
                    for e=1:s.mesh.numLocalElements
                        element = s.mesh.getLocalElement(e);
                        [X,Y] = element.getNodes;

                        data = reshape(s.active.getValue(selected,e),element.finite.Q);

        %                 if (layer==1)
                            surf(X,Y,data(:,:,end));
        %                 elseif (layer==0)
        %                     surf(X,Y,data{e}(:,:,1)');
        %                 end
                        hold on;
                    end                    
                    colorbar;
                    view([25,25]);
                    
                case 3
                    if (s.mesh.disc.spaceTime)
                        for e=1:s.mesh.numLocalElements
                            element = s.mesh.getLocalElement(e);
                            [X,Y] = element.getNodes;
                            data = reshape(s.active.getValue(selected,e),element.finite.Q);
                            surf(X(:,:,end),Y(:,:,end),data(:,:,end));
                            hold on;
                        end
                        colorbar;
                        view([25,25]);
                    end
            end

            %title(titleString);  
        end
        
        function plotxxy(s)          
            for e=1:s.numElements
                element = s.mesh.getLocalElement(e);
                data = reshape(s.xxyDerivative(e),element.disc.Q);
                [X,Y] = meshgrid( element.xelem,element.yelem );
%                 if (layer==1)
                    surf(X,Y,(data(:,:,end))');
%                 elseif (layer==0)
%                     surf(X,Y,data{e}(:,:,1)');
%                 end
                hold on;
            end
                        
            colorbar;
            view([25,25]);
%             title(titleString);            
        end
        
        function plotyyx(s)          
            for e=1:s.numElements
                element = s.mesh.getLocalElement(e);
                data = reshape(s.yyxDerivative(e),element.disc.Q);
                [X,Y] = meshgrid( element.xelem,element.yelem );
%                 if (layer==1)
                    surf(X,Y,(data(:,:,end))');
%                 elseif (layer==0)
%                     surf(X,Y,data{e}(:,:,1)');
%                 end
                hold on;
            end
                        
            colorbar;
            view([25,25]);
%             title(titleString);            
        end
        
        function plotxyx(s)          
            for e=1:s.numElements
                element = s.mesh.getLocalElement(e);
                data = reshape(s.xyxDerivative(e),element.disc.Q);
                [X,Y] = meshgrid( element.xelem,element.yelem );
%                 if (layer==1)
                    surf(X,Y,(data(:,:,end))');
%                 elseif (layer==0)
%                     surf(X,Y,data{e}(:,:,1)');
%                 end
                hold on;
            end
                        
            colorbar;
            view([25,25]);
%             title(titleString);            
        end
        
        function plotz(s,nRange)
            if nargin==1
                nRange = 5;
            end
            count = 0;
            for i=nRange
                count = count+1;
               	subplot(numel(nRange),1,count);
                for e=1:s.mesh.numLocalElements
                    element = s.mesh.getLocalElement(e);
                    data = reshape(s.current.getValue(Derivative.z,e),element.disc.Q);
                    [X,Y] = meshgrid( element.xelem,element.yelem );
    %                 if (layer==1)
                        surf(X,Y,data(:,:,i)');
    %                 elseif (layer==0)
    %                     surf(X,Y,data{e}(:,:,1)');
    %                 end
                    hold on;
                end

                colorbar;
                view([120,15]);
%             title(titleString);            
            end
        end
        
        function plotLaplacian(s)
            clf
            
            for e=1:s.mesh.numElements
                element = s.mesh.getLocalElement(e);
                data1   = reshape(s.active.getValue(Derivative.xx,e),element.finite.qSize);
                data2   = reshape(s.active.getValue(Derivative.yy,e),element.finite.qSize);
%                 data_xy = reshape(s.active.getValue(Derivative.xy,e),element.finite.qSize);
                [X,Y] = element.getNodes;
                
                X = squeeze(X);
                Y = squeeze(Y);
                
                if (s.mesh.sDim==s.mesh.dim)
                    surf(X,Y, (data1+data2) );
                else
                    surf(X(:,:,end),Y(:,:,end),( data1(:,:,end) + data2(:,:,end) ));            % final time slice
                end
                hold on;
            end
                        
            x0 = s.mesh.X(1,1);
            x1 = s.mesh.X(2,1);
            y0 = s.mesh.X(1,2);
            y1 = s.mesh.X(2,2);

            pbx = x1 - x0;
            pby = y1 - y0;
            pbaspect([pbx pby 1])
            
            colorbar;
            view([25,25]);
%             title(titleString);            
        end
        
        function plotDirectional2nd(s,scale)
            % Function to plot the directional 2nd derivative (used
            % for C field in Cahn-Hilliard). This is function is only for 
            % 2D fields.
            
            if nargin == 1
                scale = 1;
            end
            
            for e=1:s.mesh.numElements
                element = s.mesh.getLocalElement(e);
                
                dx  = reshape(s.active.getValue(Derivative.x,e),element.finite.qSize);
                dy  = reshape(s.active.getValue(Derivative.y,e),element.finite.qSize);
                dxx = reshape(s.active.getValue(Derivative.xx,e),element.finite.qSize);
                dyy = reshape(s.active.getValue(Derivative.yy,e),element.finite.qSize);
                dxy = reshape(s.active.getValue(Derivative.xy,e),element.finite.qSize);
                
                % 3D
%                 dz  = zeros( size(dx) );
%                 dxz = zeros( size(dxy) );
%                 dyz = zeros( size(dxy) );
%                 dzz = zeros( size(dxx) );
                
                norm2 = dx.^2 + dy.^2;              % 2D
%                 norm2 = dx.^2 + dy.^2 + dz.^2;      % 3D
                
                [X,Y] = element.getNodes;
                
                X = squeeze(X);
                Y = squeeze(Y);

                % 2D
                values = scale * (dx.^2.*dxx + 2*dx.*dy.*dxy + dy.^2.*dyy) ./ norm2;
                
                % 3D
%                 values = ( dx.^2 .* dxx + dy.^2.*dyy + dz.^2.*dzz + 2*dx.*dy.*dxy + 2*dx.*dz.*dxz + 2*dy.*dz.*dyz )./ norm2;
                
                if (s.mesh.sDim==s.mesh.dim)
                    surf(X,Y, values );
                else
%                     surf(X(:,:,end),Y(:,:,end),( data1(:,:,end) + data2(:,:,end) ));            % final time slice
                end
                hold on;
            end
                        
            x0 = s.mesh.X(1,1);
            x1 = s.mesh.X(2,1);
            y0 = s.mesh.X(1,2);
            y1 = s.mesh.X(2,2);

            pbx = x1 - x0;
            pby = y1 - y0;
            pbaspect([pbx pby 1])
            
            colorbar;
            view([25,25]);
%             title(titleString);            
        end
        
        function plotDirectional2ndTangential(s,scale)
            % Function to plot the directional 2nd derivative (used
            % for C field in Cahn-Hilliard). This is function is only for 
            % 2D fields.
            
            if nargin == 1
                scale = 1;
            end
            
            for e=1:s.mesh.numElements
                element = s.mesh.getLocalElement(e);
                
                dx  = reshape(s.active.getValue(Derivative.x,e),element.finite.qSize);
                dy  = reshape(s.active.getValue(Derivative.y,e),element.finite.qSize);
                dxx = reshape(s.active.getValue(Derivative.xx,e),element.finite.qSize);
                dyy = reshape(s.active.getValue(Derivative.yy,e),element.finite.qSize);
                dxy = reshape(s.active.getValue(Derivative.xy,e),element.finite.qSize);
                
                % 3D
%                 dz  = zeros( size(dx) );
%                 dxz = zeros( size(dxy) );
%                 dyz = zeros( size(dxy) );
%                 dzz = zeros( size(dxx) );
                
                norm2 = dx.^2 + dy.^2;              % 2D
%                 norm2 = dx.^2 + dy.^2 + dz.^2;      % 3D
                
                [X,Y] = element.getNodes;
                
                X = squeeze(X);
                Y = squeeze(Y);

                % 2D
                values = scale * (dy.^2.*dxx - 2*dx.*dy.*dxy + dx.^2.*dyy) ./ norm2;
                
                % 3D
%                 values = ( dx.^2 .* dxx + dy.^2.*dyy + dz.^2.*dzz + 2*dx.*dy.*dxy + 2*dx.*dz.*dxz + 2*dy.*dz.*dyz )./ norm2;
                
                if (s.mesh.sDim==s.mesh.dim)
                    surf(X,Y, values );
                else
%                     surf(X(:,:,end),Y(:,:,end),( data1(:,:,end) + data2(:,:,end) ));            % final time slice
                end
                hold on;
            end
                        
            x0 = s.mesh.X(1,1);
            x1 = s.mesh.X(2,1);
            y0 = s.mesh.X(1,2);
            y1 = s.mesh.X(2,2);

            pbx = x1 - x0;
            pby = y1 - y0;
            pbaspect([pbx pby 1])
            
            colorbar;
            view([25,25]);
%             title(titleString);            
        end
        
        function plotLaplacianDelta(s,scale)
            % Function to plot the directional 2nd derivative (used
            % for C field in Cahn-Hilliard). This is function is only for 
            % 2D fields.
            
            if nargin == 1
                scale = 1;
            end
            
            for e=1:s.mesh.numElements
                element = s.mesh.getLocalElement(e);
                
                dx  = reshape(s.active.getValue(Derivative.x,e),element.finite.qSize);
                dy  = reshape(s.active.getValue(Derivative.y,e),element.finite.qSize);
                dxx = reshape(s.active.getValue(Derivative.xx,e),element.finite.qSize);
                dyy = reshape(s.active.getValue(Derivative.yy,e),element.finite.qSize);
                dxy = reshape(s.active.getValue(Derivative.xy,e),element.finite.qSize);
                
                % 3D
%                 dz  = zeros( size(dx) );
%                 dxz = zeros( size(dxy) );
%                 dyz = zeros( size(dxy) );
%                 dzz = zeros( size(dxx) );
                
                norm2 = dx.^2 + dy.^2;              % 2D
%                 norm2 = dx.^2 + dy.^2 + dz.^2;      % 3D
                
                [X,Y] = element.getNodes;
                
                X = squeeze(X);
                Y = squeeze(Y);

                % 2D
                values_tt  = scale * (dy.^2.*dxx - 2*dx.*dy.*dxy + dx.^2.*dyy) ./ norm2;
                values_nn  = scale * (dx.^2.*dxx + 2*dx.*dy.*dxy + dy.^2.*dyy) ./ norm2;
                values_lap = scale * (dxx + dyy);
                
                values = values_lap - values_nn - values_tt;
                
                % 3D
%                 values = ( dx.^2 .* dxx + dy.^2.*dyy + dz.^2.*dzz + 2*dx.*dy.*dxy + 2*dx.*dz.*dxz + 2*dy.*dz.*dyz )./ norm2;
                
                if (s.mesh.sDim==s.mesh.dim)
                    surf(X,Y, values );
                else
%                     surf(X(:,:,end),Y(:,:,end),( data1(:,:,end) + data2(:,:,end) ));            % final time slice
                end
                hold on;
            end
                        
            x0 = s.mesh.X(1,1);
            x1 = s.mesh.X(2,1);
            y0 = s.mesh.X(1,2);
            y1 = s.mesh.X(2,2);

            pbx = x1 - x0;
            pby = y1 - y0;
            pbaspect([pbx pby 1])
            
            colorbar;
            view([25,25]);
%             title(titleString);            
        end
       
        function plotSurfaceTension(s)
            % Function to plot the directional 2nd derivative (used
            % for C field in Cahn-Hilliard). This is function is only for 
            % 2D fields.
            
%             s.xx2_av_init;
%             s.yy2_av_init;
            
            for e=1:s.mesh.numElements
                element = s.mesh.getLocalElement(e);
                
                dx  = reshape(s.active.getValue(Derivative.x,e),element.finite.qSize);
                dy  = reshape(s.active.getValue(Derivative.y,e),element.finite.qSize);
                
                dxx = reshape(s.active.getValue(Derivative.xx,e),element.finite.qSize);
                dyy = reshape(s.active.getValue(Derivative.yy,e),element.finite.qSize);
                dxy = reshape(s.active.getValue(Derivative.xy,e),element.finite.qSize);
                
%                 cxx = s.xx2_mean(e);
%                 cyy = s.yy2_mean(e);
%                 dxx = reshape(cxx,element.finite.qSize);
%                 dyy = reshape(cyy,element.finite.qSize);
                
                % 3D
%                 dz  = zeros( size(dx) );
%                 dxz = zeros( size(dxy) );
%                 dyz = zeros( size(dxy) );
%                 dzz = zeros( size(dxx) );
                
                norm2 = dx.^2 + dy.^2;              % 2D
%                 norm2 = dx.^2 + dy.^2 + dz.^2;      % 3D
                
                [X,Y] = element.getNodes;
                
                X = squeeze(X);
                Y = squeeze(Y);

                % 2D
                values = -( (dxx + 0*dxy.^2 + dyy) - (dx.^2.*dxx + 2*dx.*dy.*dxy + dy.^2.*dyy) ./ norm2 );
                
                % 3D
%                 values = ( dx.^2 .* dxx + dy.^2.*dyy + dz.^2.*dzz + 2*dx.*dy.*dxy + 2*dx.*dz.*dxz + 2*dy.*dz.*dyz )./ norm2;
                
                if (s.mesh.sDim==s.mesh.dim)
                    surf(X,Y, values );
                else
%                     surf(X(:,:,end),Y(:,:,end),( data1(:,:,end) + data2(:,:,end) ));            % final time slice
                end
                hold on;
            end
                        
            x0 = s.mesh.X(1,1);
            x1 = s.mesh.X(2,1);
            y0 = s.mesh.X(1,2);
            y1 = s.mesh.X(2,2);

            pbx = x1 - x0;
            pby = y1 - y0;
            pbaspect([pbx pby 1])
            
            colorbar;
            view([25,25]);
%             title(titleString);    
        end
        
        % Only useful for cField
        function plotPotential(s,Cn)
            for e=1:s.mesh.numElements
                element = s.mesh.getLocalElement(e);
                c       = s.val2(e,s.mesh);
                cx      = s.x2(e,s.mesh);
                cy      = s.y2(e,s.mesh);
                cxx     = s.xx2(e,s.mesh);
                cxy     = s.xy2(e,s.mesh);
                cyy     = s.yy2(e,s.mesh);
                [X,Y]   = element.getNodes;
                
                cnn = ( cx.^2 .* cxx + 2*cx.*cy.*cxy + cy.^2 .* cyy ) ./ (cx.^2+cy.^2);
                
                alp = 6*sqrt(2);
                
                data = alp/Cn * ( 1*(c.^3-3/2*c.^2+1/2*c) - 1*Cn^2*(cnn) );
                
                data = reshape( data, size(X) );
                
%                 if (layer==1)
                    surf(X,Y,data);
%                 elseif (layer==0)
%                     surf(X,Y,data{e}(:,:,1)');
%                 end
                hold on;
            end
                        
            colorbar;
            view([25,25]);
%             title(titleString);
        end
        
        
%         function out = y(f)
%             out = diag( f.activeElement.Dy * f.getAlphaRelax );
%         end    

        function zero(s)
            for j=1:numel(s.nonlinear)
                for i=1:numel(s.nonlinear{j}.alpha)
                    s.nonlinear{j}.alpha{i} = zeros(size(s.nonlinear{j}.alpha{i}));
                    s.nonlinear{j}.value{i} = zeros(size(s.nonlinear{j}.value{i}));
                end
            end
            
            for j=1:numel(s.coupled)
                for i=1:numel(s.coupled{j}.alpha)
                    s.coupled{j}.alpha{i} = zeros(size(s.coupled{j}.alpha{i}));
                    s.coupled{j}.value{i} = zeros(size(s.coupled{j}.value{i}));
                end
            end
            
            %cellfun(@(x) 0*x,s.current.alpha,'UniformOutput',false);
            %cellfun(@(x) 0*x,s.current.value,'UniformOutput',false);
            
            s.nonlinear{1}.convertAlpha(0);
        end
        
        function reset(s)
            
            for i=1:numel(s.subFieldList)
                if ~isempty(s.subFieldList{i})
                    s.subFieldList{i}.reset;
                end
            end
            
            s.prescribed = cell(s.mesh.numLocalElements,1);
            id = 0;
            for e=s.mesh.eRange
                id = id+1;
                element = s.mesh.getElement(e);
                s.prescribed{ id } = false( element.finite.dSize );
            end
        end
        
        function setValue(s,element,elementalValueArray)
            if (~isempty(element))
                if (numel(s.active.value)~=s.mesh.numLocalElements)
                    s.active.reset;
                end
                if (numel(elementalValueArray)==1)
                    s.active.value{element} = elementalValueArray*ones( size(s.active.value{element}) );
                else
                    s.active.value{element} = reshape( elementalValueArray, size(s.active.value{element}) );
                end
            else
                for e=1:numel(elementalValueArray)
                    s.setValue( e, elementalValueArray{e} );
                end
            end
        end
        
        % Function to reset the value cell array to zero
        function resetValue(s)
            s.numElements = s.mesh.numLocalElements;
            
            s.value = cell(s.numElements,1);            
            for e=1:s.numElements
                element = s.mesh.getLocalElement(e);
                s.value{e} = zeros(element.disc.dofeq);
            end
            
            s.isEmpty = true;
        end
        
        function out = alpha(s,e,fieldType)
            if nargin==1
                e = [];
            end
            if nargin<3
                fieldType = [];
            end
            
%             subField = s.getSubField(fieldType);
%             out = subField.alp(e);
            
            out = s.active.alp(e);
        end
        
        function out = alp(s,fieldType,e)
            out = s.subFieldList{ fieldType }.alp( e );
        end
        
        function out = value(s,e,fieldType,dim)
%             if nargin==1
%                 e   = [];
%                 dim = 1;
%             end
%             if nargin<3
%                 fieldType = [];
%             end
            
            %subField = s.getSubField(fieldType);
            %out = subField.val(e,dim);
            
            %if (numel(s.subFieldList)==8)
            %try
                out = s.active.val;
            %else
            %    error('Please specify the subField you want')
            %end
        end
        
        function unifyMesh(s)
            for i=numel(s.subFieldList)
                if (~isempty(s.subFieldList{i}))
                    if (s.subFieldList{i}.needsMeshUpdate)
                        s.subFieldList{i}.meshUpdate;
                    end
                end
            end
        end
        
        function out = val(s,subFieldName,e)
            
            % NB: 'e' is a global element number
            
            % Ensure the idx is a valid FieldType
            try
                if (isa(subFieldName,'FieldType'))
                    idx = subFieldName;
                else
                    idx = FieldType.(subFieldName);
                end
            catch 
                error('%s is not a valid FieldType',subFieldName); %#ok<CPROPLC>
            end
            
            %idx = s.getSubFieldNameIndex(subFieldName);            
            selectedSubField = s.subFieldList{idx};
            
            if (selectedSubField.needsMeshUpdate)
                selectedSubField.meshUpdate();
            end
            
            try
            if (iscell(selectedSubField))                
                out = [];            
                for i=1:numel(selectedSubField)
                    out = out + selectedSubField{i}.val(e); %+ selectedSubField{i}.weight;
                end            
            else
                out = selectedSubField.val(e);
            end
            catch MExp
                save(['field.val.' int2str(System.rank) '.mat']);
                MExp
            end
        end
        

        
        function out = oldVal(s,e,mesh,timeLevel)
            
            if (timeLevel==0)
                selectedSubField = s.subFieldList{1};                       % Get the solution of the current time level
            else
                if ~isempty( s.oldTimeLevels )
                    if (timeLevel<=numel(s.oldTimeLevels))
                        selectedSubField = s.oldTimeLevels{timeLevel};              % Get the solution at a certain timeLevel
                    else
                        out = [];
                        return
                    end
                else
                    out = [];
                    return
                end
            end
            
            % Interpolate if the meshes differ
            if ( isequal(selectedSubField.meshID,s.mesh.Lv) )
                % no interpolation required
                
            elseif (isequal(selectedSubField.mesh.Lv,s.mesh.Lv))
                % The mesh of this Field is identical to the mesh of the
                % selectedSubField, although the meshIDs differ. The
                % selectedSubField needs to update the mesh
                if ( selectedSubField.needsMeshUpdate )
                    selectedSubField.meshUpdate();
                end
                
            else
                % interpolate
                error('Unsupported request :: interpolation for Fields needs to be implemented')
%                 % verify that the interpolate subField exists
%                 if ( isempty(s.subFieldList{ FieldType.Interpolated }) )
%                     s.addSubField(FieldType.Interpolated,false,mesh);
%                     out = s.subFieldList{ FieldType.Interpolated };
%                     out.setSubField( s.subFieldList{ subFieldName } );
%                 else
%                     out = s.subFieldList{ FieldType.Interpolated };
%                     out.mesh = mesh;
%                 end
% 
%                 if (out.needsMeshUpdate)
%                     out.meshUpdate();
%                 end
            end
            
            % NB: 'e' is a global element number            
            out = selectedSubField.val(e);
        end
        
        function out = oldVal_x(s,e,mesh,timeLevel)
            
            selectedSubField = s.oldTimeLevels{timeLevel};
            
            % Interpolate if the meshes differ
            if ( isequal(selectedSubField.meshID,s.mesh.Lv) )
                % no interpolation required
            else
                % interpolate
                error('Unsupported request :: interpolation for Fields needs to be implemented')
            end
            
            % NB: 'e' is a global element number            
            out = selectedSubField.x(e);
        end
        
        function out = oldVal_y(s,e,mesh,timeLevel)
            
            selectedSubField = s.oldTimeLevels{timeLevel};
            
            % Interpolate if the meshes differ
            if ( isequal(selectedSubField.meshID,s.mesh.Lv) )
                % no interpolation required
            else
                % interpolate
                error('Unsupported request :: interpolation for Fields needs to be implemented')
            end
            
            % NB: 'e' is a global element number            
            out = selectedSubField.y(e);
        end
        
        function out = oldVal_z(s,e,mesh,timeLevel)
            
            selectedSubField = s.oldTimeLevels{timeLevel};
            
            % Interpolate if the meshes differ
            if ( isequal(selectedSubField.meshID,s.mesh.Lv) )
                % no interpolation required
            else
                % interpolate
                error('Unsupported request :: interpolation for Fields needs to be implemented')
            end
            
            % NB: 'e' is a global element number            
            out = selectedSubField.z(e);
        end
        
        function out = val2(s,e,varargin)
            % NB: 'e' is a global element number
            if (e==1)
                selectedSubField = s.selectInterpolatedField(varargin{:});
            else
                selectedSubField = s.active;
            end
            out = selectedSubField.val(e);
        end
        
        function out = x2(s,e,varargin)            
            % NB: 'e' is a global element number 
            if (e==1)
                selectedSubField = s.selectInterpolatedField(varargin{:});
            else
                selectedSubField = s.active;
            end
            out = selectedSubField.x(e);
        end
        
        function out = y2(s,e,varargin)
            % NB: 'e' is a global element number
            if (e==1)
                selectedSubField = s.selectInterpolatedField(varargin{:});
            else
                selectedSubField = s.active;
            end
            out = selectedSubField.y(e);
        end
        
        function out = z2(s,e,varargin)
            % NB: 'e' is a global element number   
            if (e==1)
                selectedSubField = s.selectInterpolatedField(varargin{:});
            else
                selectedSubField = s.active;
            end
            out = selectedSubField.z(e);
        end
        
        function out = t2(s,e,varargin)
            % NB: 'e' is a global element number 
            if (e==1)
                selectedSubField = s.selectInterpolatedField(varargin{:});
            else
                selectedSubField = s.active;
            end
            out = selectedSubField.t(e);
        end
        
        function out = nn2(s,e,varargin)
            % NB: 'e' is a global element number 
            if (e==1)
                selectedSubField = s.selectInterpolatedField(varargin{:});
            else
                selectedSubField = s.active;
            end
            out = selectedSubField.nn(e);
        end
        
        function out = xx2(s,e,varargin)
            % NB: 'e' is a global element number 
            if (e==1)
                selectedSubField = s.selectInterpolatedField(varargin{:});
            else
                selectedSubField = s.active;
            end
            out = selectedSubField.xx(e);
        end
        
        function out = yy2(s,e,varargin)
            % NB: 'e' is a global element number   
            if (e==1)
                selectedSubField = s.selectInterpolatedField(varargin{:});
            else
                selectedSubField = s.active;
            end
            out = selectedSubField.yy(e);
        end
        
        function out = zz2(s,e,varargin)
            % NB: 'e' is a global element number
            if (e==1)
                selectedSubField = s.selectInterpolatedField(varargin{:});
            else
                selectedSubField = s.active;
            end
            out = selectedSubField.zz(e);
        end
        
        function out = xy2(s,e,varargin)
            % NB: 'e' is a global element number
            if (e==1)
                selectedSubField = s.selectInterpolatedField(varargin{:});
            else
                selectedSubField = s.active;
            end
            out = selectedSubField.xy(e);
        end
        
        function out = xz2(s,e,varargin)
            % NB: 'e' is a global element number
            if (e==1)
                selectedSubField = s.selectInterpolatedField(varargin{:});
            else
                selectedSubField = s.active;
            end
            out = selectedSubField.xz(e);
        end
        
        function out = yz2(s,e,varargin)
            % NB: 'e' is a global element number
            if (e==1)
                selectedSubField = s.selectInterpolatedField(varargin{:});
            else
                selectedSubField = s.active;
            end
            out = selectedSubField.yz(e);
        end
        
        function out = selectInterpolatedField(s,varargin)
            % Function to select a specific subField, and interpolate that
            % subField on a specific mesh.
            %
            % varargin = [ Mesh, FieldType ]
            %
            % If no subFieldType is provided, the active subField is used. 
            % If no mesh is not provided, the mesh of the Field is used.
            defaultValues = {s.mesh,s.active.type};
            [mesh_in,subField_in] = VerifyInput(varargin,defaultValues);                % set the correct defaults
            
            s.meshUpdate; % verify if this field needs to be updated (it will update the prescribed array too if that's the case)
            
            if (s.subFieldList{ subField_in }.needsMeshUpdate)
                s.subFieldList{ subField_in }.meshUpdate;
            end
            
            % Assume FieldType.Current, but change to FieldType.Interpolated 
            % if the mesh is different wrt the s.mesh
            if ( isequal(s.subFieldList{ subField_in }.meshID,mesh_in.Lv) )
                out = s.subFieldList{ subField_in };                                   % no interpolation required
            else
                % interpolate
                % verify that the interpolate subField exists
                if ( isempty(s.subFieldList{ FieldType.Interpolated }) )
                    s.addSubField(FieldType.Interpolated,false,mesh_in);
                    out = s.subFieldList{ FieldType.Interpolated };
                    out.setSubField( s.subFieldList{ subField_in } );
                else
                    out = s.subFieldList{ FieldType.Interpolated };
                    out.mesh = mesh_in;
                end

                if (out.needsMeshUpdate)
                    out.meshUpdate();                                                   % perform the interpolation
                end
            end
        end
        
        function xx2_av_init(s,varargin)
            if (s.useMean)
                s.numElements = s.mesh.numLocalElements;

                s.xx2_av = cell(s.numElements,1);

                s.computeAlpha;     % required if only values are set

                selectedSubField = s.selectInterpolatedField(varargin{:}); 

                % fill xx2_av with local values
                for e=s.mesh.eRange
                    element = s.mesh.getElement( e );
                    s.xx2_av{e}   = reshape( selectedSubField.xx(e),element.finite.qSize);
                end

                qDofs = s.dh.qdofs;

                avValues = zeros(s.dh.numqDof,1);
                avCount  = zeros(s.dh.numqDof,1);

                % average with neighbors (checkerboard pattern)
                for e=s.mesh.eRange
                    element = s.mesh.getElement( e );

                    avValues( qDofs{e}(:) ) = avValues( qDofs{e}(:) ) + s.xx2_av{e}(:);

                    avCount( qDofs{e}(:) ) = avCount( qDofs{e}(:) ) + 1;
                end

                % Compute the updated mean
                for e=s.mesh.eRange
                    values = avValues( qDofs{e}(:) ) ./ avCount( qDofs{e}(:) );
                    s.xx2_av{e} = reshape( values,element.finite.qSize);
                end
            end
        end
        
        function yy2_av_init(s,varargin)
            if (s.useMean)
                s.numElements = s.mesh.numLocalElements;

                s.yy2_av = cell(s.numElements,1);

                s.computeAlpha;     % required if only values are set

                selectedSubField = s.selectInterpolatedField(varargin{:}); 

                % fill xx2_av with local values
                for e=s.mesh.eRange
                    element = s.mesh.getElement( e );
                    s.yy2_av{e}   = reshape( selectedSubField.yy(e),element.finite.qSize);
                end

                qDofs = s.dh.qdofs;

                avValues = zeros(s.dh.numqDof,1);
                avCount  = zeros(s.dh.numqDof,1);

                % average with neighbors (checkerboard pattern)
                for e=s.mesh.eRange
                    element = s.mesh.getElement( e );

                    avValues( qDofs{e}(:) ) = avValues( qDofs{e}(:) ) + s.yy2_av{e}(:);

                    avCount( qDofs{e}(:) ) = avCount( qDofs{e}(:) ) + 1;
                end

                % Compute the updated mean
                for e=s.mesh.eRange
                    values = avValues( qDofs{e}(:) ) ./ avCount( qDofs{e}(:) );
                    s.yy2_av{e} = reshape( values,element.finite.qSize);
                end
            end
        end
        
        function xx2_mean_plot(s)
                        
            id = 0;
            for e=s.mesh.eRange %1:numel(s.alpha)
                id = id+1;
                element = s.mesh.getElement(e);
                
                data = s.xx2_mean( e );
                
                [X,Y,Z,T] = element.getNodes();
                X = squeeze(X); Y = squeeze(Y);
                %Z = squeeze(Z); T = squeeze(T);
                
                plotData = reshape( data, size(X) );
              	surf(X,Y,plotData,'FaceColor','interp');
                
                hold on;
            end

            x0 = s.mesh.X(1,1);
            x1 = s.mesh.X(2,1);
            y0 = s.mesh.X(1,2);
            y1 = s.mesh.X(2,2);

            pbx = x1 - x0;
            pby = y1 - y0;
            pbaspect([pbx pby 1])

            colorbar;
            view([25,25]);
        end
        
        function yy2_mean_plot(s)
                        
            id = 0;
            for e=s.mesh.eRange %1:numel(s.alpha)
                id = id+1;
                element = s.mesh.getElement(e);
                
                data = s.yy2_mean( e );
                
                [X,Y,Z,T] = element.getNodes();
                X = squeeze(X); Y = squeeze(Y);
                %Z = squeeze(Z); T = squeeze(T);
                
                plotData = data;
              	surf(X,Y,plotData,'FaceColor','interp');
                
                hold on;
            end

            x0 = s.mesh.X(1,1);
            x1 = s.mesh.X(2,1);
            y0 = s.mesh.X(1,2);
            y1 = s.mesh.X(2,2);

            pbx = x1 - x0;
            pby = y1 - y0;
            pbaspect([pbx pby 1])

            colorbar;
            view([25,25]);
        end
        
        function out = xx2_mean(s,e)
            if (s.useMean)
                out = s.xx2_av{ e }(:);
            else
                out = s.xx2( e );
            end
        end
        
        function out = yy2_mean(s,e)
            if (s.useMean)
                out = s.yy2_av{ e }(:);
            else
                out = s.yy2( e );
            end
        end
        
        function out = x(s,subFieldName,e,level)
%             if nargin==1
%                 e = [];
%             end
%             if nargin<3
%                 out = s.current.x(e);
%             else
%                 subField = s.getSubField(fieldType);
%                 out = subField.x(e);
%             end


            % NB: 'e' is a global element number
            
            % Ensure the idx is a valid FieldType
            try
                if (isa(subFieldName,'FieldType'))
                    idx = subFieldName;
                else
                    idx = FieldType.(subFieldName);
                end
            catch 
                error('%s is not a valid FieldType',subFieldName); %#ok<CPROPLC>
            end
            
            %idx = s.getSubFieldNameIndex(subFieldName);            
            selectedSubField = s.subFieldList{idx};
            
            if (selectedSubField.needsMeshUpdate)
                selectedSubField.meshUpdate();
            end
            
            
            
            try
            if (iscell(selectedSubField))                
                out = [];            
                for i=1:numel(selectedSubField)
                    out = out + selectedSubField{i}.x(e); % + selectedSubField{i}.weight;
                end            
            else
                out = selectedSubField.x(e);
            end
            catch MExp
                save(['field.val.' int2str(System.rank) '.mat']);
                MExp
            end
            
            

%             switch s.activeType
%                 case FieldType.Current
%                     out = s.nonlinear{1}.x(e);
%                 case FieldType.Nonlinear
%                     out = 0;
%                     for i=1:numel(s.nWeight)
%                         out = out + s.nWeight(i) * s.nonlinear{i}.x(e);
%                     end
%                 case FieldType.Coupled
%                     out = 0;
%                     for i=1:numel(s.cWeight)
%                         out = out + s.cWeight(i) * s.coupled{i}.x(e);
%                     end
%                 case FieldType.TimeLevel
%                     out = s.timeLevel{level}.x(e);
%             end
        end
        
        function out = y(s,subFieldName,e)
            % NB: 'e' is a global element number
            
            % Ensure the idx is a valid FieldType
            if (isa(subFieldName,'FieldType'))
                idx = subFieldName;
            else
                idx = FieldType.(subFieldName);
            end
                        
            selectedSubField = s.subFieldList{idx};
            
            if (selectedSubField.needsMeshUpdate)
                selectedSubField.meshUpdate();
            end
            
            try
            if (iscell(selectedSubField))                
                out = [];            
                for i=1:numel(selectedSubField)
                    out = out + selectedSubField{i}.y(e);
                end            
            else
                out = selectedSubField.y(e);
            end
            catch MExp
                save(['field.y.' int2str(System.rank) '.mat']);
                error('Problem in Field.y')
            end
        end 
        
        function out = yValue(s,e,fieldType,dim)
%             s.yValue = 0;
%             for e=1:s.numElements
%                 element = s.mesh.getLocalElement(e);
%                 s.yValue{e} = reshape(s.current.getValue(Derivative.y,e),element.disc.Q);
%             end
%             
%             out = s.yValue;
%             
            if nargin==1
                e   = [];
                dim = 1;
            end
            if nargin<3
                fieldType = [];
            end
            
            subField = s.getSubField(fieldType);
            yValue = subField.y;
            
            for e=1:s.numElements
                element = s.mesh.getLocalElement(e);
                yValue{e} = reshape(yValue{e},element.disc.Q);
            end
            
            out = yValue;
        end
        
        function out = z(s,subFieldName,e)
            % NB: 'e' is a global element number
            
            % Ensure the idx is a valid FieldType
            if (isa(subFieldName,'FieldType'))
                idx = subFieldName;
            else
                idx = FieldType.(subFieldName);
            end
                        
            selectedSubField = s.subFieldList{idx};
            
            if (selectedSubField.needsMeshUpdate)
                selectedSubField.meshUpdate();
            end
            
            try
            if (iscell(selectedSubField))                
                out = [];            
                for i=1:numel(selectedSubField)
                    out = out + selectedSubField{i}.z(e);
                end            
            else
                out = selectedSubField.z(e);
            end
            catch MExp
                save(['field.z.' int2str(System.rank) '.mat']);
                error('Problem in Field.z')
            end
        end 
        
        function out = t(s,subFieldName,e)
            % NB: 'e' is a global element number
            
            % Ensure the idx is a valid FieldType
            if (isa(subFieldName,'FieldType'))
                idx = subFieldName;
            else
                idx = FieldType.(subFieldName);
            end
                        
            selectedSubField = s.subFieldList{idx};
            
            if (selectedSubField.needsMeshUpdate)
                selectedSubField.meshUpdate();
            end
            
            try
            if (iscell(selectedSubField))                
                out = [];            
                for i=1:numel(selectedSubField)
                    out = out + selectedSubField{i}.t(e);
                end            
            else
                out = selectedSubField.t(e);
            end
            catch MExp
                save(['field.t.' int2str(System.rank) '.mat']);
                error('Problem in Field.t')
            end
        end
        
        function out = laplacian(s,subFieldName,e)
            out = s.xx(subFieldName,e) + s.yy(subFieldName,e);
        end
        
        function out = nn(s,subFieldName,e)

            scale = 1;
            
            element = s.mesh.getLocalElement(e);

            dx  = reshape(s.active.getValue(Derivative.x,e),element.finite.qSize);
            dy  = reshape(s.active.getValue(Derivative.y,e),element.finite.qSize);
            dxx = reshape(s.active.getValue(Derivative.xx,e),element.finite.qSize);
            dyy = reshape(s.active.getValue(Derivative.yy,e),element.finite.qSize);
            dxy = reshape(s.active.getValue(Derivative.xy,e),element.finite.qSize);

            % 3D
%                 dz  = zeros( size(dx) );
%                 dxz = zeros( size(dxy) );
%                 dyz = zeros( size(dxy) );
%                 dzz = zeros( size(dxx) );

            norm2 = dx.^2 + dy.^2;              % 2D
%                 norm2 = dx.^2 + dy.^2 + dz.^2;      % 3D

            % 2D
            out = scale * (dx.^2.*dxx + 2*dx.*dy.*dxy + dy.^2.*dyy) ./ norm2;
            
            out = out(:);
        end
        
        function out = xx(s,subFieldName,e)
            % NB: 'e' is a global element number
            
            % Ensure the idx is a valid FieldType
            if (isa(subFieldName,'FieldType'))
                idx = subFieldName;
            else
                idx = FieldType.(subFieldName);
            end
                        
            selectedSubField = s.subFieldList{idx};
            
            if (~isempty(selectedSubField))
                if (selectedSubField.needsMeshUpdate)
                    selectedSubField.meshUpdate();
                end

                try
                if (iscell(selectedSubField))                
                    out = [];            
                    for i=1:numel(selectedSubField)
                        out = out + selectedSubField{i}.xx(e);
                    end            
                else
                    out = selectedSubField.xx(e);
                end
                catch MExp
                    save(['field.xx.' int2str(System.rank) '.mat']);
                    error('Problem in Field.xx')
                end
            else
                out = [];
            end
        end
        
        function out = yy(s,subFieldName,e)
            % NB: 'e' is a global element number
            
            % Ensure the idx is a valid FieldType
            if (isa(subFieldName,'FieldType'))
                idx = subFieldName;
            else
                idx = FieldType.(subFieldName);
            end
                        
            selectedSubField = s.subFieldList{idx};
            
            if (~isempty(selectedSubField))
                if (selectedSubField.needsMeshUpdate)
                    selectedSubField.meshUpdate();
                end

                try
                if (iscell(selectedSubField))                
                    out = [];            
                    for i=1:numel(selectedSubField)
                        out = out + selectedSubField{i}.yy(e);
                    end            
                else
                    out = selectedSubField.yy(e);
                end
                catch MExp
                    save(['field.yy.' int2str(System.rank) '.mat']);
                    error('Problem in Field.yy')
                end
            else
                out = [];
            end
        end
        
        function out = zz(s,subFieldName,e)
            % NB: 'e' is a global element number
            
            % Ensure the idx is a valid FieldType
            if (isa(subFieldName,'FieldType'))
                idx = subFieldName;
            else
                idx = FieldType.(subFieldName);
            end
                        
            selectedSubField = s.subFieldList{idx};
            
            if (selectedSubField.needsMeshUpdate)
                selectedSubField.meshUpdate();
            end
            
            try
            if (iscell(selectedSubField))                
                out = [];            
                for i=1:numel(selectedSubField)
                    out = out + selectedSubField{i}.zz(e);
                end            
            else
                out = selectedSubField.zz(e);
            end
            catch MExp
                save(['field.zz.' int2str(System.rank) '.mat']);
                error('Problem in Field.zz')
            end
        end
        
        function out = xy(s,subFieldName,e)
            % NB: 'e' is a global element number
            
            % Ensure the idx is a valid FieldType
            if (isa(subFieldName,'FieldType'))
                idx = subFieldName;
            else
                idx = FieldType.(subFieldName);
            end
                        
            selectedSubField = s.subFieldList{idx};
            
            if (~isempty(selectedSubField))
                if (selectedSubField.needsMeshUpdate)
                    selectedSubField.meshUpdate();
                end

                try
                if (iscell(selectedSubField))                
                    out = [];            
                    for i=1:numel(selectedSubField)
                        out = out + selectedSubField{i}.xy(e);
                    end            
                else
                    out = selectedSubField.xy(e);
                end
                catch MExp
                    save(['field.xy.' int2str(System.rank) '.mat']);
                    error('Problem in Field.xy')
                end
            else
                out = [];
            end
        end
        
        function out = xt(s,subFieldName,e)
            % NB: 'e' is a global element number
            
            % Ensure the idx is a valid FieldType
            if (isa(subFieldName,'FieldType'))
                idx = subFieldName;
            else
                idx = FieldType.(subFieldName);
            end
                        
            selectedSubField = s.subFieldList{idx};
            
            if (~isempty(selectedSubField))
                if (selectedSubField.needsMeshUpdate)
                    selectedSubField.meshUpdate();
                end

                try
                if (iscell(selectedSubField))                
                    out = [];            
                    for i=1:numel(selectedSubField)
                        out = out + selectedSubField{i}.xt(e);
                    end            
                else
                    out = selectedSubField.xt(e);
                end
                catch MExp
                    save(['field.xt.' int2str(System.rank) '.mat']);
                    error('Problem in Field.xt')
                end
            else
                out = [];
            end
        end
        
        function out = yt(s,subFieldName,e)
            % NB: 'e' is a global element number
            
            % Ensure the idx is a valid FieldType
            if (isa(subFieldName,'FieldType'))
                idx = subFieldName;
            else
                idx = FieldType.(subFieldName);
            end
                        
            selectedSubField = s.subFieldList{idx};
            
            if (~isempty(selectedSubField))
                if (selectedSubField.needsMeshUpdate)
                    selectedSubField.meshUpdate();
                end

                try
                if (iscell(selectedSubField))                
                    out = [];            
                    for i=1:numel(selectedSubField)
                        out = out + selectedSubField{i}.yt(e);
                    end            
                else
                    out = selectedSubField.yt(e);
                end
                catch MExp
                    save(['field.yt.' int2str(System.rank) '.mat']);
                    error('Problem in Field.yt')
                end
            else
                out = [];
            end
        end
        
        function out = xxx(s,subFieldName,e)
            % NB: 'e' is a global element number
            
            % Ensure the idx is a valid FieldType
            if (isa(subFieldName,'FieldType'))
                idx = subFieldName;
            else
                idx = FieldType.(subFieldName);
            end
                        
            selectedSubField = s.subFieldList{idx};
            
            if (~isempty(selectedSubField))
                if (selectedSubField.needsMeshUpdate)
                    selectedSubField.meshUpdate();
                end

                try
                if (iscell(selectedSubField))                
                    out = [];            
                    for i=1:numel(selectedSubField)
                        out = out + selectedSubField{i}.xxx(e);
                    end            
                else
                    out = selectedSubField.xxx(e);
                end
                catch MExp
                    save(['field.xxx.' int2str(System.rank) '.mat']);
                    error('Problem in Field.xxx')
                end
            else
                out = [];
            end
        end
            
        function add(s,value,fieldType,varfield)
            if nargin==2 || nargin==4                
                if (nargin==2)
%                     s.subFieldList{1}.add(value);
                    s.active.add(value);
                else
                    s.subFieldList{1}.add(value,varfield);
                end
            else
                subField = s.getSubField(fieldType);
                subField.add(value);
            end
            
%             if (isa(value,'function_handle'))
%                 for e=1:s.numElements
%                     element = s.mesh.getLocalElement(e);
% 
%                     x = element.xelem;
%                     y = element.yelem;
%                     z = element.zelem;
% 
%                     if (s.isEmpty)
%                         s.value{e} = value(x,y,z);
%                     else
%                         s.value{e} = max( s.value{e} , value(x,y,z) );
%                     end
%                 end
%                 
%                 if (s.isEmpty)
%                     s.isEmpty = false;
%                 end
%             else
%                 error('unsupported ADD in FIELD class')
%             end
        end
        
        function out = getL2Norm(s,fieldType)
            switch fieldType
                case FieldType.Nonlinear
                    out = s.nNorm;
                case FieldType.Coupled
                    out = s.cNorm;
            end
        end
        
        function out = getNorms(s,fieldType)
            out = s.subFieldList{ fieldType }.getNorms;
        end
        
        % Compute the L2 norm 
        function norms = computeL2Norm(s,fieldType,derivative)
            
            if (nargin<3)
                derivative = 'val'; 
            end

            
            [norms,maxValue] = Norm.scalarL2Norm( s.subFieldList{ FieldType.Current }, ...
                                                  s.subFieldList{ fieldType }, ...
                                                  derivative);

            s.subFieldList{ fieldType }.setNorms(norms);
        end
        
        function norm = H1(s)
            norm_L2   = 0;
            norm_H1_x = 0; norm_H1_y = 0; norm_H1_z = 0;
            norm      = 0;
            
            % L2 norm
            norm_L2 = Norm.squaredNorm( s.subFieldList{ FieldType.Current }, ...
                                        s.subFieldList{ FieldType.Exact   }, ...
                                        'val' );

            if ( s.mesh.sDim > 0 )                                        
                % x derivative norm
                norm_H1_x = Norm.squaredNorm( s.subFieldList{ FieldType.Current }, ...
                                              s.subFieldList{ FieldType.Exact   }, ...
                                              'x' );
            end
            
            if ( s.mesh.sDim > 1 )                                        
                % y derivative norm
                norm_H1_y = Norm.squaredNorm( s.subFieldList{ FieldType.Current }, ...
                                              s.subFieldList{ FieldType.Exact   }, ...
                                              'y' );
            end
            
            if ( s.mesh.sDim > 2 )                                        
                % y derivative norm
                norm_H1_z = Norm.squaredNorm( s.subFieldList{ FieldType.Current }, ...
                                              s.subFieldList{ FieldType.Exact   }, ...
                                              'z' );
            end
            
            s.L2norm = sqrt( norm_L2 );
            s.H1norm = sqrt( norm_L2 + norm_H1_x + norm_H1_y + norm_H1_z );
            
            norm = s.H1norm;
        end
        
        function out = normc(s)
%             out = Norm.scalarL2Norm(s.value,s.valuec);
            [out, maxNorm] = Norm.scalarL2NormTest(s.current,s.coupled,s.mesh);
            
%             if (NMPI.instance.rank==0)
%                 fprintf('                                                       Max %s = %e\n',s.name,maxNorm);
%             end
        end
        
        function checkElementNumber(s,e)
            if (~ismember(e,s.mesh.eRange)) 
                error('A global element number is expected: %d is out of local range : %d - %d on rank %d\n',e,s.mesh.eRange(1),s.mesh.eRange(end),System.rank)
            end
        end
        
        function varargout = plot2(s,derivative,layer)
            plotType = 0;   % 0=compute value from internal alpha field, and 1=use present value directly
           
            if (plotType==1 && derivative==0)
                data = s.active.value;
            elseif (plotType==0 && derivative==0)
                data = s.active.getValue(Derivative.value);
            elseif (derivative==1)
                data = s.active.getValue(Derivative.x);
            end
            
            % Determine the mesh ranges to scale the plot
            plotMesh = s.active.mesh;
            ranges   = (plotMesh.X(2,:)-plotMesh.X(1,:))';

            % Determine the plot dimension
            plotDim = s.mesh.dim;            
            if (s.mesh.dim==1)
                plotDim = 1;
            elseif (s.mesh.dim==2 && ~System.settings.time.spaceTime)
                plotDim = 2;
            elseif (s.mesh.dim==3 && System.settings.time.spaceTime )
                plotDim = 2;
            elseif ( (s.mesh.dim==3 && ~System.settings.time.spaceTime))
                plotDim = 3;
            end
            
            % Plot the data
            switch plotDim                
                case 1
                    for e=plotMesh.eRange
                        element = plotMesh.getElement(e);                        
                        X       = element.getNodes;
                        
                        if ( size(data{ element.localID },2)==1 && numel(element.disc.Q)>1 )
                            data{ element.localID } = reshape(data{ element.localID },element.disc.Q);
                           	surf(X,Y,data{ element.localID }(:,:,t),'FaceColor','interp');
                        else
                            if (element.disc.DIM == 1)
                                line(X,data{ element.localID });
                            else
                                surf(X,Y,data{ element.localID }(:,:,t),'FaceColor','interp');
                            end
                        end
                        
                        hold on
                    end
                    
                    colorbar;
                    title(titleString);
                    pbaspect([ranges(1) 1 1])
                    xlim( s.mesh.X(1,:) );
                    
                case 2
                    for e=plotMesh.eRange
                        element = plotMesh.getElement(e);                        
                        [X,Y]   = element.getNodes;
                        
                        X = X(:,:,:,end);
                        Y = Y(:,:,:,end);
                        
                        if ( size(data{ element.localID },2)==1 && numel(element.finite.qSize)>1 )
                            if (element.lvl==0 || element.position{element.finite.dim}==element.refineFactor(element.finite.dim) || ~element.finite.spaceTime )
                                data{ element.localID } = reshape(data{ element.localID },element.finite.qSize);
                                surf(X,Y,data{ element.localID }(:,:,end),'FaceColor','interp');
                            end
                        else
                            if (element.lvl==0 || element.position{element.disc.DIM}==1)
                                data{ element.localID } = reshape(data{ element.localID },element.disc.Q);
                                surf(X,Y,data{ element.localID }(:,:,1),'FaceColor','interp');
                            end
                        end
                        
                        hold on
                    end
                    
                    colorbar;
                    view([25,25]);
                    pbaspect([ranges(1) ranges(2) 1])
                    xlim( s.mesh.X(:,1) );
                    ylim( s.mesh.X(:,2) );
                    
                case 3
                    for e=plotMesh.eRange
                        element = plotMesh.getElement(e);                        
                        [X,Y,Z] = element.getNodes;
                        
                        data{ element.localID } = reshape(data{ element.localID },element.disc.Q);
                        %data{ element.localID } = permute( data{ element.localID }, [2,1,3] );
                        [f,v] = isosurface(X,Y,Z,data{ element.localID  },0.5);
                        patch('Faces',f,'Vertices',v,'FaceColor','red')
                        
                        hold on
                    end
                    
                    colorbar;
                    title(titleString);
                    view([25,25]);
                    pbaspect([ranges(1) ranges(2) ranges(3)])
                    xlim( s.mesh.X(1,:) );
                    ylim( s.mesh.X(2,:) );
                    zlim( s.mesh.X(3,:) );
            end
            
            if nargout>0
                varargout{1} = gcf;
            end            
        end
        
        function plot(s,plotVar)
            if nargin==1
                s.active.plot();
            else
                s.active.plot(plotVar);
            end
        end
        
        function varargout = plot3(s,titleString,layer,plotType,offset,fieldType,tRange)
            
            clf;
            
            plotDim = 0;
            
            if (nargin==1 || nargin<7)
                % default values
                titleString  = '';                      % title for this plot
                if nargin<3
                    layer        = 1;                       % 0=t0 and 1=t1 (for spaceTime field)
                end
                plotType     = 0;                       % 0=compute value from internal alpha field, and 1=use present value
                offset       = 0;                       % for parallel fields
                fieldType = FieldType.Current;     % by default no relaxation of the values
                iRange       = 0;
            end
            
            if nargin==7
                iRange = tRange;
            end
            
            if (plotType==1)
                if (nargin==6)
                    if (fieldType==FieldType.Current)
                        subField = s.current;
                        data = s.current.val;
                    elseif (fieldType==FieldType.Nonlinear) 
                        subField = s.nonlinear;
                        data = s.nonlinear.val;
                    elseif (fieldType==FieldType.Interpolated)                        
                        subField = s.interpolated;
                        data = s.interpolated.val;
                    end
                else
                    data = s.current.value;
                end
            elseif (plotType==0)
                try
                    data = s.active.computeValue;
                    subField = s.active;
                catch
                    s.setActive(FieldType.Current);
                    data = s.active.computeValue;
                    subField = s.active;
                end
                
                
            end
            
%             data = s.active.computeValue;
%             subField = s.active;
            
            %data = s.active.value;
            
%             if iRange==0
%                 if (nargin>2)
%                     iRange = layer;
%                 else
%                     if (s.mesh.dim<3)
%                         iRange = 1;
%                     else
%                         iRange = s.mesh.finite.Q(3); %size(data{1},3);
%                     end
%                 end
%             end
                        
            % Check if argument is supplied to this function, and set
            % default values if the variable is not present
            if (~exist('title','var'))
                if (plotType==0)
                    titleString = ['Field ' s.name ' (alpha)'];
                elseif (plotType==1)
                    titleString = ['Field ' s.name ' (value)'];
                end
            end
            if (~exist('layer','var'))
                layer = 1;
            end
            if (~exist('plotType','var'))
                plotType = 1;
            end
            if (~exist('offset','var'))
                offset = 0;
            end
            
            plotMesh = subField.mesh;
                
            count = 0;
            
            % Determine the mesh ranges to scale the plot
            plotMesh = subField.mesh;
            ranges   = (plotMesh.X(2,:)-plotMesh.X(1,:))';

            plotDim = s.mesh.dim;
            
            if (s.mesh.dim==1)
                plotDim = 1;
            elseif (s.mesh.dim==2 && ~System.settings.time.spaceTime)
                plotDim = 2;
            elseif (s.mesh.dim==3 && System.settings.time.spaceTime )
                plotDim = 2;
            elseif ( (s.mesh.dim==3 && ~System.settings.time.spaceTime))
                plotDim = 3;
            end
            
            % Plot the data
            switch plotDim
                
                case 1
                    for e=plotMesh.eRange
                        element = plotMesh.getElement(e);                        
                        X       = element.getNodes;
                        
                        if ( size(data{ element.localID },2)==1 && numel(element.disc.Q)>1 )
                            data{ element.localID } = reshape(data{ element.localID },element.disc.Q);
                           	surf(X,Y,data{ element.localID }(:,:,t),'FaceColor','interp');
                        else
                            if (element.disc.DIM == 1)
                                line(X,data{ element.localID });
                            else
                                surf(X,Y,data{ element.localID }(:,:,t),'FaceColor','interp');
                            end
                        end
                        
                        hold on
                    end
                    
                    colorbar;
                    title(titleString);
                    pbaspect([ranges(1) 1 1])
                    xlim( s.mesh.X(1,:) );
                    
                case 2
                    for e=plotMesh.eRange
                        element = plotMesh.getElement(e);                        
                        [X,Y]   = element.getNodes;
                        
                        X = X(:,:,:,end);
                        Y = Y(:,:,:,end);
                        
                        if ( size(data{ element.localID },2)==1 && numel(element.finite.qSize)>1 )
                            if (element.lvl==0 || element.position{element.finite.dim}==element.refineFactor(element.finite.dim) || ~element.finite.spaceTime )
                                data{ element.localID } = reshape(data{ element.localID },element.finite.qSize);
                                surf(X,Y,data{ element.localID }(:,:,end),'FaceColor','interp');
                            end
                        else
                            if (element.lvl==0 || element.position{element.disc.DIM}==1)
                                data{ element.localID } = reshape(data{ element.localID },element.disc.Q);
                                surf(X,Y,data{ element.localID }(:,:,1),'FaceColor','interp');
                            end
                        end
                        
                        hold on
                    end
                    
                    colorbar;
                    title(titleString);
                    view([25,25]);
                    pbaspect([ranges(1) ranges(2) 1])
                    xlim( s.mesh.X(:,1) );
                    ylim( s.mesh.X(:,2) );
                    
                case 3
                    for e=plotMesh.eRange
                        element = plotMesh.getElement(e);                        
                        [X,Y,Z] = element.getNodes;
                        
                        data{ element.localID } = reshape(data{ element.localID },element.disc.Q);
                        %data{ element.localID } = permute( data{ element.localID }, [2,1,3] );
                        [f,v] = isosurface(X,Y,Z,data{ element.localID  },0.5);
                        patch('Faces',f,'Vertices',v,'FaceColor','red')
                        
                        hold on
                    end
                    
                    colorbar;
                    title(titleString);
                    view([25,25]);
                    pbaspect([ranges(1) ranges(2) ranges(3)])
                    xlim( s.mesh.X(1,:) );
                    ylim( s.mesh.X(2,:) );
                    zlim( s.mesh.X(3,:) );
            end
                    
                
%             for t=iRange
%                 count = count+1;
%                 subplot(numel(iRange),1,count);
%                 for e=plotMesh.eRange
%                     element = plotMesh.getElement(e);
%                     
%                     if (~isempty(element.xelem))
%                         [X,Y] = meshgrid( element.xelem,element.yelem );
%                     else
%                         if (s.mesh.dim==1)
%                             plotDim = 1;
%                             X = element.nodes{:};
%                         elseif (s.mesh.dim==2 && ~s.mesh.disc.spaceTime)
%                             plotDim = 2;
%                             [X,Y] = meshgrid( element.nodes{:} );
%                         elseif (s.mesh.dim==3 && s.mesh.disc.spaceTime )
%                             plotDim = 2;
%                             [X,Y] = meshgrid( element.nodes{1:end-1} );
%                         elseif ( (s.mesh.dim==3 && ~s.mesh.disc.spaceTime))
%                             plotDim = 3;
%                             [X,Y,Z] = meshgrid( element.nodes{:} );
%                         else
%                             error('fix this please')
%                             [X,Y] = meshgrid( element.nodes{:} );
%                         end
%                     end
% 
%                     if (plotDim==1 || plotDim==2)
%                         if ( size(data{ element.localID },2)==1 && numel(element.disc.Q)>1 )
%                             data{ element.localID } = reshape(data{ element.localID },element.disc.Q);
% 
%     %                         if (layer==1)
%                                 surf(X',Y',data{ element.localID }(:,:,t),'FaceColor','interp');
%     %                         elseif (layer==0)
%     %                             surf(X,Y,data{ element.localID }(:,:,1)','FaceColor','interp');
%     %                         end
%                         else
%                             %[X,Y] = meshgrid( element.xelem,element.yelem );
%     %                         if (layer==1)
%                             if (element.disc.DIM == 1)
%                                 line(X,data{ element.localID });
%                             else
%                                 surf(X',Y',data{ element.localID }(:,:,t),'FaceColor','interp');
%                             end
% 
% 
%     %                         elseif (layer==0)
%     %                             surf(X,Y,data{ element.localID }(:,:,1)','FaceColor','interp');
%     %                         end
%                         end
%                         
%                     elseif (plotDim==3)
%                         data{ element.localID } = reshape(data{ element.localID },element.disc.Q);
%                         data{ element.localID } = permute( data{ element.localID }, [2,1,3] );
%                         [f,v] = isosurface(X,Y,Z,data{ element.localID  },0.5);
%                         patch('Faces',f,'Vertices',v,'FaceColor','red')
%                         
% %                         surf(X,Y,Z,data{ element.localID }(:,:,t),'FaceColor','interp');
%                     end                    
%                     
%                     hold on;
%                 end

%                 colorbar;
%                 if (s.mesh.dim>1)
%                     view([25,25]);
%                 end
%                 title(titleString);
%             end
            
%             ranges = (s.mesh.X(:,2)-s.mesh.X(:,1))';
%             if (numel(ranges)>=3)
%                 
%             elseif (numel(ranges)>=2)
%                 pbaspect([ranges(1) ranges(2) 2])
%             else
%                 pbaspect([ranges(1) 1 1])
%             end
%             
%             xlim( s.mesh.X(1,:) );
%             ylim( s.mesh.X(2,:) );
%             if s.mesh.dim>2
%                 zlim( s.mesh.X(3,:) );
%             end

            if nargout>0
                varargout{1} = gcf;
            end

        end
                
        function writeVTK(s)
            xyz = [s.mesh.octTree.elements.nodes]';
            x = [xyz{:,1}]';
            y = [xyz{:,2}]';
            z = [xyz{:,3}]';
            
            [X,Y,Z] = s.mesh.getElement(2).getNodes;
            data = s.active.value{2};
            
            vtkwrite('field2.vtk', 'structured_grid', X,Y,Z, 'scalars', s.name, data);
            %'vectors', 'vector_field', u, v, w, 'vectors', 'vorticity', cu, cv, cw, 
            
        end
        
        function plotContour(s,contourValue)
            
            if (NMPI.instance.nproc>1)
                
%                 globalAlphaArray = s.getGlobalAlpha;
                
                if (NMPI.instance.rank==0)
                    numElements = s.mesh.getNumGlobalElements;

                    alpha = cell(numElements,1);
                    for e=1:numElements
                        element = s.mesh.getGlobalElement(e);
                        alpha{ e } = s.globalAlphaArray( s.mesh.getGlobalGM{e} );
                    end

                    value = cell(numElements,1);
                    for e=1:numElements
                        element = s.mesh.getGlobalElement(e);
                        value{e} = element.getH * (element.Z * alpha{e}(:) );
                    end

                    % Plot the surfaces
                    for e=1:numElements
                        data = value{e};
                        data = reshape(data,s.mesh.disc.Q);
                        data = data(:,:,end);

                        element = s.mesh.getGlobalElement(e);
                        
                        x = element.xelem;
                        y = element.yelem;

                        nn = 10;
                        xi = linspace(x(1), x(end), nn);
                        yi = linspace(y(1), y(end), nn);

                        ax = get(gca);
                        ax.SortMethod = 'childorder'; % this is the important line

                        zi = interp2(x, y, data', xi, yi', 'spline');
                        hh = surf(xi,yi,zi);
                        hold on;
                        set(hh,'edgecolor','none') ;
                    end

                    offset = 0.5;

                    % Add the contour line(s)
                    for e=1:numElements
                        data = value{e};
                        data = reshape(data,s.mesh.disc.Q);
                        data = data(:,:,end);

                        element = s.mesh.getGlobalElement(e);

                        x = element.xelem;
                        y = element.yelem;

                        set(gcf, 'renderer', 'zbuffer')
                        [~,contourHandle]=contour3(x,y,data',[contourValue+offset contourValue+offset],'k','LineWidth',2);
                        set(contourHandle,'ZData',contourHandle.ZData+offset);

                        hold on;
                    end              
                end
            else
            
                % Plot the surfaces
                for e=1:s.numElements
                    data = s.getValue(e);
                    data = reshape(data,s.mesh.disc.Q);
                    data = data(:,:,end);

                    x = s.mesh.getElement(e).xelem;
                    y = s.mesh.getElement(e).yelem;

                    nn = 10;
                    xi = linspace(x(1), x(end), nn);
                    yi = linspace(y(1), y(end), nn);

                    ax = get(gca);
                    ax.SortMethod = 'childorder'; % this is the important line

                    zi = interp2(x, y, data', xi, yi', 'spline');
                    hh = surf(xi,yi,zi);
                    hold on;
                    set(hh,'edgecolor','none') ;

                    uistack(hh,'bottom')

    %                 set(gcf, 'renderer', 'zbuffer')
    %                 [C,contourHandle]=contour3(x,y,data',[contourValue contourValue],'k','LineWidth',2);
                    %hold on;
                end

                offset = 0.5;
                if exist('contourHandle')==1
                    delete(contourHandle);
                end

                % Add the contour line(s)
                for e=s.mesh.eRange
                    data = s.getValue(e);
                    data = reshape(data,s.mesh.disc.Q);
                    data = data(:,:,end);

                    x = s.mesh.getElement(e).xelem;
                    y = s.mesh.getElement(e).yelem;

                    set(gcf, 'renderer', 'zbuffer')
                    [~,contourHandle]=contour3(x,y,data',[contourValue+offset contourValue+offset],'k','LineWidth',2);
                    set(contourHandle,'ZData',contourHandle.ZData+offset);

                    hold on;
                end
                
            end
            
            xlabel('x')
            ylabel('y')
            view([0,90])
            %caxis([0, 1])
            
            colormap jet
            h=colorbar('SouthOutside');
            drawnow;
            
            frame = getframe(gcf);
            im = frame2im(frame);
            [imind,cm] = rgb2ind(im,256);
            outfile = 'test.gif';

            % On the first loop, create the file. In subsequent loops, append.
            if (exist(outfile,'file')~=2)
                imwrite(imind,cm,outfile,'gif','DelayTime',0,'loopcount',inf);
            else
                imwrite(imind,cm,outfile,'gif','DelayTime',0,'writemode','append');
            end
        end
        
        function plotAlpha(s,offset)
            data = s.alpha;
                           
            for e=s.mesh.eRange
                element = s.mesh.getElement(e);
                if (size(data{e},2)==1)
                    data{e} = reshape(data{e},element.disc.Pn);
                    [X,Y] = meshgrid( element.xelem,element.yelem );
                    surf(X,Y,data{e}(:,:,end)');
                else
                    dx = element.xelem(end)-element.xelem(1);
                    xelem = [element.xelem(1):dx/4:element.xelem(end)];
                    dy = element.yelem(end)-element.yelem(1);
                    yelem = [element.yelem(1):dy/4:element.yelem(end)];
                    [X,Y] = meshgrid( xelem,yelem );
                    surf(X,Y,data{e}(:,:,end)');
                end
                hold on;
            end
            
            colorbar;
            view([25,25]);
%             title(titleString);            
        end
        
        function out = getVariable(s)
            out = s.variable;
        end
        
        function setPrescribed(s,e,flags)
            s.checkElementNumber(e);
            
            s.prescribed{e}(flags) = true;
            
            s.prescribedSynced = false;
        end
        
        function resetSyncPrescribed(s)
            s.prescribedSynced = false;
        end
        
        function syncPrescribed(s)
            % create a multi-element array with all the prescribed flags
            prescribedArray = false(s.numDof,1);

            for e=s.mesh.eRange
                element = s.mesh.getElement(e);
                localDofs = s.mesh.dh.localDofs{ e };
                prescribedArray( localDofs(:) ) = or( prescribedArray( localDofs(:) ), s.prescribed{ element.localID }(:) );
            end
            
            for e=s.mesh.eRange
                element = s.mesh.getElement(e);
                s.prescribed{ element.localID } = prescribedArray( element.GMlocal(:) );
            end
            
            s.prescribedSynced = true;
        end
        
        function out = getPrescribed(s,e)
            s.checkElementNumber(e);
            
            if (~s.prescribedSynced)
                s.syncPrescribed;
            end
            
            element = s.mesh.getElement(e);
            
            out = s.prescribed{ element.localID }(:);
        end
        
        function out = getPrescribedArray(s,e)
            s.checkElementNumber(e);
            
            if (~s.prescribedSynced)
                s.syncPrescribed;
            end
            
            element = s.mesh.getElement(e);
            
            out = s.prescribed{ element.localID }(:);
        end
        
        function out = getPrescribedValues(s,e)
            s.checkElementNumber(e);
            
            if (~s.prescribedSynced)
                s.syncPrescribed;
            end
            
            element = s.mesh.getElement(e);
            
            out = s.current.alpha{ element.localID }( s.prescribed{ element.localID }(:) );
        end
              
%         function updateTimeSlab(s)
%             s.value;
%         end
        
        % Function to assign a (manufactured) solution to this field. The
        % passed argument needs to be a function handle. After initializing
        % the field (reset), the values are set. Next, alpha(Array) are
        % determined from these values.
        %
        % fh    : function_handle which provides the value at location (x,y,z)
        %
        function assignSolution(s,fh)
            if (~isa(fh,'function_handle'))
                error('A function should be provided to be able to assign a manufactured solution')
            end
            
            s.initSubField(FieldType.Exact);
            s.exact.assignSolution(fh);
            
%             s.exact.weight=1;
%             
%             for e=1:s.numElements
%                 element = s.mesh.getLocalElement(e);               
%                 s.value{e} = fh(s.variable,element.xelem,element.yelem,element.zelem);
%             end
%             
%             s.computeAlpha();       % sets both alpha and alphaArray
        end
        
        % Function to set the initial alpha based on boundary and/or initial
        % conditions
        function setAlpha(s,boundaryNumber,derivative,condition)
            boundary = s.mesh.getBoundary(boundaryNumber);
            
            if (derivative==0)
                temp    = boundary.H;
                tempPos = boundary.Hpos;
            elseif (derivative==1)
                temp    = boundary.Dn;
                tempPos = boundary.Dpos;
            end

            elemental = true; % elemental / global
            
            if (elemental)

                for be=1:boundary.getNumElements
                    e = boundary.getElementNumbers(be);
                    
                    if ismember(e,s.mesh.eRange)
                        
                        element = s.mesh.getElement(e);

                        GM = element.GMlocal;
                        
%                         GM = s.getGM(e+NMPI.instance.eStart-1,variable);                   % Get the GatheringMatrix for this GLOBAL element/variable
% %                         GM = element.GM;
%                         GM2{e,1} = GM(:,:,end);
                                                     
                        % Step 1: Initialize alpha( tempPos ) to 0
                        s.alphaArray( GM(tempPos) ) = 0;

                        % Step 2: Assign boundary condition values
                        if (~isa(condition,'function_handle'))
                            if ~isa(condition,'Field') 
                                bc = (temp==1);                  % Get only the elements that are exactly equal to 1
                                bc(:,all(~bc,1)) = [];                % Remove empty columns
                                bc(all(~bc,2),:) = [];                % Remove empty rows

                                valueVector = condition*ones( nnz(bc), 1 );

                                [~,col]     = find(temp==1);
                                bcGM        = GM(col);
                            else
                                bc = temp*element.Z;                  % 
                                bc(:,all(~bc,1)) = [];                % Remove empty columns
                                bc(all(~bc,2),:) = [];                % Remove empty rows

                                if (s==condition)
                                    valueVector = reshape( s.value{ element.localID }, element.disc.dofeq);
                                    valueVector = valueVector(:,:,end);
                                else
                                    valueVector = condition.value{e}(:,:,end);
                                end

                                colNonZero  = ~all(~temp,1);      % Find all nonzero column numbers
                                bcGM        = GM(colNonZero);           % Use the nonzero columns to reduce the GM for just the boundary
                            end
                        else
                            % use the function handle, which provides a valueVector based on (x,y,z) location 
                            bc = temp;                       % 
                            bc(:,all(~bc,1)) = [];                % Remove empty columns
                            bc(all(~bc,2),:) = [];                % Remove empty rows

                            x = element.xelem;
                            y = element.yelem;
                            z = element.zelem;

                            switch boundary.side
                                case Neighbor.Left
                                    x = s.mesh.x0;
                                case Neighbor.Right
                                    x = s.mesh.x1;
                                case Neighbor.Bottom
                                    y = s.mesh.y0;
                                case Neighbor.Top
                                    y = s.mesh.y1;
                                case Neighbor.Front
                                    z = s.mesh.z0;
                                case Neighbor.Back
                                    z = s.mesh.z1;
                                case 9
                                    x = -0.5;
                                    y = -0.5;
                            end

                            valueVector = condition(s.getVariable,x,y,z);

                            colNonZero  = ~all(~temp,1);      % Find all nonzero column numbers
                            bcGM        = GM(colNonZero);           % Use the nonzero columns to reduce the GM for just the boundary
                        end

                        % Compute the corresponding alpha for the
                        % prescribed boundary condition
                        if (derivative==0)
                            s.alphaArray( bcGM ) = bc \ ( valueVector(:) ) ;
                        elseif (derivative==1)
                            s.alphaArray( bcGM ) = bc \ ( valueVector(:) * boundary.Jn(be) ) ;
                        else
                            error('unsupported derivative flag')
                        end
%                         end

%                         if (boundaryNumber==5)
%                             A{e,1} = bc' * bc;
%                             b{e,1} = bc' * valueVector(:);
%                         end
                    end
                end
%                 end
                
            else
%                 for be=1:boundary.getNumElements
%                     e = boundary.getElementNumbers(be);
%                     if (e>=NMPI.instance.eStart && e<=NMPI.instance.eEnd)
% 
%                         GM = s.getGM(e,variable);                   % Get the GatheringMatrix for this element/variable
% 
%                         % Step 1: Initialize alpha( tempPos ) to 0
%                         s.alpha( GM(tempPos) ) = 0;
% 
%                         % Step 2: Assign boundary condition values
%                         if (~isa(value,'function_handle'))
%                             if length(value)==1 
%                                 bc = (temp==1);                  % Get only the elements that are exactly equal to 1
%                                 bc(:,all(~bc,1)) = [];                % Remove empty columns
%                                 bc(all(~bc,2),:) = [];                % Remove empty rows
% 
%                                 valueVector = value*ones( nnz(bc), 1 );
% 
%                                 [~,col]     = find(temp==1);
%                                 bcGM        = GM(col);
%                             else
%                                 bc = temp;                       % 
%                                 bc(:,all(~bc,1)) = [];                % Remove empty columns
%                                 bc(all(~bc,2),:) = [];                % Remove empty rows
% 
%                                 valueVector = value{e}(:,:,end);
% 
%                                 colNonZero  = ~all(~temp,1);      % Find all nonzero column numbers
%                                 bcGM        = GM(colNonZero);           % Use the nonzero columns to reduce the GM for just the boundary
%                             end
%                         else
%                             % use the function handle, which provides a valueVector based on (x,y,z) location 
%                             bc = temp;                       % 
%                             bc(:,all(~bc,1)) = [];                % Remove empty columns
%                             bc(all(~bc,2),:) = [];                % Remove empty rows
% 
%                             element = s.mesh.getElement(e);
%                             x = element.xelem;
%                             y = element.yelem;
%                             z = element.zelem;
% 
%                             switch boundary.side
%                                 case Neighbor.Left
%                                     x = s.mesh.x0;
%                                 case Neighbor.Right
%                                     x = s.mesh.x1;
%                                 case Neighbor.Bottom
%                                     y = s.mesh.y0;
%                                 case Neighbor.Top
%                                     y = s.mesh.y1;
%                                 case Neighbor.Front
%                                     z = s.mesh.z0;
%                                 case Neighbor.Back
%                                     z = s.mesh.z1;
%                                 case 9
%                                     x = -0.5;
%                                     y = -0.5;
%                             end
% 
%                             valueVector = value(variable,x,y,z);
% 
%                             colNonZero  = ~all(~temp,1);      % Find all nonzero column numbers
%                             bcGM        = GM(colNonZero);           % Use the nonzero columns to reduce the GM for just the boundary
%                         end
% 
%                         % Compute the corresponding alpha for the
%                         % prescribed boundary condition
%                         s.alpha( bcGM ) = bc \ valueVector(:);
%                         A()
%                     end
%                 end
            end
            
%             if (boundaryNumber==7)
%                 GMflag = false(s.getNumDof,1);
%                 fprintf('numBoundaryElements  : %d\n',boundary.getNumLocalElements)
%                 fprintf('numDof in Physics    : %d\n',s.getNumDof)
% %                 fprintf('max value in Physics : %d\n',max(GM2{be}(:)))
% %                 counter = 0;
%                 for be=1:boundary.getNumLocalElements
%                     e = boundary.getLocalElementNumbers(be);
%                     if inrange(e,1,s.getNumLocalElements)
% %                         counter = counter+1;
%                         GMflag( GM2{e}(:) ) = true;
%                     end
%                 end            
% %                 numFreeDof = nnz(GMflag);
% %                 GMrenumbering = zeros(s.getNumDof,1);
% %                 GMrenumbering(GMflag) = (1:numFreeDof);
% % %                 counter = 0;
% %                 for be=1:boundary.getNumLocalElements
% %                     e = boundary.getLocalElementNumbers(be);
% %                     if inrange(e,1,s.getNumLocalElements)
% % %                         counter = counter+1;
% %                         GMnew{e,1} = GMrenumbering(GM2{e}(:));
% %                     end
% %                 end
%                 
%                 for be=1:boundary.getNumLocalElements
%                     s.prescribed{be}(:,2) = true;
%                     s.prescribed{be}(1:(layer-1)*numel(GM2{be})+1:end,1) = true;
%                     s.prescribed{be}(layer*numel(GM2{be})+1:end,1) = true;
%                 end                
%                 
%                 alpha_in = s.alpha( GMflag,1 );
%                 [alf,iter]=PrecElemCG_par_light(A,b,GM2, s.prescribed,s.mesh.mutualGM, s.getNumDof,s.epsilon, alpha_in);
% 
% %                 alpha2 = zeros(s.getNumDof,1);
% %                 alpha2( GMflag,1 ) = alf;
% %                 s.alpha = alpha2;
%                 
%                 s.alpha( GMflag,1 ) = alf;
%             end
            
            s.convertAlpha(1);
            s.value = s.computeValue;
        end
        
        % Function to set the initial alpha based on boundary and/or initial
        % conditions
        function setAlphaLine(s,boundaryNumber1,boundaryNumber2,derivative,condition)
            boundary1 = s.mesh.getBoundary(boundaryNumber1);    
            boundary2 = s.mesh.getBoundary(boundaryNumber2);
            

            
            if (derivative==0)                                
                Hpos1 = boundary1.Hpos;
                Hpos2 = boundary2.Hpos;
                tempPos = intersect(Hpos1,Hpos2);
                
                H1 = boundary1.H;
                H2 = boundary2.H;
                temp = H1(:,tempPos);
            elseif (derivative==1)
                error('Derivative bc in fField.setAlphaLine has not been implemented yet')
                temp    = boundary.Dn;
                tempPos = boundary.Dpos;
            end

            elemental = true; % elemental / global
            
            if (elemental)

                elements1=boundary1.getElementNumbers;
                elements2=boundary2.getElementNumbers;
                mutualElements = intersect(elements1,elements2);
                
                for me=1:numel(mutualElements)
                    e = mutualElements(me);
                    
                    if ismember(e,s.mesh.eRange)
                        
                        element = s.mesh.getElement(e);

                        GM = element.GMlocal;
                        GM = GM(tempPos);
                        
%                         GM = s.getGM(e+NMPI.instance.eStart-1,variable);                   % Get the GatheringMatrix for this GLOBAL element/variable
% %                         GM = element.GM;
%                         GM2{e,1} = GM(:,:,end);
                                                     
                        % Step 1: Initialize alpha( tempPos ) to 0
                        s.alphaArray( GM ) = 0;

                        % Step 2: Assign boundary condition values
                        if (~isa(condition,'function_handle'))
                            if ~isa(condition,'Field') 
                                bc = (temp==1);                  % Get only the elements that are exactly equal to 1
                                bc(:,all(~bc,1)) = [];                % Remove empty columns
                                bc(all(~bc,2),:) = [];                % Remove empty rows

                                valueVector = condition*ones( nnz(bc), 1 );

                                [~,col]     = find(temp==1);
                                bcGM        = GM(col);
                            else
                                bc = temp*element.Z;                  % 
                                bc(:,all(~bc,1)) = [];                % Remove empty columns
                                bc(all(~bc,2),:) = [];                % Remove empty rows

                                if (s==condition)
                                    valueVector = reshape( s.value{e}, element.disc.dofeq);
                                    valueVector = valueVector(:,:,end);
                                else
                                    valueVector = condition.value{e}(:,:,end);
                                end

                                colNonZero  = ~all(~temp,1);      % Find all nonzero column numbers
                                bcGM        = GM(colNonZero);           % Use the nonzero columns to reduce the GM for just the boundary
                            end
                        else
                            % use the function handle, which provides a valueVector based on (x,y,z) location 
                            bc = temp;                       % 
                            bc(:,all(~bc,1)) = [];                % Remove empty columns
                            bc(all(~bc,2),:) = [];                % Remove empty rows

                            x = element.xelem;
                            y = element.yelem;
                            z = element.zelem;

                            switch boundary.side
                                case Neighbor.Left
                                    x = s.mesh.x0;
                                case Neighbor.Right
                                    x = s.mesh.x1;
                                case Neighbor.Bottom
                                    y = s.mesh.y0;
                                case Neighbor.Top
                                    y = s.mesh.y1;
                                case Neighbor.Front
                                    z = s.mesh.z0;
                                case Neighbor.Back
                                    z = s.mesh.z1;
                                case 9
                                    x = -0.5;
                                    y = -0.5;
                            end

                            valueVector = condition(s.getVariable,x,y,z);

                            colNonZero  = ~all(~temp,1);      % Find all nonzero column numbers
                            bcGM        = GM(colNonZero);           % Use the nonzero columns to reduce the GM for just the boundary
                        end

                        % Compute the corresponding alpha for the
                        % prescribed boundary condition
                        if (derivative==0)
                            s.alphaArray( bcGM ) = bc \ ( valueVector(:) ) ;
                        elseif (derivative==1)
                            s.alphaArray( bcGM ) = bc \ ( valueVector(:) * boundary.Jn(me) ) ;
                        else
                            error('unsupported derivative flag')
                        end
                    end
                end                
            else
                
            end
            
            s.current.convertAlpha(1);
            s.value = s.current.computeValue;
        end
        
        function out = omegaError(s)
            errorValue = 0;
            
            if (nargin==1)
                position = 0;
            end
            
            Omega   = FieldCollection.get('omg');
            
            if ( Omega.subFieldList{4}.type == FieldType.Exact )
                exact = Omega.subFieldList{ FieldType.Exact };
                
                if (exact.needsMeshUpdate)
                    exact.meshUpdate;
                end                
            else
                error('no exact field available')
            end
            
            Cn      = System.settings.phys.Cn;
            alpha   = System.settings.phys.alpha;
            
            % In the case a spaceTime mesh is used, this flag determines if
            % the error is computed for the complete spaceTime element or just 
            % a slice. The slice is set by the 'position' input parameter
            completeSpaceTime = true;
            
            for e=s.mesh.eRange   
                element = s.mesh.getElement(e);
                
                % Computed omega
                Ck = s.value{element.localID}(:);
                Ze = s.mesh.Z{e};
                Nabla2 = element.getDxx + element.getDyy;
                data = (Ck.^3 - 3/2*Ck.^2 + 0.5.*Ck) - Cn^2 * ( Nabla2 * Ze * s.alpha{element.localID}(:) );
                computedOmega = alpha/Cn * data;
                
                data = (computedOmega(:) - exact.value{ element.localID }(:)).^2;
                data = reshape( data, element.finite.qSize );
                
                if (completeSpaceTime)
                    w = element.finite.W;
                    errorValue = errorValue + dot( data(:), w ) * (element.J) ;
                else
                    wSpatial = element.finite.Wspatial;

                    if position == 0
                        % time level 0 of element
                        plane = data(:,:,1);
                    else
                        plane = data(:,:,end);
                    end
                    errorValue = errorValue + plane(:)' * wSpatial * (element.JSpatial) ;
                end
            end
            
            if (numel(errorValue)==1)
                errorValue = NMPI.Allreduce(errorValue,1,'+');
            else
                error('LSQDIM:Field','Wrong number of elements in errorValue (%d elements where 1 is expected)',numel(errorValue));
            end
            s.error = sqrt(errorValue);
            %if (NMPI.instance.rank==0)
            %    fprintf('Total error %3s: %e\n','omega',s.error);
            %end
            
            out = s.error;
        end
        
        function computeError(s,position)
%             [s.errorNorm, s.errorMax] = Norm.scalarL2NormTest(s.current,s.exact,s.mesh);
            
            errorValue = 0;
            
            if (nargin==1)
                position = 0;
            end
            
            if ( s.subFieldList{4}.type == FieldType.Exact )
                exact = s.subFieldList{ FieldType.Exact };
                
                if (exact.needsMeshUpdate)
                    exact.meshUpdate;
                end                
            else
                error('no exact field available')
            end
            
            % In the case a spaceTime mesh is used, this flag determines if
            % the error is computed for the complete spaceTime element or just 
            % a slice. The slice is set by the 'position' input parameter
            if (System.settings.time.method~=TimeMethod.SpaceTime || position==-1)
                completeElement = true;
            else
                completeElement = false;
            end
            
            integrateHO = true;
            
            for e=s.mesh.eRange
                
                element = s.mesh.getElement(e);
                el = element.localID;
                
                if integrateHO
                    element.setFinite( System.getIntegrationDisc );
                end
                
                valActive = s.active.getValue(Derivative.value,el);
                valExact  = exact.getValue(Derivative.value,el);
                    
                    %valActive = element.getH * s.active.alpha{ el }(:);
                    %valExact  = finiteElement.H * exact.alpha{ el }(:);
                    data = (valActive - valExact).^2;
%                 else
%                     data = (s.active.value{ el }(:) - exact.value{ el }(:)).^2;
%                 end
                
                data = reshape( data, element.finite.qSize );
                
                if (completeElement)
                    w = element.finite.W;
                    errorValue = errorValue + data(:)' * w * (element.J) ;
                else
                    % Compute the error of each time slice if spaceTime is
                    % used and requested as such
                    wSpatial = element.finite.Wspatial;
                    
                    % reset errorValue to zero at first element
                    if e==s.mesh.eRange(1)
                        errorValue = zeros(element.finite.Q(end),1);
                    end
                    
                    for t=1:finiteElement.Q(end)
                        if (numel(size(data))==2)
                            plane = data(:,t);
                        elseif (numel(size(data))==3)
                            plane = data(:,:,t);
                        end
                        errorValue(t) = errorValue(t) + plane(:)' * wSpatial * (element.JSpatial) ;
                    end
                end
                
                if integrateHO
                    element.setFinite(); % reset to simulation finite element
                end
            end
            
            % Sum errors from all other processes
            errorValue = NMPI.Allreduce(errorValue,numel(errorValue),'+');
            newError = sqrt(errorValue); 
            
            % Compute the relative error
            s.relError = abs(newError - s.error) / newError;
            
            % Update the error in this field
            s.error = newError;
            
            if (NMPI.instance.rank==0)
                if (numel(s.error)==1)
                    fprintf('Total error %3s: %e (%e)\n',s.name,s.error,s.relError);
                else
                    fprintf('Total error %3s: %e / %e\n',s.name,s.error(1),s.error(end));
                end
            end
        end
        
        function computeRelativeError(s,position)    
            error = 0;
            sum   = 0;
            
            if (nargin==1)
                position = 0;
            end
            
            exact = s.subFieldList{ FieldType.Exact };
            
            if (position>=0) 
                % For a time slice 
                for e=s.mesh.eRange
                    element = s.mesh.getElement(e);  
                    data = (s.active.value{ element.localID }(:) - exact.value{ element.localID }(:)).^2;
                    data = reshape(data,size(exact.value{ element.localID }));
                    
                    %wxy = kron(element.finite.wy,element.disc.wx);
                    wxy = element.getW_spatial;
                    
                    if position == 0
                        % time level 0 of element
                        plane = data(:,:,1);
                    else
                        plane = data(:,:,end);
                    end
                    error = error + plane(:) .* wxy; %* (element.J(1)*element.J(2)) ;
                end
            else
                % For the entire slab
                P = System.settings.stde.Pn;
                Q = System.settings.comp.integration;
                sDim = System.settings.stde.sDim;
                C = System.settings.stde.C;
                spaceTime = System.settings.time.spaceTime;
                discIntegration = LSQdisc_4(P,Q,C,sDim,spaceTime);
                
%                 discIntegration = element.disc;
                
                for e=s.mesh.eRange              
%                     data = (s.current.value{e}(:) - s.exact.value{e}(:)).^2;
%                     data = reshape(data,size(s.exact.value{e}));

                    element = s.mesh.getElement(e);
                    
                    Ze = s.mesh.Z{e};

                    data = (discIntegration.getH*Ze*s.current.alpha{ element.localID }(:) - discIntegration.getH*Ze*s.exact.alpha{ element.localID }(:)).^2;
                    data = reshape(data,discIntegration.Q);
                    
                    wxyz = superkron(discIntegration.wz,discIntegration.wy,discIntegration.wx);
                    error = error + data(:)' * wxyz * prod(element.J) ;
                end
            end
             
            if (numel(error)==1)
                error = NMPI.Allreduce(error,1,'+');
            else
                error('LSQDIM:Field','Wrong number of elements in error (%d elements where 1 is expected)',numel(error));
            end
            
            if (position>=0) 
                for e=s.mesh.eRange
                    element = s.mesh.getElement(e);
                    
                    data = (exact.value{ element.localID }(:)).^2;
                    data = reshape(data,size(exact.value{e}));
                    
                    %wxy = kron(element.disc.wy,element.disc.wx);
                    wxy = element.getW_spatial;
                    
                    if position == 0
                        % time level 0 of element
                        plane = data(:,:,1);
                    else
                        plane = data(:,:,end);
                    end
                    sum = sum + plane(:) .* wxy;% * (element.J(1)*element.J(2)) ;
                end
            else
                for e=1:s.mesh.numLocalElements                
%                     data = (s.exact.value{e}(:)).^2;
%                     data = reshape(data,size(s.exact.value{e}));
                    Ze = s.mesh.Z{e};

                    data = (discIntegration.getH*Ze*s.exact.alpha{e}(:)).^2;
                    data = reshape(data,discIntegration.Q);
                    
                    element = s.mesh.getLocalElement(e);
                    wxyz = superkron(discIntegration.wz,discIntegration.wy,discIntegration.wx);
                    sum = sum + data(:)' * wxyz * prod(element.J) ;
                end
            end  
            
            if (numel(sum)==1)
                sum = NMPI.Allreduce(sum,1,'+');
            else
                error('LSQDIM:Field','Wrong number of elements in sum (%d elements where 1 is expected)',numel(sum));
            end
            
            s.error    = sqrt(error);
            s.relError = sqrt(error) / sqrt(sum);
            
            if (NMPI.instance.rank==0)
                fprintf('Total / relative error %3s: %e / %e\n',s.name,s.error,s.relError)
            end
        end        
        
%         function computeMaxValue(s)
%             l1 = length(s.value);       
%             s.maxValue = 0;            
%             for i=1:l1
%                 s.maxValue = max(s.maxValue, abs(s.value{i}(:)));
%             end
%             
%             if numel(s.maxValue)>1
%                 %rank = System.rank;
%                 %save(['parallel.' int2str(System.rank) '.mat']);
%                 disp('There are more than 1 values in Field.computeMaxValue')
% %                 exit
%             end
%             
%             %disp('maxValue in Field')
%             
%             if (numel(s.maxValue)==1)
%                 s.maxValue = NMPI_Allreduce(s.maxValue,1,'M',NMPI.instance.mpi_comm_world);
%             else
%                 error('LSQDIM:SubField:computeMaxValue','Problem with NMPI_Allreduce, maxValue has %d elements where 1 is expected',numel(maxValue))
%             end
%             
%             s.maxValue
%         end
        
        function out = getMaxValue(s)
            
            %disp('getMaxValue in Field')
            
%             s.current.computeMaxValue;
%             out = s.current.maxValue;
            
            s.subFieldList{1}.computeMaxValue;
            out = s.subFieldList{1}.maxValue;
        end
        
        function out = getError(s)
            out = s.error;
        end
        
        function out = getRelativeError(s)
            out = s.relError;
        end
        
        % PARALLEL FUNCTIONS
        
        % This function merges the local alphaArrays into a global one on
        % the master rank. It is used to write the global alpha array to a
        % solution/restart file. Since some alpha values are shared between
        % a certain number of ranks, the dofRank array is used to scale the
        % values locally before they are send to the master rank.
        function out = getGlobalAlpha(s)
            out = zeros(s.mesh.dh.numDof,1);
            
            scaling =  s.mesh.dh.dofRankArray(:,NMPI.instance.rank+1) ./ sum(s.mesh.dh.dofRankArray,2);
            
            eStart = s.mesh.eStart;
            eEnd   = s.mesh.eEnd;
            
            %dbstack
            %save(['getGlobalAlpha.' int2str(System.rank) '.mat']);
            
            if (s.subFieldList{1}.needsMeshUpdate)
                %fprintf('Field.getGlobalAlpha on rank %d\n',System.rank)
                s.subFieldList{1}.meshUpdate;
            end
            
            for e=eStart:eEnd
                out( s.mesh.dh.dofs{e}(:) ) = s.subFieldList{1}.alpha{e-eStart+1};
            end
                
%             tmp = s.mesh.dofRank;
%             save(['field' s.name num2str(NMPI.instance.rank) '.mat'],'out','scaling','tmp');
            
            out = scaling .* out;
            
%             save(['field' s.name num2str(NMPI.instance.rank) '.mat'],'out','scaling');
            
            % If the values are only required on the master rank (0):
            out = NMPI.Reduce(full(out),length(out),'+',0);
            
            % If the values are required on all ranks:
            % out = NMPI_Alleduce(full(out),length(out),'+',NMPI.instance.mpi_comm_world);
            
            if (NMPI.instance.rank==0)
                s.globalAlphaArray = out;
            end
        end
        
        function setName(s,name)
            s.name = name;
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % Use 'power' of order n on the field values
        function out = power(s,n)
            numElements = numel(s.value);
            out = Field(s.mesh,0,'default');
            out.resetValue;
            out.wi = 1;
            out.wc = 1;
            for e=1:numElements
                out.value{e} = s.value{e}.^n;
            end
        end
        
        % Sum the values of two field
        function out = plus(s1,s2)
            if (numel(s1.value)~=numel(s2.value))
                error('The number of elements should be equal for a summation')
            end
            out = Field(s1.mesh,0,'default');
            out.resetValue;
            out.wi = 1;
            out.wc = 1;
            numElements = numel(s1.value);
            for e=1:numElements
                out.value{e} = s1.value{e} + s2.value{e};
            end
        end 
        
% %         % Overload the subsref, which allows access to octTree.data
% %         % directly through octTree()
% %         function varargout = subsref(obj,S)
% %             if ( strcmp(S(1).type,'()') )
% %                 if ( isempty( S(1).subs{1} ) )
% %                     varargout{1} = obj;
% %                 elseif ( S(1).subs{1}==FieldType.Nonlinear )
% %                     varargout{1} = obj.nonlinear;
% %                 elseif ( S(1).subs{1}==FieldType.Interpolated )
% %                     varargout{1} = obj.interpolated;
% %                 elseif ( S(1).subs{1}==FieldType.Coupled )
% %                     varargout{1} = obj.coupled;
% %                 elseif ( S(1).subs{1}==FieldType.Exact )
% %                     varargout{1} = obj.exact;
% %                 elseif ( S(1).subs{1}==FieldType.Current )
% %                     varargout{1} = obj.current;
% %                 else
% %                     [varargout{1:nargout}] = builtin('subsref',obj,S);
% %                 end
% %             elseif ( strcmp(S(1).type,'{}') )                                                           % FIELD{ 1 }
% %                 if (length(S)==1)
% %                     [varargout{1:nargout}] = obj.getValue( S(1).subs{1} );
% %                 elseif ( strcmp(S(2).type,'()') )                                                     	% FIELD{ 1 }( 2 )
% %                     if (length(S(2).subs)==3)
% %                         [varargout{1:nargout}] = obj.getValueArrayFormat( S(1).subs{1}, S(2).subs );
% %                     else
% %                         [varargout{1:nargout}] = obj.getValue( S(1).subs{1}, S(2).subs{1} );
% %                     end
% %                     
% %                 elseif ( strcmp(S(2).type,'.') )    
% %                     if ( strcmp(S(2).subs,'value') && strcmp(S(3).type,'()') )                          % FIELD{ 1 }.value( 3 )
% %                         if (length(S(3).subs)==3)
% %                             [varargout{1:nargout}] = obj.getValueArrayFormat( S(1).subs{1}, S(3).subs );
% %                         else
% %                             [varargout{1:nargout}] = obj.getValue( S(1).subs{1}, S(3).subs{1} );
% %                         end
% %                     elseif ( strcmp(S(2).subs,'prescribed') )                                           % FIELD{ 1 }.prescribed
% %                         if ( length(S)==3 && strcmp(S(3).type,'()') )   
% %                             [varargout{1:nargout}] = obj.getPrescribedArray( S(1).subs{1} );
% %                         else
% %                             [varargout{1:nargout}] = obj.getPrescribed( S(1).subs{1} );
% %                         end
% %                     elseif ( strcmp(S(2).subs,'moveSolution') )                                         % FIELD{ e }.moveSolution
% %                         if ( length(S)==3 && strcmp(S(3).type,'()') )   
% %                             [varargout{1:nargout}] = obj.moveSolution( S(1).subs{1}, S(3).subs{1} );
% %                         else
% %                             error('unsupported subsref in FIELD')
% %                         end
% %                     elseif ( strcmp(S(2).subs,'x') )                                                    
% %                         if ( length(S)==3 && strcmp(S(3).type,'()') )                                   % FIELD{ 1 }.x( 3 )
% %                             [varargout{1:nargout}] = obj.xDerivative( S(1).subs{1}, S(3).subs{1} );
% %                         else                                                                            % FIELD{ 1 }.x
% %                             [varargout{1:nargout}] = obj.xDerivative( S(1).subs{1} );
% %                         end                        
% %                     elseif ( strcmp(S(2).subs,'y') )
% %                         if ( length(S)==3 && strcmp(S(3).type,'()') )                                   % FIELD{ 1 }.y( 3 )
% %                             [varargout{1:nargout}] = obj.yDerivative( S(1).subs{1}, S(3).subs{1} );
% %                         else                                                                            % FIELD{ 1 }.y
% %                             [varargout{1:nargout}] = obj.yDerivative( S(1).subs{1} );
% %                         end
% %                     elseif ( strcmp(S(2).subs,'z') )
% %                         if ( length(S)==3 && strcmp(S(3).type,'()') )                                   % FIELD{ 1 }.y( 3 )
% %                             [varargout{1:nargout}] = obj.zDerivative( S(1).subs{1}, S(3).subs{1} );
% %                         else                                                                            % FIELD{ 1 }.y
% %                             [varargout{1:nargout}] = obj.zDerivative( S(1).subs{1} );
% %                         end
% %                     elseif ( strcmp(S(2).subs,'xx') )                                                    
% %                         if ( length(S)==3 && strcmp(S(3).type,'()') )                                   % FIELD{ 1 }.x( 3 )
% %                             [varargout{1:nargout}] = obj.xxDerivative( S(1).subs{1}, S(3).subs{1} );
% %                         else                                                                            % FIELD{ 1 }.x
% %                             [varargout{1:nargout}] = obj.xxDerivative( S(1).subs{1} );
% %                         end   
% %                     elseif ( strcmp(S(2).subs,'alpha') && strcmp(S(3).type,'()') )                      % FIELD{ 1 }.alpha( 3 )
% %                         if (length(S(3).subs)==3)
% %                             [varargout{1:nargout}] = obj.getAlphaArrayFormat( S(1).subs{1}, S(3).subs );
% %                         else
% %                             [varargout{1:nargout}] = obj.getAlpha( S(1).subs{1}, S(3).subs{1} );
% %                         end
% %                     end
% %                 else
% %                     [varargout{1:nargout}] = builtin('subsref',obj,S);
% %                     %error('unsupported subsref in FIELD')
% %                 end
% %                 
% %             elseif ( strcmp(S(1).type,'.') )
% %                 if ( strcmp(S(1).subs,'value') && strcmp(S(2).type,'{}') )                          % FIELD.value{ elementNumber }( 3 )
% %                     if (numel(S)==3 && strcmp(S(3).type,'()') && length(S(3).subs)==3)
% %                         [varargout{1:nargout}] = obj.getValueArrayFormat( S(2).subs{1}, S(3).subs );
% %                     else
% %                         [varargout{1:nargout}] = builtin('subsref',obj,S);
% %                     end
% %                 elseif ( strcmp(S(1).subs,'x') && numel(S)==1 )
% %                     s.getValue(Derivative.x);
% %                 else
% %                     [varargout{1:nargout}] = builtin('subsref',obj,S);
% %                 end
% %                 
% %             else
% %                 [varargout{1:nargout}] = builtin('subsref',obj,S);
% %             end
% %         end
        
%          function a = subsasgn(a,s,elementalValueArray)
%              if strcmp(s(1).subs,'value') && strcmp(s(2).type,'{}')
%                  e = s(2).subs{1};
%                  a.setValue(e,elementalValueArray);
%              end
%          end

        function out = get.name(s)
            out = s.name;
        end
        
%         function out = get.maxValue(s)
%             out = s.current.maxValue;
%         end
        
        function out = getNumDof(s)
            out = s.numDof;
        end
        
        function out = getSubField(s,fieldType)
            if nargin==1 || isempty(fieldType)
                out = s.activesubFieldList{1};
            elseif ( isa(fieldType,'FieldType') )
                out = s.subFieldList{ fieldType };
            else
                error('LSQDIM:field:getSubField :: Please provide a valid fieldType');
            end            
        end
% 
%         % Function to return the number of levels of a subField. For
%         % nonlinear and coupled subFields the minimum of levels is 2 (if
%         % they are used at all), since nonlinear and coupled convergence
%         % checks needs at least one field as a reference.
%         function out = getNumLevels(s,fieldType)
%             switch fieldType
%                 case FieldType.Nonlinear
%                     out = numel(s.nWeight);
%                     if s.nWeight(1)==0
%                         out = 0;
%                     end
%                     out = max(out,2);
%                 case FieldType.Coupled
%                     out = numel(s.cWeight);
%                     if s.cWeight(1)==0
%                         out = 0;
%                     else
%                         out = max(out,2);
%                     end
%                 case FieldType.TimeLevel
%                     out = numel(s.tWeight);
%                     if s.tWeight(1)==0
%                         out = 0;
%                     end
%             end
%         end
        
        function out = save(s)
            % MMKK :: 02/05/2019
            % TODO: Change to only saving the current (#1) subField,
            % so do not save the nonlinear and coupled subFields.
            
            out.name        = s.name;
            %out.mesh        = save(s.mesh);
            for i=1:numel(s.subFieldList)
                if ~isempty(s.subFieldList{i})
                    out.subFields{i} = save( s.subFieldList{i} );
                else
                    out.subFields{i} = [];
                end
            end
        end
        
        function load(s,data)
            if (isequal(s.name,data.name))
                for i=1:numel(data.subFields)
                    if (~isempty(data.subFields{i}))
                        if (isempty(s.subFieldList{i}))
                            s.addSubField(i,false);
                        end
                        s.subFieldList{i}.load( data.subFields{i} );
                    end
                end
                
                % Old time levels
                if ( isfield( data, 'oldTimeLevels' ) ) 
                    for i=1:numel(data.oldTimeLevels)
                        if (~isempty(data.oldTimeLevels{i}))
                            if (isempty(s.subFieldList{i}))
                                s.addSubField(i,false);
                            end
                            s.oldTimeLevels{i} = SubField(s,'oldTimeLevel',s.mesh,s.dh,0);
                            s.oldTimeLevels{i}.load( data.oldTimeLevels{i} );
                        end
                    end % for
                end % if
            else
                error('The variable names of the existing field and the restart field are not identical\n')
            end
        end
        
        function set.plotOrder(s,value)
            s.plotOrder = value;
        end
    end
    
    methods (Access=private)
        function position = getSubFieldListPosition(s)
            numSubFields = numel(s.subFieldList);
            position     = numSubFields+1;
            
            % initialize the new position
            s.subFieldList{position,1} = [];
        end
        
        function idx = getSubFieldNameIndex(s,subFieldName)
            availableNames = cellfun( @(x) x.name, s.subFieldList,'UniformOutput',false);
            idx = find(strcmp(availableNames,subFieldName));
        end
    end
    
    methods(Static)

    end
    
end

