classdef FieldType < int32
   enumeration
      Current(1), Nonlinear(2), Coupled(3), Exact(4), Residual(5), Interpolated(6), Timelevel1(7), TimeLevel2(8);
   end
end