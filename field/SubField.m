classdef SubField < handle
    %SubField is the most basic element of a Field
    %   A SubField is a single solution of a Field. The Field contains all
    %   information of one variable, and a subField contains for example 
    %   the solution at a specific time level, the (temporary) nonlinear 
    %   solution and the solution related to the coupling with other
    %   equations. 
    
    properties(Access=private)
        norms;
        isConverged;
        isEmpty;        
        
        activeMesh;
        
        performElementNumberCheck = false;
        
        useAlphaSolver;
    end
    
    properties (GetAccess=public, SetAccess=public)
        alpha, localAlpha;
        value, localValue;      % physical solution at time level n (per element)
         
        Lv;                 % Used as a fingerprint to check if an update is required
        
        maxValue;
        field;              % superclass of this SubField
        name;               % name of this subField
        fullname;
        
        mesh;
        dh;
        numDoF;
        meshID;
        
        weight;             % relaxation parameter
        
        type;
        
        numElements;
        
        isInterpolated = false;
        
        convergence;
        
        definedFunction;
        
        relaxation;
    end
    
    methods (Access = ?Field)  %Only Field is allowed to construct a SubField
        function s = SubField(field,varargin)
            
            % Process required parameter
            s.field = field;
            
            % set defaults for optional inputs and verify the input
            default = {'subFieldName',field.mesh,field.dh,0,true};
            [s.type,s.mesh,s.dh,s.definedFunction,s.useAlphaSolver] = VerifyInput(varargin,default);
            
            if (~isstring(s.type))
                s.name = char(s.type);
            else
                s.name = s.type;
            end
            
            s.weight = 1;
            
            s.fullname  = [ s.field.name '.' s.name ];
            
            s.meshID = s.mesh.Lv;
            
            s.setup();
        end
        
        function setup(s)
            dofHandler   = s.mesh.getDoFHandler(s.field.periodic);
            
            s.localAlpha = zeros(dofHandler.numLocalDof,1);
            s.localValue = zeros(dofHandler.numLocalDof,1);
            
            s.alpha      = cell(s.mesh.numLocalElements,1);
            s.value      = cell(s.mesh.numLocalElements,1);
            
            if isa(s.definedFunction, 'function_handle')
                % First compute the values
                id = 0;
                for e=s.mesh.eRange
                    id = id+1;
                    element         = s.mesh.getElement(e);
                    if (nargin( s.definedFunction )>1)
                        [X,Y,Z,T]       = element.getNodes;                    
                        functionValues  = s.definedFunction(X,Y,Z,T);
                    else
                        functionValues  = s.definedFunction(element);
                    end
                    s.value{ id } 	= functionValues;
                end
                
                % Next compute the alpha / localAlpha
                s.computeAlpha;
                
            elseif (s.definedFunction==0)
                s.reset()
                
%                 for e=s.mesh.eRange
%                     element         = s.mesh.getElement(e);
%                     s.alpha{ e }    = zeros(element.disc.dofe);
%                     s.value{ e }    = zeros(element.disc.dofeq);
%                 end
            end
        end

        % Function to reset both value and alpha cell arrays
        function reset(s)
            s.value = cell(s.mesh.numLocalElements,1);
            s.alpha = cell(s.mesh.numLocalElements,1);  
            
            % Get the number of quadrature points and dofs in each 
            % direction, and initialize both to zero
            id = 0;
            for e=s.mesh.eRange
                id = id+1;
                element = s.mesh.getElement(e);                
                s.alpha{id} = zeros( element.finite.dSize );
                s.value{id} = zeros( element.finite.qSize );
            end
            
            s.meshID    = s.mesh.Lv;
            s.Lv        = s.mesh.Lv;            
            s.isEmpty   = true;
        end
        
        function out = computeValue(s)
            out = cell(s.mesh.numLocalElements,1);
            id = 0;
            for e=s.mesh.eRange
                id = id+1;
                out{id}  = s.mesh.H(e) * ( s.mesh.Z(e) * s.alpha{id}(:) );
            end
        end
        
        function updateValue(s)
            s.value = s.computeValue;
        end
        
        % Function to assign a (manufactured) solution to this subField. The
        % passed argument needs to be a function handle. After initializing
        % the field (reset), the values are set. Next, alpha(Array) are
        % determined from these values.
        %
        % fh    : function_handle which provides the value at location (x,y,z)
        %
        function assignSolution(s,fh)
            if (~isa(fh,'function_handle'))
                error('A function should be provided to be able to assign a manufactured solution')
            end
            
            s.weight = 1;
            s.reset;
            
            for e=1:s.field.getNumElements
                element = s.mesh.getElement(e);
                try
                    s.value{ element.localID } = fh(s.field.variable,element.xelem,element.yelem,element.zlocation);
                catch 
                    switch element.disc.DIM
                        case 1
                            [X] = ndgrid(element.nodes{:});
                            
                            if (nargin(fh)~=1 && e==1)
                                warning('Please adjust the function handle so it depends on 1 variable, now zero values are assumed')
                            end
                            
                            var = cell(nargin(fh),1);
                            var{1} = X;
                            for i=2:nargin(fh)
                                var{i} = 0;
                            end
                            
                            s.value{ element.localID } = fh(var{:});
                        case 2
                            [X,Y] = ndgrid(element.nodes{:});
                            
                            if (nargin(fh)~=2 && e==1)
                                warning('Please adjust the function handle so it depends on 2 variables, now zero values are assumed')
                            end
                            
                            var = cell(nargin(fh),1);
                            var{1} = X;
                            var{2} = Y;
                            for i=3:nargin(fh)
                                var{i} = 0;
                            end
                            s.value{ element.localID } = fh(var{:});
                        case 3
                            [X,Y,Z] = ndgrid(element.nodes{:});
                            
                            if (nargin(fh)~=2 && e==1)
                                warning('Please adjust the function handle so it depends on 3 variables, now zero values are assumed')
                            end
                            
                            var = cell(nargin(fh),1);
                            var{1} = X;
                            var{2} = Y;
                            var{3} = Z;
                            for i=4:nargin(fh)
                                var{i} = 0;
                            end
                            s.value{ element.localID } = fh(var{:});
                        otherwise
                            error('Unsupported option!')
                    end
                end
            end
            
            s.localAlpha = zeros(s.mesh.getNumDof,1);
            
%             s.computeAlphaMG();
            
            s.computeAlpha
            
            %s.convertAlpha(1);
        end
        
        function setNorms(s,norms)
            s.norms = norms;
        end
        
        function out = getNorms(s)
            out = s.norms;
        end
        
        function copy(s,subField)
            %fprintf('Copy subfield on rank %d\n',System.rank)
            
            s.alpha         = subField.alpha;
            s.value         = subField.value;
            s.localAlpha    = subField.localAlpha;
            %s.eRange        = subField.eRange;
            s.meshID        = subField.meshID;
            s.numDoF        = subField.numDoF;
            
            if ( s.needsMeshUpdate )
                s.meshUpdate(s.mesh);
            end
        end
        
        function copy_add(s,subField)
            %fprintf('Copy subfield on rank %d\n',System.rank)
            
            for n=1:numel(s.alpha)
                s.alpha{n} = s.alpha{n} + subField.alpha{n};
                s.value{n} = s.value{n}(:) + subField.value{n}(:);
            end
            
            s.localAlpha    = s.localAlpha + subField.localAlpha;
            %s.eRange        = subField.eRange;
            s.meshID        = subField.meshID;
            s.numDoF        = subField.numDoF;
            
            if ( s.needsMeshUpdate )
                s.meshUpdate(s.mesh);
            end
        end
        
        function out = getSize(s)
            out = size(s.alpha,1);
        end
        
        function update(s,solutionMode)
            if (nargin==1)
                dofHandler      = s.mesh.getDoFHandler( s.field.periodic );
                s.field.numDof  = dofHandler.numLocalDof;
            else
                switch solutionMode
                    case SolutionMode.Nonlinear
                        s.copyNonLinear;
                    case SolutionMode.Coupling
                        s.copyCoupling;
                end
            end
        end
        
        function extrapolateSolution(s,oldTimeLevels,extrapolationOrder)
            % Function to extrapolate previous solution towards a future
            % solution
            
            switch extrapolationOrder
                case 1
                    
                case 2
                    for e=1:numel(s.alpha)
                        s.alpha{ e } = 2 * oldTimeLevels{1}.alpha{e} - oldTimeLevels{2}.alpha{e};
                    end
                    
                    s.value = s.computeValue;               % compute the values based on the new elemental alpha
                    s.convertAlpha(0);       % convert alpha -> alphaArray
                    
                otherwise
                    error('SubField.extrapolateSolution :: Unsupported extrapolation order')
            end
                
        end
        
        function setSubField(s,basis)
            s.copy(basis);
%             
%             if (~s.isInterpolated)
%                 % Copy alpha & value cell arrays
%                 s.copy(basis);
%             end
%             
%             if (s.isInterpolated || ~isequal(s.mesh.Lv{2},s.mesh.octTree.getLv) )
%                 s.copy(basis);
%             end
        end

        function cellArray = redistributeCellArray(s,cellArray)
            
%             if (any( cellfun(@(x) isempty(x),cellArray) ))
%                 error('SubField:redistributeCellArray','There are empty cells in the cellArray!')
%             end
            
            % Sync data about array_in. Data contains the array size of the
            % cell (column 1), the rank which contains the data now (column 2). 
            data = zeros(numel(cellArray),2);
            for e=1:numel(cellArray)
                if (~isempty(cellArray{e}))
                    data(e,1) = numel(cellArray{e});
                    data(e,2) = System.rank;
                end
            end
            %data(s.mesh.eRange,3) = System.rank;
            
            if (System.settings.anal.debug)
                %if (System.rank==0)
                    fprintf('Before NMPI.Allreduce in SubField.redistributeCellArray on rank %d (variable : %s)\n',System.rank,s.fullname)
                %end
                %save(['redistributeCellArray.' int2str(System.rank) '.mat']);
%                 NMPI_Barrier;
            end
            
            %fprintf('rank %d has %d elements to sync\n',System.rank,numel(data))
            
            %NMPI_Barrier;
            
            %save(['redistributeCellArray.' int2str(System.rank) '.mat']);
            
            data = NMPI.Allreduce(data,numel(data),'+','redistributeCellArray');
            data = reshape(data,[numel(cellArray),2]);
            
            if (System.settings.anal.debug)
                %if (System.rank==0)
                    fprintf('After NMPI.Allreduce in SubField.redistributeCellArray on rank %d (variable : %s)\n',System.rank,s.fullname)
                %end
                %save(['redistributeCellArray.' int2str(System.rank) '.mat']);
%                 NMPI_Barrier;
            end
            
%             rank = System.rank;
%             save(['data.before.' int2str(System.rank) '.mat']); 
            
            % Setup send/recv of elemental data between ranks
            rank            = System.rank;
            elementNumbers  = (1:s.mesh.numElements)';                     	% Simple array with all elementNumbers (global)
            elementRanks    = s.mesh.getElementRanks';                      % 
            localElement    = elementRanks==rank;                           % logical array with local element positions
            dataSize        = data(:,1);                                    % Size of each element
            
            % Recv info            
            recvFlags    = and(data(:,2)~=rank,localElement);               % determine if the element is not on this rank, but is a local element 
            recvSize     = data(recvFlags,1);                               % size of the elements to recv
            recvNumbers  = elementNumbers(recvFlags);                       % numbers of the elements to recv
            source       = data(recvFlags,2);                               % ranks where to recv the elements from
            
            % Send info
            sendFlags    = and(data(:,2)==rank,~localElement);              % determine if the element is on this rank, but not a local element
            sendElements = cellArray(sendFlags);                            % data of elements to send
            sendNumbers  = elementNumbers(sendFlags);                       % numbers of the elements to send
            destination  = elementRanks(sendFlags);                         % ranks where to send the elements to
            
            %save(['data.' int2str(System.rank) '.mat']); 
            
            for i=0:System.nproc-1
                
                if (i~=rank)
                    % Send to other ranks, but only if there is something
                    % to send to that specific rank
                    ids = sendNumbers(destination==i);
                    
                    if (~isempty(ids))
                        package = cellfun(@(x) x(:), sendElements(destination==i) ,'UniformOutput',false);
                        package = cat( 1, package{:} );
                        %fprintf('Variable %s.%s :rank %d will send elements %d-%d to rank %d (package size %d))\n',s.field.name,s.name,rank,ids(1),ids(end),i,numel(package))
                        NMPI.Send(package,numel(package),i);
                        %fprintf('Variable %s.%s :rank %d send done\n',s.field.name,s.name,rank)  
                    end
                    
                elseif (i==rank)
                    for j=0:System.nproc
                        % Recv from other ranks, but only if there is something
                        % to recv from that specific rank
                        ids = recvNumbers(source==j);
                    
                        if (~isempty(ids))
                            
                            %fprintf('Variable %s.%s :rank %d will recv elements %d-%d from rank %d (package size %d))\n',s.field.name,s.name,rank,ids(1),ids(end),j,sum(dataSize(ids)))
                            package = NMPI.Recv(sum(recvSize),j);
                            %fprintf('Variable %s.%s :rank %d recv done\n',s.field.name,s.name,rank)                            
                            
                            % The 'package' contains one array with all data 
                            % for local elements, this data needs to be split 
                            % and assigned to single elements
                            offset = 0;
                            for e=ids'
                                cellArray{e} = package( offset+1:offset+dataSize(e) );
                                try
                                    element = s.mesh.getElement(e);
                                    cellArray{e} = reshape(cellArray{e},s.mesh.getElement(e).finite.dofSize);
                                catch Mexp
%                                     if (System.rank==0)
%                                         save('reshape.mat');
%                                         fprintf('The number of elements in cellArray{e} is %d\n',numel(cellArray{e}))
%                                         fprintf('The number of the reshape is %d\n',prod(s.mesh.getElement(e).disc.dofe))
%                                         fprintf('\n')
%                                         fprintf('The P points have %d elements\n',prod(s.mesh.getElement(e).disc.dofe))
%                                         fprintf('The Q points have %d elements\n',prod(s.mesh.getElement(e).disc.Q))
%                                     end
                                    save(['redistributeCellArray2.' int2str(System.rank) '.mat']);
                                    if (System.nproc>1)
                                        exit
                                    end
                                end
                                offset = offset + dataSize(e);
                            end
                            
                        end
                    end
                end
                
            end
            
% %             % Sync the actual data
% %             for e=1:numel(cellArray)
% %                 if ( ismember(e,s.mesh.eRange) && isempty(cellArray{e}) )
% %                     % receive from other rank
% %                     %fprintf('Receiving data on rank %d for element %e\n',System.rank,e)
% %                     
% %                     %if (System.settings.anal.debug)
% %                         fprintf('rank %d will recv element %d from rank %d (package size %d)\n',System.rank,e,data(e,2),data(e,1))
% %                     %end
% %                     
% %                     %fprintf('Recv element %d\n',e);
% %                     
% %                     cellArray{e} = NMPI_Recv(data(e,1),data(e,2));
% %                     cellArray{e} = reshape(cellArray{e},s.mesh.getElement(e).disc.Q);
% %                     
% %                     %fprintf('Done receiving data on rank %d for element %e\n',System.rank,e)
% %                 elseif ( ~ismember(e,s.mesh.eRange) && ~isempty(cellArray{e}))
% %                     % send to other rank
% %                     %fprintf('Sending data from rank %d for element %e\n',System.rank,e)
% %                     
% %                     %if (System.settings.anal.debug)
% %                         fprintf('rank %d will send element %d to rank %d (package size %d))\n',System.rank,e,s.mesh.getElementRanks(e),data(e,1))
% %                     %end
% %                     
% %                     %fprintf('Send element %d\n',e);
% %                     NMPI_Send(cellArray{e},data(e,1),s.mesh.getElementRanks(e));
% %                     
% %                     
% %                     %fprintf('Done sending data from rank %d for element %e\n',System.rank,e)
% %                 end
% %             end
            
%             rank = System.rank;
%             save(['data.after.' int2str(System.rank) '.mat']);            
            
        end
        
        function out = createRefinementTree(s,Lv,numBaseElements,currentLv,version)
            
            if (nargin<5)
                version = 0;
            end
            
            if (version==1)
                out = cell(numBaseElements,1);
                e = 1;

                for n=1:numBaseElements
                    eStart = e;
                    out{n} = s.mesh.data(n).getLvTree;
                    volume = 0;

                    baseElementVolume = s.mesh.data(n).getVolume;
                    
                    while volume < baseElementVolume
                       volume = volume + volumeChild^(Lv(e)-currentLv);
                       e = e+1;
                    end
                    if max(Lv(eStart:e-1))>currentLv
                        out{n} = s.createRefinementTree( Lv(eStart:e-1), numChild, currentLv+1 );
                    else
                        out{n} = Lv(eStart:e-1);
                    end
                end
                
            elseif (version == 0)
                out = cell(numBaseElements,1);
                e = 1;

                numChild    = prod(System.settings.mesh.refinementRatio);
                volumeChild = 1/numChild;

                for n=1:numBaseElements
                    eStart = e;
                    volume = 0;

                    baseElementVolume = s.mesh.data(n).getVolume;

                    while volume < baseElementVolume
                       volume = volume + volumeChild^(Lv(e)-currentLv);
                       e = e+1;
                    end
                    if max(Lv(eStart:e-1))>currentLv
                        out{n} = s.createRefinementTree( Lv(eStart:e-1), numChild, currentLv+1, 0 );
                    else
                        out{n} = Lv(eStart:e-1);
                    end
                end
                
            elseif (version==2)
                out = cell(numBaseElements,1);
                e = 1;

                numChild    = prod(System.settings.mesh.refinementRatio);
                volumeChild = 1/numChild;

                elementVolume = volumeChild.^(Lv-currentLv);
                
                for n=1:numBaseElements
                    eStart = e;
                    volume = 0;

                    %baseElementVolume = s.mesh.data(n).getVolume;
                    baseElementVolume = 1;
                    
                    while volume < baseElementVolume
                       volume = volume + elementVolume(e);
                       e = e+1;
                    end
                    if max(Lv(eStart:e-1))>currentLv
                        out{n} = s.createRefinementTree( Lv(eStart:e-1), numChild, currentLv+1, 2 );
                    else
                        out{n} = Lv(eStart:e-1);
                    end
                end
            
            end
        end
        
        
        
        function [out,id] = createCellArrayTree2(s,cellArray,eRange,refinementTree,id)
            out = refinementTree;
            
%             if (numel(cellArray)~=numel(refinementTree))
%                 globalCellArray = cell(numel(refinementTree),1);
%                 globalCellArray(eRange) = cellArray;
%                 cellArray = globalCellArray;
%             end
            
            for e=1:numel(refinementTree)
                if numel(refinementTree{e})~=1
                    [out{e},id] = s.createCellArrayTree2( cellArray, 1, refinementTree{e}, id );
                else
                    %fprintf('numel cellArray : %d -- id == %d\n',numel(cellArray),id);
                    try
                    
                    out{e} = cellArray{id};
                    id=id+1;
                    catch
                        %rank = System.rank;
                        fprintf('Fatal error : problem in createCellArrayTree2\n')
                        save(['createCellArrayTree2.' int2str(System.rank) '.mat']);
                        %error('SubField.createCellArrayTree','Whats up')
                        if (System.nproc>1)
                            exit
                        end
                    end
                end
            end
        end
        
        function out = flattenAlpha(s,alphaTree,materialTree)
            
            numCells = s.countCells(alphaTree);            
            out = cell(numCells,1);
            
            position = 0;
            for e=1:numel(alphaTree)
                if (iscell(alphaTree{e}))
                    numChildren = numel(alphaTree{e});
                    out(position+1:position+numChildren) = alphaTree{e};
                    position = position + numChildren;
                else
                    out{position+1} = alphaTree{e};
                    position = position + 1;
                end
            end
            
%             numCells = s.countCells(out);            
        end
        
        function counter = countCells(s,array)
            counter = 0;
            for e=1:numel(array)
                if (iscell(array{e}))
                    counter = counter + numel(array{e});
                else
                    counter = counter + 1;
                end
            end
        end
        
        
        % Splitting of alpha for refinement step
        function [alpha_out,Lv] = splitAlpha(s,alpha_in,Lv)
            
            disc = s.mesh.getElement(1).finite;
            Z = disc.getZrefine();
            alpha_out = cell(size(Z));
            
            if ~isempty(alpha_in)                
                for i=1:numel(Z)
                    alpha_out{ i } = reshape(Z{i} * alpha_in(:), size(alpha_in));
                end
            else
                for i=1:numel(Z)
                    alpha_out{ i } = alpha_in;
                end
            end
            
            % Increase Lv and return a multidimensional cell array because of refinement 
            newLv = Lv+1;
            Lv = num2cell( newLv*ones(size(Z)) );
        end
        
        % Splitting of alpha for refinement step
        function [var_out,Lv] = splitLogical(s,var_in,Lv)
            
            disc = s.mesh.getElement(1).finite;
            var_out = cell(disc.refinementRatio);
            
            if ~isempty(var_in)                
                for i=1:numel(var_out)
                    var_out{ i } = var_in;
                end
            else
                for i=1:numel(Z)
                    var_out{ i } = var_in;
                end
            end
            
            % Increase Lv and return a multidimensional cell array because of refinement 
            newLv = Lv+1;
            Lv = num2cell( newLv*ones(size(var_out)) );
        end
        
        function out = checkEmptyCells(s,cellArray)
            flags = false(numel(cellArray),1);
            
            for i=1:numel(cellArray)
                flags(i) = isempty(cellArray{i});
            end
            
            out = flags;
        end
        

        function computeAlpha(s,elementNumber)

            useParallel = true;
            useSolver   = s.useAlphaSolver;
            
            dofHandler   = s.mesh.getDoFHandler(s.field.periodic);
            
            s.localAlpha = zeros(dofHandler.numLocalDof,1);
            s.alpha      = cell(numel(s.mesh.eRange),1);
            
            if nargin==1
               elem = s.mesh.eRange;
            else
               elem = elementNumber;
            end
            
            % Compute alpha for each requested element
            id = 0;
            for e=elem
                id = id+1;
                
                element = s.mesh.getElement(e);
                
                Ze  = s.mesh.Z{e};
                W	= element.getW;
                %x
                A   = element.getH * Ze;                
                b   = s.value{ id }(:);
                
                %cols_with_all_zeros     = find(all(A==0));
                cols_with_any_nonzeros  = find(any(A~=0));
                
                x = A(:,cols_with_any_nonzeros) \ b;
                
                s.alpha{ id }(cols_with_any_nonzeros,1) = x;
            end
            
            %filename = ['computeAlpha' int2str(System.rank) '.mat'];
            %save(filename)
            
            % The alphaArray may be discontinuous across element boundaries. 
            % The following iterative cg solver code makes sure it will be 
            % continuous across element boundaries.
            if (nargin==1 && useSolver)
                % Convert the given alpha -> alphaArray
                s.convertAlpha(0);
                
                dofHandler      = s.mesh.getDoFHandler(s.field.periodic);
                
                tempGM          = cell(s.mesh.numLocalElements,1);
                GMflag          = false(dofHandler.numLocalDof,1);
                A               = cell(s.mesh.numLocalElements,1);
                b               = cell(s.mesh.numLocalElements,1);

                id = 0;
                for e=s.mesh.eRange
                    id = id+1;
                    
                    element = s.mesh.getElement(e);                    
                    Ze = s.mesh.Z{e};

                    W   = element.getW;
                    
                    %W   = W .* (0.001.^s.mesh.dh.dofLevels{e}(:));
                    %W   = W .* (0.00001.^element.lvl);
                    
                    opL = element.getH * Ze;
                    
                    sqrtW = sqrt(W);
                    
                    dofFlag( dofHandler.localDofs{id}(:) ) = true;

                    tempValues = s.value{id}(:);

                    %A{id} = H' * ( spdiag(W) * H );
                    %b{id} = H' * ( spdiag(W) * tempValues(:) );
                    
                    try
                        A{id} = dsyrkL( full( (sqrtW) .* opL ) );
                    catch
                        A{id} = opL' * ( sqrtW .* opL );
                    end
                    b{id} = opL' * ( W .* tempValues(:) );
                end            


                alphaTmp = s.localAlpha( dofFlag,1 );            
                numLocalDof = nnz( dofFlag );

                %filename = ['computeAlpha2.' int2str(System.rank) '.mat'];
                %save(filename)
                
                dofHandler 	= s.mesh.getDoFHandler(s.field.periodic);
                tempGM      = dofHandler.localDofs;
                
%                 try
                    t1 = toc;
                    if (useSolver)
                        if (NMPI.instance.nproc>1 || useParallel) %numLocalDof>5000 || 
                            % iterative solver
                            epsilon = 1e-9; silent = true;
                            %s.alphaArray( GMflag,1 ) = PrecElemCG_parallel(A,b,tempGM,0,s.mesh.mutualGM,s.mesh.dofPerVariable,numLocalDof,epsilon,silent,alphaTmp);
                            s.localAlpha = PCG_hybrid(A,b,dofHandler,epsilon,silent,alphaTmp);
                        elseif ( NMPI.instance.nproc==1 )
                            % direct solver
    %                         directSolver = Solver(A,b,tempGM,0,numLocalDof,numLocalDof,alphaTmp);
    %                         s.alphaArray( GMflag,1 ) = directSolver.x;
                            directSolver = Solver_dh(A,b,dofHandler,alphaTmp);
                            s.localAlpha = directSolver.x;
                        else
                            % iterative solver
                            epsilon = 1e-9; silent = true;
                            %s.alphaArray( GMflag,1 ) = PrecElemCG_parallel(A,b,tempGM,0,s.mesh.mutualGM,s.mesh.dofPerVariable,numLocalDof,epsilon,silent,alphaTmp);
                            s.localAlpha = PCG_hybrid(A,b,dofHandler,epsilon,silent,alphaTmp);
                        end
                    end
                    
                    t2 = toc;
                    %fprintf(  'Solve time : %f\n',t2-t1);
                    
                    s.convertAlpha(1);      % Convert the given alphaArray -> alpha
                
                    s.value = s.computeValue;
                    
%                 catch Mexp
%                     %disp('Problem with computeAlpha')
%                     save(['precelem.' int2str(System.rank) '.mat']);
%                     error('Problem with computeAlpha')
%                 end
                
%                 if (strcmp(s.name,'current'))
%                   s.value = s.computeValue();
%                 end
%             else
%                 
%                 %s.alphaArray = DirectSolver(A,b,tempGM);
%                 
%             
%                 % Convert alpha -> alphaArray (sofar only t0 solution of 'alphaArray' has been set)
%                 s.convertAlpha(0);
%                 s.convertAlpha(1);
% %                 s.computeValue();

            else
                s.convertAlpha(0);      % Convert the given alphaArray -> alpha
            end
        end
                
        function out = getValueArrayFormat(s,e,selection)
            element = s.mesh.getLocalElement(e);
            values = reshape( s.value{e}, element.disc.dofeq);
            out = values(selection{:});
        end
        
        function out = getAlphaArrayFormat(s,e,selection)
            element = s.mesh.getLocalElement(e);
            values = reshape( s.alpha{e}, element.disc.dofe);
            out = values(selection{:});
        end
        
        % Return the alpha for an specified element e (global id)
        function out = getAlpha(s,e)
%             if (nargin==2)
%                 solutionMode = SolutionMode.Normal;
%             end
            
            s.checkElementNumber(e);

            if (s.weight==1)
                try
                    element = s.mesh.getElement(e);
                    out = s.alpha{ element.localID }(:);
                catch
                    %rank = System.rank;
                    fprintf('Fatal error : problem in getAlpha\n')
                    save(['getAlpha.' int2str(System.rank) '.mat']);
                    if (System.nproc>1)
                        exit
                    end
                end
            elseif (s.weight==0)
                if (s.isInterpolated)
                    out = s.field.interpolated.getAlpha(e);
                else
                    out = s.field.current.getAlpha(e);
                end
            else
                if (s.isInterpolated)
                    element = s.field.interpolated.mesh.getElement(e);
                    out = ( s.weight ) * s.alpha{ element.localID }(:) + ( 1-s.weight ) * s.field.interpolated.getAlpha(e);
                else
                    element = s.field.active.mesh.getElement(e);
%                     out = ( s.weight ) * s.alpha{ element.localID }(:) + ( 1-s.weight ) * s.field.active.getAlpha(e);
                    out = ( s.weight ) * s.alpha{ element.localID }(:);%+ ( 1-s.weight ) * s.field.active.getAlpha(e);
                end
            end
            
%             if (nargin==3)
%                 switch solutionMode
%                     case SolutionMode.Nonlinear
%                         out = ( s.wi ) * s.alpha{e}(:) + (1-s.wi) * s.alphai{e}(:);
%                     case SolutionMode.Coupling
%                         out = ( s.wc ) * s.alpha{e}(:) + (1-s.wc) * s.alphac{e}(:);
%                     case SolutionMode.Normal
%                         out = s.alpha{e}(:);
%                 end
%             elseif (nargin==1)
%                 out = s.alpha;
%             end
        end
        
        function out = getAlphaArray(s)
            %fprintf('subField.getAlphaArray on rank %d\n',System.rank)
            
            if (s.needsMeshUpdate)
                s.meshUpdate;
            end
            out = s.localAlpha;
        end
        
        function setAlphaArray(s,array)
            if (isempty(s.field.getNumDof))
                s.update;
            end
            
            dofHandler = s.mesh.getDoFHandler( s.field.periodic );
            numDof     = dofHandler.numLocalDof;
            
%             s.copyNonLinear;
            if (numDof==numel(array))
                if (s.relaxation==1)
                    s.localAlpha = array(1:numDof);
                else
                    s.localAlpha = s.relaxation * array(1:numDof) + (1-s.relaxation) * s.localAlpha;        % value(n+1) = w * value(n+1) + (1-w) * value(n)
                end
            else
                error('SubField.setAlphaArray :: should not be here, the passed array has an unexpected size')
                %var    = s.field.getVariable;
                %s.localAlpha = array( (var-1)*numDof+1 : var*numDof );
            end
            s.convertAlpha(1);      % localAlpha --> alpha
            s.value = s.computeValue;           
        end
        
        function convertAlpha(s,type)
            if (type==1)
                % localAlpha --> alpha
                
                % Check if size of alpha is correct, and correct if required
                if (numel(s.alpha) ~= s.mesh.numLocalElements)
                    s.reset;
                end
                
                %save(['convertAlpha.' int2str(System.rank) '.mat']);
                
                id = 0;
                try
                    dofHandler = s.mesh.getDoFHandler(s.field.periodic);
                    for e=s.mesh.eRange
                        id = id+1;
                        s.alpha{ id } = s.localAlpha( dofHandler.localDofs{id} );
                    end
                catch MExp
                    disp('')
                end
            else 
                % alpha --> alphaArray
                
%                 fprintf('Number of global dofs : %f\n',s.field.mesh.getNumGlobalDof)
%                 fprintf('Number of local  dofs : %f\n',s.field.mesh.getNumLocalDof)

                %dofHandler  = s.mesh.getDoFHandler(s.field.periodic);
                
                dofHandler  = DoFHandlerCollection.getByID( s.mesh, s.field.periodic );

                globalAlpha = zeros(dofHandler.numDof,1);
                counter     = zeros(dofHandler.numDof,1);
                
                % Global approach
                id = 0;
                for e = s.mesh.eRange
                    id = id+1;
                    try                        
                        dofs    = dofHandler.dofs{e}(:);
                        Z       = s.mesh.Z{e};
                        
                        nonZeroZ = any(Z,1)';
                        
                        globalAlpha( dofs(nonZeroZ) ) = globalAlpha( dofs(nonZeroZ) ) + s.alpha{ id }(nonZeroZ); 
                        counter( dofs(nonZeroZ) )     = counter( dofs(nonZeroZ) ) + 1;
                    catch Mexp
%                         fprintf('Fatal error : problem in convertAlpha (1)\n')
%                         save(['convertAlpha1.' int2str(System.rank) '.mat']);
                        error('Fatal error : problem in convertAlpha (1)')
%                         if (System.nproc>1)
%                             exit
%                         end
                    end
                end
                
                % Synchronize between all processors
                try
                    globalAlpha = NMPI.Allreduce(globalAlpha, numel(globalAlpha),'+','globalAlpha');
                    counter     = NMPI.Allreduce(counter 	, numel(globalAlpha),'+','counter');
                    %globalAlpha = NMPI_Allreduce(globalAlpha, numel(globalAlpha),'+',NMPI.instance.mpi_comm_world);
                    %counter     = NMPI_Allreduce(counter 	, numel(globalAlpha),'+',NMPI.instance.mpi_comm_world);
                catch Mexp
                    fprintf('Fatal error : problem in convertAlpha (2)\n')
                    save(['convertAlpha2.' int2str(System.rank) '.mat']);
                    if (System.nproc>1)
                        exit
                    end
                end
                
                globalAlpha = globalAlpha ./ counter;
                
                % Move values to localGM
                dofHandler   = s.mesh.getDoFHandler(s.field.periodic);
                s.localAlpha = zeros(dofHandler.numLocalDof,1);
                
                try
                    id = 0;
                    for e=s.mesh.eRange
                        id = id+1;
                        
                        % Ensure alpha is a multidimensional array, and not
                        % a single vector (only if 
                        if (numel(s.alpha{id})==size(s.alpha{id},1))
                            element = s.mesh.getElement(e);
                            s.alpha{id} = reshape( s.alpha{id}, element.finite.dofSize );
                        end
                        
                        globalDofs                  = dofHandler.dofs{e}(:);
                        localDofs                   = dofHandler.localDofs{id}(:);
                        s.localAlpha( localDofs( localDofs~=-1 ) )   = globalAlpha( globalDofs( localDofs~=-1 ) );
                    end
                catch MExp
                    save(['convertAlpha2.' int2str(System.rank) '.mat']);
                    error('problem in subfield')
                end
%                 % Local approach
%                 s.globalAlphaArray  = zeros(s.field.mesh.getNumGlobalDof,1);
%                 counter             = zeros(s.field.mesh.getNumGlobalDof,1);
%                 
%                 for e=1:s.field.getNumElements
%                     element = s.field.mesh.getLocalElement(e);
%                     s.alphaArray( element.GM(:) ) = s.alpha{ e }(:);
%                 end
            end
        end
        
        function moveSolution(s,elementList,direction)
            
            if (direction==2)
                continuity = System.settings.stde.C(direction);
                
%                 if size(elementList,1)~=1 
%                     elementList = elementList';
%                 end
                
                switch continuity
                    case 0  % C0 continuity: copy 1 alpha layers (value only)                    
                        for e = elementList
                            Q = size(s.alpha{e},2);
                            s.alpha{e}(:,1) = s.alpha{e}(:,Q);
                        end
                        
                    case 1  % C1 continuity: copy 2 alpha layers (value and derivative)
                        for e = elementList
                            Q = size(s.alpha{e},2);
                            s.alpha{e}(:,1:2) = s.alpha{e}(:,Q-1:Q);
                        end
                end
                
                s.value = s.computeValue;
                
            elseif (direction==3)
                
                continuity = System.settings.stde.C(direction);
                
                if size(elementList,1)~=1 
                    elementList = elementList';
                end
                
                switch continuity
                    case 0  % C0 continuity: copy 2 alpha layers (value and derivative)                    
                        for e = elementList
                            Q = size(s.alpha{e},3);
                            s.alpha{e}(:,:,1) = s.alpha{e}(:,:,Q);
                        end
                        
                    case 1  % C1 continuity: copy 2 alpha layers (value and derivative)
                        for e = elementList
                            Q = size(s.alpha{e},3);
                            s.alpha{e}(:,:,1:2) = s.alpha{e}(:,:,Q-1:Q);
                        end
                end
                
                s.value = s.computeValue;
            else
                error('A direction other than z(3) is not supported at this moment')
            end                        
        end
        
        function copyBoundarySolution(s,srcElements,dstElements,direction,scaling)
            if (abs(direction)==1)
                continuity = System.settings.stde.C(abs(direction));
                
                switch continuity
                    case 0  % C0 continuity: copy 1 alpha layers (value only) 
                        bElements = numel(srcElements);
                        for e = 1:bElements
                            Q = size(s.alpha{e},2);
                            s.alpha{ dstElements(e) }(:,1) = s.alpha{ srcElements(e) }(:,Q);
                        end
                        
                    case 1  % C1 continuity: copy 2 alpha layers (value and derivative)
                        bElements = numel(srcElements);
                        for e = 1:bElements
                            % Select the src and dst indices to copy
                            %copyFlag      = [1 0];
                            srcInds = mat2ind( s.alpha{ srcElements(e) } );
                            srcInds{ abs(direction) } = srcInds{ abs(direction) }(end-continuity:end);
                            dstInds = mat2ind( s.alpha{ dstElements(e) } );
                            dstInds{ abs(direction) } = dstInds{ abs(direction) }(1:1+continuity);
                            
                            try
                                s.alpha{ dstElements(e) }( dstInds{:} ) = scaling * s.alpha{ srcElements(e) }( srcInds{:} );
                            catch
                                error('LSQDIM:field - SubField.m :: Problem in copyBoundarySolution')
                            end
                        end
                end
                
                s.value = s.computeValue;
            end
        end
        
        function copyBoundarySolutionValue(s,srcElements,dstElements,direction,scaling)
            %if (abs(direction)==1)
                continuity = System.settings.stde.C(abs(direction));
                
                switch continuity
                    case 0  % C0 continuity: copy 1 alpha layers (value only) 
                        bElements = numel(srcElements);
                        for e = 1:bElements
                            Q = size(s.alpha{e},2);
                            s.alpha{ dstElements(e) }(:,1) = s.alpha{ srcElements(e) }(:,Q);
                        end
                        
                    case 1  % C1 continuity: copy 2 alpha layers (value and derivative)
                        bElements = numel(srcElements);
                        for be = 1:bElements
                            % Select the src and dst indices to copy
                            %copyFlag      = [1 0];
                            
                            % Source data
                            if ( inrange(srcElements(be),s.mesh.eRange) )
                                srcValues = s.getValue(Derivative.value,srcElements(be));
                                srcValues = reshape( srcValues, s.mesh.getElement( srcElements(be) ).finite.qSize );
                                srcInds = mat2ind( srcValues );
                                srcInds{ abs(direction) } = srcInds{ abs(direction) }(end);
                                
                                srcValues = srcValues( srcInds{:} );
                            else
                                srcValues = [];
                                srcInds   = [];
                            end

                            % Destination data
                            if ( inrange(dstElements(be),s.mesh.eRange) )
                                dstValues = s.getValue(Derivative.value,dstElements(be));
                                dstValues = reshape( dstValues, s.mesh.getElement( dstElements(be) ).finite.qSize );
                                dstInds = mat2ind( dstValues );
                                dstInds{ abs(direction) } = dstInds{ abs(direction) }(1);
                            else
                                dstValues = [];
                                dstInds   = [];
                            end
                            
                            % Parallel send/receive operations
                            if ( inrange(srcElements(be),s.mesh.eRange) && ~inrange(dstElements(be),s.mesh.eRange) )
                                % send to other rank
                                % Select the element and obtain the rank of the required element
                                element = s.mesh.getElement( dstElements(be) );
                                elementRank = element.rank;
                                
                                %fprintf('Rank %d send package to rank %d for element id %d\n',System.rank,elementRank,element.id);
                                
                                NMPI.Send( srcValues(:), numel( srcValues ), elementRank );
                                
                                %package = [srcInds{:}];
                                %NMPI_Send( package, numel(package), elementRank );
                                
                                %fprintf('Send done on rank %d\n',System.rank);
                                
                            elseif ( ~inrange(srcElements(be),s.mesh.eRange) && inrange(dstElements(be),s.mesh.eRange) )
                                % receive from other rank
                                % Select the element and obtain the rank of the required element
                                element = s.mesh.getElement( srcElements(be) );
                                elementRank = element.rank;
                                
                                %fprintf('Rank %d recv package from rank %d for element id %d\n',System.rank,elementRank,element.id);
                                
                                packageSize = numel( dstValues( dstInds{:} ) );
                                srcValues = NMPI.Recv(packageSize,elementRank);
                                srcValues = reshape( srcValues, size( dstValues( dstInds{:} ) ) );
                                
                                %fprintf('Recv done on rank %d\n',System.rank);
                                
%                                 packageSize = numel( [dstInds{:}] );
%                                 package = NMPI_Recv(packageSize,elementRank);
                                
%                                 % Copy dstInds to get the correct size
%                                 srcInds = dstInds;
%                                 count = 0;
%                                 for i=1:numel(srcInds)
%                                     for n=1:numel(srcInds{i})
%                                         count = count+1;
%                                         srcInds{i}(n) = package( count );
%                                     end
%                                 end
                                
                                %save(['copyBoundarySolution.' int2str(System.rank) '.mat']);
                                %if (System.nproc>1 && System.rank>1)
                                %    exit
                                %end
                            end
                            
                            try
                                if ( inrange(dstElements(be),s.mesh.eRange) )
                                    
                                    dstValues_selection = dstValues( dstInds{:} );

                                    %delta           = srcValues_selection - dstValues_selection;
                                    scaling_delta   = scaling(1) - scaling(2);      % scaling_src / scaling_dst
                                    if (abs(scaling_delta)>1e-6)
                                        %scaler = (scaling(1)-scaling(3)) / scaling_delta;
                                        %newValues = srcValues - delta * abs(scaler);
                                        newValues = srcValues / scaling(1) * scaling(3); 
                                    else
                                        newValues = 0*srcValues - 1; %scaling(3);
                                    end

                                    % Compute the new alpha
                                    element = s.mesh.getElement( dstElements(be) );
                                    localID = element.localID;
                                    
                                    %dstValues = s.getValue(Derivative.value,dstElements(be));
                                    %dstValues = reshape( dstValues, s.mesh.getElement( dstElements(be) ).finite.qSize );
                                    dstValues( dstInds{:} ) = newValues;
                                    s.value{ localID }  = dstValues(:);
                                    
                                    Ze      = s.mesh.Z{ dstElements(be) };
                                    A       = element.getH * Ze;
                                    b       = s.value{ localID }(:);
                                    s.alpha{ localID } = A \ b;
                                end
                
                                %s.alpha{ dstElements(e) }( dstInds{:} ) = s.alpha{ srcElements(e) }( srcInds{:} ) - delta * abs(scaler);
                            catch Mexp
                                Mexp
                                error('LSQDIM:field - SubField.m :: Problem in copyBoundarySolution')
                            end
                        end
                end
                
                s.value = s.computeValue;
            %end
        end
        
        function out = alp(s,e)
            if nargin==2 && ~isempty(e)
                s.checkElementNumber(e);
                element = s.mesh.getElement(e);
                out = s.alpha{ element.localID }(:);
            else
                out = s.alpha;
            end
        end

        % Return a physical value (computed from alpha)
        function out = getValue(s,valueType,e)
            if nargin==2
                % return a cell array with all elements
                out = cell(s.mesh.numLocalElements,1);
                
                count = 0;
                for e=s.mesh.eRange
                    count = count+1;
                    out{ count } = s.getValue(valueType,e);
                end
                
            else
                % only return the values for 1 element
                s.checkElementNumber(e);

                element      = s.mesh.getElement(e);
                
                if (s.type==FieldType.Exact && valueType==Derivative.value)
                    [X,Y,Z,T] = element.getNodes;
                    out = s.definedFunction(X,Y,Z,T);
                    out = out(:);
                    return
                end
                
                %if (valueType ~= Derivative.value)
                    elementAlpha = s.getAlpha(e);
                %end
                
                Ze = s.mesh.Z{e};

                switch valueType
                    case Derivative.value
    %                     out = element.getH    * element.Z * elementAlpha;
    %                     out = s.value{ element.localID }(:);

                        out = element.getH    * Ze * elementAlpha;

                    case Derivative.x
                        out = element.getDx   * Ze * elementAlpha;
                    case Derivative.y
                        out = element.getDy   * Ze * elementAlpha;
                    case Derivative.z
                        out = element.getDz   * Ze * elementAlpha;
                    case Derivative.t
                        out = element.getDt   * Ze * elementAlpha;
                    case Derivative.xx
                        out = element.getDxx  * Ze * elementAlpha;
                    case Derivative.yy
                        out = element.getDyy  * Ze * elementAlpha;
                    case Derivative.xy
                        out = element.getDxy  * Ze * elementAlpha;
                    case Derivative.yx
                        out = element.getDyx  * Ze * elementAlpha;
                    case Derivative.xt
                        out = element.getDxt  * Ze * elementAlpha;
                    case Derivative.yt
                        out = element.getDyt  * Ze * elementAlpha;
                    case Derivative.xxy
                        out = element.getDxxy * Ze * elementAlpha;
                    case Derivative.yyx
                        out = element.getDyyx * Ze * elementAlpha;
                    case Derivative.xyx
                        out = element.getDxyx * Ze * elementAlpha;
                    case Derivative.xxx
                        out = element.getDxxx * Ze * elementAlpha;
                    case Derivative.yyy
                        out = element.getDyyy * Ze * elementAlpha;
                end
            end
        end
        
        function checkElementNumber(s,e)
            if (s.performElementNumberCheck)
                if (false)
                    if (~ismember(e,s.mesh.eRange)) 
                        error('A global element number is expected: %d is out of local range : %d - %d on rank %d\n',e,s.mesh.eRange(1),s.mesh.eRange(end),System.rank)
                    end
                else
                    if ( ~ismembc(e,s.mesh.eRange) )
                        error('A global element number is expected: %d is out of local range : %d - %d on rank %d\n',e,s.mesh.eRange(1),s.mesh.eRange(end),System.rank)
                    end
                end
            end
        end
 
        function plotxxdiff(s)
            dataAll = zeros(s.numDof,1);
            counter = zeros(s.numDof,1);
            for e=1:s.numElements
                element = s.mesh.getLocalElement(e);
                data = s.xxDerivative(e);
                dataAll(element.getGM) = dataAll(element.getGM) + data;
                counter(element.getGM) = counter(element.getGM) + 1;
            end
            
            % Compute the averaged field
            dataAveragedAll = dataAll ./ counter;
                
            % Put the averaged field back to the elements
            dataAveraged = cell(s.numElements,1);            
            for e=1:s.numElements
                element = s.mesh.getLocalElement(e);
                dataAveraged{ element.localID } = dataAveragedAll(element.getGM);
            end
               
            % Plot the averaged field
            for e=1:s.numElements
                element = s.mesh.getLocalElement(e);
                data = reshape(dataAveraged{ element.localID },element.disc.Q);
                [X,Y] = meshgrid( element.xelem,element.yelem );
                surf(X,Y,data(:,:,end)');
                hold on;
            end
                        
            colorbar;
            view([25,25]);
        end
         
        function plotxx(s)          
            for e=1:numel(s.alpha)
                element = s.mesh.getLocalElement(e);
                data = reshape(s.getValue( Derivative.xx, e ),element.disc.Q);
                [X,Y] = meshgrid( element.xelem,element.yelem );
%                 if (layer==1)
                    surf(X,Y,data(:,:,end)','FaceColor','interp');
%                 elseif (layer==0)
%                     surf(X,Y,data{e}(:,:,1)');
%                 end
                hold on;
            end
                        
            colorbar;
            view([25,25]);
%             title(titleString);            
        end
        
        function plotyy(s)          
            for e=1:numel(s.alpha)
                element = s.mesh.getLocalElement(e);
                data = reshape(s.getValue( Derivative.yy, e ),element.disc.Q);
                [X,Y] = meshgrid( element.xelem,element.yelem );
%                 if (layer==1)
                    surf(X,Y,data(:,:,end)','FaceColor','interp');
%                 elseif (layer==0)
%                     surf(X,Y,data{e}(:,:,1)');
%                 end
                hold on;
            end
                        
            colorbar;
            view([25,25]);
%             title(titleString);            
        end
        
        function plotxy(s)          
            for e=1:s.numElements
                element = s.mesh.getLocalElement(e);
                data = reshape(s.xyDerivative(e),element.disc.Q);
                [X,Y] = meshgrid( element.xelem,element.yelem );
%                 if (layer==1)
                    surf(X,Y,(data(:,:,end))');
%                 elseif (layer==0)
%                     surf(X,Y,data{e}(:,:,1)');
%                 end
                hold on;
            end
                        
            colorbar;
            view([25,25]);
%             title(titleString);            
        end
        
        function plotxxy(s)          
            for e=1:s.numElements
                element = s.mesh.getLocalElement(e);
                data = reshape(s.xxyDerivative(e),element.disc.Q);
                [X,Y] = meshgrid( element.xelem,element.yelem );
%                 if (layer==1)
                    surf(X,Y,(data(:,:,end))');
%                 elseif (layer==0)
%                     surf(X,Y,data{e}(:,:,1)');
%                 end
                hold on;
            end
                        
            colorbar;
            view([25,25]);
%             title(titleString);            
        end
        
        function plotyyx(s)          
            for e=1:s.numElements
                element = s.mesh.getLocalElement(e);
                data = reshape(s.yyxDerivative(e),element.disc.Q);
                [X,Y] = meshgrid( element.xelem,element.yelem );
%                 if (layer==1)
                    surf(X,Y,(data(:,:,end))');
%                 elseif (layer==0)
%                     surf(X,Y,data{e}(:,:,1)');
%                 end
                hold on;
            end
                        
            colorbar;
            view([25,25]);
%             title(titleString);            
        end
        
        function plotxyx(s)          
            for e=1:s.numElements
                element = s.mesh.getLocalElement(e);
                data = reshape(s.xyxDerivative(e),element.disc.Q);
                [X,Y] = meshgrid( element.xelem,element.yelem );
%                 if (layer==1)
                    surf(X,Y,(data(:,:,end))');
%                 elseif (layer==0)
%                     surf(X,Y,data{e}(:,:,1)');
%                 end
                hold on;
            end
                        
            colorbar;
            view([25,25]);
%             title(titleString);            
        end
        
        function plotLaplacian(s)
            for e=1:s.numElements
                element = s.mesh.getLocalElement(e);
                data1 = reshape(s.getValue( Derivative.xx, e ),element.disc.Q);
                data2 = reshape(s.getValue( Derivative.yy, e ),element.disc.Q);
                [X,Y] = meshgrid( element.xelem,element.yelem );
%                 if (layer==1)
                    surf(X,Y,data1(:,:,end)'+data2(:,:,end)');
%                 elseif (layer==0)
%                     surf(X,Y,data{e}(:,:,1)');
%                 end
                hold on;
            end
                        
            colorbar;
            view([25,25]);
%             title(titleString);            
        end
        
        function setValue(s,element,elementalValueArray)
            s.value{element} = elementalValueArray;
            s.computeAlpha(element);
        end
        
        % Function to reset the value cell array to zero
        function resetValue(s)
            s.numElements = s.mesh.getNumLocalElements;
            
            s.value = cell(s.mesh.getNumLocalElements,1);            
            for e=s.mesh.eRange
                element = s.mesh.getLocalElement(e);
                s.value{ element.localID } = zeros(element.disc.dofeq);
            end
            
            s.isEmpty = true;
        end
        
        function add(s,value,varField)
            
            if (nargin==3)
                error('LSQDIM:SubField :: Investigate reason for varField')
            end
            
            if (s.isEmpty && s.mesh.numLocalElements~=numel(s.alpha))
                s.reset;
            end
            
            if (isa(value,'Field'))
                % Copy the values from the passed field to this field.
                s.value = value.value;
                
                for e=s.mesh.eRange
                    s.value{e} = 2 * s.value{e};
                end
                
            else
                % the passed value is not a Field
                for e=s.mesh.eRange
                    element = s.mesh.getElement(e);

                    t = 0;

                    isSpaceTime = (System.settings.time.method==TimeMethod.SpaceTime);

                    if (System.settings.mesh.sdim==1 && isSpaceTime)
                        x = element.nodes{1};
                        y = 0;
                        z = 0;
                        t = element.nodes{2};

                    elseif (System.settings.mesh.sdim==2 && isSpaceTime)
                        x = element.nodes{1};
                        y = element.nodes{2};
                        z = 0;
                        t = element.nodes{3};

                    else
                        x = element.nodes{1};
                        y = element.nodes{2};
                        if (System.settings.mesh.sdim==2 && isSpaceTime)
                            z = element.zlocation; %element.zelem + System.settings.time.current;
                        elseif (System.settings.mesh.sdim==3)
                            z = element.nodes{3};
                        else
                            z = 0;
                        end
                    end

                    if (System.settings.time.current==0)
                        t = t*0;
                    end

                    [X,Y,Z,T] = ndgrid(x,y,z,t);

                    X = squeeze(X);
                    Y = squeeze(Y);
                    Z = squeeze(Z);
                    T = squeeze(T);

                    if (isa(value,'function_handle'))
                        if (nargin==3)
                            v = value(x,y,z,varField.val(e));
                        else
    %                         if ( strcmp(s.field.name,'omg') || strcmp(s.field.name,'c') )
    %                             [v,dv,ddv] = value(element.nodes{:});
    %                             v   = value(X,Y,Z,T);
    %                         else
                                v   = value(X,Y,Z,T);
                                dv  = [];
                                ddv = [];
    %                         end
                        end
                    elseif isnumeric(value)
                        v = value * ones( size(X) );
                    end

                    if (s.isEmpty)
                        try
                        if ( ndims(s.value{ element.localID })~=ndims(v) )
                            repDim = size(s.value{ element.localID });
                            repDim(1:end-1) = 1;
                            v = repmat(v,repDim);
                        end
                        catch MExp
                            disp('')
                        end
                        s.value{ element.localID } = v;

                        if (exist('dv','var') && ~isempty(dv))
                            if ( ndims(s.valuex{ element.localID })~=ndims(dv{1}) )
                                repDim = size(s.value{ element.localID });
                                repDim(1:end-1) = 1;
                                dv{1} = repmat(dv{1},repDim);
                            end
                            s.valuex{ element.localID } = dv{1};

                            if ( ndims(s.valuey{ element.localID })~=ndims(dv{2}) )
                                repDim = size(s.value{ element.localID });
                                repDim(1:end-1) = 1;
                                dv{2} = repmat(dv{2},repDim);
                            end
                            s.valuey{ element.localID } = dv{2};
                        end

                    else
                        s.value{ element.localID } = max( s.value{ element.localID } , v );
                    end
                end
            end

            s.computeAlpha;

            if (s.isEmpty)
                s.isEmpty = false;
            end
        end
        
        function plotAlpha(s,offset)
            data = s.alpha;
                           
            for e=1:s.mesh.numLocalElements
                element = s.mesh.getLocalElement(e+offset);
                if (size(data{ element.localID },2)==1)
                    data{e} = reshape(data{ element.localID },element.disc.Pn);
                    [X,Y] = meshgrid( element.xelem,element.yelem );
                    surf(X,Y,data{ element.localID }(:,:,end)');
                else
                    dx = element.xelem(end)-element.xelem(1);
                    xelem = [element.xelem(1):dx/4:element.xelem(end)];
                    dy = element.yelem(end)-element.yelem(1);
                    yelem = [element.yelem(1):dy/4:element.yelem(end)];
                    [X,Y] = meshgrid( xelem,yelem );
                    surf(X,Y,data{ element.localID }(:,:,end)');
                end
                hold on;
            end
            
            colorbar;
            view([25,25]);
%             title(titleString);            
        end
        
        function out = getVariable(s)
            out = s.field.variable;
        end
        
        function setPrescribed(s,e,flags)
            s.checkElementNumber(e);
            element = s.mesh.getElement(e);
            s.prescribed{ element.localID }(flags) = true;
            s.prescribedSynced = false;
        end
        
        function resetSyncPrescribed(s)
            s.prescribedSynced = false;
        end
        
        function syncPrescribed(s)
            % create a multi-element array with all the prescribed flags
            prescribedArray = false(s.numDof,1);
            for e=1:s.numElements
                element = s.mesh.getLocalElement(e);
                prescribedArray( element.GMlocal(:) ) = or( prescribedArray( element.GMlocal(:) ), s.prescribed{ element.localID }(:) );
            end
            
            for e=1:s.mesh.eRange
                element = s.mesh.getElement(e);
                s.prescribed{ element.localID } = prescribedArray( element.GMlocal(:) );
            end
            
            s.prescribedSynced = true;
        end
        
        function out = getPrescribed(s,e)
            out = s.field.getPrescribed(e);
        end
        
        function out = getPrescribedArray(s,e)
            if (~s.prescribedSynced)
                s.syncPrescribed;
            end
            
            s.checkElementNumber(e);
            element = s.mesh.getElement(e);
            out = s.prescribed{ element.localID }(:);
        end
        
        function out = getPrescribedValues(s,e)
            if (~s.prescribedSynced)
                s.syncPrescribed;
            end
            s.checkElementNumber(e);
            element = s.mesh.getElement(e);
            out = s.alpha{e}( s.prescribed{ element.localID }(:) );
        end
              
        function updateTimeSlab(s)
            s.value;
        end
        
        % Function to assign a (manufactured) solution to this field. The
        % passed argument needs to be a function handle. After initializing
        % the field (reset), the values are set. Next, alpha(Array) are
        % determined from these values.
        %
        % fh    : function_handle which provides the value at location (x,y,z)
        %
%         function assignSolution(s,fh)
%             if (~isa(fh,'function_handle'))
%                 error('A function should be provided to be able to assign a manufactured solution')
%             end
%             
%             s.wi = 1; s.wc = 1;
%             
%             s.reset;
%             
%             for e=1:s.numElements
%                 element = s.mesh.getLocalElement(e);               
%                 s.value{e} = fh(s.variable,element.xelem,element.yelem,element.zelem);
%             end
%             
%             s.computeAlpha();       % sets both alpha and alphaArray
%         end
        
        % Function to set the initial alpha based on boundary and/or initial
        % conditions
        function setAlpha(s,boundaryNumber,derivative,condition)
            boundary = s.mesh.getBoundary(boundaryNumber);
            
            if (derivative==0)
                temp    = boundary.H;
                tempPos = boundary.Hpos;
            elseif (derivative==1)
                temp    = boundary.Dn;
                tempPos = boundary.Dpos;
            end

            elemental = true; % elemental / global
            
            if (elemental)

                for be=1:boundary.getNumLocalElements
                    e = boundary.getLocalElementNumbers(be)
                    
                    if inrange(e,1,s.field.getNumElements)
                        
                        element = s.mesh.getLocalElement(e);

                        GM = element.GMlocal;
                        
%                         GM = s.getGM(e+NMPI.instance.eStart-1,variable);                   % Get the GatheringMatrix for this GLOBAL element/variable
% %                         GM = element.GM;
%                         GM2{e,1} = GM(:,:,end);
                                                     
                        % Step 1: Initialize alpha( tempPos ) to 0
                        s.localAlpha( GM(tempPos) ) = 0;

                        % Step 2: Assign boundary condition values
                        if (~isa(condition,'function_handle'))
                            if ~isa(condition,'Field') 
                                bc = (temp==1);                  % Get only the elements that are exactly equal to 1
                                bc(:,all(~bc,1)) = [];                % Remove empty columns
                                bc(all(~bc,2),:) = [];                % Remove empty rows

                                valueVector = condition*ones( nnz(bc), 1 );

                                [~,col]     = find(temp==1);
                                bcGM        = GM(col);
                            else
                                bc = temp*element.Z;                  % 
                                bc(:,all(~bc,1)) = [];                % Remove empty columns
                                bc(all(~bc,2),:) = [];                % Remove empty rows

                                if (s==condition.current)
                                    valueVector = reshape( s.value{ element.localID }, element.disc.dofeq);
                                    valueVector = valueVector(:,:,end);
                                else
                                    valueVector = condition.current.value{ element.localID }(:,:,end);
                                end

                                colNonZero  = ~all(~temp,1);      % Find all nonzero column numbers
                                bcGM        = GM(colNonZero);           % Use the nonzero columns to reduce the GM for just the boundary
                            end
                        else
                            % use the function handle, which provides a valueVector based on (x,y,z) location 
                            bc = temp;                       % 
                            bc(:,all(~bc,1)) = [];                % Remove empty columns
                            bc(all(~bc,2),:) = [];                % Remove empty rows

                            % element = s.mesh.getLocalElement(e);
                            x = element.xelem;
                            y = element.yelem;
                            z = element.zelem;

                            switch boundary.side
                                case Neighbor.Left
                                    x = s.mesh.x0;
                                case Neighbor.Right
                                    x = s.mesh.x1;
                                case Neighbor.Bottom
                                    y = s.mesh.y0;
                                case Neighbor.Top
                                    y = s.mesh.y1;
                                case Neighbor.Front
                                    z = s.mesh.z0;
                                case Neighbor.Back
                                    z = s.mesh.z1;
                                case 9
                                    x = -0.5;
                                    y = -0.5;
                            end

                            valueVector = condition(s.getVariable,x,y,z);

                            colNonZero  = ~all(~temp,1);      % Find all nonzero column numbers
                            bcGM        = GM(colNonZero);           % Use the nonzero columns to reduce the GM for just the boundary
                        end

                        % Compute the corresponding alpha for the
                        % prescribed boundary condition
                        if (derivative==0)
                            s.localAlpha( bcGM ) = bc \ ( valueVector(:) ) ;
                        elseif (derivative==1)
                            s.localAlpha( bcGM ) = bc \ ( valueVector(:) * boundary.Jn(be) ) ;
                        else
                            error('unsupported derivative flag')
                        end
                    end
                end
                
            else
            end
            
            s.convertAlpha(1);      % localAlpha --> alpha
            s.value = s.computeValue;
        end
        
        % Function to set the initial alpha based on boundary and/or initial
        % conditions
        function setAlphaLine(s,boundaryNumber1,boundaryNumber2,derivative,condition)
            boundary1 = s.mesh.getBoundary(boundaryNumber1);    
            boundary2 = s.mesh.getBoundary(boundaryNumber2);
            

            
            if (derivative==0)                                
                Hpos1 = boundary1.Hpos;
                Hpos2 = boundary2.Hpos;
                tempPos = intersect(Hpos1,Hpos2);
                
                H1 = boundary1.H;
                H2 = boundary2.H;
                temp = H1(:,tempPos);
            elseif (derivative==1)
                error('Derivative bc in fField.setAlphaLine has not been implemented yet')
                temp    = boundary.Dn;
                tempPos = boundary.Dpos;
            end

            elemental = true; % elemental / global
            
            if (elemental)

                elements1=boundary1.getLocalElementNumbers;
                elements2=boundary2.getLocalElementNumbers;
                mutualElements = intersect(elements1,elements2);
                
                for me=1:numel(mutualElements)
                    e = mutualElements(me);
                    
                    if inrange(e,1,s.mesh.getNumLocalElements)
                        
                        element = s.mesh.getLocalElement(e);

                        GM = element.GMlocal;
                        GM = GM(tempPos);
                        
%                         GM = s.getGM(e+NMPI.instance.eStart-1,variable);                   % Get the GatheringMatrix for this GLOBAL element/variable
% %                         GM = element.GM;
%                         GM2{e,1} = GM(:,:,end);
                                                     
                        % Step 1: Initialize alpha( tempPos ) to 0
                        s.localAlpha( GM ) = 0;

                        % Step 2: Assign boundary condition values
                        if (~isa(condition,'function_handle'))
                            if ~isa(condition,'Field') 
                                bc = (temp==1);                  % Get only the elements that are exactly equal to 1
                                bc(:,all(~bc,1)) = [];                % Remove empty columns
                                bc(all(~bc,2),:) = [];                % Remove empty rows

                                valueVector = condition*ones( nnz(bc), 1 );

                                [~,col]     = find(temp==1);
                                bcGM        = GM(col);
                            else
                                bc = temp*element.Z;                  % 
                                bc(:,all(~bc,1)) = [];                % Remove empty columns
                                bc(all(~bc,2),:) = [];                % Remove empty rows

                                if (s==condition)
                                    valueVector = reshape( s.value{ element.localID }, element.disc.dofeq);
                                    valueVector = valueVector(:,:,end);
                                else
                                    valueVector = condition.value{ element.localID }(:,:,end);
                                end

                                colNonZero  = ~all(~temp,1);      % Find all nonzero column numbers
                                bcGM        = GM(colNonZero);           % Use the nonzero columns to reduce the GM for just the boundary
                            end
                        else
                            % use the function handle, which provides a valueVector based on (x,y,z) location 
                            bc = temp;                       % 
                            bc(:,all(~bc,1)) = [];                % Remove empty columns
                            bc(all(~bc,2),:) = [];                % Remove empty rows

                            % element = s.mesh.getLocalElement(e);
                            x = element.xelem;
                            y = element.yelem;
                            z = element.zelem;

                            switch boundary.side
                                case Neighbor.Left
                                    x = s.mesh.x0;
                                case Neighbor.Right
                                    x = s.mesh.x1;
                                case Neighbor.Bottom
                                    y = s.mesh.y0;
                                case Neighbor.Top
                                    y = s.mesh.y1;
                                case Neighbor.Front
                                    z = s.mesh.z0;
                                case Neighbor.Back
                                    z = s.mesh.z1;
                                case 9
                                    x = -0.5;
                                    y = -0.5;
                            end

                            valueVector = condition(s.getVariable,x,y,z);

                            colNonZero  = ~all(~temp,1);      % Find all nonzero column numbers
                            bcGM        = GM(colNonZero);           % Use the nonzero columns to reduce the GM for just the boundary
                        end

                        % Compute the corresponding alpha for the
                        % prescribed boundary condition
                        if (derivative==0)
                            s.localAlpha( bcGM ) = bc \ ( valueVector(:) ) ;
                        elseif (derivative==1)
                            s.localAlpha( bcGM ) = bc \ ( valueVector(:) * boundary.Jn(me) ) ;
                        else
                            error('unsupported derivative flag')
                        end
                    end
                end                
            else
                
            end
            
            s.convertAlpha(1);      % localAlpha --> alpha
            s.value = s.computeValue;
        end
        
        function computeMaxValue(s)
            l1 = length(s.value);       
            maxValue = 0;            
            for i=1:l1
                maxValue = max(maxValue, max(abs(s.value{i}(:))) );
            end
            
            if (numel(maxValue)==1)
                s.maxValue = NMPI.Allreduce(maxValue,1,'M','computeMaxValue');
            else
                error('LSQDIM:SubField:computeMaxValue','Problem with NMPI.Allreduce, maxValue has %d elements where 1 is expected',numel(maxValue))
            end            
        end
        
        % PARALLEL FUNCTIONS
        
        % This function merges the local alphaArrays into a global one on
        % the master rank. It is used to write the global alpha array to a
        % solution/restart file. Since some alpha values are shared between
        % a certain number of ranks, the dofRank array is used to scale the
        % values locally before they are send to the master rank.
        function out = getGlobalAlpha(s)
            out = zeros(s.mesh.getNumGlobalDof,1);
            
            scaling =  s.mesh.dofRank(:,NMPI.instance.rank+1) ./ sum(s.mesh.dofRank,2);
            
            eStart = s.mesh.octTree.eStart;
            eEnd   = s.mesh.octTree.eEnd;
            
            for e=eStart:eEnd
                element = s.mesh.getElement(e);
%                 out( s.mesh.GM{ element.localID }(:) ) = s.alpha{ element.localID };
                out( element.GM(:) ) = s.alpha{ element.localID };
            end
            
%             tmp = s.mesh.dofRank;
%             save(['field' s.name num2str(NMPI.instance.rank) '.mat'],'out','scaling','tmp');
            
            out = scaling .* out;
            
%             save(['field' s.name num2str(NMPI.instance.rank) '.mat'],'out','scaling');
            
            % If the values are only required on the master rank (0):
            out = NMPI.Reduce(full(out),length(out),'+',0);
            
            % If the values are required on all ranks:
            % out = NMPI_Alleduce(full(out),length(out),'+',NMPI.instance.mpi_comm_world);
            
            if (NMPI.instance.rank==0)
                s.globalAlphaArray = out;
            end
        end
        
        function setName(s,name)
            s.name = name;
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % Use 'power' of order n on the field values
        function out = power(s,n)
            numElements = numel(s.value);
            out = Field(s.mesh,0,'default');
            out.resetValue;
            out.wi = 1;
            out.wc = 1;
            for e=1:numElements
                element = s.mesh.getElement(e);
                out.value{ element.localID } = s.value{ element.localID }.^n;
            end
        end
        
        % Sum the values of two field
        function out = plus(s1,s2)
            if (numel(s1.value)~=numel(s2.value))
                error('The number of elements should be equal for a summation')
            end
            out = SubField(s1.field,'plus');
            out.resetValue;
            out.wi = 1;
            out.wc = 1;
            numElements = numel(s1.value);
            for e=1:numElements
                element = s.mesh.getElement(e);
                out.value{ element.localID } = s1.value{ element.localID } + s2.value{ element.localID };
            end
        end 
        
        % Sum the values of two field
        function out = minus(s1,s2)
            if (numel(s1.value)~=numel(s2.value))
                error('The number of elements should be equal for a summation')
            end
            out = SubField(s1.field,'minus');
            out.resetValue;
            numElements = numel(s1.value);
            for e=1:numElements
                element = s.mesh.getElement(e);
                out.value{ element.localID } = reshape( s1.value{ element.localID }(:) - s2.value{ element.localID }(:), s1.mesh.getLocalElement(e).disc.Q );
            end
            out.computeAlpha();
        end 
        
                
        function computeL2Norm(s,solutionMode)
            s.field.computeL2Norm(solutionMode);
        end
        
        function checkInterpolation(s,newMesh)
            s.isInterpolated = ~isequal(s.field.mesh,newMesh);
            if (~s.isInterpolated)
                s.activeMesh = s.field.mesh;
            else
                s.activeMesh = newMesh;
            end
        end
        

    end
    
    methods
        %% DISP
%         function disp(s)
%             fprintf('SubField %s.%s with %d (of %d) elements on a %s mesh\n',s.field.name,s.name,s.mesh.numLocalElements,s.mesh.numElements,s.mesh.name);
%         end

        function set.relaxation(s,value)
            s.relaxation = value;
        end
        
        function plotResidualH(s)
            residual = 0;
            check    = 0;
            
            clf;
            
            id = 0;
            for e=s.mesh.eRange
                id = id+1;
                    
                element = s.mesh.getElement(e);                    
                Ze = s.mesh.Z{e};

                W   = element.getW;
                %W   = W .* (2.^s.mesh.dh.dofLevels{e}(:));

                A = element.getH * Ze;
                x = s.alpha{id}(:);
                b = s.value{id}(:);
                
                r = A * x - b;
                
                residual = residual + dot(W,r)^2;
                check    = check    + sum(W);
                
                data = reshape(r,element.finite.qSize);
                [X,Y] = element.getNodes;
                plotData = data(:,:,end);
                surf(X(:,:,:,end),Y(:,:,:,end),(plotData),'FaceColor','interp');
                hold on;
            end
            
            fprintf('Residual integral : %e\n',sqrt(residual));
            fprintf('Check    integral : %e\n',check);
        end

        %% SET METHODS
        
        % Set a mesh, or use the sharedMesh if none is provided. A warning
        % is thrown if the type of the passed mesh is not correct.
        function set.mesh(s,mesh)
            if (nargin==2)
                if (isa(mesh,'OctreeMesh'))
                    s.mesh = mesh;
                else
                    warning('SubField:Mesh','The passed mesh is not of the correct type, assuming a shared mesh')
                end
            end
            s.checkInterpolation(mesh);
        end
        
        function set.weight(s,weight)
            s.weight = weight;
        end
        
        %% GET METHODS
        function out = get.alpha(s)
            out = s.alpha;
        end
        
%         function out = get.name(s)
%             out = s.name; %[s.field.name '.' s.name];
% %             fieldName = s.field.name;
% %             out = [fieldName '.' ownName];
%         end
        
        function out = get.type(s)
            out = s.type;
        end
        
        function out = get.isInterpolated(s)
            out = s.isInterpolated;
        end
        
        function out = get.weight(s)
            out = s.weight;
        end
        
%         function out = get.maxValue(s)
%             
%             disp('get.maxValue in SubField')
%             
%             %s.computeMaxValue;
%             out = s.maxValue;
%         end
        
%         function out = get.numElements(s)
%             out = s.mesh.numLocalElements;
%         end
        
        function out = get.mesh(s)
%             if (s.isInterpolated)
%                 out = s.mesh;
%             else
%                 out = s.field.mesh;
%             end
            out = s.activeMesh;
        end
        
        function out = needsMeshUpdate(s)
            %out = s.mesh.isChanged;
            out = ~isequal(s.meshID,s.mesh.Lv);
        end
        
        function out = val(s,e,dim)
            % NB: e is a global element number
            
            if (s.needsMeshUpdate)
                %fprintf('SubField.val on rank %d\n',System.rank)
                s.meshUpdate;
            end
            
            if nargin<3
                dim = 1; 
            end

            if nargin>1 && ~isempty(e)
                s.checkElementNumber(e);
                element = s.mesh.getElement(e);
                if dim==1
                    try 
                        %out2 = s.value{ element.localID }(:); 
                        
                        if (s.type==FieldType.Exact)
                            out = s.getValue(Derivative.value,e);
                            %[X,Y,Z,T] = element.getNodes;
                            %out = s.definedFunction(X,Y,Z,T);
                            %out = out(:);
                            return
                            
                        else
                            Ze = s.mesh.Z{e};
                            out = element.getH * Ze * s.alpha{ element.localID }(:); 
                        end
                        
                        %if ~(isequal(out,out2))
                        %    error('the value and H*Z*alpha are not equal!')
                        %end
                    catch Mexp
                        error('Problem in SubField.val');
                    end
                else
                    out = s.value{ element.localID };
                end
            else
                if dim==1
                    out = s.value;
                else
                    out = cell( size(s.value) );
                    for e=1:s.mesh.numLocalElements
                        element = s.mesh.getLocalElement(e);
                        out{e} = reshape( s.value{e}, element.disc.Q );
                    end
                end
            end
        end
        
        function out = x(s,e)
            if nargin==2
                s.checkElementNumber(e);

                element      = s.mesh.getElement(e);
                elementAlpha = s.alpha{element.localID}(:);
                Ze = s.mesh.Z{e};
                out = element.getDx * Ze * elementAlpha;
            else
                out = cell(numel(s.alpha),1);
                for i=1:numel(out)
                    element      = s.mesh.getLocalElement(i);
                    Ze = s.mesh.Z{element.globalID};
                    out{i} = element.getDx * Ze * s.alpha{i}(:);
                end
            end
        end
        
        function out = y(s,e)
            if (nargin==2)
                s.checkElementNumber(e);

                element      = s.mesh.getElement(e);
                elementAlpha = s.alpha{element.localID}(:);
                Ze = s.mesh.Z{e};
                out = element.getDy * Ze * elementAlpha;
            else
                out = cell(numel(s.alpha),1);
                for i=1:numel(out)
                    element      = s.mesh.getLocalElement(i);
                    Ze = s.mesh.Z{element.globalID};
                    out{i} = element.getDy * Ze * s.alpha{i}(:);
                end
            end
        end
        
        function out = z(s,e)
            if (nargin==2)
                s.checkElementNumber(e);

                element      = s.mesh.getElement(e);
                elementAlpha = s.alpha{element.localID}(:);
                Ze = s.mesh.Z{e};
                out = element.getDz * Ze * elementAlpha;
            else
                out = cell(numel(s.alpha),1);
                for i=1:numel(out)
                    element      = s.mesh.getLocalElement(i);
                    Ze = s.mesh.Z{element.globalID};
                    out{i} = element.getDz * Ze * s.alpha{i}(:);
                end
            end
        end
        
        function out = t(s,e)
            if (nargin==2)
                s.checkElementNumber(e);

                element      = s.mesh.getElement(e);
                elementAlpha = s.alpha{element.localID}(:);
                Ze = s.mesh.Z{e};
                out = element.getDt * Ze * elementAlpha;
            else
                out = cell(numel(s.alpha),1);
                for i=1:numel(out)
                    element      = s.mesh.getLocalElement(i);
                    out{i} = element.getDt * element.Z * s.alpha{i}(:);
                end
            end
        end
        
        function out = nn(s,e)
%             if nargin==2
%                 s.checkElementNumber(e);
% 
%                 element      = s.mesh.getElement(e);
%                 elementAlpha = s.getAlpha(e);
%                 Ze = s.mesh.Z{e};
%                 out = element.getDnn * Ze * elementAlpha;
%             else
%                 out = cell(numel(s.alpha),1);
%                 for i=1:numel(out)
%                     element = s.mesh.getLocalElement(i);
%                     Ze   	= s.mesh.Z{i};
%                     out{i}  = element.getDnn * Ze * s.alpha{i}(:);
%                 end
%             end
        end
        
        function out = xx(s,e)
            if nargin==2
                s.checkElementNumber(e);

                element      = s.mesh.getElement(e);
                elementAlpha = s.getAlpha(e);
                Ze = s.mesh.Z{e};
                out = element.getDxx * Ze * elementAlpha;
            else
                out = cell(numel(s.alpha),1);
                for i=1:numel(out)
                    element = s.mesh.getLocalElement(i);
                    Ze   	= s.mesh.Z{i};
                    out{i}  = element.getDxx * Ze * s.alpha{i}(:);
                end
            end
        end
        
        function out = yy(s,e)
            if nargin==2
                s.checkElementNumber(e);

                element      = s.mesh.getElement(e);
                elementAlpha = s.getAlpha(e);
                Ze = s.mesh.Z{e};
                out = element.getDyy * Ze * elementAlpha;
            else
                out = cell(numel(s.alpha),1);
                for i=1:numel(out)
                    element      = s.mesh.getLocalElement(i);
                    Ze   	= s.mesh.Z{i};
                    out{i} = element.getDyy * Ze * s.alpha{i}(:);
                end
            end
        end
        
        function out = zz(s,e)
            if nargin==2
                s.checkElementNumber(e);

                element      = s.mesh.getElement(e);
                elementAlpha = s.getAlpha(e);
                Ze = s.mesh.Z{e};
                out = element.getDzz * Ze * elementAlpha;
            else
                out = cell(numel(s.alpha),1);
                for i=1:numel(out)
                    element      = s.mesh.getLocalElement(i);
                    Ze   	= s.mesh.Z{i};
                    out{i} = element.getDzz * Ze * s.alpha{i}(:);
                end
            end
        end
        
        function out = xy(s,e)
            if nargin==2
                s.checkElementNumber(e);

                element      = s.mesh.getElement(e);
                elementAlpha = s.getAlpha(e);
                Ze = s.mesh.Z{e};
                out = element.getDxy * Ze * elementAlpha;
            else
                out = cell(numel(s.alpha),1);
                for i=1:numel(out)
                    element = s.mesh.getLocalElement(i);
                    Ze   	= s.mesh.Z{i};
                    out{i}  = element.getDxy * Ze * s.alpha{i}(:);
                end
            end
        end
        
        function out = xz(s,e)
            if nargin==2
                s.checkElementNumber(e);

                element      = s.mesh.getElement(e);
                elementAlpha = s.getAlpha(e);
                Ze = s.mesh.Z{e};
                out = element.getDxz * Ze * elementAlpha;
            else
                out = cell(numel(s.alpha),1);
                for i=1:numel(out)
                    element = s.mesh.getLocalElement(i);
                    Ze   	= s.mesh.Z{i};
                    out{i}  = element.getDxz * Ze * s.alpha{i}(:);
                end
            end
        end
        
        function out = yz(s,e)
            if nargin==2
                s.checkElementNumber(e);

                element      = s.mesh.getElement(e);
                elementAlpha = s.getAlpha(e);
                Ze = s.mesh.Z{e};
                out = element.getDyz * Ze * elementAlpha;
            else
                out = cell(numel(s.alpha),1);
                for i=1:numel(out)
                    element = s.mesh.getLocalElement(i);
                    Ze   	= s.mesh.Z{i};
                    out{i}  = element.getDyz * Ze * s.alpha{i}(:);
                end
            end
        end
        
        function out = xt(s,e)
            if nargin==2
                s.checkElementNumber(e);

                element      = s.mesh.getElement(e);
                elementAlpha = s.getAlpha(e);
                Ze = s.mesh.Z{e};
                out = element.getDxt * Ze * elementAlpha;
            else
                out = cell(numel(s.alpha),1);
                for i=1:numel(out)
                    element = s.mesh.getLocalElement(i);
                    Ze   	= s.mesh.Z{i};
                    out{i}  = element.getDxt * Ze * s.alpha{i}(:);
                end
            end
        end
        
        function out = yt(s,e)
            if nargin==2
                s.checkElementNumber(e);

                element      = s.mesh.getElement(e);
                elementAlpha = s.getAlpha(e);
                Ze = s.mesh.Z{e};
                out = element.getDyt * Ze * elementAlpha;
            else
                out = cell(numel(s.alpha),1);
                for i=1:numel(out)
                    element = s.mesh.getLocalElement(i);
                    Ze   	= s.mesh.Z{i};
                    out{i}  = element.getDyt * Ze * s.alpha{i}(:);
                end
            end
        end
        
        function out = xxx(s,e)
            if nargin==2
                s.checkElementNumber(e);

                element      = s.mesh.getElement(e);
                elementAlpha = s.getAlpha(e);
                Ze = s.mesh.Z{e};
                out = element.getDxxx * Ze * elementAlpha;
            else
                out = cell(numel(s.alpha),1);
                for i=1:numel(out)
                    element = s.mesh.getLocalElement(i);
                    Ze   	= s.mesh.Z{i};
                    out{i}  = element.getDxxx * Ze * s.alpha{i}(:);
                end
            end
        end
        
        function loadAlphaArray(s,array)
            if (isempty(s.field.numDof))
                s.update;
            end
            
            eStart = s.mesh.octTree.eStart;
            eEnd   = s.mesh.octTree.eEnd;
            
            s.field.copyNonLinear;
%             if (s.numDof==numel(array))
%                 s.alphaArray = array( 1 : s.numDof );
%             else
          	try
                for e=eStart:eEnd
%                     element = s.mesh.getLocalElement(e);
                    s.alpha{ e-eStart+1 } = array( s.mesh.getElement(e).GM );
                end
            catch Mexp
                disp('ERROR with loadAlphaArray')   
            end
%             end
            s.convertAlpha(0);      % alpha --> localAlpha
            s.value = s.computeValue;
        end
        
        function meshUpdate(s,newMesh,oldMesh)
            
            if (System.settings.anal.debug)
                fprintf('SubField.meshUpdate on rank %d\n',System.rank)
            end
            
            if (nargin==1)
                oldMesh = s.mesh.backup;
                newMesh = s.mesh;
            elseif (nargin==2)
                %oldMesh = s.mesh.backup;
                oldMesh = s.field.mesh;
                
                if (~isequal(s.name,'Interpolated') && ~isequal(s.name,'Exact'))
                    error('Unforseen error in SubField.meshUpdate!')
                end
            end
            
            % It is possible that the temp mesh should be used to update
            % this subField
            if (~isequal(oldMesh.Lv,s.meshID))
                if (~isempty(s.mesh.temp))
                    oldMesh = s.mesh.temp;
                else
                    oldMesh = s.mesh.backup;
                end
            end
            
            % If there's still a difference now, there is a severe error
            if (~isequal(oldMesh.Lv,s.meshID))
                physics = cell(PhysicsCollection.size,1);
                for i=1:numel(physics)
                    physics{i} = PhysicsCollection.get(i);
                end
                fprtinf('The meshID of subField %s does not match any known meshes, so no update can be performed!\n',s.fullname)
                filename = ['meshUpdate_meshID.' int2str(System.rank) '.mat'];
                save(filename);
                %error('The meshID of subField %s does not match any known meshes, so no update can be performed!\n',s.fullname)
                if System.nproc>1
                    exit;
                end
            end
            
            % Set some variables: _0 == old and _1 == new
            Z_0             = oldMesh.Z;
            Lv_0            = oldMesh.Lv;
            eRange_0        = oldMesh.eRange;
            numElements_0   = oldMesh.numElements; % numel( eRange_0 );
            
            Z_1             = newMesh.Z;
            Lv_1            = newMesh.getLv;
            eRange_1        = newMesh.eRange;
            numElements_1   = newMesh.numElements; 
            %numElements_1   = numel( eRange_1 );
            
            numBaseElements = prod(max(newMesh.Ne,1));          % NB: constant, equal for both meshes

            if (numel(eRange_0) == numel(s.alpha))
                Zalpha = cell(numel(eRange_0),1);      % initZalpha
                id = 0;
                for e=eRange_0
                    id = id+1;
%                     dofSize = oldMesh.getElement(e).finite.dof
                    Zalpha{id} = Z_0{e} * s.alpha{id}(:);
                    Zalpha{id} = reshape(Zalpha{id},size(s.alpha{id}));
                end
            else
                physics = cell(PhysicsCollection.size,1);
                for i=1:numel(physics)
                    physics{i} = PhysicsCollection.get(i);
                end
                filename = ['meshUpdate1.' int2str(System.rank) '.mat'];
                save(filename);
                error('SubField:ZAlpha','The number of elements in Z and s.alpha should be the same!')
            end
            
%             % Fix for multiple materials:
            %Lv_temp = -ones( numel(oldMesh.getMaterial),1 );
            %Lv_temp( oldMesh.getMaterial==s.mesh.activeMaterial ) = Lv_0;
%             tree_0 = s.createRefinementTree(Lv_temp,numBaseElements,0);   	% original mesh
%             
%             
%             % Create the old en new refinement trees
%             % Create the init and goal refinement trees
%             tree_0 = s.createRefinementTree(Lv_0,numBaseElements,0);   	% original mesh
            
            
            %allElements = s.mesh.getElementsByMaterial(Material.All);
            %Lv_1 = allElements.getLv;
            
            if (isstruct(oldMesh))
                tree_0 = s.mesh.createRefinementTree(oldMesh.LvAll,[],oldMesh.LvAll);
                tree_1 = s.mesh.createRefinementTree(); %Lv_1,numBaseElements,Lv_1,0);  	% new mesh
                
                materialTree_0 = s.mesh.createRefinementTree(oldMesh.material,[],oldMesh.LvAll);
                %materialTree_1 = s.mesh.createRefinementTree(s.mesh.getMaterial);
            else
                error('the mesh is not a struct')
            end
            
            try
                alphaTree   = s.createCellArrayTree(Zalpha,eRange_0,numElements_0,tree_0,materialTree_0,1);
                ZTree       = s.createCellArrayTree(Z_0   ,eRange_0,numElements_0,tree_0,materialTree_0,1);
            catch MExp
                filename = ['meshUpdate2a.' int2str(System.rank) '.mat'];
                save(filename);
                if (System.nproc>1)
                    exit
                end
            end
            
            try
                newAlphaTree = s.getNewAlpha(alphaTree,ZTree,tree_0,tree_1);
            catch MExp
                filename = ['meshUpdate2b.' int2str(System.rank) '.mat'];
                fprintf('%s\n', MExp.message);
                save(filename);
                if (System.nproc>1)
                    exit
                end
            end
    
            %
            materialSelection = (s.mesh.getMaterial==s.field.activeMaterial);
            Lv_1_all = s.mesh.getLvByMaterial( Material.All );
            tempAlphaTree = DataCellArray(newAlphaTree,DataFormat.Tree,numBaseElements,4,Lv_1_all);
            newAlphaTree  = tempAlphaTree.getData( DataFormat.Flat );
            newAlphaTree  = newAlphaTree( materialSelection );
            
            % Check if the number of elements is correct
            if ( numel(newAlphaTree)~= numElements_1 )
                error('The number of elements in the newAlphaTree does not match the expected value\n')
            end
            
%             %if (s.isInterpolated)
%                 while numel(newAlphaTree) < numElements_1
%                     newAlphaTree = s.flattenAlpha(newAlphaTree,materialTree_1);
%                 end
% %             else
% %                 while numel(newAlphaTree) < s.field.getNumElements
% %                     newAlphaTree = s.flattenAlpha(newAlphaTree);
% %                 end
% %             end
            
            % Convert the alpha values to element ones by using the Z
            % transformation matrix
%             s.alpha = cell(numElements_1,1);
            s.alpha = cell(numel(eRange_1),1);
            
            %rank = System.rank;
            %save(['parallel.' int2str(System.rank) '.mat']);  

            newAlphaTree = s.redistributeCellArray(newAlphaTree);
            
            try
                id = 0;
                for e=eRange_1 %1:numElements_1
                    id = id+1;
                    %element = s.mesh.getLocalElement(e);
                    element = s.mesh.getElement(e);
                        
                    nonZeroZ = any(Z_1{e},1);
                    tmp = zeros( numel(newAlphaTree{ element.id }),1 );
                    
                    tmp( nonZeroZ ) = Z_1{e}(:,nonZeroZ) \ newAlphaTree{ element.id }(:);
                    s.alpha{id} = reshape(tmp,size(newAlphaTree{ element.id }));
                end
            catch MExp
                save(['meshUpdate3.' int2str(System.rank) '.mat']);
                error('SubField:meshUpdate','Problem with isConvergedValue') 
            end
            
            s.convertAlpha(0);      % alpha --> localAlpha
            s.value = s.computeValue;
            
%             %% PRESCRIBED FLAGS
% %             if ( numel(s.field.mesh.Lv)~=numel(Lv_0) || ~(all(s.field.mesh.Lv==Lv_0)) )
%                 prescribedTree      = s.createCellArrayTree(s.field.prescribed,eRange_0,numElements_0,tree_0,1);
%                 newPrescribedTree   = s.updateVarTree(prescribedTree,tree_0,tree_1);
%                 while numel(newPrescribedTree) < numElements_1
%                     newPrescribedTree = s.flattenAlpha(newPrescribedTree);
%                 end
%                 newPrescribedTree   = s.redistributeCellArray(newPrescribedTree);
% 
%                 s.field.prescribed = newPrescribedTree;
% %             end
            
            %rank = System.rank;
            %save(['parallel.' int2str(System.rank) '.mat']); 
            
            % Store the values of this mesh for future use
            %s.Lv     = s.mesh.octTree.getLv;
            %s.eRange = s.mesh.eRange;
            
            dofHandler = newMesh.getDoFHandler( s.field.periodic );
            
            s.numDoF = dofHandler.numLocalDof;
            
            s.meshID = Lv_1;
        end

        
        function [newAlphaTree,oldZTree,oldLvTree] = getNewAlpha(s,oldAlphaTree,oldZTree,oldLvTree,newLvTree)
            
            if (isscalar(newLvTree))
                scalar = newLvTree;
                newLvTree = cell(numel(oldAlphaTree),1);
                for i=1:numel(newLvTree)
                    newLvTree{i} = scalar;
                end
            end
            
            newAlphaTree = newLvTree;
            
            id = 1;
            
            while id < numel(oldAlphaTree)+1
                if ( numel(newLvTree{id} )==1 && numel(oldLvTree{id} )==1 )
                    % Same levels, just copy alpha
                    newAlphaTree{id} = oldAlphaTree{id};
                    id = id+1;

                elseif ( numel(newLvTree{id} )~=1 && numel(oldLvTree{id} )~=1 )
                    % Both trees are refined, recursive call
                    if (iscell(oldZTree))
                        ZTree = oldZTree{id};
                    else
                        ZTree = oldZTree;
                    end
                    newAlphaTree{id} = s.getNewAlpha(oldAlphaTree{id},ZTree,oldLvTree{id},newLvTree{id});
                    id = id+1;

                elseif ( numel(newLvTree{id} )==1 && numel(oldLvTree{id} )~=1 )
                    % coarsening, numel(oldLvTree{e} ) > 1 so the old
                    % element is more refined than the new element
                    % group/collect
                    
                    % check if all 
                    for i=1:numel(oldLvTree{id})
                        if ( isa(oldLvTree{id}{i},'cell') )
                            [oldAlphaTree{id},oldZTree{id},oldLvTree{id}] = s.getNewAlpha(oldAlphaTree{id},oldZTree{id},oldLvTree{id},newLvTree{id});
                        end
                    end
                    
%                     try
                        % Extract the number of boundary dofs, and assume
                        % both sides have the same amount of boundary dofs.
                        % That's why only half of the available numBoundDof
                        % array is send to joinAlpha.
                        temp = cellfun( @(x) x.numBoundDof, s.mesh.getElement(id).finite.basis, 'uni', 0);
                        numBoundDof = vertcat( temp{:} );
                        numBoundDof = numBoundDof(:,1);
                        [oldAlphaTree{id},oldZTree{id},oldLvTree{id}] = joinAlphaGeometric(s,oldAlphaTree{id},oldZTree{id},oldLvTree{id},numBoundDof); 
%                     catch MExp
%                         %rank = System.rank;
%                         fprintf('Fatal error : problem in getNewAlpha\n')
%                         save(['getNewAlpha.' int2str(System.rank) '.mat']);
%                         if (System.nproc>1)
%                             exit
%                         end
%                     end

                elseif ( numel(newLvTree{id} )~=1 && numel(oldLvTree{id} )==1 )
                    % refining, numel(newLvTree{e} ) > 1 so the new
                    % element is more refined than the old element
                    [oldAlphaTree{id},oldLvTree{id}] = splitAlpha(s,oldAlphaTree{id},oldLvTree{id});
                    
                end
            end
        end
        
        
        function [newVarTree,oldLvTree] = updateVarTree(s,oldVarTree,oldLvTree,newLvTree)
            
            if (isscalar(newLvTree))
                scalar = newLvTree;
                newLvTree = cell(numel(oldVarTree),1);
                for i=1:numel(newLvTree)
                    newLvTree{i} = scalar;
                end
            end
            
            newVarTree = newLvTree;
            
            id = 1;
            
            while id < numel(oldVarTree)+1
                if ( numel(newLvTree{id} )==1 && numel(oldLvTree{id} )==1 )
                    % Same levels, just copy the variable
                    newVarTree{id} = oldVarTree{id};
                    id = id+1;

                elseif ( numel(newLvTree{id} )~=1 && numel(oldLvTree{id} )~=1 )
                    % Both trees are refined, recursive call
                    newVarTree{id} = s.updateVarTree(oldVarTree{id},oldLvTree{id},newLvTree{id});
                    id = id+1;

                elseif ( numel(newLvTree{id} )==1 && numel(oldLvTree{id} )~=1 )
                    % coarsening, numel(oldLvTree{e} ) > 1 so the old
                    % element is more refined than the new element
                    % group/collect
                    
                    % check if all 
                    for i=1:numel(oldLvTree{id})
                        if ( isa(oldLvTree{id}{i},'cell') )
                            [oldVarTree{id},oldLvTree{id}] = s.updateVarTree(oldVarTree{id},oldLvTree{id},newLvTree{id});
                        end
                    end
                    
%                     try
                        % Extract the number of boundary dofs, and assume
                        % both sides have the same amount of boundary dofs.
                        % That's why only half of the available numBoundDof
                        % array is send to joinAlpha.
                        temp = cellfun( @(x) x.numBoundDof, s.mesh.getElement(id).finite.basis, 'uni', 0);
                        numBoundDof = vertcat( temp{:} );
                        numBoundDof = numBoundDof(:,1);
                        [oldVarTree{id},oldLvTree{id}] = joinLogicalGeometric(s,oldVarTree{id},oldLvTree{id},numBoundDof); 
%                     catch MExp
%                         %rank = System.rank;
%                         fprintf('Fatal error : problem in getNewAlpha\n')
%                         save(['getNewAlpha.' int2str(System.rank) '.mat']);
%                         if (System.nproc>1)
%                             exit
%                         end
%                     end

                elseif ( numel(newLvTree{id} )~=1 && numel(oldLvTree{id} )==1 )
                    % refining, numel(newLvTree{e} ) > 1 so the new
                    % element is more refined than the old element
                    [oldVarTree{id},oldLvTree{id}] = splitLogical(s,oldVarTree{id},oldLvTree{id});
                    
                end
            end
        end
                
        
        % cellArray      : array with only local elements 
        % refinementTree : global tree with the Lv values of the elements
        % out            : 
        function [out,id] = createCellArrayTree(s,cellArray,eRange,numOldElements,refinementTree,materialTree,id)
            out = refinementTree;
            
            % For parallel use:
            if (numel(cellArray)~=numOldElements)
                globalCellArray = cell(numOldElements,1);
                globalCellArray(eRange) = cellArray;
                cellArray = globalCellArray;
            end
            
            for e=1:numel(refinementTree)
                if numel(refinementTree{e})~=1
                    [out{e},id] = s.createCellArrayTree( cellArray, 1, size(cellArray), refinementTree{e}, materialTree{e}, id );
                else
                    %fprintf('numel cellArray : %d -- id == %d\n',numel(cellArray),id);
%                     try
                    
                    if (materialTree{e}==s.field.activeMaterial)
                        out{e} = cellArray{id};
                        id=id+1;
                    else
                        out{e} = [];
                    end
                    
%                     catch MExp
%                         %rank = System.rank;
%                         fprintf('Fatal error : problem in createCellArrayTree\n')
%                         save(['createCellArrayTree.' int2str(System.rank) '.mat']);
%                         if (System.nproc>1)
%                             exit
%                         end
%                     end
                end
            end
            
            % Synchronize parallel data
            
        end
        
        % Joining of alpha for coarsening step
        % The following should be approximated:
        %    alpha_in{n} = Z_in{n} * alpha_out for all n
        %
        function [alpha_out,Z_out,Lv] = joinAlphaGeometric(s,alpha_in,Z_in,Lv,numBoundDof)
            
            % Some sanity checks for the input parameters
            alphaFlags = s.checkEmptyCells(alpha_in);
            ZFlags     = s.checkEmptyCells(Z_in);
            
            if ( all(alphaFlags) ) % && all(ZFlags) )
                % Return empty
                alpha_out = alpha_in{1};
                Z_out = Z_in{1};
                Lv = Lv{1}-1;
                return
            elseif ( any(alphaFlags) ) %&& any(ZFlags) )
                error('SubField:joinAlpha','Partial empty element joins are not supported, please adjust the distribution of elements over ranks')
            end
            
            finiteElement = FECollection.get(1);
            Z = finiteElement.Z;
            
            % Initialize the output
            alpha_out   = NaN( size( alpha_in{1} ) );
            
            % Determine the ranges in each direction
            ranges      = cell( finiteElement.dim,1 );

            for i=1:finiteElement.dim
                numDof      = finiteElement.basis{i}.P;
                ranges{i}   = cell(1,3);
                ranges{i,1} = 1:finiteElement.basis{i}.numBoundDof(1);
                ranges{i,3} = numDof-finiteElement.basis{i}.numBoundDof(2)+1:numDof;
                ranges{i,2} = max(ranges{i,1})+1 : min(ranges{i,3})-1;
            end
            
            refinementRatio = System.settings.mesh.refinementRatio;
            alpha_in = reshape(alpha_in,refinementRatio);
            
            %% vertices
            v = permn([1 3],finiteElement.dim);
            v = sortrows(v,[finiteElement.dim:-1:1]);
            verticesPattern = (finiteElement.dofType==0);
            for i=1:size(v,1)
                verticeFlags = logical( verticesPattern .* (finiteElement.dofTypeID==i) );
                
                position    = cell(1,finiteElement.dim);
                zPosition   = cell(1,finiteElement.dim);
                for d=1:finiteElement.dim
                    position{ d } = ranges{d,v(i,d)};
                    if (v(i,d)==1)
                        zPosition{d} = 1;
                    elseif (v(i,d)==3)
                        zPosition{d} = size(Z,d);
                    else
                        error('LSQDIM:field:subField :: unsupported position for Z')
                    end
                end
                
                zSection = Z{ zPosition{:} };
                zSection = zSection( verticeFlags(:), verticeFlags(:) );
                
                temp = zSection \ alpha_in{ zPosition{:} }( verticeFlags(:) );
                
                alpha_out( position{:} ) = reshape( temp, size(alpha_out( position{:} )) );
            end
            
            %% edges: first all edges in x-direction, etc.
            e = permn([1 2 3],finiteElement.dim);
            e = sortrows(e,[finiteElement.dim:-1:1]);
            e = e( sum(e==2,2)==1, :);
            temp = e(:,:)==2;
            [~,index] = sortrows(temp,[finiteElement.dim:-1:1]);
            e = e(index,:);
            
            edgePattern = (finiteElement.dofType==1);
            for i=1:size(e,1)
                edgeFlags = logical( edgePattern .* (finiteElement.dofTypeID==i) );
                
                position    = cell(1,finiteElement.dim);
                zPosition   = cell(1,finiteElement.dim);
                for d=1:finiteElement.dim
                    position{ d } = ranges{d,e(i,d)};
                    if (e(i,d)==1)
                        zPosition{d} = 1;
                    elseif (e(i,d)==2)
                        zPosition{d} = 1:size(Z,d);
                    elseif (e(i,d)==3)
                        zPosition{d} = size(Z,d);
                    else
                        error('LSQDIM:field:subField :: unsupported position for Z')
                    end
                end
                
                zSection = vertcat( Z{ zPosition{:} } );
                alpha_temp = cat(finiteElement.dim, alpha_in{ zPosition{:} } );
                temp = zSection \ alpha_temp(:);
                temp = temp( edgeFlags );                                
                alpha_out( position{:} ) = reshape( temp, size(alpha_out( position{:} )) );
            end
            
            %% faces: first all faces in x-direction, etc.
            f = permn([1 2 3],finiteElement.dim);
            f = sortrows(f,[finiteElement.dim:-1:1]);
            f = f( sum(f==2,2)==2, :);
            temp = f(:,:)==2;
            [~,index] = sortrows(temp,[1:finiteElement.dim]);
            f = f(index,:);
            
            facePattern = (finiteElement.dofType==2);
            for i=1:size(f,1)
                faceFlags = logical( facePattern .* (finiteElement.dofTypeID==i) );
                
                position    = cell(1,finiteElement.dim);
                zPosition   = cell(1,finiteElement.dim);
                for d=1:finiteElement.dim
                    position{ d } = ranges{d,f(i,d)};
                    if (f(i,d)==1)
                        zPosition{d} = 1;
                    elseif (f(i,d)==2)
                        zPosition{d} = 1:size(Z,d);
                    elseif (f(i,d)==3)
                        zPosition{d} = size(Z,d);
                    else
                        error('LSQDIM:field:subField :: unsupported position for Z')
                    end
                end
                
                zSection = vertcat( Z{ zPosition{:} } );
                alpha_temp = cat(finiteElement.dim, alpha_in{ zPosition{:} } );
                temp = zSection \ alpha_temp(:);
                temp = temp( faceFlags );                                
                alpha_out( position{:} ) = reshape( temp, size(alpha_out( position{:} )) );
            end
            
            %% internal
            i = [2 2 2];
            internalPattern = (finiteElement.dofType==3);
            internalFlags   = logical( internalPattern );
            if (any(internalFlags))
                position        = cell(1,finiteElement.dim);
                zPosition       = cell(1,finiteElement.dim);

                for d=1:finiteElement.dim
                    position{ d } = ranges{d,i(d)};
                    if (i(d)==1)
                        zPosition{d} = 1;
                    elseif (i(d)==2)
                        zPosition{d} = 1:size(Z,d);
                    elseif (i(d)==3)
                        zPosition{d} = size(Z,d);
                    else
                        error('LSQDIM:field:subField :: unsupported position for Z')
                    end
                end

                zSection = vertcat( Z{ zPosition{:} } );
                alpha_temp = cat(finiteElement.dim, alpha_in{ zPosition{:} } );
                temp = zSection \ alpha_temp(:);
                temp = temp( internalFlags );                                
                alpha_out( position{:} ) = reshape( temp, size(alpha_out( position{:} )) );
            end
            
            %if (any(isnan(alpha_out(:))))
            %    disp()
            %end
            
            Z_out       = eye(numel(alpha_out));
            
            % Update Lv after coarsening: decrease refinement level and 
            % return just a single scalar for this joined element
            Lv = Lv{1}-1;  
            
        end
        
        function [var_out,Lv] = joinLogicalGeometric(s,var_in,Lv,numBoundDof)
            
            % Some sanity checks for the input parameters
            varFlags = s.checkEmptyCells(var_in);
            
            if ( all(varFlags) ) % && all(ZFlags) )
                % Return empty
                var_out = var_in{1};
                Lv = Lv{1}-1;
                return
            elseif ( any(varFlags) ) %&& any(ZFlags) )
                error('SubField:joinVar','Partial empty element joins are not supported, please adjust the distribution of elements over ranks')
            end
            
            % if all false, in that case just use that 
            mergeAll = [var_in{:}];
            if all( ~mergeAll(:) )
                var_out = var_in{1};
                Lv      = Lv{1}-1; 
                return
            end
            
            finiteElement = FECollection.get(1);
            Z = finiteElement.Z;
            
            % Initialize the output
            var_out   = NaN( size( var_in{1} ) );
            
            % Determine the ranges in each direction
            ranges      = cell( finiteElement.dim,1 );

            for i=1:finiteElement.dim
                numDof      = finiteElement.basis{i}.P;
                ranges{i}   = cell(1,3);
                ranges{i,1} = 1:finiteElement.basis{i}.numBoundDof(1);
                ranges{i,3} = numDof-finiteElement.basis{i}.numBoundDof(2)+1:numDof;
                ranges{i,2} = max(ranges{i,1})+1 : min(ranges{i,3})-1;
            end
            
            refinementRatio = System.settings.mesh.refinementRatio;
            var_in = reshape(var_in,refinementRatio);
            
            %% vertices
            verticesPattern = (finiteElement.dofType==0);
            numVertices = max( verticesPattern(:) .* finiteElement.dofTypeID(:) );
            for i=1:numVertices
                verticeFlags = logical( verticesPattern .* (finiteElement.dofTypeID==i) );                
                var_out( verticeFlags ) = var_in{ i }( verticeFlags );
            end
            
            %% edges: first all edges in x-direction, etc.   
            e = permn([1 2 3],finiteElement.dim);
            e = sortrows(e,[finiteElement.dim:-1:1]);
            e = e( sum(e==2,2)==1, :);
            temp = e(:,:)==2;
            [~,index] = sortrows(temp,[finiteElement.dim:-1:1]);
            e = e(index,:);
            
            edgePattern = (finiteElement.dofType==1);
            numEdges = max( edgePattern(:) .* finiteElement.dofTypeID(:) );
            for i=1:numEdges
                edgeFlags = logical( edgePattern .* (finiteElement.dofTypeID==i) );
                                
                position    = cell(1,finiteElement.dim);
                zPosition   = cell(1,finiteElement.dim);
                for d=1:finiteElement.dim
                    position{ d } = ranges{d,e(i,d)};
                    if (e(i,d)==1)
                        zPosition{d} = 1;
                    elseif (e(i,d)==2)
                        zPosition{d} = 1:size(Z,d);
                    elseif (e(i,d)==3)
                        zPosition{d} = size(Z,d);
                    else
                        error('LSQDIM:field:subField :: unsupported position for Z')
                    end
                end
                
                zSection = vertcat( Z{ zPosition{:} } );
                var_temp = cat(finiteElement.dim, var_in{ zPosition{:} } );
                temp = zSection \ var_temp(:);
                temp = temp( edgeFlags );                                
                var_out( position{:} ) = reshape( temp, size(var_out( position{:} )) );
            end
            
            %% faces: first all faces in x-direction, etc.
            f = permn([1 2 3],finiteElement.dim);
            f = sortrows(f,[finiteElement.dim:-1:1]);
            f = f( sum(f==2,2)==2, :);
            temp = f(:,:)==2;
            [~,index] = sortrows(temp,[1:finiteElement.dim]);
            f = f(index,:);
            
            facePattern = (finiteElement.dofType==2);
            for i=1:size(f,1)
                faceFlags = logical( facePattern .* (finiteElement.dofTypeID==i) );
                
                position    = cell(1,finiteElement.dim);
                zPosition   = cell(1,finiteElement.dim);
                for d=1:finiteElement.dim
                    position{ d } = ranges{d,f(i,d)};
                    if (f(i,d)==1)
                        zPosition{d} = 1;
                    elseif (f(i,d)==2)
                        zPosition{d} = 1:size(Z,d);
                    elseif (f(i,d)==3)
                        zPosition{d} = size(Z,d);
                    else
                        error('LSQDIM:field:subField :: unsupported position for Z')
                    end
                end
                
                zSection = vertcat( Z{ zPosition{:} } );
                var_temp = cat(finiteElement.dim, var_in{ zPosition{:} } );
                temp = zSection \ var_temp(:);
                temp = temp( faceFlags );                                
                var_out( position{:} ) = reshape( temp, size(var_out( position{:} )) );
            end
            
            %% internal
            i = [2 2 2];
            internalPattern = (finiteElement.dofType==3);
            internalFlags   = logical( internalPattern );
            if (any(internalFlags))
                position        = cell(1,finiteElement.dim);
                zPosition       = cell(1,finiteElement.dim);

                for d=1:finiteElement.dim
                    position{ d } = ranges{d,i(d)};
                    if (i(d)==1)
                        zPosition{d} = 1;
                    elseif (i(d)==2)
                        zPosition{d} = 1:size(Z,d);
                    elseif (i(d)==3)
                        zPosition{d} = size(Z,d);
                    else
                        error('LSQDIM:field:subField :: unsupported position for Z')
                    end
                end

                zSection = vertcat( Z{ zPosition{:} } );
                var_temp = cat(finiteElement.dim, var_in{ zPosition{:} } );
                temp = zSection \ var_temp(:);
                temp = temp( internalFlags );                                
                var_out( position{:} ) = reshape( temp, size(var_out( position{:} )) );
            end
            
            %if (any(isnan(alpha_out(:))))
            %    disp()
            %end
            
%             Z_out       = eye(numel(var_out));
            
            % Update Lv after coarsening: decrease refinement level and 
            % return just a single scalar for this joined element
            Lv = Lv{1}-1;  
            
        end
        
        
        % Joining of alpha for coarsening step
        % The following should be approximated:
        %    alpha_in{n} = Z_in{n} * alpha_out for all n
        %
        function [alpha_out,Z_out,Lv] = joinAlpha(s,alpha_in,Z_in,Lv,numBoundDof)
            
            alphaFlags = s.checkEmptyCells(alpha_in);
            ZFlags     = s.checkEmptyCells(Z_in);
            
            if ( all(alphaFlags) && all(ZFlags) )
                % Return empty
                alpha_out = alpha_in{1};
                Z_out = Z_in{1};
                Lv = Lv{1}-1;
                return
            elseif ( any(alphaFlags) && any(ZFlags) )
                error('SubField:joinAlpha','Partial empty element joins are not supported, please adjust the distribution of elements over ranks')
            end
                
            %Z = s.mesh.disc.Z;
            finiteElement = FECollection.get(1);
            Z = finiteElement.Z;
            
            % Compute the weights of the basis functions for each subElement
            subAlpha = cell(numel(alpha_in),1);
            for i=1:numel(alpha_in)                
                subAlpha{i} = NaN( size(alpha_in{i} ) );                
                cols_with_any_nonzeros  = any(Z{i}~=0);                
                subAlpha{i}(cols_with_any_nonzeros) = Z{i}(:,cols_with_any_nonzeros) \ alpha_in{i}(:); 
            end
            
            refinementRatio = System.settings.mesh.refinementRatio;
            subAlpha = reshape(subAlpha,refinementRatio);
                        
            % Create the new alpha, which is initialized with only NaN,
            % which allows a check which values still need to be set
            alphaNew = zeros( size(alpha_in{1}) );
            alphaNew(alphaNew==0) = NaN;
            
            % Setup the subIterator
            subIterator     = cell(size(refinementRatio));
            for i=1:numel(subIterator)
                subIterator{i} = 1:refinementRatio(i);
            end
            
            numBoundDof( refinementRatio==1 ) = -1;
           
            subIndices  = combvec(subIterator{:});
            ranges      = cell( size(subIndices,1), 2 );
            
            % Setup the lower/upper ranges for boundary dofs
            for dir=1:size(ranges,1)
                minVal = 1;
                maxVal = size(alphaNew,dir);
                
                if (numBoundDof(dir)~=-1)
                    ranges{dir,1} = minVal:numBoundDof(dir)-1+minVal;
                    ranges{dir,2} = maxVal-numBoundDof(dir)+1:maxVal;
                else
                    ranges{dir,1} = minVal:maxVal;
                    ranges{dir,2} = [];
                end
            end
            
            % Extract alphaNew from the corner subAlphas
            for n=1:size(subIndices,2)
                subIndex    = num2cell( subIndices(:,n) );
                data        = subAlpha{ subIndex{:} };
                dataRanges  = cell( size(ranges,1),1 );
                
                for i=1:numel(subIndex)
                    dataRanges{i}  = ranges{i,subIndex{i}};
                end
                
                alphaNew( dataRanges{:} ) = data( dataRanges{:} );
            end
            
            %% Check that the alphaNew is equal to all subAlphas (neglecting NaN)
            checkAll = true;
            maxDiff = 0;
            if (checkAll)
                for n=1:size(subIndices,2)
                    diff = abs( alphaNew-subAlpha{n} ); 
                    maxDiff = max( maxDiff, max(diff(:)) );
                    if ( any(diff( diff==diff ) > 1e-8) )
%                         error('there is a difference between alphaNew and subAlpha{%d} of %e\n',n,max(diff(:)))
                        fprintf('there is a difference between alphaNew and subAlpha{%d} of %e\n',n,max(diff(:)))
                    end
                    
                    for i=1:size(subIndices,2)
                        if (i>n)
                            diff = abs( subAlpha{i}-subAlpha{n} );
                            maxDiff = max( maxDiff, max(diff(:)) );
                            if ( any(diff( diff==diff ) > 1e-8) )
%                                 error('there is a difference between subAlpha{%d} and subAlpha{%d} of %e\n',n,i,max(diff(:)))
                                fprintf('there is a difference between subAlpha{%d} and subAlpha{%d} of %e\n',n,i,max(diff(:)))
                            end
                        end
                    end
                end
                
                %fprintf('The maximum difference is %e\n',maxDiff)
            end

%             % Find the unset locations by checking which values are NaN
%             unsetLocation = (alphaNew~=alphaNew);
            
            % check if all alphaNew values have been set
            if ( nnz( (alphaNew~=alphaNew) ) > 0 )
                for i=1:numel(subAlpha)
                    alphaNew( alphaNew~=alphaNew ) = subAlpha{i}( alphaNew~=alphaNew );
                end
            end
%             
%             ZAll = cell2mat(Z_in);            
%             for i=1:4
%                 alpha_in{i} = alpha_in{i}(:);
%             end
%             alphaAll = cell2mat(alpha_in);
%             
%             % Update the rhs by subtracting the current solution from the 
%             % current alphaAll for all subElements
%             rhs = (alphaAll - ZAll(:,~unsetLocation) * alphaNew(~unsetLocation));
%             
%             test = nnz(ZAll( and(ZAll~=0,ZAll~=1) ));
%             
%             sideBySide = true;
%             
%             if (sideBySide)
%                 % Solve remaining unknowns side by side
%                 % Bottom
%                 pattern = false(size(unsetLocation));
%                 pattern(:,1:2,:) = unsetLocation(:,1:2,:);
%                 if (nnz(pattern)>0)
%                     A = [    Z_in{1};    Z_in{2}];
%                     b = [alpha_in{1};alpha_in{2}];
%                     rhs = (b - A(:,~unsetLocation) * alphaNew(~unsetLocation));
%                     nonzeroRows = logical ( A*pattern(:) );           
%                     alphaNew(pattern) = A(nonzeroRows,pattern(:)) \ rhs(nonzeroRows);
%                     unsetLocation(pattern) = false;
%                 end
%                 
%                 % Left
%                 pattern = false(size(unsetLocation));
%                 pattern(1:2,:,:) = unsetLocation(1:2,:,:);
%                 if (nnz(pattern)>0)
%                     A = [    Z_in{1};    Z_in{3}];
%                     b = [alpha_in{1};alpha_in{3}];
%                     rhs = (b - A(:,~unsetLocation) * alphaNew(~unsetLocation));
%                     nonzeroRows = logical ( A*pattern(:) );
%                     alphaNew(pattern) = A(nonzeroRows,pattern(:)) \ rhs(nonzeroRows);
%                     unsetLocation(pattern) = false;
%                 end
%                 
%                 % Right
%                 pattern = false(size(unsetLocation));
%                 pattern(end-1:end,:,:) = unsetLocation(end-1:end,:,:);
%                 if (nnz(pattern)>0)
%                     A = [    Z_in{2};    Z_in{4}];
%                     b = [alpha_in{2};alpha_in{4}];
%                     rhs = (b - A(:,~unsetLocation) * alphaNew(~unsetLocation));
%                     nonzeroRows = logical ( A*pattern(:) );           
%                     alphaNew(pattern) = A(nonzeroRows,pattern(:)) \ rhs(nonzeroRows);
%                     unsetLocation(pattern) = false;
%                 end
%                 
%                 % Top
%                 pattern = false(size(unsetLocation));
%                 pattern(:,end-1:end,:) = unsetLocation(:,end-1:end,:);
%                 if (nnz(pattern)>0)
%                     A = [    Z_in{3};    Z_in{4}];
%                     b = [alpha_in{3};alpha_in{4}];
%                     rhs = (b - A(:,~unsetLocation) * alphaNew(~unsetLocation));
%                     nonzeroRows = logical ( A*pattern(:) );           
%                     alphaNew(pattern) = A(nonzeroRows,pattern(:)) \ rhs(nonzeroRows);
%                     unsetLocation(pattern) = false;
%                 end
%                 
%                 % Center
%                 pattern = false(size(unsetLocation));
%                 pattern(3:end-2,3:end-2,:) = unsetLocation(3:end-2,3:end-2,:);
%                 if (nnz(pattern)>0)
%                     rhs = (alphaAll - ZAll(:,~unsetLocation) * alphaNew(~unsetLocation));
%                     nonzeroRows = logical ( ZAll*pattern(:) );           
%                     alphaNew(pattern) = ZAll(nonzeroRows,pattern(:)) \ rhs(nonzeroRows);
%                     unsetLocation(pattern) = false;
%                 end
%                 
%             else
%                 % Solve all remaining unknowns at once
%                 nonzeroRows = logical ( ZAll*unsetLocation(:) );           
%                 alphaNew(unsetLocation) = ZAll(nonzeroRows,unsetLocation(:)) \ rhs(nonzeroRows);
%             end
%             
%             
%             
%             Z_out = eye(numel(alpha_in{1}));
            
            
            Z_out       = eye(numel(alphaNew));
            alpha_out   = alphaNew;
            
            % Update Lv after coarsening: decrease refinement level and 
            % return just a single scalar for this joined element
            Lv = Lv{1}-1;            
        end
        
        function write(s)
            write_vtu( s, System.settings.time.iter, System.settings.time.current, System.parallel.rank, System.parallel.nproc );
        end
        
        function out = getData(s,dataType,elementNumber,pointFlags)
            
            element         = s.mesh.getElement( elementNumber );            
            elementAlpha    = s.alpha{ elementNumber }(:);
            
            Ze = element.Z;
            
            if (nargin==3)
                pointFlags = true( element.disc.dofevq , 1 );
            end
            
            switch dataType
                case Derivative.value
                    out = s.value{ elementNumber }( pointFlags );
                case Derivative.x
                    Dx  = element.getDx;   out = Dx(pointFlags,:)  * ( Ze * elementAlpha );
                case Derivative.y
                    Dy  = element.getDy;   out = Dy(pointFlags,:)  * ( Ze * elementAlpha );
                case Derivative.z
                    Dz  = element.getDz;   out = Dz(pointFlags,:)  * ( Ze * elementAlpha );
                case Derivative.xx
                    Dxx = element.getDxx;  out = Dxx(pointFlags,:) * ( Ze * elementAlpha );
            end
        end
        
        % General plot function, where the plotVar input parameter can be
        % one of the options in the Derivative enumeration. Possible
        % options can be displayed by typing 'Derivative.options'.
        function plot(s,plotVar)
            %clf;
            
            if nargin==1
                plotVar = Derivative.value;
                fprintf('Assuming Derivative.Value as a 2nd input parameter, so the value will be plotted\n')
            end
            
            layer = 1;
            
            isSpaceTime = (System.settings.time.method==TimeMethod.SpaceTime);
            
            if (s.field.plotOrder>0)
                integrateHO = true;
                plotFE = FiniteElement(System.settings.stde.Pn,s.field.plotOrder*ones(size(System.settings.stde.Q)),System.settings.stde.C,s.mesh.sDim,s.mesh.spaceTime);
%                 = System.getIntegrationDisc;
            else
                integrateHO = false;
            end
            
            id = 0;
            for e=s.mesh.eRange %1:numel(s.alpha)
                id = id+1;
                element = s.mesh.getElement(e);
                
                if (integrateHO)
                    element.setFinite( plotFE );
                end
                
                data = reshape(s.getValue( plotVar, e ),element.finite.qSize);
                [X,Y,Z,T] = element.getNodes();
                X = squeeze(X); Y = squeeze(Y);
                Z = squeeze(Z); T = squeeze(T);
                if (isSpaceTime)
                    if (System.settings.mesh.sdim==2)
                        if (layer==1)
                            %CH = PhysicsCollection.get(2);
                            %C = CH.fields{1};
                            %cdata = reshape(C.getValue(  e ),element.finite.qSize);
                            %plotData = cdata(:,:,1) .* data(:,:,end);
                            plotData = data(:,:,end);
                        elseif (layer==0)
                            plotData = data(:,:,1);
                        end
                        surf(X(:,:,end),Y(:,:,end),plotData,'FaceColor','interp');
                    else
                        if (layer==1)
                            plotData = data(:,end);
                        elseif (layer==0)
                            plotData = data(:,1);
                        end
                        %plot(X(:,end),plotData,'Color','k');
                        surf(X,T,data,'FaceColor','interp');
                    end
                else
                    plotData = data;
                    if (System.settings.mesh.sdim==1)
                        plot(X(:,end),plotData,'Color','k');
                    elseif (System.settings.mesh.sdim==2)
                        surf(X,Y,plotData,'FaceColor','interp');
                    elseif (System.settings.mesh.sdim==3)
                        surf(X(:,:,1),Y(:,:,1),plotData(:,:,1),'FaceColor','interp');
                    else
                        error('Check Subfield.plot : only sDim == 1 and == 2 are supported')
                    end
                end
                
                hold on;
                
                if (integrateHO)
                    element.setFinite(); % reset to computation finite element
                end
            end
            
            if (System.settings.mesh.dim==3)
                x0 = s.mesh.X(1,1);
                x1 = s.mesh.X(2,1);
                y0 = s.mesh.X(1,2);
                y1 = s.mesh.X(2,2);

                pbx = x1 - x0;
                pby = y1 - y0;
                pbaspect([pbx pby 1])

                colorbar;
                view([25,25]);        
            
            else
                x0 = s.mesh.X(1,1);
                x1 = s.mesh.X(2,1);
                y0 = s.mesh.X(1,2);
                y1 = s.mesh.X(2,2);

                pbx = x1 - x0;
                pby = y1 - y0;
                pbaspect([pbx pby 1])

                colorbar;
                view([25,25]);   
                
            end
        end
        
        function plotPatch(s,patchID,derivative)
            clf;
            
            patch    = s.mesh.patchList{ patchID };
            
            elements = patch.elements;
            nodes    = patch.nodes;
            eNumbers = patch.localElementNumbers;
                        
            for e=1:patch.numElements
                element = elements(e);
                
                % Get the quadrature coordinates in all directions for this element
                [c{1},c{2},c{3},c{4}] = element.getNodes;
                
                % Use the node pattern from the patch to take a subset of
                % these coordinates
                c = cellfun( @(x) x(nodes{e}), c , 'UniformOutput', false);
                
                % Detect which ranges are zero, at most 2 should be large
                % than zero.
                ranges  = cellfun( @(x) range(x), c );
                plotDir = (ranges>0);
                
                % Select the data to plot
                data = s.getData( derivative, eNumbers(e), nodes{e} );
                %data = s.value{ eNumbers(e) }( nodes{e} );
                
                if (nnz(plotDir)==2)
                    % 2D plot
                    
                    dir   = find(plotDir);
                    shape = element.disc.dofeq( dir );
                    
                    c1   = reshape( c{dir(1)}, shape );
                    c2   = reshape( c{dir(2)}, shape );
                    data = reshape( data     , shape );
                    surf( c1, c2 , data,'FaceColor','interp');
                else
                    error('Only 2D plotting is provided for now')
                end
                
                hold on;
            end
            
            % Set title & axis labels
            title( sprintf('Patch %d : %s (%s)',patch.id,patch.name,patch.conditions.name) )
            xlabel( Dir( dir(1) ).string )
            ylabel( Dir( dir(2) ).string )
            zlabel( s.fullname )
            
            % Set aspect ratios
            pbx = s.mesh.X(dir(1),2) - s.mesh.X(dir(1),1);
            pby = s.mesh.X(dir(2),2) - s.mesh.X(dir(2),1);
            pbaspect([pbx pby 1])
            
            %shading interp;
            colorbar;
            
            view([25,25]);
            
        end
        

        function plotFEnergy1(s)
            % Fenergy = (3*Ck.^2 - 3*Ck + 0.5).*H - s.Cn^2 * ( Nabla2 );
            clf;
                        
            layer = 1;
            
            factor = 1;%424.2641;
            
            for e=1:numel(s.alpha)
                element = s.mesh.getLocalElement(e);
                
                Ck = s.value{e}(:);
                
                Ze = s.mesh.Z{e};
                
                Cn = System.settings.phys.Cn;
                Nabla2 = element.getDxx + element.getDyy;
                data = ( (3*Ck.^2 - 3*Ck + 0.5).*element.getH - Cn^2 * ( Nabla2 ) ) * Ze * s.alpha{e}(:);
                %data = factor * ( (3*Ck.^2 - 3*Ck + 0.5) );
%                 data = element.getH * Ze * s.alpha{e}(:);

                data = data - (2*Ck.^3 - 1.5*Ck.^2);
                
                data = data * factor;
                
                data = reshape(data,element.finite.qSize);
                [X,Y] = element.getNodes;
                if (layer==1)
                    plotData = data(:,:,end);
                elseif (layer==0)
                    plotData = data(:,:,1);
                end
                surf(X(:,:,:,end),Y(:,:,:,end),plotData,'FaceColor','interp');
                hold on;
            end
            
            x0 = s.mesh.X(1,1);
            x1 = s.mesh.X(2,1);
            y0 = s.mesh.X(1,2);
            y1 = s.mesh.X(2,2);
            
            pbx = x1 - x0;
            pby = y1 - y0;
            pbaspect([pbx pby 1])
               
            colorbar;
            view([25,25]);
        end
        
        function plotFEnergy2(s)
            % Fenergy = (2*Ck.^3 - 1.5*Ck.^2)
            clf;
                        
            layer = 1;
            
            for e=1:numel(s.alpha)
                element = s.mesh.getLocalElement(e);
                
                Ck = s.value{e}(:);
                Cn = System.settings.phys.Cn;
                data = (2*Ck.^3 - 1.5*Ck.^2);
                
                data = reshape(data,element.finite.qSize);
                [X,Y] = element.getNodes;
                if (layer==1)
                    plotData = data(:,:,end);
                elseif (layer==0)
                    plotData = data(:,:,1);
                end
                surf(X(:,:,:,end),Y(:,:,:,end),plotData,'FaceColor','interp');
                hold on;
            end
            
            x0 = s.mesh.X(1,1);
            x1 = s.mesh.X(2,1);
            y0 = s.mesh.X(1,2);
            y1 = s.mesh.X(2,2);
            
            pbx = x1 - x0;
            pby = y1 - y0;
            pbaspect([pbx pby 1])
               
            colorbar;
            view([25,25]);
        end
        
        
        function plotValue(s,titleString,layer,plotType,offset,solutionMode)
            
            if (s.needsMeshUpdate)
                s.meshUpdate;
            end
            
            if (nargin==1)
                % default values
                titleString  = '';                      % title for this plot
                layer        = 1;                       % 0=t0 and 1=t1 (for spaceTime field)
                plotType     = 1;                       % 0=compute value from internal alpha field, and 1=use present value
                offset       = 0;                       % for parallel fields
                solutionMode = SolutionMode.Normal;     % by default no relaxation of the values
            end
            
            if (plotType==1)
                %if (nargin==6)
                    data = s.value;
                %end
            elseif (plotType==0)
                data = s.computeValue;
                fprintf('Plotting with a computed value\n');
            end
            
            % Check if argument is supplied to this function, and set
            % default values if the variable is not present
            if (~exist('title','var'))
                if (plotType==0)
                    titleString = ['Field ' s.name ' (alpha)'];
                elseif (plotType==1)
                    titleString = ['Field ' s.name ' (value)'];
                end
            end
            if (~exist('layer','var'))
                layer = 1;
            end
            if (~exist('plotType','var'))
                plotType = 1;
            end
            if (~exist('offset','var'))
                offset = 0;
            end
            

            id = 0;
            for e=1:s.mesh.numLocalElements
                id = id+1;
                
                element         = s.mesh.getLocalElement(e+offset);
                finiteElement   = element.finite;
                
                [X,Y,Z,T] = element.getNodes;
                
                X = squeeze(X);
                Y = squeeze(Y);
                Z = squeeze(Z);
                T = squeeze(T);
                
                if (size(data{ id },2)==1)
                    data{ id } = reshape(data{ id },finiteElement.qSize);
                    
                    if (layer==1)
                        plotID = surf(X(:,:,end),Y(:,:,end),data{ id }(:,:,end),'FaceColor','interp');
                    elseif (layer==0)
                        plotID = surf(X(:,:,1),Y(:,:,1),data{ id }(:,:,1),'FaceColor','interp');
                    end
                else
                    if (layer==1)
                        plotID = surf(X(:,:,end),Y(:,:,end),data{ id }(:,:,end),'FaceColor','interp');
                    elseif (layer==0)
                        plotID = surf(X(:,:,1),Y(:,:,1),data{ id }(:,:,1),'FaceColor','interp');
                    end
                end
                hold on;
            end
            
            pbx = s.mesh.X(2,1) - s.mesh.X(1,1);
            pby = s.mesh.X(2,2) - s.mesh.X(1,2);
            
            if ( (s.mesh.dim > s.mesh.sDim) && s.mesh.dim==2 )
                pby = 1;
            end
            
            pbaspect([pbx pby 1])
            
            %shading interp;
            colorbar;
            
            view([25,25]);
            title(titleString);            
        end
        
        function out = save(s)
            
%             % original
%             out.alpha  = s.alpha;
%             out.value  = s.value;
%             out.name   = s.name;
%             out.meshID = s.meshID;
            
            % new 
            out.localAlpha  = s.localAlpha;
            out.name        = s.name;
            out.meshID      = s.meshID;
            out.relaxation  = s.relaxation;
        end
        
        function load(s,data)

            
            % new
            if ( isfield(data,'localAlpha') )
                s.meshID     = data.meshID;
                s.name       = data.name;
                s.relaxation = 0.8; %data.relaxation;
                s.localAlpha = data.localAlpha;         % load the localAlpha array
                s.convertAlpha(1);                      % convert localAlpha --> alpha
                s.computeValue;                         % use alpha to compute the values
            
            else
                % original
                s.alpha  = data.alpha;
                s.value  = data.value;
                s.meshID = data.meshID;
                s.convertAlpha(0);      % alpha --> localAlpha
                
            end
            
%             else
%                 s.alpha  = data.alpha( s.mesh.eRange );
%                 s.value  = data.value( s.mesh.eRange );
%                 s.meshID = data.meshID;
%             end
        end
        
        function out = mtimes(arg1_in,arg2_in)
            temp = SubField(arg2_in.field);
            
            if isnumeric(arg1_in)
                arg1 = arg1_in;
            elseif (isa(arg1_in,'SubField'))
                arg1 = arg1_in.alpha;
            end
            
            if isnumeric(arg2_in)
                arg2 = arg2_in;
            elseif (isa(arg2_in,'SubField'))
                arg2 = arg2_in.alpha;
            end
           
            out = cell( size(arg2) );
            for n=1:numel(arg2)
                temp.alpha{n} = arg1 * arg2{n};
            end
        end
    end
end

