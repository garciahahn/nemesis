function [alf,iter]=PrecElemCG_par_light(Ke,Ge, GM, mutualGM, globalDof, localDof, epsilon, x_init)

    rank = Parallel3D.MPI.rank;

    UPLO = 'L';

    if nargin<7
        x_init    = zeros(localDof,1);
        fprintf('x_init is reset to zero\n')
    end
    
    % Convergence
    tol2        = epsilon*epsilon;
    maxIter     = 50000;
    outputStep  = 5000;

    % MPI
    rank        = Parallel3D.MPI.rank;
    Master_rank = 0;   
    e_start     = Parallel3D.MPI.eStart;
    e_end       = Parallel3D.MPI.eEnd;
    
    numLocalElements = e_end-e_start+1;
    
    % flags
    oldStyle    = true;        % true=full solution method, false=elemental method (each element has it's own local solution)
    elemental   = false;
    prec        = 0;

    % timing
    tStart      = toc;
    temp1       = toc;
    t           = zeros(15,1);
    
    %% Adjust GM for strong boundaries (renumbering)
    % remove empty (=prescribed) elements from M
    isPrescribed = true(globalDof,1);
    for e=1:numLocalElements
        isPrescribed( GM{e} ) = false;
    end
    numExternalDof = globalDof-localDof;
    numPrescribed = nnz(isPrescribed)-numExternalDof;
    numFree = localDof-numPrescribed;

    fprintf('RANK %d -- There are locally %d free DOFs, and %d prescribed DOFs\n',Parallel3D.MPI.rank,numFree,numPrescribed);
     
    numPrescribedAll = NMPI_Allreduce(numPrescribed,1,'+',Parallel3D.MPI.mpi_comm_world);
    numFreeAll       = NMPI_Allreduce(numFree,1,'+',Parallel3D.MPI.mpi_comm_world);
    numMutualDof     = numFreeAll - globalDof;
    if (Parallel3D.MPI.rank==0)
        fprintf('There are globally %d free DOFs, %d prescribed DOFs, and %d mutual DOFs\n',globalDof,numPrescribedAll,numMutualDof);
    end
    
    numelGM = numel(GM{1});
    constantSize = true;
    for i=1:numLocalElements
        if (numel(GM{i}) ~= numelGM)
            constantSize = false;
            break;
        end
    end
        
    if (constantSize)
        GM2 = int32([GM{1:numLocalElements}]);
    end
    
    transfer = cell(Parallel3D.MPI.nproc,1); 
    GMold = GM;
    GMnew=zeros(localDof,1);
    GMfree=(1:numFree)';
    GMnew(~isPrescribed)=GMfree;        
    for e=1:numLocalElements
        GM{e} = GMnew(GM{e});
    end

    nVar = 3;
    
    for i=1:Parallel3D.MPI.nproc
        if (i~=Parallel3D.MPI.rank+1)
            transfer{i} = [GMnew( intersect(GM2,full(mutualGM{i})) )];
            for n=2:nVar
                transfer{i} = [transfer{i}; GMnew( intersect(GM2,full(mutualGM{i})) )+(n-1)*1000];
            end
        end
    end

    filename = ['transfer_' num2str(Parallel3D.MPI.rank) '.mat' ];
    save(filename,'transfer');
    
    
    %% Compute A*x
    for e=1:numLocalElements
        GM{e} = GM{e}(:);
        Ke{e} = full(Ke{e});
        Ge{e} = full(Ge{e});
    end    
    
    if (~elemental)
        sizeKe = size(Ke{1});
        constantSize = true;
        for i=1:numLocalElements
            if (size(Ke{i}) ~= sizeKe)
                constantSize = false;
                break;
            end
        end
        
        if (constantSize)  % weak
            if (rank==0) 
                disp('Constant sized Ke')
            end
            
            strongSolver = false;
            
%             Ge_local = full([Ge{e_start:e_end}]);
            Ke_local = full([Ke{1:numLocalElements}]);
            GM2      = int32([GM{1:numLocalElements}]);

            x = x_init;
            Ax_local = matvec_all(Ke_local,GM2,x,transfer);       % U=0 at the initial, originally, R = b-Ax = F - K*U
    %         Ax_local = matvec_all_logical(Ke_local,GM2,x);
            Ax = Ax_local;
        else
            strongSolver = true;
            if (length(x_init)==localDof) 
                x = x_init(~isPrescribed);
            elseif (length(x_init)==numFree) 
                x = x_init;
            else
                error('The x_init provided to the iterative solver is unsupported')
            end
            
            Ax_local = matvec_one(UPLO,numLocalElements,Ke,x,GM);
            Ax = Ax_local;
        end
    else
        x = cell(numLocalElements,1);
        
        % Convert x into elemental x
        for e=1:numLocalElements
            x{e} = x_init(GM{e});
        end
        
        Ax = matvec_element(Ke,x,numLocalElements,transfer);       % U=0 at the initial, originally, R = b-Ax = F - K*U
    end
 

    
    temp2 = toc; t(1) = t(1) + temp2-temp1; temp1 = toc;
    
    %% Compute residual R, delta0 (ry_old), 
    if (~elemental)
        M    = zeros(numFree,1);          % extract only diagonal components of Ke and assembly them
        Minv = zeros(numFree,1);          % extract only diagonal components of Ke and assembly them
        R    = zeros(numFree,1) ;

        % construction of preconditioner M & R vector
        for e=1:numLocalElements                                      % length(GM): number of elements
            for ind=1:numel(GM{e})                                  % dofL: Nvar*number of points in one element
                M(GM{e}(ind),1) = M(GM{e}(ind),1) + Ke{e}(ind,ind);
            end 
            R(GM{e}) = R(GM{e}) + Ge{e};
        end
        
        fprintf('RANK %d -- numel(R) = %d and numel(Ax) = %d\n',Parallel3D.MPI.rank,numel(R),numel(Ax))

        R = R - Ax;
        norm_res2_0 = dot(R,R);
        norm_res2   = norm_res2_0;
        sqrtnorm_res2_0 = sqrt(norm_res2_0);
        
        fprintf('initial residual norm = %e\n',norm_res2_0)
        
        p = zeros(numFree,1);
        
        M = NMPI_Allreduce(M,numFree,'+',Parallel3D.MPI.mpi_comm_world) ;
        R = NMPI_Allreduce(R,numFree,'+',Parallel3D.MPI.mpi_comm_world) ;

%         if rank==Master_rank
            Minv        = (1./M);
            y           = Minv.*R;
            p           = y;
            delta0      = dotProduct(R,y);  % delta0      = R'*y;
            delta_init  = delta0;
%         end 
    else
        Minv = cell(numLocalElements,1);
        R    = cell(numLocalElements,1);
        p    = cell(numLocalElements,1);
%         w    = cell(e_end-e_start+1,1);
        y    = cell(numLocalElements,1);
        
        dof  = size(Ke{1},1);
        
        for e=1:numLocalElements
%             Minv{e} = zeros()
            Minv{e} = zeros(dof,1);          % extract only diagonal components of Ke and assembly them
            R{e}    = zeros(dof,1) ;
            p{e}    = zeros(dof,1);
            y{e}    = zeros(dof,1);
        end

        % construction of preconditioner M & R vector
        for e=1:numLocalElements                                        % length(GM): number of elements
            for ind=1:dof                                           % dofL: Nvar*number of points in one element
                Minv{e}(ind,1) = 0*Minv{e}(ind,1) + 1.0/Ke{e}(ind,ind);
            end 
            R{e} = Ge{e} - Ax{e};
        end
        
        norm_res2_0 = 0;
        delta0      = 0;
        
        for e=1:numLocalElements
            norm_res2_0 = norm_res2_0 + dot(R{e},R{e});
        end
        
        sqrtnorm_res2_0 = sqrt(norm_res2_0);
        
        fprintf('initial residual = %f\n',sqrtnorm_res2_0)
        
        % all reduce between proc
                
%         Minv(M==0)  = 0;
        for e=1:numLocalElements
            y{e}        = Minv{e}.*R{e};
            p{e}        = y{e};
            delta0      = delta0 + dot(R{e},y{e});
        end
    end
    
    iter=0;
    
    temp2 = toc; t(2) = t(2) + temp2-temp1; 

    if (norm_res2_0 > tol2)
        for iter=1:maxIter

            temp1 = toc;

            if (oldStyle)
                p=NMPI_Bcast3(p,numFree,Master_rank,rank);
            end

            temp2 = toc;
            t(3) = t(3) + temp2-temp1;
            temp1 = toc;

    %         if (p(1)==1) && (p(2)==2) && (p(3)==3) 
    %             break;
    %         end
            if (~elemental)
                if (strongSolver)
                    w = matvec_one(UPLO,numLocalElements,Ke,p,GM);
                else
                    w = matvec_all(Ke_local,GM2,p,transfer);
                end
            else
                w = matvec_element(Ke,p,1,numLocalElements);       % U=0 at the initial, originally, R = b-Ax = F - K*U
            end

            temp2 = toc;
            t(4) = t(4) + temp2-temp1;

            temp1 = toc;

            w = NMPI_Reduce(w,numFree,'+',Master_rank) ;

            temp2 = toc;
            t(5) = t(5) + temp2-temp1;
            temp1 = toc;

            if rank==Master_rank
                
                alpha = delta0 / dot(p,w);

                x = x + alpha*p;
                Rold = 0*R;  % flexible CG (see Wikipedia)
                R = R - alpha*w;                

                norm_res2 = dot(R,R);

                % Relative residual norm
                if (norm_res2_0>1)
                    norm_res2 = norm_res2 / norm_res2_0;
                end

                y = Minv.*R;

                delta1 = (R-Rold)'*y;

                if rem(iter,outputStep)==0
                    fprintf('          %5d       residual: %e %e %e\n',iter,sqrt(norm_res2),sqrt(delta1),epsilon)
%                     fprintf('          %5d       timings : %f %f %f %f\n',iter,t(3),t(4),t(5),t(6))
%                     break;
                end

                if (delta1 <= tol2)
                    fprintf('          %5d       residual: %e %e %e\n',iter,sqrt(norm_res2),sqrt(delta1),tol)
                end

                beta   = delta1 / delta0;
                delta0 = delta1;

                p = y + beta*p;

            else
                delta1 = 0;

            end %if

            norm_res2 = NMPI_Bcast3(norm_res2,1,Master_rank,rank);
            delta1 = NMPI_Bcast3(delta1,1,Master_rank,rank);
            if (delta1 <= tol2)
                break;
            end

            temp2 = toc;
            t(6) = t(6) + temp2-temp1;

        end %while
    end

    temp1 = toc;

    x = NMPI_Bcast3(x,numFree,Master_rank,rank);
    
    if (length(x_init)==numFree) 
        x_out = x;
    elseif (length(x_init)==dof) 
        x_out = zeros(dof,1);
        x_out(isPrescribed)  = x_init(isPrescribed);
        x_out(~isPrescribed) = x;
%     elseif (length(x_init)==numFree) 
%         x_out = x_init;
    else
        error('The x_init provided to the iterative solver is unsupported')
    end
    
%     if (dof_all ~= dof)
%         alf = zeros(dof_all,1);
%         alf(GM_global(:,1)~=0,1) = x;
%     else
        alf = x_out;
%     end
    
    temp2 = toc;
    t(7) = t(7) + temp2-temp1;
    temp1 = toc;
    
%     if my_rank==Master_rank
% %         fprintf('mv_product : %e %e %e\n',t_mvProduct1,t_mvProduct2,t_mvProduct3)
%         fprintf('mv_product : %e \n',   t_mvProduct3)
%         fprintf('reduceB    : %e\n',t_reduceB)
%     end    
    
%     if my_rank==Master_rank
%         fprtinf('     iteration for CG: %5d', iter);
%     end

%     endTime = toc;
    
%     time1 = t1 - startTime;
%     time2 = t2 - t1;
%     time3 = endTime - t2;
%     time4 = endTime - startTime;
    
%     if (Parallel3D.MPI.rank==0)
%         fprintf('Solver timings (%d) : %f %f %f %f \n',iter,time1,time2,time3,time4)
%     end

    temp2 = toc;
    t(8) = t(8) + temp2-temp1;
    
    tEnd = toc;
    
    if (Parallel3D.MPI.rank==0)
        fprintf('Solver timings (%d) : %f ',iter,t(1))
        for i=2:7
            fprintf(' %f ',t(i));
        end
        fprintf(' %f %f\n',t(8),tEnd-tStart);
    end
end %function ElemCG
% 
% function R=loc2gbl_matrix_vector(e_start,e_end,Ke, D, GM, dof)
%     R = zeros(dof,1) ;
% 
%     %JF Parallelization**************************************************
%     for e=e_start:e_end
%         I = GM{e};
%         Ge=D(I);
%         C = Ke{e-e_start+1}*Ge ;
%         R(I) = R(I) + C ;
%     end 
% end
% 
function [Ke,Ge,GM_out] = sortArrays(Ke,Ge,GM,dof)
    GM_out = cell(length(Ke),1);
    
    % Make Ke full again
    %% Upper format
%     for i=1:length(Ke)
%         Ke{i} = triu(Ke{i},1)'+Ke{i};
%         [~,idx]=sort(GM{i});
%         Ke{i} = Ke{i}(idx,idx);     % shuffle columns of Ke in sorted order
%         Ke{i} = triu(Ke{i});
%         Ge{i} = Ge{i}(idx);
%         GM_out{i} = sparse(false(dof,1));
%         GM_out{i}(GM{i}(:)) = true;
%     end
    
    %% Lower format
    for i=1:length(Ke)
        Ke{i} = tril(Ke{i},1)'+Ke{i};
        [~,idx]=sort(GM{i});
        Ke{i} = Ke{i}(idx,idx);     % shuffle columns of Ke in sorted order
        Ke{i} = tril(Ke{i});
        Ge{i} = Ge{i}(idx);
        GM_out{i} = sparse(false(dof,1));
        GM_out{i}(GM{i}(:)) = true;
    end
end

function R = matvec_one(ul,numLocalElements,Ke, D, GM)
    R = zeros(length(D),1) ;
    switch ul
        case 'U'
            for e=1:numLocalElements
                R(GM{e}) = R(GM{e}) + dsymv( Ke{e} , D(GM{e}) );
            end
        case 'L'
            for e=1:numLocalElements
                R(GM{e}) = R(GM{e}) + dsymvL( Ke{e} , D(GM{e}) );
            end   
    end
end

function R = matvec_all(Ke,GM,p,transfer)
%     R = dsymvav( GM, Ke, p); % Ke in upper format
%     p = syncElementBoundaries(p,transfer);
%     size(p)
    R = dsymvavL( GM, Ke, p); % Ke in lower format
end

function R = matvec_all_logical(Ke,GM,p)
    R = dsymvav_logical( GM, Ke, p); 
end

function b = matvec_element(A,x,numLocalElements,transfer)
%     x = syncElementBoundaries(x,numLocalElements,transfer);
    b = cell(numLocalElements,1);
    for e=1:numLocalElements
        b{e} = dsymvavLelement(A{e},x{e});
    end
end

function c = dotProduct(a,b,numLocalElements)
    c = 0;
    if nargin==4
        for e=1:numLocalElements
            c = c + dot(a{e},b{e});
        end
    elseif nargin==2
        if (iscell(a) && iscell(b))
            c = dot(vertcat(a{:}),vertcat(b{:}));
        else
            c = dot(a,b);
        end
    end
    c = NMPI_Allreduce(c,1,'+',Parallel3D.MPI.mpi_comm_world) ; 
end

function x_out = syncElementBoundaries(x,transfer)    
    for i=Parallel3D.MPI.nproc:-1:1
        if (i<Parallel3D.MPI.rank+1)
            % receive from lower rank
%             fprintf('Receiving %d values on rank %d (sending from rank %d)\n',numel(transfer{i}),Parallel3D.MPI.rank,i-1)
            count  = numel(transfer{i});
            source = i-1;
            x_tmp = NMPI_Recv( count, source );
            x(transfer{i}) = x_tmp;
        elseif (i>Parallel3D.MPI.rank+1)
            % send to higher rank
%             fprintf('Sending %d values from rank %d (receiving on rank %d)\n',numel(transfer{i}),Parallel3D.MPI.rank,i-1)
            data   = x(transfer{i});
            count  = numel(transfer{i});
            dest   = i-1;
            NMPI_Isend( data, count, dest );
        else
            % do nothing
            x_out = x;
        end        
    end
%     for e=1:numLocalElements
%         x;
%     end
end