clear all;

load('sparseTestData.mat')

for i=1:numel(data.A)
    data.A{i} = sparse(data.A{i});
    data.b{i} = full(data.b{i});
end

x = dsymavL_sparse(data.A,data.b,data.GM);