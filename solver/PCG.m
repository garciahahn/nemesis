function x = PCG(A,b,tol,x_init)
    
    numFree = size(A,2);
   
    if nargin<3
        tol2 = 1e-12;
    else
        tol2 = tol*tol;
    end
    
    if nargin<4
        x_init = zeros(numFree,1);
    end
    
    %% Compute residual R, delta0 (ry_old)
    R       = b - A * x_init;
    M       = diag(A);
    
    Minv   	= (1./M);
    y   	= Minv.*R;
    
    delta1 = R'*y;  % delta0      = R'*y;
    
    if (delta1 > tol2)
        for iter=1:maxIter

            % Step 1: Compute w and pw
            if (numLocalElements>0)
                if (matvecPerElement)
                    w = matvec_one(UPLO,numLocalElements,Ke,p,GM,transfer);
                else
                    w = matvec_all(UPLO,Ke_all,GM_all,p,transfer);
                end
            else
                w=0;
            end
            
            pw = dotProduct(p,w,transfer);

            % Step 2: Compute alpha
            alpha = delta1 / (pw);      % delta1 = ry

            % Step 3: Compute new x
            x = x + alpha*p;
            
            x_norm_old = x_norm;
            x_norm = norm(x-x_start);
            
            %fprintf('in loop on rank %d\n',rank)
            
            x_norm = NMPI_Allreduce(x_norm,1,'+',Parallel3D.MPI.mpi_comm_world,'x_norm',debug) ;
            
            % Step 4: Compute new R
            R = R - alpha*w;

            if (outputProgress)
                x_diff = max(abs(1-x./x_start));
                x_diff = NMPI_Allreduce(x_diff,1,'M',Parallel3D.MPI.mpi_comm_world,'x_diff',debug) ; 
            
                deltaconv = abs((delta1)-(delta_ref))/(delta1);

                if (rem(iter,25)==0)
                    delta_ref = delta1;
                end
            
                data(1,iter) = iter;
                data(2,iter) = alpha;
                data(3,iter) = beta;
                data(4,iter) = delta1;
                data(5,iter) = x_diff;
                data(6,iter) = x_norm; 
                data(7,iter) = deltaconv;
            end
            
            % Step 5: Check for convergence & output information
            %norm_res2 = dotProduct(R,R);
            %norm_res2 = norm_res2 / norm_res2_0;
            if ( delta1 <= tol2 || and( true, abs(x_norm_old-x_norm)/x_norm < 1e-6 ) ) % || (x_diff>0.05 && iter>100) ) % || xnorm>x_limit )
                if (Parallel3D.MPI.rank==0 && ~silent)
                    fprintf('          %5d        residual: %e\n',iter,sqrt(delta1))
                end
                break;
            end
            if (Parallel3D.MPI.rank==0)                
                if (rem(iter,outputStep)==0 && ~silent) 
%                     fprintf('          %5d       residual: %e %e %e\n',iter,sqrt(norm_res2),sqrt(delta1),sqrtnorm_res2_0)
                    t2 = toc;
                    if (rem(iter,100)==0)
                        fprintf('iteration %d : %e (%f s) -- rate: %e -- xnorm: %e\n',iter,sqrt(delta1),t2-t1,abs(sqrt(delta1)-sqrt(delta_ref))/sqrt(delta1),x_diff)
                        delta_ref = delta1;
                    else
                        fprintf('iteration %d : %e (%f s) -- rate: %s -- xnorm: %e\n',iter,sqrt(delta1),t2-t1,'          ',xnorm)
                    end
                end
            end
            
            % Step 6 : Backup delta1 (ry) as delta0 (ry_old)
            delta0 = delta1;

            % Step 7: Compute new y and delta1 (ry)
            y = Minv.*R;
            delta1 = dotProduct(R,y,transfer);
            
            % Step 8: Compute beta
            beta   = delta1 / delta0;   % = ry / ry_old
            
            % Step 9 : Compute new p
            %p_old = p;
            p = y + beta*p;
            
            %fprintf('%6.4f %6.4f\n',alpha,beta)
            %if (iter==1)
            %    filename = ['iterstep_' num2str(rank) '.mat' ];
            %    save(filename,'w','Ke_all','GM_all','p','p_old','x','alpha','beta','delta1','delta0','Minv','R','y','pw');
            %end
        end
    end
end