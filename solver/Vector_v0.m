classdef Vector_v0 < handle
    %VECTOR A vector is a collection of alpha arrays of fields
    %   Detailed explanation goes here
    
    properties (Access={?Matrix_v0})
        fields;
        mesh;
        name;
        numFree;
        type;
        
        valueArray;
        valueCell;
                
        valueType;
    end
    
    methods
        function s = Vector_v0(name,mesh,type,varargin)
            
            %% Process input
            s.name          = name;     
            s.mesh          = mesh;
            s.type          = type;
            s.fields     	= varargin;
            
            %% Perform some sanity checks
            % Ensure type is a VectorType
            if ~isa(type,'VectorType')
                error('The type must be a valid VectorType')
            end
            
%             % Ensure all field.mesh are identical to mesh
%             for i=1:numel(s.fields)
%                 if ( s.mesh ~= s.fields{i}.mesh )
%                     error('The field meshes are not all identical to the base mesh')
%                 end
%             end
            
            switch type
                case VectorType.alpha
                    s.numFree = s.mesh.dh.numDof;

                    s.valueArray = zeros( s.numFree*numel(s.fields),1 );

                    % Map the elemental alpha to the alphaVector
                    for f=1:numel(s.fields)
                        for e=1:numel(s.fields{f}.alpha)
                            s.valueArray( s.mesh.dh.dofs{e}(:) + (f-1) * s.numFree )  = s.fields{f}.alpha{e}(:);
                        end
                    end
                    
                case VectorType.value
                    s.valueCell = varargin{:};                    
            end
        end
        
        function disp(s)
            fprintf('Vector %s with %d unkowns\n',s.name,s.numFree)
        end
        
%         function outputArg = method1(obj,inputArg)
%             %METHOD1 Summary of this method goes here
%             %   Detailed explanation goes here
%             outputArg = obj.Property1 + inputArg;
%         end
    end
end

