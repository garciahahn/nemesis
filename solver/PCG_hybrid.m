% Preconditioned CG solver, where each element is treated separately. The
% optional arguments are:
%   epsilon     : convergence criteria (default: 1e-10)
%   silent      : with / without output (default: false)
%   x_init      : initial solution (default: a zero vector)
%   tag         : (default: [])

function [x_out,info]=PCG_hybrid(Ke,Ge,dh,varargin)

    tStart = toc;
    
    % It is possible to merge all local (on the same processor) elemental
    % array into one big array, which might speed up the entire solver
    mergeLocally = false;
    
    if (mergeLocally)
        Ke_local = sparse(dh.numDof,dh.numDof);
        for i=1:numel(Ke)
            l = dh.dofs{i}(:);
            Ke_local(l,l) = Ke_local(l,l) + Ke{i};
        end
        
        % Ensure the matrix is lower triangular
        %Ke_local = full( triu(Ke_local,1)'+tril(Ke_local) );
        Ke_local = Ke_local' + Ke_local - diag(diag(Ke_local));
        
        Ge_local = sparse(dh.numDof,1);
        for i=1:numel(Ge)
            l = dh.dofs{i}(:);
            Ge_local(l) = Ge_local(l) + Ge{i};
        end
        
        %Ge_local = full( Ge_local );
    end
    
    % Extract Variables from the DoFHandler
    prescribed  = 0;
    GM          = dh.dofs;
    localDofs   = dh.localDofs;
    sharedGM    = dh.sharedDoFs;    
    numDof      = dh.numDof;
    numLocalDof = dh.numLocalDof;
    
    useMPI = (System.nproc>1);
    useMex = System.useMexSolver;
    
    diagonalPreconditioner = true;  % false == full inverse preconditioner, incomplete cholesky; true = diagonal (Jacobi) preconditioner
    
    optimizedResidual = false;
    
    % Process the optional arguments
    default = {1e-10,true,zeros(numLocalDof,1),[],[],[],[]};
    [epsilon,silent,x_init,tag,L,G,W] = VerifyInput( varargin, default );
        
    % Solver settings
    outputStep      = 1000;
    UPLO            = 'L';                      % is data stored in lower / upper format?
    
    % Convergence settings
    tol2            = epsilon*epsilon;
    maxIter         = System.settings.comp.CG.maxIter;    
    
    % Parallel settings
    uniqueSolving   = true;                     % flag to determine if each unknown is solved on only 1 processor (uniqueSolving==true), or on multiple and averaged afterwards
    rank            = NMPI.instance.rank;

    if (uniqueSolving)
        own = (dh.dofRanks==rank);
    else
        own = true( size(dh.dofRanks) );
    end

     
%     e_start     = NMPI.instance.eStart;
%     e_end       = NMPI.instance.eEnd;
    
    numLocalElements = numel(Ke);

    % flags
    prec        = 0;
    iter        = 0;
    
    %% Make sure prescribed is a cell array
    % Check if the passed 'prescribed' is a cell array, and if not make a
    % cell array of zeros. This is necessary to make the code below as
    % general as possible.
    if (~iscell(prescribed))
        prescribed = cell(numel(GM),1);
        for e=1:numel(GM)
            prescribed{e} = false( numel(GM{e}) , 1 );
        end
    end
    
%     %% Check if any zero columns/rows exist
    zeroColumn = cell( numel(Ke), 1);
    for e=1:numel(Ke)
        zeroColumn{e} = false( numel(Ke{e}) , 1 );
        if nnz( all(Ke{e}==0) )>0
            zeroColumn{e}( all(Ke{e}==0), 1 ) = true;
        end
    end
    
    %% Set the dofType flags for the passed GM
    % The 'dofType' contains a flag to indicate what kind of DOF it is, 
    % there are 3 possible types:
    %
    %   -1  the DOF is not on this rank 
    %    0  the DOF is on this rank and not prescribed (aka free DOF)
    %    1  the DOF is on this rank and prescribed
    %
    % Initially dofType==-1, and based on the passed 'GM' and
    % 'prescribed' cell arrays the corresponding dofType is set
    %
    dofType = -1 * ones(numDof,1);
    for e=1:numel(GM)
        if (dh.eRanks(e)==rank)
            dofType( GM{e}(:) ) = max( dofType( GM{e}(:) ), prescribed{e} );
        end
    end    

    debug = false;
    
    %% Free dofs : dofType==0
    %numFree             = nnz(dofType==0);
    numFree             = numel( dh.sharedDoFs{rank+1} );
    
    if (debug)
        save(['PCG_hybrid.' int2str(System.rank) '.mat']);
        fprintf('numFree on rank %d = %d\n',NMPI.instance.rank,numFree)
    end
    
    if (System.nproc>1)
        numFreeAll    	= NMPI.Allreduce(numFree,1,'+','numFreeAll'); %,NMPI.instance.mpi_comm_world,'numFreeAll',debug);
        if (debug)
            fprintf('numFreeAll : %d\n',numFreeAll)
        end
    else
        numFreeAll      = numFree;
    end

    %% Prescribed dofs : dofType==1
    t1=toc;
    numPrescribed       = nnz(dofType==1);
    if (System.nproc>1)
        numPrescribedAll	= NMPI.Allreduce(numPrescribed,1,'+','numPrescribedAll'); %,NMPI.instance.mpi_comm_world,'numPrescribedAll',debug);
    else
        numPrescribedAll    = numPrescribed;
    end
    t2=toc;
    
%     fprintf('timing 1 : %f\n',t2-t1)
    
    %% Renumbering of free DOFs
    % As the GM does contain prescribed DOFs, these should be removed. By
    % simply removing these, the numbering will be discontinuous.
    % Therefore, below a new GM is formed, which has a continuous numbering
    % for the free DOFs. First a GMmap is formed which maps the full
    % numbering to just free numbering
%     numFree = numFree - zeroColumn;
%     
%     GMmap               = zeros(numDof,1);
%     GMmap( dofType==0 ) = (1:numFree);
% 
%     for e=1:numLocalElements
%         if (iscell(prescribed))
%             GM{e} = GMmap( GM{e}(~prescribed{e}) );
%         else
%             GM{e} = GMmap( GM{e} );
%         end
%     end    

    %% Shared DOFs
    % Determine the shared DOFs (free and prescribed). The 'sharedGM{i}'
    % contains the DOF numbers on this rank which are shared with proc i.
    % By definition 'dofType(sharedGM{i})' should only return 0 and 1
    % values, as the sharedGM contains only local DOFs. 
    numSharedFree       = 0;
    numSharedPrescribed = 0;
%     transfer            = cell(NMPI.instance.nproc,1);
    
    
    
%     for i=1:NMPI.instance.nproc
%         if (i~=(NMPI.instance.rank+1))
%             transfer{i}         = GMmap( sharedGM{i}( dofType(sharedGM{i})==0 ) );                                  % NB: sharedGM numbering includes prescribed DOFs, therefore GMmap is used
%             
%             %localFree = ( dh.dofRanks( dofType==0 ) == i );
%             
%             numSharedFree       = numSharedFree       + nnz( dofType(sharedGM{i})==0 );                             % NB: nnz( dofType(sharedGM{i})==0 ) == numel(transfer{i})
%             numSharedPrescribed = numSharedPrescribed + nnz( dofType(sharedGM{i})==1 );
%         end
%     end
        
    % After changing the mesh construction and creating the DoFHandler the
    % transfer changed, this function just creates a similar layout as
    % before
    if (uniqueSolving)
        transfer    = dh.sharedDoFs;
        %oldTransfer	= convertTransfer(transfer);
    else
        newTransfer = dh.sharedDoFs;
        transfer    = convertTransfer(newTransfer);
    end
    
    %save(['transfer.' int2str(System.rank) '.mat']);
    
    numSharedFreeAll        = NMPI.Allreduce(numSharedFree,1,'+','numSharedFreeAll') / 2;          %,NMPI.instance.mpi_comm_world,'numSharedFreeAll',debug) / 2;                % divide by 2 because of double counting 
    numSharedPrescribedAll  = NMPI.Allreduce(numSharedPrescribed,1,'+','numSharedPrescribedAll') / 2;    %,NMPI.instance.mpi_comm_world,'numSharedPrescribedAll',debug) / 2;          % divide by 2 because of double counting 
    
    %numSharedFreeAll        = NMPI_Allreduce(numSharedFree,1,'+',NMPI.instance.mpi_comm_world,'numSharedFreeAll',debug) / 2;                % divide by 2 because of double counting 
    %numSharedPrescribedAll  = NMPI_Allreduce(numSharedPrescribed,1,'+',NMPI.instance.mpi_comm_world,'numSharedPrescribedAll',debug) / 2;          % divide by 2 because of double counting 

    numSharedAll            = numSharedFreeAll+numSharedPrescribedAll;
        
    if ( numDof ~= ( numFreeAll-numSharedFreeAll + numPrescribedAll-numSharedPrescribedAll ) )
        %error('The passed globalDof (%d) is not equal to the computed globalDof : free (%d) + prescribed (%d) - shared (%d)\n',globalDof,numFreeAll,numPrescribedAll,numSharedAll)
        numDof = numFreeAll-numSharedFreeAll + numPrescribedAll-numSharedPrescribedAll;
    end
    
    if (rank==0 && ~silent && System.settings.outp.debug) 
        fprintf('DOFs          | global (unique/shared) : %d (%d/%d) == %d (%d/%d) free + %d (%d/%d) prescribed\n', ...
            numDof+numSharedAll,numDof,numSharedAll,...
            numFreeAll,numFreeAll-numSharedFreeAll,numSharedFreeAll,...
            numPrescribedAll,numPrescribedAll-numSharedPrescribedAll,numSharedPrescribedAll);
    end
    
    %save(['cgsolver.' int2str(System.rank) '.mat']);
    
    %% Ensure no sparse arrays are used
    GM = cell(1,numLocalElements);
    try
        for e=1:numLocalElements
            GM{e} = localDofs{e}(:);
            if (useMex)
                Ke{e} = full(Ke{e});
                Ge{e} = full(Ge{e});
            else
                Ke{e} = sparse(Ke{e});
                Ge{e} = sparse(Ge{e});
            end
        end
    catch MExp
        save(['parallel_solver_' num2str(NMPI.instance.rank) '.mat'])
    end
    
    %% Check if each element is of the same size
    sizeKe = 0;
    if (~isempty(Ke))
        sizeKe = size(Ke{1});    
    end
    
    constantSize = true;
    for i=1:numLocalElements
        if (size(Ke{i}) ~= sizeKe)
            constantSize = false;
            break;
        end
    end
    
    %% Set the initial solution x (consisting of free DOFs)
    isPrescribedLocal = logical( dofType( dofType>=0 ) );
    x = x_init(~isPrescribedLocal);
    
    if (uniqueSolving)
        x = syncSolution(x,transfer);
    else
        x = averageElementBoundaries(x,transfer);
    end
    
    %save(['parallel_solver_' num2str(NMPI.instance.rank) '.mat'])
    %fprintf('parallel_solver written on rank %d\n',System.rank)
    
    if (constantSize)  % weak
        matvecPerElement = false;

        if (~optimizedResidual)
            
            if (useMex)
                Ke_all = full([Ke{1:numLocalElements}]);
                GM_all = int32([GM{1:numLocalElements}]);
            end

            %x = x_init;
            if (numLocalElements>0)
                if (useMex)
                    Ax_local = matvec_all(UPLO,Ke_all,GM_all,x,transfer,uniqueSolving);       % U=0 at the initial, originally, R = b-Ax = F - K*U
                else
                    Ax_local = loc2gbl_matrix_vector(Ke, x, GM, numel(x));
                end
            else
                Ax_local = [];
            end
            Ax = Ax_local;
        else
            Ax = cell(numLocalElements,1);
            %Ax_merge = zeros( numel(x), numLocalElements+1 );
            for i=1:numLocalElements
                Ke_element = Ke{i} + Ke{i}' - diag(diag(Ke{i}));
                Ax{i} = Ke_element * x(GM{i});
                %Ax_merge( GM{i},i ) = Ax_elemental{i};
            end
            %Ax_merge(:,5) = sum(Ax_merge(:,1:4),2);
            %Ax_merge(:,6) = Ax;
        end
        
    else
        matvecPerElement = true;
        if (numLocalElements>0)
            Ax_local = matvec_one(UPLO,numLocalElements,Ke,x,GM,transfer,uniqueSolving);
        else
            Ax_local = [];
        end
        Ax = Ax_local;
    end
        
    %% Compute residual R, delta0 (ry_old)
    if (diagonalPreconditioner)
        M = zeros(numFree,1);          % extract only diagonal components of Ke and assembly them
    else
        Minv = cell(numLocalElements,1);
    end
    
    if (~optimizedResidual)
        R    = zeros(numFree,1) ;

        for e=1:numLocalElements                                      % length(GM): number of elements
            if ( diagonalPreconditioner )
                for ind=1:numel(GM{e})                                  % dofL: Nvar*number of points in one element
                    M(GM{e}(ind),1) = M(GM{e}(ind),1) + Ke{e}(ind,ind);
                end 
                %M( GM{e}(:),1 ) = M( GM{e}(:),1 ) + diag( Ke{e} );
            else
                %t = Ke{e} + tril(Ke{e})' - diag(diag(Ke{e}));
                %Minv{e} = tril( inv( t ) );
                tolerance = 1e-10;
                t = Ke{e} + tril(Ke{e})' - diag(diag(Ke{e}));
                L = chol(t,'lower');
                Minv{e} = inv( L );
            end
            R(GM{e}) = R(GM{e}) + Ge{e};
        end
        
    else
        % Option 1
%         for e=1:numel(GM)                                  % dofL: Nvar*number of points in one element
%             M = zeros(numFree,1); 
%             ind = GM{e};
%             for i=1:numel(ind)
%                 M(ind(i),1) = M(ind(i),1) + Ke{e}(i,i);
%             end
%         end

        % Option 2
        M = cell(numLocalElements,1); 
        for e=1:numLocalElements
            M{e} = diag(Ke{e});
        end
        
        R = Ge';
    end
    
    %save(['parallel_solver2_' num2str(NMPI.instance.rank) '.mat'])
    %fprintf('parallel_solver2 written on rank %d\n',System.rank)
    
    % The M and R values on processor boundaries have to be exchanged. The
    % addElementBoundaries performs a similar operation as NMPI_Reduce, but
    % only on the shared DOFs
    if (uniqueSolving)
        if (diagonalPreconditioner)
            M = syncBoundaries(M,transfer);
            %M = syncSolution(M,transfer); 
        end
        R = syncBoundaries(R,transfer);  
        %R = syncSolution(R,transfer); 
    else
        if (diagonalPreconditioner)
            M = addElementBoundaries(M,transfer);
        end
        R = addElementBoundaries(R,transfer);
    end
    
    %save(['parallel_solver3_' num2str(NMPI.instance.rank) '.mat'])
    
%     if ( ~all(Rold(own)==R(own)) )
%         save(['Rold.' int2str(System.rank) '.mat']);
%         error('Rold and R are different')
%         exit
%     else
%         fprintf('Rold and R are identical on rank %d\n',rank)
%     end
    
    %save(['MRnew' num2str(rank) '.mat'])
    
    
    
    
    
    
    % Compute the residual, where it should be noted that the shared 
    % (boundary) values of Ax have been exchanged already with neighboring
    % processors at the end of the matvec operation!
    if (~optimizedResidual)
        R = R - Ax; 
    else
        R = cellfun( @(x,y) x-y,R,Ax,'Uni',false);
        
%         R_global = zeros( numel(x), 1);
%         for e=1:numLocalElements
%             R_global( GM{e} ) = R_global( GM{e} ) + R{e};
%         end
%         for e=1:numLocalElements
%             R{e} = R_global( GM{e} );
%         end
    end
    
    if (diagonalPreconditioner)
        if (~optimizedResidual)
            Minv = (1./M);
            y    = Minv.*R;
        else

%             invM = 1./M;
%             Minv = cellfun(@(x) diag(invM(x)), GM,'Uni',false);
            
            for e=1:numLocalElements
                Minv{e} = diag(1./M{e});
            end

%             y    = cellfun(@(x,y) x.*y,Minv,R,'Uni',false);
            y = matvec_one_cell(UPLO,numLocalElements,Minv,R,GM,transfer,uniqueSolving);
        end
    else
        Minv_all = full([Minv{1:numLocalElements}]);
        y        = matvec_all(UPLO,Minv_all,GM_all,R,transfer,uniqueSolving);
    end
    
    %save(['parallel_solver4_' num2str(NMPI.instance.rank) '.mat'])
    
    p           = y;
    if (~optimizedResidual)
        delta1  = dotProduct(R(own),y(own),useMPI); %,transfer);  % delta0      = R'*y;
    else
%         delta1  = cellfun(@(x,y) dotProduct(x,y),R,y,'Uni',false);
%         delta1  = sum([delta1{:}]);
        delta1 = dotProduct_cell(R,y,useMPI);
    end
    beta        = 0;
    alpha       = 1;
    
    t1 = toc;
    
    if (rank==0 && ~silent && System.settings.outp.debug)
        fprintf('iteration %4d : %e (%f s)\n',0,sqrt(delta1),0)
    end
    
    x_start = x;
    x_limit = 10000;
    delta_ref = 0;
	x_norm  = 10000;
    
    outputProgress = false;
%     if (~silent)
%         outputProgress = true;
%     end
    
    if (outputProgress)
        header = cell(7,1);
        data   = zeros(7,maxIter);
    
        if (rank==0 && ~isempty(tag))
            filename = [System.settings.outp.directory 'cg-' tag '.txt'];
            header = {'iter','alpha','beta','delta1','x_diff','x_norm','deltaconv'};
            System.writeHeader(filename,header);
        end
    end
    
    %save(['iterative.' int2str(System.rank) '.mat']);
    
    %fprintf('delta on on rank %d : %f\n',rank,delta1)
    
    info.initRes = sqrt(delta1);
    
    time1 = toc;
    
%     timings = zeros(10,1);

    if (~mergeLocally)
        if (delta1 ~=0) %> tol2)
        for iter=1:maxIter

            %fprintf('iter %d -- delta on on rank %d : %f\n',iter,rank,delta1)
            
%             t0 = toc;
            
            % Step 1: Compute w and pw
            if (numLocalElements>0)
                if (matvecPerElement)
                    if (rank==0 && iter==1) 
                        fprintf('Using matvec_one in PCG_hybrid, this is not efficient')
                    end
                    w = matvec_one(UPLO,numLocalElements,Ke,p,GM,transfer,uniqueSolving);
                else
                    if (~optimizedResidual)
                        if (useMex)
                            w = matvec_all(UPLO,Ke_all,GM_all,p,transfer,uniqueSolving);
                        else
                            if (rank==0 && iter==1) 
                                fprintf('Using loc2gbl_matrix_vector in PCG_hybrid, this is not efficient')
                            end
                            w = loc2gbl_matrix_vector(Ke, p, GM, numel(p));
                        end
                    else
                        if (rank==0 && iter==1) 
                            fprintf('Using matvec_one_cell in PCG_hybrid, this is not efficient')
                        end
                            
                        %p = syncSolution(p,transfer);
                        p_global = zeros( numel(x), 1);
                        for e=1:numLocalElements
                            p_global( GM{e} ) = p_global( GM{e} ) + p{e};
                        end
                        for e=1:numLocalElements
                            p{e} = p_global( GM{e} );
                        end
                        w = matvec_one_cell(UPLO,numLocalElements,Ke,p,GM,transfer,uniqueSolving);
                    end
                end
            else
                w=0;
            end
            
%             t1 = toc;
%             n = 1;
%             timings(n) = timings(n) + t1-t0;
            
            if (~optimizedResidual)
                pw = dotProduct(p(own),w(own),useMPI); %,transfer);
            else
                pw = dotProduct_cell(p,w);
            end

%             t2 = toc;
%             n = n+1;
%             timings(n) = timings(n) + t2-t1;
            
            % Step 2: Compute alpha
            alpha = delta1 / pw;      % delta1 = ry

%             t3 = toc;
%             n = n+1;
%             timings(n) = timings(n) + t3-t2;
            
            % Step 3: Compute new x
            if (~optimizedResidual)
                x(own) = x(own) + alpha*p(own);
            else
                for e=1:numLocalElements
                    %continuous_p{e} = p_global( GM{e} ) ./ p_count( GM{e} );
                    %x(GM{e}) = x(GM{e}) + alpha*continuous_p{e}(:);
                    
%                     x(GM{e}) = x(GM{e}) + alpha*p_global( GM{e} );
                    
                    x(GM{e}) = x(GM{e}) + alpha*p{e}(:);
                end
            end
            
%             x_norm_old = x_norm;
%             x_norm = norm(x-x_start);
%             
%             %fprintf('in loop on rank %d\n',rank)
%             
%             x_norm = NMPI_Allreduce(x_norm,1,'+',NMPI.instance.mpi_comm_world,'x_norm',debug) ;
            
%             t4 = toc;
%             n = n+1;
%             timings(n) = timings(n) + t4-t3;

            % Step 4: Compute new R
            if (~optimizedResidual)
                R(own) = R(own) - alpha*w(own);
            else
                for e=1:numLocalElements
                    R{e} = R{e} - alpha*w{e}(:);
                end
                
                %R_global = zeros( numel(x), 1);
                %for e=1:numLocalElements
                %    R_global( GM{e} ) = R_global( GM{e} ) + R{e};
                %end
                %for e=1:numLocalElements
                %    R{e} = R_global( GM{e} );
                %end
            end
            
            % The code below should sync the residual between elements, to
            % achieve a continuous residual. It is however not working.
            %% BEGIN OF CODE
            %             if (false && ~isempty(L))
            %                 globalResidual = zeros(dh.numqDof,1);
            %                 res_tmp = cell(numLocalElements,1);
            %                 res_loc = cell(numLocalElements,1);
            %                 for e=1:numLocalElements
            %                     res_tmp{e} = abs( G{e} - L{e} * x( GM{e} ) );
            %                     
            %                     globalResidual( dh.qdofs{e}(:) ) = globalResidual( dh.qdofs{e}(:) ) + res_tmp{e};
            %                 end
            %                 
            %                 for e=1:numLocalElements
            %                     %res_loc{e} = globalResidual( dh.qdofs{e} );
            %                     res_loc{e} = res_tmp{e} - globalResidual( dh.qdofs{e}(:) );
            %                 end
            %                 
            %                 R2 = zeros( size(R) );
            %                 for e=1:numLocalElements
            %                     R2( GM{e} ) = R2( GM{e} ) + L{e}' * ( W{e} .* res_loc{e}(:) );
            %                 end
            %                 
            %                 %R = R + R2;
            %                 %R = R2;
            %             end
            %% END OF CODE
            
%             if (outputProgress)
%                 x_diff = max(abs(1-x./x_start));
%                 x_diff = NMPI_Allreduce(x_diff,1,'M',NMPI.instance.mpi_comm_world,'x_diff',debug) ; 
%             
%                 deltaconv = abs((delta1)-(delta_ref))/(delta1);
% 
%                 if (rem(iter,25)==0)
%                     delta_ref = delta1;
%                 end
%             
%                 data(1,iter) = iter;
%                 data(2,iter) = alpha;
%                 data(3,iter) = beta;
%                 data(4,iter) = delta1;
%                 data(5,iter) = x_diff;
%                 data(6,iter) = x_norm; 
%                 data(7,iter) = deltaconv;
%             end
            
%             t5 = toc;
%             n = n+1;
%             timings(n) = timings(n) + t5-t4;

            % Step 5: Check for convergence & output information
            %norm_res2 = dotProduct(R,R);
            %norm_res2 = norm_res2 / norm_res2_0;
            if ( delta1 <= tol2 || beta == 1 || alpha < 1e-20 ) %|| and( true, abs(x_norm_old-x_norm)/x_norm < 1e-6 ) ) % || (x_diff>0.05 && iter>100) ) % || xnorm>x_limit )
                if (NMPI.instance.rank==0 && ~silent)
                    fprintf('          %5d        residual: %e\n',iter,sqrt(delta1))
                end
                break;
            end
            if (NMPI.instance.rank==0)                
                if (rem(iter,outputStep)==0 && ~silent) 
%                     fprintf('          %5d       residual: %e %e %e\n',iter,sqrt(norm_res2),sqrt(delta1),sqrtnorm_res2_0)
                    t2 = toc;
%                     if (rem(iter,1)==0)
%                         fprintf('iteration %d : %e (%f s) -- rate: %e -- xnorm: %e\n',iter,sqrt(delta1),t2-t1,abs(sqrt(delta1)-sqrt(delta_ref))/sqrt(delta1),x_diff)
%                         delta_ref = delta1;
%                     else
                        fprintf('iteration %d : %e (%f s) -- rate: %s -- xnorm: %e\n',iter,sqrt(delta1),t2-t1,'          ',x_norm)
%                     end
                end
                t2 = toc;
            end
            
%             t6 = toc;
%             n = n+1;
%             timings(n) = timings(n) + t6-t5;
            
            % Step 6 : Backup delta1 (ry) as delta0 (ry_old)
            delta0 = delta1;
            
%             t7 = toc;
%             n = n+1;
%             timings(n) = timings(n) + t7-t6;

            % Step 7: Compute new y and delta1 (ry)
            if (diagonalPreconditioner)
                if (~optimizedResidual)
                    y(own) = Minv(own).*R(own);
                else
                    y = matvec_one_cell(UPLO,numLocalElements,Minv,R,GM,transfer,uniqueSolving);
                end
            else
                y(own) = matvec_all(UPLO,Minv_all,GM_all,R(own),transfer,uniqueSolving);
            end
            
            if (~optimizedResidual)
                delta1 = dotProduct(R(own),y(own),useMPI); %,transfer);
            else
                delta1 = dotProduct_cell(R,y);
            end
            
%             t8 = toc;
%             n = n+1;
%             timings(n) = timings(n) + t8-t7;
            
            % Step 8: Compute beta
            beta   = delta1 / delta0;   % = ry / ry_old
            
%             t9 = toc;
%             n = n+1;
%             timings(n) = timings(n) + t9-t8;
            
            % Step 9 : Compute new p
            %p_old = p;
            if (~optimizedResidual)
                p(own) = y(own) + beta*p(own);
            else
                for e=1:numLocalElements
                    p{e} = y{e} + beta*p{e}(:);
                end
            end
            
%             t10 = toc;
%             n = n+1;
%             timings(n) = timings(n) + t10-t9;
        end
        end
    
    else
        %% Use the mergeLocal version
        if (delta1 ~=0) %> tol2)
            for iter=1:maxIter
                % Step 1: Compute w and pw
                %w = dsymvL(Ke_local,p);
                w = Ke_local * p;
                pw = dot(p,w);      % NB: needs sync for MPI

                % Step 2: Compute alpha
                alpha = delta1 / pw;      % delta1 = ry

                % Step 3: Compute new x
                x = x + alpha * p;

                % Step 4: Compute new R
                R = R - alpha*w;

                % Step 5: Check for convergence & output information

                %norm_res2 = dotProduct(R,R);
                %norm_res2 = norm_res2 / norm_res2_0;
                if ( delta1 <= tol2 || beta == 1 || alpha < 1e-20 ) %|| and( true, abs(x_norm_old-x_norm)/x_norm < 1e-6 ) ) % || (x_diff>0.05 && iter>100) ) % || xnorm>x_limit )
                    if (NMPI.instance.rank==0 && ~silent)
                        fprintf('          %5d        residual: %e\n',iter,sqrt(delta1))
                    end
                    break;
                end
                if (NMPI.instance.rank==0)                
                    if (rem(iter,outputStep)==0 && ~silent) 
    %                     fprintf('          %5d       residual: %e %e %e\n',iter,sqrt(norm_res2),sqrt(delta1),sqrtnorm_res2_0)
                        t2 = toc;
    %                     if (rem(iter,1)==0)
    %                         fprintf('iteration %d : %e (%f s) -- rate: %e -- xnorm: %e\n',iter,sqrt(delta1),t2-t1,abs(sqrt(delta1)-sqrt(delta_ref))/sqrt(delta1),x_diff)
    %                         delta_ref = delta1;
    %                     else
                            fprintf('iteration %d : %e (%f s) -- rate: %s -- xnorm: %e\n',iter,sqrt(delta1),t2-t1,'          ',x_norm)
    %                     end
                    end
                    t2 = toc;
                end

                % Step 6 : Backup delta1 (ry) as delta0 (ry_old)
                delta0 = delta1;

                % Step 7: Compute new y and delta1 (ry)
                if (diagonalPreconditioner)
    %                 if (~optimizedResidual)
    %                     y(own) = Minv(own).*R(own);
    %                 else
    %                     y = matvec_one_cell(UPLO,numLocalElements,Minv,R,GM,transfer,uniqueSolving);
    %                 end
                    y = Minv .* R;
                else
                    %y(own) = matvec_all(UPLO,Minv_all,GM_all,R(own),transfer,uniqueSolving);
                    error('problem in the cg solver')
                end

                delta1 = dot(R,y);

                % Step 8: Compute beta
                beta   = delta1 / delta0;   % = ry / ry_old

                % Step 9 : Compute new p
                p = y + beta * p;
            end
        end
    
    end
    
%     timings_all = NMPI.Allreduce(timings,numel(timings),'+');
%     if ( rank==0 )
%         for i=1:numel(timings)
%             fprintf('All timings (%d): %f\n',i,timings_all(i));
%         end
%     end
    
    time2 = toc;
    
%     fprintf('Pure CG solver time : %f\n',time2-time1);
    
    info.finalRes = sqrt(delta1);
    info.iter     = iter;
    
    if (rank==0 && ~isempty(tag) && outputProgress)
        System.appendToFile(filename,data(:,1:iter));
    end
    
    %fprintf('Rank %d is here (delta1 = %e)\n',NMPI.instance.rank,delta1)
    
    %xtmp = x;
    %x = averageElementBoundaries(xtmp,transfer);
    %fprintf('There are %d changes in x after syncElementBoundaries\n',nnz(~(x==xtmp)));

    x = syncSolution(x,transfer);
    
    x_out = zeros(numLocalDof,1);
    x_out(isPrescribedLocal)  = x_init(isPrescribedLocal);    
    x_out(~isPrescribedLocal) = x;
    
%     x = (rank+1)+0*x;
%     x_out(~isPrescribedLocal) = setElementBoundaries(x,transfer);    
%     x_out(isPrescribedLocal) = 0;
        
    tEnd = toc;
    
    if (rank==0 && ~silent && System.settings.outp.debug) 
        fprintf('Solver wall clock time (%d iterations) : %f ---- final residual : %e\n',iter,tEnd-tStart,sqrt(delta1))
    end    
end

% 
% function R=loc2gbl_matrix_vector(e_start,e_end,Ke, D, GM, dof)
%     R = zeros(dof,1) ;
% 
%     %JF Parallelization**************************************************
%     for e=e_start:e_end
%         I = GM{e};
%         Ge=D(I);
%         C = Ke{e-e_start+1}*Ge ;
%         R(I) = R(I) + C ;
%     end 
% end
% 
function [Ke,Ge,GM_out] = sortArrays(Ke,Ge,GM,dof)
    GM_out = cell(length(Ke),1);
    
    % Make Ke full again
    %% Upper format
%     for i=1:length(Ke)
%         Ke{i} = triu(Ke{i},1)'+Ke{i};
%         [~,idx]=sort(GM{i});
%         Ke{i} = Ke{i}(idx,idx);     % shuffle columns of Ke in sorted order
%         Ke{i} = triu(Ke{i});
%         Ge{i} = Ge{i}(idx);
%         GM_out{i} = sparse(false(dof,1));
%         GM_out{i}(GM{i}(:)) = true;
%     end
    
    %% Lower format
    for i=1:length(Ke)
        Ke{i} = tril(Ke{i},1)'+Ke{i};
        [~,idx]=sort(GM{i});
        Ke{i} = Ke{i}(idx,idx);     % shuffle columns of Ke in sorted order
        Ke{i} = tril(Ke{i});
        Ge{i} = Ge{i}(idx);
        GM_out{i} = sparse(false(dof,1));
        GM_out{i}(GM{i}(:)) = true;
    end
end

function R = matvec_one(ul,numLocalElements,Ke, D, GM, transfer, uniqueSolving)
    R = zeros(length(D),1) ;
    
%     D = averageElementBoundaries(D,transfer);

    %D = syncSolution(D,transfer);
    
    switch ul
        case 'U'
            for e=1:numLocalElements
                R(GM{e}) = R(GM{e}) + dsymv( Ke{e} , D(GM{e}) );
            end
        case 'L'
            for e=1:numLocalElements
                R(GM{e}) = R(GM{e}) + dsymvL( Ke{e} , D(GM{e}) );
            end   
    end
    
    if (uniqueSolving)
        R = syncBoundaries(R,transfer);
        %R = syncSolution(R,transfer); 
    else
        R = addElementBoundaries(R,transfer);
    end
end

function R = matvec_one_cell(ul,numLocalElements,Ke, D, GM, transfer, uniqueSolving)
    R = cell(numLocalElements,1);
    
    switch ul
        case 'U'
            for e=1:numLocalElements
                R{e} = dsymv( Ke{e} , D{e}(:) );
            end
        case 'L'
            for e=1:numLocalElements
                R{e} = dsymvL( Ke{e} , D{e}(:) );
            end   
    end
    
    if (uniqueSolving)
        R = syncBoundaries(R,transfer);
        %R = syncSolution(R,transfer); 
    else
        R = addElementBoundaries(R,transfer);
    end
end

% Compute R = Ke * p, where GM are the locations of the DOFs in p
function R = matvec_all(ul,Ke,GM,p,transfer, uniqueSolving)
    
%     t1 = toc;

    if (uniqueSolving)
        %disp('p solution sync')
        p = syncSolution(p,transfer);
    else
        %p = averageElementBoundaries(p,transfer);
    end    
    
%     t2 = toc;
    
    %fprintf('dsymav start on rank %d\n',System.rank)
    
    switch ul
        case 'U'
            R = dsymvav( GM, Ke, p); % Ke in upper format
        case 'L'
            R = dsymvavL( GM, Ke, p); % Ke in lower format
    end
    
%     t3 = toc;
    
    %fprintf('dsymav end on rank %d\n',System.rank)
    
    if (uniqueSolving)
        %fprintf('R sync start on rank %d\n',System.rank)
        %save(['beforeR.' int2str(System.rank) '.mat']);
        R = syncBoundaries(R,transfer);
        %R = syncSolution(R,transfer); 
        %fprintf('R sync end on rank %d\n',System.rank)
    else
        R = addElementBoundaries(R,transfer);
    end
    
%     t4 = toc;
%     fprintf('matvec_all : %f %f %f\n',t2-t1,t3-t2,t4-t3)
end

% function R = matvec_all_logical(Ke,GM,p)
%     R = dsymvav_logical( GM, Ke, p); 
% end
% 
% function b = matvec_element(A,x,numLocalElements,transfer)
%     x = syncElementBoundaries(x,numLocalElements,transfer);
%     b = cell(numLocalElements,1);
%     for e=1:numLocalElements
%         b{e} = dsymvavLelement(A{e},x{e});
%     end
% end

function c = dotProduct(a,b,useMPI)

%     if (isempty(a) || isempty(b))
%         fprintf('a or b is empty on rank %d\n',System.rank);
%     end

    try
%         b_tmp = b;
%         %for i=1:NMPI.instance.rank
%             b_tmp( transfer{NMPI.instance.rank+1}~=NMPI.instance.rank ) = 0;
%         %end
%         
%         c = dot(a,b_tmp);
        c = a'*b;
    catch
        disp('WARNING: USING THE CATCH IN dotProduct')
        if (isempty(a) || isempty(b))
            fprintf('a or b is empty on rank %d\n',System.rank);
            c = 0;
        else
            c = dot(vertcat(a{:}),vertcat(b{:}));           % if (iscell(a) && iscell(b))
        end
    end
    
    if ( useMPI )
%         c = NMPI.Allreduce(c,1,'+','dotProduct'); %,NMPI.instance.mpi_comm_world); %,'dotProduct',1) ; 
        c = NMPI.Allreduce(c,1,'+'); %,NMPI.instance.mpi_comm_world); %,'dotProduct',1) ; 
    end
end

function c = dotProduct_cell(a,b,transfer)

    c = 0;
    for e=1:numel(a)
        c = c + a{e}'*b{e};
    end
    
    if ( System.nproc > 1 )
        c = NMPI.Allreduce(c,1,'+','dotProduct_cell'); %,NMPI.instance.mpi_comm_world); %,'dotProduct',1) ; 
    end
end

function x_out = swapElementBoundaries(x,transfer)
    x_out = syncElementBoundaries(x,transfer,0,1,4);
end

function x_out = setElementBoundaries(x,transfer)
    x_out = x;
    for i=1:NMPI.instance.nproc
        if (i~=NMPI.instance.rank+1)
            x_out(transfer{i}) = (i+NMPI.instance.rank+1)*0.5;
        end
    end
end

function x_out = averageElementBoundaries(x,transfer)
    x_out = syncElementBoundaries(x,transfer,0.5,0.5,4);
end

function x_out = addElementBoundaries(x,transfer)
    x_out = syncElementBoundaries(x,transfer,1,1,4);
end

function x_out = moveElementBoundaries(x,transfer)
    x_out = syncElementBoundaries(x,transfer,0,1,1);
end

% General function to synchronize boundary values between processors. The 
% alpha values determine the way values are synchronized:
%
%   alpha1 =  0  & alpha2 =  1      : swap values between processors
%   alpha1 =  1  & alpha2 =  0      : nothing happens, no values are copied between processors
%   alpha1 = 0.5 & alpha2 = 0.5     : values are averaged between processors
%   alpha1 =  1  & alpha2 =  1      : values are added to local values
%
function x_out = syncElementBoundaries(x,transfer,alpha1,alpha2,direction)    
    
    rank = NMPI.instance.rank;
    
    x_in  = x;    
    x_out = x;
   
    if (alpha2~=0)
        
        %disp('in syncElementBoundaries')
        %save(['syncElementBoundaries.' int2str(System.rank) '.mat']);
        
        if (direction==1 || direction==4)
            % RECV FROM LOWER RANKS, SEND TO HIGHER RANKS
            for i=NMPI.instance.nproc:-1:1
                if (i<rank+1)
                    count  = numel(transfer{i});
                    if (count>0)
                        source = i-1;
                        %fprintf('recv on rank %d from rank %d (%d values)\n',rank,source,count)
                        x_tmp = NMPI.Recv( count, source );
                        x_out(transfer{i}) = alpha1*x_out(transfer{i}) + alpha2*x_tmp;
                    end
                elseif (i>rank+1)
                    count  = numel(transfer{i});
                    if (count>0)
                        data   = x_in(transfer{i});
                        dest   = i-1;
                        %fprintf('send on rank %d to rank %d (%d values)\n',rank,dest,count)
                        NMPI.Send( data, count, dest );
                        
%                             if (rank==0)
%                                 data
%                             end
                        
                    end
                end
            end
        end

        if (direction==2 || direction==4)
            % RECV FROM HIGHER RANKS, SEND TO LOWER RANKS
            for i=1:1:NMPI.instance.nproc
                if (i<rank+1)
                    count  = numel(transfer{i});
                    if (count>0)
                        data   = x_in(transfer{i});                        
                        dest   = i-1;
                        %fprintf('send on rank %d to rank %d (%d values)\n',rank,dest,count)
                        NMPI.Send( data, count, dest );
                    end
                elseif (i>rank+1)
                    count  = numel(transfer{i});
                    if (count>0)
                        source = i-1;
                        %fprintf('recv on rank %d from rank %d (%d values)\n',rank,source,count)
                        x_tmp = NMPI.Recv( count, source );
                        x_out(transfer{i}) = alpha1*x_out(transfer{i}) + alpha2*x_tmp;
                    end
                end
            end
        end
        
    end
end

function x_out = syncBoundaries(x,transfer)
    if (NMPI.instance.nproc>1)
        x_out = syncData(x,transfer,1);
    else
        x_out = x;
    end
end

function x_out = syncSolution(x,transfer)
    if (NMPI.instance.nproc>1)
        x_out = syncData(x,transfer,0);
    else
        x_out = x;
    end
end


% General function to synchronize data. It accepts two versions:
%       version 0: exchange only values that are not owned by this rank
%       version 1: exchange all values that are shared with other ranks
%
function x_out = syncData(x,transfer,version)        
    x_in    = x;
    x_out   = x;
    
    rank = NMPI.instance.rank;
        
    switch version
        case 0
            % overwrite local value by remote values
            m1 = 0; m2 = 1;
        case 1
            % keep local values and add remote values
            m1 = 1; m2 = 1;
        otherwise
            error('Only version values 0 and 1 are accepted')
    end
    
    %disp('in syncElementBoundaries')
    %save(['syncElementBoundaries.' int2str(System.rank) '.mat']);

%     try
    
    % RECV FROM LOWER RANKS, SEND TO HIGHER RANKS
    for i=NMPI.instance.nproc:-1:1
        
        partnerRank = i-1;
        
        if ( partnerRank < rank )
            
            % In general only DOF values which are owned by other ranks
            % should be received
            transferFlags = ( transfer{rank+1}==partnerRank );
            
            % If values must be summed, also select to recv the values
            % owned by the current rank
            if version==1
                transferFlags( transfer{rank+1}==rank ) = transfer{partnerRank+1}; 
            end            
            
            % Determine the amount of values to recv
            numRecv = nnz( transferFlags ); 
            
            if (numRecv>0)
                %fprintf('recv1 on rank %d from rank %d (%d values)\n',rank,partnerRank,numRecv)
                data_in = NMPI.Recv( numRecv, partnerRank );                
                x_out( transferFlags ) = m1 * x_out( transferFlags ) + m2 * data_in;
            end
            
        elseif ( partnerRank > rank )
            
            % In general only DOF values which are owned by this rank
            % should be send
            transferFlags = false( size(transfer{rank+1}) );
            transferFlags( transfer{rank+1}==rank ) = transfer{partnerRank+1};
            
            % If values must be summed, also select to send the values
            % owned by the partnerRank
            if version==1
                transferFlags( transfer{rank+1}==partnerRank ) = true;
            end
%                 transferFlags = false( size(transfer{rank+1}) );
%                 if ( nnz( transfer{partnerRank+1} )>0 )
%                     transferFlags( transfer{rank+1}==rank ) = transfer{partnerRank+1};
%                 end
%             end
            numSend = nnz( transferFlags );
            
%             if ( nnz( transfer{partnerRank+1} )>0 )
%                 transferFlags( transfer{rank+1}==rank ) = transfer{partnerRank+1};
%                 numSend = nnz( transferFlags );
%             else
%                 numSend = 0;
%             end
            
            if (numSend>0)
                data_out    = x_in( transferFlags );                   % extract the data to be send
                %fprintf('send1 on rank %d to rank %d (%d values)\n',rank,partnerRank,numSend)
                NMPI.Send( data_out, numSend, partnerRank );
            end            
        end
    end
            
    % RECV FROM HIGHER RANKS, SEND TO LOWER RANKS
    for i=1:1:NMPI.instance.nproc
        
        partnerRank = i-1;
        
        if (partnerRank < rank)
            
            % In general only DOF values which are owned by this rank
            % should be send
            transferFlags = false( size(transfer{rank+1}) );
            transferFlags( transfer{rank+1}==rank ) = transfer{partnerRank+1};
            
            % If values must be summed, also select to send the values
            % owned by the partnerRank
            if version==1
                transferFlags( transfer{rank+1}==partnerRank ) = true;
            end
            numSend = nnz( transferFlags );
            
            
            % Determine which and the amount of values to send
%             if version==1
%                 transferFlags = ( transfer{rank+1}==partnerRank );
%             else
%                 transferFlags = false( size(transfer{rank+1}) );
%             end
%             
%             if ( nnz( transfer{partnerRank+1} )>0 )
%                 transferFlags( transfer{rank+1}==rank ) = transfer{partnerRank+1};
%                 numSend = nnz( transferFlags );
%             else
%                 numSend = 0;
%             end
            
            if (numSend>0)
                data_out    = x_in( transferFlags );
                %fprintf('send2 on rank %d to rank %d (%d values)\n',rank,partnerRank,numSend)
                NMPI.Send( data_out, numSend, partnerRank );
            end
            
        elseif ( partnerRank > rank )
            % Determine which and the amount of values to receive
            transferFlags = ( transfer{rank+1}==partnerRank );
            if version==1
                transferFlags( transfer{rank+1}==rank ) = transfer{partnerRank+1}; 
            end            
            numRecv = nnz( transferFlags ); 
            
            if (numRecv>0)
                data_in = NMPI.Recv( numRecv, partnerRank );
                %fprintf('recv2 on rank %d from rank %d (%d values)\n',rank,partnerRank,numRecv)                
                x_out( transferFlags ) = m1 * x_out( transferFlags ) + m2 * data_in;
            end
            
        end
    end
    
%     catch MExp
%         save(['syncData.' int2str(System.rank) '.mat']);
%         exit
%     end
end

function x = syncSolutionNew(x,transfer,multiplier)    
    
    if (nargin==2)
        multiplier=0;
    end
    
    x_in    = x;
    x_out   = x;
    
    rank = NMPI.instance.rank;
    
    for i=1:1:NMPI.instance.nproc
        
        partnerRank = i-1;
        
        if (partnerRank ~= rank)
            
            % Message to send
            numSend     = nnz( transfer{partnerRank+1} );
            localData   = x_in( transfer{rank+1}==rank );
            data_out    = localData( transfer{partnerRank+1} );
            
            % Message to receive
            recvFlags   = transfer{rank+1}==partnerRank;
            numRecv     = nnz( recvFlags );
                        
            % Communication
            if (numSend>0 && numRecv>0)
                % Send and receive data
                %fprintf('sendrecv between rank %d and rank %d (send: %d / recv: %d)\n',rank,partnerRank,numSend,numRecv)
                data_in = NMPI.Sendrecv( data_out, numSend, partnerRank, numRecv, partnerRank );
                
            elseif (numSend>0)
                % Only send data
                %fprintf('send from rank %d to rank %d (send: %d)\n',rank,partnerRank,numSend)
                NMPI.Send( data_out, numSend, partnerRank );
                
            elseif (numRecv>0)
                % Only receive data
                %fprintf('recv on rank %d from rank %d (recv: %d)\n',rank,partnerRank,numRecv)
                data_in = NMPI.Recv( numRecv, partnerRank ); 
                
            end
            
            % Store received message
            if (numRecv>0)
                x_out( recvFlags ) = multiplier * x_out( recvFlags ) + data_in;
            end
        end
    end
end

% function R=loc2gbl_matrix_vector(Ke, Ge, GM, dof)
function R=loc2gbl_matrix_vector(Ke, D, GM, dof)

    Ne=length(GM);
    C = cell(Ne,1) ;
    R = zeros(dof,1) ;
    
    Ge = cell(Ne,1) ;
    for e=1:Ne
        Ge{e} = D(GM{e}) ;
    end
    
    % to be parallelized
%     parfor e=1:Ne
    for e=1:Ne
        C{e} = Ke{e}*Ge{e} ;
    end
    
    for e=1:Ne
        I = GM{e} ;
        R(I) = R(I) + C{e} ;
    end
    
end
