function [x_out,iter]=PrecElemCG_parallel(Ke,Ge, GM, prescribed, sharedGM, globalDof, localDof, epsilon, silent, x_init,tag)

    tStart = toc;

    UPLO = 'L';

    if (nargin>7)
        if isempty(silent)
            silent = false;
        end
    end
    
    if nargin<10
        x_init    = zeros(localDof,1);
        if (~silent) 
            fprintf('x_init is reset to zero\n')
        end
    end
    
    if nargin<11
        tag = [];
    end
        
    % Convergence
    tol2        = epsilon*epsilon;
    maxIter     = System.settings.comp.CG.maxIter;
    outputStep  = 100;

    % MPI
    rank        = Parallel3D.MPI.rank; 
%     e_start     = Parallel3D.MPI.eStart;
%     e_end       = Parallel3D.MPI.eEnd;
    
    numLocalElements = numel(Ke);

    % flags
    prec        = 0;
    iter        = 0;
    
    %% Make sure prescribed is a cell array
    % Check if the passed 'prescribed' is a cell array, and if not make a
    % cell array of zeros. This is necessary to make the code below as
    % general as possible.
    if (~iscell(prescribed))
        prescribed = cell(numLocalElements,1);
        for e=1:numLocalElements
            prescribed{e} = false( numel(GM{e}) , 1 );
        end
    end
    
    %% Set the dofType flags for the passed GM
    % The 'dofType' contains a flag to indicate what kind of DOF it is, 
    % there are 3 possible types:
    %
    %   -1  the DOF is not on this rank 
    %    0  the DOF is on this rank and not prescribed (aka free DOF)
    %    1  the DOF is on this rank and prescribed
    %
    % Initially dofType==-1, and based on the passed 'GM' and
    % 'prescribed' cell arrays the corresponding dofType is set
    %
    dofType = -1 * ones(globalDof,1);
    for e=1:numLocalElements
        dofType( GM{e}(:) ) = max( dofType( GM{e}(:) ), prescribed{e} );
    end

    debug = false;
    
    %% Free dofs : dofType==0
    numFree             = nnz(dofType==0);
    numFreeAll          = NMPI_Allreduce(numFree,1,'+',Parallel3D.MPI.mpi_comm_world,'numFreeAll',debug);

    %% Prescribed dofs : dofType==1
    numPrescribed       = nnz(dofType==1);
    numPrescribedAll    = NMPI_Allreduce(numPrescribed,1,'+',Parallel3D.MPI.mpi_comm_world,'numPrescribedAll',debug);
    
    %% Renumbering of free DOFs
    % As the GM does contain prescribed DOFs, these should be removed. By
    % simply removing these, the numbering will be discontinuous.
    % Therefore, below a new GM is formed, which has a continuous numbering
    % for the free DOFs. First a GMmap is formed which maps the full
    % numbering to just free numbering
    GMmap               = zeros(globalDof,1);
    GMmap( dofType==0 ) = (1:numFree);
    
    for e=1:numLocalElements
        if (iscell(prescribed))
            GM{e} = GMmap( GM{e}(~prescribed{e}) );
        else
            GM{e} = GMmap( GM{e} );
        end
    end    

    %% Shared DOFs
    % Determine the shared DOFs (free and prescribed). The 'sharedGM{i}'
    % contains the DOF numbers on this rank which are shared with proc i.
    % By definition 'dofType(sharedGM{i})' should only return 0 and 1
    % values, as the sharedGM contains only local DOFs. 
    numSharedFree       = 0;
    numSharedPrescribed = 0;
    transfer            = cell(Parallel3D.MPI.nproc,1);
    
    for i=1:Parallel3D.MPI.nproc
        if (i~=(Parallel3D.MPI.rank+1))
            transfer{i}         = GMmap( sharedGM{i}( dofType(sharedGM{i})==0 ) );                                  % NB: sharedGM numbering includes prescribed DOFs, therefore GMmap is used
            
            numSharedFree       = numSharedFree       + nnz( dofType(sharedGM{i})==0 );                             % NB: nnz( dofType(sharedGM{i})==0 ) == numel(transfer{i})
            numSharedPrescribed = numSharedPrescribed + nnz( dofType(sharedGM{i})==1 );
        end
    end
    
    %save(['transfer.' int2str(System.rank) '.mat']);
    
    numSharedFreeAll        = NMPI_Allreduce(numSharedFree,1,'+',Parallel3D.MPI.mpi_comm_world,'numSharedFreeAll',debug) / 2;                % divide by 2 because of double counting 
    numSharedPrescribedAll  = NMPI_Allreduce(numSharedPrescribed,1,'+',Parallel3D.MPI.mpi_comm_world,'numSharedPrescribedAll',debug) / 2;          % divide by 2 because of double counting 

    numSharedAll            = numSharedFreeAll+numSharedPrescribedAll;
        
    if ( globalDof ~= ( numFreeAll-numSharedFreeAll + numPrescribedAll-numSharedPrescribedAll ) )
        %error('The passed globalDof (%d) is not equal to the computed globalDof : free (%d) + prescribed (%d) - shared (%d)\n',globalDof,numFreeAll,numPrescribedAll,numSharedAll)
        globalDof = numFreeAll-numSharedFreeAll + numPrescribedAll-numSharedPrescribedAll;
    end
    
    if (rank==0 && ~silent) 
        fprintf('DOFs          | global (unique/shared) : %d (%d/%d) == %d (%d/%d) free + %d (%d/%d) prescribed\n', ...
            globalDof+numSharedAll,globalDof,numSharedAll,...
            numFreeAll,numFreeAll-numSharedFreeAll,numSharedFreeAll,...
            numPrescribedAll,numPrescribedAll-numSharedPrescribedAll,numSharedPrescribedAll);
    end
    
    %% Ensure no sparse arrays are used
    for e=1:numLocalElements
        GM{e} = GM{e}(:);
        Ke{e} = full(Ke{e});
        Ge{e} = full(Ge{e});
    end    
    
    %% Check if each element is of the same size
    sizeKe = 0;
    if (~isempty(Ke))
        sizeKe = size(Ke{1});    
    end
    
    constantSize = true;
    for i=1:numLocalElements
        if (size(Ke{i}) ~= sizeKe)
            constantSize = false;
            break;
        end
    end
    
    %% Set the initial solution x (consisting of free DOFs)
    isPrescribedLocal = logical( dofType( dofType>=0 ) );
    x = x_init(~isPrescribedLocal);

    x = averageElementBoundaries(x,transfer);
    
    if (constantSize)  % weak
        matvecPerElement = false;

        Ke_all = full([Ke{1:numLocalElements}]);
        GM_all = int32([GM{1:numLocalElements}]);

        %x = x_init;
        if (numLocalElements>0)
            Ax_local = matvec_all(UPLO,Ke_all,GM_all,x,transfer);       % U=0 at the initial, originally, R = b-Ax = F - K*U
        else
            Ax_local = [];
        end
        Ax = Ax_local;
    else
        matvecPerElement = true;
        if (numLocalElements>0)
            Ax_local = matvec_one(UPLO,numLocalElements,Ke,x,GM,transfer);
        else
            Ax_local = [];
        end
        Ax = Ax_local;
    end
    
    %% Compute residual R, delta0 (ry_old), 
    M    = zeros(numFree,1);          % extract only diagonal components of Ke and assembly them
    R    = zeros(numFree,1) ;

    for e=1:numLocalElements                                      % length(GM): number of elements
        for ind=1:numel(GM{e})                                  % dofL: Nvar*number of points in one element
            M(GM{e}(ind),1) = M(GM{e}(ind),1) + Ke{e}(ind,ind);
        end 
        R(GM{e}) = R(GM{e}) + Ge{e};
    end
    
    % The M and R values on processor boundaries have to be exchanged. The
    % addElementBoundaries performs a similar operation as NMPI_Reduce, but
    % only on the shared DOFs
    M = addElementBoundaries(M,transfer);
    R = addElementBoundaries(R,transfer);
    
    % Compute the residual, where it should be noted that the shared 
    % (boundary) values of Ax have been exchanged already with neighboring
    % processors at the end of the matvec operation!
    R = R - Ax; 
    
    Minv        = (1./M);
    y           = Minv.*R;
    p           = y;
    delta1      = dotProduct(R,y,transfer);  % delta0      = R'*y;
    beta        = 1;
    alpha       = 1;
    
    t1 = toc;
    
    if (rank==0 && ~silent)
        fprintf('iteration %4d : %e (%f s)\n',0,sqrt(delta1),0)
    end
    
    x_start = x;
    x_limit = 10000;
    delta_ref = 0;
	x_norm  = 10000;
    
    outputProgress = false;
    
    if (outputProgress)
        header = cell(7,1);
        data   = zeros(7,maxIter);
    end
    
    if (rank==0 && ~isempty(tag) && outputProgress)
        filename = [System.settings.outp.directory 'cg-' tag '.txt'];
        header{1} = 'iter';
        header{2} = 'alpha';
        header{3} = 'beta';
        header{4} = 'delta1';
        header{5} = 'x_diff';
        header{6} = 'x_norm';
        header{7} = 'deltaconv';
        System.writeHeader(filename,header);
    end
    
    %fprintf('delta on on rank %d : %f\n',rank,delta1)
    
    if (delta1 > tol2)
        for iter=1:maxIter

            % Step 1: Compute w and pw
            if (numLocalElements>0)
                if (matvecPerElement)
                    w = matvec_one(UPLO,numLocalElements,Ke,p,GM,transfer);
                else
                    w = matvec_all(UPLO,Ke_all,GM_all,p,transfer);
                end
            else
                w=0;
            end
            
            pw = dotProduct(p,w,transfer);

            % Step 2: Compute alpha
            alpha = delta1 / (pw);      % delta1 = ry

            % Step 3: Compute new x
            x = x + alpha*p;
            
            x_norm_old = x_norm;
            x_norm = norm(x-x_start);
            
            %fprintf('in loop on rank %d\n',rank)
            
            x_norm = NMPI_Allreduce(x_norm,1,'+',Parallel3D.MPI.mpi_comm_world,'x_norm',debug) ;
            
            % Step 4: Compute new R
            R = R - alpha*w;

            if (outputProgress)
                x_diff = max(abs(1-x./x_start));
                x_diff = NMPI_Allreduce(x_diff,1,'M',Parallel3D.MPI.mpi_comm_world,'x_diff',debug) ; 
            
                deltaconv = abs((delta1)-(delta_ref))/(delta1);

                if (rem(iter,25)==0)
                    delta_ref = delta1;
                end
            
                data(1,iter) = iter;
                data(2,iter) = alpha;
                data(3,iter) = beta;
                data(4,iter) = delta1;
                data(5,iter) = x_diff;
                data(6,iter) = x_norm; 
                data(7,iter) = deltaconv;
            end
            
            % Step 5: Check for convergence & output information
            %norm_res2 = dotProduct(R,R);
            %norm_res2 = norm_res2 / norm_res2_0;
            if ( delta1 <= tol2 || and( true, abs(x_norm_old-x_norm)/x_norm < 1e-6 ) ) % || (x_diff>0.05 && iter>100) ) % || xnorm>x_limit )
                if (Parallel3D.MPI.rank==0 && ~silent)
                    fprintf('          %5d        residual: %e\n',iter,sqrt(delta1))
                end
                break;
            end
            if (Parallel3D.MPI.rank==0)                
                if (rem(iter,outputStep)==0 && ~silent) 
%                     fprintf('          %5d       residual: %e %e %e\n',iter,sqrt(norm_res2),sqrt(delta1),sqrtnorm_res2_0)
                    t2 = toc;
                    if (rem(iter,100)==0)
                        fprintf('iteration %d : %e (%f s) -- rate: %e -- xnorm: %e\n',iter,sqrt(delta1),t2-t1,abs(sqrt(delta1)-sqrt(delta_ref))/sqrt(delta1),x_diff)
                        delta_ref = delta1;
                    else
                        fprintf('iteration %d : %e (%f s) -- rate: %s -- xnorm: %e\n',iter,sqrt(delta1),t2-t1,'          ',xnorm)
                    end
                end
            end
            
            % Step 6 : Backup delta1 (ry) as delta0 (ry_old)
            delta0 = delta1;

            % Step 7: Compute new y and delta1 (ry)
            y = Minv.*R;
            delta1 = dotProduct(R,y,transfer);
            
            % Step 8: Compute beta
            beta   = delta1 / delta0;   % = ry / ry_old
            
            % Step 9 : Compute new p
            %p_old = p;
            p = y + beta*p;
            
            %fprintf('%6.4f %6.4f\n',alpha,beta)
            %if (iter==1)
            %    filename = ['iterstep_' num2str(rank) '.mat' ];
            %    save(filename,'w','Ke_all','GM_all','p','p_old','x','alpha','beta','delta1','delta0','Minv','R','y','pw');
            %end
        end
    end
        
    if (rank==0 && ~isempty(tag) && outputProgress)
        System.appendToFile(filename,data(:,1:iter));
    end
    
    %xtmp = x;
    %x = averageElementBoundaries(xtmp,transfer);
    %fprintf('There are %d changes in x after syncElementBoundaries\n',nnz(~(x==xtmp)));

    x_out = zeros(localDof,1);
    x_out(isPrescribedLocal)  = x_init(isPrescribedLocal);
    x_out(~isPrescribedLocal) = x;
    
%     x = (rank+1)+0*x;
%     x_out(~isPrescribedLocal) = setElementBoundaries(x,transfer);    
%     x_out(isPrescribedLocal) = 0;
        
    tEnd = toc;
    
    if (rank==0 && ~silent) 
        fprintf('Solver wall clock time (%d iterations) : %f ---- final residual : %e\n',iter,tEnd-tStart,sqrt(delta1))
    end    
end

% 
% function R=loc2gbl_matrix_vector(e_start,e_end,Ke, D, GM, dof)
%     R = zeros(dof,1) ;
% 
%     %JF Parallelization**************************************************
%     for e=e_start:e_end
%         I = GM{e};
%         Ge=D(I);
%         C = Ke{e-e_start+1}*Ge ;
%         R(I) = R(I) + C ;
%     end 
% end
% 
function [Ke,Ge,GM_out] = sortArrays(Ke,Ge,GM,dof)
    GM_out = cell(length(Ke),1);
    
    % Make Ke full again
    %% Upper format
%     for i=1:length(Ke)
%         Ke{i} = triu(Ke{i},1)'+Ke{i};
%         [~,idx]=sort(GM{i});
%         Ke{i} = Ke{i}(idx,idx);     % shuffle columns of Ke in sorted order
%         Ke{i} = triu(Ke{i});
%         Ge{i} = Ge{i}(idx);
%         GM_out{i} = sparse(false(dof,1));
%         GM_out{i}(GM{i}(:)) = true;
%     end
    
    %% Lower format
    for i=1:length(Ke)
        Ke{i} = tril(Ke{i},1)'+Ke{i};
        [~,idx]=sort(GM{i});
        Ke{i} = Ke{i}(idx,idx);     % shuffle columns of Ke in sorted order
        Ke{i} = tril(Ke{i});
        Ge{i} = Ge{i}(idx);
        GM_out{i} = sparse(false(dof,1));
        GM_out{i}(GM{i}(:)) = true;
    end
end

function R = matvec_one(ul,numLocalElements,Ke, D, GM, transfer)
    R = zeros(length(D),1) ;
    
%     D = averageElementBoundaries(D,transfer);
    
    switch ul
        case 'U'
            for e=1:numLocalElements
                R(GM{e}) = R(GM{e}) + dsymv( Ke{e} , D(GM{e}) );
            end
        case 'L'
            for e=1:numLocalElements
                R(GM{e}) = R(GM{e}) + dsymvL( Ke{e} , D(GM{e}) );
            end   
    end
    
    R = addElementBoundaries(R,transfer);
end

% Compute R = Ke * p, where GM are the locations of the DOFs in p
function R = matvec_all(ul,Ke,GM,p,transfer)
    
    %p = averageElementBoundaries(p,transfer);
    
    switch ul
        case 'U'
            R = dsymvav( GM, Ke, p); % Ke in upper format
        case 'L'
            R = dsymvavL( GM, Ke, p); % Ke in lower format
    end
    
    R = addElementBoundaries(R,transfer);
end

% function R = matvec_all_logical(Ke,GM,p)
%     R = dsymvav_logical( GM, Ke, p); 
% end
% 
% function b = matvec_element(A,x,numLocalElements,transfer)
%     x = syncElementBoundaries(x,numLocalElements,transfer);
%     b = cell(numLocalElements,1);
%     for e=1:numLocalElements
%         b{e} = dsymvavLelement(A{e},x{e});
%     end
% end

function c = dotProduct(a,b,transfer)

%     if (isempty(a) || isempty(b))
%         fprintf('a or b is empty on rank %d\n',System.rank);
%     end

    try
        b_tmp = b;
        for i=1:Parallel3D.MPI.rank
            count  = numel(transfer{i});
            if (count>0)
                b_tmp(transfer{i}) = 0;
            end
        end
        
        %c = dot(a,b_tmp);
        c = a'*b;
    catch
        if (isempty(a) || isempty(b))
            fprintf('a or b is empty on rank %d\n',System.rank);
            c = 0;
        else
            c = dot(vertcat(a{:}),vertcat(b{:}));           % if (iscell(a) && iscell(b))
        end
    end
    
    c = NMPI_Allreduce(c,1,'+',Parallel3D.MPI.mpi_comm_world); %,'dotProduct',1) ; 
end

function x_out = swapElementBoundaries(x,transfer)
    x_out = syncElementBoundaries(x,transfer,0,1,4);
end

function x_out = setElementBoundaries(x,transfer)
    x_out = x;
    for i=1:Parallel3D.MPI.nproc
        if (i~=Parallel3D.MPI.rank+1)
            x_out(transfer{i}) = (i+Parallel3D.MPI.rank+1)*0.5;
        end
    end
end

function x_out = averageElementBoundaries(x,transfer)
    x_out = syncElementBoundaries(x,transfer,0.5,0.5,4);
end

function x_out = addElementBoundaries(x,transfer)
    x_out = syncElementBoundaries(x,transfer,1,1,4);
end

function x_out = moveElementBoundaries(x,transfer)
    x_out = syncElementBoundaries(x,transfer,0,1,1);
end

% General function to synchronize boundary values between processors. The 
% alpha values determine the way values are synchronized:
%
%   alpha1 =  0  & alpha2 =  1      : swap values between processors
%   alpha1 =  1  & alpha2 =  0      : nothing happens, no values are copied between processors
%   alpha1 = 0.5 & alpha2 = 0.5     : values are averaged between processors
%   alpha1 =  1  & alpha2 =  1      : values are added to local values
%
function x_out = syncElementBoundaries(x,transfer,alpha1,alpha2,direction)    
    
    rank = Parallel3D.MPI.rank;
    
    x_in  = x;    
    x_out = x;
   
    if (alpha2~=0)
        
        %disp('in syncElementBoundaries')
        %save(['syncElementBoundaries.' int2str(System.rank) '.mat']);
        
        if (direction==1 || direction==4)
            % RECV FROM LOWER RANKS, SEND TO HIGHER RANKS
            for i=Parallel3D.MPI.nproc:-1:1
                if (i<rank+1)
                    count  = numel(transfer{i});
                    if (count>0)
                        source = i-1;
                        x_tmp = NMPI_Recv( count, source );
                        x_out(transfer{i}) = alpha1*x_out(transfer{i}) + alpha2*x_tmp;
                    end
                elseif (i>rank+1)
                    count  = numel(transfer{i});
                    if (count>0)
                        data   = x_in(transfer{i});
                        dest   = i-1;                        
                        NMPI_Send( data, count, dest );
                    end
                end
            end
        end

        if (direction==2 || direction==4)
            % RECV FROM HIGHER RANKS, SEND TO LOWER RANKS
            for i=1:1:Parallel3D.MPI.nproc
                if (i<rank+1)
                    count  = numel(transfer{i});
                    if (count>0)
                        data   = x_in(transfer{i});                        
                        dest   = i-1;
                        NMPI_Send( data, count, dest );
                    end
                elseif (i>rank+1)
                    count  = numel(transfer{i});
                    if (count>0)
                        source = i-1;
                        x_tmp = NMPI_Recv( count, source );
                        x_out(transfer{i}) = alpha1*x_out(transfer{i}) + alpha2*x_tmp;
                    end
                end
            end
        end
        
    end
end


% function x_out = addElementBoundaries(x,transfer)    
%     x_in  = x;    
%     x_out = x;
% 
%     % RECV FROM LOWER RANKS, SEND TO HIGHER RANKS
%     for i=Parallel3D.MPI.nproc:-1:1
%         if (i<Parallel3D.MPI.rank+1)
%             count  = numel(transfer{i});
%             source = i-1;
%             x_tmp = NMPI_Recv( count, source );
%             x_out(transfer{i}) = x_out(transfer{i}) + x_tmp;
%         elseif (i>Parallel3D.MPI.rank+1)
%             data   = x_in(transfer{i});
%             count  = numel(transfer{i});
%             dest   = i-1;
%             NMPI_Isend( data, count, dest );
%         end
%     end
% 
%     % RECV FROM HIGHER RANKS, SEND TO LOWER RANKS
%     for i=1:1:Parallel3D.MPI.nproc
%         if (i>Parallel3D.MPI.rank+1)
%             count  = numel(transfer{i});
%             source = i-1;
%             x_tmp = NMPI_Recv( count, source );
%             x_out(transfer{i}) = x_out(transfer{i}) + x_tmp;
%         elseif (i<Parallel3D.MPI.rank+1)
%             data   = x_in(transfer{i});
%             count  = numel(transfer{i});
%             dest   = i-1;
%             NMPI_Isend( data, count, dest );
%         end
%     end
% end
