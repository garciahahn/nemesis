function [alf,iter] = PrecElemCG_par(my_rank,ranks,index_e_start,index_e_end,Ke,Ge, GM, GMlocal, dof, numLocalDof, epsilon)

    tol2 = epsilon*epsilon;
    
    % In the new code the matrices of Ke are lower/upper triangular, which 
    % is not supported here. Therefore a full matrix for each element has 
    % to be formed
    %fprintf('the size of Ke is %d %d on rank %d\n',size(Ke),my_rank)
    if ( istril(Ke{1}) )
        e_start = index_e_start;
        e_end   = index_e_end;
        for e=e_start:e_end
            Ke{e-e_start+1} = Ke{e-e_start+1}+tril(Ke{e-e_start+1},-1)';
            GM{e} = GM{e}(:);
        end
    elseif ( istriu(Ke{1}) )
        e_start = index_e_start;
        e_end   = index_e_end;
        for e=e_start:e_end
            Ke{e-e_start+1} = Ke{e-e_start+1}+triu(Ke{e-e_start+1},1)';
            GM{e} = GM{e}(:);
        end
    else
        e_start = index_e_start(my_rank+1);
        e_end   = index_e_end(my_rank+1);
    end

%function [alf,iteration,delta0]=PrecElemCG_par(comm,my_rank,ranks,index_e_start,index_e_end,Ke,Ge, GM, dof, epsilon)
% epsilon: the amount of convergence (user's choice)
% (13.10.14) upgraded to be solved by multiple processors
  
    Master_rank=0;
    Qinv=zeros(dof,1);          % extract only diagonal components of Ke and assembly them
    dofL=length(GM{1});
    U=zeros(dof,1);
    R=zeros(dof,1) ;
    % construction of Qinv & R vector
    
    for el_=e_start:e_end        % length(GM): number of elements
        for index_=1:dofL       % dofL: Nvar*number of points in one element
            Qinv(GM{el_}(index_),1)=Qinv(GM{el_}(index_),1) + Ke{el_-e_start+1}(index_,index_);
        end 
        R(GM{el_}) = R(GM{el_}) + Ge{el_-e_start+1} ;
    end
    
    D=zeros(dof,1);
    B=zeros(dof,1);
    
    Qinv = NMPI_Reduce(Qinv,dof,'+',Master_rank) ;
    R = NMPI_Reduce(R,dof,'+',Master_rank) ;
        
    if my_rank==Master_rank
        
        Qinv = 1./Qinv;
        
        R=-R;       % U=0 at the initial, originally, R=K*U-F
        B=Qinv.*R;
        D=-B;
        delta0=R'*B;
    else
        delta0 = 10; % not used
    end 
    
    iter=0;
    
    filename = ['Solver_' num2str(my_rank) '.mat' ];
    save(filename,'Ke','Ge','GM','D','B','R','delta0','Qinv','dof','GMlocal','numLocalDof');
    
% missed the delta0 criteria for this while loop.
    while(1)
        
        D=NMPI_Bcast3(D,dof,Master_rank,my_rank);

        if (D(1)==1) && (D(2)==2) && (D(3)==3) 
            break;
        end
        
        iter=iter+1;
        
%     %JF Changed
        B = loc2gbl_matrix_vector(e_start , e_end , Ke , D , GM, dof );
        B = NMPI_Reduce(B,dof,'+',Master_rank) ;

        if my_rank==Master_rank

            tau=delta0/(D'*B);
            U=U+tau*D;
            R=R+tau*B;
            B=Qinv.*R;
            delta1=R'*B;
               
            beta=delta1/delta0;
            delta0=delta1;
            D=-B+beta*D;
            if (delta1 <= tol2)
            	D(1)=1; D(2)=2; D(3)=3;
            end

            if rem(iter,5000)==0
                fprintf('iteration %d : delta1 = %e\n',iter,delta1) ;
            end

              
        end %if

    end %while
    
    U=NMPI_Bcast3(U,dof,Master_rank,my_rank);
    
    %alf = U;
    alf = zeros(numLocalDof,1);
    for e=e_start:e_end
        alf( GMlocal{e-e_start+1} ) = U( GM{e} );
    end
    
    if my_rank==Master_rank
        display(['     iteration for CG: ', num2str(iter)]) ;
    end

end %function ElemCG

%changed JF
function R=loc2gbl_matrix_vector(e_start,e_end,Ke, D, GM, dof)
    R = zeros(dof,1) ;
    %JF Parallelization**************************************************
    for e=e_start:e_end
        I = GM{e};

        Ge=D(I);

        C = Ke{e-e_start+1}*Ge ;

        R(I) = R(I) + C ;
       
    end

end


