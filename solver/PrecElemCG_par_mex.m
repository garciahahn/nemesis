function [alf,iter]=PrecElemCG_par_mex(Ke,Ge, GM, prescribed, dof, epsilon, x_init)

    if nargin<7
        x_init    = zeros(dof,1);
        fprintf('x_init is reset to zero')
    end
    
    % Convergence
    tol2        = epsilon*epsilon;
    maxIter     = 100000;

    % MPI
    rank        = Parallel3D.MPI.rank;
    Master_rank = 0;   
    e_start     = Parallel3D.MPI.eStart;
    e_end       = Parallel3D.MPI.eEnd;
    
    % flags
    oldStyle    = true;        % true=full solution method, false=elemental method (each element has it's own local solution)
    prec        = 0;

    % timing
    tStart      = toc;
    temp1       = toc;
    t           = zeros(15,1);

    %% Compute A*x
    for e=e_start:e_end
        GM{e} = GM{e}(:);
        Ke{e} = full(Ke{e});
        Ge{e} = full(Ge{e});
    end    
    
    sorted = false;
    
    if (oldStyle)
        if (nnz([prescribed{1:end}])==0)  % weak
            strongSolver = false;

    %         [Ke,Ge,GMsorted] = sortArrays(Ke,Ge,GM,dof);
    % %         for e=e_start:e_end
    % %             GM{e} = full(GM{e});
    % %         end
    %         sorted = true;

            Ge_local = full([Ge{e_start:e_end}]);
            Ke_local = full([Ke{e_start:e_end}]);
            GM2      = int32([GM{e_start:e_end}]);

            [alf,residual,iter] = cgSolver(Ke_local,Ge_local,x_init,GM2,e_start,e_end,20000,epsilon,1000,Parallel3D.MPI.mpi_comm_world);
            fprintf('Final residual is %f after %d iterations\n',residual,iter);
        else
            strongSolver = true;
            x = x_init;
    %         [Ke,Ge,GM] = sortArrays(Ke,Ge,GM,dof);
    %         sorted = true;
            Ax_local = matvec_one(e_start,e_end,Ke,x,GM);
            Ax = Ax_local;
        end
    else
        x  = cell(e_end-e_start+1,1);
        
        % Convert x into elemental x
        for e=e_start:e_end  
            x{e} = x_init(GM{e});
        end
        
        Ax = matvec_element(Ke,x,e_start,e_end);       % U=0 at the initial, originally, R = b-Ax = F - K*U
    end
 
% % %     temp2 = toc; t(1) = t(1) + temp2-temp1; temp1 = toc;
% % %     
% % %     %% Compute residual R, delta0 (ry_old), 
% % %     if (oldStyle)
% % %         M    = zeros(dof,1);          % extract only diagonal components of Ke and assembly them
% % %         Minv = zeros(dof,1);          % extract only diagonal components of Ke and assembly them
% % %         R    = zeros(dof,1) ;
% % % 
% % %         % construction of preconditioner M & R vector
% % %         if (sorted)
% % %             for e=e_start:e_end                                         % length(GM): number of elements
% % %                 M( GM{e} ) = M( GM{e} ) + diag( Ke{e} ); 
% % %                 R( GM{e} ) = R( GM{e} ) + Ge{e};
% % %             end   
% % %         else
% % %             for e=e_start:e_end                                         % length(GM): number of elements
% % %                 for ind=1:numel(GM{e})                                  % dofL: Nvar*number of points in one element
% % %                     M(GM{e}(ind),1) = M(GM{e}(ind),1) + Ke{e}(ind,ind);
% % %                 end 
% % %                 R(GM{e}) = R(GM{e}) + Ge{e};
% % %             end
% % %         end
% % % 
% % %         R = R - Ax;
% % %         norm_res2_0 = dot(R,R);
% % %         norm_res2   = norm_res2_0;
% % %         sqrtnorm_res2_0 = sqrt(norm_res2_0);
% % %         
% % %         p = zeros(dof,1);
% % %         
% % %         M = NMPI_Allreduce(M,dof,'+',Parallel3D.MPI.mpi_comm_world) ;
% % %         R = NMPI_Allreduce(R,dof,'+',Parallel3D.MPI.mpi_comm_world) ;
% % % 
% % %         if rank==Master_rank
% % %             Minv        = (1./M);
% % %             y           = Minv.*R;
% % %             p           = y;
% % %             delta0      = R'*y;
% % %             delta_init  = delta0;
% % %         end 
% % %     else
% % %         Minv = cell(e_end-e_start+1,1);
% % %         R    = cell(e_end-e_start+1,1);
% % %         p    = cell(e_end-e_start+1,1);
% % % %         w    = cell(e_end-e_start+1,1);
% % %         y    = cell(e_end-e_start+1,1);
% % %         
% % %         dof  = size(Ke{1},1);
% % %         
% % %         for e=e_start:e_end
% % % %             Minv{e} = zeros()
% % %             Minv{e} = zeros(dof,1);          % extract only diagonal components of Ke and assembly them
% % %             R{e}    = zeros(dof,1) ;
% % %             p{e}    = zeros(dof,1);
% % %             y{e}    = zeros(dof,1);
% % %         end
% % % 
% % %         % construction of preconditioner M & R vector
% % %         for e=e_start:e_end                                         % length(GM): number of elements
% % %             for ind=1:dof                                           % dofL: Nvar*number of points in one element
% % %                 Minv{e}(ind,1) = 1.0/Ke{e}(ind,ind);
% % %             end 
% % %             R{e} = Ge{e} - Ax{e};
% % %         end
% % %         
% % %         norm_res2_0 = 0;
% % %         delta0      = 0;
% % %         
% % %         for e=e_start:e_end
% % %             norm_res2_0 = norm_res2_0 + dot(R{e},R{e});
% % %         end
% % %         
% % %         sqrtnorm_res2_0 = sqrt(norm_res2_0);
% % %         
% % %         % all reduce between proc
% % %                 
% % % %         Minv(M==0)  = 0;
% % %         for e=e_start:e_end  
% % %             y{e}        = Minv{e}.*R{e};
% % %             p{e}        = y{e};
% % %             delta0      = delta0 + dot(R{e},y{e});
% % %         end
% % %     end
% % %     
% % %     iter=0;
% % %     
% % %     temp2 = toc; t(2) = t(2) + temp2-temp1; 
% % % 
% % %     for iter=1:maxIter
% % %         
% % %         temp1 = toc;
% % %         
% % %         if (oldStyle)
% % %             p=NMPI_Bcast(p,dof,Master_rank,rank);
% % %         end
% % %         
% % %         temp2 = toc;
% % %         t(3) = t(3) + temp2-temp1;
% % %         temp1 = toc;
% % %         
% % % %         if (p(1)==1) && (p(2)==2) && (p(3)==3) 
% % % %             break;
% % % %         end
% % %         if (oldStyle)
% % %             if (strongSolver)
% % %                 w = matvec_one(e_start,e_end,Ke,p,GM);
% % %             else
% % %                 w = matvec_all(Ke_local,GM2,p);
% % %             end
% % %         else
% % %             w = matvec_element(Ke,p,e_start,e_end);       % U=0 at the initial, originally, R = b-Ax = F - K*U
% % %         end
% % %             
% % %         temp2 = toc;
% % %         t(4) = t(4) + temp2-temp1;
% % % 
% % %         if (oldStyle)
% % %             temp1 = toc;
% % % 
% % %             w = NMPI_Reduce(w,dof,'+',Master_rank) ;
% % % 
% % %             temp2 = toc;
% % %             t(5) = t(5) + temp2-temp1;
% % %             temp1 = toc;
% % % 
% % %             if rank==Master_rank
% % % 
% % %                 fprintf('pw          = %f\n',dot(p,w));
% % %                 
% % %                 alpha = delta0 / dot(p,w);
% % % 
% % %                 fprintf('post alpha  = %f\n',alpha);
% % % 
% % %                 x = x + alpha*p;
% % %                 Rold = 0*R;  % flexible CG (see Wikipedia)
% % %                 R = R - alpha*w;                
% % %                 
% % %                 norm_res2 = dot(R,R);
% % %                 norm_res2 = norm_res2 / norm_res2_0;
% % %                 
% % %                 if rem(iter,1)==0
% % %                     fprintf('          %5d       residual: %e %e %e\n',iter,sqrt(norm_res2),sqrt(norm_res2*norm_res2_0),sqrtnorm_res2_0)
% % % %                     fprintf('          %5d       timings : %f %f %f %f\n',iter,t(3),t(4),t(5),t(6))
% % % %                     break;
% % %                 end
% % %                 
% % %                 if (norm_res2 <= tol2)
% % %                     fprintf('          %5d       residual: %e %e %e\n',iter,sqrt(norm_res2),sqrt(norm_res2*norm_res2_0),sqrtnorm_res2_0)
% % %                 end
% % %                 
% % %                 y = Minv.*R;
% % % 
% % %                 delta1 = (R-Rold)'*y;
% % %                 beta   = delta1 / delta0;
% % %                 delta0 = delta1;
% % % 
% % %                 p = y + beta*p;
% % % 
% % %             end %if
% % %             
% % %             norm_res2 = NMPI_Bcast(norm_res2,1,Master_rank,rank);
% % %             if (norm_res2 <= tol2)
% % %                 break;
% % %             end
% % %             
% % %             temp2 = toc;
% % %             t(6) = t(6) + temp2-temp1;
% % %         else
% % %             % newStyle
% % %             temp1 = toc;
% % %             
% % %             pw = dotProduct(p,w);
% % %             
% % % %             pw = NMPI_Allreduce(pw,1,'+',Parallel3D.MPI.mpi_comm_world) ; 
% % %             
% % %             temp2 = toc;
% % %             t(5) = t(5) + temp2-temp1;
% % %             temp1 = toc;
% % %             
% % %             alpha = delta0 / (pw);      % delta0 = ry_old
% % % 
% % %             for e=e_start:e_end 
% % %                 x{e} = x{e} + alpha*p{e};
% % %                 R{e} = R{e} - alpha*w{e};
% % %             end
% % % 
% % %             norm_res2 = dotProduct(R,R);
% % %             norm_res2 = norm_res2 / norm_res2_0;
% % %             
% % %             if (Parallel3D.MPI.rank==0 && rem(iter,1000)==0)
% % %                 fprintf('          %5d       residual: %e %e %e\n',iter,sqrt(norm_res2),sqrt(norm_res2*norm_res2_0),sqrtnorm_res2_0)
% % %                 fprintf('          %5d       timings : %f %f %f %f\n',iter,t(3),t(4),t(5),t(6))
% % %             end
% % %             
% % %             if (norm_res2 <= tol2)
% % %                 break;
% % %             end
% % %             
% % %             for e=e_start:e_end 
% % %                 y{e} = Minv{e}.*R{e};
% % %             end
% % %             
% % %             delta1 = dotProduct(R,y);              % = ry
% % %             
% % % %             delta1 = NMPI_Allreduce(delta1,1,'+',Parallel3D.MPI.mpi_comm_world) ; 
% % %              
% % %             beta   = delta1 / delta0;   % = ry / ry_old
% % %             delta0 = delta1;
% % %             
% % %             for e=e_start:e_end 
% % %                 p{e} = y{e} + beta*p{e};
% % %             end
% % % 
% % % %             if rem(iter,500)==0
% % % %                 fprintf('          %5d       residual: %e %e\n',iter,delta1,delta_init)
% % % %             end
% % %             
% % %             temp2 = toc;
% % %             t(6) = t(6) + temp2-temp1;
% % %         end
% % %         
% % %         fprintf('%d : alpha = %f  beta= %f  delta0 = %f\n',iter,alpha,beta,delta0);
% % %         
% % %     end %while
% % % 
% % %     temp1 = toc;
% % % 
% % %     x = NMPI_Bcast(x,dof,Master_rank,rank);
% % %     
% % % %     if (dof_all ~= dof)
% % % %         alf = zeros(dof_all,1);
% % % %         alf(GM_global(:,1)~=0,1) = x;
% % % %     else
% % %         alf = x;
% % % %     end
% % %     
% % %     temp2 = toc;
% % %     t(7) = t(7) + temp2-temp1;
% % %     temp1 = toc;
% % %     
% % % %     if my_rank==Master_rank
% % % % %         fprintf('mv_product : %e %e %e\n',t_mvProduct1,t_mvProduct2,t_mvProduct3)
% % % %         fprintf('mv_product : %e \n',   t_mvProduct3)
% % % %         fprintf('reduceB    : %e\n',t_reduceB)
% % % %     end    
% % %     
% % % %     if my_rank==Master_rank
% % % %         fprtinf('     iteration for CG: %5d', iter);
% % % %     end
% % % 
% % % %     endTime = toc;
% % %     
% % % %     time1 = t1 - startTime;
% % % %     time2 = t2 - t1;
% % % %     time3 = endTime - t2;
% % % %     time4 = endTime - startTime;
% % %     
% % % %     if (Parallel3D.MPI.rank==0)
% % % %         fprintf('Solver timings (%d) : %f %f %f %f \n',iter,time1,time2,time3,time4)
% % % %     end
% % % 
% % %     temp2 = toc;
% % %     t(8) = t(8) + temp2-temp1;
% % %     
% % %     tEnd = toc;
% % %     
% % %     if (Parallel3D.MPI.rank==0)
% % %         fprintf('Solver timings (%d) : %f ',iter,t(1))
% % %         for i=2:7
% % %             fprintf(' %f ',t(i));
% % %         end
% % %         fprintf(' %f %f\n',t(8),tEnd-tStart);
% % %     end
end %function ElemCG
% 
% function R=loc2gbl_matrix_vector(e_start,e_end,Ke, D, GM, dof)
%     R = zeros(dof,1) ;
% 
%     %JF Parallelization**************************************************
%     for e=e_start:e_end
%         I = GM{e};
%         Ge=D(I);
%         C = Ke{e-e_start+1}*Ge ;
%         R(I) = R(I) + C ;
%     end 
% end
% 
function [Ke,Ge,GM_out] = sortArrays(Ke,Ge,GM,dof)
    GM_out = cell(length(Ke),1);
    
    % Make Ke full again
    %% Upper format
%     for i=1:length(Ke)
%         Ke{i} = triu(Ke{i},1)'+Ke{i};
%         [~,idx]=sort(GM{i});
%         Ke{i} = Ke{i}(idx,idx);     % shuffle columns of Ke in sorted order
%         Ke{i} = triu(Ke{i});
%         Ge{i} = Ge{i}(idx);
%         GM_out{i} = sparse(false(dof,1));
%         GM_out{i}(GM{i}(:)) = true;
%     end
    
    %% Lower format
    for i=1:length(Ke)
        Ke{i} = tril(Ke{i},1)'+Ke{i};
        [~,idx]=sort(GM{i});
        Ke{i} = Ke{i}(idx,idx);     % shuffle columns of Ke in sorted order
        Ke{i} = tril(Ke{i});
        Ge{i} = Ge{i}(idx);
        GM_out{i} = sparse(false(dof,1));
        GM_out{i}(GM{i}(:)) = true;
    end
end

function R = matvec_one(e_start,e_end,Ke, D, GM)
    R = zeros(length(D),1) ;
    for e=e_start:e_end
        R(GM{e}) = R(GM{e}) + dsymv( Ke{e-e_start+1} , D(GM{e}) );
    end 
end

function R = matvec_all(Ke,GM,p)
%     R = dsymvav( GM, Ke, p); % Ke in upper format
    R = dsymvavL( GM, Ke, p); % Ke in lower format
end

function R = matvec_all_logical(Ke,GM,p)
    R = dsymvav_logical( GM, Ke, p); 
end

function b = matvec_element(A,x,e_start,e_end)
%     x = syncElementBoundaries(x,e_start,e_end);
    b = cell(e_end-e_start+1,1);
    for e=e_start:e_end  
        b{e} = dsymvavLelement(A{e},x{e});
    end
end

function c = dotProduct(a,b,e_start,e_end)
    c = 0;
    if nargin==4
        for e=e_start:e_end  
            c = c + dot(a{e},b{e});
        end
    elseif nargin==2
        c = dot(vertcat(a{:}),vertcat(b{:}));
    end
    % c = NMPI_Allreduce(c,1,'+',Parallel3D.MPI.mpi_comm_world) ; 
end

function x_out = syncElementBoundaries(x,e_start,e_end)
    for e=e_start:e_end 
        x;
    end
end