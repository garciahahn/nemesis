function compileSolver

    [root] = fileparts( mfilename('fullpath') );

    %mex([root,'/source/cgSolver.c'],'CC=mpicc','COMPFLAGS -O3 -march=native','-lmwblas','-outdir',root)
    mex([root,'/source/dsymv.c'],'COMPFLAGS -O3 -Wall -march=native','-lmwblas','-outdir',root)
    mex([root,'/source/dsymvav-logical.c'],'COMPFLAGS -O3 -Wall -march=native','-lmwblas','-outdir',root)
    mex([root,'/source/dsymvav.c'],'COMPFLAGS -O3 -Wall -march=native','-lmwblas','-outdir',root)
    mex([root,'/source/dsymvavL.c'],'COMPFLAGS -O3 -Wall -march=native','-lmwblas','-outdir',root)
    mex([root,'/source/dsymvavLelement.c'],'COMPFLAGS -O3 -Wall -march=native','-lmwblas','-outdir',root)
    mex([root,'/source/dsymv.c'],'COMPFLAGS -O3 -Wall -march=native','-lmwblas','-outdir',root)
    mex([root,'/source/dsymvL.c'],'COMPFLAGS -O3 -Wall -march=native','-lmwblas','-outdir',root)
    mex([root,'/source/dsyrkL.c'],'COMPFLAGS -O3 -Wall -march=native','-lmwblas','-outdir',root)
    if ispc
    extra_args = {'-IC:\Program Files (x86)\Microsoft SDKs\MPI\Include', '-LC:\Program Files (x86)\Microsoft SDKs\MPI\Lib\x64', '-lmsmpi'};
    mex([root,'/source/parMex.c'],'CC=mpicc', extra_args{:},'-lmwblas','-outdir',root)
    else
    mex([root,'/source/parMex.c'],'CC=mpicc','-lmwblas','-outdir',root)
    end
    %mex([root,'/source/sparseMatrixMultiply.c'],'COMPFLAGS -O3 -Wall -march=native','-I/opt/intel/mkl_2019.3.199/mkl/include -L/opt/intel/mkl_2019.3.199/mkl/lib/intel64/','-lmwblas','-outdir',root)
end