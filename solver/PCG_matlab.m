function [alf,info]=PCG_matlab(Ke,Ge, GM, dof, epsilon)
% epsilon: the amount of convergence (user's choice)
% (13.10.14) upgraded to be solved by multiple processors
    
    tol2 = epsilon*epsilon;
    
    Qinv=zeros(dof,1);          % extract only diagonal components of Ke and assembly them
    dofL=length(GM{1});
    
    % to be parallelized
    for el_=1:length(GM)        % length(GM): number of elements
        for index_=1:dofL       % dofL: Nvar*number of points in one element
            Qinv(GM{el_}(index_),1)=Qinv(GM{el_}(index_),1) + Ke{el_}(index_,index_);
        end
    end
    Qinv = 1./Qinv;
    

    iter=1;
    U=zeros(dof,1);
    %R=F;
    R=loc2gbl(Ge,GM, dof);      % assembly of right-hand side vector
    %R=K*U-F;
    R=-R;       % U=0 at the initial, originally, R=K*U-F
    
    B=Qinv.*R;
    
    D=-B;
    
    delta0=R'*B;
    
    info.initRes = sqrt(delta0);
    
    if (delta0>tol2) 
        while(1)
            iter=iter+1;
            
%             B= loc2gbl_matrix_vector(Ke, gbl2loc(D, GM) , GM, dof );    % Is it the most effective way? :)
            B= loc2gbl_matrix_vector(Ke, D, GM, dof );      % This is better
            
            tau=delta0/(D'*B);
            U=U+tau*D;
            R=R+tau*B;
            B=Qinv.*R;
            delta1=R'*B;
            
            if delta1 <= tol2
                break;
            end
            beta=delta1/delta0;
            delta0=delta1;
            D=-B+beta*D;
            
        end %while
    end %if
    
    alf = U ;
    
    info.finalRes = sqrt(delta1);
    info.iter     = iter;

end %function ElemCG

        
% function R=loc2gbl_matrix_vector(Ke, Ge, GM, dof)
function R=loc2gbl_matrix_vector(Ke, D, GM, dof)

    Ne=length(GM);
    C = cell(Ne,1) ;
    R = zeros(dof,1) ;
    
    Ge = cell(Ne,1) ;
    for e=1:Ne
        Ge{e} = D(GM{e}) ;
    end
    
    % to be parallelized
%     parfor e=1:Ne
    for e=1:Ne
        C{e} = Ke{e}*Ge{e} ;
    end
    
    for e=1:Ne
        I = GM{e} ;
        R(I) = R(I) + C{e} ;
    end
    
end

function G=loc2gbl(Ge, GM, dof)
    Ne=length(GM);
    G=zeros(dof,1);
    
    for e=1:Ne
        G(GM{e})=G(GM{e})+Ge{e} ;
    end
    
end %function loc2gbl

function Ge=gbl2loc(G, GM)
    Ne=length(GM);
    Ge=cell(Ne,1);
    for e=1:Ne
        Ge{e}=G(GM{e});
    end
end %function gbl2loc
