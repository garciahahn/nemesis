classdef Matrix_v0 < handle
    %VECTOR A vector is a collection of alpha arrays of fields
    %   Detailed explanation goes here
    
    properties (Access=protected)
        fields;
        mesh;
        name;
        numFree;
        type;
        
        valueArray;
        valueCell;
                
        valueType;
    end
    
    methods
        function s = Matrix_v0(name,mesh,varargin)
            
            %% Process input
            s.name          = name;     
            s.mesh          = mesh;
            s.valueCell  	= varargin{:};            
        end
        
        function out = mtimes(s,vector)
            temp = cell(1,numel(s.valueCell));
            
            for e=1:numel(s.valueCell)
                temp{e} = s.valueCell{e} * vector.valueCell{e}(:);
            end
            
            newName = [s.name '*' vector.name];
            
            out = Vector(newName,vector.mesh,VectorType.value,temp);
        end
        
        function disp(s)
            fprintf('Matrix %s with %d unkowns\n',s.name,s.numFree)
        end
        
%         function outputArg = method1(obj,inputArg)
%             %METHOD1 Summary of this method goes here
%             %   Detailed explanation goes here
%             outputArg = obj.Property1 + inputArg;
%         end
    end
end

