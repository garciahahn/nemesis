classdef Solver_dh < handle
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        method;
        
        A, f, x;
        Ke, Ge;
        dof;
        GM;
        
        % Preconditioning
        M;
        
        % Deflation
        Z; E; Q; P; x2;
        
        % Analysis
        ev1, ev2; ev3; p;
    end
    
    methods
        function s = Solver_dh(Ke,Ge,dh,x_init)
            s.ev1 = 0;
            s.ev2 = 0;
            s.ev3 = 0;
            
            e_start     = dh.eStart;
            e_end       = dh.eEnd;

            prescribed = 0;
            numDof     = dh.numDof;
            dof        = dh.numDof;
            totalDof   = dh.numDof;
            
            GM         = dh.dofs;
            
            if (prescribed==0)
                numPrescribed = 0;
                numFree = numDof;
            else            
                % remove empty (=prescribed) elements from M
                isPrescribed = true(dof,1);
                for e=e_start:e_end
                    isPrescribed( GM{e} ) = prescribed{e};
                end
                numPrescribed = nnz(isPrescribed);
                numFree = dof-numPrescribed;
            end
            
            %fprintf('There are %d free DOFs\n',numFree);

            if (prescribed==0)
                isPresent   = false(totalDof,1);
                GMnew       = zeros(totalDof,1);
                
                for e=e_start:e_end
                    isPresent( GM{e} ) = true;
                end
                GMnew( isPresent ) = 1:dof;
                for e=e_start:e_end
                    GM{e} = GMnew( GM{e}(:) );
                end
            else
                GMnew=zeros(dof,1);
                GMprescribed=(1:numFree)';
                GMnew(~isPrescribed)=GMprescribed;
                for e=e_start:e_end
                    GM{e} = GMnew( GM{e}(~prescribed{e}) );
                end
            end
            
            s.A = zeros(numFree,numFree);
            s.f = zeros(numFree,1);
            
            s.Ke = Ke;
            s.Ge = Ge;
            
            s.dof = dof;
            s.GM = GM;
            
            s.makeSymK();
            s.createA();
            s.createf();

            s.x = s.solveDirect();
            
%             s.createM();
%             s.deflation(0);            
%             [s.x2,flag1,rr1,iter1,rv1] = pcg(s.A,s.f,1e-10,1000,s.M);            
%             s.x2 = s.solveDeflatedCG();

            x_out = zeros(dof,1);
            
            if (prescribed==0)
                x_out = s.x;
            else
                x_out(isPrescribed) = x_init(isPrescribed);
                x_out(~isPrescribed) = s.x;
            end
            
            s.x = x_out;
        end 
        
        function createA(s)
            for e=1:length(s.Ke)
                ind = s.GM{e}(:);
                s.A(ind,ind) = s.A(ind,ind) + s.Ke{e};
            end
            % If only the upper part of A has been set before, A is not
            % symmetric yet. Below the correct upper part is used to create
            % a full symmetric matrix A.
            if (~issymmetric(s.A))
                if (istriu(s.A))
                    s.A = triu(s.A,0)+triu(s.A,1)';
                else
                    if ( max(max(abs(s.A'-s.A))) > 1e-10 )
                        disp('error in Solver.createA: the matrix is not upper triangular, and neither symmetric')
                    end
                end
            end
        end
        
        function createf(s)
            for e=1:length(s.Ge)
                ind = s.GM{e}(:);
                s.f(ind,1) = s.f(ind,1) + s.Ge{e};
            end
        end
        
        function makeSymK(s)
            for e=1:length(s.Ke)
                if (~issymmetric(s.Ke{e}))
                    if (istriu(s.Ke{e}))
                        s.Ke{e} = triu(s.Ke{e},0)+triu(s.Ke{e},1)';
                    elseif (istril(s.Ke{e}))
                        s.Ke{e} = tril(s.Ke{e},0)+triu(s.Ke{e}',1);
                    else
                        if ( max(max(abs(s.Ke{e}'-s.Ke{e}))) > 1e-10 )
                            disp('error in Solver.makeSymK: the matrix is not upper or lower triangular, but neither symmetric already')
                        end                        
                    end
                end 
            end
        end
        
        function deflation(s,n)
            if n>0
                s.createZ(n);
                s.createE();
                s.createQ();
                s.createP();
            else
                s.P = sparse( eye(size(s.A,1)) );
            end
        end
        
        function createM(s)
            s.M = sparse(diag(diag(s.A)));
        end
        
        function createE(s)
%             s.E = zeros(size(s.Z,2),size(s.Z,2));
            s.E = s.Z' * s.A * s.Z;
        end
        
        function createQ(s)
%             s.E = zeros(size(s.Z,2),size(s.Z,2));
%             s.Q = s.Z * inv(s.E) * s.Z';
            s.Q = s.Z * ( s.E \ s.Z');
        end
        
        function createP(s)
            s.P = eye(size(s.A,1)) - s.A*s.Q;
        end
            
        function createZ(s,dmax)
            Nvar = 3;
%             dmax = 5;
            
            s.Z = zeros(size(s.A,1),Nvar*length(s.Ke)*dmax);
            
            ind = 0;
                        
            for e=1:length(s.Ke)
                GM = s.GM{e};
                sizeGM = size(GM,1);
                for v=1:Nvar
                    for d=1:dmax
                        ind = ind+1;
                        
                        dStart = (d-1)*sizeGM/dmax+1;
                        dEnd   = (d  )*sizeGM/dmax;
                        
                        s.Z(GM(dStart:dEnd,v),ind) = 1;
                    end
                end
            end
            
            scaling = sum(s.Z,2);
            s.Z = (1.0./scaling) .* s.Z;
            
            s.Z = sparse(s.Z);
            
            fprintf('Number of deflation vectors : %d\n',size(s.Z,2))
        end
        
        function x = solveDirect(s)
            tic
            x = s.A \ s.f;
            toc
            
            % sparse
            tic
            As = sparse(s.A);
            x = As \ s.f;
            toc
        end
        
        function x = solveDeflatedCG(s)
            epsilon = 1e-10;
            maxiter = 10000;
            
            tol2 = epsilon*epsilon;
            
%             s.p = symrcm(s.A(1:1190,1:1190));
%             s.p = [s.p s.p+1190 s.p+2*1190];
            s.p = [1:size(s.A,1)];
            
            s.A = s.A(s.p,s.p);
            s.A = sparse(s.A);
            
            x = zeros(s.dof,1);
            b = s.f(s.p);
            
            r = b - s.A*x;
            
            r = s.P*r;
            y = s.M\r;
            ry = dot(r,y);
            
            p = y;
            
            norm_res2_0 = dot(r,r);
            
            if (norm_res2_0 <= tol2)
                norm_res2 = norm_res2_0;
            end
            
            for i=1:maxiter
                w = s.A * p;
                w = s.P * w;
                pw = dot(p,w);
                
                alpha = ry / pw;
                x = x+alpha*p;
                r_old = 0*r;
                r = r-alpha*w;
                                
                norm_res2 = dot(r,r) / norm_res2_0;
                
                if (mod(i,500)==0)
                    fprintf('iter %5d : %e  %e\n',i,sqrt(norm_res2),sqrt(norm_res2*norm_res2_0))
                end
                
                if (norm_res2<=tol2)
                    fprintf('iter %5d : %e  %e\n',i,sqrt(norm_res2),sqrt(norm_res2*norm_res2_0))
                    break;
                end
                
                ry_old = ry;
                
                y = s.M\r;
                ry = dot(r-r_old,y);
                
                beta = ry/ry_old;
                
                p = beta*p+y;
            end
        end
        
        % Reverse Cuthill-McKee ordering
        function plotRCM(s)
            
            % Plot 1
            subplot(3,2,1)
            spy(s.A)
            title('original A')
            
            % Plot 2
            s.p = symrcm(s.A);
            subplot(3,2,2)
            spy(s.A(s.p,s.p))
            title('A with Reverse Cuthill-McKee ordering')
            
            % Output information
            [lower,upper] = bandwidth(s.A);
            fprintf('Bandwidth of original matrix     : %d\n',upper);
            [lower,upper] = bandwidth(s.A(s.p,s.p));
            fprintf('Bandwidth of RCM adjusted matrix : %d\n',upper);
            
            % Plot 3
            fprintf('\n')
            if (s.ev1==0)
                fprintf('Working on eig(A), please wait...\n')
%                 s.ev1 = eig(s.A);    % Get the eigenvalues of A
%                 s.ev1 = sort(abs(s.ev1));
            end
            subplot(3,2,3)
            semilogy([1:(size(s.A,1))],abs(s.ev1)) %   Plot real and imaginary parts
            xlabel('Real')
            ylabel('Imaginary')
            title(['Eigenvalues of a A with size ' num2str(size(s.A,1)) ' x ' num2str(size(s.A,1))])
            sa = svd(s.A);
            fprintf('Condition number    = %e\n',sa(1)/sa(end));

            % Plot 4: preconditioned A (NB: cond is used since MA is NOT normal: MA'*MA ~= MA*MA')
            fprintf('\n')
            if (s.ev2==0)
                s.M = diag(1./diag(s.A));
                MA = s.M*s.A;
                fprintf('Working on eig(MA), please wait...\n')
%                 s.ev2 = eig(MA);    % Get the eigenvalues of A
%                 s.ev2 = sort(abs(s.ev2));
            end
            subplot(3,2,4)
            semilogy([1:(size(MA,1))],abs(s.ev2)) %   Plot real and imaginary parts
            xlabel('Real')
            ylabel('Imaginary')
            title(['Eigenvalues of a A with size ' num2str(size(MA,1)) ' x ' num2str(size(MA,1))])
            
%             fprintf('Largest eigenvalue  = %e\n',max(abs(s.ev2)))
%             fprintf('Smallest eigenvalue = %e\n',min(abs(s.ev2)))
%             fprintf('Condition number    = %e\n',max(abs(s.ev2))/min(abs(s.ev2)))
%             fprintf('Condition number    = %e\n',cond(MA));
            sma = svd(MA);
            fprintf('Condition number    = %e\n',sma(1)/sma(end-1));
            
            % Plot 4: deflated A (NB: cond is used since MA is NOT normal: MA'*MA ~= MA*MA')
            fprintf('\n')
            if (s.ev3==0)
                MPA = diag(1./diag(s.A))*s.P*s.A;
                fprintf('Working on eig(MPA), please wait...\n')
%                 s.ev3 = eig(MPA);    % Get the eigenvalues of A
%                 s.ev3 = sort(abs(s.ev3));
            end
            subplot(3,2,5)
            semilogy([1:(size(MPA,1))],abs(s.ev3)) %   Plot real and imaginary parts
            xlabel('Real')
            ylabel('Imaginary')
            title(['Eigenvalues of a A with size ' num2str(size(MPA,1)) ' x ' num2str(size(MPA,1))])
            
%             fprintf('Largest eigenvalue  = %e\n',max(abs(s.ev3)))
%             fprintf('Smallest eigenvalue = %e\n',min(abs(s.ev3)))
            smpa = svd(MPA);
            fprintf('Condition number    = %e\n',smpa(1)/smpa(end-1-size(s.Z,2)));  % here cond() does not work anymore, as the last N singular values are all zero by deflation

        end
    end
    
end

