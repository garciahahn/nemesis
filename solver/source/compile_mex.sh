#!/bin/sh
cd "$(dirname "$0")"
rm *.mexa64 *.mexmaci64

# Linking with Intel MKL (add the library directory to the LD_LIBRARY_PATH environment variable!)
# -L/opt/intel/mkl/lib/intel64_lin/ -lmkl_rt

#mex -g CC=mpicc COMPFLAGS='$COMPFLAGS -O3 -march=native' cgSolver.c -lmwblas
mex -g COMPFLAGS='$COMPFLAGS -O3 -march=native' dsymv.c -lmwblas
mex -g COMPFLAGS='$COMPFLAGS -O3 -march=native' dsymvav-logical.c -lmwblas
mex -g COMPFLAGS='$COMPFLAGS -O3 -march=native' dsymvav.c -lmwblas
mex -g COMPFLAGS='$COMPFLAGS -O3 -Wall -march=native' dsymvavL.c -lmwblas
mex -g COMPFLAGS='$COMPFLAGS -O3 -march=native' dsymvavLelement.c -lmwblas
#mex -g COMPFLAGS='$COMPFLAGS -O3 -march=native' dsym.c -lmwblas
mex -g COMPFLAGS='$COMPFLAGS -O2 -march=native' dsymvL.c -lmwblas
mex -g COMPFLAGS='$COMPFLAGS -O2 -march=native' dsyrkL.c -lmwblas
mex parMex.c CC=mpicc -lmwblas
#mex -g COMPFLAGS='$COMPFLAGS -O3 -Wall -march=native' sparseMatrixMultiply.c -I/opt/intel/mkl_2019.3.199/mkl/include -L/opt/intel/mkl_2019.3.199/mkl/lib/intel64/  -lmkl_rt
#mex -g COMPFLAGS='$COMPFLAGS -O3 -Wall -march=native' dsyrkL.c -I/opt/intel/mkl_2019.3.199/mkl/include -L/opt/intel/mkl_2019.3.199/mkl/lib/intel64/  -lmkl_rt
#mex -g COMPFLAGS='$COMPFLAGS -O3 -Wall -march=native' -v dsyrkL.c  -L${MKLROOT}/lib/intel64 -lmkl_intel_ilp64 -lmkl_sequential -lmkl_core -lpthread -lm -ldl
#mex -g COMPFLAGS='$COMPFLAGS -O3 -Wall -march=native' -v dsyrkL.c  -L${MKLROOT}/lib/intel64 -Wl -lmkl_intel_ilp64 -lmkl_sequential -lmkl_core -lpthread -lm -ldl
#-I/opt/intel/mkl_2019.3.199/mkl/include -L/opt/intel/mkl_2019.3.199/mkl/lib/intel64/ mkl_rt.lib

rm *.o
mv *.mex*64 ../.
