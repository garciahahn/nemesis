/* libraries to be included */

/*
    Definition:
    9 *  ===========
   10 *
   11 *       SUBROUTINE DSYRK(UPLO,TRANS,N,K,ALPHA,A,LDA,BETA,C,LDC)
   12 *
   13 *       .. Scalar Arguments ..
   14 *       DOUBLE PRECISION ALPHA,BETA
   15 *       INTEGER K,LDA,LDC,N
   16 *       CHARACTER TRANS,UPLO
   17 *       ..
   18 *       .. Array Arguments ..
   19 *       DOUBLE PRECISION A(LDA,*),C(LDC,*)
   20 *       ..
*/

#include <stdio.h>   // standard input-output
#include <math.h>    // math library
#include <mex.h>     // mex (Matlab external) stuff 

/*#include </usr/local/MATLAB/R2017a/extern/include/blas.h>*/

#include <blas.h>

/* Handy macro.  Add macros for output parameters here later. */
/* C arrays always begin from position 0 */

#define A_IN prhs[0]   /* first input parameter (right-hand side) */
// #define w_IN prhs[1]   /* second input parameter (right-hand side) */
// #define y_IN prhs[2]
#define C_OUT plhs[0]   /* first (and only) in/output parameter (left-hand side) */

/* This routine calls an LAPACK routine implemented in the Sun Performance Library. 
 * This is DIFFERENT from calling routines from Netlib's CLAPACK.
 * When calling routines from CLAPACK, have to append trailing underscore after the name.
 * This is NOT necessary when calling LAPACK routines from the Sun performance library. 
 * Also, when calling routines from CLAPACK, even scalars that don't return a value have 
 * to be passed by reference.  This is NOT done when calling LAPACK routines from the Sun
 * Performance Library . The reason is that CLAPACK conventions follow Fortran, where   
 * everything is call by reference (no call by value.)  For more info, see p. 36 of
 * Sun Performance Library User's Guide. */

/* To access the Sun Performance Library, it's necessary to use the "cc" compiler on 
 * Solaris (NOT gcc) */

/* We don't need to use a double asterisk indicating a two-dimensional array in this
 * code, because all we are doing is passing a chunk of memory provided by Matlab on
 * to LAPACK - both of them are using the column-by-column storage convention *  
 *-------------------------------------------------------------------*/


/* mex interface function, also written in C */
/* As yet, there are no output parameters! */

void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{ 
   double *A, *C, *w;
   
   double alpha,beta;
   ptrdiff_t K,LDA,LDC,N;
   char uplo,trans;
    
    alpha = 1.0;
    beta = 0.0;
    
    uplo  = 'L';
    trans = 'T';

   /* check number of parameters */
   if (nrhs != 1) {
      mexErrMsgTxt("dsymv.c requires one input argument.");
   } else if (nlhs != 1) {
      mexErrMsgTxt("dsymv.c requires one output parameters.");
   }

   /* convert Matlab arrays into format suitable for the C function lapacktest */
   /* Note: A_IN is defined at top of file to be first input parameter. */
   /* NB: N <= M */
   N = mxGetN(A_IN);    /* column size of input parameter */
   K = mxGetM(A_IN);
   LDA = mxGetM(A_IN);
   LDC = mxGetN(A_IN);
   
   A = mxGetPr(A_IN);   /* pass as one-dimensional array */
//    w = mxGetPr(w_IN);
//    y = mxGetPr(y_IN);
   
   C_OUT = mxCreateDoubleMatrix(LDC,LDC,mxREAL);
   C = mxGetPr(C_OUT);
   
   dsyrk(&uplo,&trans,&N,&K,&alpha,A,&LDA,&beta,C,&LDC);
   
//    y2 = y;

} /* end function mexFunction */

/*-------------------------------------------------------------------*/