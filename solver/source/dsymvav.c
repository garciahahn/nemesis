/* libraries to be included */

#include <stdio.h>   // standard input-output
#include <math.h>    // math library
#include <mex.h>     // mex (Matlab external) stuff 

/*#include </usr/local/MATLAB/R2017a/extern/include/blas.h>*/

#include <blas.h>

/* Handy macro.  Add macros for output parameters here later. */
/* C arrays always begin from position 0 */
#define GM_IN prhs[0]
#define A_IN prhs[1]   /* first input parameter (right-hand side) */
#define D_IN prhs[2]   /* second input parameter (right-hand side) */
#define y_OUT plhs[0]   /* first (and only) in/output parameter (left-hand side) */

/* This routine calls an LAPACK routine implemented in the Sun Performance Library. 
 * This is DIFFERENT from calling routines from Netlib's CLAPACK.
 * When calling routines from CLAPACK, have to append trailing underscore after the name.
 * This is NOT necessary when calling LAPACK routines from the Sun performance library. 
 * Also, when calling routines from CLAPACK, even scalars that don't return a value have 
 * to be passed by reference.  This is NOT done when calling LAPACK routines from the Sun
 * Performance Library . The reason is that CLAPACK conventions follow Fortran, where   
 * everything is call by reference (no call by value.)  For more info, see p. 36 of
 * Sun Performance Library User's Guide. */

/* To access the Sun Performance Library, it's necessary to use the "cc" compiler on 
 * Solaris (NOT gcc) */

/* We don't need to use a double asterisk indicating a two-dimensional array in this
 * code, because all we are doing is passing a chunk of memory provided by Matlab on
 * to LAPACK - both of them are using the column-by-column storage convention *  
 *-------------------------------------------------------------------*/


/* mex interface function, also written in C */
/* As yet, there are no output parameters! */

void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{ 
   mwSignedIndex Dn, n, numElements; /* or: ptrdiff_t n; */
   mwIndex i,j,T,Tm;
   double *A, *D, *x, *y, *x2, *y2;
   int32_T* GM;
   int32_T* GMi;
   mxArray *x_tmp, *y_tmp;
   
   double alpha,beta;
   ptrdiff_t m,incx,incy;
   char s;
    
    alpha = 1.0;
    beta = 0.0;
    incx = 1;
    incy = 1;
    
    s='U';

   /* check number of parameters */
   if (nrhs != 3) {
      mexErrMsgTxt("dsymvav.c requires three input argument.");
   } else if (nlhs != 1) {
      mexErrMsgTxt("dsymvav.c requires one output parameters.");
   }

   /* convert Matlab arrays into format suitable for the C function lapacktest */
   /* Note: A_IN is defined at top of file to be first input parameter. */

   m = mxGetM(A_IN);    /* row size of input parameter */
   n = mxGetN(A_IN);    /* column size of input parameter */
   Dn = mxGetM(D_IN);
   numElements = n/m;
   
   /*
   mexPrintf ("m            = %d.\n",m);
   mexPrintf ("n            = %d.\n",n);
   mexPrintf ("numElements  = %d.\n",numElements);
   mexPrintf ("Dn           = %d.\n",Dn);
   */
   
   A  = mxGetPr(A_IN);   /* pass as one-dimensional array */
   GMi = (int32_T *) mxGetData(GM_IN);
   D  = (double *) mxGetData(D_IN);

   x_tmp = mxCreateDoubleMatrix(1,m,mxREAL);
   x2 = mxGetPr(x_tmp);
   y_tmp = mxCreateDoubleMatrix(1,m,mxREAL);
   y2 = mxGetPr(y_tmp);   
   
   y_OUT = mxCreateDoubleMatrix(Dn,1,mxREAL);
   y = mxGetPr(y_OUT);
   
   /*GM = mxCreateNumericMatrix(m, 1, mxINT32_CLASS, 0);*/
   
   /* for debugging:
      numElements = 1; */
          
   for (i=0;i<numElements;i++) {
        /* mexPrintf ("GM[%d] = %d\n",i,GMi[i]);*/
        /* GM = GMi[i*m:(i+1)*m-1]; */
        T = i*m;
        Tm = T*m;
        for (j=0; j<m; j++) {
            x2[j] = D[GMi[T+j]-1];
            /*x2[j] = D[GM[j]-1];*/
            /* mexPrintf ("x2[%d] = %e / %e\n",j,x2[j],D[j]); */
        }
        dsymv(&s,&m,&alpha,A+Tm,&m,x2,&incx,&beta,y2,&incy);
        for (j=0; j<m; j++) {
            y[GMi[T+j]-1] += y2[j];
            /* mexPrintf ("y2[%d] = %e\n",j,y2[j]); */
            /* mexPrintf ("y[%d] = %e\n",GMi[i*m+j]-1,y[GMi[i*m+j]-1]); */
        }
   }
   
   /* mxDestroyArray(x_tmp); */
   /* mxDestroyArray(y_tmp); */

} /* end function mexFunction */

/*-------------------------------------------------------------------*/