/* libraries to be included */

#include <stdio.h>      // standard input-output
#include <math.h>       // math library
#include <mex.h>        // mex (Matlab external) stuff 
#include <mkl.h>

/*#include </usr/local/MATLAB/R2017a/extern/include/blas.h>*/

//#include <blas.h>

/* Handy macro.  Add macros for output parameters here later. */
/* C arrays always begin from position 0 */
#define A_IN prhs[0]   /* first input parameter (right-hand side) */
#define x_IN prhs[1]   /* second input parameter (right-hand side) */
#define GM_IN prhs[2]
#define y_OUT plhs[0]   /* first (and only) in/output parameter (left-hand side) */

/* This routine calls an LAPACK routine implemented in the Sun Performance Library. 
 * This is DIFFERENT from calling routines from Netlib's CLAPACK.
 * When calling routines from CLAPACK, have to append trailing underscore after the name.
 * This is NOT necessary when calling LAPACK routines from the Sun performance library. 
 * Also, when calling routines from CLAPACK, even scalars that don't return a value have 
 * to be passed by reference.  This is NOT done when calling LAPACK routines from the Sun
 * Performance Library . The reason is that CLAPACK conventions follow Fortran, where   
 * everything is call by reference (no call by value.)  For more info, see p. 36 of
 * Sun Performance Library User's Guide. */

/* To access the Sun Performance Library, it's necessary to use the "cc" compiler on 
 * Solaris (NOT gcc) */

/* We don't need to use a double asterisk indicating a two-dimensional array in this
 * code, because all we are doing is passing a chunk of memory provided by Matlab on
 * to LAPACK - both of them are using the column-by-column storage convention *  
  -------------------------------------------------------------------*/


/* 
 * Compute z = alpha*A*x + beta*y 
 *
 */
void dsymvElement( char s, ptrdiff_t elementDof, double alpha, const mxArray* A, const double* x, double beta, double* y ) {
//void dsymvElement( char s, int numElements, ptrdiff_t elementDof, ptrdiff_t totalDof, double alpha, double* A, double* x, double beta, double* y, double *z, int* GM, double* x_tmp ) {

    // pointers to matricies that are input from matlab
    double *Adata;

    int i, j, T, Tm;
    ptrdiff_t incx=1, incy=1;
        
    sparse_operation_t operation;
    
    sparse_matrix_t cscA;

    struct matrix_descr descr;

    // status for mkl_sparse operations
    sparse_status_t mkl_status;

    // matrix dimensions and sparse indicies 
    mwIndex nRow, nCol, rowInd, pntrB, pntrE;
    MKL_INT nRowm, nColm, rowIndm, pntrBm, pntrEm, nnz2m;   

    // description of sparse indexing type 
    sparse_index_base_t sparseIndType = SPARSE_INDEX_BASE_ZERO;

    //mexPrintf ("MKL_INT size : %d\n",sizeof(MKL_INT));
    //mexPrintf ("mwIndex size : %d\n",sizeof(mwIndex));

    // Creation of sparse matrices for MKL
    if (beta==0) {

        // Specify the sparse matrix properties
        descr.type = SPARSE_MATRIX_TYPE_SYMMETRIC;   // The matrix is symmetric (only the requested triangle is processed).
        descr.mode = SPARSE_FILL_MODE_LOWER;         // The lower triangular matrix part is processed.
        descr.diag = SPARSE_DIAG_NON_UNIT;           // Diagonal elements might not be equal to one.

        Adata = mxGetPr(A);
        //Adata = (double *)mxGetData(A);

        // dimensions of input matrices
        nRow = mxGetM(A);
        nCol = mxGetN(A);
        
        // get row indicies 
        rowInd = *mxGetIr(A);

        // get pointers B and E
        pntrB = *mxGetJc(A);
        pntrE = *mxGetJc(A)+1;

        // Cast mwIndex to MKL_INT
        //if ( sizeof(MKL_INT) != sizeof(mwIndex) ) {
        nRowm   = (MKL_INT) nRow;
        nColm   = (MKL_INT) nCol;
        rowIndm = (MKL_INT) rowInd;
        pntrBm  = (MKL_INT) pntrB;
        pntrEm  = (MKL_INT) pntrE;
        
        //mexPrintf ("nRowm   : %d\n",nRowm);
        //mexPrintf ("nColm   : %d\n",nColm);

        mkl_status = mkl_sparse_d_create_csc(&cscA, sparseIndType, nRowm, nColm, &pntrBm, &pntrEm, &rowIndm, Adata);

        if (mkl_status!=0) {
            mexPrintf ("Error with mkl_sparse_d_create_csc : status =  %d\n",mkl_status);
        }

        operation = SPARSE_OPERATION_NON_TRANSPOSE;

        //mexPrintf ("x(3) = %f.\n",x[3]);

        // Compute y = A * x
        //mkl_status = mkl_sparse_d_mv(operation,alpha,cscA,descr,x,beta,y);
        mkl_status = mkl_sparse_d_mv(operation,1.0,cscA,descr,x,0,y);

         if (mkl_status!=0) {
            mexPrintf ("Error with mkl_sparse_d_mv : status =  %d\n",mkl_status);
         }
      
            mexPrintf ("mkl_sparse done.\n");
            //mexPrintf ("y(2) = %e.\n",*y(2));

 //           for (j=0; j<elementDof; j++) {
 //               z[ GM[T+j]-1 ] += y[ j ];
 //           }    
 //       }

    }
    else {
        /*
        //printf("beta ~= 0\n");
        for (i=0;i<numElements;i++) {
            T = i*elementDof; 
            Tm = T*elementDof;
            for (j=0; j<elementDof; j++) {
                x_tmp[j] = x[ GM[T+j]-1 ];
//                 y_tmp[ j ] = y[ j+i*elementDof ];
            }
            //y_tmp = y+T;  // pointer to first element
//             dsymv(&s,&elementDof,&alpha,A+Tm,&elementDof,x_tmp,&incx,&beta,y_tmp,&incy);
            dsymv(&s,&elementDof,&alpha,A+Tm,&elementDof,x_tmp,&incx,&beta,y+T,&incy);
            for (j=0; j<elementDof; j++) {
                z[ GM[T+j]-1 ] += (y+T)[ j ];
//                 z[ GM[T+j]-1 ] += y_tmp[j];
            }    
        }
        */
    }
}


/* mex interface function, also written in C */
/* As yet, there are no output parameters! */

void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{ 
   mwSignedIndex Dn, m, n, numElements; /* or: ptrdiff_t n; */
   mwIndex i,j,T,Tm;
   double *A, *x, *y, *x2, *y2;
   int32_T* GM;
   int32_T* GMi;
   mxArray *x_tmp, *y_tmp;

   // Cell array variables
   mwSignedIndex numCells;
   const mxArray *cellA;            // complete cell array of A
   const mxArray *cellx;            // complete cell array of x
   const mxArray *cellGM;           // complete cell array of GM

   // Element variables
   mwIndex jcell;                   // cell counter
   const mxArray *elementA;         // A element (one cell)
   const double *elementx;         // x element (one cell)
   const mxArray *elementGM;        // GM element (one cell)

   mwSignedIndex numDof1, numDof2, elementDof;

   double alpha,beta;
   ptrdiff_t incx,incy;
   char s;
    
    alpha = 1.0;
    beta = 0.0;
    incx = 1;
    incy = 1;
    
    s='L';

   /* check number of parameters */
   if (nrhs != 3) {
      mexErrMsgTxt("dsymvavL_sparse.c requires three input argument.");
   } else if (nlhs != 1) {
      mexErrMsgTxt("dsymvav_sparse.c requires one output parameters.");
   }

   /* Note: A_IN and GM_IN are defined at top of this file. */
   // get the cell array from the input
   cellA  = A_IN;
   cellx  = x_IN;
   cellGM = GM_IN;

   numCells = mxGetM(A_IN);
   mexPrintf ("Number of cells = %d.\n",numCells);

   // Loop over the cell array
   for (jcell=0; jcell<numCells; jcell++) {

      // Extract single cell, which is the sparse array of one element
      elementA  = mxGetCell(cellA ,jcell);
      elementx  = (double *) mxGetPr( mxGetCell(cellx ,jcell) );
      elementGM = mxGetCell(cellGM,jcell);
      
      numDof1 = mxGetM(elementGM);    /* row size of input parameter */
      //numDof2 = mxGetN(elementGM);    /* row size of input parameter */
   
      //mexPrintf ("x(2) = %f.\n",elementx[2]);
      //mexPrintf ("n            = %d.\n",numDof2);

      y_tmp = mxCreateDoubleMatrix(1,numDof1,mxREAL);
      y = mxGetPr(y_tmp);

      // Reset to zero
      for (j=0; j<numDof1; j++) {
    	y[ j ] = 0;
      }

      // Sparse matrix vector multiplication
      dsymvElement(s,numDof1,1.0,elementA,elementx,0.0,y);

      //mexPrintf ("y(1) = %e.\n",y(1));
      //mexPrintf ("y(2) = %e.\n",y(2));
      /*
      T = jcell*numDof1; 
      Tm = T*numDof1;
      for (i=0; i<numDof1; i++) {
        b[i] = x[ elementGM[T+i]-1 ];
      }
      */

   }

//   m = mxGetM(A_IN);    /* row size of input parameter */
//   n = mxGetN(A_IN);    /* column size of input parameter */
//   Dn = mxGetM(D_IN);
//   numElements = n/m;
   
   /*
   mexPrintf ("m            = %d.\n",m);
   mexPrintf ("n            = %d.\n",n);
   mexPrintf ("numElements  = %d.\n",numElements);
   mexPrintf ("Dn           = %d.\n",Dn);
   */
/*    
   A  = mxGetPr(A_IN); 
   GMi = (int32_T *) mxGetData(GM_IN);
   D  = (double *) mxGetData(D_IN);

   y_tmp = mxCreateDoubleMatrix(1,m,mxREAL);
   y2 = mxGetPr(y_tmp);   
   
   y_OUT = mxCreateDoubleMatrix(Dn,1,mxREAL);
   y = mxGetPr(y_OUT);
  */ 
   /*GM = mxCreateNumericMatrix(m, 1, mxINT32_CLASS, 0);*/
   
   /* for debugging:
      numElements = 1; */
          
// //    for (i=0;i<numElements;i++) {
// //         /* mexPrintf ("GM[%d] = %d\n",i,GMi[i]);*/
// //         /* GM = GMi[i*m:(i+1)*m-1]; */
// //         T = i*m;
// //         Tm = T*m;
// //         for (j=0; j<m; j++) {
// //             x2[j] = D[GMi[T+j]-1];
// //             /*x2[j] = D[GM[j]-1];*/
// //             /* mexPrintf ("x2[%d] = %e / %e\n",j,x2[j],D[j]); */
// //         }
// //         dsymv(&s,&m,&alpha,A+Tm,&m,x2,&incx,&beta,y2,&incy);
// //         for (j=0; j<m; j++) {
// //             y[GMi[T+j]-1] += y2[j];
// //             /* mexPrintf ("y2[%d] = %e\n",j,y2[j]); */
// //             /* mexPrintf ("y[%d] = %e\n",GMi[i*m+j]-1,y[GMi[i*m+j]-1]); */
// //         }
// //    }
   
//   dsymvElements(s,numElements,m,Dn,1.0,A,D,0.0,x2,y,GMi,y2);
   
   /* mxDestroyArray(x_tmp); */
   /* mxDestroyArray(y_tmp); */

} /* end function mexFunction */
