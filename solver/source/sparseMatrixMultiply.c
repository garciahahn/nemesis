    /*==============================================================
     * sparseMatrixMultiply.c - Example for illustrating how to use 
     *    BLAS within a C MEX-file. sparseMatrixMultiply uses the 
     *    MKL Sparse BLAS function 'mkl_sparse_sp2m'.
     *    
     * Auxillary function calls are also made to:
     *   'mkl_sparse_c_create_csc' and 'mkl_sparse_convert_csr'
     *
     * C = sparseMatrixMultiply(A,B) computes the matrix product of A*B,
     *   where A, B, and C are sparse matrices containing real 64-bit
     *   doubles representing bit-encoded 32-bit complex singles.
     *
     * This is based off of the MATLAB mex routine 'matrixMultiply.c'
     *============================================================*/
    #include "mex.h"
    #include "matrix.h"
    #include "mkl.h"
    //#include "/workdisk/matlab/intel/parallel_studio_xe_2018.3.051/compilers_and_libraries_2018/linux/mkl/include/mkl_spblas.h"
    void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
    {
        /* DEFINE VARIABLES */
        // pointers to matricies that are input from matlab
        MKL_Complex8 *A, *B, *C; 
//        mxArray *C;
        // pointers to matricies that are used in MKL
        sparse_matrix_t cscA, cscB, cscC;
        // matrix dimensions and sparse indicies 
        mwIndex nRow0, nCol0, nRow1, nCol1, rowInd0, rowInd1, pntrB0, pntrE0, pntrB1, pntrE1;
        MKL_INT nRow0m, nCol0m, nRow1m, nCol1m, rowInd0m, rowInd1m, pntrB0m, pntrE0m, pntrB1m, pntrE1m, nnz2;   
        // form of op(A) & op(B) to use in matrix multiplication
        sparse_operation_t op_T = SPARSE_OPERATION_TRANSPOSE;
        sparse_operation_t op_NT = SPARSE_OPERATION_NON_TRANSPOSE;  
        sparse_operation_t op_CT = SPARSE_OPERATION_CONJUGATE_TRANSPOSE;
        sparse_operation_t opA, opB;
        // description of symmetries in A and B. For now assume general sparse matricies.
        //sparse_matrix_type_t descrA, descrB;
        //descrA = SPARSE_MATRIX_TYPE_GENERAL;
        //descrB = SPARSE_MATRIX_TYPE_GENERAL;
        struct matrix_descr descrA;
        descrA.type = SPARSE_MATRIX_TYPE_GENERAL;

        struct matrix_descr descrB;
        descrB.type = SPARSE_MATRIX_TYPE_GENERAL;

        // description of sparse indexing type 
        sparse_index_base_t sparseIndType = SPARSE_INDEX_BASE_ZERO;
        // description of alrogithm stage namespace
        sparse_request_t sparseStage = SPARSE_STAGE_FULL_MULT;
        // status for mkl_sparse operations
        sparse_status_t mkl_status;
        /* EXTRACT INFO FROM INPUTS */
        // get input matrix values
        A = (MKL_Complex8 *)mxGetData(prhs[0]);  /* first input matrix */
        B = (MKL_Complex8 *)mxGetData(prhs[1]);  /* second input matrix */
        // dimensions of input matrices
        nRow0 = mxGetM(prhs[0]);
        nCol0 = mxGetN(prhs[0]);
        nRow1 = mxGetM(prhs[1]);
        nCol1 = mxGetN(prhs[1]);
        // get row indicies 
        rowInd0 = *mxGetIr(prhs[0]);
        rowInd1 = *mxGetIr(prhs[1]);
        // get pointers B and E
        pntrB0 = *mxGetJc(prhs[0]);
        pntrE0 = *mxGetJc(prhs[0])+1;
        pntrB1 = *mxGetJc(prhs[1]);
        pntrE1 = *mxGetJc(prhs[1])+1;
        // check to make sure that MKL_INT and mxIndex have the same number of bits
        if ( sizeof(MKL_INT) != sizeof(mwIndex) ) {
            mexErrMsgIdAndTxt("", "mxIndex and MKL_INT do not have the same size. A fix for this has not yet been implemented (yet).");
        } else {
            nRow0m = nRow0;
            nRow1m = nRow1;
            nCol0m = nCol0;
            nCol1m = nCol1;
            pntrB0m = pntrB0;
            pntrB1m = pntrB1;
            pntrE0m = pntrE0;
            pntrE1m = pntrE1;
            rowInd0m = rowInd0;
            rowInd1m = rowInd1;
        }
        // check that array sizes are compatable (i.e., nCols0 == nRows1)
        // should the size be ambiguous, prioritize operations with fewer transposes
        if ((nCol0 != nRow1) && (nCol0 != nCol1) && (nRow0 != nRow1) && (nRow0 != nCol1) ) {
            mexErrMsgIdAndTxt("MATLAB:matrixMultiply:matchdims", "Inner dimensions of matrix multiply do not match.");
        } else if (nCol0 == nRow1) {
            opA = op_NT;
            opB = op_NT;
        } else if (nCol0 == nCol1) {
            opA = op_NT;
            opB = op_T;
        } else if (nRow0 == nRow1) {
            opA = op_T;
            opB = op_NT;
        } else {
            opA = op_T;
            opB = op_T;    
        }
        /* CALL MKL's SPARSE BLAS LIBRARY TO IMPLEMENT 2-STEP INSTECTOR-EXECUTOR SPARSE MATRIX MULTIPLY SUBROUTINE */
        // (PART 1) Get handles for use with MKL...This converts things to MKL's internal format
        // This call should (I think) interpret the 64-bit real bit-encoded doubles as 32-bit complex singles   
        // A matrix (1st input) -->  cscA
        mkl_status = mkl_sparse_c_create_csc(&cscA, sparseIndType, nRow0m, nCol0m, &pntrB0m, &pntrE0m, &rowInd0m, A);
        // B matrix (2nd input) -->  cscB
        mkl_status = mkl_sparse_c_create_csc(&cscB, sparseIndType, nRow1m, nCol1m, &pntrB1m, &pntrE1m, &rowInd1m, B);
        // (PART 2) Pass arguments to MKL and implement 2-step inspector-executor mkl_sparse BLAS operation
        // USE COMBINED METHOD "SPARSE_STAGE_FULL_MULT"  -->  cscC
        mkl_status = mkl_sparse_sp2m(opA, descrA, cscA, opB, descrB, cscB, sparseStage, &cscC);  
        /* EXPORT RESULTS */
        // Convert internal representation back to CSC format (by converting to CSR and transposing)  -->  plhs[0]
        mkl_status = mkl_sparse_convert_csr(cscC, op_T, &cscC);   
        // output to be sent to matlab as a mxArray
        plhs[0] = mxCreateSparse(nRow0, nCol1, 0, mxREAL);
        // Convert CSC matrix representation back to sparse mxArray  
//        C = &cscC;
        mxSetData(plhs[0], &cscC);  
    }
