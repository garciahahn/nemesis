/* libraries to be included */

#include <stdio.h>   // standard input-output
#include <math.h>    // math library
#include <mex.h>     // mex (Matlab external) stuff 
#include <mpi.h>
#include <string.h>

/*#include </usr/local/MATLAB/R2017a/extern/include/blas.h>*/

#include <blas.h>

/* Handy macro.  Add macros for output parameters here later. */
/* C arrays always begin from position 0 */
#define A_IN prhs[0]   /* first input parameter (right-hand side) */
#define b_IN prhs[1]   /* second input parameter (right-hand side) */
#define x_IN prhs[2]
#define GM_IN prhs[3]
#define eStart_IN prhs[4]
#define eEnd_IN prhs[5]
#define maxIter_IN prhs[6]
#define epsilon_IN prhs[7]
#define outputStep_IN prhs[8]
#define COMM_WORLD_IN prhs[9]

#define x_OUT plhs[0]   /* first in/output parameter (left-hand side) */
// #define w_OUT plhs[1]
// #define p_OUT plhs[2]
// #define iter_OUT plhs[1]
// #define r_OUT plhs[2]

/* 
 * Compute z = alpha*A*x + beta*y 
 *
 */
void dsymvElements( char s, int numElements, ptrdiff_t elementDof, ptrdiff_t totalDof, double alpha, double* A, double* x, double beta, double* y, double *z, int* GM, double* x_tmp ) {

    int i, j, T, Tm;
    ptrdiff_t incx=1, incy=1;
        
    //printf("allocated\n");
    
    // Reset to zero
    for (j=0; j<totalDof; j++) {
    	z[ j ] = 0;
    }
    
    if (beta==0) {
//         printf ("s           : %c\n",s);
//         printf ("numElements : %d\n",numElements);
//         printf ("elementDof  : %d\n",elementDof);
//         printf ("totalDof    : %d\n",totalDof);
//         printf ("alpha       : %f\n",alpha);
//         printf ("beta        : %f\n",beta);   
        for (i=0;i<numElements;i++) {
            T = i*elementDof; 
            Tm = T*elementDof;
            for (j=0; j<elementDof; j++) {
                x_tmp[j] = x[ GM[T+j]-1 ];
            }
            dsymv(&s,&elementDof,&alpha,A+Tm,&elementDof,x_tmp,&incx,&beta,y,&incy);
            for (j=0; j<elementDof; j++) {
                z[ GM[T+j]-1 ] += y[ j ];
            }    
        }
    }
    else {
        //printf("beta ~= 0\n");
        for (i=0;i<numElements;i++) {
            T = i*elementDof; 
            Tm = T*elementDof;
            for (j=0; j<elementDof; j++) {
                x_tmp[j] = x[ GM[T+j]-1 ];
//                 y_tmp[ j ] = y[ j+i*elementDof ];
            }
            //y_tmp = y+T;  // pointer to first element
//             dsymv(&s,&elementDof,&alpha,A+Tm,&elementDof,x_tmp,&incx,&beta,y_tmp,&incy);
            dsymv(&s,&elementDof,&alpha,A+Tm,&elementDof,x_tmp,&incx,&beta,y+T,&incy);
            for (j=0; j<elementDof; j++) {
                z[ GM[T+j]-1 ] += (y+T)[ j ];
//                 z[ GM[T+j]-1 ] += y_tmp[j];
            }    
        }
    }
}

/* Hadamard product */
void dhad( int n, double* x, double* y, double *z ) {
    int i;    
    for (i=0; i<n; i++) {
        z[i] = x[i] * y[i];
    }
}

/* Inverse vector */
void inverseVector( int n, double* x, double* y) {
    int i;
    for (i=0; i<n; i++) {
        y[ i ] = 1.0 / x[ i ];
    }
}

/* mex interface function, also written in C */
/* As yet, there are no output parameters! */

void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{ 
    int COMM_WORLD;
    int rank, nproc;
    
    double static *A;
    double *b, *x;
    int32_T* GM;
    int eStart, eEnd, maxIter, outputStep;
    double epsilon, residual;
    
    double *Minv, *Mall, *M, *R, *Rall, *Rtemp;
    double *p, *w, *wall;
    
    mwSignedIndex elementDof;
    ptrdiff_t totalDof;
    
    mwSignedIndex Dn, m, n, numElements; /* or: ptrdiff_t n; */
    mwIndex ind,e,i,j,T,Tm;
    double *y, *x2, *y2, *tmp;

    double *x_tmp, *y_tmp, *tmp2;

    int iter;
    
    // Values
    double norm_res2, norm_res2_0, sqrtnorm_res2_0;
    double delta0, delta1, delta_init;
    double tol2;
    double sum;
    
    // Parameters
    double alpha,beta;
    ptrdiff_t incx,incy;
    char s;
    double ONE;
        
    /////////////////////////////////////////////////////////
    
    // Process input
    A           = mxGetPr(A_IN);
    b           = mxGetPr(b_IN);
    x           = mxGetPr(x_IN);
    GM          = (int32_T *) mxGetData(GM_IN);
    eStart      = (int) mxGetScalar(eStart_IN);
    eEnd        = (int) mxGetScalar(eEnd_IN);
    maxIter     = (int) mxGetScalar(maxIter_IN);
    epsilon     = (double) mxGetScalar(epsilon_IN);
    outputStep  = (int) mxGetScalar(outputStep_IN);
    COMM_WORLD  = (int) mxGetScalar(COMM_WORLD_IN);
    
    // Setup variables
    totalDof    = mxGetM(x_IN);
    elementDof  = mxGetM(A_IN);
    numElements = eEnd-eStart+1;
            
    // Create output 
    x_OUT       = mxCreateDoubleMatrix(totalDof,1,mxREAL);
//     w_OUT       = mxCreateDoubleMatrix(totalDof,1,mxREAL);
//     p_OUT       = mxCreateDoubleMatrix(totalDof,1,mxREAL);
    
// 	x           = mxGetPr(x_OUT);
//     iter_OUT    = mxCreateNumericArray(1,1);
//     iter        = *mxGetPr(iter_OUT);
//     r_OUT       = mxCreateDoubleMatrix(1,1,mxREAL);
//     residual    = *mxGetPr(r_OUT);

    // Temporary arrays
    x_tmp = mxGetPr( mxCreateDoubleMatrix(totalDof,1,mxREAL) );
    y_tmp = mxGetPr( mxCreateDoubleMatrix(totalDof,1,mxREAL) );
    
    /////////////////////////////////////////////////////////
    
    // Parameters
    alpha   = 1.0;
    beta    = 0.0;
    incx    = 1;
    incy    = 1;    
    s       = 'L';
    ONE     = 1.0;
   
    tol2 = epsilon*epsilon;
    
    // Parallel setup
    MPI_Comm_size(COMM_WORLD, &nproc);
    MPI_Comm_rank(COMM_WORLD, &rank);
    
//     mexPrintf ("number of procs   = %d\n",nproc);
//     mexPrintf ("rank of this proc = %d\n",rank);
//     mexPrintf ("totalDof          = %d\n",totalDof);
//     mexPrintf ("elementDof        = %d\n",elementDof);
//     mexPrintf ("b elements        = %d\n",mxGetM(b_IN));
//     mexPrintf ("eStart            = %d\n",eStart);
//     mexPrintf ("eEnd              = %d\n",eEnd);
    
    /* Preconditioning */
    M       = mxGetPr( mxCreateDoubleMatrix(totalDof,1,mxREAL) );
    Mall    = mxGetPr( mxCreateDoubleMatrix(totalDof,1,mxREAL) );
    Minv    = mxGetPr( mxCreateDoubleMatrix(totalDof,1,mxREAL) );

    // Set preconditioning matrix M
    for (e=0;e<eEnd-eStart+1;e++) {
        for (ind=0;ind<elementDof;ind++) {
            M[ GM[e*elementDof+ind]-1 ] = M[ GM[e*elementDof+ind]-1 ] + A[ e*elementDof*elementDof+ind*(elementDof+1) ];
        }
    }
   
    // Merge distributed M
    MPI_Allreduce(M,Mall,totalDof,MPI_DOUBLE,MPI_SUM,COMM_WORLD);    
    inverseVector(totalDof,Mall,Minv);
      
    /* Residual */
    R       = mxGetPr( mxCreateDoubleMatrix(totalDof,1,mxREAL) );
    Rall    = mxGetPr( mxCreateDoubleMatrix(totalDof,1,mxREAL) );
        
    dsymvElements(s,numElements,elementDof,totalDof,-1.0,A,x,1.0,b,R,GM,x_tmp);          // R = b - A*x
    
    // Merge distributed R    
    MPI_Allreduce(R,Rall,totalDof,MPI_DOUBLE,MPI_SUM,COMM_WORLD);
    
    // Compute residual norm
    norm_res2_0         = ddot(&totalDof,Rall,&incx,Rall,&incy);
    norm_res2           = norm_res2_0;
    sqrtnorm_res2_0     = sqrt(norm_res2_0);    
//     mexPrintf ("norm_res2_0 = %f\n",norm_res2_0);
    
    /* Allocate arrays */
    p    = mxGetPr( mxCreateDoubleMatrix(totalDof,1,mxREAL) );
    y    = mxGetPr( mxCreateDoubleMatrix(totalDof,1,mxREAL) );
    w    = mxGetPr( mxCreateDoubleMatrix(totalDof,1,mxREAL) );
    wall = mxGetPr( mxCreateDoubleMatrix(totalDof,1,mxREAL) );
    
    if (rank==0) {
        dhad(totalDof,Minv,Rall,y);                     	// y = Minv * R
        dcopy(&totalDof,y,&incx,p,&incy);                   // p = y
        delta0      = ddot(&totalDof,Rall,&incx,y,&incy);   //
        delta_init  = delta0;
        
//         double sum = 0;
//         for (i=0; i<totalDof; i++) {
//             sum = sum + Minv[ i ];
//         }
//         for (i=0; i<10; i++) {
//             mexPrintf ("Minv(%d)  : %f\n",i,Minv[i]);
//         }
//         
//         mexPrintf ("sum(Minv)  : %f\n",sum);
//         mexPrintf ("delta 0 : %f\n",delta0);
//         exit;
    }

    mexPrintf("MEX CG SOLVER STARTED WITH EPSILON = %e\n",epsilon);
    
    for (iter=1; iter<maxIter; iter++) {
        
        MPI_Bcast(p,totalDof,MPI_DOUBLE,0,COMM_WORLD);

        dsymvElements(s,numElements,elementDof,totalDof,ONE,A,p,0.0,y_tmp,w,GM,x_tmp);          // w = A*p        

        MPI_Allreduce(w,wall,totalDof,MPI_DOUBLE,MPI_SUM,COMM_WORLD);

        if (rank==0) {
            alpha = delta0 / ddot(&totalDof,p,&incx,wall,&incy);                            // alpha = (r,y) / (p,w)
            
            daxpy(&totalDof,&alpha,p,&incx,x,&incy);                                        // x = x + alpha*p            
            
            //Rold = 0*R;  % flexible CG (see Wikipedia)
            alpha = -alpha;
            daxpy(&totalDof,&alpha,wall,&incx,Rall,&incy);                                	// R = R - alpha*w
            
            norm_res2 = ddot(&totalDof,Rall,&incx,Rall,&incy);            
            norm_res2 = norm_res2 / norm_res2_0;
            
            if (iter%outputStep==0) {
//                 mexPrintf ("eEnd              = %d.\n",eEnd);
                mexPrintf("          %5d       residual: %e %e %e\n",iter,sqrt(norm_res2),sqrt(norm_res2*norm_res2_0),sqrtnorm_res2_0);
            }
            if (norm_res2 < tol2) {
                mexPrintf("          %5d       residual: %e %e %e\n",iter,sqrt(norm_res2),sqrt(norm_res2*norm_res2_0),sqrtnorm_res2_0);
                break;
//                 fprintf('          %5d       residual: %e %e %e\n',iter,sqrt(norm_res2),sqrt(norm_res2*norm_res2_0),sqrtnorm_res2_0)
            }

            dhad(totalDof,Minv,Rall,y);                                                     // y = Minv.*R
            
            //delta1 = (R-Rold)'*y;
            delta1 = ddot(&totalDof,Rall,&incx,y,&incy);
            beta   = delta1 / delta0;                                                       // beta = (r,y)_new / (r,y)_old
            delta0 = delta1;                                                                // (r,y)_old = (r,y)_new

            dcopy(&totalDof,p,&incx,x_tmp,&incy);                                           // x_tmp = p_old
            dcopy(&totalDof,y,&incx,p,&incy);                                               // p = y
            daxpy(&totalDof,&beta,x_tmp,&incx,p,&incy);                                     // p_new = y + beta*p_old
        }
            
        MPI_Bcast(&norm_res2,1,MPI_DOUBLE,0,COMM_WORLD);

        if (norm_res2 < tol2) {
            break;
        }
            
//             temp2 = toc;
//             t(6) = t(6) + temp2-temp1;
//         else
// //             % newStyle
// //             temp1 = toc;
// //             
// //             pw = dotProduct(p,w);
// //             
// // %             pw = NMPI_Allreduce(pw,1,'+',Parallel3D.MPI.mpi_comm_world) ; 
// //             
// //             temp2 = toc;
// //             t(5) = t(5) + temp2-temp1;
// //             temp1 = toc;
// //             
// //             alpha = delta0 / (pw);      % delta0 = ry_old
// // 
// //             for e=e_start:e_end 
// //                 x{e} = x{e} + alpha*p{e};
// //                 R{e} = R{e} - alpha*w{e};
// //             end
// // 
// //             norm_res2 = dotProduct(R,R);
// //             norm_res2 = norm_res2 / norm_res2_0;
// //             
// //             if (Parallel3D.MPI.rank==0 && rem(iter,1000)==0)
// //                 fprintf('          %5d       residual: %e %e %e\n',iter,sqrt(norm_res2),sqrt(norm_res2*norm_res2_0),sqrtnorm_res2_0)
// //                 fprintf('          %5d       timings : %f %f %f %f\n',iter,t(3),t(4),t(5),t(6))
// //             end
// //             
// //             if (norm_res2 <= tol2)
// //                 break;
// //             end
// //             
// //             for e=e_start:e_end 
// //                 y{e} = Minv{e}.*R{e};
// //             end
// //             
// //             delta1 = dotProduct(R,y);              % = ry
// //             
// // %             delta1 = NMPI_Allreduce(delta1,1,'+',Parallel3D.MPI.mpi_comm_world) ; 
// //              
// //             beta   = delta1 / delta0;   % = ry / ry_old
// //             delta0 = delta1;
// // 
// //             for e=e_start:e_end 
// //                 p{e} = y{e} + beta*p{e};
// //             end
// // 
// // %             if rem(iter,500)==0
// // %                 fprintf('          %5d       residual: %e %e\n',iter,delta1,delta_init)
// // %             end
// //             
// //             temp2 = toc;
// //             t(6) = t(6) + temp2-temp1;
//         end
        
    }

    MPI_Bcast(x,totalDof,MPI_DOUBLE,0,COMM_WORLD);
        
//     x_OUT = x;
//     R_tmp = w;
    
    // return outputs from MEX-function
//     plhs[0] = mxCreateDoubleMatrix(1, len, mxREAL);
    
    memcpy(mxGetPr(x_OUT), x, totalDof*sizeof(double));
    
    plhs[1] = mxCreateNumericMatrix(1, 1, mxDOUBLE_CLASS, mxREAL);
    double* dataD = (double*) mxGetData(plhs[1]);
    dataD[0] = sqrt(norm_res2);
    
    plhs[2] = mxCreateNumericMatrix(1, 1, mxINT16_CLASS, mxREAL);
    int* dataI = (int*) mxGetData(plhs[2]);
    dataI[0] = iter;
    

    
    
//     memcpy(mxGetPr(w_OUT), w, totalDof*sizeof(double));
//     memcpy(mxGetPr(p_OUT), p, totalDof*sizeof(double));
    
    
//     memcpy(mxGetPr(plhs[4]), &iter, 1*sizeof(double));
            
//     if (nlhs > 1) {
        
//         int* value = (int*) mxGetPr(plhs[4]);
//         value = &iter;
//         memcpy(mxGetPr(plhs[4]), iter, 1*sizeof(double));
//     }
    
//     mxFree(x_tmp);
    
// // //     
// // //     
// // // 
// // // 
// // //    /* check number of parameters */
// // //    if (nrhs != 3) {
// // //       mexErrMsgTxt("dsymvav.c requires three input argument.");
// // //    } else if (nlhs != 1) {
// // //       mexErrMsgTxt("dsymvav.c requires one output parameters.");
// // //    }
// // // 
// // //    /* convert Matlab arrays into format suitable for the C function lapacktest */
// // //    /* Note: A_IN is defined at top of file to be first input parameter. */
// // // 
// // //    m = mxGetM(A_IN);    /* row size of input parameter */
// // //    n = mxGetN(A_IN);    /* column size of input parameter */
// // //    Dn = mxGetM(D_IN);
// // //    numElements = n/m;
// // //    
// // //    /*
// // //    mexPrintf ("m            = %d.\n",m);
// // //    mexPrintf ("n            = %d.\n",n);
// // //    mexPrintf ("numElements  = %d.\n",numElements);
// // //    mexPrintf ("Dn           = %d.\n",Dn);
// // //    */
// // //    
// // //    A  = mxGetPr(A_IN);   /* pass as one-dimensional array */
// // //    GMi = (int32_T *) mxGetData(GM_IN);
// // //    D  = mxGetPr(D_IN);
// // // 
// // //    x_tmp = mxCreateDoubleMatrix(1,m,mxREAL);
// // //    x2 = mxGetPr(x_tmp);
// // //    y_tmp = mxCreateDoubleMatrix(1,m,mxREAL);
// // //    y2 = mxGetPr(y_tmp);   
// // //    
// // //    y_OUT = mxCreateDoubleMatrix(Dn,1,mxREAL);
// // //    y = mxGetPr(y_OUT);
// // //    
// // //    /*GM = mxCreateNumericMatrix(m, 1, mxINT32_CLASS, 0);*/
// // //    
// // //    /* for debugging:
// // //    /* numElements = 1; */
// // //           
// // //    for (i=0;i<numElements;i++) {
// // //         /* mexPrintf ("GM[%d] = %d\n",i,GMi[i]);*/
// // //         /* GM = GMi[i*m:(i+1)*m-1]; */
// // //         T = i*m;
// // //         Tm = T*m;
// // //         for (j=0; j<m; j++) {
// // //             x2[j] = D[GMi[T+j]-1];
// // //             /*x2[j] = D[GM[j]-1];*/
// // //             /* mexPrintf ("x2[%d] = %e / %e\n",j,x2[j],D[j]); */
// // //         }
// // //         dsymv(&s,&m,&alpha,A+Tm,&m,x2,&incx,&beta,y2,&incy);
// // //         for (j=0; j<m; j++) {
// // //             y[GMi[T+j]-1] += y2[j];
// // //             /* mexPrintf ("y2[%d] = %e\n",j,y2[j]); */
// // //             /* mexPrintf ("y[%d] = %e\n",GMi[i*m+j]-1,y[GMi[i*m+j]-1]); */
// // //         }
// // //    }
   
   /* mxDestroyArray(x_tmp); */
   /* mxDestroyArray(y_tmp); */

} /* end function mexFunction */



/*-------------------------------------------------------------------*/