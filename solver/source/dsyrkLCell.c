/* libraries to be included */

/*
    Definition:
    9 *  ===========
   10 *
   11 *       SUBROUTINE DSYRK(UPLO,TRANS,N,K,ALPHA,A,LDA,BETA,C,LDC)
   12 *
   13 *       .. Scalar Arguments ..
   14 *       DOUBLE PRECISION ALPHA,BETA
   15 *       INTEGER K,LDA,LDC,N
   16 *       CHARACTER TRANS,UPLO
   17 *       ..
   18 *       .. Array Arguments ..
   19 *       DOUBLE PRECISION A(LDA,*),C(LDC,*)
   20 *       ..
*/

#include <stdio.h>   // standard input-output
#include <math.h>    // math library
#include <mex.h>     // mex (Matlab external) stuff 

/*#include </usr/local/MATLAB/R2017a/extern/include/blas.h>*/

#include <blas.h>

/* Handy macro.  Add macros for output parameters here later. */
/* C arrays always begin from position 0 */

#define CELL_IN prhs[0]   /* first input parameter (right-hand side) */
// #define w_IN prhs[1]   /* second input parameter (right-hand side) */
// #define y_IN prhs[2]
#define CELL_OUT plhs[0]   /* first (and only) in/output parameter (left-hand side) */

/* This routine calls an LAPACK routine implemented in the Sun Performance Library. 
 * This is DIFFERENT from calling routines from Netlib's CLAPACK.
 * When calling routines from CLAPACK, have to append trailing underscore after the name.
 * This is NOT necessary when calling LAPACK routines from the Sun performance library. 
 * Also, when calling routines from CLAPACK, even scalars that don't return a value have 
 * to be passed by reference.  This is NOT done when calling LAPACK routines from the Sun
 * Performance Library . The reason is that CLAPACK conventions follow Fortran, where   
 * everything is call by reference (no call by value.)  For more info, see p. 36 of
 * Sun Performance Library User's Guide. */

/* To access the Sun Performance Library, it's necessary to use the "cc" compiler on 
 * Solaris (NOT gcc) */

/* We don't need to use a double asterisk indicating a two-dimensional array in this
 * code, because all we are doing is passing a chunk of memory provided by Matlab on
 * to LAPACK - both of them are using the column-by-column storage convention *  
 *-------------------------------------------------------------------*/


/* mex interface function, also written in C */
/* As yet, there are no output parameters! */

void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{ 
   const mwSize *dims; 
   double *Adata, *C, *w;
   mxArray *temp;
   double alpha,beta;
   ptrdiff_t K,LDA,LDC,N;
   char uplo,trans;
    
    alpha = 1.0;
    beta = 0.0;
    
    uplo  = 'L';
    trans = 'T';

   /* check number of parameters */
   if (nrhs != 1) {
      mexErrMsgTxt("dsymv.c requires one input argument.");
   } else if (nlhs != 1) {
      mexErrMsgTxt("dsymv.c requires one output parameters.");
   }

   dims = mxGetDimensions(CELL_IN);
    
   mxArray *CellArray_ptr;
   CellArray_ptr = mxCreateCellMatrix( dims[0], 1 );  
   
   mxArray *A;
   
   
   for( mwIndex i=0; i<dims[0]; i++ )
   {	   
       A = mxGetCell(CELL_IN, i);
       
       /* convert Matlab arrays into format suitable for the C function lapacktest */
       /* Note: A_IN is defined at top of file to be first input parameter. */
       /* NB: N <= M */
       N = mxGetN(A);    /* column size of input parameter */
       K = mxGetM(A);
       LDA = mxGetM(A);
       LDC = mxGetN(A);
//        
//        mexErrMsgTxt("we are here 1");
//        
       Adata = mxGetPr(A);
   
       temp = mxCreateDoubleMatrix(LDC,LDC,mxREAL);
       
       C = mxGetPr(temp);
      
       dsyrk(&uplo,&trans,&N,&K,&alpha,Adata,&LDA,&beta,C,&LDC);
   
       mxSetCell( CellArray_ptr, i, temp );
   }
   
   CELL_OUT = CellArray_ptr;
   
//    y2 = y;

} /* end function mexFunction */

/*-------------------------------------------------------------------*/