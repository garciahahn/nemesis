/* libraries to be included */

#include <stdio.h>   // standard input-output
#include <math.h>    // math library
#include <mex.h>     // mex (Matlab external) stuff 

/*#include </usr/local/MATLAB/R2017a/extern/include/blas.h>*/

#include <blas.h>

/* Handy macro.  Add macros for output parameters here later. */
/* C arrays always begin from position 0 */
#define GM_IN prhs[0]
#define K_IN prhs[1]   /* first input parameter (right-hand side) */
#define D_IN prhs[2]   /* second input parameter (right-hand side) */
#define y_OUT plhs[0]   /* first (and only) in/output parameter (left-hand side) */

/* This routine calls an LAPACK routine implemented in the Sun Performance Library. 
 * This is DIFFERENT from calling routines from Netlib's CLAPACK.
 * When calling routines from CLAPACK, have to append trailing underscore after the name.
 * This is NOT necessary when calling LAPACK routines from the Sun performance library. 
 * Also, when calling routines from CLAPACK, even scalars that don't return a value have 
 * to be passed by reference.  This is NOT done when calling LAPACK routines from the Sun
 * Performance Library . The reason is that CLAPACK conventions follow Fortran, where   
 * everything is call by reference (no call by value.)  For more info, see p. 36 of
 * Sun Performance Library User's Guide. */

/* To access the Sun Performance Library, it's necessary to use the "cc" compiler on 
 * Solaris (NOT gcc) */

/* We don't need to use a double asterisk indicating a two-dimensional array in this
 * code, because all we are doing is passing a chunk of memory provided by Matlab on
 * to LAPACK - both of them are using the column-by-column storage convention *  
/*-------------------------------------------------------------------*/


/* mex interface function, also written in C */
/* As yet, there are no output parameters! */

void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{ 
    const mwSize *dims; 
    mwSignedIndex dof, m, n, numElements; /* or: ptrdiff_t n; */
    mwIndex i,j; //,T,Tm;
    double *D, *x, *y, *y2;
    
    double *x2;
    
    double temp1, temp2;

    mxArray *K; 
    const double *Ke;
    
    mwIndex *loc;
    
    mxArray *GM; 
    //mxLogical *GM;
    
    mwIndex e;
    
    mxArray *x_tmp, *y_tmp;

    double alpha,beta;
    ptrdiff_t incx,incy;
    char s;
    
    s       = 'U';
    alpha   = 1.0;
    beta    = 0.0;
    incx    = 1;
    incy    = 1;
    
    /* Set m */
    K       = mxGetCell(K_IN,1);
  	Ke      = mxGetPr(K);
    m       = mxGetM(K);
    
    /* Set vector */
    D  = (double *) mxGetData(D_IN);
    
    /* Set numDOF */
    dof     = mxGetM(D_IN);

   /* check number of parameters */
//    if (nrhs != 3) {
//       mexPrintf ("nrhs = %d.\n",nrhs);
//       mexErrMsgTxt("dsymvav.c requires three input arguments.");
//    } else if (nlhs != 1) {
//       mexErrMsgTxt("dsymvav.c requires one output parameter.");
//    }

//     K = K_IN;   /* pass as one-dimensional array */
    
//     GMi = (mxLogical *) mxGetLogicals(GM_IN);
    
    dims    = mxGetDimensions(K_IN);

    x_tmp = mxCreateDoubleMatrix(1,m,mxREAL);
    x2 = mxGetPr(x_tmp);
    y_tmp = mxCreateDoubleMatrix(1,m,mxREAL);
    y2 = mxGetPr(y_tmp);
    y_OUT = mxCreateDoubleMatrix(dof,1,mxREAL);
    y = mxGetPr(y_OUT);
    
    for (e=0; e<dims[0]; e++) {
//     for (e=0; e<1; e++) {
        K   = mxGetCell(K_IN,e);
        Ke  = mxGetPr(K);
        
        GM  = mxGetCell(GM_IN,e);
        
//         if (e==0) {
//             m = mxGetM(K);    /* row size of input parameter */
//             n = mxGetN(K);    /* column size of input parameter */
//             mexPrintf("Number of elements : %d\n",dims[0]);
//             mexPrintf("m x n = %d x %d\n", m, n);
//             if (mxIsSparse (GM2))
//                 mexPrintf("GM is sparse!\n");
//             mexPrintf("K(1,1) = %f\n",Ke[0]);
// //             mexPrintf("K(1,1) = %f\n",Ke[1,1]);
// //             mexPrintf("K(1,1) = %f\n",K[1,1]);    
//         }
                
        loc = mxGetIr(GM);
        
//         if (e==0) {
//             for (j=0; j<m; j++) {
//                 mexPrintf("loc(%d) = %d %f\n",j,loc[j],Ke[0+m+1]); 
//             }
//         }
        
        
        for (j=0; j<m; j++) {
            x2[j] = D[loc[j]];
        }
    
        dsymv(&s,&m,&alpha,Ke,&m,x2,&incx,&beta,y2,&incy);
        
        /*
        Form  y  when A is stored in upper triangle.
         */
//         for (j=0; j<m; j++) {
//             temp1 = x2[j];
//             temp2 = 0;
//             
//             for (i=0; i<j; i++) {
//                 y2[i] = y2(i) + temp1*K[i,j];
//                 temp2 = temp2 + K[i,j]*x2[i];
//             }
//             y2[j] = y2[j] + temp1*K[j,j] + temp2;
//         }

        for (j=0; j<m; j++) {
            y[loc[j]] += y2[j];
        }        
    }    
//     mxDestroyArray(x_tmp);
//     mxDestroyArray(y_tmp);
}
    
//     m = mxGetM(GM2);
//     m = 100;
    
//     loc = mxGetIr(GM2);
    
//     for (e=0; e<m; e++) {
//         mexPrintf("GM(%d) = %d\n",e,loc[e]);
//     }
    
    
    
//     mexPrintf("nnz(GM) = %d\n",nnz(GM));
    
    
// 

//     Dn = mxGetM(D_IN);

/*
   /*
   dims = mxGetDimensions(prhs[0]);
   numElements = dims[1];
*/
   /*
   mexPrintf ("m            = %d.\n",m);
   mexPrintf ("n            = %d.\n",n);
   mexPrintf ("numElements  = %d.\n",numElements);
   mexPrintf ("Dn size      = %d.\n",Dn);
   /*
/*   Kcells = A_IN;*/
   
   
   /*
    * GMi = (int32_T *) mxGetData(GM_IN);
    
   GMi = (mxLogical *) mxGetLogicals(GM_IN);

   
   dims = mxGetDimensions(prhs[0]);
   
   for (e=0; e<dims[1]; e++) {
        Ke = mxGetCell(K,e);
        K = mxGetPr(Kcell)
        mexPrintf("m x n = %d x %d\n", mxGetM(Ke), mxGetN(Ke));
   }

   numElements = mxGetM(A_IN);
   m = size(A{1},1);
   n = size(A{1},2);
   */

   /*
   if (mxIsSparse (GM_IN))
    mexErrMsgTxt ("GM is sparse!");
   
   
   D  = (double *) mxGetData(D_IN);

   x_tmp = mxCreateDoubleMatrix(1,m,mxREAL);
   x2 = mxGetPr(x_tmp);
   y_tmp = mxCreateDoubleMatrix(1,m,mxREAL);
   y2 = mxGetPr(y_tmp);   
   
   y_OUT = mxCreateDoubleMatrix(Dn,1,mxREAL);
   y = mxGetPr(y_OUT);
   
   /*GM = mxCreateNumericMatrix(m, 1, mxINT32_CLASS, 0);*/
   
   /* for debugging:
   /* numElements = 1; 
          
   for (i=0;i<numElements;i++) {
        /* mexPrintf ("GM[%d] = %d\n",i,GMi[i]);*/
        /* GM = GMi[i*m:(i+1)*m-1]; */
     //   T = i*m;        // elementNr * numElements
      //  Tm = T*m;
        /*for (j=0; j<m; j++) {*/
       // for (j=0; j<Dn; j++) {
         //   x2[j] = D[GMi[T+j]-1];
            /*x2[j] = D[GM[j]-1];*/
            /* mexPrintf ("x2[%d] = %e / %e\n",j,x2[j],D[j]); */
      //  }
        /*dsymv(&s,&m,&alpha,Kcells+Tm,&m,x2,&incx,&beta,y2,&incy);*/
        //for (j=0; j<m; j++) {
          //  y[GMi[T+j]-1] += y2[j];
            /* mexPrintf ("y2[%d] = %e\n",j,y2[j]); */
            /* mexPrintf ("y[%d] = %e\n",GMi[i*m+j]-1,y[GMi[i*m+j]-1]); */
   //     }
   //}
   
   /* mxDestroyArray(x_tmp); */
   /* mxDestroyArray(y_tmp); */

/*}  end function mexFunction */
