classdef DefaultSettings < matlab.mixin.SetGet 
    
    properties(Constant, Access=private)
        defaultFilename = 'Settings';
        fileError       = true;         % throw an error if the settings file is not found (false = load DefaultSettings)
    end
    
    % Make sure only the 'settings' class will modify the values
    properties% (SetAccess=protected)
        title;
        phys;
        time;
        mesh;
        stde;
        comp;
        outp;
        anal;
    end

    methods
        % Constructor with default values
        function s = DefaultSettings()
            
            s.phys.group                    = 'PHYSICS';
            s.phys.system                   = '';                               % Systems of equation to solve (physics class names)
            s.phys.f                        = 2;                                % Used by the poisson equation
            s.phys.L0                       = 1;
            s.phys.alpha                    = 6*sqrt(2);                        % Alpha value used in the Cahn-Hilliard equation
            s.phys.Cn                       = 1;                                % Cahn number
            s.phys.Fr                       = 1;                                % Froude number: Fr^2 = U0^2 / g*L0
            s.phys.M                        = 1;                                % Interface mobility
            s.phys.Pe                       = 100;                              % Peclet number
            s.phys.Pr                       = 1;                                % Prandtl number
            s.phys.Ja                       = 1;                                % Jacob number
            s.phys.Re                       = 1;                               % Reynolds number: Re = rho1 * U0 * L0 / mu1;
            s.phys.We                       = 0;                                % Weber number: We = rho1 * U0^2 * L0 / sigma
            %s.phys.Bo                       = 2.5537E-03;
            s.phys.sigma                    = 1;
            s.phys.Ca                       = 0;                                % Capillary number (= We / Re)
            s.phys.Oh                       = 0;                                % Ohnesorge number (= sqrt(We) / Re)
            s.phys.g                        = [0,0,0];                          % Gravity vector [x,y,z]
            s.phys.rho                      = [ 1, 1 ];
            s.phys.mu                       = [ 1, 1 ];
            s.phys.ContactAngle             = 90;                               % 0 degrees: hydrophillic, 180 degrees: hydrophobic
            s.phys.Dw                       = 0.0;
            %s.phys.isSteady                 = false;
            s.phys.dynamicReferenceFrame    = false;
            s.phys.dynamicBC                = 0;
            s.phys.radius                   = 1;
            s.phys.linearization            = Linearization.Newton;
%             s.phys.useMeanCurvature         = true;
%             s.phys.sharpRho                 = false;
            
%             s.phys.lamRho                   = s.phys.rho(2) / s.phys.rho(1);
%             s.phys.lamMu                    = s.phys.mu(2)  / s.phys.mu(1);     % 1 in the phase with C=1, lamRho in the phase with C=0
            
            %s.time.steady                   = false;
            s.time.name                     = 'TIME';
            s.time.initial                  = 0.0;
            s.time.final                    = 1.0;
            s.time.step                     = 1;
            s.time.nStep                    = [];
            s.time.nStart                   = 1;
            s.time.nEnd                     = 1;
            s.time.iter                     = 0;
            s.time.current                  = s.time.initial;
            %s.time.spaceTime                = true;                             % time integration method  
            %s.time.steppingMethod           = [];
            s.time.method                   = TimeMethod.SpaceTime;
            
            s.stde.name                     = 'STANDARD ELEMENT';
            %s.stde.sDim                     = 2;                             	% number of spatial dimensions 
            s.stde.Pn                       = [ 4, 4, 4, 0];                    % polynomial order
            s.stde.Q                        = [ 4, 4, 4, 0];                    % Pn+3;             % quadrature order
            s.stde.C                        = [ 1, 1, 1, 0];                    % continuity (polynomial type)
            
            s.mesh.group                    = 'MESH';
            s.mesh.sdim                     = 2;
            s.mesh.dim                      = 0;
            s.mesh.Ne_global                = [ 10, 10, 1, 1];                	% number of elements
            s.mesh.x                        = [-1.0, 1.0 ];
            s.mesh.y                        = [-1.0, 1.0 ];
            s.mesh.z                        = [ 0.0, s.time.step ];
            s.mesh.t                        = s.mesh.z;
            s.mesh.matching                 = false;                             % enforce matching mesh for all physics?
            s.mesh.refinementRatio          = [ 2, 2, 1, 1];
            %s.mesh.maxLevel                 = 0;                                % number of refinement steps, 0 means no AMR
            s.mesh.updateMoment             = [true; false];                    % beginning of each time step / end of each nonlinear step
            s.mesh.uniform                  = true;                             % mesh type
            s.mesh.periodic                 = [false false false];                            % Periodicity in xyz directions
            s.mesh.preventMeshRepetition    = false;                            % OBSOLTETE: If AMR coarsens and refines in every iteration, it may end up in a loop. This is prevented now by only coarsening the first iteration
            s.mesh.coordSystem              = CoordSystem.Cartesian;            % Default: Cartesian coordinate system
            s.mesh.activeMaterial           = Material.All;
            
            s.comp.group                    = 'COMPUTATIONAL';
            s.comp.restart                  = false;
            s.comp.nStart                   = 0;
            s.comp.nStop                    = 100;
            s.comp.nSteps                   = s.comp.nStop - s.comp.nStart;     % Total number of time steps
            s.comp.stabilizeInterface       = false;
            s.comp.meanKappa                = 1;
            s.comp.fixedKappa               = true;
            s.comp.extrapolationOrder       = 1;
%             s.comp.manufacturedSolution     = false;
%             s.comp.energyBalance            = true;
            
            s.comp.nonlinear.enabled        = true;
            s.comp.nonlinear.relaxation     = 0.0;
            s.comp.nonlinear.iter           = 0;
            s.comp.nonlinear.maxIter        = 1;                               % Maximum number of nonlinear iterations
            s.comp.nonlinear.convergence    = 1E-3;
            s.comp.nonlinear.convType       = [true; true];                    % Convergence criteria based on [ value; residual ]
            s.comp.maxNonlinear             = 5;
            
            s.comp.coupling.enabled         = false;
            s.comp.coupling.relaxation      = 0.0;
            s.comp.coupling.iter            = 0;
            s.comp.coupling.maxIter         = 1;                               % Maximum number of coupling iterations
            s.comp.coupling.convergence     = 1E-4;
            s.comp.coupling.convType        = [true; true];                    % Convergence criteria based on [ value; residual ]
            s.comp.maxCoupling              = 5;
            
            s.comp.CG.enabled               = true;                             % use an iterative/direct solver
            s.comp.CG.maxIter               = 5000;
            s.comp.CG.convergence           = 1E-6;
            
            %s.comp.initial.weight           = 100;
            s.comp.boundary.weight          = 100;
            s.comp.boundary.eqWeight        = 1;
            
            s.comp.integration              = [ 12, 12, 12, 0];
            
            s.comp.manufacturedSolution     = false;
            s.comp.exactRHS                 = false;
            s.comp.stabilizeInterface       = false;
            s.comp.energyBalance            = false;
            
            s.comp.CG.variable_convergence  = false;
            
            s.outp.group                    = 'OUTPUT';
            s.outp.directory                = './output/';
            s.outp.vtkDataDir               = 'vtk_data/';
            s.outp.parallelWrite            = true;
            s.outp.solutionFilename         = 'restart';
            s.outp.solutionSave             = true;
            s.outp.solutionStep             = 5;
            s.outp.writeEveryNL             = false;
            s.outp.valueFilename            = 'values';
            s.outp.valueSave                = true;
            s.outp.valueStep                = 1;
            s.outp.energyFilename           = 'energy';
            s.outp.figure.filename          = 'figure';
            s.outp.figureSave               = true;
            s.outp.figureStep               = 5;
            s.outp.error                    = false;
            s.outp.relError                 = false;
            s.outp.mass                     = true;
            s.outp.cm                       = true;
            s.outp.cmv                      = true;
            s.outp.debug                    = false;
            s.outp.verbose                  = false;
            s.outp.filecounter              = 1;
            s.outp.residual_file_is_open    = false;
            
            s.anal.group                    = 'ANALYSIS';
            s.anal.debug                    = false;
            s.anal.profiling                = false;
            s.anal.useCellFun               = false;
        end
        
        function disp(s)
            symbol = ' ';
            width  = 30;
            
            fprintf('%s\n',repmat(symbol,1,width));
            groups = properties(s);
            for i=1:numel(groups)
                %fprintf('%s %-*s %s\n',symbol,width-4,(groups{i}.name),symbol);
                disp(s.(groups{i}))
%                 fprintf('%s ') %(symbol,' ',s.(groups{i}))
                
%                 subGroup  = get(s,groups{i});
%                 variables = fields(subGroup);
%                 for n=1:numel(subGroup)
%                     disp(s.(groups{i}))
%                     fprintf('%s %-*s %s\n',symbol,width-4,subGroup{i},symbol)
%                 end
                
            end
            
%             disp('+ Physics')
%             disp(s.physics)
%             disp('*')
%             disp('* Mesh')
%             if (isempty(s.mesh))
%                 disp('    All default')
%             else
%                 disp(s.mesh)
%             end
        end
   
        % Save a settings object to a file. 
        % Conversion from the object to a struct is required here.
        function settings = save(s,writeToFile)
            
            % Save the data in a struct
            groups = properties(s); 
            for i=1:numel(groups)
                settings.(groups{i}) = get(s,groups{i});
            end
            
            % Write to file if requested
            if (nargin==2 && writeToFile)
                filename = [s.outp.directory DefaultSettings.defaultFilename];
                settings = save(filename,'-struct','settings');
                if (s.outp.verbose && System.rank==0)
                    fprintf('SETTINGS   : Settings have been saved (%s)\n',filename)
                end
            end
        end
        
        function setCurrentTime(s,t)
            s.time.current = t;
        end
        
        function setInitialTime(s,t)
            s.time.initial = t;
        end
        
        % Update the time iteration number and the time level
        function advanceTime(s)
            s.time.iter     = s.time.iter+1;
            s.time.current  = s.time.current + s.time.step;
        end
        
        function setIter(s,type,value)
            switch type
                case 'coupling'
                    s.comp.coupling.iter = value;
                case 'nonlinear'
                    s.comp.nonlinear.iter = value;
                case 'time'
                    s.time.iter = value;
            end
        end
        
        function check(s,verbose)
            if (nargin==1)
                verbose = true;
            end
            
            % Cn > 0
            if (s.phys.Cn <= 0)
                fprintf('Cn should be > 0\n')
                System.terminate(1);
            end
            
            % Capillary number (Ca = We / Re)
            if (s.phys.Ca ~= s.phys.We/s.phys.Re)
                s.phys.Ca = s.phys.We/s.phys.Re;
                %fprintf('Ca is adjusted to ensure that Ca=We/Re holds\n')
            end
            
            % Ohnesorge number (Oh = sqrt(We) / Re)
            if (s.phys.Oh ~= sqrt(s.phys.We)/s.phys.Re)
                s.phys.Oh = sqrt(s.phys.We)/s.phys.Re;
                %fprintf('Oh is adjusted to ensure that Oh=sqrt(We)/Re holds\n')
            end
            
            % Directory needs a final /
            if  ~isempty(s.outp.directory) && ~( strcmp( s.outp.directory(end), '/' ) )
                s.outp.directory = [s.outp.directory '/'];
            end
            
            if (verbose && s.outp.verbose && System.rank==0)
                fprintf('SETTINGS   : All settings are valid\n')
            end
            
            s.phys.lamRho                   = s.phys.rho(2) / s.phys.rho(1);
            s.phys.lamMu                    = s.phys.mu(2)  / s.phys.mu(1);     % 1 in the phase with C=1, lamRho in the phase with C=0
            
            if (s.time.method==TimeMethod.SpaceTime)
                if (s.mesh.sdim==2)
                    s.mesh.z = [ 0.0, s.time.step ];
                    s.mesh.Ne_global(3) = 1;
                elseif (s.mesh.sdim==1)
                   s.mesh.y = [ 0.0, s.time.step ];
                   s.mesh.Ne_global(2) = 1;
                else
                    error('Please use a spatial mesh dimension of 1 or 2 if using space-time')
                end
            end
%             s.mesh.t                        = s.mesh.z;
            
%             if (numel(s.mesh.maxLevel)<4)
%                 s.mesh.maxLevel( numel(s.mesh.maxLevel)+1:4 ) = 1;
%             end
            
            if (numel(s.mesh.refinementRatio)<4)
                s.mesh.refinementRatio( numel(s.mesh.refinementRatio)+1:4 ) = 1;
            end
            
            if (numel(s.mesh.Ne_global)<4)
                s.mesh.Ne_global( numel(s.mesh.Ne_global)+1:4 ) = 1;
            end

%             if (System.rank==0)
%                 fprintf('Physical properties:\n')
%                 fprintf('   Re = %f\n',s.phys.Re);
%                 fprintf('   We = %f\n',s.phys.We);
%                 fprintf('   Oh = %f\n',s.phys.Oh);
%             end
            
            if (s.time.method==TimeMethod.SpaceTime)
                s.mesh.dim = s.mesh.sdim + 1;
            else
                s.mesh.dim = s.mesh.sdim;
            end
            
            if (numel(s.mesh.periodic)==1)
                s.mesh.periodic = repmat( s.mesh.periodic, 1, s.mesh.dim );
            end
            
            s.mesh.refinementRatio( s.mesh.dim+1:end ) = 1;
            
            % Convergence needs to be column vector
            %if (~s.time.steady)
            if (s.time.method~=TimeMethod.Steady)
                s.time.nStart = (s.time.initial / s.time.step) + 1;
                s.time.nEnd   = (s.time.final   / s.time.step);
            else
                s.time.nStart = 1;
                s.time.nEnd   = 1;
            end
        end
        
        function setValue(s,id,value)
            
            C = strsplit(id,'.');
            
            if isnumeric(value)
                valueString = num2str(value);
            end
            
            if (numel(C)==1)
                s.(C{1}) = value;
            elseif (numel(C)==2)
                s.(C{1}).(C{2}) = value;
            elseif (numel(C)==3)
                s.(C{1}).(C{2}).(C{3}) = value;
            elseif (numel(C)==4)
                s.(C{1}).(C{2}).(C{3}).(C{4}) = value;
            else
                error('Nested structs above 4 are not supported yet')
            end
            
%             fprintf('settings.%s = %s\n',id,valueString)
        end
        
%         function setCurrentDirectory(s)
%             s.outp.directory = '';
%         end
        
        function setConvergence(s,nonlinear,coupling)
            s.comp.nonlinear.convType   = nonlinear;
            s.comp.coupling.convType	= coupling;
        end
        
        function setNSWeights(s,weights)
            s.comp.NS.equationWeights   = weights;
        end
        
        function setCHWeights(s,weights)
            s.comp.CH.equationWeights = weights;
        end
        
        function setInitialWeight(s,value)
            s.comp.initial.weight = value;
        end
        
        function setCoupling(s,flag)
            s.comp.coupling.enabled = flag;
        end
                
        function out = getCoupling(s)
            out = s.comp.coupling.enabled;
        end
    end
       
    methods (Static)
        % Load a settings object from a file. 
        % Conversion from a struct to an object is required here.
        function settings = load(filename,verbose)
            
            % Set the default filename if no valid is provided
            if ( nargin==0 || (nargin==1 || isempty(filename)) )
                filename = DefaultSettings.defaultFilename;
            end
            
            % Set verbose if no valid is provided
            if (nargin < 2 || ~islogical(verbose) )
                verbose = true;
            end

%             if (fromMatFile)
%                 if (isstruct(filename))
%                     settings = System.settings;
%                     settings.setCurrentTime( filename.time.current );
%                     settings.setInitialTime( filename.time.current );
%                     settings.setIter('time',filename.time.iter);
%                     settings.check(false);
%                     
%                 else
%                     data    = load(filename);
%                     groups  = fields(data);
%                     settings = DefaultSettings();
%                     for i=1:numel(groups)
%                         settings.(groups{i}) = data.(groups{i});
%                     end
%                     if (verbose && settings.outp.verbose && System.parallel.rank==0)
%                         fprintf('SETTINGS   : Loaded from %s\n',filename)
%                     end
%                     
%                     % Finally check if all settings are sane
%                     settings.check(verbose);
%                 end
%             else

            if ( isstruct(filename) )
                settings = System.settings;
                settings.time.iter      = filename.time.iter;               % set the current time iteration
                settings.time.current   = filename.time.current;            % set the current time level
                %settings = filename;

            elseif ( isfile([filename,'.m']) )
                settings = eval(filename);
                if (verbose && settings.outp.verbose && System.rank==0)
                    fprintf('SETTINGS   : Initialized from %s.m\n',filename)
                end
                
            else
                if DefaultSettings.fileError
                    error('SETTINGS   : %s.m not found, aborting execution\n',filename)
                else
                    settings = DefaultSettings(); 
                    if (System.rank==0)
                        fprintf('SETTINGS   : %s.m not found, initialized from DefaultSettings.m\n',filename)
                    end
                end
            end
            
%                 try
%                     %settings = Settings(); 
%                     settings = eval(filename);
%                     if (verbose && settings.outp.verbose && System.rank==0)
%                         fprintf('SETTINGS   : Initialized from Settings.m\n')
%                     end
%                 catch MExp
%                     settings = DefaultSettings(); 
%                     if (System.rank==0)
%                         fprintf('SETTINGS   : No Settings.m found, initialized from DefaultSettings.m\n')
%                         fprintf('SETTINGS   : %s\n',MExp.message)
%                     end
%                 end
                
            % Finally check if all settings are sane
            settings.check(verbose);

        end
        
%         function set.defaultFileName(filename)
%             
%         end
        
        % Adjust the mesh (add pillars)
        function setMesh( mesh )
            % dummy in case the Settings file does not contain this
            % function, it should be overloaded
        end

        function setBoundaryConditions(physics)
            disp('WARNING    : No boundary condition function in Settings.m, please add a "setBoundaryConditions(physics)" function')
        end
        
        
        function breakTimeLoop = postTimeStep(s)
            breakTimeLoop = false;
            
            %% Compute error
            for i=1:numel(System.settings.phys.system)
%                 
                selectedPhysics = PhysicsCollection.get(i);
%                 
                if (selectedPhysics.settings.exact)
                    selectedPhysics.setExactSolution(false);	% Assign the exact solution

                    selectedPhysics.computeError(1);
                    %selectedPhysics.computeRelativeError(-1);

                    selectedPhysics.writeError();
                end
            end
        end
        
        function nonLinearCallback( s )
            % dummy in case the Settings file does not contain this
            % function, it should be replaced
        end % function nonLinearCallback
        
%         function out = fileExists()
%             out = false;
%             if (exist(DefaultSettings.defaultfilename, 'file') == 2)
%                 out = true;
%             end
%         end
        
%         function out = getSettingsFile()
%             out = DefaultSettings.defaultfilename;
%         end

    end % methods
    
end
