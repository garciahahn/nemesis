function startup
%startup - Setup the paths for the Nemesis library
%At the startup of Matlab this function will ensure the correct paths are
%added, so that all library functions become available within Matlab.
%
% Syntax    : startup
%
% Result    : allows Matlab can use commands located at 'libraryRoot'
%
% Example   : modify the libraryRoot variable for your machine architecture

% Author: Marcel Kwakkel
% Work address: Thermal Two-Phase Flow Laboratory, EPT, NTNU, Kolbjørn Hejes vei 2A, NO-7491 Trondheim
% email: marcel.kwakkel@ntnu.no
% Website: https://www.ntnu.edu/employees/marcel.kwakkel
% 2017 - 2019

%------------- BEGIN CODE --------------
    if (ispc)       % Windows
        libraryRoot = 'C:\work\codes\nemesis\library';
    elseif (ismac)  % OSX / Apple
        libraryRoot = '/Users/Marcel/work/codes/nemesis/library';
    elseif (isunix) % Linux / Unix
     	libraryRoot = '~/projects/nemesis';
    end
    p = {fullfile(libraryRoot)};
    addpath(p{:});
    setupPaths;
    System.init(-1);
end
%------------- END OF CODE --------------
