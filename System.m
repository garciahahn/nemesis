% The System class is used to setup the mesh, 
%
%
classdef System < handle %< DefaultSettings
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
%     properties(Constant) %, Access=private)
%         instance = System();     % there is only one instance of System, accessible through System.Controller
%     end

  properties(Constant, Access=public)
      debug = false;
  end
        
  properties (Access=private)
    settingsData;
    parallelData;        
    verbose = false;
    mpi;
    outputDir;
    discIntegration;
    numVariables;
    init_memory;

    arch, maxSize, endian;
    time_step_wall_clock_time;

    initOnly = false;
  end
    
  methods(Access=private)
    % Guard the constructor against external invocation.  We only want
    % to allow a single instance of this class.  See description in
    % Singleton superclass.
    function self = System()
        % Initialise your custom properties.
    end
    
  end

  methods (Static)
    % Concrete implementation.  See Singleton superclass.
    function self = instance( reset )
      persistent uniqueInstance

      if (nargin==0)
          reset = false;
      end

      if isempty(uniqueInstance) || reset %|| isempty(fieldnames(uniqueInstance))
          self = System();
          uniqueInstance = self;
      else
          self = uniqueInstance;
      end
    end
                
    % Function to initialize the entire system (in serial or parallel).
    % This function will do the following:
    %   1) load the libraries
    %   2) start the parallel MPI system
    %   3) load the settings from Settings.m
    %   4) initialize the simulation (from restart data if requested)
    %   5) set time step limits (for this restart data overwrites step 3)
    %   6) write a session file and do some MPI analysis (if requested)
    function init(restartID,settingsFilename,verbose)
      tic;

      PatchCollection.reset;

      if (nargin==0 || isempty(restartID))
          error('Please provide a valid restartID (int)\n')
      end

      if (nargin<2)
        settingsFilename = [];
      end

      if (nargin<3)
        verbose = false;
      end

      if (restartID~=-2)
        instance = System.instance(true);
        [instance.arch,instance.maxSize,instance.endian] = computer;

        try
            mpiReady = logical( NMPI_Initialized() );
        catch
            mpiReady = false;
        end

        if ( ~mpiReady || (mpiReady && System.nproc == 1) )
            System.loadLibraries();
            System.startParallel();
        end
      end

      switch restartID
        case RestartID.batch_run % == -2
          System.createIntegrationDisc(System.settings.comp.integration);
          System.initSimulation(0);
          
        otherwise
          if (restartID >= 0) % new or restart
            
            % Load the settings
            instance.settingsData = DefaultSettings.load(settingsFilename,System.getVerbose);

            % Create output directory if it does not exist
            System.createDirectory(System.settings.outp.directory);

            % Create a session file inside the output folder
            System.setVerbose( true );
            System.writeSession;

            if ( System.rank==0 && System.getVerbose )
              System.printTitle;
              disp( System.parallel.status );
              fprintf('SYSTEM     : %s architecture\n',instance.arch)
              fprintf('SYSTEM     : %s endian\n',instance.endian)

            end % if

            System.createIntegrationDisc(System.settings.comp.integration);

            if (restartID==0)
                System.initSimulation(restartID);
            else
                System.reinitSimulation(restartID);
            end
        
          elseif (restartID == RestartID.initialization)
            
          end
          
      end
      if ~ispc
      System.resetMaxMemory;
      System.printMemory;
      aux = System.getMemoryValues();
      System.setInitialMemory( aux(1) );
      end
      % Copy the settingsFile to the output directory
      if ( ~isempty(settingsFilename) )
        copyfile( [settingsFilename,'.m'], System.settings.outp.directory )
      end
    end % function init

    function createAnalysisFramework(settings_file_name, step_ID)
        
        PatchCollection.reset;
        instance = System.instance(true);
        [instance.arch,instance.maxSize,instance.endian] = computer;
        
        try
            mpiReady = logical( NMPI_Initialized() );
        catch
            mpiReady = false;
        end
        
        if ( ~mpiReady || (mpiReady && System.nproc == 1) )
            System.loadLibraries();
            System.startParallel();
        end
        
        instance.settingsData = DefaultSettings.load(settings_file_name, System.getVerbose);
        
        System.setVerbose(true);
        System.reinitSimulation(step_ID);
        
    end % function createAnalysisFramework
    
    % Restart a simulation from a restartFile
    function reinitSimulation(fileID)

      data = System.loadData( fileID );

      %% Settings
      try 
          DefaultSettings.load( data.settings, true ); 
          if (System.rank==0 )
              fprintf('RESTART    : loading of settings done\n')
          end
      catch MExp
          error('RESTART    : loading of settings failed\n')
      end

      %% Load the data from previous time levels if BDF2 time stepping is used
      activeSettings = System.settings;
      if ( activeSettings.time.method==TimeMethod.BDF2 )
          oldData1 = System.loadData( fileID-1 );

          data = System.addOldTimeLevels( data, oldData1 );
      end

      %% Try to load the data from the file            
      %% Finite elements
      try
          FECollection.load( data.finiteElements );
      catch
          error('RESTART    : loading of FiniteElements failed on rank %d\n',System.rank)                
      end
      MeshCollection.reset();
%% Mesh initialization (one for each physics)    
%{
      try
          MeshCollection.load( data.mesh );
          fprintf('RESTART    : loading of Mesh done on rank %d\n',System.rank)
      catch
          error('RESTART    : loading of Mesh failed on rank %d\n',System.rank)                
      end
%}
%             %% Add settings to physics
%             for n=1:numel(data.physics)
%                 if ( ~isfield(data.physics{n},'settings') )
%                     data.physics{n}.settings = data.settings.phys.system{n};
%                 end
%             end

      %% Physics
      try 
        PhysicsCollection.load( data.physics );
        if ( System.rank == 0 )
            fprintf('RESTART    : loading of all physics done\n')
        end
      catch MExp
        save(['PhysicsCollection_load.' int2str(System.rank) '.mat']);
        if (~iscell(data.physics) && System.rank==0)
            error('RESTART    : no valid physics data present in restart file\n')
        else
            error('RESTART    : loading of physics failed')
        end
      end

      if ( System.rank == 0 )
        fprintf('RESTART    : All data has been processed successfully\n');
      end
    end

    % Start a new simulation
    function initSimulation(fileID)
        %System.createDirectory(System.settings.outp.directory);
        %if (isempty(fileID))
        %    System.saveSettings;
        %end
        if (System.rank==0)
            disp('SYSTEM     : Starting from scratch');
        end

        %% Finite element initialization
        FECollection.reset();    
        finiteElement = FiniteElement();
        FECollection.add(1,finiteElement);

        %% Mesh initialization (one for each physics)
        MeshCollection.reset();
        if (System.settings.mesh.matching)
            baseMesh = OctreeMesh();
        else
            for i=1:numel(System.settings.phys.system)
                baseMesh = OctreeMesh();
            end
        end

        %% Add special features to the mesh like pilars
        activeSettings = System.settings;
        activeSettings.setMesh( baseMesh );

        %% Reset the FieldCollection
        FieldCollection.reset();

        DoFHandlerCollection.reset();

        %% Creation of the PhysicsCollection
        PhysicsCollection.reset();   
        for i=1:numel(System.settings.phys.system)
            fh = str2func(System.settings.phys.system{i}.class);
            if (System.settings.mesh.matching)
                mesh = MeshCollection.get(1);
            else
                mesh = MeshCollection.get(i);
            end
            physics = fh(mesh,System.settings.phys.system{i});
            PhysicsCollection.add(i,physics);
        end

        % Now 'PhysicsCollection' contains all the physics, it can be 
        % used to set the initial condition of every physics. This is not
        % integrated in the previous loop, because the initial
        % solution of one physics might depend on the another.
        %
        % Note on AMR:
        % This operation will use the initial solution as well to
        % refine the mesh as requested (which can be physical values/gradients).
        PhysicsCollection.setInitialSolution();

        %% Ensure all fields correspond to their meshes
        %   NB: this might not be the case after some AMR operations
        FieldCollection.update;

        System.writeRestart();
    end

    % Function to terminate this System
    function terminate( id )
      if ( System.rank == 0 )
        if ( id == 0 )
            fprintf('SYSTEM     : terminated without errors\n');
        else
            error('No Settings.m found or the file contains an error')
        end
      end
      if ( System.nproc > 1 )
        exit
      end
    end

    function data = loadData(fileID)
      % Function to load data from a restart file. Parallel data will
      % be converted to 1 processor if the current run is not in
      % parallel

      %% Use the fileID to set the filename
      if isnumeric(fileID)
        baseDir  = System.settings.outp.directory;
        baseFile = System.settings.outp.solutionFilename;

        if (System.settings.outp.parallelWrite)
            filename = sprintf('%s%s.%.7d.%.2d.mat',baseDir,baseFile,fileID,System.rank);
        else
            filename = sprintf('%s%s.%.7d.mat',baseDir,baseFile,fileID);
        end
      elseif ischar(fileID)
        filename = fileID;
      else
        error('Unexpected fileID : please pass an integer or a full file path')
      end

      if (exist(filename,'file')==2)
        data = load(filename);

        % Merge multiple restart files (the fields in them) if the current number of processors is not equal to 
        % the original (saved) number of processors. The new created file can be read as a normal restart file.
        if ( data.nproc ~= System.nproc )
          temp = cell(data.nproc,1);

          % Load all data
          for r=0:data.nproc-1
            filename = sprintf('%s%s.%.7d.%.2d.mat',baseDir,baseFile,fileID,r);
            temp{r+1} = load(filename);
          end

          % Loop over all physics
          for p=1:numel(data.physics)

            % Loop over all fields
            for f=1:numel(data.physics{p}.fields)

              % Loop over all subfields
              for sf=1:numel(data.physics{p}.fields{f}.subFields)

                if (~isempty(data.physics{p}.fields{f}.subFields{sf}))
                  data.physics{p}.fields{f}.subFields{sf}.alpha = [];
                  data.physics{p}.fields{f}.subFields{sf}.value = [];

                  % Loop over all ranks
                  for r=1:data.nproc
                    data.physics{p}.fields{f}.subFields{sf}.alpha = [    data.physics{p}.fields{f}.subFields{sf}.alpha; ...
                                                                      temp{r}.physics{p}.fields{f}.subFields{sf}.alpha ];

                    data.physics{p}.fields{f}.subFields{sf}.value = [    data.physics{p}.fields{f}.subFields{sf}.value; ...
                                                                      temp{r}.physics{p}.fields{f}.subFields{sf}.value ];
                  end % processors
                end % non-empty subfields
              end % subfields
            end % fields
          end % physics

          filename = sprintf('%s%s.%.7d.[%.2d-%.2d].mat',baseDir,baseFile,fileID,1,data.nproc);
        end % current and previous nproc are not equal

        if (System.rank==0 && isstruct(data) || System.settings.outp.parallelWrite)
          fprintf('RESTART    : File data loaded successfully from %s\n',filename)
        else
          fprintf('RESTART    : File data loading failed from %s\n',filename)
        end
      else
        if (System.rank==0)
          error('RESTART    : %s does not exist\n',filename)
        end
      end % file exists
    end % function loadData

    function data = addOldTimeLevels( data, varargin )
      num_physics = numel( data.physics );

      for p = 1 : num_physics
        num_fields = numel( data.physics{ p }.fields );

        for f = 1 : num_fields
          for o=1:numel(varargin)
            data.physics{ p }.fields{ f }.oldTimeLevels{ o } = varargin{ o }.physics{ p }.fields{ f }.subFields{ 1 };
          end
        end
      end
    end

    function writeSession()
      % Write Matlab output to session file
      if (System.rank==0)
        sessionFile = [System.settings.outp.directory 'session.out'];        
        System.preventOverwrite(sessionFile);        
        diary(sessionFile);
        if (System.rank==0)
          System.displayTitle;
          fprintf('SYSTEM     : Session is recorded (%s)\n',sessionFile);
        end
      end
    end

    function info()
      instance = System.instance;
      fprintf('++++++++++++++++++++++++++++++++++++++++\n')
      fprintf('+                SYSTEM                +\n')
      fprintf('++++++++++++++++++++++++++++++++++++++++\n')
      fprintf('+  Architecture : %s +\n',instance.arch)
      fprintf('+  Max size     : %e +\n',instance.maxSize)
      fprintf('+  Endianness   : %s +\n',instance.endian)
      fprintf('++++++++++++++++++++++++++++++++++++++++\n')
    end

    % Display title of this simulation
    function printTitle()
        if (System.rank==0)
            if (~isempty(System.settings.title))
                fprintf('SIMULATION : %s\n', System.settings.title);
            else
                fprintf('LSQDIM     : Successful initialization\n');
            end
        end
    end

    function addPath(path)
        addpath(path);
    end

    function advanceTime(tt)

        oldTime = System.settings.time.current;
        System.settings.advanceTime;
        newTime = System.settings.time.current;

        if (System.settings.time.method~=TimeMethod.Steady)
            if (tt==System.settings.time.nStart)
                if (System.rank==0 )
                    fprintf('\n');
                    fprintf('Starting time loop\n');
                end
            end
            System.writeToScreen('time',oldTime,newTime);
        else
            if (System.rank==0 )
                fprintf('\n');
                fprintf('Starting steady solution\n');
            end
        end
    end

    function writeToScreen(name,varargin)
        outputAbsVar = false;

        if (System.rank==0) 
            switch name
                case 'time'
                    iter = System.settings.time.iter;
                    dt   = System.settings.time.step;
                    fprintf('\n')
                    fprintf('Time step %d  ---  time -> %f  ---  dt = %f\n',iter,varargin{2},dt);

                case 'nonlinear'
                    % varargin contains: 
                    %   1) physics name
                    %   2) current stage
                    %   3) numStages
                    %   4) nonlinear iteration number
                    %   5) time for nonlinear step
                    %   6) solver info
                    %   7) variable names
                    %   8) variable norms
                    %   9) equation norms
                    %   10) active variables
                    if (~isempty(varargin{6}))
                        if ( varargin{2}(1)==1 && varargin{4}(1)==1 )     % header only for the first step of the first nonlinear iteration
%                                 if (varargin{3}==1)
%                                     fprintf('  %2.2s  NL | cgResInit  cgResFinal  cgIter  wallTime ',varargin{1});
%                                 else
                                fprintf('  %2.2s NL Stg | cgResInit  cgResFinal  cgIter  wallTime ',varargin{1});
%                                 end

                            % Loop over the variable fields
                            fprintf('|');
                            for i=1:numel(varargin{7})
                                fprintf(' rel(|%s%s|) ',916,varargin{7}{i}(1));
                            end
                            for i=numel(varargin{7})+1:PhysicsCollection.getMaxNumVariables
                                fprintf('           ');
                            end

                            if (outputAbsVar)
                                fprintf('|');
                                for i=1:numel(varargin{7})
                                    fprintf(' abs(|%s%s|) ',916,varargin{7}{i}(1));
                                end
                                for i=numel(varargin{7})+1:PhysicsCollection.getMaxNumVariables
                                    fprintf('           ');
                                end
                            end

                            fprintf('|');
                            for i=1:numel(varargin{9})
                                fprintf(' rel(|%s%d|) ',916,i);
                            end
                            for i=numel(varargin{9})+1:PhysicsCollection.getMaxNumEquations+1
                                fprintf('           ');
                            end

                            fprintf('|');
                            for i=1:numel(varargin{9})
                                fprintf('   (|%s%d|)  ',916,i);
                            end
                            for i=numel(varargin{9})+1:PhysicsCollection.getMaxNumEquations+1
                                fprintf('           ');
                            end
                            fprintf('\n');
                        end

                        if (varargin{2}>1)          % current stage > 1 : no not print the NL number again
                            fprintf('        %2d  | %9.3e  %10.3e  %6d  %8.2f ',varargin{2},varargin{6}.initRes,varargin{6}.finalRes,varargin{6}.iter,varargin{5});
                        elseif (varargin{3}==1)     % single stage : do not print any stage information (but do print NL information)
                            fprintf('     %2d     | %9.3e  %10.3e  %6d  %8.2f ',varargin{4},varargin{6}.initRes,varargin{6}.finalRes,varargin{6}.iter,varargin{5});
                        else                        % otherwise (print all)
                            fprintf('     %2d %2d  | %9.3e  %10.3e  %6d  %8.2f ',varargin{4},varargin{2},varargin{6}.initRes,varargin{6}.finalRes,varargin{6}.iter,varargin{5});
                        end

                    else
                        % direct solver output : no solver convergence / number of iterations
                        if ( varargin{2}(1)==1 && varargin{4}(1)==1 )
                            fprintf('  %2.2s NL Stg | wallTime ',varargin{1});

                            % Loop over the variable fields
                            fprintf('|');
                            for i=1:numel(varargin{7})
                                fprintf(' rel(|%s%s|) ',916,varargin{7}{i}(1));
                            end
                            for i=numel(varargin{7})+1:PhysicsCollection.getMaxNumVariables
                                fprintf('           ');
                            end

                            if (outputAbsVar)
                                fprintf('|');
                                for i=1:numel(varargin{7})
                                    fprintf(' abs(|%s%s|) ',916,varargin{7}{i}(1));
                                end
                                for i=numel(varargin{7})+1:PhysicsCollection.getMaxNumVariables
                                    fprintf('           ');
                                end
                            end

                            fprintf('|');
                            for i=1:numel(varargin{9})
                                fprintf(' rel(|%s%d|) ',916,i);
                            end
                            for i=numel(varargin{9})+1:PhysicsCollection.getMaxNumEquations+1
                                fprintf('           ');
                            end

                            fprintf('|');
                            for i=1:numel(varargin{9})
                                fprintf('   (|%s%d|)  ',916,i);
                            end
                            for i=numel(varargin{9})+1:PhysicsCollection.getMaxNumEquations+1
                                fprintf('           ');
                            end
                            fprintf('\n');
                        end

                        if (varargin{2}>1)          % current stage > 1 : no not print the NL number again
                            fprintf('        %2d  | %8.2f ',varargin{2},varargin{5});
                        elseif (varargin{3}==1)     % single stage : do not print any stage information (but do print NL information)
                            fprintf('     %2d     | %8.2f ',varargin{4},varargin{5});
                        else                        % otherwise (print all)
                            fprintf('     %2d %2d  | %8.2f ',varargin{4},varargin{2},varargin{5});
                        end 
                    end

                    % Output relative L2 norm for variables
                    fprintf('|');
%                         for i=varargin{10}
%                             fprintf(' %9.3e ',varargin{8}{i}.L2rel);                                
%                         end

                    for i=1:PhysicsCollection.getMaxNumVariables
                        if (~isempty(varargin{8}))
                            if ismembc(i,varargin{10})
                                fprintf(' %9.3e ',varargin{8}{i}.L2rel); 
                            else
                                fprintf('           ');
                            end
                        else
                            fprintf('           ');
                        end
                    end

                    % Output absolute L2 norm for variables
                    if (outputAbsVar)
                        fprintf('|');
                        for i=1:numel(varargin{7})
                                fprintf(' %9.3e ',varargin{7}{i}.L2);
                        end
                        for i=numel(varargin{7})+1:PhysicsCollection.getMaxNumVariables
                            fprintf('           ');
                        end
                    end

                    % Output relative L2 norm for equations
                    fprintf('|');
                    for i=1:numel(varargin{9})
                        fprintf(' %9.3e ',varargin{9}{i}.deltaL2rel);                                
                    end
                    for i=numel(varargin{9})+1:PhysicsCollection.getMaxNumEquations+1
                        fprintf('           ');
                    end

                    % Output relative L2 norm for equations
                    fprintf('|');
                    for i=1:numel(varargin{9})
                        fprintf(' %9.3e ',varargin{9}{i}.L2);                                
                    end
                    for i=numel(varargin{9})+1:PhysicsCollection.getMaxNumEquations+1
                        fprintf('           ');
                    end

                    fprintf('\n');

                case 'coupling'
                    % varargin contains: 
                    %   1) physics position number
                    %   2) physics name
                    %   3) coupling iteration number
                    %   4) L2norms (value & residual)
                    if (varargin{1}==1)
                        fprintf('  %.2d\n',varargin{3})
                    end
                    if (System.settings.comp.CG.method==SolverMethod.direct) 
                        fprintf('  %2.2s        | %8.2f ',varargin{2},varargin{4});
                    else % to maintain alignment, there's some additional spacing for CG solver output
                        fprintf('  %2.2s        | %30s %8.2f ',varargin{2},' ',varargin{4});
                    end

                    % Output relative L2 norm for variables
                    fprintf('|');
                    for i=1:numel(varargin{5})
                        fprintf(' %9.3e ',varargin{5}{i}.L2rel);                                
                    end
                    for i=numel(varargin{5})+1:PhysicsCollection.getMaxNumVariables
                        fprintf('           ');
                    end

                    % Output relative L2 norm for equations
                    fprintf('|');
                    for i=1:numel(varargin{6})
                        fprintf(' %9.3e ',varargin{6}{i}.deltaL2rel);                                
                    end
                    for i=numel(varargin{6})+1:PhysicsCollection.getMaxNumEquations+1
                        fprintf('           ');
                    end

                    % Output relative L2 norm for equations
                    fprintf('|');
                    for i=1:numel(varargin{6})
                        fprintf(' %9.3e ',varargin{6}{i}.L2);                                
                    end
                    for i=numel(varargin{6})+1:PhysicsCollection.getMaxNumEquations+1
                        fprintf('           ');
                    end

                    fprintf('\n');

                    if (varargin{1}==PhysicsCollection.size)
                        fprintf('\n');
                    end
            end
        end
    end

    function setIter(type,value)
        System.settings.setIter(type,value);
    end

    function newLine()
        if (NMPI.instance.rank==0)
            fprintf('\n')
        end
    end

    function outputDir = initializeOutputDir(outputDir)
        if ( NMPI.instance.rank==0 && ~exist( outputDir,'dir') )
            mkdir(outputDir);
        end
        if  ~( strcmp( outputDir(end), '/' ) )
            outputDir = [outputDir '/'];
        end
    end

    function loadLibraries(verbose)
        if nargin==1
            System.setVerbose(verbose);
        end

        %% Setup library paths
        setupPaths;
    end

    % Load the settings from a Settings.m file
    function loadSettings( filename )
        if (nargin<1)
            filename = [];                
        end
        instance = System.instance;            
        instance.settingsData = DefaultSettings.load(filename,System.getVerbose);
    end

    function saveSettings()
        System.settings.save();
    end

    % Function to move files if they already exist. Existing files will
    % get a numeric suffix, and lower numbers means the file is older.
    function preventOverwrite(filename)
        isAvailable = false;

        id = 0;

        if (exist(filename,'file')==2)
            while ~isAvailable
                if (exist([filename '.' num2str(id)],'file')==2)
                    id=id+1;
                else
                    isAvailable = true;
                    newFileName = [filename '.' num2str(id)];
                    movefile(filename,newFileName);
                end                    
            end
        end
    end

    % Initialize the parallel environment
    function startParallel()
        instance = System.instance;
        instance.parallelData = NMPI.instance;
        instance.parallel.createLinear(System.getVerbose);
    end

    % Get function
    function out = rank()
      out = NMPI.instance.rank;
    end

    function out = nproc()
      try
        out = NMPI.instance.nproc;
      catch
        out = 1;
      end
    end

    function out = useMexSolver()
        out = true;
    end

    function out = parallel()
      out = System.instance.parallelData;
    end

    function out = settings()
      out = System.instance.settingsData;
    end

    function setCurrentTime(t)
      settings = System.settings;
      settings.setCurrentTime(t);
    end

    % Save the solution to a file
    function writeRestart()            
        currentStep = round( System.settings.time.current / System.settings.time.step );
        if (System.settings.outp.solutionSave && mod(currentStep,System.settings.outp.solutionStep)==0)

            if (~System.settings.outp.parallelWrite)
                filename = sprintf([System.settings.outp.directory System.settings.outp.solutionFilename '.%.7d.mat'],currentStep);
                % General information 'header'

                finiteElements  = FECollection.save();
                mesh            = MeshCollection.save();
                iter            = currentStep;
                rank            = System.rank;
                nproc           = System.nproc;
                %fields          = FieldCollection.save();
                physics         = PhysicsCollection.save();
                settings        = System.settings.save();

                if (NMPI.instance.rank==0)
                    save(filename,'finiteElements','mesh','iter','rank','nproc','physics','settings');
                end

            else
                filename = sprintf([System.settings.outp.directory System.settings.outp.solutionFilename '.%.7d.%.2d.mat'],currentStep,System.rank);
                % General information 'header'

                finiteElements  = FECollection.save();
                mesh            = MeshCollection.save();
                iter            = currentStep;
                rank            = System.rank;
                nproc           = System.nproc;
                %fields          = FieldCollection.save();
                physics         = PhysicsCollection.save();
                settings        = System.settings.save();

                %if (NMPI.instance.rank==0)
                save(filename,'finiteElements','mesh','iter','rank','nproc','physics','settings');
                %end

            end
        end 
    end

    % Save the solution to a file
    function saveSolution(standardElement,baseMesh,physics)
        currentStep = round( System.settings.time.current / System.settings.time.step );
        if (System.settings.outp.solutionSave && mod(currentStep,System.settings.outp.solutionStep)==0)
            filename = sprintf([System.settings.outp.directory System.settings.outp.solutionFilename '.%.7d.mat'],currentStep);
            % General information 'header'
            if (NMPI.instance.rank==0)
                seTemp          = standardElement;
                standardElement = standardElement.save();
                mesh            = baseMesh.save();
                iter            = currentStep;
                save(filename,'standardElement','mesh','iter');%,'Pn','Q','C','spaceTime','uniform','SDIM');
                standardElement = seTemp;
            end
            for i=1:numel(System.settings.phys.system)
                physics{i}.save(filename);
                physics{i}.fieldGroup.writeVTU(System.settings.time.iter,System.settings.time.current);
            end
        end 
    end

    % Function to write general simulation data (per time step) to an output file
    function writeValues()
      filename = [ System.settings.outp.directory 'nemesis-' System.settings.outp.valueFilename '.txt' ];

      if (System.rank==0)
          if (System.settings.time.iter==1)
              header = {'iter','time','wallTime','current_memory', 'delta_memory','maximum_memory'};
              System.writeHeader(filename, header)
          end

          memory_values = System.getMemoryValues;

          values     = zeros(6,1);
          values(1)  = System.settings.time.iter; 
          values(2)  = System.settings.time.current;
          values(3)  = System.getTimeStepWallClockTime;
          values(4)  = memory_values(1);
          values(5)  = memory_values(1) - System.instance.init_memory;
          values(6)  = memory_values(2);

          System.appendToFile(filename,values)
      end
    end
    
    % Set location for output
    function createDirectory( location )
      if ( System.rank==0 && ~exist( location,'dir') && ~isempty(location) )
        mkdir(location);
      end
      if ~isempty(location) && ~( strcmp( location(end), '/' ) )
        location = [location '/'];
      end

      if (System.rank==0)
        fprintf('SYSTEM     : Output to %s\n',location);
      end

      if (System.nproc>1)
        status  = 1;
        status = NMPI.Allreduce(status,1,'+');
      end
    end

    function createIntegrationDisc(Q)
      instance = System.instance;
      isSpaceTime = (System.settings.time.method==TimeMethod.SpaceTime);
      instance.discIntegration = FiniteElement(System.settings.stde.Pn,Q,System.settings.stde.C,System.settings.mesh.sdim,isSpaceTime);
      if (System.getVerbose && System.rank==0)
        fprintf('SIMULATION : Discretization for integration has been initialized with Q=%d\n',Q(1))
      end
    end

    function out = getIntegrationDisc()
      out = System.instance.discIntegration;
    end

    % Write a cell array with headers to a file, overwrite any existing file
    function writeHeader( filename, header )
      if ( System.rank == 0 )
        format = '%17s';
        fileID = fopen( filename, 'w' );
        
        for i=1:numel(header)
            fprintf( fileID, format, header{i} );
        end
        fprintf( fileID, '\n' );
        
        fclose( fileID );
      end
    end
        
    % Append the data of an array to a file
    function appendToFile( filename, values )
      if ( System.rank == 0 )
        format = '%17.8e';
        fileID = fopen( filename, 'a' );
        
        [ n_rows, n_columns ] = size(values);
        
        for j = 1 : n_columns
          for i = 1 : n_rows
            fprintf( fileID, format, values(i,j) );
          end
          fprintf( fileID, '\n' );
        end
        
        fclose( fileID );
      end
    end
    
    function setTimeStepWallClockTime( time )
      instance = System.instance;
      instance.time_step_wall_clock_time = time;
    end

    function out = getTimeStepWallClockTime()
      instance = System.instance;
      out = instance.time_step_wall_clock_time;
    end
    
    function setVerbose(flag)
      instance = System.instance;
      instance.verbose = flag;
    end

    function out = getVerbose()
      instance = System.instance;
      out = instance.verbose;
    end

    function setInitOnly(flag)
      instance = System.instance;
      instance.initOnly = flag;
    end
    
    function setInitialMemory( memory )
      instance = System.instance();
      instance.init_memory = memory;
    end % setInitialMemory
    
    function ret = getInitialMemory( ~ )
        instance = System.instance();
        ret = instance.init_memory;
    end % getInitialMemory

    % Display the Nemesis title
    function displayTitle()
      instance = System.instance;
      if (~instance.initOnly)
        disp('       _   __                         _      ');
        disp('      / | / /__  ____ ___  ___  _____(_)____ ');
        disp('     /  |/ / _ \/ __ `__ \/ _ \/ ___/ / ___/ ');
        disp('    / /|  /  __/ / / / / /  __(__  ) (__  )  ');
        disp('   /_/ |_/\___/_/ /_/ /_/\___/____/_/____/   ');
        disp('                                             ');
        disp('   Least-squares spectral element framework  ');
        disp('      Thermal Two-Phase Flow Laboratory      ');
        disp('       EPT / NTNU / Trondheim / Norway       ');
        disp('        2011 - 2020  //  Version 2.01        ');
        disp('                                             ');
        disp('    Marcel Kwakkel & Julián N. García Hahn   ');
        disp('                                             ');
      end
    end

    % Returns memory values of this PID in kB
    %   OUTPUT
    %   memory_values: vector with [currentLevel,maximumLevel] memory values
    function memory_values = getMemoryValues()
      [ ~, curMemory ] = system( strcat('cat /proc/',string( System.getPID ),'/status | grep VmRSS') );
      [ ~, maxMemory ] = system( strcat('cat /proc/',string( System.getPID ),'/status | grep VmHWM') );
      % JULIAN MOD
      % I changed the memory output to kilobytes to have more resolution on the data. This could be further divided by 1000 if only MB are
      % needed.
      memory_values(1) = round( str2double( strtrim( extractAfter( extractBefore( curMemory, ' kB' ), ':' ) ) ) );
      memory_values(2) = round( str2double( strtrim( extractAfter( extractBefore( maxMemory, ' kB' ), ':' ) ) ) );
    end % getMemoryValues

    % Resets the maximum memory used by this PID through a system command, necessary when
    % multiple runs are done within the same MATLAB process
    %
    %   INPUT
    %   print_message: boolean indication if a message should be printed, which is useful 
    %                  when running in parallel [optional, default = true]
    %
    %   ISSUES
    %   1) only works on Linux / MacOS?
    function resetMaxMemory( print_message )
      if ( nargin == 0 )
        print_message = true;
      end

      system( strcat('echo 5 > /proc/',string( System.getPID ),'/clear_refs') );

      if (print_message)
        fprintf("MEMORY     : Maximum memory has been reset\n");
      end
    end % function resetMaxMemory
        
    % Outputs a line with memory information from a single rank. In parallel the memory values of all processes are summed
    function printMemory()
      memory_values = System.getMemoryValues;

      if ( System.nproc > 1 )
        memory_values = NMPI.Allreduce(memory_values,numel(memory_values),'+');
      end

      if ( System.rank == 0 )
        fprintf("MEMORY     : current = %s kB / maximum = %s kB\n", System.FormatThousand(memory_values(1)), System.FormatThousand(memory_values(2)));
      end
    end % function printMemory
        
    % Returns the PID of the current Matlab process
    function pid = getPID()
      pid = feature('getpid');
    end % function getPID

    % Returns the Endian type of this machine
    function out = getEndian()
      out = System.instance.endian;
    end
    
    function str_out = FormatThousand( number )
        str = sprintf('%d', number);
        str_out = fliplr(regexprep(fliplr(str), '(\d+\.)?(\d{3})(?=\S+)', '$1$2,'));
    end % function FormatThousand
  end % method static 
end % classdef