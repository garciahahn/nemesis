classdef Norm
    %UNTITLED3 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods(Static)
%         function norm = scalarL2Norm(pNew,pOld)
%             l1 = length(pNew);
%             l2 = length(pOld);
%             
%             if (l1~=l2)
%                 disp('Error: the number of elements in computeL2Norm is not equal')
%                 exit
%             end
%             
%             normArray = zeros(l1,1);
%             
%             for i=1:l1
%                 normArray(i) = sqrt( sum((pNew{i}(:)-pOld{i}(:)).^2 ) ) / max( 1, sqrt(sum( (pNew{i}(:)).^2 )) );
%             end
%             
%             norm = max(normArray);
%             
%             norm = NMPI_Allreduce(norm,1,'M',NMPI.instance.mpi_comm_world);
%         end
        
        function [norm, maxValue] = scalarL2NormTest(pNew,pOld,mesh)
%             numElem1 = pNew.getNumElements;
%             numElem2 = pOld.getNumElements;
            
            if (size(pNew)~=size(pOld))
                disp('Error: the number of elements in computeL2Norm is not equal')
%                 exit
            end
            
%             normArray = zeros(l1,1);
            
            norm = 0;
            normNew = 0;
            normNew1 = 0;
            normOld = 0;
            normOld1 = 0;
            maxValue = 0;
            
%             w1 = pNew.getWeight;
%             w2 = pOld.getWeight;
%             
%             pNew.setWeight(1);
%             pOld.setWeight(1);

%             if (mesh.numLocalElements ~= numel(pNew))
%                 prodJ = mesh.old.prodJ;
%                 WJ = zeros(numel(mesh.disc.W),numel(prodJ));
%                 for i=1:numel(prodJ)
%                     WJ(:,i) = mesh.disc.W / prodJ(i);
%                 end
%             else
                prodJ = mesh.getProdJ;
                WJ = zeros(numel(mesh.disc.W),numel(prodJ));
                for i=1:numel(prodJ)
                    WJ(:,i) = mesh.disc.W / prodJ(i);
                end
%             end

            if ~isequal( numel(prodJ),numel(pNew) )
                save(['scalarL2NormTest.' int2str(System.rank) '.mat']); 
                error('LSQDIM:Norm','The number of elements in the mesh is not equal to the number of elements in the vector!');
            end
            
            for i=1:numel(pNew)
                norm = norm + ((pNew{i}-pOld{i}).^2)' * WJ(:,i);
                normNew = normNew + (pNew{i}.^2)' * WJ(:,i);
                normNew1 = normNew1 + abs(pNew{i})' * WJ(:,i);
                maxValue = max(maxValue, max(abs(pNew{i})) );
                normOld = normOld + (pOld{i}.^2)' * WJ(:,i);
                normOld1 = normOld1 + abs(pOld{i})' * WJ(:,i);
                %normArray(i) = sqrt( sum((pNew{i}(:)-pOld{i}(:)).*standardElement.W.^2 ) ) / max( 1, sqrt(sum( (pNew{i}(:)).^2 )) );
            end
            
%             pNew.setWeight(w1);
%             pOld.setWeight(w2);
            
            if (numel(norm)==1)
                norm    = NMPI.Allreduce(norm,1,'+','scalarL2NormTest :: norm');
            else
                error('LSQDIM:Norm','Wrong number of elements in norm (%d elements where 1 is expected)',numel(maxValue));
            end
            
            if (numel(normNew)==1)
                normNew = NMPI.Allreduce(normNew,1,'+','scalarL2NormTest :: normNew');
            else
                error('LSQDIM:Norm','Wrong number of elements in normNew (%d elements where 1 is expected)',numel(maxValue));
            end
                               
            if (numel(maxValue)==1)
                maxValue = NMPI.Allreduce(maxValue,1,'M','scalarL2NormTest :: nmaxValue');
            else
                error('LSQDIM:Norm','Wrong number of elements in maxValue (%d elements where 1 is expected)',numel(maxValue));
            end
            
%             if (normNew<1e-3)
%                 % current norm is ok
%                 norm = normNew;
%             else
            if (normNew~=0)
                norm = sqrt( norm / (1+normNew) );
            else
                norm = 0;
            end
%             end
            
%             norm = max(normArray);
%             
%             norm = NMPI_Allreduce(norm,1,'M',NMPI.instance.mpi_comm_world);
        end      
        
        % Function to compute the relative L2 norm of two fields
        function [norms, maxValue] = scalarL2Norm(newField,oldField,value)            
            % Check if the subFields have identical Lv array (which
            % indicates both underlying meshes are identical). If so,
            % assign the mesh to one of them, otherwise some more advanced
            % operations have to be performed
            if (~isempty(oldField))
                if ( ~isequal( oldField.meshID,newField.meshID ) )
                    oldField.meshUpdate(newField.mesh)                
                end
                mesh = newField.mesh;

                pNew = newField.(value);
                pOld = oldField.(value);
                
            else
                pNew = newField.(value);
                for i=1:numel(pNew)
                    pOld{i,1} = 0*pNew{i};
                end
            end
            
            if ( ~isequal( size(pNew),size(pOld)) )
                error('the number of elements in computeL2Norm is not identical')
            end

            delta    = 0;
            L1norm   = 0;
            L2norm   = 0;
            minValue = NaN;
            maxValue = NaN;
            %integral = 0;
                        
            % Loop over the elements of the subField
            i = 0;
            for e=1:numel(pNew)
                i = i+1;
                
                element = newField.mesh.getElement(e);
                WJ = element.getW;
               
                % Compute the difference between the New and Old
                diff = pNew{i}(:)-pOld{i}(:);
                
                % L1 : Compute the absolute difference and the integral
                L1norm   = L1norm + dot( abs(diff), WJ );
                
                % L2 : Compute the  delta-integral and the L2norm
                delta    = delta  + dot( (diff.^2), WJ );
                L2norm   = L2norm + dot( (pNew{i}(:).^2), WJ );
                
%                 % H1 : Compute the
%                 for d=1:newField.mesh.dim
%                     H1 = 0
%                 end
                
                % Min/max values
                minValue = min(minValue, min(pNew{i}(:)) );
                maxValue = max(maxValue, max(pNew{i}(:)) );
            end
            
            % Synchronize parallel results
            if (numel(delta)==1)
                delta = NMPI.Allreduce(delta,1,'+','scalarL2Norm : delta');
            else
                error('LSQDIM:Norm','Wrong number of elements in delta (%d elements where 1 is expected)',numel(delta));
            end
            
            if (numel(L1norm)==1)
                L1norm = NMPI.Allreduce(L1norm,1,'+','scalarL2Norm : L1norm');
            else
                error('LSQDIM:Norm','Wrong number of elements in L1norm (%d elements where 1 is expected)',numel(L1norm));
            end
            
            if (numel(L2norm)==1)
                L2norm = NMPI.Allreduce(L2norm,1,'+','scalarL2Norm : L2norm');
            else
                error('LSQDIM:Norm','Wrong number of elements in L2norm (%d elements where 1 is expected)',numel(L2norm));
            end
                               
            if (numel(maxValue)==1)
                maxValue = NMPI.Allreduce(maxValue,1,'M','scalarL2Norm : maxValue');
            else
                error('LSQDIM:Norm','Wrong number of elements in maxValue (%d elements where 1 is expected)',numel(maxValue));
            end
            
            if (numel(minValue)==1)
                minValue = NMPI.Allreduce(minValue,1,'N','scalarL2Norm : minValue');
            else
                error('LSQDIM:Norm','Wrong number of elements in minValue (%d elements where 1 is expected)',numel(minValue));
            end
            
            % Do some post computations on the norms
            L2norm = sqrt(L2norm);            
            if (L2norm~=0)
                relNorm = sqrt(delta ) / max(L2norm,1);
            else
                relNorm = 0;
            end
            
            % Save the norms for return
            norms.L1        = L1norm;
            norms.L2        = sqrt( delta );
%             norms.H1        = sqrt( H1 );
            norms.L2rel     = relNorm;
            norms.minValue  = minValue;
            norms.maxValue  = maxValue;
        end
        
        function norm2 = squaredNorm( field1, field2, value )
            % Function to return the squared L2norm (so it still needs a
            % sqrt operation!)
            
            % Initialize f1 and f2
            f1 = field1.(value);
            f2 = 0;
            
            % Check if the subFields have identical meshIDs (Lv array, which
            % indicates if both underlying meshes are identical).
            if (~isempty(field2))
                if ( ~isequal( field2.meshID,field1.meshID ) )
                    % if the meshes differ, use the mesh of field1 to
                    % update the mesh of field2
                    field2.meshUpdate(field1.mesh)                
                end
                
                f2 = field2.(value);    
                
                if ( ~isequal( size(f1),size(f2)) )
                    error('the number of elements in computeL2Norm is not identical')
                end
            end
            
            e2    = 0;
            norm2 = 0;
                        
            % Loop over the elements of the subField
            i = 0;
            for e=1:numel(f1)
                i = i+1;
                
                element = field1.mesh.getElement(e);
                WJ = element.getW;
               
                % Compute the squared values for each quadrature point
                if (iscell(f2))
                    e2 = (f1{i}(:)-f2{i}(:)).^2;
                else
                    e2 = (f1{i}(:)).^2;
                end
                
                % Compute the squared norm integral, add to current norm2
                norm2 = norm2  + dot( e2, WJ );
            end
            
            % Synchronize parallel results
            if (numel(norm2)==1)
                norm2 = NMPI.Allreduce(norm2,1,'+','squaredNorm');
            else
                error('LSQDIM:Norm','Wrong number of elements in norm2 (%d elements where 1 is expected)',numel(norm2));
            end
        end
        
    end
end

