classdef FEBoundary < Boundary
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Access=protected)
        base;
        flag;
    end
    
    properties
        W; opL, opG; id; indq;
        patchList;
        weight; 
        requiresNonLinearUpdate = false;
        singleL = false;
    end
    
    methods
        function s = FEBoundary(id,patchList,flag)
            
            s.id = id;
            s.patchList = patchList;
               
            s.numElements = 0;
            
            s.flag = flag;
            
            if (flag=='|')
                for i=1:numel(patchList)
                    s.numElements = s.numElements + patchList{i}.numElements;
                end
                
            elseif (flag=='&')
                if (numel(patchList)>1)
                    % Find intersection of boundaries
                    if (numel(patchList)==2)
                        elements1 = patchList{1}.elements;
                        elements2 = patchList{2}.elements;

                        H1 = patchList{1}.Hpos; % H matrix of boundary 1
                        H2 = patchList{2}.Hpos; % H matrix of boundary 2                        
                        
                        D1 = patchList{1}.Dpos; % D matrix of boundary 1
                        D2 = patchList{2}.Dpos; % D matrix of boundary 2

                        s.elements = intersect(elements1,elements2);
                        
                        s.Hpos     = intersect(H1,H2);
                        s.Dpos     = intersect(D1,D2);
                    else
                        error('More than 2 boundaries are not supported yet in FEBoundary')
                    end
                else
                    error('Please provide at least 2 boundaries when using the &-flag for the FEBoundary')
                end                
            else
                error('Please provide a valid flag for the FEBoundary')                
            end
            
            s.base      = patchList{1};
            
            s.dir       = abs( s.base.dir );
            
            s.setJacobian;
        end
        
        function init(s,nVar)
            s.opL = cell(s.base.numElements,nVar);
            s.opG = cell(s.base.numElements,1);
            s.W   = cell(s.base.numElements,1);
        end
        
        function set.singleL(s,flag)
            s.singleL = flag;
        end
        
        function initElementArray(s,e,m,n)
            for v=1:size(s.opL,2)
                s.opL{e,v} = sparse(m,n);
            end
            s.opG{e} = sparse(m,1);
            s.W{e}   = sparse(m,1);
        end
        
        % Value boundary condition (Dirichlet)
        function setValue(s,be,element,variable,valueVector,mesh,e)
            
            if (s.patchList{1}.spaceDimension==2)
                indq    = s.patchList{1}.nodes{1};
                dir     = s.patchList{1}.dir;
                W       = element.finite.W2{ abs(dir) };
                Jt     	= element.J( abs( dir  ) );   %  s.patchList{1}.edgeList{be}.J;
                
            elseif (s.patchList{1}.spaceDimension==1)
                
                if ( s.patchList{1}.byTag )
                    indq      = s.patchList{1}.nodeList{be};
                    dir       = s.patchList{1}.dir(be);
                    normal    = s.patchList{1}.normal(be);
                    Jn        = element.J( abs(normal) );
                    Jt        = element.J( abs( dir  ) );   %  s.patchList{1}.edgeList{be}.J;
                    
                else
                    indq    = s.patchList{1}.nodes{1};
                    dir     = s.dir;
                    Jt      = s.Jt(be);
                    Jn      = s.Jn(be);
                end
                
                if (mesh.dim==3)
                    W = element.finite.W1{s.dir};
                    error('Wrong weights')
                elseif (mesh.dim==2)
                    W = element.finite.W1{ abs(dir) };
                end
                
            elseif (s.patchList{1}.spaceDimension==0)
                indq = s.patchList{1}.nodes{1};
                W = 1;
                Jt = 1;
            else
                error('There is an error in FEBoundary.setDerivative :: unsupported spaceDimension')
                W = element.finite.W( indq(:) );
            end
            
            H = element.finite.H( indq(:),: );
            
            s.opL{be,variable}( indq(:), : ) = H * mesh.Z{e};                         % H : unit operator
            try
                s.W{be}( indq(:) )               = W.*Jt;
            catch MExp
                error('FEBoundary :: setValue')
            end
            s.opG{be}( indq(:) )           	 = valueVector( indq(:) );
        end
        
        % First derivative boundary condition
        function setDerivative(s,be,element,variable,valueVector,mesh,e)
            
            if (s.patchList{1}.spaceDimension==2)
                indq = s.patchList{1}.nodes{1};
                W = element.finite.W2{s.dir};
                normal = s.dir;
                
                Jt = element.J/element.J(s.dir);
                Jn = element.J(s.dir);
                
            elseif (s.patchList{1}.spaceDimension==1)
                
                if ( s.patchList{1}.byTag )
                    %edgeArray = [s.patchList{1}.edgeList{:}];
                    indq      = s.patchList{1}.nodeList{be};
                    dir       = s.patchList{1}.dir(be);
                    normal    = s.patchList{1}.normal(be);
                    Jn        = element.J( abs(normal) );
                    Jt        = element.J( abs( dir  ) );   %  s.patchList{1}.edgeList{be}.J;
                                        
                else
                    indq = s.patchList{1}.nodes{1};
                    dir = s.patchList{1}.dir;
                    if ( numel(s.patchList{1}.normal) > 1 )
                        normal = s.patchList{1}.normal(be);
                    else
                        normal = s.patchList{1}.normal;
                    end
                    Jt = s.Jt(be);
                    Jn = s.Jn(be);
                end
                
%                 if (s.dir==1)
%                     normal = 2;
%                 elseif (s.dir==2)
%                     normal = 1;
%                 else
%                     error('There is an error in FEBoundary.setDerivative :: unsupported direction')
%                 end
                W = element.finite.W1{ abs(dir) };
            else
                error('There is an error in FEBoundary.setDerivative :: unsupported spaceDimension')
                W = element.finite.W( indq(:) );
            end
            
            Dn  = element.finite.D{ abs(normal) }( indq(:),: );
            
            s.opL{be,variable}( indq(:), : ) = Dn / Jn * mesh.Z{e};                        % Dn : normal derivative operator
            s.W{be}( indq(:) )               = W.*Jt;
%             s.opG{be}( indq(:) )           	 = (normal) * valueVector( indq(:) ); % .* s.Jn(be);      % NB: Scaling by the normal Jacobian
            s.opG{be}( indq(:) )           	 = sign(normal) * valueVector( indq(:) ); % .* s.Jn(be);      % NB: Scaling by the normal Jacobian
        end
        
        % Special boundary condition for testing how the code works
        % The idea is to affect hardcoded variables
        function testBCX(s,be,element,variable,valueVector,mesh,e)
        % Author: Julian N. Garcia Hahn
        % Date: 27.04.2022
        
            if (s.patchList{1}.spaceDimension==2)
                indq    = s.patchList{1}.nodes{1};
                dir     = s.patchList{1}.dir;
                W       = element.finite.W2{ abs(dir) };
                Jt     	= element.J( abs( dir  ) );   %  s.patchList{1}.edgeList{be}.J;
                
            elseif (s.patchList{1}.spaceDimension==1)
                
                if ( s.patchList{1}.byTag )
                    indq      = s.patchList{1}.nodeList{be};
                    dir       = s.patchList{1}.dir(be);
                    normal    = s.patchList{1}.normal(be);
                    Jn        = element.J( abs(normal) );
                    Jt        = element.J( abs( dir  ) );   %  s.patchList{1}.edgeList{be}.J;
                    
                else
                    indq    = s.patchList{1}.nodes{1};
                    dir     = s.dir;
                    Jt      = s.Jt(be);
                    Jn      = s.Jn(be);
                end
                
                if (mesh.dim==3)
                    W = element.finite.W1{s.dir};
                    error('Wrong weights')
                elseif (mesh.dim==2)
                    W = element.finite.W1{ abs(dir) };
                end
                
            elseif (s.patchList{1}.spaceDimension==0)
                indq = s.patchList{1}.nodes{1};
                W = 1;
                Jt = 1;
            else
                error('There is an error in FEBoundary.setDerivative :: unsupported spaceDimension')
                W = element.finite.W( indq(:) );
            end
            
            H = element.finite.H( indq(:),: );
            Dx = element.finite.D{1}( indq(:),: ) / element.J(1);
            Dy = element.finite.D{2}( indq(:),: ) / element.J(2);
            RField  = FieldCollection.get('R');
            R       = RField.val2(element.id); R = R( indq(:));
            Rx      = RField.x2(element.id); Rx = Rx( indq(:));
            Ry      = RField.y2(element.id); Ry = Ry( indq(:));
            
            uField  = FieldCollection.get('u');
            u       = uField.val2(element.id); u = u( indq(:));
            ux      = uField.x2(element.id); ux = ux( indq(:));
            uy      = uField.y2(element.id); uy = uy( indq(:));
            
            vField  = FieldCollection.get('v');
            v       = vField.val2(element.id); v = v( indq(:));
            vx      = vField.x2(element.id); vx = vx( indq(:));
            vy      = vField.y2(element.id); vy = vy( indq(:));
            
            qField  = FieldCollection.get('q');
            q       = qField.val2(element.id); q = q( indq(:));
            qx      = qField.x2(element.id); qx = qx( indq(:));
            qy      = qField.y2(element.id); qy = qy( indq(:));
            
            TField  = FieldCollection.get('T');
            T       = TField.val2(element.id); T = T( indq(:));
            
            ReI = 1/System.settings.phys.Re;
            WeI = 1/System.settings.phys.We;
            
            % Rx
            
            R_x = WeI*(-Ry .* Dx);
            
            % Ry
            
            R_y = WeI*(-q.*H + Ry.*Dy - Rx.*Dx) + (((3*T)./(3-R))-6*R).*H;
            
            % Ux
            
            U_x = ReI*(-Dy);
            
            % Uy
            
            U_y = ReI*((2/3)*Dx);
            
            % Vx
            
            V_x = ReI*(-Dx);
            
            % Vy
            
            V_y = ReI*(-(4/3)*Dy);
            
            % q_x = 0
            
            
            
            % q_y
            
            q_y = -WeI * R .* H;
            
            % B_x
            
            B_x = WeI*(-Rx.*Ry);
            
            % B_y
            
            B_y = WeI*(q.*R - 0.5 * Ry .* Ry + 0.5 * Rx .* Rx) + 3 * R .* R;
            
            s.opL{be,1}( indq(:), : ) = R_x * mesh.Z{e};
            s.opL{be,2}( indq(:), : ) = U_x * mesh.Z{e};
            s.opL{be,3}( indq(:), : ) = V_x * mesh.Z{e};
            %s.opL{be,4}( indq(:), : ) =  * mesh.Z{e};
            try
                s.W{be}( indq(:) )               = W.*Jt;
            catch MExp
                error('FEBoundary :: setValue')
            end
            s.opG{be}( indq(:) )           	 = B_x;
        end
        
        % Special boundary condition for testing how the code works
        % The idea is to affect hardcoded variables
        function testBCY(s,be,element,variable,valueVector,mesh,e)
        % Author: Julian N. Garcia Hahn
        % Date: 28.04.2022
        
            if (s.patchList{1}.spaceDimension==2)
                indq    = s.patchList{1}.nodes{1};
                dir     = s.patchList{1}.dir;
                W       = element.finite.W2{ abs(dir) };
                Jt     	= element.J( abs( dir  ) );   %  s.patchList{1}.edgeList{be}.J;
                
            elseif (s.patchList{1}.spaceDimension==1)
                
                if ( s.patchList{1}.byTag )
                    indq      = s.patchList{1}.nodeList{be};
                    dir       = s.patchList{1}.dir(be);
                    normal    = s.patchList{1}.normal(be);
                    Jn        = element.J( abs(normal) );
                    Jt        = element.J( abs( dir  ) );   %  s.patchList{1}.edgeList{be}.J;
                    
                else
                    indq    = s.patchList{1}.nodes{1};
                    dir     = s.dir;
                    Jt      = s.Jt(be);
                    Jn      = s.Jn(be);
                end
                
                if (mesh.dim==3)
                    W = element.finite.W1{s.dir};
                    error('Wrong weights')
                elseif (mesh.dim==2)
                    W = element.finite.W1{ abs(dir) };
                end
                
            elseif (s.patchList{1}.spaceDimension==0)
                indq = s.patchList{1}.nodes{1};
                W = 1;
                Jt = 1;
            else
                error('There is an error in FEBoundary.setDerivative :: unsupported spaceDimension')
                W = element.finite.W( indq(:) );
            end
            
            H = element.finite.H( indq(:),: );
            Dx = element.finite.D{1}( indq(:),: ) / element.J(1);
            Dy = element.finite.D{2}( indq(:),: ) / element.J(2);
            RField  = FieldCollection.get('R');
            R       = RField.val2(element.id); R = R( indq(:));
            Rx      = RField.x2(element.id); Rx = Rx( indq(:));
            Ry      = RField.y2(element.id); Ry = Ry( indq(:));
            
            uField  = FieldCollection.get('u');
            u       = uField.val2(element.id); u = u( indq(:));
            ux      = uField.x2(element.id); ux = ux( indq(:));
            uy      = uField.y2(element.id); uy = uy( indq(:));
            
            vField  = FieldCollection.get('v');
            v       = vField.val2(element.id); v = v( indq(:));
            vx      = vField.x2(element.id); vx = vx( indq(:));
            vy      = vField.y2(element.id); vy = vy( indq(:));
            
            qField  = FieldCollection.get('q');
            q       = qField.val2(element.id); q = q( indq(:));
            qx      = qField.x2(element.id); qx = qx( indq(:));
            qy      = qField.y2(element.id); qy = qy( indq(:));
            
            TField  = FieldCollection.get('T');
            T       = TField.val2(element.id); T = T( indq(:));
            
            ReI = 1/System.settings.phys.Re;
            WeI = 1/System.settings.phys.We;
            
            % Rx
            
            R_x = WeI*(-Ry .* Dx);
            
            % Ry
            
            R_y = WeI*(-q.*H + Ry.*Dy - Rx.*Dx) + (((3*T)./(3-R))-6*R).*H;
            
            % Ux
            
            U_x = ReI*(-Dy);
            
            % Uy
            
            U_y = ReI*((2/3)*Dx);
            
            % Vx
            
            V_x = ReI*(-Dx);
            
            % Vy
            
            V_y = ReI*(-(4/3)*Dy);
            
            % q_x = 0
            
            
            
            % q_y
            
            q_y = -WeI * R .* H;
            
            % B_x
            
            B_x = WeI*(-Rx.*Ry);
            
            % B_y
            
            B_y = WeI*(q.*R - 0.5 * Ry .* Ry + 0.5 * Rx .* Rx) + 3 * R .* R;
            
            s.opL{be,1}( indq(:), : ) = R_y * mesh.Z{e};
            s.opL{be,2}( indq(:), : ) = U_y * mesh.Z{e};
            s.opL{be,3}( indq(:), : ) = V_y * mesh.Z{e};
            s.opL{be,4}( indq(:), : ) = q_y * mesh.Z{e};
            try
                s.W{be}( indq(:) )               = W.*Jt;
            catch MExp
                error('FEBoundary :: setValue')
            end
            s.opG{be}( indq(:) )           	 = B_y;
        end
        
        % Special boundary condition for testing how the code works
        % The idea is to affect hardcoded variables
        function testBCT(s,be,element,variable,valueVector,mesh,e)
        % Author: Julian N. Garcia Hahn
        % Date: 28.04.2022
        
            if (s.patchList{1}.spaceDimension==2)
                indq    = s.patchList{1}.nodes{1};
                dir     = s.patchList{1}.dir;
                W       = element.finite.W2{ abs(dir) };
                Jt     	= element.J( abs( dir  ) );   %  s.patchList{1}.edgeList{be}.J;
                
            elseif (s.patchList{1}.spaceDimension==1)
                
                if ( s.patchList{1}.byTag )
                    indq      = s.patchList{1}.nodeList{be};
                    dir       = s.patchList{1}.dir(be);
                    normal    = s.patchList{1}.normal(be);
                    Jn        = element.J( abs(normal) );
                    Jt        = element.J( abs( dir  ) );   %  s.patchList{1}.edgeList{be}.J;
                    
                else
                    indq    = s.patchList{1}.nodes{1};
                    dir     = s.dir;
                    Jt      = s.Jt(be);
                    Jn      = s.Jn(be);
                end
                
                if (mesh.dim==3)
                    W = element.finite.W1{s.dir};
                    error('Wrong weights')
                elseif (mesh.dim==2)
                    W = element.finite.W1{ abs(dir) };
                end
                
            elseif (s.patchList{1}.spaceDimension==0)
                indq = s.patchList{1}.nodes{1};
                W = 1;
                Jt = 1;
            else
                error('There is an error in FEBoundary.setDerivative :: unsupported spaceDimension')
                W = element.finite.W( indq(:) );
            end
            
            H = element.finite.H( indq(:),: );
            Dx = element.finite.D{1}( indq(:),: ) / element.J(1);
            Dy = element.finite.D{2}( indq(:),: ) / element.J(2);
            RField  = FieldCollection.get('R');
            R       = RField.val2(element.id); R = R( indq(:));
            Rx      = RField.x2(element.id); Rx = Rx( indq(:));
            Ry      = RField.y2(element.id); Ry = Ry( indq(:));
            
            uField  = FieldCollection.get('u');
            u       = uField.val2(element.id); u = u( indq(:));
            ux      = uField.x2(element.id); ux = ux( indq(:));
            uy      = uField.y2(element.id); uy = uy( indq(:));
            
            vField  = FieldCollection.get('v');
            v       = vField.val2(element.id); v = v( indq(:));
            vx      = vField.x2(element.id); vx = vx( indq(:));
            vy      = vField.y2(element.id); vy = vy( indq(:));
            
            qField  = FieldCollection.get('q');
            q       = qField.val2(element.id); q = q( indq(:));
            qx      = qField.x2(element.id); qx = qx( indq(:));
            qy      = qField.y2(element.id); qy = qy( indq(:));
            
            TField  = FieldCollection.get('T');
            T       = TField.val2(element.id); T = T( indq(:));
            
            ReI = 1/System.settings.phys.Re;
            WeI = 1/System.settings.phys.We;
            
            % Inside Reynolds term
            
            Re_term = (-1) * ((4/3) * vy - (2/3) * ux);
            We_term = (-1) * ( R.*q + Rx.*Rx / 2 - Ry.*Ry / 2 );
            P_term = (-1) * ( 3 * R.*R );
            
            L = (3 * R) ./ (R - 3) .* H;
            
            
            s.opL{be,1}( indq(:), : ) = L * mesh.Z{e};
            s.W{be}( indq(:) ) = W.*Jt;
            s.opG{be}(indq(:)) = ReI * Re_term + WeI * We_term + P_term;
        end
        
        % First derivative boundary condition
        function setDerivativeTangential(s,be,element,variable,valueVector,mesh,e)
            indq = s.patchList{1}.nodes{1};
            
            if (s.patchList{1}.spaceDimension==2)
%                 W = element.finite.W2{s.dir};
%                 normal = s.dir;
                error('Unsupported 2D element in FEBoundary.setDerivativeTangential')
            elseif (s.patchList{1}.spaceDimension==1)
                dir = s.patchList{1}.dir;
                normal = s.patchList{1}.normal;
                W   = element.finite.W1{ abs(dir) };
            else
                error('There is an error in FEBoundary.setDerivative :: unsupported spaceDimension')
                W = element.finite.W( indq(:) );
            end
            
            Dt  = element.finite.D{ abs(dir) }( indq(:),: );
            Dn  = element.finite.D{ abs(normal) }( indq(:),: );
            
%             s.opL{be,variable}( indq(:), : ) = Dn / s.Jn(be) * mesh.Z{e};                        % Dn : normal derivative operator
%             s.W{be}( indq(:) )               = W.*(s.Jn(be));
            
            s.opL{be,variable}( indq(:), : ) = ( Dn / s.Jn(be) * mesh.Z{e} ); % - Dt / s.Jt(be) * mesh.Z{e} );                        % Dn : normal derivative operator
            s.W{be}( indq(:) )               = W.*(s.Jt(be));
%             s.opG{be}( indq(:) )           	 = (normal) * valueVector( indq(:) ); % .* s.Jn(be);      % NB: Scaling by the normal Jacobian
            s.opG{be}( indq(:) )           	 = valueVector( indq(:) );
        end
        
        % Special boundary condition for isolating part of the left boundary for the Flimwise
        % Condensation problem
        function setPartialDirichletPartialNeumann(s,be,element,variable,valueVector,mesh,e)
        % Author: Julian N. Garcia Hahn
        % Date: 18.03.2022
            
            if (s.patchList{1}.spaceDimension==2)
                indq = s.patchList{1}.nodes{1};
                W = element.finite.W2{s.dir};
                normal = s.dir;
                
                Jt = element.J/element.J(s.dir);
                Jn = element.J(s.dir);
                
            elseif (s.patchList{1}.spaceDimension==1)
                
                if ( s.patchList{1}.byTag )

                    indq      = s.patchList{1}.nodeList{be};
                    dir       = s.patchList{1}.dir(be);
                    normal    = s.patchList{1}.normal(be);
                    Jn        = element.J( abs(normal) );
                    Jt        = element.J( abs( dir  ) );
                                        
                else
                    indq = s.patchList{1}.nodes{1};
                    dir = s.patchList{1}.dir;
                    if ( numel(s.patchList{1}.normal) > 1 )
                        normal = s.patchList{1}.normal(be);
                    else
                        normal = s.patchList{1}.normal;
                    end
                    Jt = s.Jt(be);
                    Jn = s.Jn(be);
                end
                

                W = element.finite.W1{ abs(dir) };
            else
                error('There is an error in FEBoundary.setDerivative :: unsupported spaceDimension')
                W = element.finite.W( indq(:) );
            end
            y_th = System.settings.phys.y_th;
            [~,Yc,~,~] = element.getNodes();
            Yc = squeeze(Yc);
            
            H = element.finite.H( indq(:),: );
            Dn  = element.finite.D{ abs(normal) }( indq(:),: );
            if (any(any(any(Yc<=y_th))))
            s.opL{be,variable}( indq(:), : ) = H * mesh.Z{e}; % H : unit operator
            s.W{be}( indq(:) )               = W.*Jt;
            s.opG{be}( indq(:) )           	 = valueVector( indq(:) );
            else
            s.opL{be,variable}( indq(:), : ) = Dn / Jn * mesh.Z{e}; % Dn : normal derivative operator
            s.W{be}( indq(:) )               = W.*Jt;
            s.opG{be}( indq(:) )           	 = sign(normal) * valueVector( indq(:) ) * 0; % .* s.Jn(be);      % NB: Scaling by the normal Jacobian
            end
        end % function setPartialDirichletPartialNeumann
        
        % First derivative boundary condition
        function setGradC(s,be,element,variable,valueVector,mesh,e)
            
            if (s.patchList{1}.spaceDimension==2)
                indq = s.patchList{1}.nodes{1};
                W = element.finite.W2{s.dir};
                normal = s.dir;
                
                Jt = s.Jt(be);
                Jn = s.Jn(be);
                
            elseif (s.patchList{1}.spaceDimension==1)
                
                if ( s.patchList{1}.byTag )
                    %edgeArray = [s.patchList{1}.edgeList{:}];
                    indq      = s.patchList{1}.nodeList{be};
                    dir       = s.patchList{1}.dir(be);
                    normal    = s.patchList{1}.normal(be);
                    Jn        = element.J( abs(normal) );
                    Jt        = element.J( abs( dir  ) );   %  s.patchList{1}.edgeList{be}.J;
                                        
                else
                    indq = s.patchList{1}.nodes{1};
                    dir = s.patchList{1}.dir;
                    normal = s.patchList{1}.normal;
                    Jt = s.Jt(be);
                    Jn = s.Jn(be);
                end
                
%                 if (s.dir==1)
%                     normal = 2;
%                 elseif (s.dir==2)
%                     normal = 1;
%                 else
%                     error('There is an error in FEBoundary.setDerivative :: unsupported direction')
%                 end
                W = element.finite.W1{ abs(dir) };
            else
                error('There is an error in FEBoundary.setDerivative :: unsupported spaceDimension')
                W = element.finite.W( indq(:) );
            end
            
            %qNodes = s.patchList{1}.nodes{1};
            
%             if (s.patchList{1}.spaceDimension==1)
%                 dir     = s.patchList{1}.dir;               % direction of the element
%                 normal  = s.patchList{1}.normal;            % normal direction of the element
%                 We      = element.finite.W1{ abs(dir) };    % quadrature weights along the element
%             else
%                 error('Unsupported element dimension in FEBoundary.setDerivativeDynamicAngleY')
%             end
            
            % Get the cField and extract the local Cx and Cy
            cField   = FieldCollection.get('c');
            c        = cField.val2(element.id);
            cx       = cField.x2(element.id);
            cy       = cField.y2(element.id);

            % Compute the local normal vector components
            normC = sqrt(cx.^2+cy.^2);
            nx = -cx ./ normC; nx = nx( indq(:) );
            ny = -cy ./ normC; ny = ny( indq(:) );
            
            if (abs(dir)==1)
                nn = ny;
                tt = nx;
            elseif (abs(dir)==2)
                nn = nx;
                tt = ny;
            end
            
            cValues = c( indq(:) );
            CH = PhysicsCollection.get('CH');
            gradC = cValues .* (1-cValues) ./ (sqrt(2)*CH.Cn_L);
                        
            % Set a specific contact angle
            %cosTheta = 1/3*sqrt(3) * ones( numel(indq),1 );           % exact 60 degrees
            %cosTheta = (mean(cx(indq))/mean(cy(indq))) * ones( numel(indq),1 );        % mean of cosTheta
            %cosTheta( normC<0.5 ) = 0;
            %else
            %    cosTheta = zeros( numel(indq),1 );
            %end
            
            % Get the elemental tangential (Dt) and normal (Dn) operators
            Dt  = element.finite.D{ abs(dir) }( indq(:),: );
            Dn  = element.finite.D{ abs(normal) }( indq(:),: );
            
            % Setup L, W and G for this boundary element
            s.opL{be,variable}( indq(:), : )  = ( Dt / Jt .* tt - Dn / Jn .* nn ) * mesh.Z{e};
            s.W{be}( indq(:) )                = W .* Jt;
            s.opG{be}( indq(:) )              = gradC; %valueVector( indq(:) ); % .* cosTheta( indq(:) );
        end
        
        % First derivative boundary condition
        function setDerivativeDynamicAngleX(s,be,element,variable,valueVector,mesh,e)
            indq = s.patchList{1}.nodes{1};
            
            if (s.patchList{1}.spaceDimension==2)
%                 W = element.finite.W2{s.dir};
%                 normal = s.dir;
                error('Unsupported 2D element in FEBoundary.setDerivativeTangential')
            elseif (s.patchList{1}.spaceDimension==1)
                dir = s.patchList{1}.dir;
                normal = s.patchList{1}.normal;
                W   = element.finite.W1{ abs(dir) };
            else
                error('There is an error in FEBoundary.setDerivative :: unsupported spaceDimension')
                W = element.finite.W( indq(:) );
            end
            
            CH = PhysicsCollection.get('CH');
            
            nx = 0;
            ny = 1;
            
            %if ( CH.maxLevel >0 && element.lvl>0 )
                % Only use dynamic interface normal if refinement is used
                % and the element level is larger than 0 (element is
                % refined)
                cField   = FieldCollection.get('c');
                cx       = cField.x2(element.localID);
                cy       = cField.y2(element.localID);
                cosTheta = cx ./ cy;
                
%                 cosTheta = 1/3*sqrt(3) * ones( numel(indq),1 );           % exact 60 degrees
                
                %cosTheta = (mean(cx(indq))/mean(cy(indq))) * ones( numel(indq),1 );        % mean of cosTheta
                
                normC = sqrt(cx.^2+cy.^2);
                
                nx = -cx ./ normC;
               	ny = -cy ./ normC;
                
                cosTheta( normC<0.5 ) = 0;
                
                nx = nx( indq(:) );
                ny = ny( indq(:) );
                
%                         datax = reshape(s.active.getValue(Derivative.x,e),element.finite.qSize);
%                         datay = reshape(s.active.getValue(Derivative.y,e),element.finite.qSize);
%                         norm = sqrt( datax.^2 + datay.^2 );
%                         nx = -datax ./ norm;
%                         ny = -datay ./ norm;
%                         dtang = -Cx .* ny + Cy .* nx;
                
            %else
            %    cosTheta = zeros( numel(indq),1 );
            %end
            
            Dt  = element.finite.D{ abs(dir) }( indq(:),: );
            Dn  = element.finite.D{ abs(normal) }( indq(:),: );
            
            
            
%             s.opL{be,variable}( indq(:), : ) = Dn / s.Jn(be) * mesh.Z{e};                        % Dn : normal derivative operator
%             s.W{be}( indq(:) )               = W.*(s.Jn(be));
            
            s.opL{be,variable}( indq(:), : ) = (-Dt / s.Jt(be) .* nx + Dn / s.Jn(be) .* ny ) * mesh.Z{e};       % - Dt / s.Jt(be) * mesh.Z{e} );                        % Dn : normal derivative operator
            s.W{be}( indq(:) )               = W.*(s.Jt(be));
%             s.opG{be}( indq(:) )           	 = (normal) * valueVector( indq(:) ); % .* s.Jn(be);      % NB: Scaling by the normal Jacobian
            s.opG{be}( indq(:) )           	 = valueVector( indq(:) ) .* cosTheta( indq(:) );
        end
        
        % First derivative boundary condition
        function setDerivativeDynamicAngle(s,be,element,variable,valueVector,mesh,e)
            
            if (s.patchList{1}.spaceDimension==2)
                indq = s.patchList{1}.nodes{1};
                W = element.finite.W2{s.dir};
                normal = s.dir;
                
                Jt = s.Jt(be);
                Jn = s.Jn(be);
                
            elseif (s.patchList{1}.spaceDimension==1)
                
                if ( s.patchList{1}.byTag )
                    %edgeArray = [s.patchList{1}.edgeList{:}];
                    indq      = s.patchList{1}.nodeList{be};
                    dir       = s.patchList{1}.dir(be);
                    normal    = s.patchList{1}.normal(be);
                    Jn        = element.J( abs(normal) );
                    Jt        = element.J( abs( dir  ) );   %  s.patchList{1}.edgeList{be}.J;
                                        
                else
                    indq = s.patchList{1}.nodes{1};
                    dir = s.patchList{1}.dir;
                    normal = s.patchList{1}.normal;
                    Jt = s.Jt(be);
                    Jn = s.Jn(be);
                end
                
%                 if (s.dir==1)
%                     normal = 2;
%                 elseif (s.dir==2)
%                     normal = 1;
%                 else
%                     error('There is an error in FEBoundary.setDerivative :: unsupported direction')
%                 end
                W = element.finite.W1{ abs(dir) };
            else
                error('There is an error in FEBoundary.setDerivative :: unsupported spaceDimension')
                W = element.finite.W( indq(:) );
            end
            
            %qNodes = s.patchList{1}.nodes{1};
            
%             if (s.patchList{1}.spaceDimension==1)
%                 dir     = s.patchList{1}.dir;               % direction of the element
%                 normal  = s.patchList{1}.normal;            % normal direction of the element
%                 We      = element.finite.W1{ abs(dir) };    % quadrature weights along the element
%             else
%                 error('Unsupported element dimension in FEBoundary.setDerivativeDynamicAngleY')
%             end
            
            % Get the cField and extract the local Cx and Cy
            cField   = FieldCollection.get('c');
            cx       = cField.x2(element.id);
            cy       = cField.y2(element.id);

            % Compute the local normal vector components
            normC = sqrt(cx.^2+cy.^2);
            nx = -cx ./ normC; nx = nx( indq(:) );
            ny = -cy ./ normC; ny = ny( indq(:) );
            
            if (abs(dir)==1)
                nn = nx;
                tt = ny;
            elseif (abs(dir)==2)
                nn = ny;
                tt = nx;
            end
                        
            % Set a specific contact angle
            %cosTheta = 1/3*sqrt(3) * ones( numel(indq),1 );           % exact 60 degrees
            %cosTheta = (mean(cx(indq))/mean(cy(indq))) * ones( numel(indq),1 );        % mean of cosTheta
            %cosTheta( normC<0.5 ) = 0;
            %else
            %    cosTheta = zeros( numel(indq),1 );
            %end
            
            % Get the elemental tangential (Dt) and normal (Dn) operators
            Dt  = element.finite.D{ abs(dir) }( indq(:),: );
            Dn  = element.finite.D{ abs(normal) }( indq(:),: );
            
            % Setup L, W and G for this boundary element
            s.opL{be,variable}( indq(:), : )  = ( Dt / Jt .* tt - Dn / Jn .* nn ) * mesh.Z{e};
            s.W{be}( indq(:) )                = W .* Jt;
            s.opG{be}( indq(:) )              = valueVector( indq(:) ); % .* cosTheta( indq(:) );
        end

        % First derivative boundary condition
        function setDerivativeStaticAngle(s,be,element,variable,valueVector,mesh,e,angle)
            
            if (s.patchList{1}.spaceDimension==1)
                
                if ( s.patchList{1}.byTag )
                    %edgeArray = [s.patchList{1}.edgeList{:}];
                    indq      = s.patchList{1}.nodeList{be};
                    dir       = s.patchList{1}.dir(be);
                    normal    = s.patchList{1}.normal(be);
                    Jn        = element.J( abs(normal) );
                    Jt        = element.J( abs( dir  ) );   %  s.patchList{1}.edgeList{be}.J;
                                        
                else
                    indq = s.patchList{1}.nodes{1};
                    dir = s.patchList{1}.dir;               % direction of the element
                    normal = s.patchList{1}.normal;      	% normal direction of the element
                    Jt = s.Jt(be);
                    Jn = s.Jn(be);
                end
                
                W = element.finite.W1{ abs(dir) };          % quadrature weights along the element
            else
                error('Unsupported element dimension in FEBoundary.setDerivativeDynamicAngleY')
            end
            
            % Get the cField and extract the local Cx and Cy
            cField   = FieldCollection.get('c');
            cx       = cField.x2(element.id);
            cy       = cField.y2(element.id);

            % Compute the local normal vector components
            normC = sqrt(cx.^2+cy.^2);
            nx = -cx ./ normC; nx = nx( indq(:) );
            ny = -cy ./ normC; ny = ny( indq(:) );
            
            if ( all(nx < 0) )
              angle = pi-angle;
            end
            
            if (abs(dir)==1)
                %nn = nx;
                %tt = ny;
                nn = sin(angle);
                tt = cos(angle);
            elseif (abs(dir)==2)
                %nn = ny;
                %tt = nx;
                nn = -normal*sin(angle);
                tt = cos(angle);
            end
                        
            % Set a specific contact angle
            %cosTheta = 1/3*sqrt(3) * ones( numel(indq),1 );           % exact 60 degrees
            %cosTheta = (mean(cx(indq))/mean(cy(indq))) * ones( numel(indq),1 );        % mean of cosTheta
            %cosTheta( normC<0.5 ) = 0;
            %else
            %    cosTheta = zeros( numel(indq),1 );
            %end
            
            % Get the elemental tangential (Dt) and normal (Dn) operators
            Dt  = element.finite.D{ abs(dir) }( indq(:),: );
            Dn  = element.finite.D{ abs(normal) }( indq(:),: );
            
            % Setup L, W and G for this boundary element
            s.opL{be,variable}( indq(:), : )  = ( Dt / Jt .* tt - Dn / Jn .* nn ) * mesh.Z{e};
            s.W{be}( indq(:) )                = W .* Jt;
            s.opG{be}( indq(:) )              = valueVector( indq(:) ); % .* cosTheta( indq(:) );
        end
        
        % First derivative boundary condition
        function setDerivativeDynamicAngleY(s,be,element,variable,valueVector,mesh,e)
            qNodes = s.patchList{1}.nodes{1};
            
            if (s.patchList{1}.spaceDimension==1)
                dir     = s.patchList{1}.dir;
                normal  = s.patchList{1}.normal;
                We      = element.finite.W1{ abs(dir) };
            else
                error('Unsupported element dimension in FEBoundary.setDerivativeDynamicAngleY')
            end
            
            % Get the cField and extract the local Cx and Cy
            cField   = FieldCollection.get('c');
            cx       = cField.x2(element.id);
            cy       = cField.y2(element.id);

            % Compute the local normal vector components
            normC = sqrt(cx.^2+cy.^2);
            nx = -cx ./ normC; nx = nx( qNodes(:) );
            ny = -cy ./ normC; ny = ny( qNodes(:) );
            
            % Set a specific contact angle
            %cosTheta = 1/3*sqrt(3) * ones( numel(indq),1 );           % exact 60 degrees
            %cosTheta = (mean(cx(indq))/mean(cy(indq))) * ones( numel(indq),1 );        % mean of cosTheta
            %cosTheta( normC<0.5 ) = 0;
            %else
            %    cosTheta = zeros( numel(indq),1 );
            %end
            
            % Get the elemental tangential (Dt) and normal (Dn) operators
            Dt  = element.finite.D{ abs(dir) }( qNodes(:),: );
            Dn  = element.finite.D{ abs(normal) }( qNodes(:),: );
            
            % Setup L, W and G for this boundary element
            s.opL{be,variable}( qNodes(:), : )  = ( Dt / s.Jt(be) .* ny - Dn / s.Jn(be) .* nx ) * mesh.Z{e};
            s.W{be}( qNodes(:) )                = We .* s.Jt(be);
            s.opG{be}( qNodes(:) )              = valueVector( qNodes(:) ); % .* cosTheta( indq(:) );
        end
        
        
        % Functions from CH:

%         % See Ding & Spelt (2007)
%         %     Yue & Feng (2011)
%         %     Dong (2012)
%         %     Yue et al (2010)
%         function applyDynamicContactAngle_v1(s,boundaryNumber,field,theta_s,bcType)
%             boundary = s.mesh.getBoundary(boundaryNumber);
%             activeFEBoundary = s.boundary_v1{boundaryNumber};   
%                         
%             if (isempty(theta_s))
%                 theta_s = System.settings.phys.ContactAngle;
%             end
%             
%             switch bcType
%                 case BCType.Strong
%                     error('A strong version of the dynamic contact angle bc has not been implemented')
%                 
%                 case BCType.Weak
%                     
%                     %U = coupledPhysics{2}.getField(1,s);
%                     %V = coupledPhysics{2}.getField(2,s);
%                     
%                     U = FieldCollection.get('u');
%                     V = FieldCollection.get('v');
%                     
%                     for be=1:boundary.getNumElements
%                         
%                         %indp = (1:boundary.getElements(be).disc.dofev) + (variable-1)*boundary.getElements(be).disc.dofev;
%                         
%                         e = boundary.getElementNumbers(be);
%                                                 
%                         if ismember(e,s.mesh.eRange)
%                             
%                             element = s.mesh.getElement(e);
%                             indq    = boundary.getHposQ(element.disc.dofeq);        % these are logicals
% 
%                             indqD   = boundary.getDposQ(element.disc.dofeq);        % these are logicals
%                             
%                             if (isempty(activeFEBoundary.opL{be,field.getVariable}))
%                                 activeFEBoundary.opL{be,field.getVariable} = sparse(element.disc.dofevq,element.disc.dofev);
%                             end
%                             if (isempty(activeFEBoundary.opG{be,field.getVariable}))
%                                 activeFEBoundary.opG{be,field.getVariable} = sparse(element.disc.dofevq,1);
%                             end
%                             if (isempty(activeFEBoundary.W{be}))
%                                 activeFEBoundary.W{be}                     = sparse(element.disc.dofevq,1);
%                             end
%                             
%                             Dw = System.settings.phys.Dw;
%                             
%                             Uk      = U.val(e);
%                             Vk      = V.val(e);
%                             Conv    = ( Uk(indq).*boundary.Dx + Vk(indq).*boundary.Dy );
%                             
% %                             Ck = wi*C(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' + (1-wi)*Ci(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' ;
% 
%                             % NB: theta is in radians
%                             %
%                             % fw' = 6 * (C^2-C) * sigma * cos(theta)
%                             % lambda = 6*sqrt(2) * sigma / Cn
%                             % fw' / lambda = 0.5*sqrt(2) * (C^2-C) / Cn * cos(theta)
% 
%                             Ck = s.cField.val(e); %getC(e,SolutionMode.Nonlinear);
%                             
%                             factor      = -1/(sqrt(2)*System.settings.phys.Cn) * cos(theta_s/180*pi) .* (Ck(indq)-1); % 0 degrees: hydrophillic, 180 degrees: hydrophobic
%                             factorrhs   = -1/(sqrt(2)*System.settings.phys.Cn) * cos(theta_s/180*pi) .* Ck(indq) .* (Ck(indq)-1); % 0 degrees: hydrophillic, 180 degrees: hydrophobic
%                             
%                             % the additional 'Ck(indq)' is included by H
%                             
%                             %boundary.opL{be}( indq(:), indp(:) ) = ( -Dw*(boundary.Dz+Conv) + boundary.Dn + factor.*boundary.H )* s.mesh.getZ(e);
%                             % L = ( -Dw*(disc.By0.Dz*mesh.Jz(elem)+Conv) + disc.By0.D*mesh.Jy(elem) + (disc.By0.H)*0.5*sqrt(2)/Cn*cos(theta_s/180*pi)*sparse(diag(1-Ck')))
% 
%                             %mixingEnergy = System.settings.phys.alpha * System.settings.phys.Cn * System.settings.phys.sigma;
%                             
%                             valueVector = -1*factorrhs + 0*ones( nnz(indqD), 1 );
% 
%                             activeFEBoundary.opL{be,field.getVariable}( indqD(:), : ) = ( -Dw*(boundary.Dz+Conv) + boundary.Dn + 0*factor.*boundary.H )* s.mesh.getZ(e);
%                                                                                                                   %boundary.Dn * s.mesh.getZ(e);      % unit operator
%                             activeFEBoundary.W{be}( indqD(:) )                        = (boundary.Wdisc)./(boundary.J(be));
%                             activeFEBoundary.opG{be,field.getVariable}( indqD(:) )    = valueVector(:) / boundary.Jn(be); 
%                         end
%                     end
%             end
%         end
% 
%         % See Ding & Spelt (2007)
%         %     Yue & Feng (2011)
%         %     Dong (2012)
%         %     Yue et al (2010)
%         function applyDynamicContactAngle(s,bcID,boundaryNumber,field,theta_s,bcType)
% %             boundary = s.mesh.getBoundary(boundaryNumber);
%             activeFEBoundary = s.boundary{bcID};   
%                         
%             if (isempty(theta_s))
%                 theta_s = System.settings.phys.ContactAngle;
%             end
%             
%             switch bcType
%                 case BCType.Strong
%                     error('A strong version of the dynamic contact angle bc has not been implemented')
%                 
%                 case BCType.Weak
%                     
%                     % Initialize the LGW cell arrays of this weak FEBoundary
%                     activeFEBoundary.init(s.Nvar);
%                     
% %                     U = coupledPhysics{2}.getField(1,s);
% %                     V = coupledPhysics{2}.getField(2,s);
%                     U = FieldCollection.get('u');
%                     V = FieldCollection.get('v');
%                     
%                     for be=1:activeFEBoundary.getNumElements
%                         
%                         %indp = (1:boundary.getElements(be).disc.dofev) + (variable-1)*boundary.getElements(be).disc.dofev;
%                         
%                         e = activeFEBoundary.getElementNumbers(be);
%                                                 
%                         if ismember(e,s.mesh.eRange)
%                             
%                             element = s.mesh.getElement(e);
%                             indq    = activeFEBoundary.getHposQ(element.disc.dofeq);        % these are logicals
%                             indqD   = activeFEBoundary.getDposQ(element.disc.dofeq);        % these are logicals
%                             
%                             if (isempty(activeFEBoundary.opL{be,field.getVariable}))
%                                 activeFEBoundary.opL{be,field.getVariable} = sparse(element.disc.dofevq,element.disc.dofev);
%                             end
%                             if (isempty(activeFEBoundary.opG{be,field.getVariable}))
%                                 activeFEBoundary.opG{be,field.getVariable} = sparse(element.disc.dofevq,1);
%                             end
%                             if (isempty(activeFEBoundary.W{be}))
%                                 activeFEBoundary.W{be}                     = sparse(element.disc.dofevq,1);
%                             end
%                             
%                             Dw = System.settings.phys.Dw;
%                             
%                             Uk      = U.val(e);
%                             Vk      = V.val(e);
%                             Conv    = ( Uk(indq).*activeFEBoundary.Dx + Vk(indq).*activeFEBoundary.Dy );
%                             
% %                             Ck = wi*C(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' + (1-wi)*Ci(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' ;
% 
%                             % NB: theta is in radians
%                             %
%                             % fw' = 6 * (C^2-C) * sisogma * cos(theta)
%                             % lambda = 6*sqrt(2) * sigma / Cn
%                             % fw' / lambda = 0.5*sqrt(2) * (C^2-C) / Cn * cos(theta)
% 
%                             Ck = s.cField.val(e); %getC(e,SolutionMode.Nonlinear);
%                             
%                             factor      = -1/(sqrt(2)*System.settings.phys.Cn) * cos(theta_s/180*pi) .* (Ck(indq)-1); % 0 degrees: hydrophillic, 180 degrees: hydrophobic
%                             factorrhs   = -1/(sqrt(2)*System.settings.phys.Cn) * cos(theta_s/180*pi) .* Ck(indq) .* (Ck(indq)-1); % 0 degrees: hydrophillic, 180 degrees: hydrophobic
%                             
%                             dCkdx = s.cField.x(e);
%                             dCkdy = s.cField.y(e);
%                             
%                             gradC = sqrt(dCkdx.^2 + dCkdy.^2);
%                             
%                             %factorrhs   = cos(theta_s/180*pi) .* gradC(indq);
%                             
%                             % the additional 'Ck(indq)' is included by H
%                             
%                             %boundary.opL{be}( indq(:), indp(:) ) = ( -Dw*(boundary.Dz+Conv) + boundary.Dn + factor.*boundary.H )* s.mesh.getZ(e);
%                             % L = ( -Dw*(disc.By0.Dz*mesh.Jz(elem)+Conv) + disc.By0.D*mesh.Jy(elem) + (disc.By0.H)*0.5*sqrt(2)/Cn*cos(theta_s/180*pi)*sparse(diag(1-Ck')))
% 
%                             %mixingEnergy = System.settings.phys.alpha * System.settings.phys.Cn * System.settings.phys.sigma;
%                             
%                             valueVector = -1*factorrhs + 0*ones( nnz(indqD), 1 );
% 
%                             activeFEBoundary.opL{be,field.getVariable}( indqD(:), : ) = ( -Dw*(activeFEBoundary.Dz+Conv) + activeFEBoundary.Dn + 0*factor.*activeFEBoundary.H )* s.mesh.getZ(e);
%                                                                                                                   %boundary.Dn * s.mesh.getZ(e);      % unit operator
%                             activeFEBoundary.W{be}( indqD(:) )                        = (activeFEBoundary.Wdisc)./(activeFEBoundary.J(be));
%                             activeFEBoundary.opG{be,field.getVariable}( indqD(:) )    = valueVector(:) / activeFEBoundary.Jn(be); 
%                             
% %                             value = 0;
%                             
% %                             if (~iscell(value) && length(value)==1)
% %                                 boundary.opG{be}( indq(:) ) = value*ones(length(boundary.Wdisc),1);
% %                             elseif (iscell(value) && numel(value{1})==element.disc.dofevq)
% %                                 values = value{e}(:,:,end);
% %                                 boundary.opG{be}( indq(:) ) = values(:);
% %                             elseif (iscell(value) && numel(value{1})==numel(indq))
% %                                 boundary.opG{be}( indq(:) ) = value{e}(:);
% %                             else
% %                                 disp('check dirichlet in CH')
% %                             end
%                             
% %                             s.K{e}( ind,ind ) = s.K{e}(ind,ind) + weight * L' * W * L;
% %                             if length(value)==1
% %                                 s.G{e}(   ind   ) = s.G{e}(   ind   ) + weight * L' * W * value*ones(length(boundary.W),1);
% %                             else
% %                                 s.G{e}(   ind   ) = s.G{e}(   ind   ) + weight * L' * W * value{be}(:);
% %                             end
%                         end
%                     end
%                 end
% % % %             
% % % %             %- dPhi/dx=dOmg/dx=0 at y=y0(bottom)
% % % %             for ebc = 1:length(mesh.BCy0.elem)
% % % %                 elem = mesh.BCy0.elem(ebc) ;
% % % %                 dCbc = zeros(Qx*Qz,1) ;
% % % %                 dOmgbc = zeros(Qx*Qz,1) ;
% % % %                 indC = [1:mesh.dofev] ;
% % % %                 indOmg = [(mesh.dofev+1):2*mesh.dofev] ;
% % % %                 if elem>=e_start && elem<=e_end
% % % %                     Ck = wi*C(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' + (1-wi)*Ci(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' ;
% % % % 
% % % %                     Dw = 0.9;
% % % %                     %Uk = 0; % wc*U(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' + (1-wc)*Uc(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' ;
% % % %                     %Vk = 0; % wc*V(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' + (1-wc)*Vc(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' ;
% % % %                     %Conv    = ( diag(Uk)*disc.Dx*mesh.Jx(elem) + diag(Vk)*disc.Dy*mesh.Jy(elem) );
% % % %                     Conv = 0;
% % % % 
% % % %                     CH.Ke{elem-e_start+1}(indC,indC) = CH.Ke{elem-e_start+1}(indC,indC) + wbc* ( ( -Dw*(disc.By0.Dz'*mesh.Jz(elem)+Conv') + disc.By0.D'*mesh.Jy(elem) + 0.5*sqrt(2)/Cn*cos(theta_s/180*pi)*sparse(diag(1-Ck'))*disc.By0.H') * ...
% % % %                                                                                                       diag(disc.By0.p.w)./(mesh.Jx(elem)*mesh.Jz(elem))*...
% % % %                                                                                                      ( -Dw*(disc.By0.Dz*mesh.Jz(elem)+Conv) + disc.By0.D*mesh.Jy(elem) + (disc.By0.H)*0.5*sqrt(2)/Cn*cos(theta_s/180*pi)*sparse(diag(1-Ck'))) );                                                                                             
% % % % 
% % % % 
% % % %     %                 CH.Ke{elem-e_start+1}(indC,indC) = CH.Ke{elem-e_start+1}(indC,indC) + wbc* ( (disc.By0.D'*mesh.Jy(elem) + 0.5*sqrt(2)/Cn*cos(theta_s/180*pi)*sparse(diag(1-Ck'))*disc.By0.H') * ...
% % % %     %                                                                                               diag(disc.By0.p.w)./(mesh.Jx(elem)*mesh.Jz(elem))*...
% % % %     %                                                                                              (disc.By0.D*mesh.Jy(elem) + (disc.By0.H)*0.5*sqrt(2)/Cn*cos(theta_s/180*pi)*sparse(diag(1-Ck'))) );                                                                                             
% % % % 
% % % %     %                 CH.Ke{elem-e_start+1}(indC,indC)=CH.Ke{elem-e_start+1}(indC,indC) + (disc.By0.D'*mesh.Jy(elem))*diag(disc.By0.p.w)./(mesh.Jx(elem)*mesh.Jz(elem))*(disc.By0.D*mesh.Jy(elem))*wbc ;
% % % %                     CH.Ge{elem-e_start+1}(indC)=CH.Ge{elem-e_start+1}(indC) + (disc.By0.D'*mesh.Jy(elem))*diag(disc.By0.p.w)./(mesh.Jx(elem)*mesh.Jz(elem))*dCbc*wbc ;
% % % % 
% % % %     %                 Ck = wi*C(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' + (1-wi)*Ci(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' ;
% % % %     %                 CH.Ke{elem-e_start+1}(indC,indC) = CH.Ke{elem-e_start+1}(indC,indC) + wbc* ( (disc.By0.D'*mesh.Jy(elem) + 2*sqrt(2)/Cn*cos(theta_s/180*pi)*sparse(diag(1-Ck'))*disc.By0.H') * ...
% % % %     %                                                                                               diag(disc.By0.p.w)./(mesh.Jx(elem)*mesh.Jz(elem))*...
% % % %     %                                                                                              (disc.By0.D*mesh.Jy(elem) + (disc.By0.H)*2*sqrt(2)/Cn*cos(theta_s/180*pi)*sparse(diag(1-Ck'))) );
% % % %     %                 CH.Ge{elem-e_start+1}(indC)=CH.Ge{elem-e_start+1}(indC) + (disc.By0.D'*mesh.Jy(elem))*diag(disc.By0.p.w)./(mesh.Jx(elem)*mesh.Jz(elem))*dCbc*wbc ;
% % % % 
% % % %                     CH.Ke{elem-e_start+1}(indOmg,indOmg)=CH.Ke{elem-e_start+1}(indOmg,indOmg) + (disc.By0.D'*mesh.Jy(elem))*diag(disc.By0.p.w)./(mesh.Jx(elem)*mesh.Jz(elem))*(disc.By0.D*mesh.Jy(elem))*wbc ;
% % % %                     CH.Ge{elem-e_start+1}(indOmg)=CH.Ge{elem-e_start+1}(indOmg) + (disc.By0.D'*mesh.Jy(elem))*diag(disc.By0.p.w)./(mesh.Jx(elem)*mesh.Jz(elem))*dOmgbc*wbc ;
% % % %                 end
% % % %             end
%         end
%         
%         function applyDirichletContactAngle(s,boundaryNumber,field,theta_s,bcType)
%             boundary = s.mesh.getBoundary(boundaryNumber);
%             activeFEBoundary = s.boundary{boundaryNumber};   
%                         
%             if (isempty(theta_s))
%                 theta_s = System.settings.phys.ContactAngle;
%             end
%             
%             switch bcType
%                 case BCType.Strong
%                     error('A strong version of the dynamic contact angle bc has not been implemented')
%                 
%                 case BCType.Weak
%                     
%                     %C = coupledPhysics{1}.getField(1,s);
%                     
%                     %U = coupledPhysics{2}.getField(1,s);
%                     %V = coupledPhysics{2}.getField(2,s);
%                     
%                     C = FieldCollection.get('c');
%                     U = FieldCollection.get('u');
%                     V = FieldCollection.get('v');
%                     
%                     for be=1:boundary.getNumElements
%                         
%                         %indp = (1:boundary.getElements(be).disc.dofev) + (variable-1)*boundary.getElements(be).disc.dofev;
%                         
%                         e = boundary.getElementNumbers(be);
%                                                 
%                         if ismember(e,s.mesh.eRange)
%                             
%                             element = s.mesh.getElement(e);
%                             indq    = boundary.getHposQ(element.disc.dofeq);        % these are logicals
% 
%                             indqD   = boundary.getDposQ(element.disc.dofeq);        % these are logicals
%                             
%                             if (isempty(activeFEBoundary.opL{be,field.getVariable}))
%                                 activeFEBoundary.opL{be,field.getVariable} = sparse(element.disc.dofevq,element.disc.dofev);
%                             end
%                             if (isempty(activeFEBoundary.opG{be,field.getVariable}))
%                                 activeFEBoundary.opG{be,field.getVariable} = sparse(element.disc.dofevq,1);
%                             end
%                             if (isempty(activeFEBoundary.W{be}))
%                                 activeFEBoundary.W{be}                     = sparse(element.disc.dofevq,1);
%                             end
%                             
%                             Dw = System.settings.phys.Dw;
%                             
%                             Uk      = U.val(e);
%                             Vk      = V.val(e);
%                             Conv    = ( Uk(indq).*boundary.Dx + Vk(indq).*boundary.Dy );
%                             
% %                             Ck = wi*C(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' + (1-wi)*Ci(mesh.GMq{elem}(1:(Qx*Qy*Qz)))' ;
% 
%                             % NB: theta is in radians
%                             %
%                             % fw' = 6 * (C^2-C) * sigma * cos(theta)
%                             % lambda = 6*sqrt(2) * sigma / Cn
%                             % fw' / lambda = 0.5*sqrt(2) * (C^2-C) / Cn * cos(theta)
% 
%                             Ck = s.cField.val(e); %getC(e,SolutionMode.Nonlinear);
%                             Cxx = s.cField.xx(e);
%                             Cyy = s.cField.yy(e);
%                             
%                             %factor      = -1/(sqrt(2)*System.settings.phys.Cn) * cos(theta_s/180*pi) .* (Ck(indq)-1); % 0 degrees: hydrophillic, 180 degrees: hydrophobic
%                             %factorrhs   = -1/(sqrt(2)*System.settings.phys.Cn) * cos(theta_s/180*pi) .* Ck(indq) .* (Ck(indq)-1); % 0 degrees: hydrophillic, 180 degrees: hydrophobic
%                             
%                             % the additional 'Ck(indq)' is included by H
%                             
%                             %boundary.opL{be}( indq(:), indp(:) ) = ( -Dw*(boundary.Dz+Conv) + boundary.Dn + factor.*boundary.H )* s.mesh.getZ(e);
%                             % L = ( -Dw*(disc.By0.Dz*mesh.Jz(elem)+Conv) + disc.By0.D*mesh.Jy(elem) + (disc.By0.H)*0.5*sqrt(2)/Cn*cos(theta_s/180*pi)*sparse(diag(1-Ck')))
% 
%                             %mixingEnergy = System.settings.phys.alpha * System.settings.phys.Cn * System.settings.phys.sigma;
%                             
%                             %valueVector = -1*factorrhs + 0*ones( nnz(indqD), 1 );
%                             
%                             C   = Ck(indq);
%                             Cxx = Cxx(indq);
%                             Cyy = Cyy(indq);
%                             
%                             valueVector = System.settings.phys.alpha/System.settings.phys.Cn * ( (3*C.^3-3/2*C.^2+0.5.*C) - System.settings.phys.Cn^2 .* ( Cxx + Cyy ) );
% 
%                             activeFEBoundary.opL{be,field.getVariable}( indqD(:), : ) = ( -0*Dw*(boundary.Dz+Conv) + 0*boundary.Dn + boundary.H )* s.mesh.getZ(e);
%                                                                                                                   %boundary.Dn * s.mesh.getZ(e);      % unit operator
%                             activeFEBoundary.W{be}( indqD(:) )                        = (boundary.Wdisc)./(boundary.J(be));
%                             activeFEBoundary.opG{be,field.getVariable}( indqD(:) )    = valueVector(:) / boundary.Jn(be); 
%                             
% %                             value = 0;
%                             
% %                             if (~iscell(value) && length(value)==1)
% %                                 boundary.opG{be}( indq(:) ) = value*ones(length(boundary.Wdisc),1);
% %                             elseif (iscell(value) && numel(value{1})==element.disc.dofevq)
% %                                 values = value{e}(:,:,end);
% %                                 boundary.opG{be}( indq(:) ) = values(:);
% %                             elseif (iscell(value) && numel(value{1})==numel(indq))
% %                                 boundary.opG{be}( indq(:) ) = value{e}(:);
% %                             else
% %                                 disp('check dirichlet in CH')
% %                             end
%                             
% %                             s.K{e}( ind,ind ) = s.K{e}(ind,ind) + weight * L' * W * L;
% %                             if length(value)==1
% %                                 s.G{e}(   ind   ) = s.G{e}(   ind   ) + weight * L' * W * value*ones(length(boundary.W),1);
% %                             else
% %                                 s.G{e}(   ind   ) = s.G{e}(   ind   ) + weight * L' * W * value{be}(:);
% %                             end
%                         end
%                     end
%             end          
%         end

        
        
%         % Second derivative boundary condition
%         function setDerivative2(s,be,element,variable,valueVector,mesh,e)
% %             indq = s.QPos(element);
%             indq = s.patchList{1}.nodes{1};
%             
%             t{1} = [ 1 0 0 0 0 1];
%             t{2} = ones(1,6);
%             t{3} = ones(1,4);
%             
%             indq = superkron( t );
%             
%             indq = logical(indq);
%             
%             dir = 1;
%             
%             W    = element.finite.W( indq(:) );
%             Dnn  = element.finite.DD{dir}( indq(:),: );
%             
%             s.opL{be,variable}( indq(:), : ) = Dnn * mesh.Z{e};                     % Dnn : normal derivative operator
%             s.W{be}( indq(:) )               = W.*(s.J(be));
%             s.opG{be}( indq(:) )           	 = valueVector( indq(:) ) .* (s.Jn(be)).^2; % NB: Scaling by the normal Jacobian
%         end

        % Slip Boundary Condition using Ls quantity as slip length
        function setSlipBC(s,be,element,variable,valueVector,mesh,e)
        % Author: Julian N. Garcia Hahn
            
            if (s.patchList{1}.spaceDimension==2)
                indq    = s.patchList{1}.nodes{1};
                dir     = s.patchList{1}.dir;
                W       = element.finite.W2{ abs(dir) };
                Jt     	= element.J( abs( dir  ) );   %  s.patchList{1}.edgeList{be}.J;
                Jn      = element.J(s.dir);
                normal  = s.dir;
                
            elseif (s.patchList{1}.spaceDimension==1)
                
                if ( s.patchList{1}.byTag )
                    indq      = s.patchList{1}.nodeList{be};
                    dir       = s.patchList{1}.dir(be);
                    normal    = s.patchList{1}.normal(be);
                    Jn        = element.J( abs(normal) );
                    Jt        = element.J( abs( dir  ) );   %  s.patchList{1}.edgeList{be}.J;
                    
                else
                    indq    = s.patchList{1}.nodes{1};
                    dir     = s.dir;
                    Jt      = s.Jt(be);
                    Jn      = s.Jn(be);
                end
                
                if (mesh.dim==3)
                    W = element.finite.W1{s.dir};
                    error('Wrong weights')
                elseif (mesh.dim==2)
                    W = element.finite.W1{ abs(dir) };
                end
                
            elseif (s.patchList{1}.spaceDimension==0)
                indq = s.patchList{1}.nodes{1};
                W = 1;
                Jt = 1;
            else
                error('There is an error in FEBoundary.setDerivative :: unsupported spaceDimension')
                W = element.finite.W( indq(:) );
            end
            
            Ls = System.settings.phys.Ls;
            H = element.finite.H( indq(:),: );
            Dn = element.finite.D{ abs(normal) }(indq(:), :);
            s.opL{be,variable}( indq(:), : ) = (H - Ls * Dn / Jn)* mesh.Z{e};
            try
                s.W{be}( indq(:) )               = W.*Jt;
            catch MExp
                error('FEBoundary :: setValue')
            end
            s.opG{be}( indq(:) )           	 = valueVector( indq(:) );
        end
        
        % Second derivative boundary condition
        function setDerivative2(s,be,element,variable,valueVector,mesh,e)
            
            if (s.patchList{1}.spaceDimension==2)
                indq = s.patchList{1}.nodes{1};
                W = element.finite.W2{s.dir};
                normal = s.dir;
                
                Jt = s.Jt(be);
                Jn = s.Jn(be);
                
            elseif (s.patchList{1}.spaceDimension==1)
                
                if ( s.patchList{1}.byTag )
                    %edgeArray = [s.patchList{1}.edgeList{:}];
                    indq      = s.patchList{1}.nodeList{be};
                    dir       = s.patchList{1}.dir(be);
                    normal    = s.patchList{1}.normal(be);
                    Jn        = element.J( abs(normal) );
                    Jt        = element.J( abs( dir  ) );   %  s.patchList{1}.edgeList{be}.J;
                                        
                else
                    indq = s.patchList{1}.nodes{1};
                    dir = s.patchList{1}.dir;
                    normal = s.patchList{1}.normal;
                    Jt = s.Jt(be);
                    Jn = s.Jn(be);
                end
                W = element.finite.W1{ abs(dir) };
            else
                error('There is an error in FEBoundary.setDerivative :: unsupported spaceDimension')
                W = element.finite.W( indq(:) );
            end
            
            Dnn  = element.finite.DD{ abs(normal) }( indq(:),: );
            
            s.opL{be,variable}( indq(:), : ) = Dnn / (Jn^2) * mesh.Z{e};                        % Dn : normal derivative operator
            s.W{be}( indq(:) )               = W.*Jt;
            s.opG{be}( indq(:) )           	 = valueVector( indq(:) );
        end
        
        
        
        % Set dC/dt + omega = 0, which is in fact the AC method. Any
        % nonzero potential on the boundary is used to adjust dC/dt, which
        % is therefore raised or lowered accordingly.
        function setWallPotential(s,be,element,field,valueVector,mesh,e,timeMethod)
            activeSettings = System.settings;
            
            if (s.patchList{1}.spaceDimension==2)
                indq = s.patchList{1}.nodes{1};
                W = element.finite.W2{s.dir};
            elseif (s.patchList{1}.spaceDimension==1)
                
                if ( s.patchList{1}.byTag )
                    indq      = s.patchList{1}.nodeList{be};
                    dir       = s.patchList{1}.dir(be);
                    normal    = s.patchList{1}.normal(be);
                    Jn        = element.J( abs(normal) );
                    Jt        = element.J( abs( dir  ) );   %  s.patchList{1}.edgeList{be}.J;
                    
                else
                    indq    = s.patchList{1}.nodes{1};
                    dir     = s.dir;
                    normal  = s.patchList{1}.normal;
                    Jt      = s.Jt(be);
                    Jn      = s.Jn(be);
                end
                
                if (mesh.dim==3)
                    W = element.finite.W1{s.dir};
                    error('Wrong weights')
                elseif (mesh.dim==2)
                    W = element.finite.W1{ abs(dir) };
                end
                
            elseif (s.patchList{1}.spaceDimension==0)
                indq = s.patchList{1}.nodes{1};
                W = 1;
                Jt = 1;
            else
                error('There is an error in FEBoundary.setDerivative :: unsupported spaceDimension')
                W = element.finite.W( indq(:) );
            end
            
            cField = FieldCollection.get('c');
            
            % Get time components
            Dt      = TimeIntegration.getTime(timeMethod,mesh,element);
            Dt_rhs  = TimeIntegration.getTime(timeMethod,mesh,element,cField);
            Dt      = Dt( indq(:),: );
            Dt_rhs  = Dt_rhs( indq(:) );
            
            H = element.finite.H( indq(:),: );
            
            
            
            s.opL{be,Variable.c  }( indq(:), : )    = Dt * mesh.Z{e};                         % time derivative : unit operator
            s.opL{be,Variable.omg}( indq(:), : )    = H  * mesh.Z{e};                         % H : unit operator
            s.W{be}( indq(:) )                      = W.*Jt;
            s.opG{be}( indq(:) )                    = Dt_rhs;
        end
        
        % Set grad(C) on the wall, which is derived from the C field. The 
        % equilibrium interface profile is used to determine this gradient. 
        % The C value corresponds to a certain d(x), which in turn gives an
        % exact grad(C) on the wall.
        function setCH0D(s,be,element,field,valueVector,mesh,e,timeMethod)
            %activeSettings = System.settings;
            
            if (s.patchList{1}.spaceDimension==2)
                indq = s.patchList{1}.nodes{1};
                W = element.finite.W2{s.dir};
            elseif (s.patchList{1}.spaceDimension==1)
                
                if ( s.patchList{1}.byTag )
                    indq      = s.patchList{1}.nodeList{be};
                    dir       = s.patchList{1}.dir(be);
                    normal    = s.patchList{1}.normal(be);
                    Jn        = element.J( abs(normal) );
                    Jt        = element.J( abs( dir  ) );   %  s.patchList{1}.edgeList{be}.J;
                    
                else
                    indq    = s.patchList{1}.nodes{1};
                    dir     = s.dir;
                    normal  = s.patchList{1}.normal;
                    Jt      = s.Jt(be);
                    Jn      = s.Jn(be);
                end
                
                if (mesh.dim==3)
                    W = element.finite.W1{s.dir};
                    error('Wrong weights')
                elseif (mesh.dim==2)
                    W = element.finite.W1{ abs(dir) };
                end
                
            elseif (s.patchList{1}.spaceDimension==0)
                indq = s.patchList{1}.nodes{1};
                W = 1;
                Jt = 1;
            else
                error('There is an error in FEBoundary.setDerivative :: unsupported spaceDimension')
                W = element.finite.W( indq(:) );
            end
            
            CH     = PhysicsCollection.get('CH');
            cField = FieldCollection.get('c');
            
            PEI  = 1/CH.Pe;
            Cn_L = CH.Cn_L;
            
            % Get time components
            Dt      = TimeIntegration.getTime(timeMethod,mesh,element);
            Dt_rhs  = TimeIntegration.getTime(timeMethod,mesh,element,cField);            
            
            Ck  = cField.val2(element.id);
            dCkdx  = cField.val2(element.id);
            dCkdy  = cField.val2(element.id);
            vecZ = element.finite.vecZ;
            matZ = element.finite.matZ;
            H   = element.getH;
            Dxx = element.getDxx;
            Dxy = element.getDxy;
            Dyy = element.getDyy;
            Ze  = mesh.Z{e};
            
            t1 = 1;
            t2 = 0;
            
            L11 = Dt;
            L12 = -PEI .* (Dxx+Dyy);
            L21 = t1/Cn_L * (3*Ck.^2 - 3*Ck + 0.5).*H - t2*Cn_L * (Dxx+Dyy);
            L22 = -H;
            L13 = matZ;
            L23 = matZ;
            
            G1  = Dt_rhs; 
            G2  = t1/Cn_L * (2*Ck.^3 - 1.5*Ck.^2);

            gradC2 = dCkdx.^2 + dCkdy.^2;
            Dtt = (dCkdy.^2 .* Dxx - 2*dCkdx.*dCkdy.*Dxy + dCkdx.^2.*Dyy)./ gradC2;      % tangential directional second derivative
            
            if (CH.Nvar==2)
                s.opL{be} = [ L11 L12 ;...
                              L21 L22 ] * superkron(eye(2),Ze);
                          
                s.opG{be} = [ G1; G2 ];
                
            elseif (CH.Nvar==3)
                
                L13 = -PEI * (Dtt); %- 0* PEI * (Dxx+Dyy);
                L23 = matZ;
                L31 = Cn_L * (Dtt);
                L32 = matZ;
                L33 = H;
                G3  = vecZ;
                
                s.opL{be} = [ L11 L12 L13 ;...
                              L21 L22 L23 ;...
                              L31 L32 L33 ] * superkron(eye(3),Ze);
                          
                s.opG{be} = [ G1; G2; G3 ];
            end
            
            s.W{be}( indq(:) ) = W.*Jt;
        end
        
        
        
        % Set grad(C) on the wall, which is derived from the C field. The 
        % equilibrium interface profile is used to determine this gradient. 
        % The C value corresponds to a certain distance function (LS) d(x), 
        % which in turn gives an exact grad(C) on the wall.
        function setCH1D(s,be,element,field,valueVector,mesh,e,timeMethod)
            %activeSettings = System.settings;
            
            if (~isempty(field))
                fprintf('Warning : the field for the CH1D boundary condition should be empty')
            end
            
            if (s.patchList{1}.spaceDimension==2)
                indq = s.patchList{1}.nodes{1};
                W = element.finite.W2{s.dir};
            elseif (s.patchList{1}.spaceDimension==1)
                
                if ( s.patchList{1}.byTag )
                    indq      = s.patchList{1}.nodeList{be};
                    dir       = s.patchList{1}.dir(be);
                    normal    = s.patchList{1}.normal(be);
                    Jn        = element.J( abs(normal) );
                    Jt        = element.J( abs( dir  ) );   %  s.patchList{1}.edgeList{be}.J;
                    
                else
                    indq    = s.patchList{1}.nodes{1};
                    dir     = s.dir;
                    normal  = s.patchList{1}.normal;
                    if (numel(normal)>1)
                        normal = normal(be);
                    end
                    Jt      = s.Jt(be);
                    Jn      = s.Jn(be);
                end
                
                if (mesh.dim==3)
                    W = element.finite.W1{s.dir};
                    error('Wrong weights')
                elseif (mesh.dim==2)
                    W = element.finite.W1{ abs(dir) };
                end
                
            elseif (s.patchList{1}.spaceDimension==0)
                indq = s.patchList{1}.nodes{1};
                W = 1;
                Jt = 1;
            else
                error('There is an error in FEBoundary.setDerivative :: unsupported spaceDimension')
                W = element.finite.W( indq(:) );
            end
            
            CH     = PhysicsCollection.get('CH');
            cField = FieldCollection.get('c');
            omgField = FieldCollection.get('omg');
            
%             PEI  = 1/CH.Pe;
            PEI  = 1;
            Cn_L = CH.Cn_L;
            
            % Get time components
            Dt      = TimeIntegration.getTime(timeMethod,mesh,element);
            Dt_rhs  = TimeIntegration.getTime(timeMethod,mesh,element,cField);            
            
            Omgk    = omgField.val2(element.id);
            Ck      = cField.val2(element.id);
            dCkdx   = cField.x2(element.id);
            dCkdy   = cField.y2(element.id);
            vecZ = element.finite.vecZ;
            matZ = element.finite.matZ;
            H   = element.getH;
            Dx = element.getDx;
            Dy = element.getDy;
            Dxx = element.getDxx;
            Dxy = element.getDxy;
            Dyy = element.getDyy;
            Ze  = mesh.Z{e};
            
            if (abs(normal)==1)
                Dn    = Dx;
                Dnn_s = Dxx;
            elseif (abs(normal)==2)
                Dn    = Dy;
                Dnn_s = Dyy;
            end
            
            gradC2      = dCkdx.^2 + dCkdy.^2;
            normC       = sqrt(gradC2);
            
            % The unit normals are derived in all quadrature points (rows),
            % and the 
            
            % compute the interface unit normal (pointing from C=1 towards 
            % C=0 by the minus sign)
            n_gamma     = -[ dCkdx  dCkdy ] ./ normC;
            
            % initialize the wall normal vector
            n_wall      = zeros( numel(Ck) , mesh.sDim );
            
            % check if the normal is a scalar or a vector
            if (numel(normal)>1)
                % vector normal: not used yet
                %if size(normal,1)==1
                %   n_wall(:,1:mesh.sDim) = -normal(1:mesh.sDim);
                %end
            else
                % scalar normal: by definition the patch normal is pointing
                % outwards, which is why a minus sign is added here.
                if (abs(normal)==1)
                    n_wall(:,1) = -sign(normal);
                    n_wall(:,2) = 0;
                elseif (abs(normal)==2)
                    n_wall(:,1) = 0;
                    n_wall(:,2) = -sign(normal);
                end 
            end
            
            % compute the dot product in each quadrature point, the result
            % is a column vector which is mathematically equal to
            % cos(theta), where theta is the angle between the two normal
            % vectors
            cosTheta = dot( n_gamma', n_wall' )';
            
            % Setup the inward pointing wall normal derivative
            Dn      = -sign(normal) .* Dn;
            Dnn_s   = -sign(normal) .* Dnn_s;
            
            Dnn = (dCkdx.^2 .* Dxx + 2*dCkdx.*dCkdy.*Dxy + dCkdy.^2.*Dyy)./ gradC2;      % normal directional second derivative
            
            % Best result, but slow convergence at the end. 
            % Also weird effects once the interface has disappeared:
            %L11 = Dt;
            %L12 = -PEI .* (Dxx+Dyy);
            %L21 = (1/Cn_L * (3*Ck.^2 - 3*Ck + 0.5).*H) - Cn_L * (Dnn);
            %L22 = -H;
            %G1  = Dt_rhs;
            %G2  = 1/Cn_L * (2*Ck.^3 - 1.5*Ck.^2);
            
            U  = FieldCollection.get('u');
            V  = FieldCollection.get('v');
            Uk = U.val2(element.id);
            Vk = V.val2(element.id);
            
            Conv = (Uk.*Dx + Vk.*Dy); % + 0*(Ukx + Vky).*H);         % convection
            
            %normC = abs(Ck .* ( 1-Ck )) / ( Cn_L * sqrt(2) );
            
%             F_imp = -sqrt(2) * normC .* H;
%             F_exp = -sqrt(2) * 0.5 * normC;
            F_imp = 1/Cn_L * (3*Ck.^2 - 3*Ck + 0.5).*H;
            F_exp = 1/Cn_L * (2*Ck.^3 - 1.5*Ck.^2);
            
            L11  = Dt + Conv; % + Cn_L^2 * sqrt(2)* Dn; % + 0*sign(-normal) .* Cn_L/Cn_L .* Dn;            % grad(C) * n_wall = dCdy
            L12 = 0*H - PEI .* (Dxx+Dyy);
            
            % dCkdy./normC .* Dx - dCkdx./normC .* Dy
            %L21 = F_imp - Cn_L * (Dnn) + Cn_L * Dn;            % 3 eq version
            %L21 = F_imp - Cn_L * (Dnn) + 0*cosTheta/sqrt(2) .* (2*Ck-1) .* H;
            
            L21 = F_imp - Cn_L * (Dnn);
            L22 = -H;
            
%             L31 = Cn_L * sqrt(2) * Dn - cosTheta .* (2*Ck-1) .* H;
            
%             G3  = -cosTheta .* Ck.^2;

            %% 3rd equation: make dC/dn a function of C and theta
            L31 = Cn_L * sqrt(2) * Dn;
            L32 = matZ;
            G3  = cosTheta .* ( Ck.^2 - Ck  );
            
            L41 = matZ;
            L42 = Dnn_s;
            G4  = vecZ;
            
            %L12 = 0*Cn_L * H - PEI .* (Dxx);
            %L21 = Cn_L * Dy + 1/Cn_L * (3*Ck.^2 - 3*Ck + 0.5).*H - Cn_L * (Dxx);
            
            
            G1  = Dt_rhs;                           % + 0*Dw1 * Cn_L .* dCkdy;  % + Cn_L * Dw1 .* normC .* cos(thcosTheta .* eta_s); 
            G2  = F_exp;        % - Dw2 * Cn_L * sqrt(gradC2).*cos(theta_s);
            
            normC = (Ck .* (1-Ck)) / (sqrt(2)*Cn_L);
            
            %L21 = sqrt(2) * normC .* H - Cn_L * (Dnn);
            %G2  = 0.5*sqrt(2) * normC;
            
            if (CH.Nvar==2)
%                 s.opL{be} = [ L11 L12 ;...
%                               L21 L22 ;...
%                               L31 L32 ;...
%                               L41 L42 ] * superkron(eye(2),Ze);
%                           
%                 s.opG{be} = [ G1; G2; G3; G4 ];

                s.opL{be} = [ L11 L12 ;...
                              L21 L22 ;...
                              L31 L32 ] * superkron(eye(2),Ze);
                          
                s.opG{be} = [ G1; G2; G3 ];

%                 s.opL{be} = [ L11 L12 ;...
%                               L21 L22 ] * superkron(eye(2),Ze);
%                           
%                 s.opG{be} = [ G1; G2 ];
                          

                
%                 s.opL{be} = [ L11 L12 ] * superkron(eye(1),Ze);
%                 s.opG{be} = [ G1 ];
                
            elseif (CH.Nvar==3)
                gradC2 = dCkdx.^2 + dCkdy.^2;
                Dtt = (dCkdy.^2 .* Dxx - 2*dCkdx.*dCkdy.*Dxy + dCkdx.^2.*Dyy)./ gradC2;      % tangential directional second derivative
                
                L13 = - 0*PEI * (Dtt); %- 0* PEI * (Dxx+Dyy);
                L23 = matZ;
                L31 = Cn_L * (Dtt);
                L32 = matZ;
                L33 = H;
                G3  = vecZ;
                
                s.opL{be} = [ L11 L12 L13 ;...
                              L21 L22 L23 ;...
                              L31 L32 L33 ] * superkron(eye(3),Ze);
                          
                s.opG{be} = [ G1; G2; G3 ];
            end
            
            s.W{be}( indq(:) ) = W.*Jt;
        end
        
        
        % Set dC/dt = omega along a the wall.
        function setAC1D(s,be,element,field,valueVector,mesh,e,timeMethod)
            %activeSettings = System.settings;
            
            if (s.patchList{1}.spaceDimension==2)
                indq = s.patchList{1}.nodes{1};
                W = element.finite.W2{s.dir};
            elseif (s.patchList{1}.spaceDimension==1)
                
                if ( s.patchList{1}.byTag )
                    indq      = s.patchList{1}.nodeList{be};
                    dir       = s.patchList{1}.dir(be);
                    normal    = s.patchList{1}.normal(be);
                    Jn        = element.J( abs(normal) );
                    Jt        = element.J( abs( dir  ) );   %  s.patchList{1}.edgeList{be}.J;
                    
                else
                    indq    = s.patchList{1}.nodes{1};
                    dir     = s.dir;
                    normal  = s.patchList{1}.normal;
                    Jt      = s.Jt(be);
                    Jn      = s.Jn(be);
                end
                
                if (mesh.dim==3)
                    W = element.finite.W1{s.dir};
                    error('Wrong weights')
                elseif (mesh.dim==2)
                    W = element.finite.W1{ abs(dir) };
                end
                
            elseif (s.patchList{1}.spaceDimension==0)
                indq = s.patchList{1}.nodes{1};
                W = 1;
                Jt = 1;
            else
                error('There is an error in FEBoundary.setDerivative :: unsupported spaceDimension')
                W = element.finite.W( indq(:) );
            end
            
            CH     = PhysicsCollection.get('CH');
            cField = FieldCollection.get('c');
            
            PEI  = 1/CH.Pe;
            Cn_L = CH.Cn_L;
            
            % Get time components
            Dt      = TimeIntegration.getTime(timeMethod,mesh,element);
            Dt_rhs  = TimeIntegration.getTime(timeMethod,mesh,element,cField);            
            
            Ck  = cField.val2(element.id);
            Cxx = cField.xx2(element.id);
            vecZ = element.finite.vecZ;
            matZ = element.finite.matZ;
            H   = element.getH;
            Dxx = element.getDxx;
            Dyy = element.getDyy;
            Ze  = mesh.Z{e};
            
            omega = 1/Cn_L * (Ck.^3 - 3/2*Ck.^2 + 0.5*Ck) - Cn_L * (Cxx);
            
            L11 = Dt;
            L12 = matZ;
            L21 = matZ; %1/Cn_L * (3*Ck.^2 - 3*Ck + 0.5).*H - Cn_L * (Dxx+0*Dyy);
            L22 = matZ;
            
            G1  = Dt_rhs - omega;
            G2  = vecZ; %1/Cn_L * (2*Ck.^3 - 1.5*Ck.^2);

            s.opL{be} = [ L11 L12  ;...
                          L21 L22 ] * superkron(eye(2),Ze);
                      
            s.opG{be} = [ G1; G2 ];
            
            s.W{be}( indq(:) ) = W.*Jt;
        end
        
        
        % Generalized Navier boundary condition (for velocities)
        function setGNBC(s,be,element,field,valueVector,mesh,e,timeMethod)
            activeSettings = System.settings;
            
            if (s.patchList{1}.spaceDimension==2)
                indq = s.patchList{1}.nodes{1};
                W = element.finite.W2{s.dir};
            elseif (s.patchList{1}.spaceDimension==1)
                
                if ( s.patchList{1}.byTag )
                    indq      = s.patchList{1}.nodeList{be};
                    dir       = s.patchList{1}.dir(be);
                    normal    = s.patchList{1}.normal(be);
                    Jn        = element.J( abs(normal) );
                    Jt        = element.J( abs( dir  ) );   %  s.patchList{1}.edgeList{be}.J;
                    
                else
                    indq    = s.patchList{1}.nodes{1};
                    dir     = s.dir;
                    normal  = s.patchList{1}.normal
                    Jt      = s.Jt(be);
                    Jn      = s.Jn(be);
                end
                
                if (mesh.dim==3)
                    W = element.finite.W1{s.dir};
                    error('Wrong weights')
                elseif (mesh.dim==2)
                    W = element.finite.W1{ abs(dir) };
                end
                
            elseif (s.patchList{1}.spaceDimension==0)
                indq = s.patchList{1}.nodes{1};
                W = 1;
                Jt = 1;
            else
                error('There is an error in FEBoundary.setDerivative :: unsupported spaceDimension')
                W = element.finite.W( indq(:) );
            end
            
            % Get time components
%             Dt      = TimeIntegration.getTime(timeMethod,mesh,element);
%             Dt_rhs  = TimeIntegration.getTime(timeMethod,mesh,element,field);
%             Dt      = Dt( indq(:),: );
%             Dt_rhs  = Dt_rhs( indq(:) );
            
            H = element.finite.H( indq(:),: );
            
            variable = field.getVariable;
            
            L0 = 1;
            ls = 0.0025;        % slip length
            Ls = ls / L0;
            lambda = 12;        % 3/(2*srt(2)) * 1/Ca
            Vs = 200;           % K*Gamma / V0
            Ld = 0.0005;        % 3/(2*sqrt(2)) * M/(V0*L0^2) = 1/Pe
            
            if (abs(normal)==2)
                uField = FieldCollection.get('u');
                dudy = uField.y2(e);
                
                vField = FieldCollection.get('v');
                dvdx = vField.x2(e);
                
                cField = FieldCollection.get('c');
                dcdx = cField.x2(e,mesh);
                dcdy = cField.y2(e,mesh);
                
                CH = PhysicsCollection.get('CH');
                
                theta = 90; % degrees
                
                Lphi = CH.Cn_L * dcdy - 6*sqrt(2)/2 * cos( theta/180*pi );
            end
            
            s.opL{be,variable}( indq(:), : ) = (H/Ls) * mesh.Z{e};                         % H : unit operator
            s.W{be}( indq(:) )               = W.*Jt;
            s.opG{be}( indq(:) )           	 = valueVector(indq(:))/Ls - ( dudy(indq(:)) + dvdx(indq(:)) ) + lambda * Lphi(indq(:)) * dcdx(indq(:));
            
%                     Dt       = s.getTime( element );
%                     dUdt_rhs = s.getTime( element, U );
%                     dVdt_rhs = s.getTime( element, V );
        end
        
        function update(s)
            if (s.requiresNonLinearUpdate)
                disp('')
            end
        end
        
        function out = computeIntegral( s, fh, overIntegration )
            
            if (nargin==2)
                overIntegration = false;
            end
            
            out = 0;
            
            % Extract the elements of the passed boundary
            elements = [s.patchList{1}.elements];
            
            % Get the quadrature nodes to integrate over
            indq = s.patchList{1}.nodes{1};
            
            count = 0;
            for element=elements
                
                count = count+1;
                
                if element.rank == System.rank
                    % Set the disc to use
                    if (overIntegration)
                        disc = disc_in;
                    else
                        disc = element.finite;
                    end

                    % Set the integration weights and Jacobian
                    switch s.patchList{1}.spaceDimension
                        case 1
                            w = disc.W1{ abs(s.patchList{1}.dir) };
                        case 2
                            w = disc.W2{ abs(s.patchList{1}.dir) };
                        case 3
                            w = disc.W;
                        otherwise
                    end

                    J = s.J( count );

                    % Compute the values in the quadrature points
                    values = fh( element.id, disc );
                    if (numel(values)~=numel(indq))
                        if (System.rank==0 && count==1)
                            fprintf('WARNING : using unit values for the integral!\n')
                        end
                        values = fh( element.id, disc ) * indq;
                    end                
                    values = values( indq );

                    out = out + dot(values,w) * J;

                    %                 % SPACE-TIME
                    %                 % Integrate the values for each element and sum to get the
                    %                 % total values on this rank
                    %                 for t=1:Qt
                    %                     cm_total(t,1) = cm_total(t,1) + cmx_total(:,t)' * w * J;               % x-direction
                    %                     cm_total(t,2) = cm_total(t,2) + cmy_total(:,t)' * w * J;               % y-direction
                    %                     
                    %                     cm_enclosed(t,1) = cm_enclosed(t,1) + cmx_enclosed(:,t)' * w * J;               % x-direction
                    %                     cm_enclosed(t,2) = cm_enclosed(t,2) + cmy_enclosed(:,t)' * w * J;               % y-direction
                    %                 end
                end
            end
            
            out  = NMPI.Allreduce(out,numel(out),'+');  
        end
        
        function out = getNumElements(s)
            %out = s.base.getNumElements;
            out = s.numElements;
        end
        
        function out = getElements(s)
            %out = s.base.getNumElements;
            out = s.base.elements;
        end
        
        function out = getElementNumbers(s,e)
            out = [s.base.elements.id];
            if (nargin==2)
                out = out(e);
            end
        end
        
        function updatepatchList(s,patchList)
            if (~isequal(s.patchList,patchList))
                disp('update boundary')
                s.numElements = 0;
                
                if (s.flag=='|')
                    for i=1:numel(patchList)
                        s.numElements = s.numElements + patchList{i}.getNumElements;
                    end
                elseif (s.flag=='&')
                    if (numel(patchList)>1)
                        % Find intersection of boundaries
                        if (numel(patchList)==2)
                            elements1 = patchList{1}.getElementNumbers;
                            elements2 = patchList{2}.getElementNumbers;

                            H1 = patchList{1}.Hpos; % H matrix of boundary 1
                            H2 = patchList{2}.Hpos; % H matrix of boundary 2                        

                            D1 = patchList{1}.Dpos; % D matrix of boundary 1
                            D2 = patchList{2}.Dpos; % D matrix of boundary 2

                            s.elements = intersect(elements1,elements2);

                            s.Hpos     = intersect(H1,H2);
                            s.Dpos     = intersect(D1,D2);
                        else
                            error('More than 2 boundaries are not supported yet in FEBoundary')
                        end
                    else
                        error('Please provide at least 2 boundaries when using the &-flag for the FEBoundary')
                    end                
                else
                    error('Please provide a valid flag for the FEBoundary')                
                end

                s.base      = patchList{1};
                
                s.side      = s.base.side;
                s.Hpos      = s.base.Hpos;
                s.Dpos      = s.base.Dpos;
                s.H         = s.base.H;
                s.Dx        = s.base.Dx;
                s.Dy        = s.base.Dy;
                s.Dz        = s.base.Dz;
                s.Dn        = s.base.Dn;
                s.Dnn       = s.base.Dnn;
                s.Wdisc     = s.base.Wdisc;
                s.J         = s.base.J;
                s.Jn        = s.base.Jn;
                s.thickness = s.base.thickness;
            end
        end
        
%         function out = getGlobalElementNumbers(s,e)
%             out = s.base.getGlobalElementNumbers(e);
%         end
        
        function reset(s)
            s.opL = cell( size(s.opL) );
            s.opG = cell( size(s.opG) );
            s.W   = cell( size(s.W) );
        end
        
        function out = QPos(s,element)
            if s.flag=='|'
                out = false(element.finite.qSize);
                
                numDim      = ndims(out);
                order       = 1:numDim;
                dir         = s.patchList{1}.dir;                
                order(1)    = dir;
                order(dir)  = 1;
                
                tmp         = permute(out,order);
                tmp(1,:)    = true;
                out         = permute(tmp,order);
                
%                 if ( s.patchList{1}.pos==0 )
%                     out()
%                 elseif ( s.patchList{1}.pos==0 )
%                     
%                 else
%                     error('unsupported boundary position')
%                 end
%                 out = s.getHposQ(element.disc.dofeq);
            elseif s.flag=='&'
                indq1 = s.patchList{1}.getHposQ(element.disc.dofeq);
                indq2 = s.patchList{2}.getHposQ(element.disc.dofeq);
                
                cellfun(@(x) x.getHposQ(element.disc.dofeq), s.patchList,'UniformOutput',false)
                            
                out = logical( indq1.*indq2 );
            else
                error('unknown flag')
            end
        end
        
        function out = active(s)
            out = ~isempty(s.opL);
        end
        
        function setJacobian(s,element)
            s.J     = [];
            s.Jn    = [];
            s.Jt    = [];
            if ( norm(s.dir)>0)
                for element = s.base.elements
                    if (s.patchList{1}.spaceDimension==2)
                        s.J     = [s.J; element.J/element.J(s.dir)];
                        s.Jn    = [s.Jn; element.J(s.dir)];
                    elseif (s.patchList{1}.spaceDimension==1)
%                         if (System.settings.mesh.dim==2)
%                             s.Jn    = [s.Jn; element.J(s.dir)];
%                             if (s.dir==1)
%                                 s.J = [s.J; element.J(2)];
%                             elseif (s.dir==2)
%                                 s.J = [s.J; element.J(1)];
%                             end
%                         else
                            s.J    = [s.J; element.J(s.dir)];
                            if (s.dir==1)
                                s.Jt = [s.Jt; element.J(1)];
                                s.Jn = [s.Jn; element.J(2)];
                            elseif (s.dir==2)
                                s.Jt = [s.Jt; element.J(2)];
                                s.Jn = [s.Jn; element.J(1)];
                            end
%                         end
                    else
                        error('Unsupported FEBoundary.setJacobian')
                    end
                end
            else
                s.J = 1;
                if (s.patchList{1}.spaceDimension~=0)
                    error('The Jacobian is set to 1, but the space dimension is not equal to 0\n')
                end
            end
        end
        
        function out = getBase(s)
            out = s.base;
        end
        
        function set.weight(s,weight)
            if (weight>=0)
                s.weight = weight;
            end
        end
    end
    
    methods (Access=protected)

    end
    
end

