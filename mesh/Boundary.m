classdef Boundary < handle
    %BOUNDARY Object containing the information of a geometric boundary
    %   Detailed explanation goes here
    
    properties (Access=protected)
        elements;
        elementNumbers;
        numElements;
        
        thickness;
    end
    
    properties( Constant )
      x0 = 1, x1 = 2;
      y0 = 3, y1 = 4;
      z0 = 5, z1 = 6;
      t0 = 7, t1 = 8;
      x0y0 = 13; x1y0 = 23;
      x0y1 = 14; x1y1 = 24;
    end
    
    properties
        dir, pos;
        
        side;
        Wdisc; 
%         W; opL, opG;
        H; Dn; Dnn; J; Jn; Jt; Jt2; Hpos; Dpos;
        Dx, Dy, Dz, Dt;
        D;
    end
    
    methods
        function s = Boundary(dir,pos,mesh,discBoundary)
            if (nargin==0)
                return
            end
            
            %s.side = side;
            s.reset;
            
            s.dir = dir;
            s.pos = pos;
            
%             switch side
%                 case {Neighbor.Right, Neighbor.Left}
%                     s.direction = 1;
%                 case {Neighbor.Bottom, Neighbor.Top}
%                     s.direction = 2;
%                 case {Neighbor.Front, Neighbor.Back}
%                     s.direction = 3;
%                 case {Neighbor.Past, Neighbor.Future}
%                     s.direction = 4;
%                 case {9} % line in z-direction
% %                     s.direction = 3;
%             end
            
            s.Wdisc = discBoundary.W;
            s.H = discBoundary.H;
            s.Hpos = discBoundary.Hpos;
            if s.dir > 0
                s.Dn = discBoundary.Dn;
                %s.Dnn = discBoundary.Dnn;
                s.Dpos = discBoundary.Dpos;
                
                if (isfield(discBoundary,'D') && ~isempty(discBoundary.D))
                    s.D = cell( numel(discBoundary.D),1);
                    for i=1:numel(discBoundary.D)
                        s.D{i} = discBoundary.D{i};
                    end
                end
                
                if (isfield(discBoundary,'Dx') && ~isempty(discBoundary.Dx))
                    s.Dx = discBoundary.Dx;
                    s.Dy = discBoundary.Dy;
                    s.Dz = discBoundary.Dz;
                end
            end
            
            if (isfield(discBoundary,'thickness'))
                s.thickness = discBoundary.thickness;
            end
            
            % LOCAL ELEMENTS
%             for e=1:mesh.getNumLocalElements
%                 if (side==9)
%                     if (mesh.getLocalElement(e).isBoundaryElement(Neighbor.Bottom) && mesh.getLocalElement(e).isBoundaryElement(Neighbor.Left)) % bottom-left
%                         s.addElement(mesh.getLocalElement(e));
%                         s.addElementNumber(e);
%                         s.direction = 3;
%                         if (s.direction > 0)
%                             s.addJacobianLine(mesh.getLocalElement(e));
%                         end
%                     end
%                 else
%                     if (mesh.getLocalElement(e).isBoundaryElement(side))
%                         s.addElement(mesh.getLocalElement(e));
%                         s.addElementNumber(e);
%                         if (s.direction > 0)
%                             s.addJacobian(mesh.getLocalElement(e));
%                         end
%                     end
%                 end
%             end
            
            for e=mesh.eRange
%                 if (side==9)
%                     if (mesh.getElement(e).isBoundaryElement(Neighbor.Bottom) && mesh.getElement(e).isBoundaryElement(Neighbor.Left)) % bottom-left
%                         s.addElement(mesh.getElement(e));
%                         s.addElementNumber(e);
%                         s.direction = 3;
%                         if (s.direction > 0)
%                             s.addJacobianLine(mesh.getElement(e));
%                         end
%                     end
%                 else
                    if (mesh.getElement(e).isBoundaryElement(dir,pos))
                        s.addElement(mesh.getElement(e));
                        s.addElementNumber(e);
                        if (dir > 0)
                            s.addJacobian(mesh.getElement(e));
                        end
                    end
%                 end
            end
            
%             s.opL = cell(s.getNumElements,1);
%             s.opG = cell(s.getNumElements,1);
%             s.W   = cell(s.getNumElements,1);
        end
        
        function addElement(s,element)
            s.elements = [s.elements; element];
        end
        
        function addJacobian(s,element)
            s.J = [s.J; prod(element.J)/element.J(s.dir)];
            s.Jn = [s.Jn; element.J(s.dir)];
        end
        
        function addJacobianLine(s,element)
            s.J = [s.J; element.J(s.dir)];
%             s.Jn = [s.Jn; element.J(s.direction)];
        end
        
        function addElementNumber(s,element)
            s.elementNumbers = [s.elementNumbers; element];
        end
        
        function out = getElementNumbers(s,n)
            if nargin==1
                out = s.elementNumbers;
            else
                out = s.elementNumbers(n);
            end
        end
        
        function n = getNumElements(s)
            n = numel(s.elements);
        end
        
%         function out = getElementNumbers(s,n)
%             if (nargin==2)
%                 out = s.getGlobalElementNumbers(n);
%             else
%                 out = s.getGlobalElementNumbers;
%             end
% %             out = out - Parallel3D.MPI.eStart + 1;
%         end
        
        function out = getElements(s,n)
            if nargin==1
                out = s.elements;
            else
                out = s.elements(n);
            end
        end
                
        function n = getDirection(s)
            n = s.dir;
        end
        
        function [out,opposite] = getHposQ(s,dofeqArray)
            out = false(dofeqArray);
            [out,opposite] = s.getPosQ(1,out,out);
        end
        
        function out = getDposQ(s,dofeqArray)
            out = false(dofeqArray);
            [out,~] = s.getPosQ(2,out,out);
        end
        
        function reset(s)
            s.elements = [];
            s.numElements = 0;
            s.J = [];
        end
        
        function out = getSizedArray(s,value,e,element,indq)
            if (~iscell(value))
                if (length(value)==1)
                    out = value*ones(length(s.Wdisc),1);
                end
            elseif (iscell(value))
                if (numel(value{e})==element.disc.dofevq)
                    out1 = value{e}(:,:,end);
                    out = out1(:);
                elseif (numel(value{e})==numel(indq))
                    out = value{e}(:);
                end
            else
                disp('check getSizedArray in NS')
            end 
        end
    end
    
    methods (Access=protected)

    end
    
    methods(Access=private)
        function [out,opposite] = getPosQ(s,pos,out,opposite)
            pos = 1;
            switch s.side
                case {Neighbor.Left}
                    out(   pos   ,:,:) = 1;
                    opposite(end-1+pos,:,:) = 1;
                case {Neighbor.Right}
                    out(end-1+pos,:,:) = 1;
                    opposite(   pos   ,:,:) = 1;
                case {Neighbor.Bottom}
                    out(:,   pos   ,:) = 1;
                    opposite(:,end-1+pos,:) = 1;
                case {Neighbor.Top}
                    out(:,end-1+pos,:) = 1;
                    opposite(:,   pos   ,:) = 1;
                case {Neighbor.Front}
                    out(:,:,   pos:pos-1+s.thickness ) = 1;
                    opposite(:,:,end+2-pos-s.thickness:end+1-pos) = 1;
                case {Neighbor.Back}
                    out(:,:,end+2-pos-s.thickness:end+1-pos) = 1;
                    opposite(:,:,   pos:pos-1+s.thickness ) = 1;
                case {9}
                    out(pos,pos,:) = 1;
                    opposite = 0;
            end
        end
    end
    
end

