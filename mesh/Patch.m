classdef Patch < handle
    %PATCH Object containing the information of a geometric patch
    %   A PATCH is just a collection of elements and quadrature nodes. From
    %   a given 'mesh' and 'condition' elements are selected. Next, the
    %   quadrature points inside those elements that match the conditions 
    %   are flagged.
    
    properties (GetAccess=public)
        mesh;
        id;
        tag;
        
        conditions;
                
        spaceDimension;
        dir;
        normal;
        
        elements;
        nodes;
        
        numElements;
        
        localElementNumbers;
        globalElementNumbers;
        
        connected = false;
        
        edgeList;
        nodeList;
        
        byTag = false;
        
        edgePositions;
        
        useVectors = false;
        
        %elementNumbers;
    end
    
%     properties (SetAccess=public)
%         keepFlags;
%     end
        
    methods
        function s = Patch(varargin)
            
            s.mesh  = varargin{1};
            s.id    = varargin{2};
            s.tag   = varargin{3};
            
            if (nargin==3)
                %[s.elements,s.edgeList,s.dir,s.normal,s.edgePositions] = s.mesh.getElementsByTag(s.name,Material.Fluid);
                s.byTag = true;
                s.update();
                
            elseif (nargin==4)
                % Use the passed elements to select the quadratureNodes
                if (isa(varargin{4},'OctreeElement'))
                    s.elements  = varargin{4};
                else
                    error('The 4th argument should be an OctreeElement type')
                end

            elseif nargin==5
                % Use the conditions to select elements and quadratureNodes
                conditions  = varargin{4};
                s.connected = varargin{5};
                
                % Update the tag
                s.tag = [varargin{3},' ',conditions];
                
                s.processCondition(conditions);
                s.update();
            else
                error('Please provide the following: (mesh, id, name, condition, connected) OR (mesh, id, name, patchElements)')
            end
        end
        
%         function disp(s)
%             fprintf('  Patch %d with properties:\n',s.id)
%             fprintf('\n')
%             fprintf('    name               : %s\n',s.name)
%             fprintf('    condition          : %s\n',s.conditions.name)
%             fprintf('    numElements        : %d\n',s.numElements)
%             fprintf('    space dimension    : %d\n',s.spaceDimension(1))
% %             if ( numel(s.dir==1) )
% %                 fprintf('    direction          : %d\n',s.dir);
% %             else
% %                 fprintf('    direction          : %s\n',num2str(unique(s.dir)));
% %             end
% %             if ( numel(s.dir==1) )
% %                 fprintf('    normal             : %d\n',s.normal);
% %             else
% %                 fprintf('    normal             : %s\n',num2str(unique(s.normal)));
% %             end
%             fprintf('\n')
%         end
        
        function update(s)
            
%             if (nargin==1)
%                 byTag = false;
%             end
            
            if (~s.byTag)
                s.selectElements();                     % Select elements based on 'condition'
                s.numElements = numel( s.elements );
                
                if (s.numElements~=0)
                    % If connected elements are requested, enforce this to the
                    % current element list
                    if (s.connected)
                        s.enforceConnectedElements();
                    else
                        s.selectNodes();
                    end
                end

                
                
            else
                % Patch created by a tag
                [s.elements,s.edgeList,s.dir,s.normal,s.edgePositions] = s.mesh.getElementsByTag(s.tag,Material.Fluid);
                
                s.numElements           = numel( s.elements );
                s.createNodeList();
                
                s.localElementNumbers   = [ s.elements.localID ];
                s.globalElementNumbers  = [ s.elements.id ];

                s.spaceDimension = 1;       % This patch consists of edges                
            end
        end
        
%         function updateConnected(s)
%             s.selectElements();
%             s.selectNodes();
%             s.numElements = numel( s.elements );
%         end
        
%         function setKeepFlags(s,condition)
%             if (condition~=0)
%                 s.keepFlags = (s.dir==condition);
%             else
%                 s.keepFlags = true( size(s.dir) );
%             end
%         end
        
%         function out = get.elements(s)
%             out = s.elements( s.keepFlags );
%         end
%         
%         function set.elements(s,array)
%             s.elements = array;
%         end
        
%         function out = get.nodes(s)
%             out = s.nodes( s.keepFlags );
%         end
        
        function out = get.numElements(s)
            out = numel( s.elements );
        end
        
%         function out = get.localElementNumbers(s)
%             out = s.localElementNumbers( s.keepFlags );
%         end
        
%         function out = get.globalElementNumbers(s)
%             out = s.globalElementNumbers( s.keepFlags );
%         end
        
        % Plot this patch
        function plot(s)
            hold on;
            
            % Plot options
            linewidth   = 3;
            scaling     = 1;
            
            if (s.spaceDimension==1)
                for e=1:numel(s.edgeList)
                    if isvalid(s.edgeList(e))
                        s.edgeList(e).plot(linewidth,scaling);
                    end
                end
                
            else
                error('Only edges are supported')
%             elseif (s.spaceDimension==0)
%                 for e=1:numel(s.nodeList)
%                     s.nodeList(e).plot();
%                 end
                
            end
        end
    end
    
    methods(Access=private)
        
        function processCondition(s,condition)
            
            C   = strsplit(condition,{',',';'});
            
            out         = struct;
            out.tag     = condition;
            out.size    = numel(C);
            out.var     = zeros(out.size,1);
            out.val     = zeros(out.size,1);            
            
            for n=1:out.size
                tmp = strsplit(C{n},'=');
                out.var(n) = find( strcmp( Dir(1:4), tmp{1} )==1 );
                if ( ~inrange(out.var(n),1,4) )
                    error('The variable %s is not recognized, please use x, y, z, or t\n',tmp{1});
                end
                out.val(n) = str2double( tmp{2} );
                
                % str2double is more efficient than str2num, but might fail
                % for some like 'pi', then it will give NaN
                if ( out.val(n) ~= out.val(n) )
                    out.val(n) = str2num(tmp{2}); %#ok<ST2NM>
                end
            end
            
            s.conditions = out;
        end
        
        % Function to select the elements based on the conditions
        function selectElements(s)
            
            tolerance = 1e-13;
            
            for c=1:s.conditions.size
                
                if (c==1)
                    % For the first condition start with all elements, for 
                    % any additional conditions continue with the elements
                    % that are still left
                    tmpElements = s.mesh.elements;
                    %tmpElements = s.mesh.getElementsByMaterial(Material.All);
                end
                
                % The process is as follows:
                %   1) from the elements extract all the nodes
                %   2) extract only the requested variable (x,y,z or t)
                %   3) extract the elements by using the logical 'flags' array
                tmpNodes    = arrayfun( @(x) x.nodes, tmpElements, 'UniformOutput', false);
                locations  	= cellfun( @(x) x{ s.conditions.var(c) }, tmpNodes, 'UniformOutput', false);
                flags       = cellfun( @(x) inrange(s.conditions.val(c),x(1)-tolerance,x(end)+tolerance), locations );                    
                tmpElements = tmpElements( flags );
            end
            
            s.localElementNumbers   = [ tmpElements.localID ];
            s.globalElementNumbers  = [ tmpElements.id ];
            
%             s.keepFlags = true( 1, numel(tmpElements) );
            if (~s.useVectors)
                s.dir       = zeros( numel(tmpElements), 1 );
                s.normal    = zeros( numel(tmpElements), 1 );
            else
                s.dir       = zeros( numel(tmpElements), s.mesh.sDim );
                s.normal    = zeros( numel(tmpElements), s.mesh.sDim );
            end
            s.elements  = tmpElements;
        end
        
        function enforceConnectedElements(s)
            
            % Find search direction (right / up)
            e1 = s.elements(1); e2 = s.elements(2);
            if ( e1.geometric.vertices(1).X(1) == e2.geometric.vertices(1).X(1) && ...
                 e1.geometric.vertices(1).X(2) ~= e2.geometric.vertices(1).X(2) )
                % Identical x-values, different y-values 
                searchDir = 2; % positive y-direction
            elseif ( e1.geometric.vertices(1).X(1) ~= e2.geometric.vertices(1).X(1) && ...
                 e1.geometric.vertices(1).X(2) == e2.geometric.vertices(1).X(2) )
                % Different x-values, identical y-values 
                searchDir = 1; % positive x-direction
            else
                error('Unknown direction, impossible to continue with the ')
            end
            
            % Check to ensure only x-direction searches are performed
            if (searchDir~=1)
                error('Only searching in x-direction is supported/validated')
            end
            
            % Check if the first element is indeed the corner element
            if ( e1.geometric.vertices(1).X(1) ~= s.msh.X(1,1) || e1.geometric.vertices(1).X(Dir.y) ~= s.msh.X(Position.low,Dir.y) )
                error('the first element is not a corner of the full mesh, this is not supported yet')
            end
            
            % Initialize the boundaryElement array
            boundaryElement = cell(1,1);
            
            % Get the first solid edge
            be = 1;
            boundaryElement{be} = s.elements(1); %.geometric.edges(1);

            % Check if this edge is indeed solid
%             if (~boundaryElement{be}.isSolid)
%                 error('The first edge is not a solid')
%             end

            count = 0; maxCount = 500;
            searchDir = 21;
            
            % Fill the cell array with all (connected) elements within this boundary
            while boundaryElement{be}.id ~= s.elements(end).id
                
                count = count+1;
                
                if (count>maxCount)
                    break;
                end
                
                if (searchDir==21)
                    % Downward-right search: Only move in x-direction once no more y-direction motion is possible
                    if ( ~isempty(boundaryElement{be}.neighbor{Position.low,Dir.y}) )
                        while ( ~isempty(boundaryElement{be}.neighbor{Position.low,Dir.y}) )
                            boundaryElement{be+1} = boundaryElement{be}.neighbor{Position.low,Dir.y};
                            be = be+1;
                            %fprintf('add element %2d (searchDir = %2d) : %2d\n',be,searchDir,boundaryElement{be}.id)
                        end
                    elseif ( isempty(boundaryElement{be}.neighbor{Position.low,Dir.y}) )                % no bottom neighbor exists
                        if ( isempty(boundaryElement{be}.neighbor{Position.high,Dir.x}) )               % no right neighbor exists
                            searchDir = 22;                                                             % flip y-direction
                        elseif ( ~isempty(boundaryElement{be}.neighbor{Position.high,Dir.x}) )          % a right neighbor exists
                            boundaryElement{be+1} = boundaryElement{be}.neighbor{Position.high,Dir.x};
                            be = be+1;
                            %fprintf('add element %2d (searchDir = %2d) : %2d\n',be,searchDir,boundaryElement{be}.id)
                        else
                            error('Did we encounter a dead end (searchDir=21)?')
                        end
                    else
                        error('Is the neighbor valid (searchDir=21)?')
                    end
                    
                elseif (searchDir==22)
                    % Right-upward search: Only move in y-direction until 
                    % a x-direction motion is possible
                    if ( ~isempty(boundaryElement{be}.neighbor{Position.high,Dir.x}) )            % a right neighbor exists
                        boundaryElement{be+1} = boundaryElement{be}.neighbor{Position.high,Dir.x};
                        be = be+1;
                        %fprintf('add element %2d (searchDir = %2d) : %2d\n',be,searchDir,boundaryElement{be}.id)
                        searchDir = 21;                                                     % flip y-direction
                    elseif ( isempty(boundaryElement{be}.neighbor{Position.high,Dir.x}) )
                        boundaryElement{be+1} = boundaryElement{be}.neighbor{Position.high,Dir.y};
                        be = be+1;
                        %fprintf('add element %2d (searchDir = %2d) : %2d\n',be,searchDir,boundaryElement{be}.id)
                    else
                        error('Is the neighbor valid (searchDir=22)?')
                    end                    
                end
            end
            
            % Remove duplicates
            
%             % Update the s.elements array
%             for e=1:numel(boundaryElement)
%                 s.connectedElements(e) = boundaryElement{e};
%             end
            
%             s.keepFlags = true( 1, numel(boundaryElement) );
            s.elements = [boundaryElement{:}];                      % these are the mesh elements (faces)
            s.localElementNumbers   = [ s.elements.localID ];
            s.globalElementNumbers  = [ s.elements.id ];
            
            s.dir       = zeros( 1, numel(s.elements) );
            s.normal    = zeros( 1, numel(s.elements) );
            
            s.spaceDimension = 1;       % This patch consists of edges
            
            % Make an edgeList & nodeList (quadrature nodes)
            s.edgeList = cell(1,1);
            s.nodeList = cell(1,1);
            c = 1;
            for e=1:numel(s.elements)
                edges = s.elements(e).geometric.edges;
                for ePos = 1: numel(edges)
                    if (edges(ePos).isSolid)
                        
                        % Depending on the edge location the nodes are
                        % flagged
                        s.nodeList{c} = s.elements(e).finite.qNodeEdges{ePos};

                        s.dir(c)    = find(edges(ePos).orientation);
                        s.normal(c) = find(edges(ePos).normal);
                        
                        % Flip the normal for edge 4 (all normals need to
                        % point towards the fluid
                        if (ePos==4)
                            s.dir(c)    = -s.dir(c);
                            s.normal(c) = -s.normal(c);
                        end
                        
                        s.edgeList{c} = edges(ePos);
                        c=c+1;
                    end
                end
            end
        end
        
        % Function to select the quadrature node locations based on the conditions
        function selectNodes(s)
            
            flags           = cell( numel(s.elements),4);
            s.nodes         = cell( numel(s.elements),1);
            spaceDimensions	= zeros(numel(s.elements),1);
            
            % Fill flags with node sizes
            for d=1:4
                flags( :, d ) = arrayfun( @(x) true( size(x.nodes{d}) ), s.elements, 'UniformOutput', false);
            end
            
            tolerance = 1e-13;
            
            % Replace flags
            for c=1:s.conditions.size
                flags( :, s.conditions.var(c) ) = arrayfun( @(x) abs(x.nodes{ s.conditions.var(c) }-s.conditions.val(c))<tolerance, s.elements, 'UniformOutput', false);
            end
            
            for e=1:numel(s.elements)
                s.nodes{e} = logical( superkron( flags(e,:) ) );
                
                % determine the dimension of this patch
                spaceDimension1    = nnz( cellfun( @(x) size(x,1), flags(e,:)) - 1 );
                spaceDimension2    = nnz( cellfun( @(x) any(x==0), flags(e,:)) );
                spaceDimensions(e) = spaceDimension1-spaceDimension2;
                
                if (spaceDimensions(e)==0)
%                     dirVec = ~cellfun( @(x) all(x), flags(e,:));
%                     
%                     if (flags{e,dirVec}(1))
%                         s.dir(e) = -find(dirVec);
%                     else
%                         % do nothing: intermediate position, but keep positive direction
%                         s.dir(e) =  find(dirVec);
%                     end
                    
                    s.dir(e) = 0;
                    
                elseif (spaceDimensions(e)==1)
                    % In 2D meshes the variable dirVec is the normal vector 
                    % of this boundary edge. It is perpendicular to the direction of
                    % the edge, and has only 1 value.
                    % In 3D meshes it is just set to the direction of the
                    % edge, so not to its normal vector!
                    if (System.settings.mesh.dim==2)
                        dirVec = ~cellfun( @(x) all(x), flags(e,1:System.settings.mesh.dim));
                    else
                        dirVec =  cellfun( @(x) all(x), flags(e,1:System.settings.mesh.dim));
                    end
                    
                    if (System.settings.mesh.dim==2)
                        if (~s.useVectors)
                            if (flags{e,dirVec}(1))
                                s.dir(e)    = -find(~dirVec);
                                s.normal(e) = -find( dirVec);
                            else
                                % do nothing: intermediate position, but keep positive direction
                                s.dir(e)    =  find(~dirVec);
                                s.normal(e) =  find( dirVec);
                            end
                        else
                            dir     = -find(~dirVec);
                            normal  = -find( dirVec);
                            
                            s.dir(    e, abs(dir)    ) = sign(dir);
                            s.normal( e, abs(normal) ) = sign(normal);
                        end
                    else
                        if (flags{e,dirVec}(1))
                            s.dir(e) = -find(dirVec);
                        else
                            % do nothing: intermediate position, but keep positive direction
                            s.dir(e) =  find(dirVec);
                        end
                    end
                elseif (spaceDimensions(e)==2)
                    dirVec = ~cellfun( @(x) all(x), flags(e,:));
                    
                    if (flags{e,dirVec}(1))
                        s.dir(e) = -find(dirVec);
                    else
                        % do nothing: intermediate position, but keep positive direction
                        s.dir(e) =  find(dirVec);
                    end
                    
                else
                    error('Only 0D, 1D and 2D patched are supported')    
                end
            end
            
            % ensure all dimensions are equal
            if ( any( spaceDimensions - spaceDimensions(1) ) )
                error('the patch dimension is not uniform')
            else
                s.spaceDimension = spaceDimensions(1);
            end
            
            % check if all directions are identical
            if ( any( s.dir - s.dir(1,:) ) )
                dirs = unique(s.dir);
                fprintf('Warning : there are %d directions within this patch :\n',numel(dirs))
                for i=1:numel(dirs)
                    fprintf('   %+d : %4d elements\n',dirs(i),nnz(dirs(i)==s.dir) )
                end
            else
                s.dir       = s.dir(1,:);
                
                s.normal    = s.normal(1,:);
            end
            
%             if (s.makeGroups)
%                 s.groups = cell( numel(dirs),1 );
%                 
%                 for i=1:numel(dirs)                    
%                     keepFlags = (s.dir==dirs(i));
% 
%                     s.groups{i}.elements              = s.elements( keepFlags );
%                     s.groups{i}.nodes                 = s.nodes( keepFlags );
%                     s.groups{i}.localElementNumbers   = s.localElementNumbers( keepFlags );
%                     s.groups{i}.globalElementNumbers  = s.globalElementNumbers( keepFlags );
%                     s.groups{i}.dir                   = s.dir( keepFlags );
%                 end
%                 
%                 fprintf('   The %d directions can be used through the setGroup(id) function!\n',numel(dirs) )
%             end
            
        end
        
        % Function to create a list with all quadrature node location (for Patch created by a tag)
        function createNodeList(s)
            s.nodeList = cell(1,s.numElements);

            for e=1:s.numElements
                % Depending on the edge location the quadrature nodes are flagged
                s.nodeList{e} = s.elements(e).finite.qNodeEdges{ s.edgePositions(e) };
            end
        end
    end
    
end

