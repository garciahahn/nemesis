
%*****************************
% 08 July 2014
%*****************************
% addpath('C:\maria\projects\LSQ\codigoMatlab\HighOrderLib');
% load HighOrderDB
% global HOLib_DB

classdef MeshRefine3D < matlab.mixin.Copyable
    
    properties (Access=private)
        GM;
        GMq;
    end
    
    properties
        x0; x1; y0; y1; z0; z1; t0; t1;

        name;
        
        Ne; Nex; Ney; Nez; Net; 
        
        uniform;
    
        isReady;
        isChanged;
        
        octTree;
        disc;
        
        old, temp;
        
        Lv;
        
        currentLv;
        Zold;
        
        dofv; dofvq;

%         GM;             % Gathering matrix (global)
        GMlocal;
        
        mutualGM;
        mutualFlag;
        
        periodic;
        
        dofPerVariable;
        qpPerVariable;
        
        mutualDof;
        numMutualDof;
        numLocalDof;

        dofRank;
        elementRank;
        
        BCx0; BCx1; 
        BCy0; BCy1;
        BCz0; BCz1; 
        BCt0; BCt1; 
        BCp;
        
        Z;
        
        eRange, eOffset, numLocalElements;
        
        maxLevel;
        
        numElements;
        
    end %properties
    
    methods
        function out = get.numElements(m)
            out = m.octTree.getNumElements;
        end 
    end
    
    methods (Access=public)
        function m = MeshRefine3D(standardElement,Ne,x0,x1,y0,y1,z0,z1,t0,t1,uniform)
            if (nargin>0)
                if (nargin==10)
                    uniform = true;
                end

                m.disc = standardElement;

                m.isReady   = false;
                m.isChanged = true;

                m.uniform = uniform;
                
                m.Ne  = Ne;
                m.Nex = Ne(1); 
                m.Ney = Ne(2); 
                m.Nez = Ne(3);
                m.Net = Ne(4);
                
                numOldLevels = 2;
                m.Lv = cell(numOldLevels,1);

                m.x0 = x0; m.x1 = x1;
                m.y0 = y0; m.y1 = y1;
                m.z0 = z0; m.z1 = z1;
                m.t0 = t0; m.t1 = t1;

                m.initOctTree(uniform);
                m.setElementLimits();
                m.createZ();  
                m.updateBoundaries();
                
                m.setName('Base')
            end
        end % end constructor 
       
        function setStandardElement(s,standardElement)
            s.disc = standardElement;
        end

        function setElementLimits(s)
            [eStart, eEnd] = Parallel3D.MPI.getElementLimits(s.numElements,s);
            
            s.eRange            = System.parallel.eRange;
            s.eOffset           = System.parallel.eOffset;
            s.numLocalElements  = System.parallel.numLocalElements;
            
            s.octTree.eStart = eStart;
            s.octTree.eEnd   = eEnd;
            
%             if ~isempty(s.eRange)
%                fprintf('Rank %.2d has the elements: %d-%d\n',System.rank,s.eRange(1),s.eRange(end))
%             else
%                fprintf('Rank %.2d has the elements: none\n',System.rank)
%             end
            
            count = 0;
            
            sizes = arrayfun(@(x) x.getNumChild, s.octTree.data);
            
            % Initialize the rank for all elements to -1, and set the 
            for e=1:s.numElements
                if ismember(e,s.eRange)
                    s.getElement(e).rank = Parallel3D.MPI.rank;
                    count = count+1;
                    s.getElement(e).localID = count;
                else
%                     s.getElement(e).rank = -1;
                    s.octTree.getElement(e).rank = -1;
                    
                end
                s.octTree.getElement(e).globalID = e;
            end
%             ranks = zeros(s.octTree.getNumElements);
%             if (Parallel3D.MPI.rank==0) 
%                 fprintf('numElements = %d\n',s.octTree.getNumElements)
%                 fprintf('%d %d s.getElementRanks = %d\n',s.eStart,s.eEnd,numel(s.getElementRanks))
%             end
            ranks = s.getElementRanks; 
            
%             rank = System.rank;
%             save(['setelementlimits.' int2str(System.rank) '.mat']);
            
            if (System.settings.anal.debug)
                fprintf('number of elements in ranks : %d\n',numel(ranks));
            end
            
            ranksAll = NMPI_Allreduce(ranks,numel(ranks),'M',Parallel3D.MPI.mpi_comm_world) ; % maximum function
     
            s.setElementRanks(ranksAll);
            
            s.setElementRank();
        end
        
        function b = getBoundary(m,boundaryNumber)
            switch boundaryNumber
                case Neighbor.Bottom
                    b = m.BCy0;
                case Neighbor.Left
                    b = m.BCx0;
                case Neighbor.Right
                    b = m.BCx1;
                case Neighbor.Top
                    b = m.BCy1;
                case Neighbor.Front
                    b = m.BCz0;
                case Neighbor.Back
                    b = m.BCz1;
                case Neighbor.Past
                    b = m.BCt0;
                case Neighbor.Future
                    b = m.BCt1;
                case 9
                    b = m.BCp;
            end
        end
        
        % This function initializes the octTree structure which is used for
        % the Adaptive Mesh Refinement
        function m = initOctTree(m,uniform)
            if (nargin==1)
                uniform = true;
            end
            
            m.octTree = OctTree(m.Nex,m.Ney,m.Nez);
            
            % fill grid node lists
            if (uniform)
                x = linspace(m.x0,m.x1,m.Nex+1);
                y = linspace(m.y0,m.y1,m.Ney+1);
                z = linspace(m.z0,m.z1,m.Nez+1);
                t = linspace(m.t0,m.t1,m.Net+1);
            else
                x = GLL(m.Nex+1,m.x0,m.x1);
                y = GLL(m.Ney+1,m.y0,m.y1);
                z = GLL(m.Nez+1,m.z0,m.z1);
                t = GLL(m.Net+1,m.t0,m.t1);
            end
            
            parent  = 0;
            level   = 0;

            neighbor = zeros(8,1);
            
            m.periodic = System.settings.mesh.periodic;
            
            % Step 1: create octs
            for et=1:m.Net
                for ez=1:m.Nez
                    for ey=1:m.Ney
                        for ex=1:m.Nex
                            id = (et-1)*m.Nex*m.Ney*m.Nez + (ez-1)*m.Nex*m.Ney + (ey-1)*m.Nex + ex;
                            
                            xelem(1) = x( ex );
                            xelem(2) = x(ex+1);

                            yelem(1) = y( ey );
                            yelem(2) = y(ey+1);

                            zelem(1) = z( ez );
                            zelem(2) = z(ez+1);

                            telem(1) = t( et );
                            telem(2) = t(et+1);

                            neighbor(1) = (ez-1)*m.Nex*m.Ney + (ey-2)*m.Nex + ex   ; % bottom
                            neighbor(2) = (ez-1)*m.Nex*m.Ney + (ey-1)*m.Nex + ex-1 ; % left
                            neighbor(3) = (ez-1)*m.Nex*m.Ney + (ey-1)*m.Nex + ex+1 ; % right
                            neighbor(4) = (ez-1)*m.Nex*m.Ney + (ey  )*m.Nex + ex   ; % top
                            neighbor(5) = (ez-2)*m.Nex*m.Ney + (ey-1)*m.Nex + ex   ; % front
                            neighbor(6) = (ez  )*m.Nex*m.Ney + (ey-1)*m.Nex + ex   ; % back
%                             neighbor(7) = (et-2)*m.Nex*m.Ney + (ey-1)*m.Nex + ex   ; % front
%                             neighbor(8) = (et  )*m.Nex*m.Ney + (ey-1)*m.Nex + ex   ; % back
                            
                            % Boundaries (periodic or not)
                            % Bottom
                            if ey==1
                                if (m.periodic(2))
                                    neighbor(1) = (ez-1)*m.Nex*m.Ney + (m.Ney-1)*m.Nex + ex;
                                else
                                    neighbor(1) = -1;
                                end
                            end 
                            
                            % Left
                            if ex==1
                                if (m.periodic(1))
                                    neighbor(2) = (ez-1)*m.Nex*m.Ney + (ey-1)*m.Nex + m.Nex ;
                                else
                                    neighbor(2) = -1;
                                end
                            end
                            
                            % Right
                            if ex==m.Nex
                                if (m.periodic(1))
                                    neighbor(3) = (ez-1)*m.Nex*m.Ney + (ey-1)*m.Nex + 1;
                                else
                                    neighbor(3) = -1;
                                end
                            end
                            
                            % Top
                            if ey==m.Ney
                                if (m.periodic(2))
                                    neighbor(4) = (ez-1)*m.Nex*m.Ney + ex;
                                else
                                    neighbor(4) = -1;
                                end
                            end
                            
                            if ez==1     ; neighbor(5) = -1; end
                            if ez==m.Nez ; neighbor(6) = -1; end                            

                            m.octTree.data(ex,ey,ez).init(id,parent,level,neighbor',xelem,yelem,zelem,m.disc);
                            m.octTree.data(ex,ey,ez).setPosition(ex,ey,ez);
                        end
                    end
                end
            end

            % Step 2: assign neighbors
            m.updateNeighborPointers();
            
            % Step 3: update element list
            m.octTree.updateElements();
    
        end
        
        % Check which elements can be refinement/coarsened, and also set 
        % if an element wants refinement / coarsening. The canRefine /
        % canCoarsen are more strict than the wantsRefinement /
        % wantsCoarsening flags.
        function [canRefine,canCoarsen] = checkRefinement(s,maxLevel,refineCriteria,coarsenCriteria)
            canRefine  = false(s.numElements,1);
            canCoarsen = false(s.numElements,1);

            refinementLevel = max(s.octTree.getLv)+1;
            
            for e=s.eRange
                
                if (e==s.eRange(1) && isa(refineCriteria(e,refinementLevel),'MeshRefine3D') )
                    goalMesh = refineCriteria(e,refinementLevel);                    
                    break;
                end
                
                element = s.octTree.getElement(e);
%                 try
                if ( any(refineCriteria(e,refinementLevel)) && element.lvl<=maxLevel )
                    element.wantsRefinement = true;
                    element.wantsCoarsening = false;
                    if (element.lvl<maxLevel)
                        canRefine(e) = true;
                    end
                else
                    element.wantsRefinement = false;
                    element.wantsCoarsening = true;
                    canRefine(e)  = false;
                    if ( element.lvl>maxLevel || (any(coarsenCriteria(e)) && element.lvl>0) )
                        canCoarsen(e) = true;
                    end
                end
%                 catch
%                     disp('')
                    
%                 end
                
            end
            
            % If the criteria contains a goalMesh, this goalMesh should be
            % used as a reference to 'detect' refinement/coarsening needs.
            % A first basic check for this is to verify if the Lv arrays
            % (which contain the level of each element are identical. If
            % the Lv's are not equal, the current mesh needs to be adjusted
            % accordingly.
            if (exist('goalMesh','var') && ~isequal(goalMesh.Lv{1},s.Lv{1}) )              
                canRefine  = false(s.numElements,1);
                canCoarsen = false(s.numElements,1);
                
                % The mesh is checked on 1 processor, and this result is
                % synchronized afterwards between all processors.
                if (System.rank==0)
                    %save(['before.' int2str(System.rank) '.mat']);
                    
                    count = 0;

                    e2 = 1;

                    for e=1:s.numElements
                        
                        %fprintf('element e=%d (level %d) --- e2=%d (level %d)\n',e,s.octTree.getElement(e).lvl,e2,goalMesh.octTree.getElement(e2).lvl)
                        
                        if goalMesh.octTree.getElement(e2).lvl == s.octTree.getElement(e).lvl
                            canRefine(e)        = false;
                            canCoarsen(e)       = false;
                            e2 = e2+1;
                        elseif goalMesh.octTree.getElement(e2).lvl > s.octTree.getElement(e).lvl
                            % The goalMesh is refined, while the current mesh
                            % is not refined yet. Increase e2 with 4
                            %fprintf('refine \n');
                            canRefine(e)        = true;

                            activeElement = goalMesh.octTree.getElement(e2);
                            while activeElement.lvl ~= s.octTree.getElement(e).lvl
                                activeElement = activeElement.parent{1};
                            end

                            e2 = e2+activeElement.getNumChild; %4^(goalMesh.octTree.getElement(e2).lvl-s.octTree.getElement(e).lvl);
                        elseif goalMesh.octTree.getElement(e2).lvl < s.octTree.getElement(e).lvl
                            % The goalMesh is coarser than the current mesh.
                            % Increase e2 with 4
                            %fprintf('coarsen \n');
                            canCoarsen(e)       = true;
                            count = count+(0.25)^(s.octTree.getElement(e).lvl-goalMesh.octTree.getElement(e2).lvl);
                            if (abs(count-1)<10*eps)
                                e2 = e2+1;      % move to next goalMesh element
                                count = 0;
                            end
                        end
                        
                        %fprintf('e = %3d and e2 = %3d :: %d -- %d\n',e,e2-1,canRefine(e),canCoarsen(e));
                    end
                    
                    %save(['goalMesh.' int2str(System.rank) '.mat']);
                end
                
                canRefine   = NMPI_AllreduceLogical(canRefine,s.numElements,'|',Parallel3D.MPI.mpi_comm_world) ;
                canCoarsen  = NMPI_AllreduceLogical(canCoarsen,s.numElements,'|',Parallel3D.MPI.mpi_comm_world) ;
                
                for e=s.eRange
                    element = s.octTree.getElement(e);                    
                    element.wantsRefinement = canRefine(e);
                    element.wantsCoarsening = canCoarsen(e);
                end
            else
                % Convert from local to global and sync between processors
                % NB: s.mesh.getElementRank is a logical array where 'true'
                % means the element is on this rank
                canRefine = NMPI_AllreduceLogical(canRefine,s.numElements,'|',Parallel3D.MPI.mpi_comm_world) ;
                canCoarsen = NMPI_AllreduceLogical(canCoarsen,s.numElements,'|',Parallel3D.MPI.mpi_comm_world) ;
            end
            
%             if (exist('goalMesh','var') && ~isequal(goalMesh.Lv{1},s.Lv{1}))
%                 save(['goalMesh.' int2str(System.rank) '.mat']);
%                 exit
%             end
        end
        
        % This function updates the mesh: refinement where the physics
        % needs it (through checkRefinement). The updateAMR first refines
        % the elements based on the 'needsRefinement' flags, and then checks 
        % iteratively for the 1-level irregularity constraint next.
        % Finally, when convergence of refinement has been reached, the new
        % limits for the parallel processes are set, and the AMR mapping arrays 
        % Z are created.
        function [isConverged,isUpdated] = refineMesh(s,maxLevel,physics)
            
            isUpdated   = false;
            
            % true if element 'e' obeys the set criteria
            refinementCriteria = @(e,refinementLevel) Settings.getRefinementCriteria(physics,e,refinementLevel);
            coarsenCriteria    = @(e) Settings.getCoarseningCriteria(physics,e);
                        
            arrayfun( @(x) physics.fields{x}.selectCurrent, 1:physics.Nvar );
            
            try
                [canRefine,canCoarsen] = s.checkRefinement(maxLevel,refinementCriteria,coarsenCriteria);
            catch Mexp
                    save(['checkRefinement.' int2str(System.rank) '.mat']);
                    
                    fprintf('\n')
                    fprintf('FATALERROR (%d)\n',System.rank)
                    if (System.rank<10)
                        fprintf('==============')
                    else
                        fprintf('===============')
                    end
                    fprintf('\n')
                    fprintf('Working on %s\n',physics.name)
                    fprintf('\n')
                    disp(Mexp)
                    fprintf('  Call history:\n')
                    for s=1:min(numel(Mexp.stack),3)
                        fprintf('    (%d) File: %s -- Function: %s:%d\n',s-1,Mexp.stack(s).file,Mexp.stack(s).name,Mexp.stack(s).line)
                    end
                    fprintf('\n');
                    
                    if (System.nproc>1)
                        exit;
                    end
            end
            
            startLv = s.octTree.getLv;
            
%             save(['refinedMesh.' int2str(System.rank) '.mat']);
            
            
            if (nnz(canRefine)>0 || nnz(canCoarsen)>0)
                     
                %save(['beforeUpdateAMR.' int2str(System.rank) '.mat']);
                
                try
                    
                    [numElementsAMR] = s.updateAMR(canRefine,canCoarsen);
                
                catch Mexp
                    save(['updateAMR.' int2str(System.rank) '.mat']);
                    
                    fprintf('\n')
                    fprintf('FATALERROR IN updateAMR(%d)\n',System.rank)
                    if (System.rank<10)
                        fprintf('==============')
                    else
                        fprintf('===============')
                    end
                    fprintf('\n')
                    fprintf('Working on %s\n',physics.name)
                    fprintf('\n')
                    disp(Mexp)
                    fprintf('  Call history:\n')
                    for s=1:min(numel(Mexp.stack),3)
                        fprintf('    (%d) File: %s -- Function: %s:%d\n',s-1,Mexp.stack(s).file,Mexp.stack(s).name,Mexp.stack(s).line)
                    end
                    fprintf('\n');
                    
                    if (System.nproc>1)
                        exit;
                    end
                end
                
                % fprintf('Number of elements during refinement : %d %d %d\n',numElementsAMR(1),numElementsAMR(2),numElementsAMR(3));
                
                if (~System.settings.anal.debug)
                    if (numElementsAMR(1)~=numElementsAMR(2) && Parallel3D.MPI.rank==0)
                        fprintf('AMR        : increase of the number of elements from %4d to %4d due to refinement upto level %d (%s)\n',numElementsAMR(1),numElementsAMR(2),s.getMaxLevel,s.name)
                    end
                    if (numElementsAMR(5)~=0 && Parallel3D.MPI.rank==0)
                        fprintf('AMR        : decrease of the number of elements from %4d to %4d due to coarsening (%s)\n',numElementsAMR(2),numElementsAMR(2)-numElementsAMR(5),s.name) 
                    end
                    if (numElementsAMR(2)~=numElementsAMR(3) && Parallel3D.MPI.rank==0)
                        fprintf('AMR        : increase of the number of elements from %4d to %4d due to 1-level irregularity constraint (%s)\n',numElementsAMR(2)-numElementsAMR(5),numElementsAMR(3)-numElementsAMR(5),s.name) 
                    end
                else
                    if (numElementsAMR(1)~=numElementsAMR(2))
                        fprintf('AMR        : increase of the number of elements from %4d to %4d due to refinement upto level %d (%s - rank %d)\n',numElementsAMR(1),numElementsAMR(2),s.getMaxLevel,s.name,System.rank)
                    end
                    if (numElementsAMR(5)~=0)
                        fprintf('AMR        : decrease of the number of elements from %4d to %4d due to coarsening (%s - rank %d)\n',numElementsAMR(2),numElementsAMR(2)-numElementsAMR(5),s.name,System.rank) 
                    end
                    if (numElementsAMR(3)~=numElementsAMR(4))
                        fprintf('AMR        : increase of the number of elements from %4d to %4d due to 1-level irregularity constraint (%s - rank %d)\n',numElementsAMR(2)-numElementsAMR(5),numElementsAMR(3)-numElementsAMR(5),s.name,System.rank) 
                    end
                    fprintf('AMR        : %d - %d - %d - %d on rank %d\n',numElementsAMR(1),numElementsAMR(2),numElementsAMR(3),numElementsAMR(4),System.rank) 
                end
                
                if (numElementsAMR(1)~=numElementsAMR(4) || numElementsAMR(5)~=0)
                    s.isChanged = true;
                    isUpdated   = true;
                    s.isReady   = false;
                end
                
                s.setElementLimits();
            end
            
            endLv = s.octTree.getLv;
            
            isConverged = isequal(startLv,endLv);
        end
        
        
        % This function updates the AMR for the flagged elements, The
        % output is an array with (1) pre-refinement number of elements,
        % (2) the number of elements after the flagged refinement, and (3)
        % the number of elements after the check of the 1-level 
        % irregularity constraint.
        function [info] = updateAMR(m,canRefine,canCoarsen)
            info = zeros(3,1);
            
            numElements = m.octTree.getNumElements;       %#ok<PROPLC> % fixed during the loop
            lastID      = numElements;                          %#ok<PROPLC> % changing during the loop
            info(1)     = numElements;                          %#ok<PROPLC> % for output
                        
            if nargin==1
                canRefine   = false(numElements,1); %#ok<PROPLC>
                canCoarsen  = false(numElements,1); %#ok<PROPLC>
            end
            
            m.octTree.setCoarsening(canCoarsen);
            
            % Step 1: Refine the flagged elements, and flag the elements
            % that can be coarsened. Even in parallel this is a loop over
            % all (global) elements, to ensure mathcing meshes between
            % ranks
            numBefore = lastID;
            for e=1:numElements %#ok<PROPLC>
                if ( canRefine(e) )
                    [lastID] = m.octTree.getElement(e).refine(lastID);
                end
            end
            if (numBefore~=lastID)
                m.octTree.updateElements;
                m.octTree.updateNeighbors;
            end
            info(2) = m.octTree.getNumElements();
            
            % Step 2: Coarsening of elements
            numBefore = lastID;
            lastID = m.octTree.coarsen(lastID); %,canCoarsen);
            if (numBefore~=lastID)
                m.octTree.updateElements;
                m.octTree.updateNeighbors;
            end
            %info(3) = m.octTree.getNumElements();
    
            % Step 3: Check if 1-level irregularity constraint is met
            finishedRefinement = false;
            loopNumber = 0;
            maxLoop    = 10;
            while (~finishedRefinement)
                loopNumber = loopNumber+1;
                
                % Check if neighbors are only 1 level apart, refine additionally if required
                m.octTree.setRefinementFlags();
                numBefore  = lastID;
                [lastID,finishedRefinement] = m.octTree.refine(lastID);

                if (loopNumber==maxLoop)
                    finishedRefinement = true;
                    if (loopNumber==maxLoop)
                        disp('WARNING : refinement not converged')
                    end
                end
                if (numBefore~=lastID)
                    m.octTree.updateElements;                
                    m.octTree.updateNeighbors;
                end
            end
            info(3) = m.octTree.getNumElements();
            
            if (~m.octTree.checkNeighbors)
                disp('There is a problem with the mesh')
            end
            
            % Process removal of elements (if all children are still flagged with 'wantsRemoval')
            numRemoved = m.octTree.removeFlagged(); %,canCoarsen);
            numRemoved = numRemoved - numRemoved/4;  % each coarsened element only decreases the number of elements with 3 elements (1 remains)
            if (numRemoved > 0)
                m.octTree.updateElements;
                m.octTree.updateNeighbors;
            end
            info(4) = m.octTree.getNumElements();
            info(5) = numRemoved;
        end
        
        function [info] = coarsenAMR(m,potentialCoarsening)
            info = zeros(3,1);
            
            numElements = m.octTree.getNumElements;       % fixed during the loop
            lastID      = numElements;                          % changing during the loop
            info(1)     = numElements;                          % for output
            
            m.octTree.coarsen(1);
            m.octTree.updateElements;
            m.octTree.updateNeighbors;
            
            
            % Step 1: Refine the flagged elements, and flag the elements
            % that can be coarsened
            for e=1:numElements
%                 if (numel(needsRefinement)>0 || needsRefinement==true )
                if ( potentialCoarsening(e) )
                    [lastID] = m.getGlobalElement(e).parent{1};
%                 else
%                     m.getGlobalElement(e).coarsenFlag = true;
%                     m.getGlobalElement(e).needsRefinement = false;
                end
%                 end
            end
            m.octTree.updateElements;
            m.octTree.updateNeighbors;
            
            info(2) = m.octTree.getNumElements();
            
            % Step 2: Check if 1-level irregularity constraint is met
            finishedRefinement = false;
            loopNumber = 0;
            maxLoop    = 10;
            while (~finishedRefinement)
                loopNumber = loopNumber+1;
                
                % Check if neighbors are only 1 level apart, refine additionally if required
                m.octTree.setRefinementFlags();
                [lastID,finishedRefinement] = m.octTree.refine(lastID);

                if (loopNumber==maxLoop)
                    finishedRefinement = true;
                    if (loopNumber==maxLoop)
                        disp('WARNING : refinement not converged')
                    end
                end
                m.octTree.updateElements;                
                m.octTree.updateNeighbors;
            end
            info(3) = m.octTree.getNumElements();
            
%             % Step 3: Coarsening of elements
%             m.octTree.coarsen(lastID);
%             info(4) = m.octTree.getNumGlobalElements();

            % Step 4: Update some settings            
            if (info(1)<info(3)) % || info(3)>0)
                m.isReady = false;
            end
            m.setElementLimits();
        end
        
        
        function update(s)
            if (~s.isReady)
                s.Lv{1} = s.octTree.getLv;
                s.setElementLimits();
                s.createZ(); 
                s.createGM();
                s.setDofRank();
                s.setLocalGM();
                s.setMutualDof();
                s.updateBoundaries();
                s.isReady = true;
            end
            
            %save(['mesh.' int2str(System.rank) '.mat']);
        end
        
        function updateNeighborPointers(m)
            % Step 2: assign neighbors
            for ez=1:m.Nez
                for ey=1:m.Ney
                    for ex=1:m.Nex
%                         id = (ez-1)*m.Nex*m.Ney + (ey-1)*m.Nex + ex;
                        % x-direction
                        if ex<m.Nex
                            m.octTree.data(ex,ey,ez).setNeighbor( Neighbor.Right , m.octTree.data(ex+1,ey,ez) );
                        elseif (ex==m.Nex && m.periodic(1))
                            m.octTree.data(ex,ey,ez).setNeighbor( Neighbor.Right , m.octTree.data(  1  ,ey,ez) );
                        end
                        if ex>1
                            m.octTree.data(ex,ey,ez).setNeighbor( Neighbor.Left  , m.octTree.data(ex-1,ey,ez) );
                        elseif (ex==1 && m.periodic(1))
                            m.octTree.data(ex,ey,ez).setNeighbor( Neighbor.Left , m.octTree.data(m.Nex,ey,ez) );
                        end

                        % y-direction
                        if ey<m.Ney
                            m.octTree.data(ex,ey,ez).setNeighbor( Neighbor.Top   , m.octTree.data(ex,ey+1,ez) );
                        elseif (ey==m.Ney && m.periodic(2))
                            m.octTree.data(ex,ey,ez).setNeighbor( Neighbor.Top   , m.octTree.data(ex,  1  ,ez) );
                        end
                        if ey>1
                            m.octTree.data(ex,ey,ez).setNeighbor( Neighbor.Bottom, m.octTree.data(ex,ey-1,ez) );
                        elseif (ey==1 && m.periodic(2))
                            m.octTree.data(ex,ey,ez).setNeighbor( Neighbor.Bottom, m.octTree.data(ex,m.Ney,ez) );
                        end

                        % t-direction
                        if ez<m.Nez
                            m.octTree.data(ex,ey,ez).setNeighbor( Neighbor.Back  , m.octTree.data(ex,ey,ez+1) );
                        end
                        if ez>1
                            m.octTree.data(ex,ey,ez).setNeighbor( Neighbor.Front , m.octTree.data(ex,ey,ez-1) );
                        end
                    end
                end
            end
            
%             for e=1:numel(m.octTree.data)
%                 disp('')
%             end
        end
        
        function saveCurrent(m,version)
            
            if (nargin==1)
                % Intermediate old values
                %m.old.Lv          = m.octTree.getLv;
                m.Lv{2}           = m.octTree.getLv;
                m.old.Z           = m.Z;
                m.old.numElements = m.numElements;
                m.old.eRange      = m.eRange;
                m.old.prodJ       = m.getProdJ;
            
            elseif (nargin==2)
                % Initial old values
                if (version==0)
                    %m.temp.Lv          = m.octTree.getLv;
                    m.temp.Lv2         = m.Lv{1};
                    m.temp.Z           = m.Z;
                    m.temp.numElements = m.numElements;
                    m.temp.eRange      = m.eRange;
                    m.temp.prodJ       = m.getProdJ;
                elseif (version==1)
                    %m.old.Lv          = m.temp.Lv;
                    m.Lv{1}           = m.octTree.getLv;
                    m.Lv{2}           = m.temp.Lv2;
                    m.old.Z           = m.temp.Z;
                    m.old.numElements = m.temp.numElements;
                    m.old.eRange      = m.temp.eRange;
                    m.old.prodJ       = m.temp.prodJ;
                    m.temp            = [];
                end
            end
        end
        
        function level = getMaxLevel(m)
            level = max(m.octTree.getLv);
        end
        
        %% Assign boundaryElements 
        function updateBoundaries(m)
%             m.BCx0 = []; m.BCx1 = [];
%             m.BCy0 = []; m.BCy1 = [];
%             m.BCt0 = []; m.BCt1 = [];

            m.BCx0 = Boundary(Neighbor.Left,m,m.disc.Bx0);
            m.BCx1 = Boundary(Neighbor.Right,m,m.disc.Bx1);
            m.BCy0 = Boundary(Neighbor.Bottom,m,m.disc.By0);
            m.BCy1 = Boundary(Neighbor.Top,m,m.disc.By1);
            m.BCz0 = Boundary(Neighbor.Front,m,m.disc.Bz0);
            m.BCz1 = Boundary(Neighbor.Back,m,m.disc.Bz1);
            
%             m.BCt0 = Boundary(Neighbor.Front,m.octTree,m.disc.Bt0);
%             m.BCt1 = Boundary(Neighbor.Back,m.octTree,m.disc.Bt1);
            m.BCp  = Boundary(9,m,m.disc.Bp);
            
%             for e=1:m.Ne        
%                 if (m.octTree.getElement(e).isBoundaryElement(Neighbor.Left))
%                     m.BCx0.addElement(e);
%                 end
% 
%                 if (m.octTree.getElement(e).isBoundaryElement(Neighbor.Right))
%                     m.BCx1 = [m.BCx1 e];
%                 end
% 
%                 if (m.octTree.getElement(e).isBoundaryElement(Neighbor.Bottom))
%                     m.BCy0 = [m.BCy0 e];
%                 end
% 
%                 if (m.octTree.getElement(e).isBoundaryElement(Neighbor.Top))
%                     m.BCy1 = [m.BCy1 e];
%                 end
% 
%                 if (m.octTree.getElement(e).isBoundaryElement(Neighbor.Front))
%                     m.BCt0 = [m.BCt0 e];
%                 end
% 
%                 if (m.octTree.getElement(e).isBoundaryElement(Neighbor.Back))
%                     m.BCt1 = [m.BCt1 e];
%                 end
%             end
        end
 
        % This function creates the gathering matrix (GM) from a passed
        % maxLevel. The GM is set in the octTree, and saved in the local
        % variable GM.
        function m = createGM(m)
            m.dofPerVariable = m.octTree.initGM();
            m.GM = m.octTree.getGM;            
        end
        
        % This function creates the gathering matrix (GMq) from a passed
        % maxLevel. The GMq is set in the octTree, and saved in the local
        % variable GMq.
        function m = createGMq(m)
            m.qpPerVariable = m.octTree.initGMq();
            m.GMq = m.octTree.getGMq;            
        end
        
        function getInfo(m)
            if (Parallel3D.MPI.rank==0)
                fprintf('Total number of global DOFs per variable (see MeshRefine) : %d\n',m.dofPerVariable)
            end
        end
        
        function out = getElementRank(s)
            out = s.elementRank(:,Parallel3D.MPI.rank+1);
        end
        
        function setElementRank(s)
            % First create a sparse logical array where all the elements from
            % each rank are set to false
            s.elementRank = sparse(false(s.numElements,Parallel3D.MPI.nproc));
            for e=1:s.numElements
                element = s.octTree.getElement(e);
                s.elementRank( e, element.rank+1 ) = true;
            end
            
%             % Compute the 'numLocalDof' on this processor
%             s.numLocalElements = nnz( s.elementRank(:,Parallel3D.MPI.rank+1) );
        end
        
        function setDofRank(s)
            % First create a sparse logical array where all the DOFs from
            % each rank are set to true
            s.dofRank = sparse(false(s.octTree.dofv,Parallel3D.MPI.nproc));
            for e=1:s.numElements
                element = s.octTree.getElement(e);
%                 try 
                    s.dofRank( element.GM, element.rank+1 ) = true;
%                 catch Mexp
%                     disp('');
%                 end
            end
            
            % Compute the 'numLocalDof' on this processor
            s.numLocalDof = nnz( s.dofRank(:,Parallel3D.MPI.rank+1) ); 
        end
        
        % Change the current global GM to a local GM (start numbering from 1)
        function setLocalGM(s)
            s.GMlocal = sparse(s.octTree.dofv,1);            
            s.GMlocal( s.dofRank(:,System.rank+1) ) = (1:s.numLocalDof);
            
            for e=s.eRange
                element = s.getElement(e);
                temp = s.GMlocal( element.GM(:) );
                element.GMlocal = reshape(full(temp),size(element.GM));               
            end
        end
        
%         function out = getGM(s)
%             disp('getting GM')
%             out = s.GM;
%         end
        
        function setMutualDof(s)
            if (isempty(s.dofRank))
                error('Please use setDofRank before using setMutualDof')
            end
            
            % Create a 'globalMutualDof' array from the 'dofRank' array, by
            % checking which DOFs are shared between ranks
            s.mutualFlag = false(s.octTree.dofv,Parallel3D.MPI.nproc);
            for r=1:Parallel3D.MPI.nproc
                if (r~=Parallel3D.MPI.rank+1)
                    s.mutualFlag(:,r) = s.dofRank(:,Parallel3D.MPI.rank+1) .* s.dofRank(:,r);
                end
            end
            
%             if (Parallel3D.MPI.rank==1)
%                 for i=1:s.octTree.dofv
%                     if (s.mutualFlag(i,1))
%                         GMlocal = full(s.GMlocal);
%                         fprintf('mutual flag == true at %d -- Local = %d\n',i,GMlocal(i))
%                     end
%                 end
%             end
            
            % Finally compute the 'numMutualDof', which is just a summation
            % of the nonzero entries in the 'mutualDof' array
            s.numMutualDof = zeros(Parallel3D.MPI.nproc,1);
            for r=1:Parallel3D.MPI.nproc
                s.numMutualDof(r) = nnz( s.mutualFlag(:,r) );
            end
            
            %if (Parallel3D.MPI.nproc==2)
            %    fprintf('There are %d mutualDof on rank %d\n',s.numMutualDof(1-Parallel3D.MPI.rank+1),Parallel3D.MPI.rank)
            %end
            
            s.mutualGM = cell(Parallel3D.MPI.nproc,1);
            for r=1:Parallel3D.MPI.nproc
                if (r~=Parallel3D.MPI.rank+1)
                    s.mutualGM{r} = s.GMlocal( s.mutualFlag(:,r) ); %s.dofRank(:,Parallel3D.MPI.rank+1) .* s.dofRank(:,r);
                end
            end    
            
%             if (Parallel3D.MPI.rank==1)
%                 fprintf('Rank %d: %d %d\n',Parallel3D.MPI.rank,s.getNumMutualDof(0),s.getNumMutualDof(1))
%                 s.getMutualGM(Parallel3D.MPI.rank-1)
%             end
% %             
%             if (Parallel3D.MPI.rank==0)
%                 fprintf('Rank 2-3: %d\n',s.getNumMutualDof(Parallel3D.MPI.rank+1))
%                 s.getMutualGM(Parallel3D.MPI.rank+1)
%                 
%             end
        end
        
        function setLevelElements(s)
            s.octTree.setLevelElements;
        end
        
        function out = getLevelElement(s,e,level)
            s.octTree;
        end
        
        % Returns the 'mutualDof' array of this process with respect to 
        % another rank. Furthermore the numMutualDof between the two is
        % returned
        function [mutualDof,numMutualDof] = getMutualDof(s,rank)
            mutualDof = s.mutualDof(:,rank+1);
            numMutualDof = s.numMutualDof(rank+1);
        end
        
        function out = getMutualGM(s,rank)
            if (nargin==2)
                out = s.mutualGM{rank+1};
            else
                out = s.mutualGM;
            end
        end
        
        function out = getNumLocalDof(s)
            out = s.numLocalDof;
        end
        
        function out = getNumMutualDof(s,rank)
            out = s.numMutualDof(rank+1);
        end
        
        function out = getNumDof(s)
            out = s.octTree.getDofv;
        end
        
        function out = getDofv(s)
            out = s.octTree.getDofv;
        end
        
        function setName(s,name)
            s.name = name;
        end
        
        function out = getName(s)
            out = s.name;
        end
        
        % Returns the Z array for global element number e. Since the Z
        % array is stored with local numbering, a conversion is made.
        function out = getZ(m,e)
%             rank = System.rank;
%             save(['Z.' int2str(System.rank) '.mat']);  
            
            out = m.Z{ m.getElement(e).localID };
        end
                        
%         function createLocalZ(s)
%             if (Parallel3D.MPI.rank>0)
%                 s.Z = cell(s.getNumLocalElements,1);
%                 for e=1:s.getNumLocalElements
%                     s.Z{e} = s.getLocalElement(e).createZ();
%                 end
%             else
%                 s.Z = cell(s.getNumGlobalElements,1);
%                 for e=1:s.getNumGlobalElements
%                     s.Z{e} = s.getGlobalElement(e).createZ();
%                 end
%             end
%         end

        % Convert a global element number to a local number
        function out = g2l(s,e)
            out = e - s.eOffset;
        end
        
        % Convert a local element number to a global number
        function out = l2g(s,e)
            out = s.eOffset + e;
        end
        
        function createZ(s)
            s.Z = cell(System.parallel.numLocalElements,1);
            for e=s.eRange
                element = s.getElement(e);
                s.Z{ element.localID } = element.createZ();
            end
        end
        
        % Returns a local element n, here the mapping from global to local 
        % is made
        function out = getElement(s,n)
            if ~ismember(n,s.eRange)
                error('MeshRefine3D:getElement','The element number %d is outside the global range %d:%d\n',n,s.eRange(1),s.eRange(end));
            end
            out = s.octTree.getElement(n);
        end
        
        function out = getLocalElement(s,e)
            % GETLOCALELEMENT  Returns the element based on local numbering
            %   out = getLocalElement(elementNumber) 
            %
            if e>s.numLocalElements
                error('MeshRefine3D:getLocalElement','The element number %d is outside the local range 1:%d\n',e,s.numLocalElements);
            end
            out = s.octTree.getElement( s.l2g(e) );
        end
        
        function out = getLocalElements(s)
            out = cell(System.parallel.numLocalElements,1);
            for e=System.parallel.eRange
                out{e-System.parallel.eOffset} = s.getElement(e);
            end
        end
        
        function out = getProdJ(s)
            %if (s.isChanged)
%                 out = s.old.prodJ;
            %else
                out = zeros(s.numLocalElements,1);
                for e=1:s.numLocalElements
                    out(e) = prod( s.getLocalElement(e).J );
                end
            %end
        end
       
        % Returns a local element n, here the mapping from global to local 
        % is made
%         function out = getElement(s,n)
%             if (Parallel3D.MPI.nproc==1)
%                 out = s.octTree.getLocalElement(n);
%             else
%                 error('MeshRefine3D:incorrectElement','Please use getLocalElement or getGlobalElement if running in parallel');
%             end
%         end
        
%         function out = getElement(s,n)
%             out = s.octTree.getElement(n);
%         end
        
        function out = getElementRanks(s,n)
            tmp = s.octTree.getElement;
            if (nargin==1)
                out = [tmp.rank];
            else
                out = tmp(n).rank;
            end
        end
        
        function setElementRanks(s,values)
            tmp = s.octTree.getElement;
            if values==0
                for i=1:numel(tmp)
                    s.octTree.getElement(i).rank = 0;
                end
            else
                for i=1:numel(tmp)
                    s.octTree.getElement(i).rank = values(i);
                end
            end
        end
    
        function out = getElementPosition(s,n)
            element     = s.getElement(n);
            elementLv   = element.getLv;
            
            out = zeros(3,elementLv+1);
            
            for l=elementLv:-1:0
                out(:,l+1) = element.getPosition;
                if l>0
                    element    = element.getParent;
                end
            end
        end
        
        function out = getElementByPosition(s,position)
            out = s.octTree.getElementByPosition(num2cell(position));
        end
        
        function fig = plot( mesh, fig, position, title_string, tag )
        %UNTITLED4 Summary of this function goes here
        %   Detailed explanation goes here
            
            % fig
            if ~exist('fig','var')
                fig = figure(1);
            end
            
            % position
            if ~exist('position','var')
                position = 0;
            end
            
            % title_string
            if ~exist('title_string','var')
                title_string = 'mesh';
            end
            
            % tag_in
            if ~exist('tag','var')
                tag = 1:mesh.numElements;
            end
            
%             if (nargin>1)
%                 
%                 fig = figure(1);
%                 position = 0;
%                 title_string = 'mesh';
%             end
        
            if (position<=1)
                clf(fig)
                screensize = get( groot, 'Screensize' );
                sizex = 1000;
                sizey = 1000;
                set(fig,'position',[0.5*(screensize(3)-sizex) 0.5*(screensize(4)-sizey) sizex sizey]); 
            end

            if (position>0)
                subplot(3,1,position);
            end

            dir = 0;

            for e=1:mesh.numElements
            %             surf(mesh.xgbl{e},mesh.ygbl{e},c{e}(:,:,1));
            %             surf(mesh.xgbl{e}(1:end-1:end),mesh.ygbl{e}(1:end-1:end),c{e}(1:end-1:end,1:end-1:end,1));
        %         surf(mesh.xgbl{e}(1:end-1:end),mesh.ygbl{e}(1:end-1:end),0*c{e}(1:end-1:end,1:end-1:end,1));
        
%                 tag = num2str( mesh.octTree.getElement(e).wantsCoarsening )
                if ~isempty(tag)
                    plotPlane(e,dir,mesh,mesh.getElementRanks(e),tag(e));
                else
                    plotPlane(e,dir,mesh,mesh.getElementRanks(e),'');
                end
                
                hold on;
            end
            title(title_string)
            colorbar off
            hold off
            
            colormap(lines);
            
            if (dir==3)
                view([0,90])
                pbaspect([(mesh.x1-mesh.x0) (mesh.y1-mesh.y0) 1])
            elseif (dir==2)
                view([90,0])
                pbaspect([1 (mesh.y1-mesh.y0) 100*(mesh.t1-mesh.t0)])
            elseif (dir==1)
                view([0,0])
                pbaspect([(mesh.x1-mesh.x0) 1 100*(mesh.t1-mesh.t0)])
            elseif (dir==0)
                view([0,90])
                pbaspect([(mesh.x1-mesh.x0) (mesh.y1-mesh.y0) 10*(mesh.t1-mesh.t0)])
            end

            xlabel('x') ;
            ylabel('y') ;
            zlabel('t') ;   
        end
        
%         function out = getNumLocalElements(s)
%             out = System.parallel.numLocalElements;
%         end
        
%         function out = getNumElements(s)
%             if (Parallel3D.MPI.nproc==1)
%                 out = s.numLocalElements;
%             end
%         end
        
        % Function to create a struct of the mesh, which can be saved in a
        % restart/solution file. The mesh can be restored by using the static 
        % load function (in combination with a standardElement).
        function mesh = save(s)
            mesh = struct;
            
            mesh.x0 = s.x0; mesh.x1 = s.x1;
            mesh.y0 = s.y0; mesh.y1 = s.y1;
            mesh.z0 = s.z0; mesh.z1 = s.z1;
            mesh.t0 = s.t0; mesh.t1 = s.t1;
            
            mesh.Ne = s.Ne;
            
            mesh.Lv = cell(2,1);            
            mesh.Lv{1} = s.octTree.getLv;       % Lv at time level n
            mesh.Lv{2} = s.currentLv;           % Lv at time level n-1
            
            mesh.maxLevel = s.maxLevel;
            
            mesh.class = class(s);
            
            mesh.uniform = s.uniform;
            
            mesh.name = s.name;
        end
        
        function CFL = computeCFL(s)
            if (System.settings.stde.sDim==2 && System.settings.stde.spaceTime)
                dxCFL           = max( (s.x1-s.x0)/(s.Nex*(s.disc.P(1)+1)^2) ,(s.y1-s.y0)/(s.Ney*(s.disc.P(2)+1)^2)) ;
                dtCFL           = System.settings.mesh.deltaT/(s.Nez*(s.disc.P(3)+1)^2) ;
            end
            CFL = dtCFL/dxCFL; 
        end
        
        function createMeshgrids(s)
            for e=1:s.numLocalElements
                element = s.getLocalElement(e);
                if isempty(element.xGrid)
                    [element.xGrid,element.yGrid] = meshgrid(element.xelem,element.yelem);
                end
            end    
        end
        
        function [xGrids,yGrids,zGrids] = getMeshgrids(s)
            s.createMeshgrids;            
            xGrids = arrayfun(@(x) x.xGrid, s.octTree.elements,'UniformOutput',false);
            yGrids = arrayfun(@(x) x.yGrid, s.octTree.elements,'UniformOutput',false);
            zGrids = arrayfun(@(x) x.zGrid, s.octTree.elements,'UniformOutput',false);
        end
    end % public methods
    
    methods (Access = protected)
        function plotPlane(elementNumber,dir,mesh,rank,tag)
            loc = cell2struct(mesh.octTree.getElement(elementNumber).getLocations,{'x','y','t'},2);
            
            if isnumeric(tag)
                tag = num2str(tag);
            elseif islogical(tag)
                tag = num2str( int8(tag) );
            end
             
             xVec = loc.x;
             yVec = loc.y;
             tVec = loc.t - loc.t(1);
             
%             xVec = mesh.octTree.getGlobalElement(elementNumber).xelem;
%             yVec = mesh.octTree.getGlobalElement(elementNumber).yelem;
%             tVec = mesh.octTree.getGlobalElement(elementNumber).zelem;
            
            p1 = [xVec( 1 ) yVec( 1 ) tVec( 1 )];
            p2 = [xVec(end) yVec( 1 ) tVec( 1 )];
            p3 = [xVec(end) yVec(end) tVec( 1 )];
            p4 = [xVec( 1 ) yVec(end) tVec( 1 )]; 
            p5 = [xVec( 1 ) yVec( 1 ) tVec(end)];
            p6 = [xVec(end) yVec( 1 ) tVec(end)];
            p7 = [xVec(end) yVec(end) tVec(end)];
            p8 = [xVec( 1 ) yVec(end) tVec(end)]; 

            hold on;
            
            isLocal = ismember(elementNumber,mesh.eRange);
            
            if isLocal
                color = [0 1 0];
            else
                color = [1 0 0];
            end
            
            if (nargin==4)
                tag = num2str(elementNumber);
            end

            if (dir==1 || dir==0)
                c1 = p1; c2 = p5; c3 = p6; c4 = p2;     % bottom
                if (abs(c1(2)-mesh.y0)<1e-14)
                    x = [c1(1) c2(1) c3(1) c4(1)];
                    y = [c1(2) c2(2) c3(2) c4(2)];
                    t = [c1(3) c2(3) c3(3) c4(3)];
                    fill3(x, y, t, color);
                end
            end
            if (dir==2 || dir==0)
                c1 = p5; c2 = p1; c3 = p4; c4 = p8;     % right
                if (abs(c1(1)-mesh.x0)<1e-14)
                    x = [c1(1) c2(1) c3(1) c4(1)];
                    y = [c1(2) c2(2) c3(2) c4(2)];
                    t = [c1(3) c2(3) c3(3) c4(3)];
                    fill3(x, y, t, color);
                end
            end
            if (dir==3 || dir==0)
                c1 = p2; c2 = p6; c3 = p7; c4 = p3;     % left
                if (abs(c1(1)-mesh.x1)<1e-14)
                    x = [c1(1) c2(1) c3(1) c4(1)];
                    y = [c1(2) c2(2) c3(2) c4(2)];
                    t = [c1(3) c2(3) c3(3) c4(3)];
                    fill3(x, y, t, color);
                end
            end
            if (dir==4 || dir==0)
                c1 = p4; c2 = p8; c3 = p7; c4 = p3;     % top
                if (abs(c1(2)-mesh.y1)<1e-14)
                    x = [c1(1) c2(1) c3(1) c4(1)];
                    y = [c1(2) c2(2) c3(2) c4(2)];
                    t = [c1(3) c2(3) c3(3) c4(3)];
                    fill3(x, y, t, color);
                end
            end
            if (dir==5 || dir==0)        
                c1 = p1; c2 = p2; c3 = p3; c4 = p4;     % front
                if (abs(c1(3)-mesh.z0)<1e-14)
                    x = [c1(1) c2(1) c3(1) c4(1)];
                    y = [c1(2) c2(2) c3(2) c4(2)];
                    t = [c1(3) c2(3) c3(3) c4(3)];
                    fill3(x, y, t, color);
                    text((c3(1)-c1(1))/2+min(c1(1),c3(1)),(c3(2)-c1(2))/2+min(c1(2),c3(2)),(c3(3)-c1(3))/2+min(c1(3),c3(3)),tag,'HorizontalAlignment','center')
                end
            end
            if (dir==6 || dir==0)        
                c1 = p5; c2 = p6; c3 = p7; c4 = p8;     % back
                if (abs(c1(3)-mesh.z1)<1e-14) 
                    x = [c1(1) c2(1) c3(1) c4(1)];
                    y = [c1(2) c2(2) c3(2) c4(2)];
                    t = [c1(3) c2(3) c3(3) c4(3)];
                    fill3(x, y, t, color);
                    text((c3(1)-c1(1))/2+min(c1(1),c3(1)),(c3(2)-c1(2))/2+min(c1(2),c3(2)),(c3(3)-c1(3))/2+min(c1(3),c3(3)),tag,'HorizontalAlignment','center')
                end
            end
        end
        
        function cpObj = copyElement(obj)
            % Make a shallow copy
            cpObj = copyElement@matlab.mixin.Copyable(obj);
            % Copy sub_prop1 in subclass
            % Assignment can introduce side effects
            cpObj.octTree = obj.octTree.copy;
            cpObj.updateNeighborPointers;
        end
    end  
    
    methods (Static)
        function s = init(standardElement)
            settings = System.settings.mesh;            
            s = MeshRefine3D(standardElement,settings.Ne_global,settings.x(1),settings.x(2),settings.y(1),settings.y(2),settings.z(1),settings.z(2),settings.t(1),settings.t(2),settings.uniform);
        end
        
        % Function to restore a mesh by loading the struct that has been 
        % saved by the save function. Every element of this mesh will have
        % the standardElement to define the locations of variables. 
        function out = load(standardElement,mesh)
            Ne = mesh.Ne;
            x0 = mesh.X(1,1);    x1 = mesh.X(1,2);
            y0 = mesh.X(2,1);    y1 = mesh.X(2,2);
            z0 = mesh.X(3,1);    z1 = mesh.X(3,2);
            t0 = mesh.X(4,1);    t1 = mesh.X(4,2);
            
            uniform = mesh.uniform;
                     
            t1 = toc;
            
            out = MeshRefine3D(standardElement,Ne,x0,x1,y0,y1,z0,z1,t0,t1,uniform);
            
            out.maxLevel = mesh.maxLevel;
            
            %fprintf('mesh (0)       : %f\n',toc-t1); t1=toc;
            
            if (~iscell(mesh.Lv))
                out.Lv = cell(1,1);
                out.Lv{1} = mesh.Lv;
            else
                out.Lv = mesh.Lv;
            end
            
            refinement = MeshRefine3D.getInverseRefinement(out.Lv{1});
            
            
            for i=1:length(refinement)
                coarsening = false(numel(refinement{i}),1);
                out.updateAMR(refinement{i},coarsening);
            end
            %fprintf('mesh (1)       : %f\n',toc-t1); t1=toc;
            
            out.update();
            
            %fprintf('mesh (2)       : %f\n',toc-t1); t1=toc;
            
            if (numel(out.Lv)>1)
                % Determine the old mesh properties
                oldMesh = MeshRefine3D(standardElement,Ne,x0,x1,y0,y1,z0,z1,t0,t1,uniform);
                refinement = MeshRefine3D.getInverseRefinement(out.Lv{2});
                for i=1:length(refinement)
                    coarsening = false(numel(refinement{i}),1);
                    oldMesh.updateAMR(refinement{i},coarsening);
                end
                oldMesh.update();
                
                oldMesh.isChanged = false;
                out.old.Z = oldMesh.Z;
                out.old.eRange = oldMesh.eRange;
                out.old.prodJ = oldMesh.getProdJ;
            end            
        end
        
        % Function to get the refinement steps required to reach a passed
        % Lv array. The steps are returned as a cell array where each cell
        % contains the flags for the elements that need to be refined.
        function out = getInverseRefinement(Lv)
            numLeafElements = 4;      % each element is divided in 4 leaf elements
            
            numSteps = max(Lv);
            out      = cell(numSteps,1);
            
            for l=numSteps:-1:1
                numLevelElements = nnz(Lv==l);%/numLeafElements;
                numElements   	 = numel(Lv)-numLevelElements+numLevelElements/numLeafElements;
                out{l}           = false(numElements,1);
                
                pos = 1;
                Lvnew = zeros(numElements,1);
                for e=1:numElements
                    if (Lv(pos)==l)
                        out{l}(e) = true;
                        Lvnew(e) = Lv(pos)-1;
                        pos = pos+numLeafElements;      % skip the next numLeafElements since they're all equal to l
                    else
                        out{l}(e) = false;
                        Lvnew(e) = Lv(pos);
                        pos = pos+1;
                    end
                end
                Lv = Lvnew;
            end
        end
    end
    
    methods (Access=protected)
        function loadObject(s)
            NS = solution.data.NS;
            
            s.name = NS.name;        
            s.uConvergence = NS.uConvergence;
            s.vConvergence = NS.vConvergence;
            s.pConvergence = NS.pConvergence;
                        
            mesh = NS.mesh;  
            mesh.isReady = false;
            mesh.update;
            
            %if isprop(mesh,'currentLv')
            
            %mesh.setElementLimits;
            %mesh.createZ;
            
            if (nargin>0)
                s.initialize(mesh,NS.Re,NS.We,NS.Fr,NS.g,NS.alpha,NS.lamRho,NS.lamMu,15,15);
            end
            
            s.uField.current.loadAlphaArray(NS.u);
            s.vField.current.loadAlphaArray(NS.v);
            s.pField.current.loadAlphaArray(NS.p);
        end
        
    end
    
end %classdef




