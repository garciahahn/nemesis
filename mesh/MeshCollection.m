classdef MeshCollection < Singleton
    %FECOLLECTION Collection of Finite Elements
    %   Detailed explanation goes here
    
    properties
        objects;
        count;
    end
    
    % Private initialization of this object
    methods(Access=private)
        function s = MeshCollection()
            s.objects = cell(0);
            s.count   = 0;
        end
    end    
    
    % Allow only 1 object to exist
    methods(Static)
        function s = instance()
            persistent uniqueInstance
            if isempty(uniqueInstance)
                s = MeshCollection();
                uniqueInstance = s;
            else
                s = uniqueInstance;
            end
        end
        
        function add(id,object)
            %ADD Adds an finiteElement to the collection
            s = MeshCollection.instance();
            
            if ( numel(s.objects)<id || isempty(s.objects{id}) )
                s.count = s.count + 1;
                s.objects{id} = object;
            else
                error('There is already an object with id=%d\n',id)
            end
        end
        
        function remove(id)
            %REMOVE Removes an finiteElement from the collection
            s = MeshCollection.instance();
            if ~isempty(s.objects{id})
                s.count = s.count - 1;
                s.objects{id} = [];
            else
                %error('There is no finite element with id=%d\n',id)
            end
        end
        
        function out = get(id)
            if (isempty(id))
                error('Please provide a valid id, this one is empty')
            end
            s = MeshCollection.instance();            
            if ( inrange(id,1,s.count) )
                out = s.objects{id};
            else
                error('No object known with id=%d\n',id)
            end
        end
        
        function info()
            s = MeshCollection.instance();
            fprintf('    MeshCollection with %d objects\n',s.count);
        end
        
        function reset()
            s = MeshCollection.instance();
            s.objects = cell(0);
            s.count = 0;
        end
        
        % Function to save a FECollection to a struct. The number of finite 
        % elements and structs of the finite elements are saved (by calling
        % the save(finiteElement) function (see FiniteElement.save).
        function out = save()
            s = MeshCollection.instance();
                        
            out = cell(s.count,1);
            for i=1:s.count
                object = MeshCollection.get(i);
                out{i} = save(object);
            end
        end
        
        % Function to load a FECollection from a saved struct. First the
        % collection is reset, and next the finite elements are loaded and
        % added to the collection (see FiniteElement.load).
        function load(objects)
            MeshCollection.reset;
            for i=1:numel(objects)
                temp = OctreeMesh.load(objects{i});
                MeshCollection.add(i,temp);
            end
        end
    end
end

