classdef PatchCollection < Singleton
    %PatchCOLLECTION Collection of patches
    %   Detailed explanation goes here
    
    properties
        objects;
        count;
    end
    
    % Private initialization of this object
    methods(Access=private)
        function s = PatchCollection()
            s.objects = cell(0);
            s.count   = 0;
        end
    end    
    
    % Allow only 1 object to exist
    methods(Static)
        function s = instance()
            persistent uniqueInstance
            if isempty(uniqueInstance)
                s = PatchCollection();
                uniqueInstance = s;
            else
                s = uniqueInstance;
            end
        end
        
        function add(object)
            %ADD Adds an finiteElement to the collection
            s = PatchCollection.instance();
            id = object.id; 
            
            if ( numel(s.objects)<id || isempty(s.objects{id}) )
                s.count = s.count + 1;
                s.objects{id} = object;
            else
                error('There is already an object with id=%d\n',id)
            end
        end
        
        function addreplace(object)
            %ADD Adds an finiteElement to the collection
            s = PatchCollection.instance();
            id = object.id; 
            
            if ( numel(s.objects)<id || isempty(s.objects{id}) )
                s.count = s.count + 1;
                s.objects{id} = object;
            else
                s.objects{id} = object;
            end
        end
        
        function update()
            s = PatchCollection.instance();
            for i=1:numel(s.objects)
                patch = s.objects{i};
                if (~isempty(patch))
                    patch.update;
                end
            end
        end
        
%         function add(id,object)
%             %ADD Adds an finiteElement to the collection
%             s = PatchCollection.instance();
%             
%             if ( numel(s.objects)<id || isempty(s.objects{id}) )
%                 s.count = s.count + 1;
%                 s.objects{id} = object;
%             else
%                 error('There is already an object with id=%d\n',id)
%             end
%         end
        
        function remove(id)
            %REMOVE Removes an finiteElement from the collection
            s = PatchCollection.instance();
            if ~isempty(s.objects{id})
                s.count = s.count - 1;
                s.objects{id} = [];
            else
                %error('There is no finite element with id=%d\n',id)
            end
        end
        
        function out = get(id,check)
            if nargin==1
                check = true;
            end
            if (isempty(id) && check)
                error('Please provide a valid id, this one is empty')
            end
            s = PatchCollection.instance();            
            if ( inrange(id,1,numel(s.objects)) )
                out = s.objects{id};
            else
                error('No object known with id=%d\n',id)
            end
        end

        function info()
            s = PatchCollection.instance();
            fprintf('    PatchCollection with %d objects\n',s.count);
            for i=1:numel(s.objects)
                patch = s.objects{i};
                if (~isempty(patch))
                    fprintf('        Patch %2d : %s (%d elements)\n',i,patch.tag,patch.numElements);
                end
            end
        end
        
        function reset()
            s = PatchCollection.instance();
            s.objects = cell(0);
            s.count = 0;
        end
        
        % Function to save a FECollection to a struct. The number of finite 
        % elements and structs of the finite elements are saved (by calling
        % the save(finiteElement) function (see FiniteElement.save).
%         function out = save()
%             s = PatchCollection.instance();
%                         
%             out = cell(s.count,1);
%             for i=1:s.count
%                 object = PatchCollection.get(i);
%                 out{i} = save(object);
%             end
%         end
        
%         % Function to load a FECollection from a saved struct. First the
%         % collection is reset, and next the finite elements are loaded and
%         % added to the collection (see FiniteElement.load).
%         function load(objects)
%             FECollection.reset;
%             for i=1:numel(objects)
%                 temp = OctreeMesh.load(objects{i});
%                 PatchCollection.add(i,temp);
%             end
%         end
    end
end

