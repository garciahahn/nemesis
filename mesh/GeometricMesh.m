classdef GeometricMesh < matlab.mixin.Copyable
    %UNTITLED7 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Constant)
        maxDim = 4;
    end
    
    properties (Access=private)        
        vertices;
        edges;
        faces;
        hexes;
        
        allEdges;
        allFaces;
    end
    
    properties (GetAccess=public, SetAccess=protected)
        dim;                            % mesh dimension
        sDim;
        spaceTime;
        
        gElementCounts;

        X = zeros(4,2);
        Ne = ones(1,4);                 % number of elements in each direction
        numElements;
        
        uniform     = true;
        periodic;
        
        geometricElements;
                
        J;
        
        nodes_xy = cell(2, 1);
        
        allElements;
        
        eStart, eEnd, eRange;
        
        coordSystem;
        
        activeMaterial;
    end
    
    properties (Access=protected)
        dh;
        dhNP;
    end
    
    methods
        function s = GeometricMesh(spatialDim,spaceTime,Ne,X,periodic,uniform,coordSystem,nodes)
            %GEOMETRICMESH Initializes the geometrics of a mesh
            %   The GeometricElements (vertices, edges, faces, hexes) are all
            %   initialized here

            % Set the dimensionality of the mesh. Here 'sdim' is the
            % spatialDimension, and 'dim' the true dimensionality of
            % the mesh including any time dimensions
            s.sDim      = spatialDim;
            s.spaceTime = spaceTime;
            s.dim       = spatialDim + spaceTime;
            
            % Assign Ne and X
            s.Ne = Ne;
            s.X  = X;
            
            % Assign the nodes of the xy plane
            s.nodes_xy = nodes;

            % Ensure both have the correct amount of elements. For Ne
            % any additional directions are padded with 1 values. For X
            % 0 values are added
            s.Ne(s.dim+1:s.maxDim)	= 0;
            s.X(:,s.dim+1:s.maxDim) = 0; 

            s.periodic          = periodic(1:s.dim);
            s.uniform           = uniform;
            
            s.activeMaterial    = System.settings.mesh.activeMaterial;
            
            s.gElementCounts = zeros(1,s.dim+1);
            %s.feLists = cell(1,s.dim+1);
            
            s.coordSystem    = coordSystem;
            
%             switch 
%                 case CoordSystem.Cartesian
          	s.initGeometry();
%                 case CoordSystem.Cylindrical
%                     s.initGeometry();
%             end
            
            %s.applyPeriodicBC();
            
            s.computeJacobians();
        end
                
        function initGeometry(s)
            
            structured = true;
            
            %% Compute the locations of the meshNodes, which distribution can be uniform or GLL-based
            % vertices
            if (structured)
                meshNodes = cell(1,s.maxDim);

                for i=1:s.maxDim
                    meshNodes{i} = 0;
                end          

                %activeSettings = System.settings;
                
                if (s.uniform)                
                    for i=1:s.dim
                        meshNodes{i} = linspace(s.X(1,i),s.X(2,i),s.Ne(i)+1);
                    end
                else
                    for i=1:s.dim
                        if (i==1)
                            if isempty(s.nodes_xy{1})
                                meshNodes{1} = linspace(s.X(1,i),s.X(2,i),s.Ne(i)+1);
                            else
                                meshNodes{1} = s.nodes_xy{1};
                            end
                        elseif(i==2)
                            if isempty(s.nodes_xy{2})
                                meshNodes{2} = linspace(s.X(1,i),s.X(2,i),s.Ne(i)+1);
                            else
                                meshNodes{2} = s.nodes_xy{2};
                            end
                        else
                            meshNodes{i} = linspace(s.X(1,i),s.X(2,i),s.Ne(i)+1);
                        end
                    end
%                     
%                     for i=1:s.dim
%                         meshNodes{i} = GLL(s.Ne(i) + 1,s.X(1,i),s.X(2,i));
%                     end
                end            

                %% vertices
                vertexSize = cellfun( @(x) numel(x), meshNodes);
                vertexSize = max(vertexSize,1);
                s.vertices = cell(vertexSize);

                [X,Y,Z,T] = ndgrid( meshNodes{:} );
                
            else
%                 % used to test the Jacobian
%                 if ( s.dim==2 )
%                     X = [0.0 0.0 0.0 ;
%                          1.0 1.1 0.9 ;
%                          2.0 2.0 2.0 ];
% 
%                     Y = [0.0 1.0 2.0 ;
%                          0.0 1.1 2.0 ;
%                          0.0 0.9 2.0 ];
% 
%                     Z = zeros(size(X));
%                     T = zeros(size(X));
% 
%                     vertexSize = [3 3 1 1];
%                     s.vertices = cell(vertexSize);
%     
%                 else
%                     meshNodes = cell(1,s.maxDim);
% 
%                     for i=1:s.maxDim
%                         meshNodes{i} = 0;
%                     end       
% 
%                     for i=1:s.dim
%                         meshNodes{i} = linspace(s.X(1,i),s.X(2,i),2);
%                     end
%                     %% vertices
%                     vertexSize = cellfun( @(x) numel(x), meshNodes);
%                     s.vertices = cell(vertexSize);
% 
%                     [X,Y,Z,T] = ndgrid( meshNodes{:} );
%                 end

                % x coordinates
                xNodes = linspace(s.X(1,1),s.X(2,1),s.Ne(1)+1)';
                X = repmat( xNodes, 1, s.Ne(2)+1 );

                % y coordinates (sin function)
                for i=1:size(X,1)
                    y0 = 0.1 * sin( X(i,1) * pi );
                    y1 = s.X(2,2);
                    Y(i,:) = linspace(y0,y1,s.Ne(2)+1)';
                end
                
                Z = zeros( size(X) );
                T = zeros( size(X) );

                vertexSize = size(X);
                s.vertices = cell( vertexSize );
            end
            
            onBoundary = zeros(1,s.dim);            
            for n=1:numel(s.vertices)
                coordinate = [ X(n) Y(n) Z(n) T(n) ];
                for i=1:s.dim
                    if (~s.periodic(i))
                        onBoundary(i) = any( s.X(:,i) == coordinate(i) );
                    else
                        onBoundary(i) = 0;
                    end
                end
                s.vertices{n} = GeometricVertex( coordinate, nnz(onBoundary) );
            end
            
            for d=1:s.dim
                if (System.settings.mesh.periodic(d))
                    
                    switch d
                        case 1
                            followers = [s.vertices{end,:}];
                            leaders   = [s.vertices{1,:}];
                            i=0;
                            for vertex = followers
                                i = i+1;
                                vertex.setPeriodic( leaders(i) )
                            end
                            
                        case 2
                            followers = [s.vertices{:,end,:}];
                            leaders   = [s.vertices{:,1,:}];
                            i=0;
                            for vertex = followers
                                i = i+1;
                                vertex.setPeriodic( leaders(i) )
                            end
                    end
                end
            end
                        
            %% edges
            s.edges = cell(4,1);
            for dir=1:4
                if ( dir <= s.dim )
                    edgeSize        = vertexSize;
                    edgeSize(dir)   = edgeSize(dir)-1;
                    s.edges{dir}    = cell( edgeSize );
                    
                    switch dir
                        case 1
                            v0 = [s.vertices{1:end-1,:,:,:}];
                            v1 = [s.vertices{2:end  ,:,:,:}];                            
                        case 2
                            v0 = [s.vertices{:,1:end-1,:,:}];
                            v1 = [s.vertices{:,2:end  ,:,:}];
                        case 3
                            v0 = [s.vertices{:,:,1:end-1,:}];
                            v1 = [s.vertices{:,:,2:end  ,:}];
                        case 4
                            v0 = [s.vertices{:,:,:,1:end-1}];
                            v1 = [s.vertices{:,:,:,2:end  }];                            
                    end                        
                    
                    for n=1:numel(v0)
                        s.edges{dir}{n} = GeometricEdge( {v0(n),v1(n)},[],[],s.coordSystem );
                        
                        % Apply periodic bc
                        if (s.edges{dir}{n}.periodic)
                            % Get the indices for this edge
                            idx = cell(size(edgeSize));
                            [idx{:}]=ind2sub(edgeSize,n);
                            
                            % Use the indices to determine the
                            % corresponding periodic edge
%                             for d=1:s.dim
%                                 if (System.settings.mesh.periodic(d))
%                                     idx{d} = 1;
%                                 end
%                                 leader = s.edges{dir}{ sub2ind(edgeSize,idx{:}) };
%                                 s.edges{dir}{n}.setPeriodic( leader );
%                             end
                            
                            for i=1:numel(System.settings.mesh.periodic)
                                if (System.settings.mesh.periodic(i) && idx{i}==edgeSize(i) && i~=dir)
                                    idx{i} = 1;
                                end
                            end
                            leader = s.edges{dir}{ sub2ind(edgeSize,idx{:}) };
                            s.edges{dir}{n}.setPeriodic( leader );
                        end
                        
                    end
                else
                    s.edges{dir}      = [];
                end
            end
            
            s.allEdges = cellfun( @(x) x(:), s.edges, 'UniformOutput', false);
            s.allEdges = vertcat( s.allEdges{:} );
            
            %% faces
            if (s.dim==2)
                s.faces = cell(4,1);
                

                faceSize = zeros(1,4);
                for n=1:4
                    faceSize(n) = size(s.edges{n},n);
                end
                
                % z-direction
                dir = 3;                
                s.faces{dir} = cell( faceSize );

                e0 = [s.edges{1}{:,1:end-1,:,:}];   % x-direction - y0
                e1 = [s.edges{1}{:,2:end  ,:,:}];   % x-direction - y1
                e2 = [s.edges{2}{1:end-1,:,:,:}];   % y-direction - x0
                e3 = [s.edges{2}{2:end  ,:,:,:}];   % y-direction - x1

                for n=1:numel(e0)
                    s.faces{dir}{n} = GeometricFace( {e0(n),e1(n),e2(n),e3(n)} );
                end
                
                s.allFaces = cellfun( @(x) x(:), s.faces, 'UniformOutput', false);
                s.allFaces = [s.allFaces{dir}];
                
            elseif (s.dim>2)
                s.faces = cell(4,1);

                faceSize_default = zeros(1,4);
                for n=1:4
                    faceSize_default(n) = size(s.edges{n},n);
                end

                for dir=1:4
                    if ( dir <= s.dim )
                        faceSize        = faceSize_default;
                        faceSize(dir)   = vertexSize(dir);
                        s.faces{dir}    = cell( faceSize );

                        switch dir
                            case 1
                                % x-normal
                                e0 = [s.edges{2}{:,:,1:end-1,:}];   % y-direction - z0
                                e1 = [s.edges{2}{:,:,2:end  ,:}];   % y-direction - z1
                                e2 = [s.edges{3}{:,1:end-1,:,:}];   % z-direction - y0
                                e3 = [s.edges{3}{:,2:end  ,:,:}];   % z-direction - y1
                                
                            case 2
                                % y-normal
                                e0 = [s.edges{1}{:,:,1:end-1,:}];   % x-direction - z0
                                e1 = [s.edges{1}{:,:,2:end  ,:}];   % x-direction - z1
                                e2 = [s.edges{3}{1:end-1,:,:,:}];   % z-direction - x0
                                e3 = [s.edges{3}{2:end  ,:,:,:}];   % z-direction - x1
                                

                            case 3
                                % z-normal
                                e0 = [s.edges{1}{:,1:end-1,:,:}];   % x-direction - y0
                                e1 = [s.edges{1}{:,2:end  ,:,:}];   % x-direction - y1
                                e2 = [s.edges{2}{1:end-1,:,:,:}];   % y-direction - x0
                                e3 = [s.edges{2}{2:end  ,:,:,:}];   % y-direction - x1

                            otherwise
                                error('Only 3D elements are supported')                            
                        end

                        for n=1:numel(e0)
                            s.faces{dir}{n} = GeometricFace( {e0(n),e1(n),e2(n),e3(n)} );
                            
                            % Apply periodic bc
                            if (s.faces{dir}{n}.periodic)
                                % Get the indices for this edge
                                idx = cell(size(faceSize));
                                [idx{:}]=ind2sub(faceSize,n);

                                % Adjust the indices to the corresponding periodic face
%                                 for d=1:s.dim
%                                     if (System.settings.mesh.periodic(d))
%                                         idx{d} = 1;
%                                     end
%                                     leader = s.faces{dir}{ sub2ind(faceSize,idx{:}) };
%                                     s.faces{dir}{n}.setPeriodic( leader );
%                                 end
                                
                                %idx{ System.settings.mesh.periodic } = 1;
                                for i=1:numel(System.settings.mesh.periodic)
                                    if (System.settings.mesh.periodic(i) && idx{i}==faceSize(i) && i==dir)
                                        idx{i} = 1;
                                    end
                                end
                                leader = s.faces{dir}{ sub2ind(faceSize,idx{:}) };
                                s.faces{dir}{n}.setPeriodic( leader );
                            end
                        end
                    else
                        s.faces{dir}      = [];
                    end
                end

                s.allFaces = cellfun( @(x) x(:), s.faces, 'UniformOutput', false);
                s.allFaces = vertcat( s.allFaces{:} );
            end
            
%             for d=1:s.dim
%                 if (System.settings.mesh.periodic(d))
%                     
%                     switch d
%                         case 1
%                             followers = [s.vertices{end,:}];
%                             leaders   = [s.vertices{1,:}];
%                             i=0;
%                             for vertex = followers
%                                 i = i+1;
%                                 vertex.setPeriodic( leaders(i) )
%                             end
%                             
%                         case 2
%                             followers = [s.vertices{:,end,:}];
%                             leaders   = [s.vertices{:,1,:}];
%                             i=0;
%                             for vertex = followers
%                                 i = i+1;
%                                 vertex.setPeriodic( leaders(i) )
%                             end
%                     end
%                 end
%             end
            
            
            %% hexes
            if (s.dim>2)
                s.hexes = cell(4,1);

                hexSize = zeros(1,4);
                for n=1:4
                    hexSize(n) = size(s.edges{n},n);
                end

                s.hexes = cell( hexSize );

                f0 = [s.faces{1}{1:end-1,:,:,:}];
                f1 = [s.faces{1}{2:end  ,:,:,:}];
                f2 = [s.faces{2}{:,1:end-1,:,:}];
                f3 = [s.faces{2}{:,2:end  ,:,:}];
                f4 = [s.faces{3}{:,:,1:end-1,:}];
                f5 = [s.faces{3}{:,:,2:end  ,:}];

                for n=1:numel(s.hexes)
                    s.hexes{n} = GeometricHex( {f0(n),f1(n),f2(n),f3(n),f4(n),f5(n)} );
                end
            end
            
            for i=1:s.dim+1
                s.gElementCounts( i ) = numel( s.gElements(i-1) );
            end
        end
        
        function initGeometryCylinder(s)
            meshNodes = cell(1,s.maxDim);

            for i=1:s.maxDim
                meshNodes{i} = 0;
            end          

            for i=1:s.dim
                meshNodes{i} = linspace(s.X(1,i),s.X(2,i),s.Ne(i)+1);
            end
            
%             r_in  = s.X(1,1);
%             r_out = s.X(2,1);
%             
%             t_in  = s.X(1,2);
%             t_out = s.X(2,2);
%             
%             % Compute the element size
%             dr = (r_out-r_in) / s.Ne(1);      % radius
%             dt = (t_out-t_in) / s.Ne(2);    	% theta
%             
%             
%             
%             % Create vertices
%             id = 0;
%             for et=1:s.Ne(2)+~s.periodic(2)             % axial direction
%                 for er=1:s.Ne(1)+~s.periodic(1)         % radial direction
%                     
%                     meshNodes{i}
%                     
%                     
%                     id = id+1;
% 
%                     % Compute the limits for theta and radius
%                     r = r_in + dr * (er-1);     % inner radius
%                     t = t_in + dt * (et-1); 	% lower theta
% 
%                     %[x,y] = circularMapping(r,t);
% 
%                     % Create the vertex object
%                     vertices(er,et) = vertex( id, [x;y], [r;t] );
%                     vertices(er,et).plot(true);
%                 end
%             end
% 
%             % z-coordinate if 3D mesh is requested
%             if (s.dim>2)
%                 i = 3;
%                 meshNodes{i} = linspace(s.X(1,i),s.X(2,i),s.Ne(i)+1);
%             end
            
            

            %% vertices
            vertexSize = cellfun( @(x) numel(x), meshNodes);
            vertexSize = max(vertexSize,1);
            s.vertices = cell(vertexSize);

            [X,Y,Z,T] = ndgrid( meshNodes{:} );
        end
        
        function applyPeriodicBC(s)
  
            switch s.dim
                case 1
                    % 1D elements : periodic vertices
                    error('GeometricMesh :: a 1D mesh with periodic bc is not supported yet')
                    
                case 2
                    % 2D elements : periodic edges
                    error('GeometricMesh :: a 2D mesh with periodic bc is not supported yet')
                    
                case 3
                    % 3D elements : periodic planes, edges and vertices
%                     range = cell(1,s.dim);
%                     for d=1:3
%                         % Reset the ranges
%                         for i=1:s.dim
%                             range{i} = ':';
%                         end
%                         if (System.settings.mesh.periodic(d))
%                             range{d}    = 1;
%                             leaders     = s.hexes{range{:}};
%                             
%                             range{d}    = size(s.hexes,d);
%                             followers   = s.hexes{range{:}};
%                             disp('')
%                             
%                             % 
%                         end
%                     end

                    % Vertices
                    for d=1:s.dim
                        if (System.settings.mesh.periodic(d))
                            switch d
                                case 1
                                    followers = [s.vertices{end,:}];
                                    leaders   = [s.vertices{1,:}];
                                    i=0;
                                    for vertex = followers
                                        i = i+1;
                                        vertex.setPeriodic( leaders(i) )
                                    end

                                case 2
                                    followers = [s.vertices{:,end,:}];
                                    leaders   = [s.vertices{:,1,:}];
                                    i=0;
                                    for vertex = followers
                                        i = i+1;
                                        vertex.setPeriodic( leaders(i) )
                                    end
                            end
                        end
                    end
            
                    % Edges
                    for d=1:s.dim
                        if (System.settings.mesh.periodic(d))
                            switch d
                                case 1
                                    followers = [s.edges{end,:}];
                                    leaders   = [s.edges{1,:}];
                                    i=0;
                                    for edge = followers
                                        i = i+1;
                                        edge.setPeriodic( leaders(i) )
                                    end

                                case 2
                                    followers = [s.edges{:,end,:}];
                                    leaders   = [s.edges{:,1,:}];
                                    i=0;
                                    for edge = followers
                                        i = i+1;
                                        edge.setPeriodic( leaders(i) )
                                    end
                            end
                        end
                    end
                    
                    disp('')
            end
            
            for d=1:s.dim
                if (System.settings.mesh.periodic(d))
                    
                    switch d
                        case 1
                            followers = [s.vertices{end,:}];
                            leaders   = [s.vertices{1,:}];
                            i=0;
                            for vertex = followers
                                i = i+1;
                                vertex.setPeriodic( leaders(i) )
                            end
                            
                        case 2
                            followers = [s.vertices{:,end,:}];
                            leaders   = [s.vertices{:,1,:}];
                            i=0;
                            for vertex = followers
                                i = i+1;
                                vertex.setPeriodic( leaders(i) )
                            end
                    end
                end
            end
        end
        
        function out = getAllElements(s,d)            
            out = [];
            
            if (d==0)                
                %% VERTICES
                % vertices within vertices
                vertices = s.gElements(0);
                activeVertices = cellfun( @(x) x.getActiveVertices, vertices(:), 'UniformOutput', 0 );
                activeVertices = vertcat( activeVertices{:} );
                out = [ out(:); activeVertices(:) ];
                
                % vertices within edges
                if (s.dim>0)
                    edges = s.gElements(1);
                    activeVertices = cellfun( @(x) x.getActiveVertices, edges(:), 'UniformOutput', 0 );
                    activeVertices = vertcat( activeVertices{:} );
                    out = [ out(:); activeVertices(:) ];
                end
                
                % vertices within faces
                if (s.dim>1)
                    faces = s.gElements(2);
                    activeVertices = cellfun( @(x) x.getActiveVertices, faces(:), 'UniformOutput', 0 );
                    activeVertices = vertcat( activeVertices{:} );
                    out = [ out(:); activeVertices(:) ];
                end
                
                % vertices within hexes
                if (s.dim>2)
                    hexes = s.gElements(3);
                    activeVertices = cellfun( @(x) x.getActiveVertices, hexes(:), 'UniformOutput', 0 );
                    activeVertices = vertcat( activeVertices{:} );
                    out = [ out(:); activeVertices(:) ];
                end
                
            elseif (d==1)
                %% EDGES
                % edges within edges
                if (s.dim>0)
                    edges = s.gElements(1);
                    activeEdges = cellfun( @(x) x.getActiveEdges, edges(:), 'UniformOutput', 0 );
                    activeEdges = vertcat( activeEdges{:} );
                    out = [ out(:); activeEdges(:) ];
                end
                
                % edges within faces
                if (s.dim>1)
                    faces = s.gElements(2);
                    activeEdges = cellfun( @(x) x.getActiveEdges, faces(:), 'UniformOutput', 0 );
                    activeEdges = vertcat( activeEdges{:} );
                    out = [ out(:); activeEdges(:) ];
                end
                
                % edges within hexes
                if (s.dim>2)
                    hexes = s.gElements(3);
                    activeEdges = cellfun( @(x) x.getActiveEdges, hexes(:), 'UniformOutput', 0 );
                    activeEdges = vertcat( activeEdges{:} );
                    out = [ out(:); activeEdges(:) ];
                end
                
            elseif (d==2)
                %% FACES
                % faces within faces
                out = [];
                if (s.dim>1)
                    faces = s.gElements(2);
                    activeFaces = cellfun( @(x) x.getActiveFaces, faces(:), 'UniformOutput', 0 );
                    activeFaces = vertcat( activeFaces{:} );
                    out = [ out(:); activeFaces(:) ];
                end
                
                % faces within hexes
                if (s.dim>2)
                    hexes = s.gElements(3);
                    activeFaces = cellfun( @(x) x.getActiveFaces, hexes(:), 'UniformOutput', 0 );
                    activeFaces = vertcat( activeFaces{:} );
                    out = [ out(:); activeFaces(:) ];
                end
                
            elseif (d==3)
                % hexes within hexes
                if (s.dim>2)
                    hexes = s.gElements(3);
                    activeHexes = cellfun( @(x) x.getActiveHexes, hexes(:), 'UniformOutput', 0 );
                    activeHexes = vertcat( activeHexes{:} );
                    out = [ out(:); activeHexes(:) ];
                end
                
            end
        end
        
        function computeJacobians(s)
            s.J = zeros(1,s.numElements);
            
            for e=1:s.numElements
                s.J(e) = s.gElements{ e }.computeJacobian;
            end
        end
        
        function out = get.numElements(s)
            %out = s.feLists{end}.numActiveElements;
            out = numel(s.elements);
        end
        
        function vertexInfo(s,id)
            vertex = s.vertices{ abs(id) };
            if id>0
                fprintf('  vertex %d with coords %s\n',vertex.id,num2str([vertex.X{:}]))
            else
                fprintf('  vertex %d with coords %s\n',vertex.id,num2str([vertex.X{:}]))
            end
        end
    
        function edgeInfo(s,id)
            edge = s.allEdges{ abs(id) };
            if id>0
                fprintf('  edge %d with nodes %d and %d\n',edge.id,edge.nodes{1},edge.nodes{2})
                %s.vertexInfo(edge.nodes{1});
                %s.vertexInfo(edge.nodes{2});
            else
                fprintf('  edge %d with nodes %d and %d\n',edge.id,edge.nodes{2},edge.nodes{1})
                %s.vertexInfo(edge.nodes{2});
                %s.vertexInfo(edge.nodes{1});
            end
        end
        
        function faceInfo(s,id)
            %if id>0
            face = s.allFaces{ abs(id) };
            fprintf('  face %d with edges : %d %d %d %d\n',abs(id),face.edges{:})
            %end
            edgeIDs = [s.allFaces{ abs(id) }.edges{:}];
            
            for e=edgeIDs
                s.edgeInfo(e);
            end
        end
        
        function hexInfo(s,id)
            faceIDs = [s.hexes{id}.faces{:}];
            
            for e=faceIDs
                s.faceInfo(e);
            end
        end
        
        function out = gElements(s,dimension)
            if (nargin==1)
                dimension = s.dim;
            end
            
            switch dimension
                case 0
                    out = s.vertices;
                case 1
                    out = s.allEdges;
                case 2
                    out = s.allFaces;
                case 3
                    out = s.hexes;
            end
        end
        
        function plotActive(s) 
            clf;
            
            %% vertices
            subplot(2,1,1);
            
            vertices = s.allElements{1};
            
            %active      = cellfun( @(x) x.active, vertices );
            hold on;
            cellfun( @(x) x.plot, vertices, 'UniformOutput', false );
            
            %% edges
            edges   = s.allElements{2};
            hold on;
            cellfun( @(x) x.plot, edges );
            
            %% faces
            subplot(2,1,2);            
            
            faces   = s.allElements{3};
            
            hold on;
            cellfun( @(x,id) x.plot(id), faces, num2cell( [1:numel(faces)]' ) );
            
        end
        
        function initDoFHandler(s)
            s.dh   = DoFHandler(s,true);
            s.dhNP = DoFHandler(s,false);     % non-periodic DH
            
            %filename = ['DoFHandler' int2str(System.rank) '.mat'];
            %save(filename)
        end
        
        function out = getDoFHandler(s,periodic)
            if (nargin==1)
                periodic = false;
            end
            
            if (numel(periodic)==1)
                if (periodic)
                    out = s.dh;
                else
                    out = s.dhNP;
                end
            else
                % New version: a periodic array 
                out = DoFHandlerCollection.getByID( s, periodic );
            end
        end
        
        function setDoFHandler(s,dh,periodic)
            if (periodic)
                s.dh   = dh;
            else
                s.dhNP = dh;
            end
        end
        
        function setActiveMaterial(s,material)
            s.activeMaterial = material;
            s.updateElements();
        end
    end
end

