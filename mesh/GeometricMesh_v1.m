classdef Geometry < handle
    %UNTITLED7 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Access=protected)
        sDim;
        
        numElements;
        
        X           = zeros(4,2);
        uniform     = true;
        periodic;
        
        vertices;
        edges;
        faces;
        hexes;
        
        allEdges;
        allFaces;
    end
    
    properties (GetAccess=public, SetAccess=protected)
        dim;                            % mesh dimension
        Ne = ones(1,4);                 % number of elements in each direction
    end
    
    methods
        function s = Geometry()
            %UNTITLED7 Construct an instance of this class
            %   Detailed explanation goes here
            
                % Set the dimensionality of the mesh. Here 'sdim' is the
                % spatialDimension, and 'dim' the true dimensionality of
                % the mesh including any time dimensions
                s.sDim	= spatialDim;
                s.dim   = spatialDim + spaceTime;
                                 
                % Assign Ne and X
                s.Ne = Ne;
                s.X  = X;
                
                % Ensure both have the correct amount of elements. For Ne
                % any additional directions are padded with 1 values. For X
                % 0 values are added
                s.Ne(s.dim+1:s.maxDim)	= 0;
                s.X(:,s.dim+1:s.maxDim) = 0; 
                                
                s.periodic          = periodic;
                s.uniform           = uniform;
                
            s.vertices  = [];
            s.edges     = [];
            s.faces     = [];
            s.hexes     = [];
            
        end
                
        function initGeometry(s,meshNodes)
            %% vertices
            vertexSize = cellfun( @(x) numel(x), meshNodes);
            s.vertices = cell(vertexSize);
            
            [X,Y,Z,T] = ndgrid( meshNodes{:} );
            
            for n=1:numel(s.vertices)
                s.vertices{n} = FEVertex(n,X(n),Y(n),Z(n),T(n));
            end
            
            %% edges
            s.edges = cell(4,1);
            id = 0;
            for dir=1:4
                if ( dir <= s.dim )
                    edgeSize        = vertexSize;
                    edgeSize(dir)   = edgeSize(dir)-1;
                    s.edges{dir}      = cell( edgeSize );
                    
                    switch dir
                        case 1
                            v0 = [s.vertices{1:end-1,:,:,:}];
                            v1 = [s.vertices{2:end  ,:,:,:}];
                        case 2
                            v0 = [s.vertices{:,1:end-1,:,:}];
                            v1 = [s.vertices{:,2:end  ,:,:}];
                        case 3
                            v0 = [s.vertices{:,:,1:end-1,:}];
                            v1 = [s.vertices{:,:,2:end  ,:}];
                        case 4
                            v0 = [s.vertices{:,:,:,1:end-1}];
                            v1 = [s.vertices{:,:,:,2:end  }];
                            
                    end
                    
                    for n=1:numel(v0)
                        id = id+1;
                        s.edges{dir}{n} = FEEdge(id,dir,v0(n),v1(n));
                    end
                else
                    s.edges{dir}      = [];
                end
            end
            
            s.allEdges = cellfun( @(x) x(:), s.edges, 'UniformOutput', false);
            s.allEdges = [s.allEdges{:}];
            
            %% faces
            if (s.dim==2)
                s.faces = cell(4,1);
                

                faceSize = zeros(1,4);
                for n=1:4
                    faceSize(n) = size(s.edges{n},n);
                end
                
                % z-direction
                dir = 3;                
                s.faces{dir} = cell( faceSize );

                e0 = [s.edges{1}{:,1:end-1,:,:}];   % x-direction - y0
                e1 = [s.edges{2}{2:end  ,:,:,:}];   % y-direction - x1
                e2 = [s.edges{1}{:,2:end  ,:,:}];   % x-direction - y1
                e3 = [s.edges{2}{1:end-1,:,:,:}];   % y-direction - x0

                id = 0;
                for n=1:numel(e0)
                    id = id+1;
                    s.faces{dir}{n} = FEFace(id,dir,e0(n),e1(n),e2(n),e3(n));
                end
                
                s.allFaces = cellfun( @(x) x(:), s.faces, 'UniformOutput', false);
                s.allFaces = [s.allFaces{dir}];
                
            elseif (s.dim>2)
                s.faces = cell(4,1);
                id = 0;

                faceSize_default = zeros(1,4);
                for n=1:4
                    faceSize_default(n) = size(s.edges{n},n);
                end

                for dir=1:4
                    if ( dir <= s.dim )
                        faceSize        = faceSize_default;
                        faceSize(dir)   = vertexSize(dir);
                        s.faces{dir}    = cell( faceSize );

                        switch dir
                            case 1
                                % x-normal
                                e0 = [s.edges{2}{:,:,1:end-1,:}];   % y-direction - z0
                                e1 = [s.edges{3}{:,2:end  ,:,:}];   % z-direction - y1
                                e2 = [s.edges{2}{:,:,2:end  ,:}];   % y-direction - z1
                                e3 = [s.edges{3}{:,1:end-1,:,:}];   % z-direction - y0

                            case 2
                                % y-normal
                                e0 = [s.edges{3}{1:end-1,:,:,:}];   % z-direction - x0
                                e1 = [s.edges{1}{:,:,2:end  ,:}];   % x-direction - z1
                                e2 = [s.edges{3}{2:end  ,:,:,:}];   % z-direction - x1
                                e3 = [s.edges{1}{:,:,1:end-1,:}];   % x-direction - z0

                            case 3
                                % z-normal
                                e0 = [s.edges{1}{:,1:end-1,:,:}];   % x-direction - y0
                                e1 = [s.edges{2}{2:end  ,:,:,:}];   % y-direction - x1
                                e2 = [s.edges{1}{:,2:end  ,:,:}];   % x-direction - y1
                                e3 = [s.edges{2}{1:end-1,:,:,:}];   % y-direction - x0

                            otherwise
                                error('Only 3D elements are supported')                            
                        end

                        for n=1:numel(e0)
                            id = id+1;
    %                         s.faces{dir}{n} = FEFace(id,dir,e0(n).id,e1(n).id,-e2(n).id,-e3(n).id);
                            s.faces{dir}{n} = FEFace(id,dir,e0(n),e1(n),e2(n),e3(n));
                        end
                    else
                        s.faces{dir}      = [];
                    end
                end

                s.allFaces = cellfun( @(x) x(:), s.faces, 'UniformOutput', false);
                s.allFaces = [s.allFaces{:}];
            end
            
            %% hexes
            if (s.dim>2)
                s.hexes = cell(4,1);
                id = 0;

                hexSize = zeros(1,4);
                for n=1:4
                    hexSize(n) = size(s.edges{n},n);
                end

                s.hexes = cell( hexSize );

                f0 = [s.faces{1}{1:end-1,:,:,:}];
                f1 = [s.faces{1}{2:end  ,:,:,:}];
                f2 = [s.faces{2}{:,1:end-1,:,:}];
                f3 = [s.faces{2}{:,2:end  ,:,:}];
                f4 = [s.faces{3}{:,:,1:end-1,:}];
                f5 = [s.faces{3}{:,:,2:end  ,:}];

                for n=1:numel(s.hexes)
                    id = n;
    %                 s.hexes{n} = FEHex(id,-f0(n).id,f1(n).id,-f2(n).id,f3(n).id,-f4(n).id,f5(n).id);
                    s.hexes{n} = FEHex(id,f0(n),f1(n),f2(n),f3(n),f4(n),f5(n));
                end
            end            
        end
        
        function vertexInfo(s,id)
            vertex = s.vertices{ abs(id) };
            if id>0
                fprintf('  vertex %d with coords %s\n',vertex.id,num2str([vertex.X{:}]))
            else
                fprintf('  vertex %d with coords %s\n',vertex.id,num2str([vertex.X{:}]))
            end
        end
    
        function edgeInfo(s,id)
            edge = s.allEdges{ abs(id) };
            if id>0
                fprintf('  edge %d with nodes %d and %d\n',edge.id,edge.nodes{1},edge.nodes{2})
                %s.vertexInfo(edge.nodes{1});
                %s.vertexInfo(edge.nodes{2});
            else
                fprintf('  edge %d with nodes %d and %d\n',edge.id,edge.nodes{2},edge.nodes{1})
                %s.vertexInfo(edge.nodes{2});
                %s.vertexInfo(edge.nodes{1});
            end
        end
        
        function faceInfo(s,id)
            %if id>0
            face = s.allFaces{ abs(id) };
            fprintf('  face %d with edges : %d %d %d %d\n',abs(id),face.edges{:})
            %end
            edgeIDs = [s.allFaces{ abs(id) }.edges{:}];
            
            for e=edgeIDs
                s.edgeInfo(e);
            end
        end
        
        function hexInfo(s,id)
            faceIDs = [s.hexes{id}.faces{:}];
            
            for e=faceIDs
                s.faceInfo(e);
            end
        end
        
        function out = fElements(s,dimension)
            if (nargin==1)
                dimension = s.dim;
            end
            
            switch dimension
                case 0
                    out = s.vertices;
                case 1
                    out = s.allEdges;
                case 2
                    out = s.allFaces;
                case 3
                    out = s.hexes;
            end
        end
        
        
    end
end

