classdef PolygonalMesh
    %POLYGONALMESH Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        vertices;
        edges;
        faces;
        planes;
    end
    
    methods
        function obj = PolygonalMesh(mesh)
            %POLYGONALMESH Construct an polygonal mesh data structure
            %   Detailed explanation goes here

            numElements = mesh.Ne;
            X           = mesh.X;
            periodic    = mesh.periodic;
            uniform     = mesh.uniform;
            
            
            %% FE Nodes : 1 coordinate
            coords = cell(size(X,1),1);
            
            for i=1:size(X,1)
                if (uniform)
                    if (X(i,2) > X(i,1))
                        if (~periodic(i))
                            coords{i} = linspace(X(i,1),X(i,2),Ne(i)+1);
                        else
                            coords{i} = linspace(X(i,1),X(i,2),Ne(i)  );
                        end
                    else
                        coords{i} = X(i,1);
                    end
                else
                    if (~periodic(i))
                        coords{i} = GLL(s.Ne(i)+1,X(i,1),X(i,2));
                    else
                        coords{i} = GLL(s.Ne(i)  ,X(i,1),X(i,2));
                    end
                end
            end
            
            [X,Y,Z,T] = ndgrid(coords{:});
            
            nodes( numel(X),1 ) = FENode();
            
            for n=1:numel(X)
                nodes(n) = FENode(n,X(n),Y(n),Z(n),T(n));
            end
            
            %% FE Edges : 2 FENodes
            id = 0;
            for dir=1:4
                numEdgesPerPoint = Ne(dir);
                numPoints        = max(Ne - periodic,1);
                numPoints(dir)   = 1;
                totNumPoints     = prod(numPoints);
                
                id = id+1;
                
                for p=1:totNumPoints
                    for e=1:numEdgesPerPoint
                        FEEdge(id,dir,FENode())
                    end
                end
            end
            
        end
        
        function outputArg = method1(obj,inputArg)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            outputArg = obj.Property1 + inputArg;
        end
    end
end

