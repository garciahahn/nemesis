% This class implements a StaticMesh

classdef StaticMesh < GeometricMesh %& Subscriptor
     
    properties (Access=protected)
        name;
        
        Zmap;                           % AMR mappings for all elements (identity matrix if element level == 0)
        isReady;
        
        elementRank;
        allPatchList;
        
        patchList;
        patchListCorner;
        patchListEdge;
        patchListEdgePattern;
        patchListFace;
    end
    
    properties (GetAccess=public, SetAccess=protected)        
        elements;                       % vector with active elements
        numLocalElements;               % local number of elements      
        feType;                         % the finite element type of each element
    end
    
    properties
        isChanged;
    end
    
    methods (Access=public)
        function s = StaticMesh(spatialDim,spaceTime,Ne,X,periodic,uniform,coordSystem,nodes)
           
            % Initialize the GeometricMesh
            s@GeometricMesh(spatialDim,spaceTime,Ne,X,periodic,uniform,coordSystem, nodes);
            
            if ( isequal(class(s),'StaticMesh') )
                
                % Set a default mesh name
                s.name              = 'StaticBase';
                
                % Setup additional parameters
                s.isReady   = false;
                s.isChanged = true;
                
                s.initElements;
                s.initPatch();
                
                %s.setElementLimits();
            end
        end
                
        function initElements(s)            
            % Step 1: create the elements
            s.createElements();
            
            % Step 2: assign feTypes
            s.feType(1:s.numElements) = 1;
            arrayfun( @(x) x.setFEType(1), s.elements )
        end
        
        function createElements(s)            
%             NeCell = num2cell( max(s.Ne,1) );
%             
%             s.elements  = StaticElement();
%             s.elements( NeCell{:} ) = StaticElement();  
%                         
%             elementNodes = zeros(4,2);          % contains the start/end node for an element in each direction
%             
%             ranges = max(1, s.Ne);
            
            % Extract the finiteElements within this mesh
            fElements       = s.fElements( s.dim );
            
            % Initialize the elements object array
            s.elements = StaticElement();
            s.elements( 1,s.numElements ) = StaticElement(); 
            
            for e=1:numel(fElements)
                s.elements( e ) = StaticElement( e, fElements{e} );
            end
        end
                

               
        % This function initializes the mesh nodes
        function initMeshNodes(s)
            
            for i=1:4
                s.meshNodes{i} = 0;
            end
            
            % fill grid node lists
            if (s.uniform)                
                for i=1:s.dim
                    %if (s.periodic(i))
                        s.meshNodes{i} = linspace(s.X(1,i),s.X(2,i),s.Ne(i)+1);
                    %else
                    %    s.meshNodes{i} = linspace(s.X(i,1),s.X(i,2),s.Ne(i)+1);
                    %end
                end
            else
                for i=1:s.dim
                    %if (s.periodic(i))
                        s.meshNodes{i} = GLL(s.Ne(i) + 1,s.X(1,i),s.X(2,i));
                    %else
                    %    s.meshNodes{i} = GLL(s.Ne(i)+1,s.X(i,1),s.X(i,2));
                    %end
                end
            end
            
            s.initGeometry( s.meshNodes );
        end

        function setElementIDs(s)
            %s.distributeElements;
            
            dofHandler = s.getDoFHandler(true);
            
            s.eStart = dofHandler.eStart;
            s.eEnd   = dofHandler.eEnd;
            s.eRange = dofHandler.eRange;
            
            s.numLocalElements = numel(s.eRange);
                            
            count = 0;
            
            % Initialize the rank for all elements to -1, and set the 
            for e=1:s.numElements
                
                element = s.getElement(e);
                
                if ismember(e,s.eRange)
                    count = count+1;
                    %element.rank    = NMPI.instance.rank;                    
                    element.localID = count;
                else
                    %element.rank    = -1; 
                    element.localID = [];                                       
                end
                element.id = e;
            end
            %ranks = s.getElementRanks; 
            
            %if (System.settings.anal.debug)
            %    fprintf('number of elements in ranks : %d\n',numel(ranks));
            %end
            
            %if (System.nproc>1)
            %    ranksAll = NMPI_Allreduce(ranks,numel(ranks),'M',NMPI.instance.mpi_comm_world) ; % maximum function
            %else
            %    ranksAll = ranks;
            %end
                
            %s.setElementRanks(ranksAll);
            
            s.setElementRank();
        end
        
        function createZ(s)
            s.Z = cell(System.parallel.numLocalElements,1);
            for e=s.eRange
                element = s.getElement(e);
                try
                    s.Z{ element.localID } = element.createZ();
                catch
                    disp('')
                end
            end
        end
        
        function createBoundaryPatches(s)
            
            locations = {'x0','x1','y0','y1'};
            
            for id=1:numel(locations)
                
                location = locations{id};
                
                % Select the elements to modify with pillars
                if (strcmp(location,'y0'))
                    selectedElements = s.data(:,1,:);       % s.data(:,1) contains the bottom elements of an unrefined mesh (L0)
                elseif (strcmp(location,'y1'))
                    selectedElements = s.data(:,end,:);     % s.data(:,end) contains the top elements of an unrefined mesh (L0)
                elseif (strcmp(location,'x0')) 
                    selectedElements = s.data(1,:,:);       % s.data(1,:) contains the left elements of an unrefined mesh (L0)
                elseif (strcmp(location,'x1')) 
                    selectedElements = s.data(end,:,:); 	% s.data(end,:) contains the right elements of an unrefined mesh (L0)
                elseif (strcmp(location,'z0')) 
                    selectedElements = s.data(:,:,1);       % s.data(:,:,1) contains the front elements of an unrefined mesh (L0)
                elseif (strcmp(location,'z1')) 
                    selectedElements = s.data(:,:,end); 	% s.data(:,:,end) contains the back elements of an unrefined mesh (L0)
                else
                    error('This pillar location is not supported\n')
                end

                % Depending on the location of the pillars the flags need to be
                % flipped to ensure they are actually created on the walls and
                % not as floating blocks
                switch location(1)
                    case 'x'
                        if (location(end)=='1') 
                            neighborMaterials = [Material.Fluid, Material.Solid, Material.Fluid, Material.Fluid];   % left/right, bottom/top
                        else
                            neighborMaterials = [Material.Solid, Material.Fluid, Material.Fluid, Material.Fluid];   % left/right, bottom/top
                        end
                    case 'y'
                        if (location(end)=='1') 
                            neighborMaterials = [Material.Fluid, Material.Fluid, Material.Fluid, Material.Solid];   % left/right, bottom/top
                        else
                            neighborMaterials = [Material.Fluid, Material.Fluid, Material.Solid, Material.Fluid];   % left/right, bottom/top
                        end
    %                 case 'z'
    %                     if (location(end)=='1')
    %                         solidPattern_z = fliplr(solidPattern_z);
    %                     end
                end

                for e0 = 1:numel(selectedElements)
                    element_L0 = selectedElements(e0);
                    element_L0.setTag( location, neighborMaterials );                % Tag geometric fluid elements that have no fluid neighbor
                end
                
                % Create a patch of the elements that surround the pillars,
                % which will replace the default (flat) bottom patch (y=0)
                %s.createPillarPatch(Dir.y,Position.low);
                patch = Patch(s,id,location);
                PatchCollection.addreplace(patch);

                if (System.rank==0 && System.debug)
                    fprintf('MESH       : Patch has been created (%s)\n',patch.tag);
                end
                
            end
        end
        
        %% Patches & boundaries
        function initPatch(s)
            
            % New approach: PatchCollection
            %s.createBoundaryPatches;
            
            % Old approach: patchList
            s.allPatchList          = cell(10,1);
            
            s.patchList             = cell(2,s.dim);
            s.patchListCorner       = cell(2^(s.dim),1);
            s.patchListEdge         = cell(2*(s.dim-1),1);
            s.patchListEdgePattern  = cell(2*(s.dim-1),1);
            
            id = 0;
            
            for dir=1:s.dim
                for pos=1:2
                    id = id+1;
                    
                    patchName   = 'meshBoundary';
                    condition   = sprintf('%s=%f',string(Dir(dir)),s.X(pos,dir));
                    connected   = false;
                    
                    patch = Patch(s,id,patchName,condition,connected);
                    
                    s.patchList{ pos,dir } = patch;         % old: patchList
                    PatchCollection.add( patch );           % new: use PatchCollection
                end 
            end
            
            %% MESH CORNERS
            count = 0;
            switch s.sDim
                case 1
                    for dir1=1:s.sDim
                        id = id+1;
                        count = count+1;

                        patchName   = 'meshEdgeBoundary';
                        condition   = sprintf('%s=%f',string(Dir(1)),s.X(dir1,1));
                        connected   = false;
                        
                        patch = Patch(s,id,patchName,condition,connected);
                        s.patchListCorner{ count,1 } = patch;
                        PatchCollection.add( patch );           % new: use PatchCollection
                    end
            
                case 2
                    for dir2=1:s.sDim
                        for dir1=1:s.sDim
                            id = id+1;
                            count = count+1;

                            patchName   = 'meshCornerBoundary';
                            condition   = sprintf('%s=%f,%s=%f',string(Dir(1)),s.X(dir1,1),...
                                                                string(Dir(2)),s.X(dir2,2));
                            connected   = false;

                            patch = Patch(s,id,patchName,condition,connected);
                            s.patchListCorner{ count,1 } = patch;
                            PatchCollection.add( patch );           % new: use PatchCollection
                        end
                    end
                    
                case 3
                    for dir3=1:2
                        for dir2=1:2
                            for dir1=1:2
                                id = id+1;
                                count = count+1;

                                patchName   = 'meshCornerBoundary';
                                condition   = sprintf('%s=%f,%s=%f,%s=%f',...
                                                string(Dir(1)),s.X(dir1,1) ,...
                                                string(Dir(2)),s.X(dir2,2) ,...
                                                string(Dir(3)),s.X(dir3,3) );
                                connected   = false;
                                            
                                patch = Patch(s,id,patchName,condition,connected);
                                s.patchListCorner{ count,1 } =  patch;
                                PatchCollection.add( patch );           % new: use PatchCollection
                            end
                        end
                    end
                    
                otherwise 
                    error('Unsupported spatial dimension for the mesh')
            end

            
            % mesh edges
            count = 0;
            if (s.sDim==2)
                % 2D mesh, create the boundery which is an edge
                for dir2=1:s.sDim
                    for dir1=1:s.sDim
                        id = id+1;
                        count = count+1;

                        patchName   = 'meshEdgeBoundary';
                        %condition   = sprintf('%s=%f,%s=%f',string(Dir(1)),s.X(dir1,1),...
                        %                                    string(Dir(2)),s.X(dir2,2));
                        condition   = sprintf('%s=%f',string(Dir(dir2)),s.X(dir1,dir2));
                        connected   = false;

                        s.patchListEdge{ count,1 } = Patch(s,id,patchName,condition,connected);
                    end
                end
                
                % 2D mesh, create the patterned boundery which is an edge
                if (s.maxLevel == 2)
                    for dir2=1:s.sDim
                        for dir1=1:s.sDim
                            id = id+1;
                            count = count+1;

                            patchName   = 'meshEdgeBoundary';
    %                         condition   = sprintf('%s=%f,%s=%f',string(Dir(1)),s.X(dir1,1),...
    %                                                             string(Dir(2)),s.X(dir2,2));
                            condition   = sprintf('%s=%f',string(Dir(dir2)),s.X(dir1,dir2));
                            connected   = true;

                            s.patchListEdgePattern{ count,1 } = Patch(s,id,patchName,condition,connected);
                        end
                    end
                end
                
            elseif (s.sDim==1)
                for dir1=1:s.sDim
                    id = id+1;
                    count = count+1;

                    patchName   = 'meshEdgeBoundary';
                    condition   = sprintf('%s=%f',string(Dir(1)),s.X(dir1,1));
                    connected   = false;
                    
                    s.patchListEdge{ count,1 } = Patch(s,id,patchName,condition,connected);
                end
            end
            
            %% Mesh faces (only 3D)
            if (s.sDim==3)
                
                s.patchListFace = cell(2*(s.dim),1);
                
                id = 0;
                
                % Loop over all 6 faces
                for sDir=1:3
                    for pos=1:2
                        id = id+1;

                        patchName   = 'meshFaceBoundary';
                        condition   = sprintf('%s=%f',string(Dir(sDir)),s.X(pos,sDir));
                        connected   = false;
                        
                        s.patchListFace{ id,1 } = Patch(s,id,patchName,condition,connected);
                    end
                end
                
                
            end
            %end
        end
        
        function addPatch(s,id,name,condition)
            s.patchList{id} = Patch(s,id,name,condition);
        end
        
        function updatePatch(s)
            cellfun( @(x) x.update, s.patchList );
            
            PatchCollection.update;
        end
        
        %% Assign boundaryElements 
        function updateBoundaries(m)
            m.BC = cell(m.disc.DIM,2);

            for dir=1:m.disc.DIM
                m.BC{dir,1} = Boundary(dir,0,m,m.disc.boundary{dir,1});
                m.BC{dir,2} = Boundary(dir,1,m,m.disc.boundary{dir,2});
            end
        end
        
%         function distributeElements(s)
% %             nproc = NMPI.instance.nproc;
% %             rank  = NMPI.instance.rank;
% %             
% %             s.eStart = round((rank  )*prod(s.Ne)/nproc)+1;
% %             s.eEnd   = round((rank+1)*prod(s.Ne)/nproc)  ;
% %             
% %             s.eRange = s.eStart : s.eEnd;
%             
%             s.eStart = s.dh.eStart;
%             s.eEnd   = s.dh.eEnd;
%             s.eRange = s.dh.eRange;
%             
%             s.numLocalElements = numel(s.eRange);
%         end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        function setStandardElement(s,standardElement)
            s.disc = standardElement;
        end
        
        function out = getPatchByID(s,id)
            out = s.allPatchList{id};
        end

        function b = getPatch(m,dir,pos)
            b = m.patchList{pos+1,dir};
        end
        
        function b = getCornerPatch(m,pos)
            b = m.patchListCorner{pos};
        end
        
        function b = getEdgePatch(m,pos)
            b = m.patchListEdge{pos};
        end
        
        function b = getFacePatch(m,pos)
            b = m.patchListFace{pos};
        end
        
        function b = getPatterenedEdge(m,dir,pos)
            disp('')
%             b = m.patchList{pos+1,dir};
        end
      
        function update(s)
            
            s.dh.update;
            
            if (~s.isReady)
                %s.setElementLimits();
                %
                %s.createZ(); 
                
                % DoFHandler:
                %s.dofHandler.update();
                
                %s.createGM();
                %s.setDofRank();
                %s.setLocalGM();
                %s.setMutualDof();
                
                % Patches / boundaries
%                 s.updatePatch();
                %s.updateBoundaries();
                
                s.isReady = true;
            end
        end
        
        % Function to update the 'active' flag for all GeometricElements
        function updateActiveGeometricElements(s)
            s.allElements = cell(1,s.dim+1);
            
%             t1 = toc;
            
            for i=1:s.dim+1
                s.allElements{i} = s.getAllElements(i-1);
                if (~isempty(s.allElements{i}))
                    cellfun( @(x) x.resetActive(false), s.allElements{i} )
                end
            end
            
%             t2 = toc;
            
            % Deactivate all geometric elements (recursively) before continuing
            %cellfun( @(x) x.resetActive, s.gElements(s.dim));
             
%             t3 = toc;
%             t4 = t2-t1;
%             t5 = t3-t2;
            
%             fprintf('cellfun( @(x) x.updateActivePeriodic(), s.gElements(s.dim)) time --- NEW: %f   OLD: %f\n',t4,t5)
            
            % Reset the active flags for all elements (Material.All)
            allElements = s.getElementsByMaterial(Material.All);
            for i = 1:numel(allElements)
                allElements(i).active = false;
            end

            % Set the new active flags for all 'activeMaterial' elements
            activeFlags = s.checkMaterial(s.activeMaterial);
            for e=1:numel(s.elements)
                s.elements(e).active = activeFlags(e);
                s.elements(e).geometric.updateActive( activeFlags(e) );
            end
            
%             % Loop over the main GeometricElements, and make the
%             % subElements active if they fulfill some conditions
%             %cellfun( @(x) x.updateActive(true), s.gElements(s.dim));
%             for e=1:numel(s.elements)
%                 if (s.elements(e).checkMaterial(s.activeMaterial))
%                     activeFlag = true;
%                 else
%                     activeFlag = false;
%                 end
%                 s.elements(e).active = activeFlag;
%                 
%                 
%                 s.elements(e).geometric.updateActive( activeFlag );
%                 
%                 %s.gElements{e}.updateActive( activateFlag );
%             end
            
            % Loop again to ensure periodic elements are treated as they should
            cellfun( @(x) x.updateActivePeriodic(), s.gElements(s.dim));
            
            withCheck = true;
            if (withCheck)
                for i=1:s.dim+1
                    s.allElements{i} = s.getAllElements(i-1);

                    activeFlags = cellfun( @(x) x.active, s.allElements{i} );

                    if ~all(activeFlags)
                        error('LSQDIM:mesh:GeometricMesh:updateActiveGeometricElements :: not all geometric elements in allElements are active (they should be)')
                    end
                end

                s.numElements = numel( s.allElements{s.dim+1} );

                for i=1:s.dim+1
                    s.gElementCounts( i ) = numel( s.allElements{i} );
                end
            end
        end
        
        function out = getElementRank(s)
            out = s.elementRank(:,NMPI.instance.rank+1);
        end
        
        function setElementRank(s)
            % First create a sparse logical array where all the elements from
            % each rank are set to false
            s.elementRank = sparse(false(s.numElements,NMPI.instance.nproc));
            for e=1:s.numElements
                element = s.getElement(e);
                s.elementRank( e, element.rank+1 ) = true;
            end
            
%             % Compute the 'numLocalDof' on this processor
%             s.numLocalElements = nnz( s.elementRank(:,NMPI.instance.rank+1) );
        end
        
        function setName(s,name)
            s.name = name;
        end
        
        function out = getName(s)
            out = s.name;
        end
        
        % Returns the Z array for global element number e. Since the Z
        % array is stored with local numbering, a conversion is made.
        function out = getZ(m,e)            
            out = m.Z{ m.getElement(e).localID };
        end
            
        % Convert a global element number to a local number
        function out = g2l(s,e)
            out = e - s.eOffset;
        end
        
        % Convert a local element number to a global number
        function out = l2g(s,e)
            out = s.eOffset + e;
        end
        
        % Returns a local element n, here the mapping from global to local 
        % is made
        function out = getElement(s,n)
            if ~ismember(n,s.eRange)
                error('MeshRefine3D:getElement','The element number %d is outside the global range %d:%d\n',n,s.eRange(1),s.eRange(end));
            end
            out = s.octTree.getElement(n);
        end
        
        function out = getLocalElement(s,e)
            % GETLOCALELEMENT  Returns the element based on local numbering
            %   out = getLocalElement(elementNumber) 
            %
            if e>s.numLocalElements
                error('MeshRefine3D:getLocalElement','The element number %d is outside the local range 1:%d\n',e,s.numLocalElements);
            end
            out = s.octTree.getElement( s.l2g(e) );
        end
        
        function out = getLocalElements(s)
            out = cell(System.parallel.numLocalElements,1);
            for e=System.parallel.eRange
                out{e-System.parallel.eOffset} = s.getElement(e);
            end
        end
        
        function out = Z(s,e)
            out = s.Zmap;
            if (nargin==2)
                out = s.Zmap{e};
            elseif (nargin>2)
                error('Too many input parameters')
            end
        end
        
        function out = H(s,e)
            if nargin==2
                finiteElement = FECollection.get( s.feType(e) );
                out = finiteElement.H;
            else
                out = cell(1,numel(s.feType));
                for e=1:numel(out)
                    out{e} = s.H(e);
                end
            end
        end
        
        function out = W(s,e)
            if nargin==2
                finiteElement = FECollection.get( s.feType(e) );
                out = finiteElement.W / s.elements(e).J;
            else
                out = cell(1,numel(s.feType));
                for e=1:numel(out)
                    out{e} = s.W(e);
                end
            end
        end
        
        function out = getProdJ(s)
            %if (s.isChanged)
%                 out = s.old.prodJ;
            %else
                out = zeros(s.numLocalElements,1);
                
                c=1;
                for e=s.eRange
                    out(c) = prod( s.getElement(e).J );
                    c=c+1;
                end
            %end
        end
        
        function out = getElementRanks(s,n)
            tmp = s.getElement;
            if (nargin==1)
                out = [tmp.rank];
            else
                out = tmp(n).rank;
            end
        end
        
%         function setElementRanks(s,values)
%             tmp = s.getElement;
%             if values==0
%                 for i=1:numel(tmp)
%                     s.getElement(i).rank = 0;
%                 end
%             else
%                 for i=1:numel(tmp)
%                     s.getElement(i).rank = values(i);
%                 end
%             end
%         end
    
        function out = getElementPosition(s,n)
            element     = s.getElement(n);
            elementLv   = element.getLv;
            
            out = zeros(3,elementLv+1);
            
            for l=elementLv:-1:0
                out(:,l+1) = element.getPosition;
                if l>0
                    element    = element.getParent;
                end
            end
        end
        
        function out = getElementByPosition(s,position)
            out = s.getElementByPosition(num2cell(position));
        end
        
        %% PLOTTING
        function fig = plot( s, fig, position, title_string, tag, material )
        %UNTITLED4 Summary of this function goes here
        %   Detailed explanation goes here
            
            % fig
            if ~exist('fig','var') || isempty(fig)
                fig = figure(1);
            end
            
            % position
            if ~exist('position','var') || isempty(position)
                position = 0;
            end
            
            % title_string
            if ~exist('title_string','var')  || isempty(title_string)
                title_string = 'mesh';
            end
            
            % tag_in
            if ~exist('tag','var')  || isempty(tag)
                tag = 1:s.getNumElements;
            end
            
            % material
            if ~exist('material','var')  || isempty(material)
                material = s.activeMaterial;
            end
            
%             if (nargin>1)
%                 
%                 fig = figure(1);
%                 position = 0;
%                 title_string = 'mesh';
%             end
        
            if (position<=1)
                clf(fig)
                screensize = get( groot, 'Screensize' );
                sizex = 1000;
                sizey = 1000;
                set(fig,'position',[0.5*(screensize(3)-sizex) 0.5*(screensize(4)-sizey) sizex sizey]); 
            end

            if (position>0)
                subplot(3,1,position);
            end

            dir = 0;
            
            for e=1:s.getNumElements
            %             surf(mesh.xgbl{e},mesh.ygbl{e},c{e}(:,:,1));
            %             surf(mesh.xgbl{e}(1:end-1:end),mesh.ygbl{e}(1:end-1:end),c{e}(1:end-1:end,1:end-1:end,1));
        %         surf(mesh.xgbl{e}(1:end-1:end),mesh.ygbl{e}(1:end-1:end),0*c{e}(1:end-1:end,1:end-1:end,1));
        
%                 tag = num2str( mesh.octTree.getElement(e).wantsCoarsening )
                element = s.getElement(e);

                if (element.checkMaterial(material))

                    if ~isempty(tag)
                        plotWireframe(e,dir,s,s.getElementRanks(e),tag(e));
                    else
                        plotWireframe(e,dir,s,s.getElementRanks(e),'');
                    end
                    
                end

%                 if ~isempty(tag)
%                     plotPlane(e,dir,s,s.getElementRanks(e),tag(e));
%                 else
%                     plotPlane(e,dir,s,s.getElementRanks(e),'');
%                 end
                
                hold on;
            end
            title(title_string)
            colorbar off
            hold off
            
            colormap(lines);
            
            if (dir==3)
                view([0,90])
                pbaspect([(s.x1-s.x0) (s.y1-s.y0) 1])
            elseif (dir==2)
                view([90,0])
                pbaspect([1 (s.y1-s.y0) 100*(s.t1-s.t0)])
            elseif (dir==1)
                view([0,0])
                pbaspect([(s.x1-s.x0) 1 100*(s.t1-s.t0)])
            elseif (dir==0)
                view([0,90])
%                 if (s.dim==1)
%                     pbaspect([(s.X(1,2)-s.X(1,1)) 1 1])
                    
                %elseif (s.dim==2 || (s.dim==3 && s.disc.spaceTime) )
                
                    %range = s.X(2,:)-s.X(1,:);
                    %range( range==0 ) = 1;
                    %pbaspect( range(1:3) )
                
                    %else                    
                %    pbaspect([(s.X(1,2)-s.X(1,1)) (s.X(2,2)-s.X(2,1)) (s.X(3,2)-s.X(3,1))])
                %end
            end

            xlabel('x') ;
            ylabel('y') ;
            
%             if (s.disc.spaceTime)
%                 zlabel('t') ;
%             else
%                 zlabel('z') ;
%             end
        end
        
        function fig = plotFluid(s)
            fig = s.plot([],[],[],[],Material.Fluid);
        end
        
        function fig = plotSolid(s)
            fig = s.plot([],[],[],[],Material.Solid);
        end
        
        %% SAVE / LOAD
        % Function to create a struct of the mesh, which can be saved in a
        % restart/solution file. The mesh can be restored by using the static 
        % load function (in combination with a standardElement).
        function mesh = save(s)
            mesh = struct;
            
            mesh.name       = s.name;
            mesh.X          = s.X;
            mesh.Ne         = s.Ne;
            mesh.uniform    = s.uniform; 
            mesh.periodic   = s.periodic;
            mesh.maxLevel   = s.maxLevel;            
            mesh.class      = class(s);
        end
        
        function CFL = computeCFL(s)
            if (System.settings.stde.sDim==2 && System.settings.stde.spaceTime)
                dxCFL           = max( (s.x1-s.x0)/(s.Nex*(s.disc.P(1)+1)^2) ,(s.y1-s.y0)/(s.Ney*(s.disc.P(2)+1)^2)) ;
                dtCFL           = System.settings.mesh.deltaT/(s.Nez*(s.disc.P(3)+1)^2) ;
            end
            CFL = dtCFL/dxCFL; 
        end
        
        function createMeshgrids(s)
            for e=1:s.numLocalElements
                element = s.getLocalElement(e);
                if isempty(element.xGrid)
                    [element.xGrid,element.yGrid] = meshgrid(element.xelem,element.yelem);
                end
            end    
        end
        
        function [xGrids,yGrids,zGrids] = getMeshgrids(s)
            s.createMeshgrids;            
            xGrids = arrayfun(@(x) x.xGrid, s.elements,'UniformOutput',false);
            yGrids = arrayfun(@(x) x.yGrid, s.elements,'UniformOutput',false);
            zGrids = arrayfun(@(x) x.zGrid, s.elements,'UniformOutput',false);
        end
        
        % Function which returns all OctreeElements of 'material' that 
        % have a (geometric) subElement that is tagged by 'tag', and 
        function [taggedElements,geometricElements,directions,normals,edgePositions] = getElementsByTag(s,tag,material)
            taggedElements      = [];
            geometricElements   = [];
            directions          = [];
            normals             = [];
            edgePositions       = [];
            
            if (nargin==3)
                selectedElements = s.getElementsByMaterial( material );
            else
                selectedElements = s.elements;
            end
            
            % Loop over all elements, 
            for e=1:numel(selectedElements)
                currentElement = selectedElements(e);
                
                % Add the currentElement to the list if the tag matches
                if ( currentElement.material == material && currentElement.hasTag(tag) )
                    [hasTag,gElements,direction,normal,edgePosition] = currentElement.hasTag(tag);
                    if (hasTag)
                        taggedElements      = [ taggedElements      , repmat(currentElement,1,numel(gElements)) ];
                        geometricElements   = [ geometricElements   , gElements      ];
                        directions          = [ directions          , direction      ];
                        normals             = [ normals             , normal         ];
                        edgePositions       = [ edgePositions       , edgePosition   ];
                    end
                end
            end
        end
        
%         function out = getElementsByMaterial(s,material)
%             if (nargin==2)
%                 out = s.elements( [s.elements.material]==material );
%             else
%                 out = s.elements( [s.elements.material]==s.activeMaterial );
%             end
%         end

        % Check the materials of the whole mesh
        function out = checkMaterial(s,activeMaterial)
            if activeMaterial==Material.All
                out = true( size(s.elements) );
            else
                out = ( [s.elements.material] == activeMaterial );
            end
        end
        
        function elements = getElementsByMaterial(s,material)
            if (nargin~=2)
                fprintf('Please provide a material to create the list, now the default (activeMaterial) is used.\n')
                material = s.activeMaterial;
            end
            
            position    = 0;
            
            for e=1:numElements
                if (s.data(e).material==material)
                    position = position+1;
                    elements( position ) = s.data(e);
                end
            end
        end
    end % public methods
    
    methods (Access = protected)
        function plotPlane(elementNumber,dir,mesh,rank,tag)
%             loc = cell2struct(mesh.octTree.getElement(elementNumber).getLocations,{'x','y','t'},2);
            loc = mesh.getElement(elementNumber).nodes;
            
            if isnumeric(tag)
                tag = num2str(tag);
            elseif islogical(tag)
                tag = num2str( int8(tag) );
            end
             
            plotDimension = mesh.dim;
            
            switch plotDimension
                case 1                   
                    % 2D plot
                    xVec = mesh.getElement(elementNumber).nodes{1};

                    p1 = [xVec( 1 ) 0.02 0];
                    p2 = [xVec(end) 0.02 0];
                    p3 = [xVec(end) 0.02 0];
                    p4 = [xVec( 1 ) 0.02 0];
            
                case 2
                    % 2D plot
                    xVec = mesh.getElement(elementNumber).nodes{1};
                    yVec = mesh.getElement(elementNumber).nodes{2};

                    p1 = [xVec( 1 ) yVec( 1 ) 0];
                    p2 = [xVec(end) yVec( 1 ) 0];
                    p3 = [xVec(end) yVec(end) 0];
                    p4 = [xVec( 1 ) yVec(end) 0]; 
                case 3
                    % 3D plot
                    xVec = loc{1};
                    yVec = loc{2};
                    tVec = loc{3} - loc{3}(1);

                    p1 = [xVec( 1 ) yVec( 1 ) tVec( 1 )];
                    p2 = [xVec(end) yVec( 1 ) tVec( 1 )];
                    p3 = [xVec(end) yVec(end) tVec( 1 )];
                    p4 = [xVec( 1 ) yVec(end) tVec( 1 )]; 
                    p5 = [xVec( 1 ) yVec( 1 ) tVec(end)];
                    p6 = [xVec(end) yVec( 1 ) tVec(end)];
                    p7 = [xVec(end) yVec(end) tVec(end)];
                    p8 = [xVec( 1 ) yVec(end) tVec(end)]; 
            end
             
%             xVec = mesh.octTree.getGlobalElement(elementNumber).xelem;
%             yVec = mesh.octTree.getGlobalElement(elementNumber).yelem;
%             tVec = mesh.octTree.getGlobalElement(elementNumber).zelem;
            


            hold on;
            
            isLocal = ismember(elementNumber,mesh.eRange);
            
            if isLocal
                color = [0 1 0];
            else
                color = [1 0 0];
            end
            
            if (nargin==4)
                tag = num2str(elementNumber);
            end

            if (plotDimension==3)            
                if (dir==1 || dir==0)
                    c1 = p1; c2 = p5; c3 = p6; c4 = p2;     % bottom
                    if (abs(c1(2)-mesh.X(2,1))<1e-14)
                        x = [c1(1) c2(1) c3(1) c4(1)];
                        y = [c1(2) c2(2) c3(2) c4(2)];
                        t = [c1(3) c2(3) c3(3) c4(3)];
                        fill3(x, y, t, color);
                    end
                end
                if (dir==2 || dir==0)
                    c1 = p5; c2 = p1; c3 = p4; c4 = p8;     % right
                    if (abs(c1(1)-mesh.X(1,1))<1e-14)
                        x = [c1(1) c2(1) c3(1) c4(1)];
                        y = [c1(2) c2(2) c3(2) c4(2)];
                        t = [c1(3) c2(3) c3(3) c4(3)];
                        fill3(x, y, t, color);
                    end
                end
                if (dir==3 || dir==0)
                    c1 = p2; c2 = p6; c3 = p7; c4 = p3;     % left
                    if (abs(c1(1)-mesh.X(1,2))<1e-14)
                        x = [c1(1) c2(1) c3(1) c4(1)];
                        y = [c1(2) c2(2) c3(2) c4(2)];
                        t = [c1(3) c2(3) c3(3) c4(3)];
                        fill3(x, y, t, color);
                    end
                end
                if (dir==4 || dir==0)
                    c1 = p4; c2 = p8; c3 = p7; c4 = p3;     % top
                    if (abs(c1(2)-mesh.X(2,2))<1e-14)
                        x = [c1(1) c2(1) c3(1) c4(1)];
                        y = [c1(2) c2(2) c3(2) c4(2)];
                        t = [c1(3) c2(3) c3(3) c4(3)];
                        fill3(x, y, t, color);
                    end
                end
            end
            
            if (plotDimension>1)
                if (dir==5 || dir==0)        
                    c1 = p1; c2 = p2; c3 = p3; c4 = p4;     % front
                    if (plotDimension==2 || abs(c1(3)-mesh.X(3,1))<1e-14)
                        x = [c1(1) c2(1) c3(1) c4(1)];
                        y = [c1(2) c2(2) c3(2) c4(2)];
                        t = [c1(3) c2(3) c3(3) c4(3)];
                        fill3(x, y, t, color);
                        text((c3(1)-c1(1))/2+min(c1(1),c3(1)),(c3(2)-c1(2))/2+min(c1(2),c3(2)),(c3(3)-c1(3))/2+min(c1(3),c3(3)),tag,'HorizontalAlignment','center')
                    end
                end
            else
                if (dir==5 || dir==0)        
                    c1 = p1; c2 = p2; c3 = p3; c4 = p4;
                    x = [c1(1) c2(1) c3(1) c4(1)];
                    y = [c1(2) c2(2) c3(2) c4(2)];
                    t = [c1(3) c2(3) c3(3) c4(3)];
                    fill3(x, y, t, color);
                    text((c3(1)-c1(1))/2+min(c1(1),c3(1)),(c3(2)-c1(2))/2+min(c1(2),c3(2)),(c3(3)-c1(3))/2+min(c1(3),c3(3)),tag,'HorizontalAlignment','center')
                end
            end
            
            if (plotDimension==3)
                if (dir==6 || dir==0)        
                    c1 = p5; c2 = p6; c3 = p7; c4 = p8;     % back
                    if (abs(c1(3)-mesh.X(3,2))<1e-14) 
                        x = [c1(1) c2(1) c3(1) c4(1)];
                        y = [c1(2) c2(2) c3(2) c4(2)];
                        t = [c1(3) c2(3) c3(3) c4(3)];
                        fill3(x, y, t, color);
                        text((c3(1)-c1(1))/2+min(c1(1),c3(1)),(c3(2)-c1(2))/2+min(c1(2),c3(2)),(c3(3)-c1(3))/2+min(c1(3),c3(3)),tag,'HorizontalAlignment','center')
                    end
                end
            end
        end
        
        function plotWireframe(elementNumber,dir,mesh,rank,tag)
            loc = mesh.getElement(elementNumber).nodes;
            
            if isnumeric(tag)
                tag_string = num2str(tag);
            elseif islogical(tag)
                tag_string = num2str( int8(tag) );
            end
            
            plotDimension = mesh.dim;
             
            switch plotDimension
                case 1
                    xVec = mesh.getElement(elementNumber).nodes{1};

                    %p1 = [xVec( 1 ) 0 0];
                    %p2 = [xVec(end) 0 0];
                    
                    xVec = loc{1};
                    v = zeros(2,2);

                    % Start point
                    v(1,1) = xVec( 1 );
                    v(1,2) = 0;
                    
                    % End point
                    v(2,1) = xVec(end);
                    v(2,2) = 0;

                    hold on;
                    plot(v(:,1),v(:,2))

                    if (mod(tag,1)==0)
                        xc = xVec(1) + 0.5 * ( xVec(end) - xVec(1) );
                        text(xc,0,tag_string,'HorizontalAlignment','center')
                    end
            
                case 2
                    xVec = mesh.getElement(elementNumber).nodes{1};
                    yVec = mesh.getElement(elementNumber).nodes{2};

                    p1 = [xVec( 1 ) yVec( 1 ) 0];
                    p2 = [xVec(end) yVec( 1 ) 0];
                    p3 = [xVec(end) yVec(end) 0];
                    p4 = [xVec( 1 ) yVec(end) 0]; 

                    xVec = loc{1};
                    yVec = loc{2};

                    v = zeros(4,2);

                    v(1,:) = [xVec( 1 ) yVec( 1 )];
                    v(2,:) = [xVec(end) yVec( 1 )];
                    v(3,:) = [xVec(end) yVec(end)];
                    v(4,:) = [xVec( 1 ) yVec(end)];

                    hold on;
                    isLocal = ismember(elementNumber,mesh.eRange);

                    % plot solid vertices
                    vertices = mesh.getElement(elementNumber).geometric.vertices;
                    for e=1:numel(vertices)
                        if ( strcmp(vertices(e).getTag,'bottomPillars') && vertices(e).active )
                            vertices(e).plot('r');
                        end
                    end
                    
                    % plot solid edges
                    edges = mesh.getElement(elementNumber).geometric.edges;
                    for e=1:numel(edges)
                        if ( strcmp(edges(e).getTag,'bottomPillars') && edges(e).active )
                            linewidth = 4;
                            scaling = 1.0;
                            edges(e).plot(linewidth,scaling);
                        end
                    end
                    
                    f = [1 2 3 4];
                    col = [0; 1];
                    
                    if (mesh.getElement(elementNumber).material==Material.Solid)
                        patch('Faces',f,'Vertices',v,'FaceVertexCData',col,'FaceColor','red');
                    else
                        patch('Faces',f,'Vertices',v,'FaceVertexCData',col,'FaceColor','none');
                    end

                    if (mod(tag,1)==0)
                        xc = xVec(1) + 0.5 * ( xVec(end) - xVec(1) );
                        yc = yVec(1) + 0.5 * ( yVec(end) - yVec(1) );
                        text(xc,yc,tag_string,'HorizontalAlignment','center')
                    end
                    
                case 3
                    try
                    xVec = loc{1};
                    yVec = loc{2};
                    zVec = loc{3};

                    catch
                        disp('')
                    end
                    
                    v = zeros(8,3);

                    v(1,:) = [xVec( 1 ) yVec( 1 ) zVec( 1 )];
                    v(2,:) = [xVec(end) yVec( 1 ) zVec( 1 )];
                    v(3,:) = [xVec(end) yVec(end) zVec( 1 )];
                    v(4,:) = [xVec( 1 ) yVec(end) zVec( 1 )]; 
                    v(5,:) = [xVec( 1 ) yVec( 1 ) zVec(end)];
                    v(6,:) = [xVec(end) yVec( 1 ) zVec(end)];
                    v(7,:) = [xVec(end) yVec(end) zVec(end)];
                    v(8,:) = [xVec( 1 ) yVec(end) zVec(end)]; 

                    hold on;

                    isLocal = ismember(elementNumber,mesh.eRange);

                    f = [1 2 3 4; 5 6 7 8; 1 2 6 5; 4 3 7 8];
                    col = [0; 1; 2];
                    patch('Faces',f,'Vertices',v,'FaceVertexCData',col,'FaceColor','none');

                    if (mod(tag,1)==0)
                        xc = xVec(1) + 0.5 * ( xVec(end) - xVec(1) );
                        yc = yVec(1) + 0.5 * ( yVec(end) - yVec(1) );
                        zc = zVec(1) + 0.5 * ( zVec(end) - zVec(1) );
                        text(xc,yc,zc,tag_string,'HorizontalAlignment','center')
                    end
            end
        end
        
        function cpObj = copyElement(obj)
            % Make a shallow copy
            cpObj = copyElement@matlab.mixin.Copyable(obj);
            % Copy sub_prop1 in subclass
            % Assignment can introduce side effects
            cpObj.octTree = obj.octTree.copy;
            cpObj.updateNeighborPointers;
        end
    end  
    
    methods (Static)
        function s = init(standardElement)
            settings = System.settings.mesh;
            X = [ settings.x ; settings.y ; settings.z ; settings.t ];
            s = MeshRefineMultiD(standardElement,settings.Ne_global,X,settings.uniform);
        end
       
        % Function to restore a mesh by loading the struct that has been 
        % saved by the save function. Every element of this mesh will have
        % the standardElement to define the locations of variables. 
        function out = load(standardElement,mesh)                 
            t1 = toc;
            
            out = StaticMesh(standardElement,mesh.Ne,mesh.X,mesh.uniform);
                        
            out.maxLevel = mesh.maxLevel;
            
            refinement = MeshRefine3D.getInverseRefinement(out.octTree.Lv);
            
            
            for i=1:length(refinement)
                coarsening = false(numel(refinement{i}),1);
                out.updateAMR(refinement{i},coarsening);
            end
            %fprintf('mesh (1)       : %f\n',toc-t1); t1=toc;
            
            out.update();
            
            %fprintf('mesh (2)       : %f\n',toc-t1); t1=toc;
            
            if (numel(out.octTree.Lv)>1)
                % Determine the old mesh properties
                oldMesh = MeshRefineMultiD(standardElement,mesh.Ne,mesh.X,mesh.uniform);
                refinement = MeshRefineMultiD.getInverseRefinement(out.Lv{2});
                for i=1:length(refinement)
                    coarsening = false(numel(refinement{i}),1);
                    oldMesh.updateAMR(refinement{i},coarsening);
                end
                oldMesh.update();
                
                oldMesh.isChanged = false;
                out.old.Z = oldMesh.Z;
                out.old.eRange = oldMesh.eRange;
                out.old.prodJ = oldMesh.getProdJ;
            end            
        end
    end
    
    methods (Access=protected)
        % Function to return the nodes of an element in a certain direction
        function out = getElementNodes(m,elementNumber,dir)    
            out = zeros(1,2);            
            if ( ~isempty( m.meshNodes{dir} ) )
                try
                    out(1) = m.meshNodes{dir}( elementNumber   );
                    out(2) = m.meshNodes{dir}( elementNumber+1 );
                catch
                   error('Problem with getElementNodes') 
                end
            end
        end 
        
        function loadObject(s)
            NS = solution.data.NS;
            
            s.name = NS.name;        
            s.uConvergence = NS.uConvergence;
            s.vConvergence = NS.vConvergence;
            s.pConvergence = NS.pConvergence;
                        
            mesh = NS.mesh;  
            mesh.isReady = false;
            mesh.update;
                        
            %mesh.setElementLimits;
            %mesh.createZ;
            
            if (nargin>0)
                s.initialize(mesh,NS.Re,NS.We,NS.Fr,NS.g,NS.alpha,NS.lamRho,NS.lamMu,15,15);
            end
            
            s.uField.current.loadAlphaArray(NS.u);
            s.vField.current.loadAlphaArray(NS.v);
            s.pField.current.loadAlphaArray(NS.p);
        end
    end
    
end %classdef




