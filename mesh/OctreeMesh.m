classdef OctreeMesh < StaticMesh
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Access=private)
%         backup; temp;
    end
    
    properties        
        levels;
        data = OctreeElement;
        
        refineFactor;
        
        refinementFlags;
        coarseningFlags;
        
        backup; temp;
        
        Lv;
        
        levelLimit;
    end
 
    methods
        
        % Constructor: make an array based on the OCT object
        function s = OctreeMesh(spatialDim,spaceTime,Ne,X,periodic,uniform,coordSystem)
            nodes = cell(2, 1);
            if (nargin==0)
                % Get values from the settings file
                spatialDim = System.settings.mesh.sdim;
                spaceTime  = (System.settings.time.method==TimeMethod.SpaceTime);
                Ne         = System.settings.mesh.Ne_global;
                
                
                %Added by Julian
                % Everything 'node' related was added by me
                % 18.05.2022
                if isfield(System.settings.mesh, 'x_nodes')
                    x_nodes = System.settings.mesh.x_nodes;
                    x = [min(System.settings.mesh.x_nodes),...
                        max(System.settings.mesh.x_nodes)];
                    nodes{1} = x_nodes;
                else
                    x = System.settings.mesh.x;
                end
                if isfield(System.settings.mesh, 'y_nodes')
                    y_nodes = System.settings.mesh.y_nodes;
                    y = [min(System.settings.mesh.y_nodes),...
                        max(System.settings.mesh.y_nodes)];
                    nodes{2} = y_nodes;
                else
                    y = System.settings.mesh.y;
                end
                
                
                z = System.settings.mesh.z;
                t = System.settings.mesh.t;
                X = [x;y;z;t]';
                
                periodic    = System.settings.mesh.periodic;
                uniform     = System.settings.mesh.uniform; 
                coordSystem = System.settings.mesh.coordSystem; 
            end
            
            s = s@StaticMesh(spatialDim,spaceTime,Ne,X,periodic,uniform,coordSystem, nodes);
            
            if ( isequal(class(s),'OctreeMesh') )
                
                % Set a default mesh name
                s.name              = 'OctreeBase';
                
                % Setup additional parameters
                s.isReady   = false;
                s.isChanged = true;
                
                s.initElements;
                
                s.refineFactor = System.settings.mesh.refinementRatio;
                
                %s.setElementLimits();
            end
            ins = MeshCollection.instance;
            MeshCollection.add(ins.count + 1, s);
        end
        
        function initElements(s)
            % Step 1: initialize the octree (create elements)
            s.initTree();
            
            % Step 2: set the neighbors of all elements
            s.initNeighbors();

            % Step 3: create the dof handler
            s.updateActiveGeometricElements();
            s.initDoFHandler();
            
            % Step 4: set the element limits for parallel computing (sets id & localID)
            s.setElementIDs;
            
            % Step 5: initialize the patches of this mesh
            s.initPatch;
            
            % Step 6: create Z arrays which control the AMR mapping
            s.createZ();
        end
        
        function updateElements(s)
%             t(1) = toc;
            
            % Step 1: update the octree (update elements)
            s.updateTree();
            
%             t(2) = toc;
            
            % Step 2: update the neighbors of all elements (flag is to force an additional check)
            s.updateNeighbors(false);   
            
%             t(3) = toc;
            
            % Step 3: update the dof handler
            s.updateActiveGeometricElements();
            
%             t(4) = toc;
            
            s.dh.update;
            
%             t(5) = toc;
            
            %s.dhNP.update;
            DoFHandlerCollection.update(s);
            
%             t(6) = toc;
            
            % Step 4: set the element limits for parallel computing (sets id & localID)
            s.setElementIDs;
            
%             t(5) = toc;
            
            % Step 5: update the patches of this mesh
            s.updatePatch;
            
%             t(6) = toc;
            
            % Step 6: create Z arrays which control the AMR mapping
            s.createZ();
            
%             t(7) = toc;
%             fprintf('Time required to update the elements : %f %f %f %f %f %f %f\n',t(2:7)-t(1:6),t(7)-t(1));
        end
        
        function initTree(s)
            %s.updateActiveGeometricElements();
            
            s.elements = OctreeElement();            
            NeCell = num2cell(s.Ne);            
            s.data( NeCell{1:s.dim} ) = OctreeElement();
            
            position = cell(1,s.dim);
            
            for e=1:numel(s.gElements)
                [position{:}] = ind2sub(size(s.gElements),e);
                parent = [];
                s.data( e ) = OctreeElement( e, parent, position, s.gElements{e} );
            end
            
            s.elements = s.data(:)';
            
            s.feType = ones(1,s.numElements);
            
            s.Lv = s.getLv;
        end
        
        % After a change in the tree structure, this function updates the
        % elements (which is a flat array)
        function updateTree(s)            
            %s.updateActiveGeometricElements();
            
%             activeMaterial = Material.Fluid;
            
            % First reset the elements array to a single oct, and set the
            % correct size next. Without the first statement the elements 
            % array would not shrink if the total amount of elements decreases.
            s.elements = OctreeElement();                                   % clearing of current elements array
            s.elements(1,s.getNumElements) = OctreeElement();               % pre allocation of the elements array
             
%             t1=toc;
                       
            numElements_l0  = numel(s.data);
            position        = 0;
            
            % Loop over the level0 elements and extract all their children.
            % The result is added to the 'elements' array, which in the end
            % contains a linear structure of all elements in this octree mesh
            s.elements = s.getElementsByMaterial( s.activeMaterial );
            
%             for e=1:numElements_l0
%                 %numChild = s.data(e).getNumChild;
%                 %numChild = numel( arrayfun(@(x) x.isSolid, s.data(e).getElements( direction ) ) );
%                 childElements   = s.data(e).getElements();
%                 childMaterials  = [childElements.material];
%                 
%                 if (s.activeMaterial==Material.All)
%                     selected = true( size(childMaterials) );
%                 else
%                     selected = (childMaterials==s.activeMaterial);
%                 end
%                 
%                 numChild = sum(selected);
%                 s.elements( position+1 : position+numChild ) = childElements( selected );
%                 position = position + numChild;
%             end
            
%             t2 = toc;
%             
%             direction       = [];            
%             numElements_l0  = numel(s.data);
%             position        = 0;
%             
%             for e=1:numElements_l0
%                 %numChild = s.data(e).getNumChild;
%                 %numChild = numel( arrayfun(@(x) x.isSolid, s.data(e).getElements( direction ) ) );
%                 numChild = numel( s.data(e).getElements( direction, s.activeMaterial ) );
%                 s.elements( position+1 : position+numChild ) = s.data(e).getElements( direction, s.activeMaterial );
%                 position = position + numChild;
%             end
%             
%             t3 = toc;
%             
%             fprintf('Timings : %f   %f \n',t2-t1,t3-t2)
            
            s.feType = ones(1,s.getNumElements);
            
            s.Lv = s.getLv;
        end
        
        % Neighbor functions
        function initNeighbors(s)
            
            sub = cell(1,s.dim);
            
            for e=1:numel(s.data)
                
                [sub{:}] = ind2sub( max(s.Ne,1), e );
                
                element = s.data(e);
                
                for dir=1:s.dim
                    
                    % Upper element
                    position = sub;
                    if sub{dir} < s.Ne(dir)
                        position{dir} = position{dir} + 1;
                        upperNeighbor = s.data( position{:} );
                        element.setNeighbor( dir, 1, upperNeighbor );                        
                    elseif ( sub{dir}==s.Ne(dir) && s.periodic(dir) )
                        position{dir} = 1;
                        upperNeighbor = s.data( position{:} );
                        element.setNeighbor( dir, 1, upperNeighbor );
                    end

                    % Lower element
                    position = sub;
                    if sub{dir} > 1
                        position{dir} = position{dir} - 1;                        
                        lowerNeighbor = s.data( position{:} );
                        element.setNeighbor( dir, 0, lowerNeighbor );
                        
                    elseif ( position{dir}==1 && s.periodic(dir) )
                        position{dir} = s.Ne(dir);
                        lowerNeighbor = s.data( position{:} );
                        element.setNeighbor( dir, 0, lowerNeighbor );
                    end
                end         
            end 
        end
        
        function updateNeighbors(s,check)
            if (nargin==1)
                check = false;    
            end
            
            for e=1:s.getNumElements(s.activeMaterial)
                s.elements(e).updateNeighbors();
            end
            
            if (check)
                s.checkNeighbors;
            end
        end
        
        function checkNeighbors(s)
            allNeighborCorrect = true;
            for e=1:s.getNumElements
                allNeighborCorrect = allNeighborCorrect * s.elements(e).checkNeighbors();
            end
            if ~allNeighborCorrect
                error('There is a problem with the neighbors')
            else
                fprintf('MESH       : All neighbors are correct\n')
            end
        end
        
        % Function to check if dofs are periodic if requested, but also
        % check if dofs are non-periodic when requested
        function checkPeriodicity(s)
            Ne = System.settings.mesh.Ne_global;
            
            allOk = true;
            
            if (Ne(3)==1)
                % x-direction
                lowerPatch = s.getPatch(1,0);
                upperPatch = s.getPatch(1,1);

                tx = zeros(lowerPatch.numElements,1);

                for i=1:lowerPatch.numElements
                    lowerElementDof = lowerPatch.elements(i).dof;
                    upperElementDof = upperPatch.elements(i).dof;
                    tx(i) = isequal(upperElementDof(end-1:end,:,:),lowerElementDof(1:2,:,:));
                end
                if ~all(tx) && System.settings.mesh.periodic(1)
                    allOk = false;
                    fprintf('Periodicity error in x-direction (non periodic dof numbering)!\n')
                elseif any(tx) && ~System.settings.mesh.periodic(1)
                    allOk = false;
                    fprintf('Periodicity error in x-direction (periodic dof numbering)!\n')                        
                end

                % y-direction
                lowerPatch = s.getPatch(2,0);
                upperPatch = s.getPatch(2,1);

                ty = zeros(lowerPatch.numElements,1);

                for i=1:lowerPatch.numElements
                    lowerElementDof = lowerPatch.elements(i).dof;
                    upperElementDof = upperPatch.elements(i).dof;
                    ty(i) = isequal(upperElementDof(:,end-1:end,:),lowerElementDof(:,1:2,:));
                end

                if ~all(ty) && System.settings.mesh.periodic(2)
                    allOk = false;
                    fprintf('Periodicity error in y-direction (non periodic dof numbering)!\n')
                elseif any(ty) && ~System.settings.mesh.periodic(2)
                    allOk = false;
                    fprintf('Periodicity error in y-direction (periodic dof numbering)!\n')   
                end

                if (allOk)
                    fprintf('Periodicity ok\n')
                else
                    fprintf('Periodicity error!\n')
                end
                
            else
                error('OctreeMesh :: periodicity check failed because only 1 element in z-direction is implemented')
            end
        end
        
        % Function which creates the Z arrays. The result depends 
        % on the levels of the neighbors of the element, and how the
        % element itself is refined.
        function createZ(s)
            s.Zmap = cell(1,numel(s.elements));
            
            id = 0;
            for element=s.elements
                id = id+1;                
                s.Zmap{id} = element.createZ;
            end
        end
        
        function removeInvalidElements(s)
            valid = isvalid( s.elements(:) );
            s.elements = s.elements( valid );
        end
        
        function out = getDim(s)
            out = ones(1,4);
            out(1:numel(size(s.data))) = size(s.data);
        end
        
        function meshUpdate(s,maxLevel,refineCondition,coarsenCondition,updatePhysics,name)
            
            if (System.settings.outp.debug)
                fprintf('started with meshUpdate on rank %d\n',System.rank)
            end
            
            if maxLevel==0
                s.update();
            end
            
            isConverged = false;            
            maxIter     = 10;
            meshIter    = 0;

            updatePhysics();
            
            s.saveCurrent('save');
            s.isChanged = false;
            
            meshList = cell(maxIter+1,1);
            meshList{1} = s.Lv;

            %if (System.rank==0)
            %    fprintf('Working on AMR for %s\n',name)
            %end
            
            while ~isConverged

                % Check if the solution that was set will update the mesh
                if maxLevel >= 0
                    [isConverged,isUpdated] = s.processAMR(maxLevel,refineCondition,coarsenCondition,meshIter);
                else
                    isConverged = true;
                    isUpdated   = false;
                end
                
                if (System.settings.outp.debug)
                    fprintf('started with updatePhysics on rank %d\n',System.rank)
                end
                
%                 if (isUpdated)
%                     updatePhysics();
%                 end
                
                if (System.settings.outp.debug)
                    fprintf('finished with updatePhysics on rank %d\n',System.rank)
                end
                
                % To ensure this loop does not continue forever, a maximum
                % number of iterations 
                meshIter = meshIter + 1;
                
                % Save the current Lv to allow a check for repeating meshes
                meshList{meshIter+1} = s.Lv;
                
                % If the last meshLv is identical to the meshLv two
                % iterations before, and if the number of elements
                % (numel(meshLv)) is larger or equal to the previous break
                % the loop. In this case a repeated mesh is created.                
                if (meshIter>1 && System.settings.mesh.preventMeshRepetition)
                    if (isequal(meshList{meshIter-1},meshList{meshIter+1}) && numel(meshList{meshIter+1})>=numel(meshList{meshIter}) )
                        fprintf('AMR finished due to a repeated mesh on rank %d\n',System.rank)
                        break;
                    end
                end
                
                % Break off to prevent an infinite loop
                if (meshIter==maxIter)
                    warning('Mesh:meshUpdate','The mesh update process has not converged within %d iterations!\n',maxIter);
                    break;
                end
                
%                 if (System.rank==0)
%                     physics = cell(PhysicsCollection.size,1);
%                     for i=1:numel(physics)
%                         physics{i} = PhysicsCollection.get(i);
%                     end
%                     filename = ['Octree-mesh-update.' int2str(System.rank) '.' int2str(meshIter) '.mat'];
%                     save(filename);
%                 end
            end

            s.saveCurrent('restore');
                
            if (System.settings.anal.debug)
                fprintf('The mesh update is finished for %s\n',s.name)
            end
            
            if (System.settings.outp.debug)
                fprintf('finished with meshUpdate on rank %d\n',System.rank)
            end
        end
        
        function [lastID] = coarsen(s,lastID)
            for l=1:s.et
                for k=1:s.ez
                    for j=1:s.ey
                        for i=1:s.ex
                            s.data(i,j,k).checkCoarsening();
                        end
                    end
                end
            end
        end
        
        % Check which elements can be refinement/coarsened, and also set 
        % if an element wants refinement / coarsening. The canRefine /
        % canCoarsen are more strict than the wantsRefinement /
        % wantsCoarsening flags.
        function [canRefine,canCoarsen] = checkRefinement(s,maxLevel,refineCriteria,coarsenCriteria)
            canRefine  = false(s.numElements,1);
            canCoarsen = false(s.numElements,1);

            refinementLevel = s.maxLevel+1;
            
            for e=s.eRange
                
                element = s.getElement(e);
                
                if (e==s.eRange(1) && isa(refineCriteria(e,refinementLevel),'MeshRefineMultiD') )
                    goalMesh = refineCriteria(e,refinementLevel);                    
                    break;
                end
                
                if ( any(refineCriteria(e,refinementLevel)) && element.lvl<=maxLevel )
                    element.wantsRefinement = true;
                    element.wantsCoarsening = false;
                    if (element.lvl<maxLevel)
                        canRefine(e) = true;
                    end
                else
                    element.wantsRefinement = false;
                    element.wantsCoarsening = true;
                    canRefine(e)  = false;
                    if ( element.lvl>maxLevel || (any(coarsenCriteria(e)) && element.lvl>0) )
                        canCoarsen(e) = true;
                    end
                end                
            end
            
            % If the criteria contains a goalMesh, this goalMesh should be
            % used as a reference to 'detect' refinement/coarsening needs.
            % A first basic check for this is to verify if the Lv arrays
            % (which contain the level of each element are identical. If
            % the Lv's are not equal, the current mesh needs to be adjusted
            % accordingly.
            if (exist('goalMesh','var') && ~isequal(goalmesh.getLv,s.Lv) )              
                canRefine  = false(s.numElements,1);
                canCoarsen = false(s.numElements,1);
                
                % The mesh is checked on 1 processor, and this result is
                % synchronized afterwards between all processors.
                if (System.rank==0)
                    %save(['before.' int2str(System.rank) '.mat']);
                    
                    count = 0;

                    e2 = 1;

                    for e=1:s.numElements
                        
                        %fprintf('element e=%d (level %d) --- e2=%d (level %d)\n',e,s.octTree.getElement(e).lvl,e2,goalmesh.octTree.getElement(e2).lvl)
                        
                        if goalmesh.getElement(e2).lvl == s.getElement(e).lvl
                            canRefine(e)        = false;
                            canCoarsen(e)       = false;
                            e2 = e2+1;
                        elseif goalmesh.getElement(e2).lvl > s.getElement(e).lvl
                            % The goalMesh is refined, while the current mesh
                            % is not refined yet. Increase e2 with 4
                            %fprintf('refine \n');
                            canRefine(e)        = true;

                            activeElement = goalmesh.getElement(e2);
                            while activeElement.lvl ~= s.getElement(e).lvl
                                activeElement = activeElement.parent{1};
                            end

                            e2 = e2+activeElement.getNumChild; %4^(goalmesh.octTree.getElement(e2).lvl-s.octTree.getElement(e).lvl);
                        elseif goalmesh.getElement(e2).lvl < s.getElement(e).lvl
                            % The goalMesh is coarser than the current mesh.
                            % Increase e2 with 4
                            %fprintf('coarsen \n');
                            canCoarsen(e)       = true;
                            count = count+(0.25)^(s.getElement(e).lvl-goalmesh.getElement(e2).lvl);
                            if (abs(count-1)<10*eps)
                                e2 = e2+1;      % move to next goalMesh element
                                count = 0;
                            end
                        end
                        
                        %fprintf('e = %3d and e2 = %3d :: %d -- %d\n',e,e2-1,canRefine(e),canCoarsen(e));
                    end
                    
                    %save(['goalmesh.' int2str(System.rank) '.mat']);
                end
                
                canRefine   = NMPI.AllreduceLogical(canRefine,s.numElements,'|');
                canCoarsen  = NMPI.AllreduceLogical(canCoarsen,s.numElements,'|');
                
                for e=s.eRange
                    element = st.getElement(e);                    
                    element.wantsRefinement = canRefine(e);
                    element.wantsCoarsening = canCoarsen(e);
                end
            else
                % Convert from local to global and sync between processors
                % NB: s.mesh.getElementRank is a logical array where 'true'
                % means the element is on this rank
                canRefine = NMPI.AllreduceLogical(canRefine,s.numElements,'|');
            end
        end
        
        function initAMR(s)
            s.isChanged = false;
            s.saveCurrent;
        end
        
        function setRefinementFlags(s,condition)
            
            s.refinementFlags = false(s.numElements,1);
            
            if isa(condition,'function_handle')
                for e=s.eRange
                    element = s.getElement(e);
                    if ( condition(e,element.lvl) )
                        s.refinementFlags(e) = true;
                    end
                end
            elseif (numel(condition)==s.numElements && islogical(condition))
                s.refinementFlags = condition;
            elseif (numel(condition)==1 && islogical(condition))
                s.refinementFlags = repmat(condition,s.numElements,1);
            else
                error('Unexpected condition in Octreemesh.setRefinementFlags')
            end
                   
            %fprintf(' 1  max(s.refinementFlags) = %d on rank %d\n',max(s.refinementFlags),System.rank);
            
            s.refinementFlags = NMPI.AllreduceLogical(s.refinementFlags,s.numElements,'|','refinementFlags');
            
            %fprintf(' *  max(s.refinementFlags) = %d on rank %d\n',max(s.refinementFlags),System.rank);
            %s.refinementFlags = logical( s.refinementFlags );
            %fprintf(' ** max(s.refinementFlags) = %d on rank %d\n',max(s.refinementFlags),System.rank);
            %if (System.rank==0)
            %    logical(s.refinementFlags)
            %end
        end
        
        function setCoarseningFlags(s,condition)
            
            s.coarseningFlags = false(s.numElements,1);
            
            if isa(condition,'function_handle')
                for e=s.eRange
                    element = s.getElement(e);
                    if ( condition(e,element.lvl) )              
                        s.coarseningFlags(e) = true;
                    end
                    
                    if (~isempty(s.levelLimit))
                        if (element.lvl>s.levelLimit)
                            s.coarseningFlags(e) = true;
                        end
                    end
                end
            elseif (numel(condition)==s.numElements && islogical(condition))
                s.coarseningFlags = condition;
            elseif (islogical(condition) && numel(condition)==1)
                if condition
                    s.coarseningFlags = true(s.numElements,1);
                else
                    s.coarseningFlags = false(s.numElements,1);
                end
            else
                error('Unexpected condition in Octreemesh.setCoarseningFlags')
            end
            
            s.coarseningFlags = NMPI.AllreduceLogical(s.coarseningFlags,s.numElements,'|','coarseningFlags');
            %s.coarseningFlags = logical( s.coarseningFlags );
        end
        
        function out = maxLevel(s)
            out = max(s.Lv);
        end
        
        % This function updates the mesh: refinement where the physics
        % needs it (through checkRefinement). The updateAMR first refines
        % the elements based on the 'needsRefinement' flags, and then checks 
        % iteratively for the 1-level irregularity constraint next.
        % Finally, when convergence of refinement has been reached, the new
        % limits for the parallel processes are set, and the AMR mapping arrays 
        % Z are created.
        %
        % Use 
        %   mesh.processAMR(level,true) refines all elements upto level 1
        % to refine the mesh (not deeper than 'level'
        %
        % Use 
        %   s.mesh.processAMR(level,false,true,0)
        % to coarsen the mesh
        %
        function [isConverged,isUpdated] = processAMR(s,maxLevel,varargin)
            
            if (System.settings.outp.debug)
                fprintf('started with processAMR on rank %d\n',System.rank)
            end
            
            isUpdated   = false;
            
            % only want 2 optional input at most
            numvarargs = length(varargin);
            if numvarargs > 3
                error('LSQDIM:OctreeMesh:refine:TooManyInputs','requires at most 3 optional input');
            end
            
            % set defaults for optional inputs
            [refinementCriteria,coarseningCriteria,meshIter] = VerifyInput(varargin,{false,false,0});
                        
            % First check which elements can be refined/coarsened. During
            % the use of this function, the fields used to verify if 
            % refinement/coarsening is needed will be transformed already to
            % the latest mesh (by comparing it with the mesh.old values). 
%            [canRefine,canCoarsen] = s.checkRefinement(maxLevel,s.refinementCriteria,s.coarseningCriteria);
            
            % Set the refinement & coarsening flags locally
            s.setRefinementFlags( refinementCriteria );
            if (meshIter==0)
                s.setCoarseningFlags( coarseningCriteria );
            else
                s.setCoarseningFlags( false );
            end
            
            % Next update the new mesh.old values. It is required that this
            % happens after 'checkRefinement', as the field transformation
            % will not work otherwise!
            s.saveCurrent();
            
            startLv = s.Lv;
            
            % Ensure elements with the maxLevel are not refined any further:
            s.refinementFlags( startLv>=maxLevel ) = false;
            s.coarseningFlags( startLv==0 ) = false;
            
            %save(['octreemesh.' int2str(System.rank) '.mat']);
            
%             if (nnz(s.refinementFlags)>0 || nnz(s.coarseningFlags)>0)
                  
            if ( System.settings.outp.debug && System.rank==0 )
                fprintf('Elements to refine / coarsen : %d / %d\n',nnz(s.refinementFlags),nnz(s.coarseningFlags));
            end

            numElements0 = s.numElements;   

            %fprintf('process AMR %d : %d elements\n',System.rank,numElements0)

            numElements1 = s.refineCoarsen();

            if ( (~System.settings.anal.debug && NMPI.instance.rank==0) || System.settings.anal.debug )
                if ( numElements0 < numElements1 )
                    fprintf('AMR        : increase of the number of elements from %4d to %4d due to refinement upto level %d (%s)\n',numElements0,numElements1,s.maxLevel,s.name)
                elseif ( numElements0 > numElements1 )
                    fprintf('AMR        : decrease of the number of elements from %4d to %4d due to coarsening (%s)\n',numElements0,numElements1,s.name) 
                end
            end
                
            endLv   = s.getLv;
            s.Lv    = endLv;
            
            % Set the flags to indicate if any changes have occured. The 
            % simple check is if the number of elements changed, but the 
            % true check is if the startLv and endLv match. It might occur
            % that internally the mesh changes, while the number of
            % elements remains the same.
            if (numElements0~=numElements1 || ~isequal(startLv,endLv))
                s.isChanged = true;
                isUpdated   = true;
                s.isReady   = false;
            end
            
            % declare convergence is the element levels array is constant
            isConverged = isequal(startLv,endLv);
            
            if (System.settings.outp.debug)
                fprintf('finished with processAMR on rank %d\n',System.rank)
            end
        end
        
        % This function updates the AMR for the flagged elements, The
        % output is an array with (1) pre-refinement number of elements,
        % (2) the number of elements after the flagged refinement, and (3)
        % the number of elements after the check of the 1-level 
        % irregularity constraint.
        function [out] = refineCoarsen(s)
            info = zeros(2,1);
            
            numElements = s.numElements;                        % fixed during the loop  
            info(1)     = numElements;                          % for output

            targetLevel = max(0, s.Lv + s.refinementFlags - s.coarseningFlags);
            
            if ( numel(targetLevel) ~= numel(s.Lv) )
                error('LSQDIM:mesh:OctreeMesh :: targetLevel has a wrong dimension, it should contain the same number of elements as Lv')
            end
            
            % Set element refineFlag, coarsenFlag and refineFactor
            for e = 1:numElements
                s.elements(e).refineFlag    = s.refinementFlags(e);
                s.elements(e).coarsenFlag   = s.coarseningFlags(e);
                s.elements(e).refineFactor  = s.refineFactor;
                s.elements(e).setTargetLvl(targetLevel(e));
            end
            
            % Assure 1-L irregularity by comparing targetLvl of neighbors.
            % This check needs to be done as long as the 'numFlagged' is
            % still changing. 
            numFlagged = 0;
            maxRegularityLoop = 15;
            
            allElements = s.getElementsByMaterial(Material.All);
            
            for l=1:maxRegularityLoop
                numFlagged_init = numFlagged;
                
                for e = 1:numel(allElements)
                    numFlagged = allElements(e).applyRegularityCondition(numFlagged);
                end
                
                if (numFlagged_init==numFlagged)
                    break;
                elseif (l==maxRegularityLoop)
                    error('Problem with the 1-L irregularity : maxRegularityLoop reached without convergence')
                end
            end
            
            % Step 1: Refine the flagged elements, and flag the elements
            % that can be coarsened. Even in parallel this is a loop over
            % all (global) elements, to ensure matching meshes between
            % ranks
            needsUpdate = 0;
            
            lastID = numElements;
            %allElements = s.elements;
            allElements = s.getElementsByMaterial(Material.All);
            for element = allElements
                if (isvalid(element))
                    [lastID,isChanged] = element.refineCoarsen(lastID);
                    needsUpdate = needsUpdate + isChanged;
                else
                    disp('')
                end
            end
                        
            if ( needsUpdate > 0 )
                s.updateElements;
            end
            
            info(2) = s.numElements();            
            out = info(2);
        end
                
        function out = getCoarsening(s)
            out = [s.elements.wantsCoarsening]';
        end
        
        function setCoarsening(s,canCoarsen)
            for e=1:s.getNumElements
                s.getElement(e).wantsCoarsening = canCoarsen(e);
            end
        end
        
        function setTargetLevel(s,level)
            for e=1:s.getNumElements
                s.getElement(e).setTargetLvl(level);
            end
        end
        
        function setLevelLimit(s,level)
            s.levelLimit = level;
            for e=1:s.getNumElements
                s.getElement(e).levelLimit = level;
            end
        end        
    
        % Get the refinement levels 
        function out = getLv(s,varargin)
            
            [id,material] = VerifyInput(varargin,{[],s.activeMaterial});
            
            if (isempty(id))
                
                elements = s.getElementsByMaterial( material );
                out = elements.getLv;
                
%                 for k=1:dim(3)
%                     for j=1:dim(2)
%                         for i=1:dim(1)
%                             childElements = s.data(i,j,k).getElements();
%                             childMaterial = [ childElements.material ];
%                             if (material==Material.All)
%                                 selected = true( size(childMaterial) );
%                             else
%                                 selected = (childMaterial==material);
%                             end
%                             numChild      = sum( selected );
%                             
%                             %numChild = s.data(i,j,k).getNumChild( s.activeMaterial );
%                             %out(position+1:position+numChild) = s.data(i,j,k).getElementLv( [], s.activeMaterial );
%                             out(position+1:position+numChild) = childElements( selected ).getLv();
%                             position = position + numChild;
%                         end
%                     end                
%                 end
                
            else
                out = s.getSingleLv(id);
            end
        end
        
        function out = getLvByMaterial(s,material)
            out = s.getLv([],material);
        end
        
        function setLevelElements(s)
            s.levelElements = cell(max(s.Lv)+1,1);
            
            s.levelElements{1} = s.data(:);
            
            for l=2:max(s.Lv)+1
                layer = [];
                for e=1:numel(s.levelElements{l-1})
                    if (s.levelElements{l-1}(e).isRefined)
                        layer = [layer ; [s.levelElements{l-1}(e).ch{:}]'];
                    else
                        layer = [layer ; s.levelElements{l-1}(e)];
                    end
                end
                
                s.levelElements{l} = layer;
            end
        end
        
        function out = getSingleLv(s,id)
            lv = s.Lv();
            out = lv(s.getID==id);
            if (isempty(out))
                out = 10;
            end
        end
       
        % Get the IDs
        function out = getID(s)
            out = zeros(s.getNumGlobalElements,1);
            dim = s.getDim();
            
            position = 0;
            
            for k=1:dim(3)
                for j=1:dim(2)
                    for i=1:dim(1)
                        numChild = s.data(i,j,k).getNumChild;
                        out(position+1:position+numChild) = s.data(i,j,k).getID;
                        position = position + numChild;
                    end
                end
            end
        end
        
        % Function to determine the total number of active elements (all
        % children included). The parents of children are not included!
        function numElements = getNumElements(s,material)
            if (nargin==1)
                material = s.activeMaterial;
            end
            
            elements    = s.getElementsByMaterial( material );
            numElements = numel(elements);
            
%             numElements = 0;
%             for e=1:numel(s.data)
%                 childElements   = s.data(e).getElements();
%                 childMaterials  = [childElements.material];
% 
%                 if (material==Material.All)
%                     selected = true( size(childMaterials) );
%                 else
%                     selected = (childMaterials==material);
%                 end
% 
%                 numChild = sum(selected);
%                 numElements = numElements + numChild;
%             end
%             
%             if (numElements==numElements1)
%                 disp('identical')
%             else
%                disp('different') 
%             end
        end
        
        function numChild = getNumChild(s)
            dim = s.getDim();
            numChild = zeros(dim);
            for k=1:dim(3)
                for j=1:dim(2)
                    for i=1:dim(1)
                        numChild(i,j,k) = s.data(i,j,k).getNumChild;
                    end
                end
            end
        end
        
        % Return the neighbors 
        function out = getNeighbors(s)
            out = zeros(s.getNumElements,6);
            dim = s.getDim();
            
            position = 0;
            
            for k=1:dim(3)
                for j=1:dim(2)
                    for i=1:dim(1)
                        numChild = s.data(i,j,k).getNumChild;
                        out(position+1:position+numChild,:) = s.data(i,j,k).getNeighbors;
                        position = position + numChild;
                    end
                end
            end
        end
        

        function out = getElement(s,n)
            if nargin==2
                try
                    out = s.elements(n);
                catch
                    fprintf('There are %d elements and you are requesting element %d\n',numel(s.elements),n)
                end
            else
                out = s.elements;
            end
        end
        
        % The value 'n' is a local element number, and it is mapped to the
        % global element number inside this function
        function out = getLocalElement(s,n)
            out = s.elements(n-1 + s.eStart);
        end
        
        function out = getElements(s)
            out = s.elements;
        end
        
        function elements = getElementsByMaterial(s,material)
            if (nargin~=2)
                fprintf('Please provide a material to create the list, now the default (activeMaterial) is used.\n')
                material = s.activeMaterial;
            end
             
            position    = 0;
            
            for e=1:numel(s.data)
                childElements = s.data(e).getElements();
                childMaterial = [ childElements.material ];
                
                if (material==Material.All)
                    selected = true( size(childMaterial) );
                else
                    selected = (childMaterial==material);
                end
                
                numChild = sum( selected );
                elements( position+1 : position+numChild ) = childElements( selected );
                position = position + numChild;
            end            
        end
        
        function out = getElementTree(s)
            out = s.data;
        end
        
        function out = getElementByPosition(s,position)
            out = s.data(position{:});
        end
        
        function out = getRank(s)
            out = zeros(s.getNumGlobalElements,1) ;
            dim = s.getDim();
            
            position = 0;
            
            for k=1:dim(3)
                for j=1:dim(2)
                    for i=1:dim(1)
                        numChild = s.data(i,j,k).getNumChild;
                        out(position+1:position+numChild) = s.data(i,j,k).getRank;
                        position = position + numChild;
                    end
                end
            end
        end
        
        % Function to create pillars at the bottom (for now) of a mesh
        % The following parameters can be passed:
        %   refinementLevel : how many refinement levels must the bottom
        %                     element get (default: 2)
        %   pillarCount     : how many pillar within a single l0 element 
        %                     (default: 1, centered position)
        %   pillarHeight    : how many refined elements should be used to
        %                     give the pillar its height? (default: 3)
        %   wallType        : 0 = use [0110] solid pattern with pillarCount & pillarHeight
        %                     1 = use [0111] solid pattern with pillarCount & pillarHeight
        %                     2 = use forward facing step (change of pillarHeight) for positive x values
        %                     3 = use backward facing step (change of pillarHeight) for negative x values
        %   
        % With the default settings, a single element will look like:
        %
        %  | | | | |    where x indicates the wall/solid
        %  | |x|x| |         
        %  | |x|x| |
        %  | |x|x| |
        %  xxxxxxxxx
        %
        function createPillars(s,varargin)
            
            % Default values
            id              = 5;
            tag             = 'pillars';
            location        = 'y0';
            refinementLevel = 2;
            pillarCount     = 1;    % every nth element
            pillarHeight    = 3;
            wallType        = 0;    % use default solidPattern (single pillar)
            
            [id,tag,location,refinementLevel,pillarCount,pillarHeight,wallType] = VerifyInput(varargin,{id,tag,location,refinementLevel,pillarCount,pillarHeight,wallType});
            
            % Check that the pillarHeight <= 2^refinementLevel; as a pillar
            % must be contained within a single L0 element
            if ( pillarHeight > 2^refinementLevel )
                error('The pillarHeight is too large for this refinementLevel!\n')
            end
            
            %disp('Creating pillars');
            
            % Select the elements to modify with pillars
            if (strcmp(location,'y0'))
                selectedElements = s.data(:,1);     % s.data(:,1) contains the bottom elements of an unrefined mesh (L0)
            elseif (strcmp(location,'y1'))
                selectedElements = s.data(:,end);   % s.data(:,end) contains the top elements of an unrefined mesh (L0)
            elseif (strcmp(location,'x0')) 
                selectedElements = s.data(1,:);     % s.data(1,:) contains the left elements of an unrefined mesh (L0)
            elseif (strcmp(location,'x1')) 
                selectedElements = s.data(end,:); 	% s.data(end,:) contains the right elements of an unrefined mesh (L0)
            else
                error('This pillar location is not supported\n')
            end
            
            % Set bottom edge as solid
            %for e=1:numel(selectedElements)
            %    selectedElements(e).geometric.edges(1).isSolid = true;
            %end

            
            % Determine the pattern for the solid elements
            if (refinementLevel == 1)
                solidPattern_x = [ 1 0 ];                                   % centered (there are 4 children for L2 refinement)
                solidPattern_y = zeros(1,numel(solidPattern_x));         	% initialization of the 4 children in y-direction
            elseif (refinementLevel == 2)
                solidPattern_x = [ 0 1 1 0 ];                               % centered (there are 4 children for L2 refinement)
                solidPattern_y = zeros(1,4);                                % initialization of the 4 children in y-direction
            end
            solidPattern_y(1:pillarHeight) = 1;                             % set the pillarHeight
            
            % Depending on the location of the pillars the flags need to be
            % flipped to ensure they are actually created on the walls and
            % not as floating blocks
            switch location(1)
                case 'x'
                    if (location(end)=='1') 
                        solidPattern_x = fliplr(solidPattern_x);
                        neighborMaterials = [Material.Fluid, Material.Solid, Material.Fluid, Material.Fluid];   % left/right, bottom/top
                    else
                        neighborMaterials = [Material.Solid, Material.Fluid, Material.Fluid, Material.Fluid];   % left/right, bottom/top
                    end
                case 'y'
                    if (location(end)=='1') 
                        solidPattern_y = fliplr(solidPattern_y);
                        neighborMaterials = [Material.Fluid, Material.Fluid, Material.Fluid, Material.Solid];   % left/right, bottom/top
                    else
                        neighborMaterials = [Material.Fluid, Material.Fluid, Material.Solid, Material.Fluid];   % left/right, bottom/top
                    end
%                 case 'z'
%                     if (location(end)=='1')
%                         solidPattern_z = fliplr(solidPattern_z);
%                     end
            end
            
            for e0 = 1:numel(selectedElements)
                element_L0 = selectedElements(e0);
                element_L0.setTag( tag, neighborMaterials );                % Tag geometric fluid elements that have no bottom neighbor
            end

            % Set the targetLvl for each element & refine
            s.Lv([selectedElements.id]) = refinementLevel;
            s.processAMR(refinementLevel,false,false);
            
            % Check to see if the pattern is correct use: full(reshape(solidPattern,[4,4])). 
            % N.B.: rows mean x-direction and columns y-direction (cartesian order), 
            % it needs to be transformed to obtain the correct AMR ordering.
            %
            % Compare the following ordering schemes:
            %
            % Cartesian order:          AMR order:
            % 13 14 15 16               11 12 15 16
            %  9 10 11 12                9 10 13 14
            %  5  6  7  8                3  4  7  8
            %  1  2  3  4                1  2  5  6 
            %
            if (refinementLevel == 1)
                cart2amr = [1 2 3 4];
            elseif (refinementLevel == 2)
                cart2amr = [1 2 5 6 3 4 7 8 9 10 13 14 11 12 15 16];
            end
            
            % default solidPattern
            solidPattern     = superkron(solidPattern_y,solidPattern_x);
            solidPattern     = solidPattern(cart2amr);
            
            % special solidPatterns (only works with L2 refinement!)
            if (wallType==1)
                solidPattern_x = [ 0 1 1 1 ];                                   % centered (there are 4 children for L2 refinement)
                solidPatternHole = superkron(solidPattern_y,solidPattern_x);
                solidPatternHole = solidPatternHole(cart2amr);
            elseif (wallType==2 || wallType==3)
                % Pillars with H=0 (no pillars inside this element)
                solidPattern_x = [ 0 0 0 0 ];                                   % centered (there are 4 children for L2 refinement)
                solidPatternH0 = superkron(solidPattern_y,solidPattern_x);
                solidPatternH0 = solidPatternH0(cart2amr);
                % Pillars with H=4 (the whole element is solid)
                solidPattern_x = [ 1 1 1 1 ];                                   % centered (there are 4 children for L2 refinement)
                solidPatternH4 = superkron(solidPattern_y,solidPattern_x);
                solidPatternH4 = solidPatternH4(cart2amr);
            end
            
            % Set the targetLvl for each element
            %arrayfun(@(x) x.setTargetLvl(refinementLevel),selectedElements);
            
            % Convert pillar element into solids
            numSolidElements = 0;
            for e0 = 1:numel(selectedElements)
                
                if (wallType==1)
                  % make a single hole
                  if (e0==1)
                    selectedSolidPattern = solidPatternHole;
                  else
                    selectedSolidPattern = solidPattern;
                  end
                elseif (wallType==2)
                  % make an edge
                  x = selectedElements(e0).getNodes;
                  if ( all(x(:,1)>=0) )
                    selectedSolidPattern = solidPatternH0;
                  else
                    selectedSolidPattern = solidPatternH4;
                  end
                elseif (wallType==3)
                  % make an edge
                  x = selectedElements(e0).getNodes;
                  if ( all(x(:,1)<=0) )
                    selectedSolidPattern = solidPatternH4;
                  else
                    selectedSolidPattern = solidPatternH0;
                  end
                else
                  selectedSolidPattern = solidPattern;
                end
                
                element_L0 = selectedElements(e0);
                
                allChildren = [element_L0.getChildren];
                
                for c=1:numel(allChildren)
                    subElement = allChildren(c);
                    if logical(selectedSolidPattern(c))
                        if ( mod(e0+1,pillarCount)==0 )                       % Only create a pillar every nth element
                            subElement.setMaterial( Material.Solid );
                            numSolidElements = numSolidElements + 1;
                        end
                    end
                    
                    subElement.setTag( tag, neighborMaterials );            % Tag geometric fluid elements that have no bottom neighbor
                end
                
%                 numSolidElements = numSolidElements + sum(solidPattern);
            end
            
            % Deactivate all geometric elements
            cellfun( @(x) x.resetActive, s.gElements(s.dim));
            
            % Update elements
            s.updateElements();
            
            % Create a patch of the elements that surround the pillars,
            % which will replace the default (flat) bottom patch (y=0)
            %s.createPillarPatch(Dir.y,Position.low);
            patch = Patch(s,id,tag);
            PatchCollection.addreplace(patch);
            
            if (System.rank==0)
                fprintf('MESH       : Pillars have been created, %d elements converted into solids\n',numSolidElements);
            end
        end
        
        function out = getMaterial(s)
            out = [];
            for e=1:numel(s.data)
                out = [ out, s.data(e).getMaterial ];
            end
        end
        
        function setMaterial(s,material)
            allElements = s.getElements;
            for e=1:numel(allElements)
                s.getElement(e).setMaterial(material(e));
            end
        end
        
        % Function to create a struct of the mesh, which can be saved in a
        % restart/solution file. The mesh can be restored by using the static 
        % load function (in combination with a standardElement).
        function mesh = save(s)
            mesh = struct;
            
            mesh.name           = s.name;
            mesh.sDim           = s.sDim;
            mesh.spaceTime      = s.spaceTime;
            mesh.coordSystem    = s.coordSystem;
            mesh.X              = s.X;
            mesh.Ne             = s.Ne;
            mesh.uniform        = s.uniform; 
            mesh.periodic       = s.periodic;
            mesh.maxLevel       = s.maxLevel;            
            mesh.Lv             = s.getLvByMaterial(Material.All);
            mesh.material       = s.getMaterial;
            mesh.class          = class(s);
            
            % The following is used for backup of meshes (see elsewhere in
            % this class). It's better to use the 'save' function there too
%             m.backup.Lv          = m.Lv;
%             m.backup.Z           = m.Z;
%             m.backup.numElements = m.numElements;
%             m.backup.eRange      = m.eRange;
%             m.backup.prodJ       = m.getProdJ;
%             m.backup.sDim        = m.sDim;
%             m.backup.spaceTime   = m.spaceTime;
%             m.backup.X           = m.X;
%             m.backup.periodic    = m.periodic;
%             m.backup.uniform     = m.uniform;
%             m.backup.Ne          = m.Ne;
%             m.backup.maxLevel    = m.maxLevel;
%             m.backup.getMaterial = m.getMaterial;
%             s.backup.coordSystem = m.coordSystem;
%             m.backup.getLvAll    = m.getLvByMaterial( Material.All );
                
        end
        
        % This recursive function creates an AMR tree structure (cell array)
        % of the passed 'array' (flat). The full Lv array (for Material.All) 
        % is used to determine the tree structure. 
        % Last modified by MMKK on 31/10/2019
        function out = createRefinementTree(s,array,numBaseElements,Lv,currentLv)
            
            % Use the Material.All Lv array if none is given
            if (nargin==1)
                array = s.getLvByMaterial( Material.All );
            end
            
            % Initialization settings, so the function can be called by
            % just a single argument 'array'.
            if (nargin<3)
                numBaseElements = numel(s.data);                            % Extract the number of baseElements from s.data
                Lv              = s.getLvByMaterial( Material.All );        % Get the current Lv array for all materials
                currentLv       = 0;                                        % Start from level 0
            else
                if (isempty(numBaseElements))
                    numBaseElements = numel(s.data);
                end
                if (nargin<5 || isempty(currentLv) )
                    currentLv = 0;
                end
            end
            
            out = cell(numBaseElements,1);
            e = 1;

            % Determine the volume of the current children
            numChild        = prod(s.refineFactor);
            volumeChild     = 1/numChild;
            elementVolume   = volumeChild.^(Lv-currentLv);

            % Loop over the baseElements until the summed volume is equal
            % to 1. That indicates that the range (eStart:e-1) represents
            % a full L0 element.
            for n=1:numBaseElements
                eStart = e;
                volume = 0;

                while volume < 1
                   volume = volume + elementVolume(e);
                   e = e+1;
                end
                
                if max(Lv(eStart:e-1))>currentLv
                    % move one level deeper in the octree structure, this
                    % will create a nested cell array
                    out{n} = s.createRefinementTree( array(eStart:e-1), numChild, Lv(eStart:e-1), currentLv+1 );
                else
                    % return the 
                    out{n} = array(eStart:e-1);
                end
            end
        end
        
        % This recursive function removes a AMR tree structure (cell array)
        % of the passed 'array'. The full Lv array (for Material.All) is 
        % used to determine the tree structure. 
        % Last modified by MMKK on 31/10/2019
        function out = flattenTree(s,array,flags)
            
            out = cell( numel(flags), 1 );
            
            % Count the number of cells upto the first child
            numCells = 0;
            for e=1:numel(array)
                
                temp = array{e};
                
                while iscell(temp)
                
                    if (iscell(temp))
                        numCells = numCells + numel(temp);

                    else
                        numCells = numCells + 1;
                    end
                end
            end
                      
            out = cell(numCells,1);
            
            % Extract the child cell arrays 
            position = 0;
            for e=1:numel(array)
                if (iscell(array{e}))
                    numChildren = numel(array{e});
                    out(position+1:position+numChildren) = array{e};
                    position = position + numChildren;
                else
                    out{position+1} = array{e};
                    position = position + 1;
                end
            end
            
        end
    end
    
    methods (Access = protected)
        
        function createPillarPatch(s,dir,pos)
            id          = 99;
            patchName   = 'pillarBoundary';
            condition   = 'y=0';
            connected   = true;
            s.patchList{ pos,dir } = Patch(s,id,patchName,condition,connected);
        end
        
        function createPatchByElements(s,id,patchName,patchElements)
            s.patchList{id} = Patch(s,id,patchName,patchElements);
        end
        
        function createPatchByTag(s,id,tag)
            s.allPatchList{id} = Patch(s,id,tag);
        end
        
        function saveCurrent(m,action)
            
            if (nargin==1)
                % Intermediate old values
                m.backup.Lv          = m.Lv;
                m.backup.Z           = m.Z;
                m.backup.numElements = m.numElements;
                m.backup.eRange      = m.eRange;
                m.backup.prodJ       = m.getProdJ;
                m.backup.sDim        = m.sDim;
                m.backup.spaceTime   = m.spaceTime;
                m.backup.X           = m.X;
                m.backup.periodic    = m.periodic;
                m.backup.uniform     = m.uniform;
                m.backup.Ne          = m.Ne;
                m.backup.maxLevel    = m.maxLevel;
                m.backup.material    = m.getMaterial;
                m.backup.coordSystem = m.coordSystem;
                m.backup.LvAll       = m.getLvByMaterial( Material.All );
                m.backup.ranks       = m.getElementRanks;
                
                %if (System.rank==0)
                   %fprintf('current octree mesh saved (1) on rank %d\n',System.rank)
                   %fprintf('(1) %d element in eRange on rank %d\n',numel(m.backup.eRange),System.rank)
                %end
            
            else
                if (strcmp(action,'save'))
                    % Initial old values
                    % Save the old values
                    m.backup.Lv          = m.Lv;
                    m.backup.Z           = m.Z;
                    m.backup.numElements = m.numElements;
                    m.backup.eRange      = m.eRange;
                    m.backup.prodJ       = m.getProdJ;                    
                    m.backup.sDim        = m.sDim;
                    m.backup.spaceTime   = m.spaceTime;
                    m.backup.X           = m.X;
                    m.backup.periodic    = m.periodic;
                    m.backup.uniform     = m.uniform;
                    m.backup.Ne          = m.Ne;
                    m.backup.maxLevel    = m.maxLevel;
                    m.backup.material    = m.getMaterial;
                    m.backup.coordSystem = m.coordSystem;
                    m.backup.LvAll       = m.getLvByMaterial( Material.All );
                    m.backup.ranks       = m.getElementRanks;

                    m.temp.Lv            = m.backup.Lv;
                    m.temp.Z             = m.backup.Z;
                    m.temp.numElements   = m.backup.numElements;
                    m.temp.eRange        = m.backup.eRange;
                    m.temp.prodJ         = m.backup.prodJ;
                    m.temp.sDim          = m.backup.sDim;
                    m.temp.spaceTime     = m.backup.spaceTime;
                    m.temp.X             = m.backup.X;
                    m.temp.periodic      = m.backup.periodic;
                    m.temp.uniform       = m.backup.uniform;
                    m.temp.Ne            = m.backup.Ne;
                    m.temp.maxLevel      = m.backup.maxLevel;
                    m.temp.material      = m.backup.material;
                    m.temp.coordSystem   = m.backup.coordSystem;
                    m.temp.LvAll         = m.backup.LvAll;
                    m.temp.ranks         = m.backup.ranks;
                    
                    %if (System.rank==0)
                    %    fprintf('current octree mesh saved (2)\n')
                    %end
                    %fprintf('current octree mesh saved (2) on rank %d\n',System.rank)
                    %fprintf('(2) %d element in eRange on rank %d\n',numel(m.backup.eRange),System.rank)

                elseif (strcmp(action,'restore'))
                    m.backup.Lv          = m.temp.Lv;
                    m.backup.Z           = m.temp.Z;
                    m.backup.numElements = m.temp.numElements;
                    m.backup.eRange      = m.temp.eRange;
                    m.backup.prodJ       = m.temp.prodJ;
                    m.backup.sDim        = m.temp.sDim;
                    m.backup.spaceTime   = m.temp.spaceTime;
                    m.backup.X           = m.temp.X;
                    m.backup.periodic    = m.temp.periodic;
                    m.backup.uniform     = m.temp.uniform;
                    m.backup.Ne          = m.temp.Ne;
                    m.backup.maxLevel    = m.temp.maxLevel;
                    m.backup.material    = m.temp.material;
                    m.backup.coordSystem = m.temp.coordSystem;
                    m.backup.LvAll       = m.temp.LvAll;
                    m.backup.ranks       = m.temp.ranks;
                    
                    m.temp            = [];
                    
                    %if (System.rank==0)
                    %    fprintf('current octree mesh restored (2)\n')
                    %end
                    %fprintf('current octree mesh restored (2) on rank %d\n',System.rank)
                    %fprintf('(3) %d element in eRange on rank %d\n',numel(m.backup.eRange),System.rank)
                end
            end
        end
        
        function cpObj = copyElement(obj)
            % Make a shallow copy
            cpObj = copyElement@matlab.mixin.Copyable(obj);
            % Copy sub_prop1 in subclass
            % Assignment can introduce side effects
            cpObj.data = copy(obj.data);
            cpObj.updateElements();
        end
    end
    
    methods (Static)
        
        % Function to restore a mesh by loading the struct that has been 
        % saved by the save function. Every element of this mesh will have
        % the standardElement to define the locations of variables. 
        function out = load(data)
            
            sDim                = data.sDim;
            spaceTime           = data.spaceTime;
            Ne                  = data.Ne;
            x = System.settings.mesh.x;
            y = System.settings.mesh.y;
            z = System.settings.mesh.z;
            t = System.settings.mesh.t;
            X = [x;y;z;t]';
            %X                   = data.X;
            periodic            = data.periodic;
            uniform             = data.uniform;
            
            %activeSettings = System.settings;
            %coordinateSystem    = CoordSystem.Cartesian; %activeSettings.mesh.coordSystem;
            
            coordSystem         = data.coordSystem;
            
            out = OctreeMesh(sDim,spaceTime,Ne,X,periodic,uniform,coordSystem);
            
            refinement = OctreeMesh.getInverseRefinement(data.Lv);
            
            for i=1:numel(refinement)
                coarsening = false(numel(refinement{i}),1);
                out.processAMR(data.maxLevel,refinement{i},coarsening);
            end
            
            %out.update();
            
%             if (numel(out.octTree.Lv)>1)
%                 % Determine the old mesh properties
%                 oldMesh = MeshRefineMultiD(standardElement,mesh.Ne,mesh.X,mesh.uniform);
%                 refinement = MeshRefineMultiD.getInverseRefinement(out.Lv{2});
%                 for i=1:length(refinement)
%                     coarsening = false(numel(refinement{i}),1);
%                     oldmesh.updateAMR(refinement{i},coarsening);
%                 end
%                 oldmesh.update();
%                 
%                 oldmesh.isChanged = false;
%                 out.old.Z = oldmesh.Z;
%                 out.old.eRange = oldmesh.eRange;
%                 out.old.prodJ = oldmesh.getProdJ;
%             end    
            
            out.setMaterial( data.material );

        end

        % Function to get the refinement steps required to reach a passed
        % Lv array. The steps are returned as a cell array where each cell
        % contains the flags for the elements that need to be refined.
        function out = getInverseRefinement(Lv)
            numLeafElements = prod(System.settings.mesh.refinementRatio);      % each element is divided in 4 leaf elements
            
            numSteps = max(Lv);
            out      = cell(numSteps,1);
            
            for l=numSteps:-1:1
                numLevelElements = nnz(Lv==l);%/numLeafElements;
                numElements   	 = numel(Lv)-numLevelElements+numLevelElements/numLeafElements;
                out{l}           = false(numElements,1);
                
                pos = 1;
                Lvnew = zeros(numElements,1);
                for e=1:numElements
                    if (Lv(pos)==l)
                        out{l}(e) = true;
                        Lvnew(e) = Lv(pos)-1;
                        pos = pos+numLeafElements;      % skip the next numLeafElements since they're all equal to l
                    else
                        out{l}(e) = false;
                        Lvnew(e) = Lv(pos);
                        pos = pos+1;
                    end
                end
                Lv = Lvnew;
            end
        end
    end
    
end
