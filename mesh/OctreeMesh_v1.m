classdef OctreeMesh < StaticMesh
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Access=private)
        
    end
    
    properties
        elements = oct()
        
        levelElements;
        
        eStart, eEnd;
        dofv; dofvq;
        data = oct;
    end
 
    methods
        
        % Constructor: make an array based on the OCT object
        function s = OctreeMesh(standardElement,Ne,X,periodic,uniform)
            
            s = s@(standardElement,Ne,X,periodic,uniform);

%             s.Ne        = mesh.Ne;
%             s.X         = mesh.X;
%             s.periodic  = mesh.periodic;
%             s.uniform   = mesh.uniform;
%             s.dim       = mesh.dim;
%             s.disc      = mesh.disc;
            
            s.elements = oct();
            
            NeCell = num2cell(s.Ne);            
            s.data( NeCell{:} ) = oct();
            
            % Step 1: initialize
            s.init();
            
            % Step 2: assign neighbors
            s.updateNeighborPointers();
            
            % Step 3: update element list
            s.updateElements();
        end
        
        function init(s)
            parent  = 0;
            level   = 0;
            
            elementNodes = zeros(4,2);          % contains the start/end node for an element in each direction
            
            % Step 1: create octs
            for et=1:s.Ne(4)
                
                if (s.dim>3)
                    elementNodes(4,:) = s.getElementNodes(et,4);
                end
                
                for ez=1:s.Ne(3)
                    
                    if (s.dim>2)
                        elementNodes(3,:) = s.getElementNodes(ez,3);
                    end
                    
                    for ey=1:s.Ne(2)
                        
                        if (s.dim>1)
                            elementNodes(2,:) = s.getElementNodes(ey,2);
                        end
                        
                        for ex=1:s.Ne(1)
                            
                            elementNodes(1,:) = s.getElementNodes(ex,1);
                            
                            % The element id is increasing in the usual way
                            id = (et-1)*s.Ne(1)*s.Ne(2)*s.Ne(3) + (ez-1)*s.Ne(1)*s.Ne(2) + (ey-1)*s.Ne(1) + ex;

                            position = {ex,ey,ez,et};
                            
                            s.data(ex,ey,ez,et).init_multi(id,parent,level,elementNodes,position,s.disc);
                        end
                    end
                end
            end
        end
        
        function updateNeighborPointers(s)            
            for et=1:s.Ne(4)
                for ez=1:s.Ne(3)
                    for ey=1:s.Ne(2)
                        for ex=1:s.Ne(1)
                            
                            position        = {ex,ey,ez,et};                            
                            activeElement   = s.data( position{:} );
                            
                            for dir=1:s.dim
                                
                                % Upper element
                                if position{dir} < s.Ne(dir)
                                    position1 = position; position1{dir} = position1{dir} + 1;
                                    upperElement = s.data( position1{:} );
                                    activeElement.setNeighbor2( dir, 1, upperElement );                                    
                                elseif ( position{dir}==s.Ne(dir) && s.periodic(dir) )
                                    position1 = position; position1{dir} = 1;
                                    upperElement = s.data( position1{:} );
                                    activeElement.setNeighbor2( dir, 1, upperElement );
                                end
                                
                                % Lower element
                                if position{dir} > 1
                                    position0 = position; position0{dir} = position0{dir} - 1;
                                    lowerElement = s.data( position0{:} );
                                    activeElement.setNeighbor2( dir, 0, lowerElement );
                                elseif ( position{dir}==1 && s.periodic(dir) )
                                    position0 = position; position0{dir} = s.Ne(dir);
                                    lowerElement = s.data( position0{:} );
                                    activeElement.setNeighbor2( dir, 0, lowerElement );
                                end
                            end
                        end
                    end
                end
            end
        end
        
        % After a change in the tree structure, this function updates the
        % elements (which is a flat array)
        function s = updateElements(s)
            % First reset the elements array to a single oct, and set the
            % correct size next. Without the first statement the elements 
            % array would not shrink if the total amount of elements decreases.
            s.elements = oct();                                 % clearing of current elements array
            s.elements(s.getNumElements,1) = oct();             % pre allocation of the elements array
            
            dim = s.getDim();                                   % dimensions of the 'data' array
            pos = 0;                                            % position
            
            % Loop over the directions of the octTree, where getElements
            % returns all the subElements (along the branches up to the 
            % leafs) from a node (which is in the data(i,j,k) array).
            
            iRange = 1:dim(1);
            jRange = 1:dim(2);
            kRange = 1:dim(3);
            
%             direction = [true,true,true];
            direction = [];
            
            for k=kRange
                for j=jRange                   
                    for i=iRange
                        numChild = s.data(i,j,k).getNumChild;
                        s.elements(pos+1:pos+numChild) = s.data(i,j,k).getElements( direction );
                        pos = pos + numChild;
                    end
                    %iRange = fliplr(iRange);
                    %direction(1) = ~direction(1);
                end
                %jRange = fliplr(jRange);
                %direction(2) = ~direction(2);
            end
        end
        
        
        function out = getDim(s)
            out = ones(1,4);
            out(1:numel(size(s.data))) = size(s.data);
            
%             out = [s.ex,s.ey,s.ez,s.et];
        end
        
        function [lastID,finishedRefinement] = refine(s,lastID)
            finishedRefinement = false;
            
            temp = lastID;
            
            for e=1:s.getNumElements
                if ( s.getElement(e).wantsRefinement )
                    [lastID] = s.getElement(e).refine(lastID);
                end
            end
            
            if (lastID-temp==0)
                finishedRefinement = true;
            end
        end
        
        function [lastID] = coarsen(s,lastID)
            for l=1:s.et
                for k=1:s.ez
                    for j=1:s.ey
                        for i=1:s.ex
                            s.data(i,j,k).checkCoarsening();
                        end
                    end
                end
            end
        end
        
        function numRemoved = removeFlagged(s)
            numRemoved = 0;
            for l=1:s.et
                for k=1:s.ez
                    for j=1:s.ey
                        for i=1:s.ex
                            [~,numRemoved] = s.data(i,j,k,l).checkRemoval(numRemoved);                        
                        end
                    end
                end
            end
        end
        
        function out = getCoarsening(s)
            out = [s.elements.wantsCoarsening]';
        end
        
        function setCoarsening(s,canCoarsen)
            for e=1:s.getNumElements
                s.getElement(e).wantsCoarsening = canCoarsen(e);
            end
        end
    
        % Get the refinement levels 
        function out = getLv(s,id)
            out = zeros(s.getNumElements,1);
            dim = s.getDim();
            
            position = 0;
            
            if (nargin==1)            
                for k=1:dim(3)
                    for j=1:dim(2)
                        for i=1:dim(1)
                            numChild = s.data(i,j,k).getNumChild;
                            out(position+1:position+numChild) = s.data(i,j,k).getElementLv;
                            position = position + numChild;
                        end
                    end                
                end
            elseif (nargin==2)
                out = s.getSingleLv(id);
            end
        end
        
        function setLevelElements(s)
            s.levelElements = cell(max(s.getLv)+1,1);
            
            s.levelElements{1} = s.data(:);
            
            for l=2:max(s.getLv)+1
                layer = [];
                for e=1:numel(s.levelElements{l-1})
                    if (s.levelElements{l-1}(e).isRefined)
                        layer = [layer ; [s.levelElements{l-1}(e).ch{:}]'];
                    else
                        layer = [layer ; s.levelElements{l-1}(e)];
                    end
                end
                
                s.levelElements{l} = layer;
            end
        end
        
        function out = getSingleLv(s,id)
            lv = s.getLv();
            out = lv(s.getID==id);
            if (isempty(out))
                out = 10;
            end
        end
       
        % Get the IDs
        function out = getID(s)
            out = zeros(s.getNumGlobalElements,1);
            dim = s.getDim();
            
            position = 0;
            
            for k=1:dim(3)
                for j=1:dim(2)
                    for i=1:dim(1)
                        numChild = s.data(i,j,k).getNumChild;
                        out(position+1:position+numChild) = s.data(i,j,k).getID;
                        position = position + numChild;
                    end
                end
            end
        end
        
        % Function to determine the total number of active elements (all
        % children included). The parents of children are not included!
        function numElements = getNumElements(s)
            numElements = 0;
            dim = s.getDim();
            for k=1:dim(3)
                for j=1:dim(2)
                    for i=1:dim(1)
                        numElements = numElements + s.data(i,j,k).getNumChild;
                    end
                end
            end
        end
        
        function numChild = getNumChild(s)
            dim = s.getDim();
            numChild = zeros(dim);
            for k=1:dim(3)
                for j=1:dim(2)
                    for i=1:dim(1)
                        numChild(i,j,k) = s.data(i,j,k).getNumChild;
                    end
                end
            end
        end
        
        % Return the neighbors 
        function out = getNeighbors(s)
            out = zeros(s.getNumElements,6);
            dim = s.getDim();
            
            position = 0;
            
            for k=1:dim(3)
                for j=1:dim(2)
                    for i=1:dim(1)
                        numChild = s.data(i,j,k).getNumChild;
                        out(position+1:position+numChild,:) = s.data(i,j,k).getNeighbors;
                        position = position + numChild;
                    end
                end
            end
        end
        

        function out = getElement(s,n)
            if nargin==2
                out = s.elements(n);
            else
                out = s.elements;
            end
        end
        
        % The value 'n' is a local element number, and it is mapped to the
        % global element number inside this function
        function out = getLocalElement(s,n)
            out = s.elements(n-1 + s.eStart);
        end
        
        function out = getElements(s)
            out = s.elements;
        end
        
        function out = getElementTree(s)
            out = s.data;
        end
        
        function out = getElementByPosition(s,position)
            out = s.data(position{:});
        end
        
        function updateNeighbors(s)
            for e=1:s.getNumElements
                s.elements(e).updateNeighbors();
            end
            %s.checkNeighbors;
        end
        
        function out = checkNeighbors(s)
            out = true;
            for e=1:s.getNumElements
                out = out * s.elements(e).checkNeighbors();
            end
        end
        
        % Initialize the GM. First a present GM is removed (reset), next
        % the GM is created from the coarsed level upto the finest level.
        function lastDOF = initGM(s) %,Px,Py,Pz)
            dim = s.getDim();
            
            lastDOF = 0;
            
            for l=0:s.maxLevel
                for m=1:dim(4)
                    for k=1:dim(3)
                        for j=1:dim(2)
                            for i=1:dim(1)
                                s.data(i,j,k,m).resetGM;
                            end
                        end
                    end
                end
            end
            
            for l=0:s.maxLevel
                for m=1:dim(4)
                    for k=1:dim(3)
                        for j=1:dim(2)
                            for i=1:dim(1)
                                lastDOF = s.data(i,j,k,m).initGM(lastDOF,l);
                            end
                        end
                    end
                end
            end
           
            s.dofv = lastDOF;
        end
        
        % Initialize the GM
        function lastQP = initGMq(s) %,Px,Py,Pz)
            dim = s.getDim();
            
            lastQP = 0;
                        
            for l=0:s.maxLevel
                for k=1:dim(3)
                    for j=1:dim(2)
                        for i=1:dim(1)
                            lastQP = s.data(i,j,k).initGMq(lastQP,l);
                        end
                    end
                end
            end
           
            s.dofvq = lastQP;
        end
        
        function out = getGM(s)
            out = cell(s.getNumElements,1) ;
            dim = s.getDim();
            
            position = 0;
            
            for k=1:dim(3)
                for j=1:dim(2)
                    for i=1:dim(1)
                        numChild = s.data(i,j,k).getNumChild;
                        out(position+1:position+numChild) = s.data(i,j,k).getGM;
                        position = position + numChild;
                    end
                end
            end
        end
        
        function out = getGMq(s)
            out = cell(s.getNumElements,1) ;
            dim = s.getDim();
            
            position = 0;
            
            for k=1:dim(3)
                for j=1:dim(2)
                    for i=1:dim(1)
                        numChild = s.data(i,j,k).getNumChild;
                        out(position+1:position+numChild) = s.data(i,j,k).getGMq;
                        position = position + numChild;
                    end
                end
            end
        end
        
        function out = getRank(s)
            out = zeros(s.getNumGlobalElements,1) ;
            dim = s.getDim();
            
            position = 0;
            
            for k=1:dim(3)
                for j=1:dim(2)
                    for i=1:dim(1)
                        numChild = s.data(i,j,k).getNumChild;
                        out(position+1:position+numChild) = s.data(i,j,k).getRank;
                        position = position + numChild;
                    end
                end
            end
        end
        
        function setRefinementFlags(s)
            numElements = s.getNumElements;
            
            for e=1:numElements
                s.getElement(e).resetRefinementFlag();
            end
            
            for e=1:numElements
                if (~s.getElement(e).wantsRemoval)
                    s.getElement(e).checkLevel();
                end
            end
        end
        
        function out = maxLevel(s)
            elementLevels = s.getLv;
            out = max(elementLevels);
        end
        
        function [eStart,eEnd] = distributeElements(s)
            
            nproc = Parallel3D.MPI.nproc;
            
            if (nargin==2)
                s.eStart = round((s.rank  )*prod(Ne)/nproc)+1;
                s.eEnd   = round((s.rank+1)*prod(Ne)/nproc)  ;

            else
                 average        = prod(s.getNumElements) / nproc;
                 maxLevel       = mesh.maxLevel;
                 maxSubElements = 4^maxLevel;
                 
                 numChild = mesh.octTree.getNumChild;
                 
%                  nproc = 16;
%                  remainingProc = nproc;
%                  
%                  largestElement = max(numChild(:));
%                  numElements = sum(numChild(:));
%                  remainingElements = sum(numChild(:));
%                  
%                  bins = zeros(nproc,1);
%                  loc = 0;
%                  
%                  eBin = elementBin(nproc,mesh.octTree);
                 
%                  while sum(bins)~=numElements
%                      
% 
%                      %numElements = sum(numChild(:));
%                      numLargest = nnz(numChild==largestElement);
%                      
%                      for i=1:numLargest
%                          pos = mod(loc+i-1,nproc)+1;
%                          while bins(pos)>min(bins)
%                              pos=pos+1;
%                          end
%                          bins(pos) = bins(pos)+largestElement;
%                          eBin.add(pos,1,largestElement);
%                      end
%                      
%                      loc = numLargest;
%                      
%                      %remainingElements = remainingElements - numLargest*largestElement;
%                      %remainingProc = remainingProc-numLargest;
%                      %elementPerProc = remainingElements / remainingProc;
%                      %procID = -1*ones(size(numChild));
%                      %procID(numChild==largestElement) = 1:numLargest;
%                      
%                      numChild( numChild==largestElement ) = 0;
%                      
%                      largestElement = max(numChild(:));
%                  end
%                  
%                  elementPerProc = remainingElements / remainingProc;
                 
                 
%                  if (remainingProc>0)
%                     
%                  end
                 
                 range = zeros(s.nproc,2); % start/end element numbers for each proc
                 
                 pos   = 1;
                 count = 0;
                 
                 for cpu=1:s.nproc
                     range(cpu,1) = count+1;
                     while pos<=numel(numChild) && (count+numChild(pos)) < (cpu*average+maxSubElements/2)
                        count = count + numChild(pos);
                        pos=pos+1;
                     end
                     range(cpu,2) = count;
                 end
                 
                 range(cpu,2) = mesh.numElements;
                 
                 s.eStart = range(s.rank+1,1);
                 s.eEnd   = range(s.rank+1,2);
            end
            
            s.eRange           = s.eStart:s.eEnd;
            s.numLocalElements = numel(s.eRange);
            s.eOffset          = s.eStart-1;

            eStart   = s.eStart;
            eEnd     = s.eEnd;
            
            %s.getInfo;
        end
        
        
        
        % Overload the subsref, which allows access to octTree.data
%         % directly through octTree()
%         function varargout = subsref(obj,S)
%             if ( strcmp(S(1).type,'()') ) % && isempty(S.subs))
%                 [varargout{1:nargout}] = builtin('subsref',obj.data,S);
%             
%             elseif ( strcmp(S(1).type,'.') )
%                 try
%                     varargout{1:nargout} = obj.(S(1).subs);
%                 catch
%                     varargout = obj.(S(1).subs);
%                 end
%             else
%                 [varargout{1:nargout}] = builtin('subsref',obj,S);
%                 % Use built-in for any other expression
% %                 varargout = builtin('subsref',obj,S);
%             end
%         end
        
        function out = getDofv(s)
            out = s.dofv;
        end
    
    end
    
    methods (Access = protected)
        function cpObj = copyElement(obj)
            % Make a shallow copy
            cpObj = copyElement@matlab.mixin.Copyable(obj);
            % Copy sub_prop1 in subclass
            % Assignment can introduce side effects
            cpObj.data = copy(obj.data);
            cpObj.updateElements();
        end
    end
    
end