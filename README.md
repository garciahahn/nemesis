# Nemesis : a least-squares spectral element framework
This repository contains the Matlab files for the least-squares spectral element framework developed at the NTNU in Trondheim, Norway. 

# Installation
1. Clone the repository to a location on your local system by using  
  ```bash
  git clone git@bitbucket.org:mkwakkel/nemesis.git
  ```

2. In order to be able to use the Nemesis library, its path needs to be added to the MATLAB paths.
  This is possible with a ```startup.m``` file, which contains the following information:  
  ```matlab
  function startup
    if (ispc)
      mypathroot = 'path_to_nemesis'; % use your path here
    elseif (ismac)
      mypathroot = 'path_to_nemesis'; % use your path here
    elseif (isunix)
      mypathroot = 'path_to_nemesis'; % use your path here
    end
    p = {fullfile(mypathroot)};
    addpath(p{:});
    setupPaths;
    System.init(-1);
  end
  ```
  
  Place this file in a location where MATLAB is able to find it during startup. On Linux (Ubuntu) this location is probably the home folder of the user: ~/ (or in ~/Documents/MATLAB/).
  Its location can be determined by calling ```which startup``` within MATLAB.
  
3. Once MATLAB is started some information related to Nemesis should be visible. It will also show an error related to MPI, which should disappear after the compilation of the MEX files.  
  The MEX files can be compiled from the terminal by running ```./install.sh``` from the root folder of Nemesis.

# Possible problems
If the code is installed on a machine with a basic MATLAB installation, it is possible that one of the functions required for the code to work is not installed.
Such function is the function `combvec` from the Deep Learning Toolbox. Be sure to have installed the Deep Learning Toolbox from MATLAB.

# Changelog
09/05/19 : Version 1.1
            - simplified compilation of parallel/solver mex files through System.compileMex command
            - added warning for NMPI.finalize use
08/05/19 : Version 1.0
