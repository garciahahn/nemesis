classdef quadratureHandler < handle
    %QuadratureHANDLER Class which controls the quadrature assignment
    %   Detailed explanation goes here
    
    properties (Constant, Access=private)
        invalid_dof_index   = -1;
    end
    
    properties (Access=public)
        rank, nproc;
        feType;             % single integer, or an array with feType for each element
        mesh;               % mesh based on StaticMesh
%         lastDoF;            % next_free_dof
        dofRankArray;       % for internal use only
        numLocalElements    %
    end
    
    properties (GetAccess=public)
        numDof;             % Global number of DoF
        numLocalDof;     	% Number of DoF on this rank (parallel)
        
        levels;
        dofLevels;
        
        dofs;               % cell array with the DoF numbers (size: numElements)
        localDofs;          % cell array with the DoF numbers (size: numLocalElements)
        sharedDoFs;
        
        eRanks;             % which rank is in control of the element (size: numElements)
        dofRanks;           % which rank is in control of the DoF (size: totalNumDoF)
        
        eStart;
        eEnd;
        eRange;
        
        lastDoF;
    end
        
    methods
        function s = quadratureHandler(mesh,feType)
            %DOFHANDLER Construct an instance of this class
            %   The DoFHandler is setup from a mesh
            
            switch nargin
                case 0
                    s.reset();
                case 1
                    %mesh.subscribe( class(s) );
                    s.mesh      = mesh;
                case 2
                    s.mesh      = mesh;
                    s.feType    = feType;
                otherwise
                    error('unsupported number of arguments, maximum is 2')
            end
            
            s.rank  = System.rank;
            s.nproc = System.nproc;
            
            %s.mesh.dh = s;
            
            s.update;
        end
        
        function disp(s)
            fprintf('DoFHandler with the following private properties:\n')
            fprintf('\n')
            fprintf('    number of elements     : %d\n',numel(s.dofs));
            fprintf('    total number of dofs   : %d\n',s.numDof);
            fprintf('\n')
        end
                
        function update(s)
            if numel(s.feType)==1
                s.feType = s.feType * ones(s.mesh.numElements,1);
            elseif (numel(s.feType) == s.mesh.getNumElements)
                % do nothing
            else
                if ( System.rank==0 && System.settings.outp.debug)
                    fprintf('The passed feType has a wrong size, assuming all equal to 1\n')
                end
                s.feType = ones(s.mesh.numElements,1);
            end
            
            s.reset;
            s.setupLevels;
            s.distribute2();
            %s.setupDofLevels;
            
            %s.renumberDofs();
            
            if (System.rank==0)
                fprintf('Globally there are %d DoFs\n',s.lastDoF);
            end
                        
            s.setElementRanks();
            s.setDoFRankArray();
            s.setSharedDoFs();
            
            s.setLocalDoFs();
            
            s.numDof = s.lastDoF;
        end
        
        function checkUniqueDofs(s)
            count = 0;
            for i=1:numel(s.dofs)
                if (numel(s.dofs{i}) ~= numel( unique(s.dofs{i})) )
                    fprintf('There are duplicate dof number in element %d\n',i);
                    count = count+1;
                end
            end
            if (count==0)
                fprintf('There are no duplicate dofs!\n')
            else
                fprintf('There are %d duplicate dofs!\n',count)
            end
        end
    end
    
    methods (Access=private)
        function reset(s)
            s.lastDoF = 0;
            
            if (isempty(s.mesh))
                s.dofs = [];
            else
                s.levels = cell(s.mesh.numElements,1);
                s.dofs   = cell(s.mesh.numElements,1);
                if (~isempty(s.feType))
                    if ~any(s.feType-s.feType(1))
                        finiteElement = FECollection.get(s.feType(1));                        
                        for e=1:numel(s.dofs)
                            s.dofs{e} = s.invalid_dof_index * ones( finiteElement.dofSize );
                        end
                    else
                        for e=1:numel(s.dofs)
                            finiteElement = FECollection.get( s.feType(e) );
                            s.dofs{e} = s.invalid_dof_index * ones( finiteElement.dofSize );
                        end
                    end
                end
                
                % Reset dofs inside all gElements
                for i=0:s.mesh.dim
                    %cellfun( @(x) x.reset, s.mesh.gElements(i) );
                    cellfun( @(x) x.reset, s.mesh.getAllElements(i) );
                end
            end
        end
        
        function setupLevels(s)
            elements = s.mesh.elements;
            for e=1:numel(elements)
                s.levels{e} = elements(e).levels();
            end
        end
        
        function setupDofLevels(s)
            elements = s.mesh.elements;
            for e=1:numel(elements)
                s.dofLevels{e} = elements(e).dofLevels();
            end
        end
        
        % This function assigns a number to each DoF by looping over all
        % gElements wihtin the mesh. For each feElement
        function distribute(s)                        
            gElements = s.mesh.allElements{ s.mesh.dim+1 };
            
            patternSize = zeros(1,s.mesh.dim);
            
            for f=1:numel(gElements)
                
                % Get the finite element from the collection
%                 finiteElement = FECollection.get( s.feType(f) );
                finiteElement = FECollection.get( gElements{f}.feType );
                
                for d=0:s.mesh.dim
                    % Get the dof flags
                    dofPattern = (finiteElement.dofType==d);
                    ePattern   = (finiteElement.dofTypeID .* dofPattern );
                
                    % Extract subElements of the hex
                    if (d<finiteElement.DIM)
                        subElements = gElements{f}.element(d);
                    else
                        subElements = gElements{f};
                    end
                    
                    %if (any(dofPattern(:)))
                        for e=1:numel(subElements)
                            selected = subElements(e);

                            if (selected.active)
                                flags = (ePattern==e);

                                if ( isempty(selected.dof) )
                                    %s.dofs{f}( flags ) = s.lastDoF + ( 1:nnz( flags ) );
                                    
                                    if (nnz(flags)>0)
                                        dofNumbering = s.lastDoF + ( 1:nnz( flags ) );

                                        %reshape
                                        for i=1:numel( size(flags) )
                                            patternSize(i) = max( unique( sum(flags,i) ) );
                                        end
                                        selected.dof = reshape( dofNumbering, patternSize );

                                        s.lastDoF    = s.lastDoF + nnz( flags );
                                    end
                                %else
                                %    % dofs have been assigned already: copy dofs from vertice
                                %    if ( numel(selected.dof) == nnz(flags) )
                                %        s.dofs{f}( flags ) = selected.dof;
                                %    else
                                %        error('The number of dofs in this vertice does not correspond to the dofs exepcted by this feElement');
                                %    end
                                end
                                                                
                                if (d==s.mesh.dim)
                                    s.dofs{f} = selected.allDoF;
                                end
                                
                            else
                                % do nothing
                            end
                        end
                    %end
                end
            end
        end
        
        % This function assigns a number to each DoF by looping over all
        % gElements wihtin the mesh. For each feElement
        function distribute2(s)                        
            elements = s.mesh.elements;
            
            %fprintf('there are %d elements in distribute2\n',numel(elements));
            
            try
                for e=1:numel(elements)    
                    [ s.dofs{e}, s.lastDoF ] = elements(e).dof(s.lastDoF);
                end
            catch MExp
                error('Problem with the DoF distribution in DoFHandler')
            end
            %fprintf('there are %d dofs in distribute2\n',s.lastDoF);
        end
          
        function renumberDofs(s)
            mapping = zeros( s.lastDoF, 1 );
            
            lastNum = 0;
            for l=0:1
                for e=1:numel(s.dofs)
                    selection = s.dofs{e}( s.dofLevels{e}==l );
                    if (~isempty(selection))
                        if ( numel( mapping( mapping(selection)==0 ) ) > 0 )
                            numbers = lastNum + [ 1:numel( mapping( mapping(selection)==0 ) ) ];
                            mapping( selection( mapping(selection)==0 ) ) = numbers;
                            lastNum = numbers(end);
                        end
                    end
                end
            end
            
            for e=1:numel(s.dofs)
                dofSize = size( s.dofs{e} );
                s.dofs{e} = reshape( mapping( s.dofs{e}(:) ), dofSize );
            end
        end
        
        % Spread the elements over all available processors 
        function setElementRanks(s)
            numElements = s.mesh.numElements;
            
            numElementsPerProc 	    = floor( numElements / s.nproc );            
            numRemainingElements    = mod( numElements, s.nproc );
            
            elementCount = numElementsPerProc * ones( s.nproc,1 );
            elementCount(1:numRemainingElements) = elementCount(1:numRemainingElements) + 1;
                        
            count = 0;
            for p=1:s.nproc
                cStart = count + 1;
                cEnd   = count + elementCount(p);
                
                s.eRanks( cStart:cEnd ) = p-1;
                
                if ((p-1)==s.rank)
                    s.eStart = cStart;
                    s.eEnd   = cEnd;
                end
                
                s.eRange = s.eStart:s.eEnd;
                
                count = cEnd;
            end
        end
        
        function setDoFRankArray(s)
            % First create a sparse logical array where all the DOFs from
            % each rank are set to true
            s.dofRankArray = sparse( false(s.lastDoF,s.nproc) );
            
            for e=1:s.mesh.numElements
                try
                    s.dofRankArray( s.dofs{e}(:), s.eRanks(e)+1 ) = true;
                catch
                    error('LSQDIM:dof:DoFHandler :: problem with setDoFRankArrays')
                end
            end
            
            % Compute the 'numLocalDof' on this processor
            s.numLocalElements  = nnz(s.eRanks==s.rank);
            s.numLocalDof       = nnz( s.dofRankArray(:,s.rank+1) ); 
                        
            %fprintf('rank %d has %d elements and %d local DoFs\n',s.rank,s.numLocalElements,s.numLocalDof)
        end
       
        function setSharedDoFs(s)           
            if (isempty(s.dofRankArray))
                error('Please use setDoFRankArray before using setMutualDoFs')
            end
            
            % If the sum in the 2nd direction is larger than 1, that DoF is
            % shared with 1 or more other processors. From these flags the
            % corresponding ranks can be determined as well
            allSharedDoFs   = ( sum(s.dofRankArray,2)>1 );
            sharedDoFRanks  = s.dofRankArray( allSharedDoFs,: );
            numSharedDoFs   = nnz( allSharedDoFs );
            
            % Initialize dofRanks
            allDofRanks = -1 * ones( s.lastDoF,1 );
            for p=1:s.nproc
                idx = logical( s.dofRankArray(:,p) .* (~allSharedDoFs) );
                allDofRanks( idx ) = p-1;
            end
            
            % Determine the current number of dofs on each rank (before 
            % distributing the shared dof)
            rankBins = zeros(1,s.nproc);
            for p=1:s.nproc
                rankBins(p) = nnz( allDofRanks==(p-1) );
            end
            
            % Check if everything is still ok
            if ( (sum(rankBins)+numSharedDoFs) ~= s.lastDoF )
                save(['setshareddof_' num2str(s.rank) '.mat'])
                error('Problem with distributing the DoF')
            end
            
            % From the global sharedDoFs the locally sharedDoFs can be extracted
            localSharedDoFs = sharedDoFRanks( sharedDoFRanks(:,s.rank+1)==1,: );
            
            % Now determine how many DoFs are shared with which processor
            numLocalSharedDoFs = zeros(1,s.nproc);
            for p=1:s.nproc
                if (p~=s.rank+1)
                    numLocalSharedDoFs(p) = nnz( localSharedDoFs(:,p) );
                end
            end
            
            idealNumDoFperRank = floor( s.lastDoF / s.nproc );
            
            sharedIdx = find(allSharedDoFs);
            for i=1:numel(sharedIdx)
                
                % First determine which bins (=rank+1) are sharing this DoF
                sharingBins = find( sharedDoFRanks(i,:) );
                
                % Next, check if the 1st sharingBin has reached the
                % idealNumDoFperRank already. If not, add it to that bin.
                % Otherwise find which of the available bins has the least
                % amount of DoF
                if ( rankBins(sharingBins(1)) < idealNumDoFperRank )
                    idx = 1;
                else
                    [~,idx] = min( rankBins( sharingBins ) );                    
                end
                
                allDofRanks( sharedIdx(i) )  = sharingBins(idx) - 1;
                rankBins( sharingBins(idx) ) = rankBins( sharingBins(idx) ) + 1;
            end
            
            % Check that all DoFs have been distributed
            if ( s.lastDoF ~= sum(rankBins) )
                error('The processor bins do not equal the total number of DoF')
            end
            
            s.dofRanks   = allDofRanks( s.dofRankArray(:,s.rank+1) );
            
            s.sharedDoFs = cell(1,s.nproc);
            for i=1:s.nproc
                if ( i == (s.rank+1) )
                    s.sharedDoFs{i} = allDofRanks( s.dofRankArray(:,i) );       % this cell contains the rank number that owns the DoFs (from which to receive)
%                     for j=1:s.nproc
%                         if (i~=j && nnz( s.sharedDoFs{i}==(j-1) ) > 0 )
%                             fprintf('Rank %d expects to receive %d values from rank %d\n',s.rank,nnz( s.sharedDoFs{i}==(j-1) ),j-1);
%                         end
%                     end
                else
                    s.sharedDoFs{i} = s.dofRankArray( allDofRanks==s.rank, i ); % these cells contain flags which DoFs they need to send to which rank (i-1)
%                     for j=1:s.nproc
%                         if (i~=j && nnz( s.sharedDoFs{i} ) > 0 )
%                             fprintf('Rank %d expects to send %d values to rank %d\n',s.rank,nnz( s.sharedDoFs{i} ),i-1);
%                         end
%                     end
                end 
            end
            
            %save(['setshareddof_' num2str(s.rank) '.mat'])
        end

        function setLocalDoFs(s)
            % Assign the local DoF numbers, starting with all -1 numbers
            localDoFArray = -1 * ones(1,s.lastDoF);
            localDoFArray( s.dofRankArray(:,s.rank+1) ) = 1:s.numLocalDof;
            
            % Convert the localDoFs array to an elemental cell array
            s.localDofs = cell(1,numel(s.eRange));
            id = 0;
            for e=s.eRange
                id = id+1;
                s.localDofs{id} = localDoFArray( s.dofs{e} );
            end
        end

        
        
        
        
%   653 template<int dim, int spacedim>
%   654 DoFHandler<dim,spacedim>::DoFHandler (const Triangulation<dim,spacedim> &tria)
%   655   :
%   656   tria(&tria, typeid(*this).name()),
%   657   selected_fe(0, typeid(*this).name()),
%   658   faces(NULL),
%   659   mg_faces (NULL)
%   660 {
%   661   // decide whether we need a
%   662   // sequential or a parallel
%   663   // distributed policy
%   664   if (dynamic_cast<const parallel::shared::Triangulation< dim, spacedim>*>
%   665       (&tria)
%   666       != 0)
%   667     policy.reset (new internal::DoFHandler::Policy::ParallelShared<dim,spacedim>());
%   668   else if (dynamic_cast<const parallel::distributed::Triangulation< dim, spacedim >*>
%   669            (&tria)
%   670            == 0)
%   671     policy.reset (new internal::DoFHandler::Policy::Sequential<dim,spacedim>());
%   672   else
%   673     policy.reset (new internal::DoFHandler::Policy::ParallelDistributed<dim,spacedim>());
%   674 }
%   675 
%   676 
%   677 template<int dim, int spacedim>
%   678 DoFHandler<dim,spacedim>::DoFHandler ()
%   679   :
%   680   tria(0, typeid(*this).name()),
%   681   selected_fe(0, typeid(*this).name()),
%   682   faces(NULL),
%   683   mg_faces (NULL)
%   684 {}
%   685 
%   686 
%   687 template <int dim, int spacedim>
%   688 DoFHandler<dim,spacedim>::~DoFHandler ()
%   689 {
%   690   // release allocated memory
%   691   clear ();
%   692 }
%   693 
%   694 
%   695 template<int dim, int spacedim>
%   696 void
%   697 DoFHandler<dim,spacedim>::initialize(
%   698   const Triangulation<dim,spacedim> &t,
%   699   const FiniteElement<dim,spacedim> &fe)
%   700 {
%   701   tria = &t;
%   702   faces = 0;
%   703   number_cache.n_global_dofs = 0;
%   704 
%   705   // decide whether we need a
%   706   // sequential or a parallel
%   707   // distributed policy
%   708   if (dynamic_cast<const parallel::shared::Triangulation< dim, spacedim>*>
%   709       (&t)
%   710       != 0)
%   711     policy.reset (new internal::DoFHandler::Policy::ParallelShared<dim,spacedim>());
%   712   else if (dynamic_cast<const parallel::distributed::Triangulation< dim, spacedim >*>
%   713            (&t)
%   714            == 0)
%   715     policy.reset (new internal::DoFHandler::Policy::Sequential<dim,spacedim>());
%   716   else
%   717     policy.reset (new internal::DoFHandler::Policy::ParallelDistributed<dim,spacedim>());
%   718 
%   719   distribute_dofs(fe);
  
  
        
%         function outputArg = method1(obj,inputArg)
%             %METHOD1 Summary of this method goes here
%             %   Detailed explanation goes here
%             outputArg = obj.Property1 + inputArg;
%         end

        % Initialize the DoF. First a present GM is removed (reset), next
        % the GM is created from the coarsed level upto the finest level.
        function lastDOF = init(s)
            s.dofs = cell(s.mesh.numElements,1);
            
            % Initialize dofs to 'Inf'
            for e=1:s.mesh.numElements
                element = s.mesh.getElement(e);
                s.dofs{e} = Inf(element.dofSize);
            end
                        
            %dim = s.getDim();
            
            lastDOF = 0;
                        
            for l=0:s.mesh.maxLevel
                for e=1:s.mesh.numElements
                    element = s.mesh.getElement(e);
                    
                    if (element.lvl==l)                        
                        % assign dofs
                        unsetDofs               = ( s.dofs{e}==Inf );
                        numUnsetDofs           	= nnz( unsetDofs );                        
                        s.dofs{e}( unsetDofs )  = lastDOF+1 : lastDOF+numUnsetDofs;                    
                        lastDOF               	= lastDOF + numUnsetDofs;
                        
                        % push dofs of current element to neighboring elements
                        for dir=1:size(element.neighbor,1)
                            for pos=1:2
                                neighbor = element.neighbor{dir,pos};                                
                                if (~isempty(neighbor))
                                    if (~neighbor.isRefined)
                                        sharedDof = element.sharedDof(dir,pos);
                                        
                                        s.dofs{ neighbor.id } = 0;
                                        disp('')
                                    else
                                        
                                    end
                                end
                            end
                        end
                    end
                end
            end
            
                
%                 for m=1:dim(4)
%                     for k=1:dim(3)
%                         for j=1:dim(2)
%                             for i=1:dim(1)
%                                 lastDOF = s.data(i,j,k,m).initGM(lastDOF,l);
%                             end
%                         end
%                     end
%                 end
%             end
           
            s.numDoF_per_variable = lastDOF;
        end
        
        
        function lastDOF = initGM(s,lastDOF,activeLv)
            
            % Check if the current element has the same level as the
            % active level (the level for which the numbering is being
            % created).
            if (s.lvl == activeLv)
                                 
                % Copy GM values from the parent. 
                % NB: the function checks if there is a parent
                s.pullGM(0);
                    
                % Copy GM values from all neighbors
                for l=0:activeLv
                    for pos=0:1
                        for dir=1:s.disc.DIM
                            s.pullGM(dir,pos,l);
                        end
                    end
                end
                    
                % If the current element is not refined, assign DOF to 
                % all unassigned GM
                if (~s.isRefined)

                    numAssign                = nnz( s.GM(:,:,:)==Inf );
                    s.GM( s.GM(:,:,:)==Inf ) = lastDOF+1 : lastDOF+numAssign;                    
                    lastDOF                  = lastDOF+numAssign;

                    % Copy GM values to 'lower' neighbors which have been
                    % refined
                    for pos=0:1
                        for dir=1:s.disc.DIM
                            s.pushGM(dir,pos,activeLv);
                        end
                    end                
                end
                    
            elseif (s.lvl<activeLv)
                    
%                 for dir=1:s.disc.DIM
%                     for pos=0:1
%                         s.pullGM(dir,pos,activeLv);
%                     end
%                 end
                
                % Switch to leaves/children if the activeLv is higher than
                % the current lvl
                if (s.isRefined)
                    for i=1:numel(s.ch)
                        lastDOF = s.ch{i}.initGM(lastDOF,activeLv);
                    end
                end
            end
        end
        
        
 
        

        
    end
end

