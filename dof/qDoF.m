classdef qDoF < handle
    %DoF Degree of Freedom belonging to one (or more) FEObject(s)
    %   Every FEObject has internally a DoF object which contains the
    %   numbering of its degrees of freedom. It is a separate object to be
    %   able to link on DoF object to multiple FEObjects, which is the case
    %   when a FEObject is periodic. In that case both FEObjects point to
    %   the same DoF object, providing them with identical dof numbering.
    
    properties (Access=private)
        numbers;
    end
    
    methods
        function s = qDoF()
            %DoF Construct an instance of this class
            %   Initially the DoF object does not have numbers
            s.reset;
        end
        
        function set(s,numbers)
            %SET Summary of this method goes here
            %   Sets the numbering of the dofs         
            if (isempty(s.numbers))
                s.numbers = numbers;
            else
                error('The qdofs have already been set')
            end
        end
        
        function out = get(s)
            out = s.numbers;
        end
        
        function reset(s)
            s.numbers = [];
        end
%         
%         function out = get.dof(s)
%             if ~isempty(s.dof.get)
% %                 out = s.dof;
%                 out = s.dof.get;
%             else
%                 if (~isempty(s.parent))
% %                     out = s.parent.dof;
%                     out = s.parent.dof.get;
%                 else
%                     out = [];
%                 end
%             end
%         end
    end
end

