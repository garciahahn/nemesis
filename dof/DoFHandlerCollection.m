classdef DoFHandlerCollection < Singleton
    %FIELDCOLLECTION Collection of the fields of all physics
    %   Every Physics has it's own fields, which are only accessible from
    %   itself. By putting all these fields also in a collection, every
    %   physics can use any fields provided by other Physics
    
    properties (Access=private)
        dofHandlers;
        numDoFHandlers= 0;
    end
    
    methods(Access=private)
        % Guard the constructor against external invocation.  We only want
        % to allow a single instance of this class.  See description in
        % Singleton superclass.
        function s = DoFHandlerCollection()
        end
    end
    
    methods (Static)
        % Concrete implementation.  See Singleton superclass.
        function s = instance()
            persistent uniqueInstance
            if isempty(uniqueInstance)
                s = DoFHandlerCollection();
                uniqueInstance = s;
            else
                s = uniqueInstance;
            end
        end
        
        % Function to add a field to the collection
        function add(dh_in)
            s = DoFHandlerCollection.instance;
            s.dofHandlers{s.numDoFHandlers+1} = dh_in;
            s.numDoFHandlers = numel(s.dofHandlers);
        end
        
        % Function to get a field from the collection (by variable name)
        function out = get(variable)
            dh = DoFHandlerCollection.instance;
            
            % If the variable passed is numeric, just use it as an index,
            % otherwise find the corresponding
            if isnumeric(variable)
                idx = variable;
            else
                fieldNames = cellfun( @(x) x.name, dh.dofHandlers, 'UniformOutput', false );
                idx = ismember(fieldNames,variable);
            end
            if (nnz(idx)==1)
                out = dh.dofHandlers{ idx };
            elseif (nnz(idx)>1)
                error('LSQDIM:dof:DoFHandlerCollection :: the requested variable is multiple times present in the DoFHandlerCollection')
            else
                out = NaN;
                %error('LSQDIM:field:FieldCollection :: the requested variable is not present in the FieldCollection')
            end
        end
        
        function update(mesh)
            dh = DoFHandlerCollection.instance;
            
            % Loop over the DoFHandlers to select the requested one
            for i=1:dh.numDoFHandlers
                selected = dh.dofHandlers{i};
                if isequal(selected.mesh.Lv,mesh.Lv)
                    selected.update;
                end
            end
        end
        
        function out = getByID(mesh,periodic)
            dh = DoFHandlerCollection.instance;
            
            out = [];
            
            % Loop over the DoFHandlers to select the requested one
            for i=1:dh.numDoFHandlers
                selected = dh.dofHandlers{i};
                if isequal(selected.periodic,periodic) && isequal(selected.mesh.Lv,mesh.Lv)
                    out = selected;
                    break;
                end
            end
            
            % If there is no suitable DoFHandler, add a new one to the
            % DoFHandlerCollection and select that one
            if isempty(out)
                newDoFHandler = DoFHandler( mesh, periodic );
                dh.add( newDoFHandler );
                out = newDoFHandler;
            end
        end
        
        function info()
            s = DoFHandlerCollection.instance();
            numDoFHandlers = numel(s.dofHandlers);
            fprintf('    DoFHandlerCollection with %d DoFHandlers\n',numDoFHandlers);
            %for i=1:numDoFHandlers
            %    fprintf('       Field %d : %s\n',i,s.dofHandlers{i}.name);
            %end
        end
        
        function reset()
            s = DoFHandlerCollection.instance();
            s.dofHandlers = cell(0);
            s.numDoFHandlers = 0;
        end
        
%         function update()
%             s = DoFHandlerCollection.instance();
%             for i=1:s.numDoFHandlers
%                 s.dofHandlers{i}.meshUpdate;
%             end
%         end
        
        % Function to save a PhysicsCollection to a struct. The number of  
        % physics and structs of the physics are saved (by calling
        % the save(physics) function (see Physics.save).
        function out = save()
            s = FieldCollection.instance();            
            out = cell(s.numDoFHandlers,1);
            for i=1:s.numDoFHandlers
                dh = DoFHandlerCollection.get(i);
                out{i} = save(dh);
            end
        end
        
        % Function to load a PhysicsCollection from a saved struct. First the
        % collection is reset, and next the physics are loaded and
        % added to the collection (see Physics.load).
        function load(dofHandlers)
            DoFHandlerCollection.reset;
            for i=1:numel(dofHandlers)
                temp = DoFHandler.load(dofHandlers{i});
                DoFHandlerCollection.add(i,temp);
            end
        end
    end
    
    methods

    end
end

