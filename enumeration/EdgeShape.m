classdef EdgeShape < uint32
   enumeration
      Linear(0), Circular(1), Sin(2), Cos(3);
   end
end