classdef CoordSystem < double
   enumeration
      Cartesian(0), Cylindrical(1), Spherical(2);
   end
end