classdef RestartID < int32
   enumeration
      batch_run(-2), initialization(-1), new(0) % otherwise restart
   end
end