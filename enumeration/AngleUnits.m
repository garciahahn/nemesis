classdef AngleUnits < int32
   enumeration
      Degrees(0), Radians(1)
   end
end