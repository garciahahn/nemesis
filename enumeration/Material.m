classdef Material < int32
   enumeration
      All(0), Fluid(1), Solid(2)
   end
end