classdef Variable < double
   enumeration
      u(1), v(2), p(3), c(1), omg(2), T(1), rho(2);
   end
end