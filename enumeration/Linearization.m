classdef Linearization < int32
   enumeration
      Newton(1), Picard(2);
   end
end