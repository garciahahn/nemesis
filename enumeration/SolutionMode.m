classdef SolutionMode < int32
   enumeration
      Nonlinear(1), Coupling(2), Normal(3), Residual(4);
   end
end