classdef Neighbor < int32
    enumeration
        % Front
%         LeftBottomFront(1), BottomFront(2), RightBottomFront(3), ...
%         LeftFront(4), Front(5), RightFront(6), ...
%         LeftTopFront(7), TopFront(8), RightTopFront(9), ...
%         
%         % Center
%         LeftBottom(10), Bottom(11), RightBottom(12), ...
%         Left(13), Right(15), ...
%         LeftTop(16), Top(17), RightTop(18), ...
%         
%         % Back
%         LeftBottomBack(19), BottomBack(20), RightBottomBack(21), ...
%         LeftBack(22), Back(23), RightBack(24), ...
%         LeftTopBack(25), TopBack(26), RightTopBack(27), ... 

      %Bottom(1), Left(2), Right(3), Top(4), Front(5), Back(6), Past(7), Future(8),...
      %BottomLeft(10);
  
      Bottom(3), Left(1), Right(2), Top(4), Front(5), Back(6), Past(7), Future(8),...
      BottomLeft(10);
    end
   
    methods(Static)
        function out = opposite(side)
%             out = 28-side;
            switch side
                case Neighbor.Bottom
                    out = Neighbor.Top;
                case Neighbor.Left
                    out = Neighbor.Right;
                case Neighbor.Right
                    out = Neighbor.Left;
                case Neighbor.Top
                    out = Neighbor.Bottom;
                case Neighbor.Front
                    out = Neighbor.Back;
                case Neighbor.Back
                    out = Neighbor.Front;
%                 case Neighbor.BottomLeft
%                     out = Neighbor.BottomLeft
            end
        end
        
        function out = children(side)
            switch side
                case Neighbor.Bottom
                    out = [3 4];
                case Neighbor.Left
                    out = [2 4];
                case Neighbor.Right
                    out = [1 3];
                case Neighbor.Top
                    out = [1 2];
                case Neighbor.Front
                    out = [];
                case Neighbor.Back
                    out = [];
%                 case Neighbor.BottomLeft
%                     out = Neighbor.BottomLeft
            end
        end
    end
end