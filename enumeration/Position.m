classdef Position < int32
   enumeration
      low(1), high(2)
   end
end