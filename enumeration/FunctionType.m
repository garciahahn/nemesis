classdef FunctionType < int32
    enumeration
        Hermite(1), Lagrange(2), BSpline(3)
    end
end