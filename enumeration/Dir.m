classdef Dir < int32
   enumeration
      x(1), y(2), z(3), t(4)
   end
end