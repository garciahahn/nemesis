classdef Direction < int32
    enumeration
        normal(1), tangential(2), tangential2(3), dynamicX(4), dynamicY(5), gradC(6), dynamicAngle(7)
    end
    
    methods (Static)
        function options()
            enumeration('Direction');
        end
    end
end