classdef CurvatureSharpening < int32
    
    % The 'default' (0) setting uses the common definition kappa = 2*H,
    % which is nonuniform around the interface (as it is a local curvature). 
    % The 'LScorrected' (1) settings uses the local levelSet value to 
    % correct the radius of curvature of the interface all around the 
    % interface. Its inverse gives therefore an uniform curvature around 
    % the interface, which value is the curvature of the interface Gamma.
    %
    % The 'default' setting provides kappa, where the 'LScorrected'
    % setting provides kappaGamma. The latter is more accurate.
    
    enumeration
        gradC0(0), gradC1(1), gradC2(2), smoothHeaviside(3), classicCH(1)
    end
    
    methods (Static)
        function options()
            enumeration('CurvatureSharpening');
        end
    end
end