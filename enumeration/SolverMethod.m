classdef SolverMethod < double
   enumeration
      direct(0), hybridCG(1), matlabCG(2), mexCG(3);
   end
end