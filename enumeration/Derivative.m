classdef Derivative < int32
    enumeration
        value(1), x(2), y(3), z(4), xx(5), yy(6), zz(7), xy(8), t(9), tt(10), yx(11), xxy(12), xxx(13), yyy(14), yyx(15), xyx(16), xt(17), yt(18)
    end
    
    methods (Static)
        function options()
            enumeration('Derivative');
        end
    end
end