classdef DataFormat < int32
   enumeration
      Flat(0), Tree(1)
   end
end