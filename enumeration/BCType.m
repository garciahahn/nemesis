classdef BCType < int32
   enumeration
      Weak(1), Strong(2), Dirichlet(0), Neumann(1), Neumann2(2), GNBC(3), WallPotential(4), CH1D(5), AC1D(6), CH0D(7), noslip(8), specialFWC(9), testBCX(10), testBCY(11), testBCT(12);
   end
end