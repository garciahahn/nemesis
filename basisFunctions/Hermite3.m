classdef Hermite3 < BasisFunction
    %HERMITE1 Hermite basis function with C1
    %   On boundaries between elements this basis function has continuous
    %   values and first derivatives
    
    properties
%         DD;          % 1D operator: 2nd derivative
        DD_amr;
        DDD_amr;
        scale;
    end
    
    methods
        function s = Hermite3(P,Q,C,scale)
            %UNTITLED6 Construct an instance of this class
            %   Detailed explanation goes here

            s = s@BasisFunction(P,Q,C);

            s.type = FunctionType.Hermite;
            
            s.ind = [1 2 3 4 9:1:s.dof 5 6 7 8];

            s.scale = scale;
            
            s.H   = s.Hermite;
            s.D   = s.DHermite;
            s.DD  = s.DDHermite;
            s.DDD = s.DDDHermite;
        end
        
        function initRefine(s,ratio)
            
            s.refinementRatio = ratio;
            
            s.qp_amr = cell(ratio,1);
            s.qw_amr = cell(ratio,1);
            
            s.H_amr  = cell(ratio,1);
            
            s.Z      = cell(ratio,1);
            
            for j=1:ratio
                x0 = -1 + 2/ratio * (j-1);
                x1 = -1 + 2/ratio *   j;
                
                [s.qp_amr{j},s.qw_amr{j}] = GLL(s.Q,x0,x1);
                
                s.H_amr{j}  = s.Hermite(s.qp_amr{j},1);
                s.D_amr{j}  = s.DHermite(s.qp_amr{j});
                s.DD_amr{j} = s.DDHermite(s.qp_amr{j});
                s.DDD_amr{j} = s.DDDHermite(s.qp_amr{j});
                
                s.Z{j} = ( s.H \ s.H_amr{j} );
                
                s.Z{j}( abs(s.Z{j}) < s.ROUNDOFF_LIMIT ) = 0;
            end
        end
        
        function plot(s)
            
            clf;
                        
            s.initRefine(2);
            
            % H
            subplot(4,3,1)
            hold on;
            for p=1:s.P
                plot( s.qp, s.H(:,p))                    
            end
            
            subplot(4,3,2)
            hold on;
            for j=1:s.refinementRatio
                for p=1:s.P
                    plot( s.qp_amr{j}, s.H_amr{j}(:,p))                    
                end
            end
            
            subplot(4,3,3)
            hold on;
                
            for p=1:s.P
                plot( (s.qp-1)/2, s.H*s.Z{1}(:,p))
                plot( (s.qp+1)/2, s.H*s.Z{2}(:,p))
            end
            
            % D
            subplot(4,3,4)
            hold on;
            for p=1:s.P
                plot( s.qp, s.D(:,p))                    
            end
            
            subplot(4,3,5)
            hold on;
            for j=1:s.refinementRatio
                for p=1:s.P
                    plot( s.qp_amr{j}, s.D_amr{j}(:,p))                    
                end
            end
            
            subplot(4,3,6)
            hold on;
            for p=1:s.P
                plot( (s.qp-1)/2, s.D * (s.refinementRatio) * s.Z{1}(:,p))          % NB: the refinementRatio is added because the Jacobian is changed by the refinement
                plot( (s.qp+1)/2, s.D * (s.refinementRatio) * s.Z{2}(:,p))
            end

            % DD
            subplot(4,3,7)
            hold on;
            for p=1:s.P
                plot( s.qp, s.DD(:,p))                    
            end  
            
            subplot(4,3,8)
            hold on;
            for j=1:s.refinementRatio
                for p=1:s.P
                    plot( s.qp_amr{j}, s.DD_amr{j}(:,p))                    
                end
            end
            
            subplot(4,3,9)
            hold on;
            for p=1:s.P
                plot( (s.qp-1)/2, s.DD * (s.refinementRatio)^2 * s.Z{1}(:,p))
                plot( (s.qp+1)/2, s.DD * (s.refinementRatio)^2 * s.Z{2}(:,p))                    
            end
            
            % DDD
            subplot(4,3,10)
            hold on;
            for p=1:s.P
                plot( s.qp, s.DDD(:,p))                    
            end  
            
            subplot(4,3,11)
            hold on;
            for j=1:s.refinementRatio
                for p=1:s.P
                    plot( s.qp_amr{j}, s.DDD_amr{j}(:,p))                    
                end
            end
            
            subplot(4,3,12)
            hold on;
            for p=1:s.P
                plot( (s.qp-1)/2, s.DDD * (s.refinementRatio)^3 * s.Z{1}(:,p))
                plot( (s.qp+1)/2, s.DDD * (s.refinementRatio)^3 * s.Z{2}(:,p))                    
            end
            
        end
    end
    
    methods (Access=private)

        %% Quintic Hermite
        % Function values for quintic Hermite
        function out = Hermite(s,x,J)
            %n:order - x:points - J:Jaccobian

            index = s.ind;      % indices for the points
            
            if (nargin<3)
                J     = s.J;        % Jacobian
            end
            
            if nargin==1
                x     = s.qp;       % quadrature points
            end
            
            out = zeros(length(x),length(index));

            t = 0.5*(x+1);
            
            for i=1:length(index)
                n = index(i);

                switch(n)
                   case 1
                       y= 1 - 35 * t.^4 + 84 * t.^5 - 70 * t.^6 + 20 * t.^7;
                   case 2
                       y= t - 20 * t.^4 + 45 * t.^5 - 36 * t.^6 + 10 * t.^7;
                   case 3 
                       y= (t.^2 - 10 * t.^4 + 20 * t.^5 - 15 * t.^6 + 4 * t.^7) / 2;
                   case 4
                       y= (t.^3 - 4 * t.^4 + 6 * t.^5 - 4 * t.^6 + t.^7) / 6;
                   case 5
                       y= 35 * t.^4 - 84 * t.^5 + 70 * t.^6 - 20 * t.^7;
                   case 6 
                       y= -15 * t.^4 + 39 * t.^5 - 34 * t.^6 + 10 * t.^7;
                   case 7 
                       y= (5 * t.^4 - 14 * t.^5 + 13 * t.^6 - 4 * t.^7) / 2;
                   case 8
                       y= (-t.^4 + 3 * t.^5 - 3 * t.^6 + t.^7) / 6;
                end

                out(:,i) = y; % / J;
            end

        end  
        
        % Function first derivatives for quintic Hermite
        function out = DHermite(s,x)

            index = s.ind;      % indices for the points
            J     = s.J;        % Jacobian
            
            % Alternative scaling
            %J = 1/(4*sqrt(2)*System.settings.phys.Cn);
            %J = 1/((4*sqrt(2)*System.settings.phys.Cn));
                        
            if (nargin==1)
                x     = s.qp;       % quadrature points
            end
            
            out = zeros(length(x),length(index));

            t = 0.5*(x+1);
            
            for i=1:length(index)
                n = index(i);

                switch(n)
                   case 1
                       y= - 140 * t.^3 + 420 * t.^4 - 420 * t.^5 + 140 * t.^6;
                   case 2
                       y= 1 - 80 * t.^3 + 225 * t.^4 - 216 * t.^5 + 70 * t.^6;
                   case 3 
                       y= (2*t - 40 * t.^3 + 100 * t.^4 - 90 * t.^5 + 28 * t.^6) / 2;
                   case 4
                       y= (3*t.^2 - 16 * t.^3 + 30 * t.^4 - 24 * t.^5 + 7*t.^6) / 6;
                   case 5
                       y= 140 * t.^3 - 420 * t.^4 + 420 * t.^5 - 140 * t.^6;
                   case 6 
                       y= -60 * t.^3 + 195 * t.^4 - 204 * t.^5 + 70 * t.^6;
                   case 7 
                       y= (20 * t.^3 - 70 * t.^4 + 78 * t.^5 - 28 * t.^6) / 2;
                   case 8
                       y= (-4*t.^3 + 15 * t.^4 - 18 * t.^5 + 7*t.^6) / 6;
                end
                out(:,i) = y / J;
            end        
        end
        
        % Function first derivatives for quintic Hermite
        function out = DDHermite(s,x)

            index = s.ind;      % indices for the points
            J     = s.J;        % Jacobian
            
            % Alternative scaling
            %J = 1/(4*sqrt(2)*System.settings.phys.Cn);
            %J = 1/((4*sqrt(2)*System.settings.phys.Cn));
                        
            if (nargin==1)
                x     = s.qp;       % quadrature points
            end
            
            t = 0.5*(x+1);
            
            out = zeros(length(x),length(index));

            for i=1:length(index)
                n = index(i);

                switch(n)
                   case 1
                       y= -420 * t.^2 + 1680 * t.^3 - 2100 * t.^4 + 840 * t.^5;
                   case 2
                       y= -240 * t.^2 + 900 * t.^3 - 1080 * t.^4 + 420 * t.^5;
                   case 3 
                       y= (2 - 120 * t.^2 + 400 * t.^3 - 450 * t.^4 + 168 * t.^5) / 2;
                   case 4
                       y= (6*t - 48 * t.^2 + 120 * t.^3 - 120 * t.^4 + 42*t.^5) / 6;
                   case 5
                       y= 420 * t.^2 - 1680 * t.^3 + 2100 * t.^4 - 840 * t.^5;
                   case 6 
                       y= -180 * t.^2 + 780 * t.^3 - 1020 * t.^4 + 420 * t.^5;
                   case 7 
                       y= (60 * t.^2 - 280 * t.^3 + 390 * t.^4 - 168 * t.^5) / 2;
                   case 8
                       y= (-12*t.^2 + 60 * t.^3 - 90 * t.^4 + 42*t.^5) / 6;
                end
                out(:,i) = y / J;
            end        
        end
        
        % Function first derivatives for quintic Hermite
        function out = DDDHermite(s,x)

            index = s.ind;      % indices for the points
            J     = s.J;        % Jacobian
            
            % Alternative scaling
            %J = 1/(4*sqrt(2)*System.settings.phys.Cn);
            %J = 1/((4*sqrt(2)*System.settings.phys.Cn));
                        
            if (nargin==1)
                x     = s.qp;       % quadrature points
            end
            
            t = 0.5*(x+1);
            
            out = zeros(length(x),length(index));

            for i=1:length(index)
                n = index(i);

                switch(n)
                   case 1
                       y= -840 * t + 5040 * t.^2 - 8400 * t.^3 + 4200 * t.^4;
                   case 2
                       y= -480 * t + 2700 * t.^2 - 4320 * t.^3 + 2100 * t.^4;
                   case 3 
                       y= (-240 * t + 1200 * t.^2 - 1800 * t.^3 + 840 * t.^4) / 2;
                   case 4
                       y= (6 - 96 * t + 360 * t.^2 - 480 * t.^3 + 210*t.^4) / 6;
                   case 5
                       y= 840 * t - 5040 * t.^2 + 8400 * t.^3 - 4200 * t.^4;
                   case 6 
                       y= -360 * t + 2340 * t.^2 - 4080 * t.^3 + 2100 * t.^4;
                   case 7 
                       y= (120 * t - 840 * t.^2 + 1560 * t.^3 - 840 * t.^4) / 2;
                   case 8
                       y= (-24*t + 180 * t.^2 - 360 * t.^3 + 210*t.^4) / 6;
                end
                out(:,i) = y / J;
            end        
        end
        
    end
end

