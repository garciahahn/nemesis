classdef Hermite1 < BasisFunction
    %HERMITE1 Hermite basis function with C1
    %   On boundaries between elements this basis function has continuous
    %   values and first derivatives
    
    properties
%         DD;          % 1D operator: 2nd derivative

        DD_amr;
        DDD_amr;
        
        Z1, Z2; 
        
        scale;
        
        order = 3;
    end
    
    methods
        function s = Hermite1(P,Q,C,scale)
            %UNTITLED6 Construct an instance of this class
            %   Detailed explanation goes here

            s = s@BasisFunction(P,Q,C);

            s.type = FunctionType.Hermite;
            
            if (P<4)
                error('For Hermite1 at least 4 modes have to be used')
            end
            
            s.ind = [1 2 5:1:s.P 3 4];

            s.scale = scale;
            
            if s.order==3
                s.H   = s.Hermite;
                s.D   = s.DHermite;
                s.DD  = s.DDHermite;
            elseif (s.order==5)
                s.H   = s.Hermite5;
                s.D   = s.DHermite5;
                s.DD  = s.DDHermite5;
            else 
                error ('Undefined order for Hermite1')
            end
            %s.DDD = s.DDDHermite;
        end
        
        function [H, D, DD] = GetCustomQuadraturePoints(s, x)
          H = s.Hermite(x);
          D = s.DHermite(x);
          DD = s.DDHermite(x);
        end % GetCustomQuadraturePoints
        
        function initRefine(s,ratio)
            
            s.refinementRatio = ratio;
            
            s.qp_amr = cell(ratio,1);
            s.qw_amr = cell(ratio,1);
            
            s.H_amr  = cell(ratio,1);
            
            s.Z      = cell(ratio,1);
            
            if (ratio==1)
                s.Z{1} = speye( s.dof );
            else
                for j=1:ratio
                    x0 = -1 + 2/ratio * (j-1);
                    x1 = -1 + 2/ratio *   j;

                    [s.qp_amr{j},s.qw_amr{j}] = GLL(s.Q,x0,x1);

                    s.J = 1;
                    
                    if s.order==3
                        s.H_amr{j}   = s.Hermite(s.qp_amr{j},1);
                        s.D_amr{j}   = s.DHermite(s.qp_amr{j});
                        s.DD_amr{j}  = s.DDHermite(s.qp_amr{j});
                    elseif (s.order==5)
                        s.H_amr{j}   = s.Hermite5(s.qp_amr{j},1);
                        s.D_amr{j}   = s.DHermite5(s.qp_amr{j});
                        s.DD_amr{j}  = s.DDHermite5(s.qp_amr{j});
                    else 
                        error ('Undefined order for Hermite1')
                    end  
                        
                        
                    %s.DDD_amr{j} = s.DDDHermite(s.qp_amr{j});
                    
                    %s.H \ H_tmp;
                    %H_tmp \ s.H_amr{j}

                    %H_tmp = s.Hermite(s.qp,0.5);
                    %s.Z{j}  = ( H_tmp   \ s.Hermite(s.qp_amr{1},1) );

                    % original
                    s.Z{j}  = ( s.H   \ s.H_amr{j} );

                    %s.Z{j}(2,2) = 0.5 * s.Z{j}(2,2);
                    %s.Z{j}(end,end) = 0.5 * s.Z{j}(end,end);

                    %H_tmp = s.Hermite(s.qp,0.5);        % rescale old quadrature points
                    %H_amr = s.Hermite(s.qp_amr{j},1);   % normal scaled new quadrature points
                    %Z1 = ( H_tmp \ H_amr );



    %                 s.Z{j}  = ( H_tmp \ s.H_amr{j} );
                    %s.Z1{j} = ( s.D  \ s.D_amr{j} );
                    %s.Z2{j} = ( s.DD \ s.DD_amr{j} );

                    s.Z{j}( abs(s.Z{j}) < s.ROUNDOFF_LIMIT ) = 0;
                    %s.Z1{j}( abs(s.Z1{j}) < s.ROUNDOFF_LIMIT ) = 0;
                    %s.Z2{j}( abs(s.Z2{j}) < s.ROUNDOFF_LIMIT ) = 0;
                end
            end
            
%             qp_all = [s.qp_amr{1}(1:end-1);s.qp_amr{2}];
%             H_all  = s.Hermite(qp_all,1);
% %             Zall_1 = ( s.H  \ H_all(1:s.Q,:) );
% %             Zall_2 = ( s.H  \ H_all(s.Q:end,:) );
%             
%             s.Z{1} = ( s.H  \ H_all(1:s.Q,:) );
%             s.Z{2} = ( s.H  \ H_all(s.Q:end,:) );
            
%             for j=1:ratio
%                 s.Z{j}( abs(s.Z{j}) < s.ROUNDOFF_LIMIT ) = 0;
%             end
        end
    
        function plot(s)
            
            clf;
                        
            s.initRefine(2);
            
            % H
            subplot(3,3,1)
            hold on;
            for p=1:s.P
                plot( s.qp, s.H(:,p))                    
            end
            
            subplot(3,3,2)
            hold on;
            for j=1:s.refinementRatio
                for p=1:s.P
                    plot( s.qp_amr{j}, s.H_amr{j}(:,p))                    
                end
            end
            
            subplot(3,3,3)
            hold on;
                
            for p=1:s.P
                plot( (s.qp-1)/2, s.H*s.Z{1}(:,p))
                plot( (s.qp+1)/2, s.H*s.Z{2}(:,p))
            end
            
            % D
            subplot(3,3,4)
            hold on;
            for p=1:s.P
                plot( s.qp, s.D(:,p))                    
            end
            
            subplot(3,3,5)
            hold on;
            for j=1:s.refinementRatio
                for p=1:s.P
                    plot( s.qp_amr{j}, s.D_amr{j}(:,p))                    
                end
            end
            
            subplot(3,3,6)
            hold on;
            for p=1:s.P
                plot( (s.qp-1)/2, s.D * (s.refinementRatio) * s.Z{1}(:,p))          % NB: the refinementRatio is added because the Jacobian is changed by the refinement
                plot( (s.qp+1)/2, s.D * (s.refinementRatio) * s.Z{2}(:,p))
            end

            % DD
            subplot(3,3,7)
            hold on;
            for p=1:s.P
                plot( s.qp, s.DD(:,p))                    
            end  
            
            subplot(3,3,8)
            hold on;
            for j=1:s.refinementRatio
                for p=1:s.P
                    plot( s.qp_amr{j}, s.DD_amr{j}(:,p))                    
                end
            end
            
            subplot(3,3,9)
            hold on;
            for p=1:s.P
                plot( (s.qp-1)/2, s.DD * (s.refinementRatio)^2 * s.Z{1}(:,p))
                plot( (s.qp+1)/2, s.DD * (s.refinementRatio)^2 * s.Z{2}(:,p))                    
            end
            
        end
    end
    
    methods (Access=private)
        
        %% Cubic Hermite
        % Function values
        function out = Hermite(s,x,J)
            %n:order - x:points - J:Jaccobian
            % 161105 modify Jacobian term by KP

            index = s.ind;      % indices for the points
            
            if (nargin<3)
                J     = s.J;        % Jacobian
            end
            
            %J = ((4*sqrt(2)*System.settings.phys.Cn)) / 32;
            
            %J = 0.01;
            
            if nargin==1
                x     = s.qp;       % quadrature points
            end
            
            out = zeros(length(x),length(index));

            for i=1:length(index)
                n = index(i);

                switch(n)
                   case 1
                       y= 1/2-3/4*x+1/4*x.^3; 
                   case 2
                       y=(1/4-1/4*x-1/4*x.^2+1/4*x.^3);  
                       %y=(s.scale/J)*(1/4-1/4*x-1/4*x.^2+1/4*x.^3);
                   case 3
                       y= 1/2+3/4*x-1/4*x.^3; 
                   case 4
                       y= (-1/4-1/4*x+1/4*x.^2+1/4*x.^3);
                       %y= (s.scale/J)*(-1/4-1/4*x+1/4*x.^2+1/4*x.^3);  
                   case 5 
                       y= sqrt(5/128)*(1-x.^2).^2;
                   case 6 
                       y=sqrt(7/128)*(1-x.^2).^2.*x;
                   case 7 
                       y=1/6*sqrt(9/128)*(1-x.^2).^2.*(-7*x.^2 +1);   % NB: the article of Maria (2011) says +7, but -7 is correct
                   case 8 
                       y=1/2*sqrt(11/128)*(1-x.^2).^2.*(3*x.^2-1).*x;
                   case 9 
                       y=1/16*sqrt(13/128)*(1-x.^2).^2.*(33*x.^4-18*x.^2+1);
                   case 10 
                       y=1/48*sqrt(15/128)*(1-x.^2).^2.*(143*x.^4-110*x.^2+15).*x;
                   case 11 
                       y=1/32*sqrt(17/128)*(1-x.^2).^2.*(143*x.^6-143*x.^4+33*x.^2-1);
                   case 12 
                       y=1/32*sqrt(19/128)*(1-x.^2).^2.*(221*x.^6-273*x.^4+91*x.^2-7).*x;
                   case 13 
                       y=1/384*sqrt(21/128)*(1-x.^2).^2.*(4199*x.^8-6188*x.^6+2730*x.^4-364*x.^2+7);        
                end

                out(:,i) = y; % / J;
            end

        end  
        
        % Function first derivatives
        function out = DHermite(s,x)

            index = s.ind;      % indices for the points
            J     = s.J;        % Jacobian
            
            % Alternative scaling
            %J = 1/(4*sqrt(2)*System.settings.phys.Cn);
            %J = 1/((4*sqrt(2)*System.settings.phys.Cn));
                        
            if (nargin==1)
                x     = s.qp;       % quadrature points
            end
            
            out = zeros(length(x),length(index));

            for i=1:length(index)
                n = index(i);

                switch(n)
                    case 1
                       y=-3/4+3/4*x.^2; 
                   case 2
                       y=(-1/4-1/2*x+3/4*x.^2);
                       %y=(s.scale/J)*(-1/4-1/2*x+3/4*x.^2);  
                   case 3
                       y=3/4-3/4*x.^2; 
                   case 4
                       y= (-1/4+1/2*x+3/4*x.^2);
                       %y= (s.scale/J)*(-1/4+1/2*x+3/4*x.^2);  
                   case 5
                       y=sqrt(5/128)*2*(1-x.^2).*(-2*x);
                   case 6 
                       y=sqrt(7/128)*(2*(1-x.^2).*(-2*x.^2) +  (1-x.^2).^2 ); 
                   case 7 
                       y=1/6*sqrt(9/128)* (2*(1-x.^2).*(-2*x).*(-7*x.^2 +1) + (-14*x).* (1-x.^2).^2);
                   case 8 
                       y=1/2*sqrt(11/128)*( 2*(1-x.^2).*(-2*x).*(3*x.^2-1).*x +  (1-x.^2).^2.*(9*x.^2 - 1) );
                   case 9 
                       y=1/16*sqrt(13/128)* ( 2*(1-x.^2).*(-2*x)  .*(33*x.^4-18*x.^2+1) + (1-x.^2).^2.*  (4*33*x.^3-2*18*x)  );
                   case 10 
                       y=1/48*sqrt(15/128)*((1-x.^2).^2.*(715*x.^4-330*x.^2+15) + 4*x.^2.*(x.^2-1).*(143*x.^4-110*x.^2+15) );
                   case 11 
                       y=1/32*sqrt(17/128)*((1-x.^2).^2.*(858*x.^5-572*x.^3+66*x) + 4*x.*(x.^2-1).*(143*x.^6-143*x.^4+33*x.^2-1) );
                   case 12 
                       y=1/32*sqrt(19/128)*((1-x.^2).^2.*(1547*x.^6-1365*x.^4+273*x.^2-7) + 4*x.^2.*(x.^2-1).*(221*x.^6-273*x.^4+91*x.^2-7) );
                   case 13 
                       y=1/384*sqrt(21/128)*(4*x.*(x.^2-1).*(4199*x.^8-6188*x.^6+2730*x.^4-364*x.^2+7) - (1-x.^2).^2.*(-33592*x.^7+37128*x.^5-10920*x.^3+728*x) );
                end
                out(:,i) = y / J;
            end        
        end
        
        % Function second derivatives
        function out = DDHermite(s,x)

            index = s.ind;      % indices for the points
            J     = s.J;        % Jacobian
            
            if (nargin==1)
                x     = s.qp;       % quadrature points
            end
            
            %J = 1/((4*sqrt(2)*System.settings.phys.Cn));
            
            out = zeros(length(x),length(index));

            for i=1:length(index)
                n = index(i);

                switch(n)
                    case 1
                       y=3/2*x; 
                   case 2
                       y=(-1/2+3/2*x); 
                       %y=(s.scale/J)*(-1/2+3/2*x);  
                   case 3
                       y=-3/2*x; 
                   case 4
                       y= (1/2+3/2*x);
                       %y= (s.scale/J)*(1/2+3/2*x);  
                   case 5
                       %y=sqrt(5/128)*2*(-2)*(1-3*x.^2);
                       y=sqrt(5/128)*(12*x.^2-4);
                   case 6 
                       %y=sqrt(7/128)*(2*(-2)*(2*x-4*x.^3) + 2*(1-x.^2).*(-2*x) ); 
                       y=sqrt(7/128)*(12*x.*(x.^2-1) + 8*x.^3 );
                   case 7 
                       %y=1/6*sqrt(9/128)* (-18+180*x.^2-210*x.^4);
                       y=1/6*sqrt(9/128)* ((-12*x.^2+4).*(7*x.^2-1)-112*(x.^2-1).*x.^2 - 14*(x.^2-1).^2 );
                   case 8  
                       %y=1/2*sqrt(11/128)*( (4*33 +9)*x.^6 - 4*33*x.^5 + (-4*18 -18 -1)*x.^4 + 4*18 * x.^3 + (-4+9+2)*x.^2-4*x-1   );
                       y=1/2*sqrt(11/128)*(18*x.*(1-x.^2).^2 + 48*x.^3.*(x.^2-1) + 8*x.^3.*(3*x.^2-1) + 12*x.*(x.^2-1).*(3*x.^2-1) );
                   case 9 
                       %y=1/16*sqrt(13/128)* ( 2*(1-x.^2).*(-2*x)  .*(33*x.^4-18*x.^2+1) + (1-x.^2).^2.*  (4*33*x.^3-2*18*x)  );
                       y=1/16*sqrt(13/128)*((12*x.^2-4).*(33*x.^4-18*x.^2+1) + (1-x.^2).^2.*(396*x.^2-36) - 8*x.*(x.^2-1).*(-132*x.^3+36*x) );
                    case 10 
                       %y=1/48*sqrt(15/128)*(1-x.^2).^2.*(143*x.^4-110*x.^2+15).*x;
                       y=1/48*sqrt(15/128)*((1-x.^2).^2.*(2860*x.^3-660*x) + (x.^2-1).*(6292*x.^5-3080*x.^3+180*x) + 8*x.^3.*(143*x.^4-110*x.^2+15) );
                   case 11 
                       %y=1/32*sqrt(17/128)*(1-x.^2).^2.*(143*x.^6-143*x.^4+33*x.^2-1);
                       y=1/32*sqrt(17/128)*((x.^2-1).*(7440*x.^6-5148*x.^4+660*x.^2-4) + (1-x.^2).^2.*(4290*x.^4-1716*x.^2+66) + 8*x.^2.*(143*x.^6-143*x.^4+33*x.^2-1) );
                   case 12 
                       %y=1/32*sqrt(19/128)*(1-x.^2).^2.*(221*x.^6-273*x.^4+91*x.^2-7).*x;
                       y=1/32*sqrt(19/128)*((x.^2-1).*(13260*x.^7-12012*x.^5+2548*x.^3-84*x) + (1-x.^2).^2.*(9282*x.^5-5460*x.^3+546*x) + 8*x.^3.*(221*x.^6-273*x.^4+91*x.^2-7) );
                   case 13 
                       %y=1/384*sqrt(21/128)*(1-x.^2).^2.*(4199*x.^8-6188*x.^6+2730*x.^4-364*x.^2+7);
                       y=1/384*sqrt(21/128)*(8*x.^2.*(4199*x.^8-6188*x.^6+2730*x.^4-364*x.^2+7) + (x.^2-1).*(285532*x.^8-321776*x.^6+98280*x.^4-7280*x.^2+28) + (1-x.^2).^2.*(235144*x.^6-185640*x.^4+32760*x.^2-728) );
                end
                %out(:,i) = y;
                out(:,i) = y / J;
            end
        end
        
        % Function third derivatives
        function out = DDDHermite(s,x)

            index = s.ind;      % indices for the points
            J     = s.J;        % Jacobian
            
            if (nargin==1)
                x     = s.qp;       % quadrature points
            end
            
            out = zeros(length(x),length(index));

            for i=1:length(index)
                n = index(i);

                switch(n)
                    case 1
                       y=3/2; 
                   case 2
                       y=3/2; 
                   case 3
                       y=-3/2; 
                   case 4
                       y=3/2;
                   case 5
                       y=sqrt(5/128)*24*x;
                   case 6 
                       error('Not changed yet')
                       y=sqrt(7/128)*(12*x.*(x.^2-1) + 8*x.^3 );
                   case 7 
                       %y=1/6*sqrt(9/128)* (-18+180*x.^2-210*x.^4);
                       y=1/6*sqrt(9/128)* ((-12*x.^2+4).*(7*x.^2-1)-112*(x.^2-1).*x.^2 - 14*(x.^2-1).^2 );
                   case 8  
                       %y=1/2*sqrt(11/128)*( (4*33 +9)*x.^6 - 4*33*x.^5 + (-4*18 -18 -1)*x.^4 + 4*18 * x.^3 + (-4+9+2)*x.^2-4*x-1   );
                       y=1/2*sqrt(11/128)*(18*x.*(1-x.^2).^2 + 48*x.^3.*(x.^2-1) + 8*x.^3.*(3*x.^2-1) + 12*x.*(x.^2-1).*(3*x.^2-1) );
                   case 9 
                       %y=1/16*sqrt(13/128)* ( 2*(1-x.^2).*(-2*x)  .*(33*x.^4-18*x.^2+1) + (1-x.^2).^2.*  (4*33*x.^3-2*18*x)  );
                       y=1/16*sqrt(13/128)*((12*x.^2-4).*(33*x.^4-18*x.^2+1) + (1-x.^2).^2.*(396*x.^2-36) - 8*x.*(x.^2-1).*(-132*x.^3+36*x) );
                    case 10 
                       %y=1/48*sqrt(15/128)*(1-x.^2).^2.*(143*x.^4-110*x.^2+15).*x;
                       y=1/48*sqrt(15/128)*((1-x.^2).^2.*(2860*x.^3-660*x) + (x.^2-1).*(6292*x.^5-3080*x.^3+180*x) + 8*x.^3.*(143*x.^4-110*x.^2+15) );
                   case 11 
                       %y=1/32*sqrt(17/128)*(1-x.^2).^2.*(143*x.^6-143*x.^4+33*x.^2-1);
                       y=1/32*sqrt(17/128)*((x.^2-1).*(7440*x.^6-5148*x.^4+660*x.^2-4) + (1-x.^2).^2.*(4290*x.^4-1716*x.^2+66) + 8*x.^2.*(143*x.^6-143*x.^4+33*x.^2-1) );
                   case 12 
                       %y=1/32*sqrt(19/128)*(1-x.^2).^2.*(221*x.^6-273*x.^4+91*x.^2-7).*x;
                       y=1/32*sqrt(19/128)*((x.^2-1).*(13260*x.^7-12012*x.^5+2548*x.^3-84*x) + (1-x.^2).^2.*(9282*x.^5-5460*x.^3+546*x) + 8*x.^3.*(221*x.^6-273*x.^4+91*x.^2-7) );
                   case 13 
                       %y=1/384*sqrt(21/128)*(1-x.^2).^2.*(4199*x.^8-6188*x.^6+2730*x.^4-364*x.^2+7);
                       y=1/384*sqrt(21/128)*(8*x.^2.*(4199*x.^8-6188*x.^6+2730*x.^4-364*x.^2+7) + (x.^2-1).*(285532*x.^8-321776*x.^6+98280*x.^4-7280*x.^2+28) + (1-x.^2).^2.*(235144*x.^6-185640*x.^4+32760*x.^2-728) );
                end
                out(:,i) = y;
            end
        end
        
        %% Quintic Hermite
        % Function values for quintic Hermite
        function out = Hermite5(s,x,J)
            %n:order - x:points - J:Jaccobian

            index = s.ind;      % indices for the points
            
            if (nargin<3)
                J     = s.J;        % Jacobian
            end
            
            if nargin==1
                x     = s.qp;       % quadrature points
            end
            
            out = zeros(length(x),length(index));

            for i=1:length(index)
                n = index(i);

                switch(n)
                   case 1
                       y= ( -3*x.^5 + 10*x.^3 - 15*x + 8 ) / 16;
                   case 2
                       y= ( -3*x.^5 + x.^4 + 10*x.^3 - 6*x.^2 - 7*x + 5 ) / 16;
                   case 3
                       y= (  3*x.^5 - 10*x.^3 + 15*x + 8 ) / 16;                   	% 1/2+3/4*x-1/4*x.^3; 
                   case 4
                       y= ( -3*x.^5 - x.^4 + 10*x.^3 + 6*x.^2 - 7*x - 5 ) / 16;   	% (-1/4-1/4*x+1/4*x.^2+1/4*x.^3);
                   case 5 
                       y= ( -x.^5 + x.^4 + 2*x.^3 - 2*x.^2 - x + 1 ) / 16;          % sqrt(5/128)*(1-x.^2).^2;
                   case 6 
                       y= (  x.^5 + x.^4 - 2*x.^3 - 2*x.^2 + x + 1 ) / 16;          % sqrt(7/128)*(1-x.^2).^2.*x;
                   case 7 
                       y=1/6*sqrt(9/128)*(1-x.^2).^2.*(-7*x.^2 +1);   % NB: the article of Maria (2011) says +7, but -7 is correct
                   case 8 
                       y=1/2*sqrt(11/128)*(1-x.^2).^2.*(3*x.^2-1).*x;
                   case 9 
                       y=1/16*sqrt(13/128)*(1-x.^2).^2.*(33*x.^4-18*x.^2+1);
                   case 10 
                       y=1/48*sqrt(15/128)*(1-x.^2).^2.*(143*x.^4-110*x.^2+15).*x;
                   case 11 
                       y=1/32*sqrt(17/128)*(1-x.^2).^2.*(143*x.^6-143*x.^4+33*x.^2-1);
                   case 12 
                       y=1/32*sqrt(19/128)*(1-x.^2).^2.*(221*x.^6-273*x.^4+91*x.^2-7).*x;
                   case 13 
                       y=1/384*sqrt(21/128)*(1-x.^2).^2.*(4199*x.^8-6188*x.^6+2730*x.^4-364*x.^2+7);   
                end

                out(:,i) = y; % / J;
            end

        end  
        
        % Function first derivatives for quintic Hermite
        function out = DHermite5(s,x)

            index = s.ind;      % indices for the points
            J     = s.J;        % Jacobian
            
            % Alternative scaling
            %J = 1/(4*sqrt(2)*System.settings.phys.Cn);
            %J = 1/((4*sqrt(2)*System.settings.phys.Cn));
                        
            if (nargin==1)
                x     = s.qp;       % quadrature points
            end
            
            out = zeros(length(x),length(index));

            for i=1:length(index)
                n = index(i);

                switch(n)
                   case 1
                       y= ( -15*x.^4 + 30*x.^2 - 15 ) / 16;
                   case 2
                       y= ( -15*x.^4 + 4*x.^3 + 30*x.^2 - 12*x - 7 ) / 16;
                   case 3
                       y= (  15*x.^4 - 30*x.^2 + 15 ) / 16;
                   case 4
                       y= ( -15*x.^4 - 4*x.^3 + 30*x.^2 + 12*x - 7 ) / 16;
                   case 5 
                       y= ( -5*x.^4 + 4*x.^3 + 6*x.^2 - 4*x - 1 ) / 16;
                   case 6 
                       y= (  5*x.^4 + 4*x.^3 - 6*x.^2 - 4*x + 1 ) / 16;
                   case 7 
                       y=1/6*sqrt(9/128)* (2*(1-x.^2).*(-2*x).*(-7*x.^2 +1) + (-14*x).* (1-x.^2).^2);
                   case 8 
                       y=1/2*sqrt(11/128)*( 2*(1-x.^2).*(-2*x).*(3*x.^2-1).*x +  (1-x.^2).^2.*(9*x.^2 - 1) );
                   case 9 
                       y=1/16*sqrt(13/128)* ( 2*(1-x.^2).*(-2*x)  .*(33*x.^4-18*x.^2+1) + (1-x.^2).^2.*  (4*33*x.^3-2*18*x)  );
                   case 10 
                       y=1/48*sqrt(15/128)*((1-x.^2).^2.*(715*x.^4-330*x.^2+15) + 4*x.^2.*(x.^2-1).*(143*x.^4-110*x.^2+15) );
                   case 11 
                       y=1/32*sqrt(17/128)*((1-x.^2).^2.*(858*x.^5-572*x.^3+66*x) + 4*x.*(x.^2-1).*(143*x.^6-143*x.^4+33*x.^2-1) );
                   case 12 
                       y=1/32*sqrt(19/128)*((1-x.^2).^2.*(1547*x.^6-1365*x.^4+273*x.^2-7) + 4*x.^2.*(x.^2-1).*(221*x.^6-273*x.^4+91*x.^2-7) );
                   case 13 
                       y=1/384*sqrt(21/128)*(4*x.*(x.^2-1).*(4199*x.^8-6188*x.^6+2730*x.^4-364*x.^2+7) - (1-x.^2).^2.*(-33592*x.^7+37128*x.^5-10920*x.^3+728*x) );
                end
                out(:,i) = y / J;
            end        
        end
        
        % Function first derivatives for quintic Hermite
        function out = DDHermite5(s,x)

            index = s.ind;      % indices for the points
            J     = s.J;        % Jacobian
            
            % Alternative scaling
            %J = 1/(4*sqrt(2)*System.settings.phys.Cn);
            %J = 1/((4*sqrt(2)*System.settings.phys.Cn));
                        
            if (nargin==1)
                x     = s.qp;       % quadrature points
            end
            
            out = zeros(length(x),length(index));

            for i=1:length(index)
                n = index(i);

                switch(n)
                   case 1
                       y= ( -15*x.^3 + 15*x ) / 4;
                   case 2
                       y= ( -15*x.^3 + 3*x.^2 + 15*x - 3 ) / 4;
                   case 3
                       y= (  15*x.^3 - 15*x ) / 4;
                   case 4
                       y= ( -15*x.^3 - 3*x.^2 + 15*x + 3 ) / 4;
                   case 5 
                       y= ( -5*x.^3 + 3*x.^2 + 3*x - 1 ) / 4;
                   case 6 
                       y= (  5*x.^3 + 3*x.^2 - 3*x - 1 ) / 4;
                   case 7 
                       %y=1/6*sqrt(9/128)* (-18+180*x.^2-210*x.^4);
                       y=1/6*sqrt(9/128)* ((-12*x.^2+4).*(7*x.^2-1)-112*(x.^2-1).*x.^2 - 14*(x.^2-1).^2 );
                   case 8  
                       %y=1/2*sqrt(11/128)*( (4*33 +9)*x.^6 - 4*33*x.^5 + (-4*18 -18 -1)*x.^4 + 4*18 * x.^3 + (-4+9+2)*x.^2-4*x-1   );
                       y=1/2*sqrt(11/128)*(18*x.*(1-x.^2).^2 + 48*x.^3.*(x.^2-1) + 8*x.^3.*(3*x.^2-1) + 12*x.*(x.^2-1).*(3*x.^2-1) );
                   case 9 
                       %y=1/16*sqrt(13/128)* ( 2*(1-x.^2).*(-2*x)  .*(33*x.^4-18*x.^2+1) + (1-x.^2).^2.*  (4*33*x.^3-2*18*x)  );
                       y=1/16*sqrt(13/128)*((12*x.^2-4).*(33*x.^4-18*x.^2+1) + (1-x.^2).^2.*(396*x.^2-36) - 8*x.*(x.^2-1).*(-132*x.^3+36*x) );
                    case 10 
                       %y=1/48*sqrt(15/128)*(1-x.^2).^2.*(143*x.^4-110*x.^2+15).*x;
                       y=1/48*sqrt(15/128)*((1-x.^2).^2.*(2860*x.^3-660*x) + (x.^2-1).*(6292*x.^5-3080*x.^3+180*x) + 8*x.^3.*(143*x.^4-110*x.^2+15) );
                   case 11 
                       %y=1/32*sqrt(17/128)*(1-x.^2).^2.*(143*x.^6-143*x.^4+33*x.^2-1);
                       y=1/32*sqrt(17/128)*((x.^2-1).*(7440*x.^6-5148*x.^4+660*x.^2-4) + (1-x.^2).^2.*(4290*x.^4-1716*x.^2+66) + 8*x.^2.*(143*x.^6-143*x.^4+33*x.^2-1) );
                   case 12 
                       %y=1/32*sqrt(19/128)*(1-x.^2).^2.*(221*x.^6-273*x.^4+91*x.^2-7).*x;
                       y=1/32*sqrt(19/128)*((x.^2-1).*(13260*x.^7-12012*x.^5+2548*x.^3-84*x) + (1-x.^2).^2.*(9282*x.^5-5460*x.^3+546*x) + 8*x.^3.*(221*x.^6-273*x.^4+91*x.^2-7) );
                   case 13 
                       %y=1/384*sqrt(21/128)*(1-x.^2).^2.*(4199*x.^8-6188*x.^6+2730*x.^4-364*x.^2+7);
                       y=1/384*sqrt(21/128)*(8*x.^2.*(4199*x.^8-6188*x.^6+2730*x.^4-364*x.^2+7) + (x.^2-1).*(285532*x.^8-321776*x.^6+98280*x.^4-7280*x.^2+28) + (1-x.^2).^2.*(235144*x.^6-185640*x.^4+32760*x.^2-728) );
                end
                out(:,i) = y / J;
            end        
        end
        
    end
end

