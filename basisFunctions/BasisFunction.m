%*****************************
% 02 July 2019 - Marcel Kwakkel
%*****************************

classdef BasisFunction < handle

  properties (Access = protected, Constant)
    ROUNDOFF_LIMIT = 1e-13;
  end % properties

  properties
    type;               % Basis function type
    C;                  % Continuity
    P, Q;

    boundDoF;
    numBoundDof;
    dofType;
    dofID; dofID2;

    dof;                % number of modes (dof)

    dofPattern;

    %% Quadrature properties
    dofq;                           % number of quadrature nodes
    qdofPattern;
    qdofType;
    qp, qw;                         % quadrature points & weights
    qdofID;

    H, D;                           % 1D operators: value and 1st derivative 
    DD;
    DDD;

    % AMR properties
    refinementRatio = 1;
    Z;

    H_amr, D_amr;
  end

  properties (Access=protected)
    J;                  % Jacobian
    ind;                % indices for polynomial

    qp_amr, qw_amr;
    %         H_amr, D_amr;
  end

  methods 
    function s = BasisFunction(P,Q,C,functionType)

      if nargin==0
        %                 disp('Default parameters are used: Lagrange functions with P=4, Q=5 and C=0')
        %                 functionType = FunctionType.Lagrange;
        %                 P = 4; Q = 5; C = 0;
        return
      end

      s.P     = P;
      s.Q     = Q;
      s.C     = C;

      s.numBoundDof(1) = C+1;
      s.numBoundDof(2) = C+1;

      s.boundDoF{1} = 1:C+1;
      s.boundDoF{2} = P-C:P;

      s.dofType = false(s.P,1);
      s.dofType( s.boundDoF{1} ) = true; % start vertex
      s.dofType( s.boundDoF{2} ) = true; % end vertex

      s.dofID{ 1 } = s.boundDoF{ 1 };
      if ( (s.P - s.numBoundDof(1) - s.numBoundDof(2)) > 0 )
        s.dofID{ 2 } = (s.numBoundDof(1)+1) : (s.P-s.numBoundDof(2));
      else
        s.dofID{ 2 } = [];
      end
      s.dofID{ 3 } = s.boundDoF{ 2 };

      s.dofPattern{1} = false(1,P);
      s.dofPattern{1}(1:s.numBoundDof(1)) = true;
      s.dofPattern{2} = false(1,P);
      s.dofPattern{2}(P+1-s.numBoundDof(1):P) = true;


      %% Quadrature parameters
      s.qdofType = false(s.Q,1);
      s.qdofType(  1  ) = true;
      s.qdofType( end ) = true;

      s.qdofID{ 1 } = 1;
      if ( (s.Q - 2) > 0 )
        s.qdofID{ 2 } = 2 : (s.Q-1);
      else
        s.qdofID{ 2 } = [];
      end
      s.qdofID{ 3 } = s.Q;

      s.qdofPattern{1} = false(1,Q);
      s.qdofPattern{1}( 1 ) = true;
      s.qdofPattern{2} = false(1,Q);
      s.qdofPattern{2}( Q ) = true;


      s.dof   = P;
      s.dofq  = Q;

      % Quadrature points & weights
      [s.qp,s.qw] = GLL(Q,-1,1);

      % Jacobian, for a -1 to 1 domain it's by definition 1
      s.J = 1;

      if nargin==4
        s.type  = functionType;

        switch functionType
          case FunctionType.Lagrange
            if (C==0)
              % Lagrange C0
              s.ind = 1:1:s.dof;

              for j=1:s.dof
                s.H(:,j) = LagrangeGLL( s.ind(j), s.qp, s.dof );
              end
              s.H( abs(s.H) < s.ROUNDOFF_LIMIT ) = 0;
              s.D  = LagrangeDerivativeMatrix_GLL2( length(s.qp)-1, s.dof-1 );
              s.D( abs(s.D) < s.ROUNDOFF_LIMIT ) = 0;
              s.DD = [];
            end

          case FunctionType.Hermite
            if (C==1)
              % Hermite C1
              s.ind = [1 2 5:1:s.dof 3 4];

              s.H   = HermiteN  ( s.ind, s.qp, J );
              s.D   = DHermiteN ( s.ind, s.qp, J );
              s.DD  = DDHermiteN( s.ind, s.qp, J );
              s.DDD = DDDHermiteN( s.ind, s.qp, J );

            elseif (C==2)

              % Hermite C2
              s.ind = [1 2 3 7:1:s.dof 4 5 6];

              s.H   = Hermite2  ( s.ind, s.qp, J );
              s.D   = DHermite2 ( s.ind, s.qp, J );
              s.DD  = DDHermite2( s.ind, s.qp, J );
            end
        end
      end
    end

    function initRefine(s,ratio)

      s.refinementRatio = ratio;

      s.qp_amr = cell(ratio,1);
      s.qw_amr = cell(ratio,1);

      s.H_amr  = cell(ratio,1);

      s.Z      = cell(ratio,1);

      for j=1:ratio
        x0 = -1 + 2/ratio * (j-1);
        x1 = -1 + 2/ratio *   j;

        [s.qp_amr{j},s.qw_amr{j}] = GLL(s.Q,x0,x1);

        %                 s.H_amr{j}  = s.Hermite(s.qp_amr{j},1);
        %                 s.D_amr{j}  = s.DHermite(s.qp_amr{j});
        %                 s.DD_amr{j} = s.DDHermite(s.qp_amr{j});

        for k=1:s.dof
          s.H_amr{j}(:,k) = LagrangeGLL( s.ind(k), s.qp_amr{j}, s.dof );
        end
        %s.D_amr{j} = LagrangeDerivativeMatrix_GLL2( length(s.qp_amr{j})-1, s.dof-1 );

        s.Z{j}  = ( s.H  \ s.H_amr{j} );

        s.Z{j}( abs(s.Z{j}) < s.ROUNDOFF_LIMIT ) = 0;
      end
    end
  end    
end