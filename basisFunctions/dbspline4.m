function y = dbspline4( x )
    % B-spline function of 4th order

    y = zeros(size(x));
    x0range = abs(x) < 0.5;
    x1range = abs(x)>=0.5 & abs(x)<1.5;
    x2range = abs(x)>=1.5 & abs(x)<2.5;
    x0 = x(x0range);
    x1 = x(x1range);
    x2 = x(x2range);
    y(x0range) = (1/24) * (24*x0.^3 - 30*x0); % + 14.375);
    y(x1range) = (1/24) * sign(x1) .* (-16*abs(x1).^3 + 60*abs(x1).^2 - 60*abs(x1) + 5);
    y(x2range) = (1/24) * sign(x2) .* (4*abs(x2).^3 - 30*abs(x2).^2 + 75*abs(x2) - 62.5);
end