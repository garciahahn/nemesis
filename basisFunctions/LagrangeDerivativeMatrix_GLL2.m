function D = LagrangeDerivativeMatrix_GLL2(Q,P)

% This method gives the matrix of the lagrange derivatives l'_j(x_i) 
% for i=1:Q+1 and j=1:P+1
%
% authors: Marcel Kwakkel, October 2017

if nargin==1
    P = Q;
end

D = zeros(Q+1,P+1);
[GLLPointsQ,GLLWeights]=GaussLobattoLegendre(Q+1);
[GLLPointsP,GLLWeights]=GaussLobattoLegendre(P+1);

for i=1:Q+1
  for j=1:P+1
      xiq = GLLPointsQ(i);
      xip = GLLPointsP(j);
      
        g = (1-xiq^2) * LegendreDerivative(P,xiq);
%         gprimeQ = -2*xiq*LegendreDerivative(P,xiq) - (P)*(P+1)*LegendreL(P,xiq) + 2*xiq*LegendreDerivative(P,xiq);
%         gprimeP = -2*xip*LegendreDerivative(P,xip) - (P)*(P+1)*LegendreL(P,xip) + 2*xip*LegendreDerivative(P,xip);
        gprimeQ = -P*(P+1)*LegendreL(P,xiq);
        gprimeP = -P*(P+1)*LegendreL(P,xip);
    
        if (abs(xiq-xip)<1e-14)
            D(i,j) = 0;
        else
            D(i,j) = (gprimeQ * (xiq-xip) - g ) / (gprimeP * (xiq-xip)^2 );
        end
        
%     if GLLPointsQ(i)==GLLPointsP(j)
%       D(i,j)=0;
%     else
%       D(i,j) = LegendreL(Q,GLLPointsQ(i))/(LegendreL(P,GLLPointsP(j))*(GLLPointsP(j)-GLLPointsQ(i)));      
%     end
  end
end

D(1,1) = -(P+1)*P/4;
D(Q+1,P+1) = (P+1)*P/4;
    
