set xrange [-1:1]
plot 1-10*(0.5*(x+1))**3+15*(0.5*(x+1))**4-6*(0.5*(x+1))**5, \
     (0.5*(x+1))-6*(0.5*(x+1))**3+8*(0.5*(x+1))**4-3*(0.5*(x+1))**5, \
     0.5*(0.5*(x+1))**2-1.5*(0.5*(x+1))**3+1.5*(0.5*(x+1))**4-0.5*(0.5*(x+1))**5, \
     0.5-3./4*x+1./4*x**3 t 'h3: v1', \
     1./32 * ( -3*x**5 + x**4 + 10*x**3 - 6*x**2 - 7*x + 5 ) t 'h5: v2',\
     1./16 * ( -x**5 + x**4 + 2*x**3 - 2*x**2 - x + 1 ) t 'h5: v5', \
     1./4 * ( -5*x**3+3*x**2+3*x-1 ) t 'h5: v5dd', \
     sqrt(7./128)*(1-x**2)**2*x, \
     1./64 * ( x**5 + x**4 - 2*x**3 - 2*x**2 + x + 1 );
