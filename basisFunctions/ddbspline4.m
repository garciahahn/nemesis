function y = ddbspline4( x )
    % B-spline function of 4th order

    y = zeros(size(x));
    x0range = abs(x) < 0.5;
    x1range = abs(x)>=0.5 & abs(x)<1.5;
    x2range = abs(x)>=1.5 & abs(x)<2.5;
    x0 = x(x0range);
    x1 = x(x1range);
    x2 = x(x2range);
    y(x0range) = (1/24) * (72*x0.^2 - 30); % + 14.375);
    y(x1range) = (1/24) * (-48*abs(x1).^2 + 120*abs(x1) - 60);
    y(x2range) = (1/24) * (12*abs(x2).^2 - 60*abs(x2) + 75);
end