classdef BSpline < BasisFunction
    %HERMITE1 Hermite basis function with C1
    %   On boundaries between elements this basis function has continuous
    %   values and first derivatives
    
    % bsplines from : https://se.mathworks.com/matlabcentral/fileexchange/27047-b-spline-tools?focused=5189377&tab=function
    
    properties
%         DD;          % 1D operator: 2nd derivative
        DD_amr;
%         Z2;
    end
    
    methods
        function s = BSpline(P,Q,C)
            %UNTITLED6 Construct an instance of this class
            %   Detailed explanation goes here

            s = s@BasisFunction(P,Q,C);
                        
            s.type = FunctionType.BSpline;
            
            % Quadrature points & weights
            %s.qp = linspace(-1,1,Q)';
            
%             s.ind = [ 1:C 2*C+1:1:P 2*C:-1:C+1 ];
            s.ind = [ 1:s.dof ];
            
            %s.dof = P;
            
            %s.numBoundDof(1) = C+1;
            %s.numBoundDof(2) = C+1;
            
            [s.H,s.D,s.DD] = s.setBSpline(s.qp);
        end
        
        function initRefine(s,ratio)
            
            s.refinementRatio = ratio;
            
            s.qp_amr = cell(ratio,1);
            s.qw_amr = cell(ratio,1);
            
            s.H_amr  = cell(ratio,1);
            
            s.Z      = cell(ratio,1);
            
            if (ratio==1)
                s.Z{1} = speye( s.dof );
            else
                for j=1:ratio
                    x0 = -1 + 2/ratio * (j-1);
                    x1 = -1 + 2/ratio * j;

                    [s.qp_amr{j},s.qw_amr{j}] = GLL(s.Q,x0,x1);

                    %temp =  ratio* ( s.qp_amr{j} - 0.5*(s.qp_amr{j}(end)+s.qp_amr{j}(1)) );

                    [s.H_amr{j}, s.D_amr{j}, s.DD_amr{j}] = s.setBSpline( s.qp_amr{j} );

                    s.Z{j} = (s.H \ s.H_amr{j});

    %                 s.Z2{j} = (s.D \ s.D_amr{j});

    %                 Hfine = s.H_amr{j}(:,3:6);
    %                 
    %                 s.H\Hfine

                    s.Z{j}( abs(s.Z{j}) < s.ROUNDOFF_LIMIT ) = 0;
    %                 s.Z2{j}( abs(s.Z2{j}) < s.ROUNDOFF_LIMIT ) = 0;
                end
            end
            
%             if (ratio==1)
%                 qp_all = s.qp_amr{1}(1:end);
%             elseif (ratio==2)
%                 qp_all = [ s.qp_amr{1}(1:end-1); s.qp_amr{2} ];
%             else
%                 error('unsupported ratio')
%             end
%             H_all  = s.setBSpline(qp_all);
%             Zall_1 = ( s.H  \ H_all(1:s.Q,:) );
%             if ratio>1
%                 Zall_2 = ( s.H  \ H_all(s.Q:end,:) );
%             end
            
%             s.Z{1} = ( s.H  \ H_all(1:s.Q,:) );
%             s.Z{2} = ( s.H  \ H_all(s.Q:end,:) );
            
%             s.Z{1}(:,1)     = 0.5 * s.Z{1}(:,1);
%             if (ratio>1)
%                 s.Z{2}(:,end)   = 0.5 * s.Z{2}(:,end);
%             end
        end
        
        function plot(s)
            
            clf;
                        
            s.initRefine(2);
            
            % H
            subplot(3,3,1)
            hold on;
            for p=1:s.P
                plot( s.qp, s.H(:,p))                    
            end
            
            subplot(3,3,2)
            hold on;
            for j=1:s.refinementRatio
                for p=1:s.P
                    plot( s.qp_amr{j}, s.H_amr{j}(:,p))                    
                end
            end
            
            subplot(3,3,3)
            hold on;               
            for p=1:s.P
                plot( (s.qp-1)/2, s.H*s.Z{1}(:,p))
                plot( (s.qp+1)/2, s.H*s.Z{2}(:,p))
            end
            
            % D
            subplot(3,3,4)
            hold on;
            for p=1:s.P
                plot( s.qp, s.D(:,p))                    
            end
            
            subplot(3,3,5)
            hold on;
            for j=1:s.refinementRatio
                for p=1:s.P
                    plot( s.qp_amr{j}, s.D_amr{j}(:,p))                    
                end
            end
            
            subplot(3,3,6)
            hold on;               
            for p=1:s.P
                plot( (s.qp-1)/2, s.D*s.Z{1}(:,p))
                plot( (s.qp+1)/2, s.D*s.Z{2}(:,p))
            end

            % DD
            subplot(3,3,7)
            hold on;
            for p=1:s.P
                plot( s.qp, s.DD(:,p))                    
            end  
            
            subplot(3,3,8)
            hold on;
            for j=1:s.refinementRatio
                for p=1:s.P
                    plot( s.qp_amr{j}, s.DD_amr{j}(:,p))                    
                end
            end
            
            subplot(3,3,9)
            hold on;               
            for p=1:s.P
                plot( (s.qp-1)/2, s.DD*s.Z{1}(:,p))
                plot( (s.qp+1)/2, s.DD*s.Z{2}(:,p))
            end
            
        end
    end
    
    methods (Access=private)
                
        function [H,D,DD] = setBSpline(s,x)
            H   = s.d0_BSpline(x);
            D   = s.d1_BSpline(x);
            DD  = s.d2_BSpline(x);
            
            H   = fliplr(H);
            D   = fliplr(D);
            DD  = fliplr(DD);
        end
        
        % Function values
        function out = d0_BSpline(s,x,J)            
            if (nargin<3)
                J     = s.J;        % Jacobian
            end
            
            if nargin==1
                x     = s.qp;       % quadrature points
            end
                        
            C = s.C+1; P = s.P;            
            
            % If the polynomial order is higher than 2*C, there are 
            % 'numInternal' modes which are completely within the element 
            % limits. These internal modes reduce the width of each 
            % polynomial. The knotLocations are scaled in such a way that
            % the 2nd and P-1 th positions match the -/+ invWidth. By doing
            % this, the corresponding modes have their maximum values at
            % the left/right boundaries. Scaling with (C+1)/2 is required
            % because of the definition of the bspline functions.
            
            numInternal     = P - 2*C;
            width           = (C+1)/2 * (1 - 2*(numInternal-1)/P); 
            invWidth        = (C+1)/2 / width;
            knotLocations   = linspace(-(P-1)/(P-C)*invWidth,(P-1)/(P-C)*invWidth,P);

            if (C==3)                
                for i=1:P
                    H(:,i) = bspline3( (C+1)/2 * ( invWidth*x + knotLocations(i) ) )';
                end
            elseif (C==4)                
                for i=1:P
                    H(:,i) = bspline4( (C+1)/2 * ( invWidth*x + knotLocations(i) ) )';
                end
            end
            
            out = H;
        end  
        
        % Function values
        function out = d1_BSpline(s,x,J)
            %n:order - x:points - J:Jaccobian
            % 161105 modify Jacobian term by KP

            index = s.ind;      % indices for the points
            
            if (nargin<3)
                J     = s.J;        % Jacobian
            end
            
            if nargin==1
                x     = s.qp;       % quadrature points
            end
            
            C = s.C+1; P = s.P;
            
            numInternal     = P - 2*C;
            width           = (C+1)/2 * (1 - 2*(numInternal-1)/P);
            invWidth        = (C+1)/2 / width;
            knotLocations   = linspace(-(P-1)/(P-C)*invWidth,(P-1)/(P-C)*invWidth,P);
            

            if (C==3)
                for i=1:P
                    D(:,i) = dbspline3( (C+1)/2 * ( invWidth*x + knotLocations(i) ) )';
                end
            elseif (C==4)
                for i=1:P
                    D(:,i) = dbspline4( (C+1)/2 * ( invWidth*x + knotLocations(i) ) )';
                end
            end
            
            out = D * invWidth * (C+1)/2;
        end
        
       % Function values
        function out = d2_BSpline(s,x,J)
            %n:order - x:points - J:Jaccobian
            % 161105 modify Jacobian term by KP

            index = s.ind;      % indices for the points
            
            if (nargin<3)
                J     = s.J;        % Jacobian
            end
            
            if nargin==1
                x     = s.qp;       % quadrature points
            end
            
            out = zeros( numel(x), s.P );
            
            C = s.C+1;
            P = s.P;
            
            numInternal     = P - 2*C;
            width           = (C+1)/2 * (1 - 2*(numInternal-1)/P);
            invWidth        = (C+1)/2 / width;
            knotLocations   = linspace(-(P-1)/(P-C)*invWidth,(P-1)/(P-C)*invWidth,P);
            
            if (C==3)
                for i=1:P
                    out(:,i) = ddbspline3( (C+1)/2 * ( invWidth*x + knotLocations(i) ) )';
                end
                
            elseif (C==4)
                for i=1:P
                    out(:,i) = ddbspline4( (C+1)/2 * ( invWidth*x + knotLocations(i) ) )';
                end
            end
            
            % Adjust the scaling of the double derivative
            out = out * ( invWidth * (C+1)/2 )^2;
        end
        
    end
end

