function write_vtu( dataGroup, iter, time, rank, nproc )
    %
    % This programs writes vtu (VTK binary unstructed data file)
    % files produced by LSQDIM. The vtu file can then be visualized in
    % VTK/ParaView. See http://www.vtk.org and http://www.paraview.org for details.
    %
    % There are 3 types of files created by this function: 
    %
    %   1) PVD-file     :: collects the PVTU files for all time steps & all ranks (written by rank==0)
    %   2) PVTU-file    :: collects the VTU files for one time step & all ranks (written by rank==0)
    %   3) VTU-file     :: solution for 1 rank at 1 time step (written by all ranks)
    %
    % The following parameters should be given (none optional):
    %
    %   dataGroup       :: FieldGroup object
    %   iter            :: the current iteration number
    %   time            :: the current physical time
    %   rank            :: the rank number of this processor (MPI)
    %   nproc           :: total number of processors (MPI)
    %
    
    %% Create an instant dataGroup when a (sub)Field is provided directly
    if ~isa(dataGroup,'FieldGroup')
        tmp_field = dataGroup;
        dataGroup = FieldGroup(1,tmp_field.fullname);        
        dataGroup.add(tmp_field,0,tmp_field.fullname);
    else
        dataGroup.unifyMesh;
    end
    
    %% Development flags
    verbose         = false;
    %debug           = false;
    timing          = false;
        
    if (timing)
        t0 = toc; %#ok<UNRCH>
    end
    
    %% Resolution ( 2 = all subelements of the quadrature grid are written to the VTU file )
    out_res         = 2;
    
    %% Sizes
    SIZE_REAL       = 4;
    %SIZE_DOUBLE     = 8;
    SIZE_INTEGER    = SIZE_REAL;
    %SIZE_LOGICAL    = SIZE_REAL;
    SIZE_FLOAT      = 4;
    SIZE_UINT8      = 1;
    
    %% Set format specifiers
    proc_width      = 3;        
    step_width      = 6;
    t_decimals      = 6;
    t_width         = 16;
    format1         = '%s\n';
    
    title       = dataGroup.shortTitle;
    baseMesh    = dataGroup.mesh;
    ndim        = baseMesh.dim; 
    
    %% Output locations
    output      = System.settings.outp;
    out_head    = output.solutionFilename;
    out_path    = output.directory;
    out_data 	= output.vtkDataDir;
    out_ext     = '.vtu';
    data_dir    = sprintf( '%s%s%s',out_path,out_data);
    file_head   = sprintf('%s_%s_p%.*d',out_head,title,proc_width,rank);
    out_fname   = sprintf('%s_%.*i%s',strtrim(file_head),step_width,iter,strtrim(out_ext));
    
    %% Output files
    pvd_file    = sprintf( '%s%s_%s.pvd',out_path,out_head,title);
    pvtu_file   = sprintf( '%s_%s_%.*i.pvtu',out_head,title,step_width,iter);
    vtu_file    = [out_path out_data strtrim(out_fname)];
    
    % Check if the data directory exists, and create if not present yet
    if ( exist(data_dir,'dir')~=7 && System.rank==0 )
        mkdir( data_dir );
    end
    
    % Ensure all processors are here before continuing, which is achieved
    % by a blocking allReduce call
    if (System.nproc>1)
        status  = 1;
        status = NMPI.Allreduce(status,1,'+'); %#ok<NASGU>
    end
    
    if (verbose)
        fprintf('writing VTK file on rank %d : %s\n',rank,title); %#ok<UNRCH>
    end
    
    %% Determine the Endianness of the architecture
    endian = System.getEndian;
    if (endian == 'L') 
        byte_order = 'LittleEndian';
    elseif (endian == 'B')
        byte_order = 'BigEndian';
  	else
        error('Illegal endianness!')
    end
    
    %% Set the VTK element type
    if ( out_res == 1 )
        % Medium resolution
        switch ndim
            case 1
                error('1D elements are not supported')
            case 2 
                vtk_etype = 9;          % 4-node quad
                NENOD_OUT = 4;
            case 3
                vtk_etype = 25;         % 20-noded hexahedra
                NENOD_OUT = 20;
        end
    else
        % Low and high resolution
        switch ndim
            case 1
               	vtk_etype = 3;          % vtk_line
                NENOD_OUT = 2;
            case 2 
                vtk_etype = 9;          % 4-node quad
                NENOD_OUT = 4;
            case 3
                vtk_etype = 12;         % 8-noded hexahedra
                NENOD_OUT = 8;
        end
    end
    
    %% STEP 1 :: Create ParaView Data (pvd) file (only on rank 0)
    if (rank==0)
        if (~(exist(pvd_file,'file')==2) || iter==0 )
            % create a new file
            [pvd_unit,errmsg] = fopen(pvd_file,'w');

            if (~isempty(errmsg))
                error('output file %s cannot be opened (%s)!\n',pvd_file,errmsg)
            end
            
            % write the header only for a new file
            buffer = '<?xml version="1.0"?>';
            fprintf(pvd_unit,format1,strtrim(buffer));
            buffer = sprintf('<VTKFile type="Collection" version="0.1" byte_order="%s">',strtrim(byte_order));
            fprintf(pvd_unit,format1,buffer);
            buffer = '  <Collection>';
            fprintf(pvd_unit,format1,buffer);
        else
            % add information to an existing file
            [pvd_unit,errmsg] = fopen(pvd_file,'rb+');

            if (~isempty(errmsg))
                error('output file %s cannot be opened (%s)!\n',pvd_file,errmsg)
            end
            
            % move the cursor to ensure the footer is overwritten
            fseek(pvd_unit, -27, 1);
        end

        %% collect pvtu file name in pvd file        
        buffer = sprintf( '    <DataSet timestep="%0*.*f" part="001" file="%s%s"/>',t_width,t_decimals,time,out_data,pvtu_file);
        fprintf(pvd_unit,format1,buffer);
        
        %% write post header for pvd file
        buffer = '  </Collection>';
        fprintf(pvd_unit,format1,buffer);
        buffer = '</VTKFile>';
        fprintf(pvd_unit,format1,buffer);
        fclose(pvd_unit);
    end
     
    %% STEP 2 :: Create pvtu file name (only on rank 0)
    if (rank==0)

%         % collect pvtu file name in pvd file        
%         buffer = sprintf( '    <DataSet timestep="%0*.*f" part="001" file="%s%s"/>',t_width,t_decimals,time(i_t),out_data,pvtu_file);
% %         buffer = sprintf( '    <DataSet timestep="%0*.*f" part="001" file="%s%s"/>',t_width,t_decimals,tstep*dt,out_data,pvtu_file);
%         fprintf(pvd_unit,format1,buffer);

        % open pvtu file
        pvtu_file_fullpath = sprintf('%s%s',data_dir,pvtu_file);
        [pvtu_unit,errmsg] = fopen(pvtu_file_fullpath,'w');
        if (~isempty(errmsg))
            error('output file %s cannot be opened (%s)!\n',pvtu_file_fullpath,errmsg)
        end

        % write headers
        buffer = '<?xml version="1.0"?>';
        fprintf(pvtu_unit,format1,buffer);
        buffer = sprintf('<VTKFile type="PUnstructuredGrid" version="0.1" byte_order="%s">',strtrim(byte_order));
        fprintf(pvtu_unit,format1,buffer);
        buffer = '  <PUnstructuredGrid GhostLevel="0">';
        fprintf(pvtu_unit,format1,buffer);
        buffer = '    <PPoints>';
        fprintf(pvtu_unit,format1,buffer);
        buffer = sprintf('      <PDataArray type="Float32" NumberOfComponents="%d" format="appended"/>',3);
        fprintf(pvtu_unit,format1,buffer);
        buffer = '    </PPoints>';
        fprintf(pvtu_unit,format1,buffer);
        buffer = '    <PCells>';
        fprintf(pvtu_unit,format1,buffer);
        buffer = '      <PDataArray type="Int32" Name="connectivity" format="appended"/>';
        fprintf(pvtu_unit,format1,buffer);
        buffer = '      <PDataArray type="Int32" Name="offsets" format="appended"/>';
        fprintf(pvtu_unit,format1,buffer);
        buffer = '      <PDataArray type="UInt8" Name="types" format="appended"/>';
        fprintf(pvtu_unit,format1,buffer);
        buffer = '    </PCells>';
        fprintf(pvtu_unit,format1,buffer);
        buffer = '    <PPointData>';
        fprintf(pvtu_unit,format1,buffer);
        for i=1:dataGroup.numFields
            buffer = sprintf( '      <PDataArray type="Float32" NumberOfComponents="%d" Name="%s" format="appended"/>', dataGroup.plotComp(i),dataGroup.name{i} );
            fprintf(pvtu_unit,format1,buffer);
        end
        buffer = '    </PPointData>';
        fprintf(pvtu_unit,format1,buffer);

        buffer = '    <PCellData>';
        fprintf(pvtu_unit,format1,buffer);

        % add element information (which is cell data)
        buffer = sprintf( '      <PDataArray type="UInt32" NumberOfComponents="1" Name="elementNumber" format="appended"/>');
        fprintf(pvtu_unit,format1,buffer);
        buffer = sprintf( '      <PDataArray type="UInt8" NumberOfComponents="1" Name="elementLevel" format="appended"/>');
        fprintf(pvtu_unit,format1,buffer);

        buffer = '    </PCellData>';
        fprintf(pvtu_unit,format1,buffer);

        % write vtu file to pvtu file
        for p=1:nproc

            % create vtu file
            file_head = sprintf('%s_%s_p%.*d',out_head,title,proc_width,p-1);
            out_fname = sprintf('%s_%.*i%s',strtrim(file_head),step_width,iter,strtrim(out_ext));

            buffer = sprintf('    <Piece Source="%s"/>',strtrim(out_fname));
            fprintf(pvtu_unit,format1,buffer);
        end

        % Write post header for pvtu file
        buffer = '  </PUnstructuredGrid>';
        fprintf(pvtu_unit,format1,buffer);
        buffer = '</VTKFile>';
        fprintf(pvtu_unit,format1,buffer);
        fclose(pvtu_unit);
    end
        
    %% STEP 3 :: Create vtu file (on all ranks)
%     ne = baseMesh.numLocalElements;
%     qp = baseMesh.getLocalElement(1).disc.dofevq; 
% 
%     % Determine the number of sub elements inside a spectral element
%     numSubElements = baseMesh.getLocalElement(1).disc.dofeq-1;
%     numSubElements( numSubElements==0 ) = 1;
%     numSubElements = prod( numSubElements );
% 
%     proc_nnode(1) = ne * qp;
%     proc_nelmt(1) = ne * numSubElements;
    
    proc_nnode(1) = 0;
    proc_nelmt(1) = 0;
    
    dofHandler = baseMesh.getDoFHandler( true );
    
    if (verbose)
        fprintf('rank %d has elements %d - %d\n',System.rank,dofHandler.eStart,dofHandler.eEnd); %#ok<UNRCH>
    end
    
    for e=dofHandler.eStart:dofHandler.eEnd
        element         = baseMesh.getElement(e);        
        proc_nnode(1)   = proc_nnode(1) + element.finite.numQuadratureNodes;
        
        % Determine the number of sub elements inside a spectral element
        numSubElements = element.finite.qSize - 1;
        numSubElements( numSubElements==0 ) = 1;
        numSubElements = prod( numSubElements );
        proc_nelmt(1) = proc_nelmt(1) + numSubElements;
    end

    if (verbose)
        ne = dofHandler.eEnd - dofHandler.eStart+1; %#ok<UNRCH>
        fprintf('  Number of spectral elements : %d\n',ne)
        fprintf('  Number of sub elements      : %d\n',proc_nelmt(1))
        fprintf('  Number of nodes             : %d\n',proc_nnode(1))
    end

    plot_nvar   = 4 + dataGroup.numFields + 2;
    off         = zeros(plot_nvar,1);
    bytes       = zeros(plot_nvar,1);

    for i_proc = 1:1    % out processors

        %         ! counts total number of points in each slice
        %         !slice_nnode(i_slice) = 0
        %         !slice_nelmt(i_slice) = 0
        % 
        %         !call cvd_count_totals_ext_mesh(slice_nproc(i_slice), &
        %         !slice_proc_list(i_slice,1:slice_nproc(i_slice)),proc_width, &
        %         !inp_path,nnode,nelmt,out_res)
        %         !write(*,'(a)')'complete!'
        %         !write(*,*)'  Total number of nodes: ',nnode
        %         !write(*,*)'  Total number of elements: ',nelmt
        % 
        %         !slice_nnode(i_slice)=nnode
        %         !slice_nelmt(i_slice)=nelmt

        % Compute bytes and offsets            
        bytes(1) = (3*proc_nnode(i_proc))*SIZE_FLOAT;                   % Coordinates
        bytes(2) = (NENOD_OUT*proc_nelmt(i_proc))*SIZE_INTEGER;         % Connectivity
        bytes(3) = (proc_nelmt(i_proc))*SIZE_INTEGER;                   % Offsets
        bytes(4) = (proc_nelmt(i_proc))*SIZE_UINT8;                     % Types
        for i=1:dataGroup.numFields
            bytes(4+i) = (dataGroup.plotComp(i)*proc_nnode(i_proc))*SIZE_FLOAT;     % Nodal values
        end
        bytes(5+dataGroup.numFields) = proc_nelmt(i_proc)*SIZE_INTEGER;	% element numbers
        bytes(6+dataGroup.numFields) = proc_nelmt(i_proc)*SIZE_UINT8; 	% element levels

        off(1) = 0; % 1st offset
        for i=1:plot_nvar
            if (i < plot_nvar)
                off(i+1)=off(i)+SIZE_INTEGER+bytes(i);
            end
            bytes(i) = bytes(i)+SIZE_INTEGER;
        end

%         % create vtu file
%         file_head = sprintf('%s_%s_p%.*d',out_head,title,proc_width,i_proc);
%         out_fname = sprintf('%s_%.*i%s',strtrim(file_head),step_width,iter,strtrim(out_ext));
% 
%         % open vtu file
%         vtu_file = [out_path out_data strtrim(out_fname)];
        [vtu_unit,errmsg] = fopen(vtu_file,'w');
        if (~isempty(errmsg))
            error('output file %s cannot be opened!\n',vtu_file)
        end

        % write header
        buffer = '<?xml version="1.0"?>';
        fprintf(vtu_unit,format1,buffer);
        buffer = ['<VTKFile type="UnstructuredGrid" version="0.1" byte_order="' strtrim(byte_order) '">'];
        fprintf(vtu_unit,format1,buffer);
        buffer = '  <UnstructuredGrid>';
        fprintf(vtu_unit,format1,buffer);
        buffer = sprintf('    <Piece NumberOfPoints="%d" NumberOfCells="%d">',proc_nnode(i_proc),proc_nelmt(i_proc));
        fprintf(vtu_unit,format1,buffer);
        buffer = '      <Points>';
        fprintf(vtu_unit,format1,buffer);
        buffer = sprintf('        <DataArray type="Float32" NumberOfComponents="%d" format="appended" offset="%d"/>',3,off(1));
        fprintf(vtu_unit,format1,buffer);
        buffer = '      </Points>';
        fprintf(vtu_unit,format1,buffer);
        buffer = '      <Cells>';
        fprintf(vtu_unit,format1,buffer);
        buffer = sprintf('        <DataArray type="Int32" Name="connectivity" format="appended" offset="%d"/>',off(2));
        fprintf(vtu_unit,format1,buffer);
        buffer = sprintf('        <DataArray type="Int32" Name="offsets" format="appended" offset="%d"/>',off(3));
        fprintf(vtu_unit,format1,buffer);
        buffer = sprintf('        <DataArray type="UInt8" Name="types" format="appended" offset="%d"/>',off(4));
        fprintf(vtu_unit,format1,buffer);
        buffer = '      </Cells>';
        fprintf(vtu_unit,format1,buffer);
        buffer = '      <PointData>';
        fprintf(vtu_unit,format1,buffer);
        for i=1:dataGroup.numFields
            buffer = sprintf('        <DataArray type="Float32" NumberOfComponents="%d" Name="%s" format="appended" offset="%d"/>',dataGroup.plotComp(i),strtrim(dataGroup.name{i}),off(4+i));
            fprintf(vtu_unit,format1,buffer);
        end
        buffer = '      </PointData>';
        fprintf(vtu_unit,format1,buffer);
        buffer = '      <CellData>';
        fprintf(vtu_unit,format1,buffer);

        % Add element information (number & level), which is cell data
        buffer = sprintf( '        <DataArray type="UInt32" NumberOfComponents="1" Name="elementNumber" format="appended" offset="%d"/>',off(end-1));
        fprintf(vtu_unit,format1,buffer);
        buffer = sprintf( '        <DataArray type="UInt8" NumberOfComponents="1" Name="elementLevel" format="appended" offset="%d"/>',off(end));
        fprintf(vtu_unit,format1,buffer);

        buffer = '      </CellData>';
        fprintf(vtu_unit,format1,buffer);
        buffer = '    </Piece>';
        fprintf(vtu_unit,format1,buffer);
        buffer = '  </UnstructuredGrid>';
        fprintf(vtu_unit,format1,buffer);
        buffer = '  <AppendedData encoding="raw">';
        fprintf(vtu_unit,format1,buffer);
        buffer = '  _';
        fprintf(vtu_unit,'%s',buffer);

        %% write coordinates to vtu file
        fwrite(vtu_unit,bytes(1),'integer*4');

        for e=dofHandler.eStart:dofHandler.eEnd
            element = baseMesh.getElement(e);
%             numElementNodes = element.finite.numQuadratureNodes;
%             numNodes = numNodes + numElementNodes;

            switch ndim
                case 1
                    [X] = element.getNodes(false);
                    Y = zeros( size(X) );
                    Z = zeros( size(X) );
                case 2
                    [X,Y] = element.getNodes(false);
                    Z = zeros( size(X) );
                case 3
                    [X,Y,Z] = element.getNodes(false);                      
            end
            
            fwrite(vtu_unit,[X(:),Y(:),Z(:)]','float32');
        end

        %% write connectivity to vtu file
        fwrite(vtu_unit,bytes(2),'integer*4');

        if (out_res == 0)
            % spectral elements
            %call cvd_write_corner_elements(NSPEC_AB,NGLOB_AB,ibool,node_count,nelmt,nnode,fd_con)
        elseif (out_res == 1)
            % spectral elements
            %call cvd_write_hexa20_elements(NSPEC_AB,NGLOB_AB,ibool,node_count,nelmt,nnode,fd_con)
        elseif (out_res == 2)
            % subdivided spectral elements

            count = 0;
            for e=dofHandler.eStart:dofHandler.eEnd
                
                element         = baseMesh.getElement(e);
                
                numElementNodes = element.finite.numQuadratureNodes;
                nodeID          = element.finite.connectivity + numElementNodes*(count);
                
                count = count+1;
                
                switch ndim
                    case 1
                        %error('not implemented yet')
                        fwrite(vtu_unit,nodeID,'integer*4');
                    case 2
%                         error('not implemented yet')
                        fwrite(vtu_unit,nodeID,'integer*4');
                    case 3
                        fwrite(vtu_unit,nodeID,'integer*4');
                end
            end
            
%             for e=1:ne
%                 nodeID = baseMesh.getLocalElement(e).disc.connectivity + numElementNodes*(e-1);
% 
%                 switch ndim
%                     case 1
%                         error('not implemented yet')
%                     case 2
% %                         error('not implemented yet')
%                         fwrite(vtu_unit,nodeID,'integer*4');
%                     case 3
%                         fwrite(vtu_unit,nodeID,'integer*4');
%                 end
%             end

        else
            error('wrong out_res value!');
        end

        %% Write offsets to vtu file
        fwrite(vtu_unit,bytes(3),'integer*4');         
        offsets = NENOD_OUT*(1:proc_nelmt(i_proc));
        fwrite(vtu_unit,offsets,'integer*4');

        %% Write element types to vtu file
        fwrite(vtu_unit,bytes(4),'integer*4');
        vtk_etypes = vtk_etype * ones(1,proc_nelmt(i_proc));
        fwrite(vtu_unit,vtk_etypes,'uint8');

        %% Write data to vtu file
        for i=1:dataGroup.numFields
            fwrite(vtu_unit,bytes(4+i),'integer*4');

            out_ncomp = dataGroup.nComp(i);

            if (out_ncomp > 1) % vector or tensor data
                
                if (out_ncomp == 2) 
                    % 2D vector: zero 3rd dimension

                    tempData = cell(1,out_ncomp+1);

                    fields = dataGroup.field{i};

                    for c=1:out_ncomp
                        temp = cellfun( @(x) x(:), fields{c}.active.value,'UniformOutput',false);
                        tempData{c} = [temp{:}];
                    end

                    tempData{3} = 0*tempData{1};
                    
                    data = cellfun( @(x) x(:), tempData,'UniformOutput',false);
                    data = [data{:}];

                    fwrite(vtu_unit,data','single');

                elseif (out_ncomp == 3) 
                    %vector
                    tempData = cell(1,out_ncomp);

                    fields = dataGroup.field{i};

                    for c=1:out_ncomp
                        temp = cellfun( @(x) x(:), fields{c}.value,'UniformOutput',false);
                        tempData{c} = [temp{:}];
                    end

                    data = cellfun( @(x) x(:), tempData,'UniformOutput',false);
                    data = [data{:}];

                    fwrite(vtu_unit,data','single');

                elseif (out_ncomp == 6)
%                     % 9-component symmetric tensor
%                     for i_slice=1:slice_nnode(i_slice)
%                         for i_comp=1:out_ncomp
%                             call read_float(tmp_rvect(i_comp),fd_array(i_slice))
%                         end
%                         call write_float(tmp_rvect(1),fd); call write_float(tmp_rvect(4),fd); call write_float(tmp_rvect(5),fd)
%                         call write_float(tmp_rvect(4),fd); call write_float(tmp_rvect(2),fd); call write_float(tmp_rvect(6),fd)
%                         call write_float(tmp_rvect(5),fd); call write_float(tmp_rvect(6),fd); call write_float(tmp_rvect(3),fd)
%                     end
                else
                    error('wrong out_ncomp value!');
                end

            else % out_ncomp==1
                
                temp=cellfun( @(x) x(:), dataGroup.field{i}.value,'UniformOutput',false);
                data = [temp{:}];

                %save(['vtuwriter.' int2str(System.rank) '.mat']);
                
                fwrite(vtu_unit,full(data),'single');

            end % out_ncomp
        end

        %% Write element information to vtu file
        elementNumber = zeros(1,proc_nelmt(1));
        elementLevel  = zeros(1,proc_nelmt(1));
        
        count = 0;
        for e=dofHandler.eStart:dofHandler.eEnd
            element         = baseMesh.getElement(e);
            finiteElement   = FECollection.get( element.feType );
            
            numSubElements  = finiteElement.qSize - 1;
            numSubElements( numSubElements==0 ) = 1;
            numSubElements  = prod( numSubElements );
            
            curRange = (1:numSubElements) + count;
            
            elementNumber(curRange) = repmat(element.id ,numSubElements,1);
            elementLevel(curRange)  = repmat(element.lvl,numSubElements,1);
            
            count = count + numSubElements;
        end

        %fprintf('there are %d subelements on rank %d\n',count,System.rank)
        
        fwrite(vtu_unit,bytes(end-1),'integer*4');
        fwrite(vtu_unit,elementNumber,'uint32');

        fwrite(vtu_unit,bytes(end),'integer*4');
        fwrite(vtu_unit,elementLevel,'uint8');

        %% Write post header for vtu file 
        buffer = '';
        fprintf(vtu_unit,format1,strtrim(buffer));
        buffer = '  </AppendedData>';
        fprintf(vtu_unit,format1,strtrim(buffer));
        buffer = '</VTKFile>';
        fprintf(vtu_unit,format1,strtrim(buffer));

        fclose(vtu_unit);

        % Display progress
        %write(*,fmt=format_str3,advance='no')CR,' slice: ',i_slice,'/',out_nslice,', time step: ',i_t,'/',t_nstep 
    end  % do i_proc
    
    if (timing)
        tend = toc; %#ok<UNRCH>
        fprintf('write_vtu complete (%6.3f s)!\n',tend-t0)
    end

end

