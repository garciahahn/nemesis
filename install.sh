#!/bin/sh

# This script will perform a basic installation of Nemesis:
#   1) compile mex files for the solver
sh ./solver/source/compile_mex.sh

#   2) compile mex files for the parallel environment (MPI)
sh ./parallel/source/compile_mex.sh
