function s = testMe()
    s = struct();
    s.patient.name = 'John Doe';
    s.patient.billing = 127.00;
    s.patient.test = [79, 75, 73; 180, 178, 177.5; 220, 210, 205];
    s.patient(2).name = 'Ann Lane';
    s.patient(2).billing = 28.50;
    s.patient(2).test = [68, 70, 68; 118, 118, 119; 172, 170, 169];
    s.patient(3).name = 'New Name';
    jsonencode(s)
end