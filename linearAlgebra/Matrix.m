classdef Matrix
    %MATRIX Matrix object
    %   Within programs different matrix storage systems might be used.
    %   This class allows the creation of a general matrix from any input,
    %   which allows easy storage, display etc.
    
    properties 
        data;
    end
    
    properties (Access = private)
        %data;
        m, n;
        
        isSymmetric;
        elemental;
        
        filename = 'matrix-default.mtx'
    end
    
    methods        
        % Constructs a global matrix from the elemental cell array K and a
        % dofHandler
        function s = Matrix(K,dofHandler,isSymmetric,elemental)
            %Matrix Construct an instance of this class
            %   Detailed explanation goes here
            
            if (nargin == 1)
                s.data = K;
                
            else
                s.isSymmetric = isSymmetric;
                s.elemental   = elemental;

                % Verify K and dofHandler are of identical sizes
                if ( numel(K) ~= numel(dofHandler.dofs))
                    error('Matrix :: sizes of K and dofHandler do not match')
                end

                % Create a sparse
                if (~elemental)
                    s.data = sparse(dofHandler.numDof,dofHandler.numDof);

                    if (isSymmetric)
                        if (istriu(K{1}))
                            % upper triangular                    
                            for i=1:numel(K)
                                s.data( dofHandler.dofs{i}, dofHandler.dofs{i} ) = s.data( dofHandler.dofs{i}, dofHandler.dofs{i} ) + K{i} + K{i}' - diag(diag(K{i}));
                            end

                        elseif (istril(K{1}))
                            % lower triangular
                            for i=1:numel(K)
                                s.data( dofHandler.dofs{i}, dofHandler.dofs{i} ) = s.data( dofHandler.dofs{i}, dofHandler.dofs{i} ) + K{i} + K{i}' - diag(diag(K{i}));
                            end

                        else
                            error('Matrix :: the matrix should be upper/lower triangular')
                        end
                    end

                    s.m = dofHandler.numDof;
                    s.n = dofHandler.numDof;
                    
                else
                    s.data = cell(numel(K),1);
                    
                    for i=1:numel(K)
                        s.data{i}( dofHandler.localDofs{i}, dofHandler.localDofs{i} ) = K{i} + K{i}' - diag(diag(K{i}));
                    end
                    
                end
            end
        end
        
        function spy(s)
            spy(s.data);
        end
        
        function mmwrite(s,filename)
            if nargin==1
                filename = s.filename;
            end
            
            filename = [System.settings.outp.directory,'/',filename];
            
            err = mmwrite(filename,s.data);
            
            if (err~=0)
                error('Matrix :: mmwrite did not function properly')
            end
        end
        
        function out = mtimes(a,b)
            out = Matrix(a.data * b.data);
        end
    end
    
    methods (Static)
        function out = read(filename,format)
            data = mmread(filename);
            out = Matrix(data);
        end
    end
end

