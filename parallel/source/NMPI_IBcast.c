#include "mex.h"
#include "mpi.h"
#include "matrix.h"
/* usage   */
/* arrayout = NMPI_IBcast(arrayin,count,root,my_rank,ranks) */
/* This is a self made Bcast */
/* */


void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
   if (nrhs!=3)
   {
      mexErrMsgTxt("3 input arguments required.");
   }
   if (nlhs != 1) 
   {
      mexErrMsgTxt("1 output arguments required.");
   }

   /*Def*/
   double *arrayout;
   double *arrayin;
   double *arraytmp;

   /* input */
   arrayin = (double*) mxGetPr(prhs[0]);
   long count = (long) mxGetScalar(prhs[1]);
   int  root = (int) mxGetScalar(prhs[2]);

   int rank; 
   int tag=0;
   int ranks;
   int my_rank;

   /*MPI */
   MPI_Request req;
   MPI_Status stat;
   MPI_Comm_size(MPI_COMM_WORLD,&ranks);
   MPI_Comm_rank(MPI_COMM_WORLD,&my_rank); 

   /* output */
   plhs[0]  = mxCreateDoubleMatrix(count,1,mxREAL);
   arrayout = mxGetPr(plhs[0]);

   /* Call  
 
   /* Receive from root */
   MPI_Irecv(arrayout,count,MPI_DOUBLE,root,tag,MPI_COMM_WORLD,&req);  
   if (root == my_rank) /* If myrank = Root then send to all ranks (r) */
   {
      for (rank=0;rank<ranks;rank++)
        MPI_Isend(arrayin,count,MPI_DOUBLE,rank,tag,MPI_COMM_WORLD,&req);
   }
  
   MPI_Wait(&req,&stat);
  
   return;
}
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NMPI
% John Floan
% NTNU - IT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2015 NORWEGIAN UNIVERISTY OF SCIENCE AND TECHNOLOGY
%
% IN NO EVENT SHALL NTNU BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
% SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF
% THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF NTNU HAS BEEN ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.
% 
% NTNU SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTIES INCLUDING,
% BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
% FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
%
% THIS SOFTWARE IS PROVIDED "AS IS," NTNU HAS NO OBLIGATION TO PROVIDE
% MAINTENANCE, SUPPORT, UPDATE, ENHANCEMENTS, OR MODIFICATIONS.
*/

