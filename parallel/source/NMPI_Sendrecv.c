#include "mex.h"
#include "mpi.h"
#include "matrix.h"

static void NMPI_Sendrecv(double *sendarray,long sendcount, int destination,
                          double *recvarray,long recvcount, int source)
{
    int tag=0;
    MPI_Status comm_stat;
    MPI_Sendrecv(sendarray,sendcount,MPI_DOUBLE,destination,tag,
                 recvarray,recvcount,MPI_DOUBLE,source,tag,MPI_COMM_WORLD,&comm_stat);
   
       
}

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
   if (nrhs!=5) 
   {
      mexErrMsgTxt("5 input arguments required.");
   }
   if (nlhs != 1) 
   {
      mexErrMsgTxt("1 output arguments required.");
   }

   /*input*/
   double *sendarray;

   sendarray = (double*) mxGetPr(prhs[0]);
   long sendcount = (long) mxGetScalar(prhs[1]);
   int  destination = (int) mxGetScalar(prhs[2]);
   long recvcount = (long) mxGetScalar(prhs[3]);
   int  source = (int) mxGetScalar(prhs[4]);

   /*output*/
   double *recvarray;
   plhs[0] = mxCreateDoubleMatrix(recvcount,1,mxREAL);
   recvarray=mxGetPr(plhs[0]);

   /*Call */
   NMPI_Sendrecv(sendarray,sendcount,destination,recvarray,recvcount,source);
   return;
}
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NMPI
% John Floan
% NTNU - IT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2015 NORWEGIAN UNIVERISTY OF SCIENCE AND TECHNOLOGY
%
% IN NO EVENT SHALL NTNU BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
% SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF
% THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF NTNU HAS BEEN ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.
% 
% NTNU SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTIES INCLUDING,
% BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
% FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
%
% THIS SOFTWARE IS PROVIDED "AS IS," NTNU HAS NO OBLIGATION TO PROVIDE
% MAINTENANCE, SUPPORT, UPDATE, ENHANCEMENTS, OR MODIFICATIONS.
*/

