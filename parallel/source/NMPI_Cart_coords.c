#include "mex.h"
#include "mpi.h"

// MPI_Cart_coords
// 
// Determines process coords in cartesian topology given rank in group
// Synopsis
// 
// int MPI_Cart_coords(MPI_Comm comm, int rank, int maxdims, int coords[])
// 
// Input Parameters
// comm             communicator with cartesian structure (handle)
// rank             rank of a process within group of comm (integer)
// maxdims          length of vector coords in the calling program (integer)
// 
// Output Parameters
// coords           integer array (of size ndims) containing the Cartesian 
//                  coordinates of specified process (integer)

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
    if (nrhs!=3) 
    {
      mexErrMsgTxt("3 input arguments required.");
    }
    if (nlhs != 1) 
    {
      mexErrMsgTxt("1 output arguments required.");
    }

    MPI_Comm comm;
    int rank;
    int maxdims;
    
    int *coords;
    
    int ierr;

    /* input */
    comm        = (MPI_Comm) mxGetScalar(prhs[0]);
    rank        = (int) mxGetScalar(prhs[1]);
    maxdims     = (int) mxGetScalar(prhs[2]);
   
//     mexPrintf ("rank    = %d.\n",rank);
//     mexPrintf ("maxdims = %d.\n",maxdims);
   
    /* output */
    plhs[0] = mxCreateNumericMatrix(maxdims, 1, mxINT32_CLASS, mxREAL);
    coords  = (int*) mxGetPr(plhs[0]);
    
    /*return */
    ierr = MPI_Cart_coords(comm,rank,maxdims,coords);
//     mexPrintf ("ierr = %i.\n",ierr);
    
	return;
}
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NMPI
% John Floan
% NTNU - IT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2015 NORWEGIAN UNIVERISTY OF SCIENCE AND TECHNOLOGY
%
% IN NO EVENT SHALL NTNU BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
% SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF
% THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF NTNU HAS BEEN ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.
% 
% NTNU SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTIES INCLUDING,
% BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
% FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
%
% THIS SOFTWARE IS PROVIDED "AS IS," NTNU HAS NO OBLIGATION TO PROVIDE
% MAINTENANCE, SUPPORT, UPDATE, ENHANCEMENTS, OR MODIFICATIONS.
*/

