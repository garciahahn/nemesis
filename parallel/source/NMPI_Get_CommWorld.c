#include "mex.h"
#include "mpi.h"

// MPI_Get_CommWorld
// 
// Returns the MPI_COMM_WORLD communicator

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
    if (nrhs!=0) 
    {
      mexErrMsgTxt("0 input arguments required.");
    }
    if (nlhs != 1) 
    {
      mexErrMsgTxt("1 output arguments required.");
    }

    MPI_Comm *comm;
   
   /* output */
//     plhs[0]   = MPI_COMM_WORLD;
    plhs[0]   = mxCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
    comm = (MPI_Comm*) mxGetPr(plhs[0]);
    *comm = MPI_COMM_WORLD;
      
//     mexPrintf ("world = %i.\n",MPI_COMM_WORLD);
    
//     comm = (MPI_Comm)MPI_COMM_WORLD;

	return;
}
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NMPI
% John Floan
% NTNU - IT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2015 NORWEGIAN UNIVERISTY OF SCIENCE AND TECHNOLOGY
%
% IN NO EVENT SHALL NTNU BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
% SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF
% THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF NTNU HAS BEEN ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.
% 
% NTNU SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTIES INCLUDING,
% BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
% FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
%
% THIS SOFTWARE IS PROVIDED "AS IS," NTNU HAS NO OBLIGATION TO PROVIDE
% MAINTENANCE, SUPPORT, UPDATE, ENHANCEMENTS, OR MODIFICATIONS.
*/

