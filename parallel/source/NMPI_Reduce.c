#include "mex.h"
#include "mpi.h"
#include "matrix.h"
static void NMPI_Reduce(double *sendarray,double *recvarray,
                        long count, char op, int root)
{
    MPI_Op mop;

    switch (op)
    {
       case '+':
          mop=MPI_SUM;
          break;
       case '*':
         mop=MPI_PROD;
         break;
       case 'M':
         mop=MPI_MAX;
         break;
       case 'N':
         mop=MPI_MIN;
         break;
       case '&':
         mop=MPI_LAND; /* logical and */
         break;
       case '|':
         mop=MPI_LOR;  /* Logical or */
         break;
       default: 
         return;
      }
      MPI_Reduce(sendarray,recvarray,count,MPI_DOUBLE,mop,root,MPI_COMM_WORLD);
}

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
   if (nrhs!=4) 
   {
      mexErrMsgTxt("4 input arguments required.");
   }
   if (nlhs != 1) 
   {
      mexErrMsgTxt("1 output arguments required.");
   }

   /*input*/
   double *sendarray;

   sendarray  = (double*) mxGetPr(prhs[0]);
   long count = (long) mxGetScalar(prhs[1]);
   char op    = (char) mxGetScalar(prhs[2]);
   int  root  = (int) mxGetScalar(prhs[3]);

   /*output*/
   double *recvarray;
   plhs[0] = mxCreateDoubleMatrix(count,1,mxREAL);
   recvarray=mxGetPr(plhs[0]);
   
   /* Check operator */

   switch (op)
   {
      case '+':
      case '*':
      case 'M':
      case 'N':
      case '&':
      case '|':
           break;
      default : 
           mexErrMsgTxt("Illigal operator");
           break;
   }

   /*Call */
   NMPI_Reduce(sendarray,recvarray,count,op,root);
   return;
}
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NMPI
% John Floan
% NTNU - IT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2015 NORWEGIAN UNIVERISTY OF SCIENCE AND TECHNOLOGY
%
% IN NO EVENT SHALL NTNU BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
% SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF
% THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF NTNU HAS BEEN ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.
% 
% NTNU SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTIES INCLUDING,
% BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
% FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
%
% THIS SOFTWARE IS PROVIDED "AS IS," NTNU HAS NO OBLIGATION TO PROVIDE
% MAINTENANCE, SUPPORT, UPDATE, ENHANCEMENTS, OR MODIFICATIONS.
*/

