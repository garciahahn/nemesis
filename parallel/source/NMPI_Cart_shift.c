#include "mex.h"
#include "mpi.h"

// MPI_Cart_shift
// 
// Returns the shifted source and destination ranks, given a shift direction 
// and amount
// 
// Synopsis
// int MPI_Cart_shift(MPI_Comm comm, int direction, int disp, int *rank_source,
//                   int *rank_dest)
// 
// Input Parameters
// comm             communicator with cartesian structure (handle)
// direction        coordinate dimension of shift (integer)
// disp             displacement (> 0: upwards shift, < 0: downwards shift) (integer)
// 
// Output Parameters
// rank_source      rank of source process (integer)
// rank_dest        rank of destination process (integer)


void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
   if (nrhs!=3) 
   {
      mexErrMsgTxt("3 input arguments required.");
   }
   if (nlhs != 2) 
   {
      mexErrMsgTxt("2 output arguments required.");
   }
   
   MPI_Comm *comm;
   int direction, disp;
   
   int rank_source;
   int rank_dest;
   
   double *drank_source;
   double *drank_dest;
   
   int ierr;
   
   /* input */
   comm         = (MPI_Comm*) mxGetPr(prhs[0]);
   direction    = (int) mxGetScalar(prhs[1]);
   disp         = (int) mxGetScalar(prhs[2]);
   
//    mexPrintf ("comm = %i.\n",comm[0]);
//    mexPrintf ("dir  = %i.\n",direction);
//    mexPrintf ("disp = %i.\n",disp);
   
   /* output */
   plhs[0]      = mxCreateDoubleMatrix(1, 1, mxREAL);
   drank_source  = (double*) mxGetPr(plhs[0]);
   plhs[1]      = mxCreateDoubleMatrix(1, 1, mxREAL);
   drank_dest    = (double*) mxGetPr(plhs[1]);
   
//    mexPrintf ("comm = %i.\n",comm);
   
   /* output */
//    plhs[0]   = mxCreateNumericMatrix(1, 1, mxINT64_CLASS, mxREAL);
//    comm_cart = (int) mxGetPr(plhs[0]);
   
   /*return */
//    s = NMPI_Cart_create(ndims,dims,periods,reorder,comm_cart);
//     mexPrintf ("comm = %i.\n",comm[0]);
//     mexPrintf ("null = %i.\n",MPI_COMM_NULL);
//     MPI_Barrier(MPI_COMM_WORLD);
   ierr = MPI_Cart_shift(comm[0],direction,disp,&rank_source,&rank_dest);
//    mexPrintf ("ierr = %i.\n",ierr);
   
   *drank_source = (double)rank_source;
   *drank_dest   = (double)rank_dest;
   
   return;
}
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NMPI
% John Floan
% NTNU - IT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2015 NORWEGIAN UNIVERISTY OF SCIENCE AND TECHNOLOGY
%
% IN NO EVENT SHALL NTNU BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
% SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF
% THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF NTNU HAS BEEN ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.
% 
% NTNU SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTIES INCLUDING,
% BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
% FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
%
% THIS SOFTWARE IS PROVIDED "AS IS," NTNU HAS NO OBLIGATION TO PROVIDE
% MAINTENANCE, SUPPORT, UPDATE, ENHANCEMENTS, OR MODIFICATIONS.
*/

