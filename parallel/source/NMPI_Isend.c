#include "mex.h"
#include "mpi.h"
#include "matrix.h"

static void NMPI_Isend(double *array,long count, int destination)
{
    int tag=0;
    MPI_Send(array,count,MPI_DOUBLE,destination,tag,MPI_COMM_WORLD);
   
       
}

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
   if (nrhs!=3) 
   {
      mexErrMsgTxt("3 input arguments required.");
   }
   if (nlhs != 0) 
   {
      mexErrMsgTxt("None output arguments required.");
   }

   /*input*/
   double *array;

   array = (double*) mxGetPr(prhs[0]);
   long count = (long) mxGetScalar(prhs[1]);
   int  destination = (int) mxGetScalar(prhs[2]);
   
   /*Call */
   NMPI_Isend(array,count,destination);
   return;
}
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NMPI
% John Floan
% NTNU - IT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2015 NORWEGIAN UNIVERISTY OF SCIENCE AND TECHNOLOGY
%
% IN NO EVENT SHALL NTNU BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
% SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF
% THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF NTNU HAS BEEN ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.
% 
% NTNU SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTIES INCLUDING,
% BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
% FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
%
% THIS SOFTWARE IS PROVIDED "AS IS," NTNU HAS NO OBLIGATION TO PROVIDE
% MAINTENANCE, SUPPORT, UPDATE, ENHANCEMENTS, OR MODIFICATIONS.
*/

