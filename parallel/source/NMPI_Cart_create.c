#include "mex.h"
#include "mpi.h"

// MPI_Cart_create
// 
// Makes a new communicator to which topology information has been attached
// 
// Synopsis
// int MPI_Cart_create(MPI_Comm comm_old, int ndims, const int dims[],
//                     const int periods[], int reorder, MPI_Comm *comm_cart)
// 
// Input Parameters
// comm_old         input communicator (handle)
// ndims            number of dimensions of cartesian grid (integer)
// dims             integer array of size ndims specifying the number of 
//                  processes in each dimension
// periods          logical array of size ndims specifying whether the grid 
//                  is periodic (true) or not (false) in each dimension
// reorder          ranking may be reordered (true) or not (false) (logical)
//
// Output Parameters
// comm_cart        communicator with new cartesian topology (handle)

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
    if (nrhs!=4) 
    {
      mexErrMsgTxt("4 input arguments required.");
    }
    if (nlhs != 1) 
    {
      mexErrMsgTxt("1 output arguments required.");
    }

    int *dims;
    int *periods;
    int reorder;
    int ndims, ierr;
    MPI_Comm *comm_cart;

    /* input */
    ndims    = (int) mxGetScalar(prhs[0]);
    dims     = (int*) mxGetPr(prhs[1]);
    periods  = (int*) mxGetPr(prhs[2]);
    reorder  = (int) mxGetScalar(prhs[3]);

    if (mxIsInt32(prhs[1]) && mxIsInt32(prhs[2])) {
//         mexPrintf ("correct\n");       
    }
    else {
        mexPrintf ("ERROR in MPI_Cart_create: please pass integer/logical arrays from MATLAB as int32 \n");
        exit;
    }
   
//    mexPrintf ("dims0 = %d.\n",dims[0]);
//    mexPrintf ("dims1 = %d.\n",dims[1]);
//    mexPrintf ("dims2 = %i.\n",dims[2]);
//    
//    mexPrintf ("periods0 = %d.\n",periods[0]);
//    mexPrintf ("periods1 = %d.\n",periods[1]);
//    mexPrintf ("periods2 = %i.\n",periods[2]);
   
   /* output */
    plhs[0]   = mxCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
    comm_cart = (MPI_Comm*) mxGetPr(plhs[0]);
  
   /*return */
    ierr = MPI_Cart_create(MPI_COMM_WORLD,ndims,dims,periods,reorder,comm_cart);
   mexPrintf ("ierr = %i.\n",ierr);
   
   mexPrintf ("world = %i.\n",MPI_COMM_WORLD);
   mexPrintf ("null  = %i.\n",MPI_COMM_NULL);
   mexPrintf ("cart  = %i.\n",comm_cart[0]);

	return;
}
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NMPI
% John Floan
% NTNU - IT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2015 NORWEGIAN UNIVERISTY OF SCIENCE AND TECHNOLOGY
%
% IN NO EVENT SHALL NTNU BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
% SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF
% THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF NTNU HAS BEEN ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.
% 
% NTNU SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTIES INCLUDING,
% BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
% FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
%
% THIS SOFTWARE IS PROVIDED "AS IS," NTNU HAS NO OBLIGATION TO PROVIDE
% MAINTENANCE, SUPPORT, UPDATE, ENHANCEMENTS, OR MODIFICATIONS.
*/

