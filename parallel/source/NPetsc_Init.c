#include "petscsys.h"
#include "mex.h"
#include "mpi.h"

/* Npetsc INIT */
static void NPetsc_Init()
{
    PetscMPIInt rank,size;
    PetscErrorCode ierr;

    //printf("Hello World before initialization\n");
    
    //static char help[] = "Appends to an ASCII file.\n\n";
    //PetscInitialize(NULL,NULL,(char*)0,help);
    ierr = PetscInitialize(PETSC_NULL,PETSC_NULL,PETSC_NULL,PETSC_NULL);   
    //MPI_Init(NULL,NULL);
 
    //MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    //printf("Hello World from rank %d\n",rank);
}

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
   if (nrhs!=0) 
   {
      mexErrMsgTxt("None input arguments required.");
   }
   if (nlhs != 0) 
   {
      mexErrMsgTxt("None output arguments required.");
   }
   //printf("Hello World before initialization\n");
   NPetsc_Init();
   return;
}
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NPetsc
% Marcel Kwakkel
% NTNU - EPT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2015 NORWEGIAN UNIVERISTY OF SCIENCE AND TECHNOLOGY
%
% IN NO EVENT SHALL NTNU BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
% SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF
% THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF NTNU HAS BEEN ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.
% 
% NTNU SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTIES INCLUDING,
% BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
% FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
%
% THIS SOFTWARE IS PROVIDED "AS IS," NTNU HAS NO OBLIGATION TO PROVIDE
% MAINTENANCE, SUPPORT, UPDATE, ENHANCEMENTS, OR MODIFICATIONS.
*/
