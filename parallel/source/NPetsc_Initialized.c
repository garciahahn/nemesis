#include "petscsys.h"
#include "mex.h"
#include "mpi.h"

// MPI_Initialized
// 
// Indicates whether MPI_Init has been called.
// 
// Synopsis
// int MPI_Initialized( int *flag )
// 
// Output Parameters
// flag             Flag is true if MPI_Init or MPI_Init_thread has been 
//                  called and false otherwise.

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
   if (nrhs!=0) 
   {
      mexErrMsgTxt("None input arguments required.");
   }
   if (nlhs != 1) 
   {
      mexErrMsgTxt("One output arguments required.");
   }
   
   PetscBool  flag;
   double   *dflag;
   
   plhs[0]  = mxCreateDoubleMatrix(1, 1, mxREAL);
   dflag    = mxGetPr(plhs[0]);

   /*return */   
   //MPI_Initialized(&flag);
   PetscInitialized(&flag);
   
   *dflag = (double)flag;
   
   return;
}
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NMPI
% John Floan
% NTNU - IT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2015 NORWEGIAN UNIVERISTY OF SCIENCE AND TECHNOLOGY
%
% IN NO EVENT SHALL NTNU BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
% SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF
% THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF NTNU HAS BEEN ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.
% 
% NTNU SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTIES INCLUDING,
% BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
% FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
%
% THIS SOFTWARE IS PROVIDED "AS IS," NTNU HAS NO OBLIGATION TO PROVIDE
% MAINTENANCE, SUPPORT, UPDATE, ENHANCEMENTS, OR MODIFICATIONS.
*/

