#include "mex.h"
#include "mpi.h"

static void NMPI_Comm_rank(double *outrank)
{
    int rank;
//     int ierr;
    double drank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    drank=(double)rank;
    *outrank=drank;
//     mexPrintf ("ierr = %i.\n",ierr);
}

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
   if (nrhs!=0) 
   {
      mexErrMsgTxt("None input arguments required.");
   }
   if (nlhs != 1) 
   {
      mexErrMsgTxt("One output arguments required.");
   }
   double *rank;
   plhs[0] = mxCreateDoubleMatrix(1, 1, mxREAL);
   rank=mxGetPr(plhs[0]);

   /*return */
   NMPI_Comm_rank(rank);
   return;
}
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NMPI
% John Floan
% NTNU - IT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2015 NORWEGIAN UNIVERISTY OF SCIENCE AND TECHNOLOGY
%
% IN NO EVENT SHALL NTNU BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
% SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF
% THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF NTNU HAS BEEN ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.
% 
% NTNU SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTIES INCLUDING,
% BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
% FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
%
% THIS SOFTWARE IS PROVIDED "AS IS," NTNU HAS NO OBLIGATION TO PROVIDE
% MAINTENANCE, SUPPORT, UPDATE, ENHANCEMENTS, OR MODIFICATIONS.
*/

