#include "mex.h"
#include "mpi.h"
#include "matrix.h"

static void NMPI_Gatherv(double *sendarray,int sendcount, 
                          double *recvarray,int recvcount, int stride, int root)
{
    int num_ranks=recvcount/stride;
    int displs[num_ranks];
    int rcounts [num_ranks];
    int i;
    for (i=0;i<num_ranks;i++)
    {
       displs[i]=i*stride;
       rcounts[i]=sendcount;
    }

    MPI_Gatherv(sendarray,sendcount,MPI_DOUBLE,
                recvarray,rcounts,displs,MPI_DOUBLE,root,MPI_COMM_WORLD);      
}

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
   if (nrhs!=5) 
   {
      mexErrMsgTxt("5 input arguments required.");
   }
   if (nlhs != 1) 
   {
      mexErrMsgTxt("1 output arguments required.");
   }

   /*input*/
   double *sendarray;

   sendarray = (double*) mxGetPr(prhs[0]);
   int sendcount = (int) mxGetScalar(prhs[1]);
   int recvcount = (int) mxGetScalar(prhs[2]);
   int stride = (int) mxGetScalar(prhs[3]);
   int root = (int) mxGetScalar(prhs[4]);

   /*output*/
   double *recvarray;
   plhs[0] = mxCreateDoubleMatrix(recvcount,1,mxREAL);
   recvarray=mxGetPr(plhs[0]);

   /*Call */
   NMPI_Gatherv(sendarray,sendcount,recvarray,recvcount,stride,root);
   return;
}
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NMPI
% John Floan
% NTNU - IT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2015 NORWEGIAN UNIVERISTY OF SCIENCE AND TECHNOLOGY
%
% IN NO EVENT SHALL NTNU BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
% SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF
% THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF NTNU HAS BEEN ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.
% 
% NTNU SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTIES INCLUDING,
% BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
% FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
%
% THIS SOFTWARE IS PROVIDED "AS IS," NTNU HAS NO OBLIGATION TO PROVIDE
% MAINTENANCE, SUPPORT, UPDATE, ENHANCEMENTS, OR MODIFICATIONS.
*/

