#include "mex.h"
#include "mpi.h"
#include "matrix.h"
/* usage   */
/* arrayout = NMPI_Bcast(arrayin,count,root,my_rank) */
/* */

static void NMPI_Bcast2(long count, int root,double *array)
{
    MPI_Bcast(array,count,MPI_DOUBLE,root,MPI_COMM_WORLD);      
}

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
   if (nrhs!=4) 
   {
      mexErrMsgTxt("4 input arguments required.");
   }
   if (nlhs != 1) 
   {
      mexErrMsgTxt("1 output arguments required.");
   }

   /*Def*/
   double *arrayout;
   double *arrayin;
   double *arraytmp;

   /* input */

   /*arrayin = (double*) mxGetData(prhs[0]);*/
   long count = (long) mxGetScalar(prhs[1]);
   int  root = (int) mxGetScalar(prhs[2]);
   int my_rank =(int) mxGetScalar(prhs[3]);
   if (my_rank==root) 
      arrayin = (double*) mxGetData(prhs[0]);
   else
      arrayin = mxGetPr(plhs[0]); 
   
   
   /* output */
   /* plhs[0]  = mxCreateDoubleMatrix(count,1,mxREAL); */
   /*arrayout = mxGetPr(plhs[0]); */
   NMPI_Bcast2(count,root,arrayin);
   return;   
   /* Call */

/*   if (root == my_rank)  
   {
      memcpy(arrayout,arrayin,count*sizeof(double));
      NMPI_Bcast(count,root,arrayout);      
   }
   else 
   {
      NMPI_Bcast(count,root,arrayout);
   }
   return;*/
}
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NMPI
% John Floan
% NTNU - IT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2015 NORWEGIAN UNIVERISTY OF SCIENCE AND TECHNOLOGY
%
% IN NO EVENT SHALL NTNU BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
% SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF
% THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF NTNU HAS BEEN ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.
% 
% NTNU SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTIES INCLUDING,
% BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
% FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
%
% THIS SOFTWARE IS PROVIDED "AS IS," NTNU HAS NO OBLIGATION TO PROVIDE
% MAINTENANCE, SUPPORT, UPDATE, ENHANCEMENTS, OR MODIFICATIONS.
*/

