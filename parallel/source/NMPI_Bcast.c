#include "mex.h"
#include "mpi.h"
#include "matrix.h"
/* usage   */
/* arrayout = NMPI_Bcast(arrayin,count,root,my_rank) */
/* */

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
   if (nrhs!=3) 
   {
      mexErrMsgTxt("3 input arguments required.");
   }
   if (nlhs != 1) 
   {
      mexErrMsgTxt("1 output arguments required.");
   }

   /*Def*/
   double *arrayout;
   double *arrayin;
   double *arraytmp;

   /* input */
   arrayin = (double*) mxGetPr(prhs[0]);/* mxGetData(prhs[0]);*/
   long count = (long) mxGetScalar(prhs[1]);
   int  root = (int) mxGetScalar(prhs[2]);

   int my_rank; 
   int i;
   /* output */
   plhs[0]  = mxCreateDoubleMatrix(count,1,mxREAL);
   arrayout = mxGetPr(plhs[0]);

   MPI_Comm_rank(MPI_COMM_WORLD,&my_rank);
    
   /* Call */
   if (root == my_rank) /* If myrank = Root then send */
   {
      #pragma omp parallel for 
      for (i=0;i<count;i++) arrayout[i] = arrayin[i];
     
      MPI_Bcast(arrayout,count,MPI_DOUBLE,root,MPI_COMM_WORLD);
   }
   else /* Other nodes receive */
   {
      MPI_Bcast(arrayout,count,MPI_DOUBLE,root,MPI_COMM_WORLD);
   }
   return;
}
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NMPI
% John Floan
% NTNU - IT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2015 NORWEGIAN UNIVERISTY OF SCIENCE AND TECHNOLOGY
%
% IN NO EVENT SHALL NTNU BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
% SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF
% THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF NTNU HAS BEEN ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.
% 
% NTNU SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTIES INCLUDING,
% BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
% FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
%
% THIS SOFTWARE IS PROVIDED "AS IS," NTNU HAS NO OBLIGATION TO PROVIDE
% MAINTENANCE, SUPPORT, UPDATE, ENHANCEMENTS, OR MODIFICATIONS.
*/

