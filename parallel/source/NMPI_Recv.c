#include "mex.h"
#include "mpi.h"
#include "matrix.h"
static void NMPI_Recv(long count,int source,double *array)
{
   int tag=0;
   MPI_Status comm_stat;
   MPI_Recv(array,count,MPI_DOUBLE,source,tag,MPI_COMM_WORLD,&comm_stat);
   
       
}

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
   if (nrhs != 2) 
   {
      mexErrMsgTxt("2 input arguments required.");
      return;
   }
   if (nlhs != 1) 
   {
      mexErrMsgTxt("1 output arguments required.");
      return;
   }

   /*input*/
   long count = (long) mxGetScalar(prhs[0]);
   int  source = (int) mxGetScalar(prhs[1]);

   /*output*/
   double *array;

   plhs[0] = mxCreateDoubleMatrix(count,1,mxREAL);

   array=mxGetPr(plhs[0]);
   
   /*return */
   NMPI_Recv(count,source,array);
   return;
}
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NMPI
% John Floan
% NTNU - IT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2015 NORWEGIAN UNIVERISTY OF SCIENCE AND TECHNOLOGY
%
% IN NO EVENT SHALL NTNU BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
% SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF
% THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF NTNU HAS BEEN ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.
% 
% NTNU SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTIES INCLUDING,
% BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
% FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
%
% THIS SOFTWARE IS PROVIDED "AS IS," NTNU HAS NO OBLIGATION TO PROVIDE
% MAINTENANCE, SUPPORT, UPDATE, ENHANCEMENTS, OR MODIFICATIONS.
*/

