#include "mex.h"
#include "mpi.h"
#include "matrix.h"
/* root=0; //Sender */
/* n=ln*ranks; */
/* array = rand(n,n);
/* array=NMPI_Scatterv(array,ln,ln,root)*/
static void NMPI_Scatterv(double *sendarray,int sendcount,int stride,  
                          double *recvarray,int recvcount, int root)
{
    int i;
    int num_ranks=sendcount/stride;
    int displs[num_ranks];
    int scounts [num_ranks];
 
    for (i=0;i<num_ranks;i++)
    {
       displs[i]=i*stride;
       scounts[i]=recvcount;
    }

    MPI_Scatterv(sendarray,scounts,displs,MPI_DOUBLE,
                 recvarray,recvcount,MPI_DOUBLE,root,MPI_COMM_WORLD);
}

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
   if (nrhs!=5) 
   {
      mexErrMsgTxt("5 input arguments required.");
   }
   if (nlhs != 1) 
   {
      mexErrMsgTxt("1 output argument required.");
   }

   /*input*/
   double *sendarray;


   sendarray = (double*) mxGetPr(prhs[0]);
   int sendcount = (int) mxGetScalar(prhs[1]);
   int stride = (int) mxGetScalar(prhs[2]);
   int recvcount = (int) mxGetScalar(prhs[3]);
   int root = (int) mxGetScalar(prhs[4]);

   /*output*/
   double *recvarray;
   plhs[0] = mxCreateDoubleMatrix(recvcount,1,mxREAL);
   recvarray=mxGetPr(plhs[0]);

   /*Call */
   NMPI_Scatterv(sendarray,sendcount,stride,recvarray,recvcount,root);
   return;
}
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NMPI
% John Floan
% NTNU - IT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2015 NORWEGIAN UNIVERISTY OF SCIENCE AND TECHNOLOGY
%
% IN NO EVENT SHALL NTNU BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
% SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF
% THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF NTNU HAS BEEN ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.
% 
% NTNU SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTIES INCLUDING,
% BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
% FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
%
% THIS SOFTWARE IS PROVIDED "AS IS," NTNU HAS NO OBLIGATION TO PROVIDE
% MAINTENANCE, SUPPORT, UPDATE, ENHANCEMENTS, OR MODIFICATIONS.
*/

