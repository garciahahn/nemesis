#include "mex.h"
#include "mpi.h"
#include "matrix.h"

// MPI_Allreduce
// 
// Combines values from all processes and distributes the result back to all processes
// 
// Synopsis
// 
// int MPI_Allreduce(const void *sendbuf, void *recvbuf, int count,
//                   MPI_Datatype datatype, MPI_Op op, MPI_Comm comm)
// 
// Input Parameters
// sendbuf              starting address of send buffer (choice)
// count                number of elements in send buffer (integer)
// datatype             data type of elements of send buffer (handle)       -- Always MPI_DOUBLE here!
// op                   operation (handle)
// comm                 communicator (handle)
// 
// Output Parameters
// recvbuf              starting address of receive buffer (choice)
// 

static void NMPI_Allreduce(double *sendarray,double *recvarray,
                           long count, char op, int comm)
{
    MPI_Op mop;

    switch (op)
    {
       case '+':
          mop=MPI_SUM;
          break;
       case '*':
         mop=MPI_PROD;
         break;
       case 'M':
         mop=MPI_MAX;
         break;
       case 'N':
         mop=MPI_MIN;
         break;
       case '&':
         mop=MPI_LAND; /* logical and */
         break;
       case '|':
         mop=MPI_LOR;  /* Logical or */
         break;
       default: 
         return;
      }
      MPI_Allreduce(sendarray,recvarray,count,MPI_DOUBLE,mop,comm);
}

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
   if (nrhs!=4) 
   {
       if (nrhs==6)
       {
           if ( (int)mxGetScalar(prhs[5]) == 1 )
           {
              mexPrintf ("NMPI_Allreduce with tag : %s\n",mxArrayToString(prhs[4]));
           }
       }
       else
       {
           mexErrMsgTxt("4 input arguments required.");
       }
   }
   if (nlhs != 1) 
   {
      mexErrMsgTxt("1 output arguments required.");
   }

   /*input*/
   double *sendarray;
   MPI_Comm *comm;

   sendarray  = (double*) mxGetPr(prhs[0]);
   long count = (long) mxGetScalar(prhs[1]);
   char op    = (char) mxGetScalar(prhs[2]);
   comm         = (MPI_Comm*) mxGetPr(prhs[3]);
   
   /*output*/
   double *recvarray;
   plhs[0]      = mxCreateDoubleMatrix(count,1,mxREAL);
   recvarray    = mxGetPr(plhs[0]);
   
   /* Check operator */

   switch (op)
   {
      case '+':
      case '*':
      case 'M':
      case 'N':
      case '&':
      case '|':
           break;
      default : 
           mexErrMsgTxt("Illegal operator");
           break;
   }
   
//    mexPrintf ("sendarray  = %f.\n",sendarray[0]);
//    mexPrintf ("count      = %i.\n",count);
//    mexPrintf ("comm       = %i.\n",comm[0]);

   /*Call */
    NMPI_Allreduce(sendarray,recvarray,count,op,comm[0]);

    if (nrhs==6)
    {
        if ( (int)mxGetScalar(prhs[5]) == 1 )
        {
            mexPrintf ("Successful NMPI_Allreduce with tag : %s\n",mxArrayToString(prhs[4]));
        }
    }
   return;
}
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NMPI
% John Floan
% NTNU - IT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2015 NORWEGIAN UNIVERISTY OF SCIENCE AND TECHNOLOGY
%
% IN NO EVENT SHALL NTNU BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
% SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF
% THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF NTNU HAS BEEN ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.
% 
% NTNU SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTIES INCLUDING,
% BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
% FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
%
% THIS SOFTWARE IS PROVIDED "AS IS," NTNU HAS NO OBLIGATION TO PROVIDE
% MAINTENANCE, SUPPORT, UPDATE, ENHANCEMENTS, OR MODIFICATIONS.
*/

