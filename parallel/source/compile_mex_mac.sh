#!/bin/sh
rm *.mexa64 *.mexmaci64

mpicc='mpicc'

mex NMPI_Allreduce.c CC=$mpicc
mex NMPI_AllreduceLogical.c CC=mpicc
mex NMPI_Barrier.c CC=mpicc
mex NMPI_Bcast.c CC=mpicc
mex NMPI_Bcast2.c CC=mpicc
mex NMPI_Bcast3.c CC=mpicc
mex NMPI_Cart_coords.c CC=mpicc
mex NMPI_Cart_create.c CC=mpicc CFLAGS='$CFLAGS -Wno-unused-value'
mex NMPI_Cart_shift.c CC=mpicc
mex NMPI_Comm_rank.c CC=mpicc
mex NMPI_Comm_size.c CC=mpicc
mex NMPI_Finalize.c CC=mpicc
mex NMPI_Gather.c CC=mpicc
mex NMPI_Gatherv.c CC=mpicc
mex NMPI_Get_CommWorld.c CC=mpicc
mex NMPI_IBcast.c CC=mpicc
mex NMPI_Init.c CC=mpicc
mex NMPI_Initialized.c CC=mpicc
mex NMPI_Irecv.c CC=mpicc
mex NMPI_Isend.c CC=mpicc
mex NMPI_Recv.c CC=mpicc
mex NMPI_Reduce.c CC=mpicc
mex NMPI_Scatter.c CC=mpicc
mex NMPI_Scatterv.c CC=mpicc
mex NMPI_Send.c CC=mpicc
mex NMPI_Sendrecv.c CC=mpicc
mex NMPI_Waitall.c CC=mpicc

mv *.mex*64 ../.
