#include "mex.h"
#include "mpi.h"
#include "matrix.h"

static void NMPI_Waitall(long count, int *request, MPI_Status *status)
{
    MPI_Waitall(count,request,status);
}

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
   if (nrhs!=2) 
   {
      mexErrMsgTxt("2 input arguments required.");
   }
   if (nlhs != 1) 
   {
      mexErrMsgTxt("One output arguments required.");
   }

   /*input*/
//    double *array;

//    array = (double*) mxGetPr(prhs[0]);
   long count = (long) mxGetScalar(prhs[0]);
//    int  destination = (int) mxGetScalar(prhs[2]);
   int* request = (int*) mxGetPr(prhs[1]);
   MPI_Status* status = (MPI_Status*) mxGetPr(plhs[0]);
   
   /*Call */
   NMPI_Waitall(count,request,status);
   return;
}
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NMPI
% John Floan
% NTNU - IT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2015 NORWEGIAN UNIVERISTY OF SCIENCE AND TECHNOLOGY
%
% IN NO EVENT SHALL NTNU BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
% SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF
% THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF NTNU HAS BEEN ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.
% 
% NTNU SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTIES INCLUDING,
% BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
% FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
%
% THIS SOFTWARE IS PROVIDED "AS IS," NTNU HAS NO OBLIGATION TO PROVIDE
% MAINTENANCE, SUPPORT, UPDATE, ENHANCEMENTS, OR MODIFICATIONS.
*/

