function HelloWorld()
%HELLOWORLD Example of MPI usage within Nemesis
%   This function initializes the System, which 
    %clear global;
    
    tic;

    %% Initialization without loading settings
    System.init(-1);
    
    %% START OF MAIN CODE
    if (System.rank==0)
        fprintf(' \n')
    end
    
    fprintf('Hello World from rank %d\n',System.rank)
    
    if (System.rank==0)
        fprintf(' \n')
    end
    
    %% END OF MAIN CODE
    
    if (System.rank==0)
        time = toc;
        fprintf('Success! Total wall clock time : %5.2e seconds\n',time)
    end

    if (System.nproc>1)
        exit;
    end
end

