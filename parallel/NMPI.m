classdef NMPI < handle
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(Constant)
        instance = NMPI();     % there is only one instance of NMPI, accessible through NMPI.instance
    end
        
    properties (Access=public)
        comm_cart;
        group_cart;
        
        mpi_ndims = 3;
        mpi_reorder = false;
        mpi_periods = [false;false;false];
        mpi_coords;
        
        mpi_comm_world;
        
        mpi_dims;
        nproc;
        rank; 
        
        Px, Py, Pz;
        
        Nx0, Nx1, Ny0, Ny1, Nz0, Nz1;       % Neighbors on all sides

        eStart, eEnd;
        eRange, eOffset;
        numLocalElements;
        
        status;
        
        rankSum = 0;
    end
    
    methods(Access=private)
        function s = NMPI()
    	end        
    end
    
    methods
        function s = createCartesian(s,CPUx,CPUy,CPUz) %,Nex,Ney,Nez,x0,x1,y0,y1,z0,z1)
            %addpath('/work/phase-field/library/NMPI')
            
            try
                if (~NMPI.Initialized())
                    NMPI.Init();
                    %NPetsc_Init();
                end
                
                s.Px = CPUx;
                s.Py = CPUy;
                s.Pz = CPUz;
                s.mpi_dims = [CPUx CPUy CPUz];

                s.mpi_coords = zeros(s.mpi_ndims,1);

                s.nproc = NMPI.Comm_size();
                s.rank  = NMPI.Comm_rank();
            catch
                s.mpi_dims  = 0;
                s.nproc     = 1;
                s.rank      = 0;
            end
           
            if (s.nproc == prod(s.mpi_dims))
                if (s.rank==0)
                    fprintf('MPI        : Successfully initialized on %d processors\n',s.nproc);
                end
            elseif ( prod(s.mpi_dims)>0 )
                if (s.rank==0)
                    fprintf('Number of processors = %d\n',s.nproc);
                    fprintf('Number needed        = %d\n',prod(s.mpi_dims));
                    fprintf('The number of processors is incorrect\n');
                    fprintf('Please adjust the number of processors\n');
                end
                NMPI_finalize();
                error('Program aborted')
            else
                fprintf('MPI        : No MPI implementation found, using single processor without MPI\n');
                %fprintf('MPI        : Number of processors = %d\n',s.nproc);
                %fprintf('MPI        : Number needed        = %d\n',prod(s.mpi_dims));
            end
            
            % Create a cartesian communicator, and save neighboring processors of this rank 
            s.comm_cart = NMPI_Cart_create(s.mpi_ndims,int32(s.mpi_dims),int32(s.mpi_periods),s.mpi_reorder);
            [s.Nx0,s.Nx1] = NMPI_Cart_shift(s.comm_cart,0,1);
            [s.Ny0,s.Ny1] = NMPI_Cart_shift(s.comm_cart,1,1);
            [s.Nz0,s.Nz1] = NMPI_Cart_shift(s.comm_cart,2,1);
                        
            % Determine the coordinates in the cartesian grid for each rank
            s.mpi_coords = double( NMPI_Cart_coords(s.comm_cart,s.rank,s.mpi_ndims) );
   
%             fprintf('rank : %d - x0 = %f - x1 = %f - y0 = %f - y1 = %f - z0 = %f - z1 = %f\n',s.rank,s.x0,s.x1,s.y0,s.y1,s.z0,s.z1);
            
            if (s.rank==0)
                fprintf('MPI        : Cartesian communicator has started\n');
            end
        end
        
        function s = createLinear(s,verbose)
            if (nargin==1)
                verbose = true;
            end
            
            % Try to initialize NMPI, and 
            try
              if (~NMPI_Initialized())
                  NMPI_Init();
              end

              % Obtain some MPI properties
              s.mpi_comm_world  = NMPI_Get_CommWorld();
              s.nproc           = NMPI_Comm_size();
              s.rank            = NMPI_Comm_rank();

              s.status = sprintf('MPI        : Successfully initialized on %d processors',s.nproc);

              if (s.rank==0 && verbose)
                  System.displayTitle;
                  disp( s.status );
              end
            catch MExp
                
                MExp
                
                s.nproc = 1;
                s.rank  = 0;

                if (s.rank==0 && verbose)
                    System.displayTitle;
                    fprintf('MPI        : No NMPI library found, using only 1 processors without MPI\n');
                end
            end
        end
        
        function [eStart,eEnd] = getElementLimits(s,Ne,mesh)
            if (nargin==2)
                s.eStart = round((s.rank  )*prod(Ne)/s.nproc)+1;
                s.eEnd   = round((s.rank+1)*prod(Ne)/s.nproc)  ;

            else
                 average        = prod(Ne) / s.nproc;
                 maxLevel       = mesh.maxLevel;
                 maxSubElements = 4^maxLevel;
                 
                 numChild = mesh.octTree.getNumChild;
                                  
                 range = zeros(s.nproc,2); % start/end element numbers for each proc
                 
                 pos   = 1;
                 count = 0;
                 
                 for cpu=1:s.nproc
                     range(cpu,1) = count+1;
                     while pos<=numel(numChild) && (count+numChild(pos)) < (cpu*average+maxSubElements/2)
                        count = count + numChild(pos);
                        pos=pos+1;
                     end
                     range(cpu,2) = count;
                 end
                 
                 range(cpu,2) = mesh.numElements;
                 
                 s.eStart = range(s.rank+1,1);
                 s.eEnd   = range(s.rank+1,2);
            end
            
            s.eRange           = s.eStart:s.eEnd;
            s.numLocalElements = numel(s.eRange);
            s.eOffset          = s.eStart-1;

            eStart   = s.eStart;
            eEnd     = s.eEnd;
            
            %s.getInfo;
        end
        
        function getInfo(s)
            fprintf('MPI        : rank %d has the elements %d to %d\n',s.rank,s.eStart,s.eEnd)
        end
        
        % Determine local coordinates based on global ones
        function [X0,X1,Y0,Y1,Z0,Z1] = getXYZ(s,x0,x1,y0,y1,z0,z1)
            X0 =  s.mpi_coords(1)    * (x1-x0) / s.Px;
            X1 = (s.mpi_coords(1)+1) * (x1-x0) / s.Px;
            Y0 =  s.mpi_coords(2)    * (y1-y0) / s.Py;
            Y1 = (s.mpi_coords(2)+1) * (y1-y0) / s.Py;
            Z0 =  s.mpi_coords(3)    * (z1-z0) / s.Pz;
            Z1 = (s.mpi_coords(3)+1) * (z1-z0) / s.Pz;
        end
        
        function s = sumRanks(s)
            fprintf('rank == %d\n',s.rank)
            s.rankSum = NMPI_Allreduce(s.rank,1,'+');
%             s.rankSum = NMPI_Reduce(s.rank,1,'+',0);
        end
        
    end
    
    methods(Static)
        
        function out = Allreduce(value,count,operation,varargin)
            
%             if (~isempty(varargin))
%                 if ( NMPI.instance.rank==0 )
%                     fprintf('Allreduce : %s\n',varargin{1});
%                 end
%             else
%                 disp('')
%             end
            
            if (numel(value)~=count)
                fprintf('Error in NMPI.Allreduce : %s -- %d ~= %d\n',varargin{1},numel(value),count);
            end
            
            if NMPI.instance.nproc>1
                out = NMPI_Allreduce(value,count,operation, NMPI.instance.mpi_comm_world );
            else
                out = value;
            end
        end
        
        function out = AllreduceLogical(value,count,operation,varargin)
            
%             if (~isempty(varargin))
%                 if ( NMPI.instance.rank==0 )
%                     fprintf('AllreduceLogical : %s\n',varargin{1});
%                 end
%             else
%                 disp('')
%             end
            
            if (numel(value)~=count)
                fprintf('Error in NMPI.AllreduceLogical : %s -- %d ~= %d\n',varargin{1},numel(value),count);
            end
            
            if NMPI.instance.nproc>1
                out = NMPI_AllreduceLogical( value,count,operation, NMPI.instance.mpi_comm_world );
            else
                out = value;
            end
        end
        
        function out = Reduce(varargin)
            if NMPI.instance.nproc>1
                out = NMPI_Reduce( varargin{:} );
            else
                out = varargin{1};
            end
        end
        
        function out = Recv(varargin)
            if NMPI.instance.nproc>1
                out = NMPI_Recv( varargin{:} );
            else
                out = varargin{1};
            end
        end
        
        function Send(varargin)
            if NMPI.instance.nproc>1
                NMPI_Send( varargin{:} );
            else
                % do nothing
            end
        end
        
        function out = Sendrecv(varargin)
            if NMPI.instance.nproc>1
                out = NMPI_Sendrecv( varargin{:} );
            else
                out = varargin{1};
            end
        end
        
        function Barrier()
            NMPI_Barrier;
        end
        
        function finalize(batch)
            if (nargin==0)
                batch = false;
            end
            
            if (~batch && System.nproc>1)
                NMPI_Finalize();
                exit;
            else
                NMPI_Finalize2();
                warning('MPI has been terminated, the only way to restart it is to restart Matlab. The use of any MPI command will crash Matlab!')
            end
        end       

    end
end

