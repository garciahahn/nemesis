function compileParallel
    if ispc
    [root] = fileparts( mfilename('fullpath') );
    extra_args = {'-IC:\Program Files (x86)\Microsoft SDKs\MPI\Include', '-LC:\Program Files (x86)\Microsoft SDKs\MPI\Lib\x64', '-lmsmpi'};
    mex([root,'/source/NMPI_Allreduce.c'], 'CC=mpicc', extra_args{:}, '-outdir',root)
    mex([root,'/source/NMPI_AllreduceLogical.c'],'CC=mpicc', extra_args{:}, '-outdir',root)
    mex([root,'/source/NMPI_Barrier.c'],'CC=mpicc', extra_args{:}, '-outdir',root)
    mex([root,'/source/NMPI_Bcast.c'],'CC=mpicc', extra_args{:}, '-outdir',root)
    %mex([root,'/source/NMPI_Cart_create.c'],'CC=mpicc', extra_args{:}, '-outdir',root)
    %mex([root,'/source/NMPI_Cart_coords.c'],'CC=mpicc', extra_args{:}, '-outdir',root)
    %mex([root,'/source/NMPI_Cart_shift.c'],'CC=mpicc', extra_args{:}, '-outdir',root)
    mex([root,'/source/NMPI_Comm_rank.c'],'CC=mpicc', extra_args{:}, '-outdir',root)
    mex([root,'/source/NMPI_Comm_size.c'],'CC=mpicc', extra_args{:}, '-outdir',root)
    mex([root,'/source/NMPI_Finalize.c'],'CC=mpicc', extra_args{:}, '-outdir',root)
    %mex([root,'/source/NMPI_Finalize2.c'],'CC=mpicc', extra_args{:}, '-outdir',root)
    mex([root,'/source/NMPI_Get_CommWorld.c'],'CC=mpicc', extra_args{:}, '-outdir',root)
    mex([root,'/source/NMPI_Init.c'],'CC=mpicc', extra_args{:}, '-outdir',root)
    mex([root,'/source/NMPI_Initialized.c'],'CC=mpicc', extra_args{:}, '-outdir',root)
    mex([root,'/source/NMPI_Reduce.c'],'CC=mpicc', extra_args{:}, '-outdir',root)
    mex([root,'/source/NMPI_Send.c'],'CC=mpicc', extra_args{:}, '-outdir',root)
    mex([root,'/source/NMPI_Recv.c'],'CC=mpicc', extra_args{:}, '-outdir',root)
    mex([root,'/source/NMPI_Sendrecv.c'],'CC=mpicc', extra_args{:}, '-outdir',root)
    mex([root,'/source/NMPI_Waitall.c'],'CC=mpicc', extra_args{:}, '-outdir',root)
    else
    [root] = fileparts( mfilename('fullpath') );

    mex([root,'/source/NMPI_Allreduce.c'],'CC=mpicc','-outdir',root)
    mex([root,'/source/NMPI_AllreduceLogical.c'],'CC=mpicc','-outdir',root)
    mex([root,'/source/NMPI_Barrier.c'],'CC=mpicc','-outdir',root)
    mex([root,'/source/NMPI_Bcast.c'],'CC=mpicc','-outdir',root)
    %mex([root,'/source/NMPI_Cart_create.c'],'CC=mpicc','-outdir',root)
    %mex([root,'/source/NMPI_Cart_coords.c'],'CC=mpicc','-outdir',root)
    %mex([root,'/source/NMPI_Cart_shift.c'],'CC=mpicc','-outdir',root)
    mex([root,'/source/NMPI_Comm_rank.c'],'CC=mpicc','-outdir',root)
    mex([root,'/source/NMPI_Comm_size.c'],'CC=mpicc','-outdir',root)
    mex([root,'/source/NMPI_Finalize.c'],'CC=mpicc','-outdir',root)
    %mex([root,'/source/NMPI_Finalize2.c'],'CC=mpicc','-outdir',root)
    mex([root,'/source/NMPI_Get_CommWorld.c'],'CC=mpicc','-outdir',root)
    mex([root,'/source/NMPI_Init.c'],'CC=mpicc','-outdir',root)
    mex([root,'/source/NMPI_Initialized.c'],'CC=mpicc','-outdir',root)
    mex([root,'/source/NMPI_Reduce.c'],'CC=mpicc','-outdir',root)
    mex([root,'/source/NMPI_Send.c'],'CC=mpicc','-outdir',root)
    mex([root,'/source/NMPI_Recv.c'],'CC=mpicc','-outdir',root)
    mex([root,'/source/NMPI_Sendrecv.c'],'CC=mpicc','-outdir',root)
    mex([root,'/source/NMPI_Waitall.c'],'CC=mpicc','-outdir',root)
    end
end