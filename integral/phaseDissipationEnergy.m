function [dissipationEnergy0,dissipationEnergy1,dissipationEnergy2] = phaseDissipationEnergy(mesh,C,Omg)

    dissipationEnergy0  = 0;
    dissipationEnergy1  = 0;
    dissipationEnergy2  = 0;

%     % Get the required fields
%     C = physics.getField('C');
%     U = physics.getField('U');
%     V = physics.getField('V');

    numElements = mesh.numLocalElements;

    % Initialize rho and the squared velocity for all elements
%     mu    	= cell(numElements,1);
%     Phi     = cell(numElements,1);

    lapOmg  = cell(numElements,1);
    gradOmg = cell(numElements,1);
    Omgk    = cell(numElements,1);    

    % Initialize variables
    for el=1:numElements
        e = mesh.getLocalElement(el).globalID;

        Omgk{el}     = (Omg.val(e)).^2;
        dOmgkdx     = Omg.x(e);
        dOmgkdy     = Omg.y(e);
        dOmgkdxx    = Omg.xx(e);
        dOmgkdyy    = Omg.yy(e);

%         mu{el}      = Ck + (1-Ck) .* System.settings.phys.lamMu;
%         Phi{el}     = 2*dUkdx.^2 + 2*dVkdy.^2 + (dVkdx+dUkdy).^2;
%         divVel{el}  = ( dUkdx + dVkdy ).^2;
        
        lapOmg{el}  = dOmgkdxx.^2 + dOmgkdyy.^2;
        gradOmg{el} = dOmgkdx.^2 + dOmgkdy.^2;
    end
    
    % Compute dissipation and divergence energies
    for e=1:numElements
        element = mesh.getLocalElement(e);
        
%         data1 = System.settings.phys.alpha * Omgk{e}; %
        data0 = (System.settings.phys.Cn/(System.settings.phys.Pe*System.settings.phys.alpha)) .* Omgk{e};
        data1 = (System.settings.phys.Cn/(System.settings.phys.Pe*System.settings.phys.alpha)) .* lapOmg{e};
        %data2 = (System.settings.phys.Cn/(System.settings.phys.Pe*System.settings.phys.alpha)) .* gradOmg{e};
        data2 = 1.0/System.settings.phys.Pe .* gradOmg{e};
        
        %data2 = -2/3 * mu{e} .* System.settings.phys.Oh .* divVel{e};

        wxyz    = element.disc.W; %superkron(element.disc.wz,element.disc.wy,element.disc.wx);
        
        dissipationEnergy0 = dissipationEnergy0 + data0(:)' * wxyz / (element.J(1)*element.J(2)*element.J(3)) ;
        dissipationEnergy1 = dissipationEnergy1 + data1(:)' * wxyz / (element.J(1)*element.J(2)*element.J(3)) ;
        dissipationEnergy2 = dissipationEnergy2 + data2(:)' * wxyz / (element.J(1)*element.J(2)*element.J(3)) ;
    end

    if (System.settings.anal.debug)
        fprintf('Before NMPI_Allreduce in phaseDissipationEnergy\n')
    end

    % Sum from all ranks
    dissipationEnergy0 = NMPI_Allreduce(dissipationEnergy0,1,'+',Parallel3D.MPI.mpi_comm_world);
    dissipationEnergy1 = NMPI_Allreduce(dissipationEnergy1,1,'+',Parallel3D.MPI.mpi_comm_world);
    dissipationEnergy2 = NMPI_Allreduce(dissipationEnergy2,1,'+',Parallel3D.MPI.mpi_comm_world);
    
    %divergenceEnergy  = NMPI_Allreduce(divergenceEnergy ,1,'+',Parallel3D.MPI.mpi_comm_world);
end