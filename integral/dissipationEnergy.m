function [dissipationEnergy,divergenceEnergy] = dissipationEnergy(mesh,C,U,V)

    dissipationEnergy   = 0;
    divergenceEnergy    = 0;

%     % Get the required fields
%     C = physics.getField('C');
%     U = physics.getField('U');
%     V = physics.getField('V');

    numElements = mesh.numLocalElements;

    % Initialize rho and the squared velocity for all elements
    mu    	= cell(numElements,1);
    Phi     = cell(numElements,1);
    divVel  = cell(numElements,1);

    % Initialize variables
    for el=1:numElements
        e = mesh.getLocalElement(el).globalID;

        Ck          = C.val(e);
        dUkdx       = U.x(e);
        dVkdx       = V.x(e);
        dUkdy       = U.y(e);
        dVkdy       = V.y(e);

        mu{el}      = Ck + (1-Ck) .* System.settings.phys.lamMu;
        Phi{el}     = 2*dUkdx.^2 + 2*dVkdy.^2 + (dVkdx+dUkdy).^2;
        divVel{el}  = ( dUkdx + dVkdy ).^2;
    end
    
    % Compute dissipation and divergence energies
    for e=1:numElements
        element = mesh.getLocalElement(e);
        
        data1 = mu{e} .* System.settings.phys.Oh .* Phi{e};
        data2 = -2/3 * mu{e} .* System.settings.phys.Oh .* divVel{e};

        wxyz = element.disc.W;
        
        dissipationEnergy = dissipationEnergy + data1(:)' * wxyz / (element.J(1)*element.J(2)*element.J(3)) ;
        divergenceEnergy  = divergenceEnergy  + data2(:)' * wxyz / (element.J(1)*element.J(2)*element.J(3)) ;
    end

    if (System.settings.anal.debug)
        fprintf('Before NMPI_Allreduce in dissipationEnergy\n')
    end

    % Sum from all ranks
    dissipationEnergy = NMPI_Allreduce(dissipationEnergy,1,'+',Parallel3D.MPI.mpi_comm_world);
    divergenceEnergy  = NMPI_Allreduce(divergenceEnergy ,1,'+',Parallel3D.MPI.mpi_comm_world);
end