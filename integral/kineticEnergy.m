% The total kinetic energy is defined by the integration of 
% 0.5*rho*U^2 over the full domain. Here rho is a function of C,
% so both fluid as interface solutions are required.
function [totalKineticEnergy,transKineticEnergy] = kineticEnergy(mesh,C,U,V)

    totalKineticEnergy = 0;
    transKineticEnergy = 0;

    numElements = mesh.numLocalElements;
    
    timeLevel = 'end';

    % Initialize rho and the squared velocity for all elements
    rho   	= cell(numElements,1);
    absVel2	= cell(numElements,1);
    absV2	= cell(numElements,1);

    for el=1:numElements
        e = mesh.getLocalElement(el).globalID;

        Ck          = C.val(e);
        Uk          = U.val(e);
        Vk          = V.val(e);

        rho{el}     = Ck + (1-Ck) .* System.settings.phys.lamRho;
        absVel2{el} = Uk.^2+Vk.^2;
        absV2{el}   = Vk.^2;
    end

    for e=1:numElements
        element = mesh.getLocalElement(e);
        
        data1 = 0.5 .* rho{e} .* absVel2{e};
        data2 = 0.5 .* rho{e} .* absV2{e};

        data1 = reshape(data1,element.disc.Q);
        data2 = reshape(data2,element.disc.Q);
        
        if isinteger(timeLevel)
            slice1 = data1(:,:,timeLevel);
            slice2 = data2(:,:,timeLevel);
        else
            slice1 = data1(:,:,end);
            slice2 = data2(:,:,end);
        end
                
        wxy = element.disc.Wspatial;
        
        totalKineticEnergy = totalKineticEnergy + slice1(:)' * wxy / (element.J(1)*element.J(2)) ;
        transKineticEnergy = transKineticEnergy + slice2(:)' * wxy / (element.J(1)*element.J(2)) ;
    end

    if (System.settings.anal.debug)
        fprintf('Before NMPI_Allreduce in integral.kineticEnergy\n')
    end

    totalKineticEnergy = NMPI_Allreduce(totalKineticEnergy,1,'+',Parallel3D.MPI.mpi_comm_world);
    transKineticEnergy = NMPI_Allreduce(transKineticEnergy,1,'+',Parallel3D.MPI.mpi_comm_world);
%     if (Parallel3D.MPI.rank==0)
%         fprintf('Total kinetic energy  : %f\n',totalKineticEnergy)
%         fprintf('Trans kinetic energy  : %f\n',transKineticEnergy)
%     end
end
