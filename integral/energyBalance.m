function out = energyBalance(physics)
    
    mainPhysics = physics{1};
    mainMesh    = mainPhysics.getMesh;

    % Get the required fields
    for i=1:numel(physics)
        if isa(physics{i},'NavierStokes')
            U = physics{i}.getField(1,mainPhysics);
            V = physics{i}.getField(2,mainPhysics);
            P = physics{i}.getField(3,mainPhysics);
        elseif isa(physics{i},'CahnHilliard')
            C = physics{i}.getField(1,mainPhysics);
            Omg = physics{i}.getField(2,mainPhysics);
            CM = physics{i}.getCM;
        end
    end

    surface                 	= integral.surfaceEnergy(mainMesh,C);
    pressure                    = integral.pressurePotential(mainMesh,P);
    [totalKinetic,transKinetic] = integral.kineticEnergy(mainMesh,C,U,V);
    gravitational               = integral.gravitationalEnergy(CM);
    [dissipation, divergence]   = integral.dissipationEnergy(mainMesh,C,U,V);
    [phaseDissipation0,phaseDissipation1,phaseDissipation2] = integral.phaseDissipationEnergy(mainMesh,C,Omg);
    
    out = table(surface,pressure,totalKinetic,transKinetic,dissipation,divergence,phaseDissipation0,phaseDissipation1,phaseDissipation2,gravitational);

end