% The pressure energy is defined by the integration of the pressure
% over the full domain. Initially the pressure is zero everywhere,
% so any change from zero will contribute to the potential.
function [pressurePotential] = pressurePotential(mesh,P)

    pressurePotential = 0;

    numElements = mesh.numLocalElements;

    % Initialize the pressure for all elements
    pressure = cell(numElements,1);

    for el=1:numElements
        e = mesh.getLocalElement(el).globalID;

        Pk          = P.val(e);
        %Ck          = C.val(e);

        pressure{el} = (Pk); %.* (1-Ck);
    end

    for e=1:numElements
        element = mesh.getLocalElement(e);
        
        data1   = reshape( pressure{e}, element.disc.Q );
        slice1  = data1(:,:,end);
        
        wxy = element.disc.Wspatial;
        pressurePotential = pressurePotential + slice1(:)' * wxy / (element.J(1)*element.J(2)) ;
    end

    if (System.settings.anal.debug)
        fprintf('Before NMPI_Allreduce in integral.pressurePotential\n')
    end

    pressurePotential = NMPI_Allreduce(pressurePotential,1,'+',Parallel3D.MPI.mpi_comm_world);
    
    pressurePotential = System.settings.comp.NS.pressureWeight * pressurePotential;

%     if (Parallel3D.MPI.rank==0)
%         fprintf('Total kinetic energy  : %f\n',totalPressureEnergy)
%     end
end