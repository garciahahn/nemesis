% The surface energy can be determined from the Helmholtz free 
% energy density model, which in the Cahn-Hilliard model is the 
% sum of the local Helmholtz energy and surface tension effects.
function surfaceEnergy = surfaceEnergy(mesh,C)

    surfaceEnergy = 0;

    Cn = System.settings.phys.Cn;
    
    numElements = mesh.numLocalElements;

    % Initialize psi and gradC for all elements
    psi     = cell(numElements,1);
    gradC2  = cell(numElements,1);

    for el=1:numElements
        e = mesh.getLocalElement(el).globalID;

        Ck          = C.val(e);
        dCkdx       = C.x(e);
        dCkdy       = C.y(e);

        psi{el}      = 0.25 * Ck.^2 .* (1-Ck).^2;
        gradC2{el}    = dCkdx.^2 + dCkdy.^2;
    end

    for e=1:numElements
        element = mesh.getLocalElement(e);
        
        data = 0.5 * Cn .* gradC2{e} + psi{e} ./ Cn;

        data = reshape(data,element.disc.Q);
        
        if (mesh.disc.spaceTime)
            slice = data(:,:,end);
        else
            slice = data(:);
        end
                
        wxy = element.disc.Wspatial;
        surfaceEnergy = surfaceEnergy + slice(:)' * wxy / (element.J(1)*element.J(2)) ;
    end

    surfaceEnergy = surfaceEnergy * System.settings.phys.alpha;

    %if (System.settings.anal.debug)
    %    fprintf('Before NMPI_Allreduce in CahnHilliard.computeSurfaceEnergy\n')
    %end

    surfaceEnergy = NMPI_Allreduce(surfaceEnergy,1,'+',Parallel3D.MPI.mpi_comm_world);
%     if (Parallel3D.MPI.rank==0)
%         fprintf('Total surface energy  : %f\n',surfaceEnergy)
%     end
end