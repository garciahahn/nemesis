classdef OctreeElement < StaticElement
        
    properties (SetAccess = public)
        refineFactor;
    end
    
    properties
        isRefined   = false;
        refineFlag  = false;        
        coarsenFlag = false;
        
        parent;
        lvl         = -1;        	% level
        targetLvl;
        levelLimit;
                
        children;
    end
    
    methods
        
        function s = OctreeElement(id,parent,position,geometricElement)
            
            if (nargin==0)
                id                 = -1;
                geometricElement   = -1;
            end
            
            s@StaticElement(id,geometricElement);
            
            if nargin>1
                s.parent    = parent;
                
                try
                    s.neighbor 	= cell(2,s.geometric.dimension);
                catch
                    error('Problem in the initialization of the OctreeElement')
                end
                
                if (isempty(parent))
                    s.lvl       = 0;
                else
                    % Copy default values from the parent
                    s.lvl       = parent.lvl+1;
                    s.neighbor  = parent.neighbor;
                    s.material  = parent.material;
                end
                
                if ( ~iscell(position) || ~all(size(position)==[1,s.geometric.dimension]) ) 
                    error('The position parameter must be a 1x%d cell array',s.geometric.dimension)
                end
                s.position      = position;
            end
        end
        
        % This function creates children
        function [id,isChanged] = refineCoarsen(s,id) %,feLists)
            
            isChanged = false;
            
            if (isvalid(s))
                % The coarsening step might delete elements, which then
                % become invalid, this statement ensure only valid elements
                % will be processed further
                
                if (s.lvl < s.targetLvl)
                    %% Refinement
                    % As long as the targetLvl is larger than the current
                    % lvl, refinement is required
                    
                    % The number of children is determined by the refineFactor, and
                    % the the size of the childPosition by the element dimension
                    s.children      = cell(s.refineFactor);
                    childPosition   = cell(1,s.geometric.dimension);

                    % Process the geometric refinement, a new geometric element for each
                    % child is created and returned as a cell array. 
                    geometricChildren = s.geometric.refine(s.refineFactor,1);

                    % Loop over the children to create all OctreeElements. The
                    % children are initialized with the geometric geometricChild
                    for c=1:numel(s.children)
                        % Determine the position of this child within the parent
                        [childPosition{:}] = ind2sub(size(s.children),c);

                        % Increase the id
                        id = id+1;

                        % Create the child OcreeElement
                        s.children{c} = OctreeElement(id,s,childPosition,geometricChildren{c});

                        s.children{c}.targetLvl     = s.targetLvl;
                        s.children{c}.refineFactor  = s.refineFactor;
                    end

                    % Set neighbors (2D):
                    %
                    %             Neighbor 4
                    %              |-------|
                    %              | 3 | 4 |
                    %  Neighbor 2  |-------|  Neighbor 3
                    %              | 1 | 2 |
                    %              |-------|
                    %             Neighbor 1
                    % 
                    % Step 2: Assign the internal neighbors
                    for l=1:s.refineFactor(4)
                        for k=1:s.refineFactor(3)
                            for j=1:s.refineFactor(2)
                                for i=1:s.refineFactor(1)

                                    % General
                                    position = {i,j,k,l};
                                    for dir=1:s.geometric.dimension
                                        activeChild = s.children{ position{:} };

                                        position0 = position; position0{dir} = position{dir}-1;
                                        position1 = position; position1{dir} = position{dir}+1;

                                        if (position{dir}>1)
                                            lowerChild = s.children{ position0{:} };
                                        end
                                        if (position{dir}<s.refineFactor(dir))
                                            upperChild = s.children{ position1{:} };
                                        end

                                        if (s.refineFactor(dir)>1)
                                            if (position{dir}==1)
                                                % Upper child
                                                activeChild.setNeighbor(dir,1,upperChild);       % upper
                                            elseif (position{dir}==s.refineFactor(dir))
                                                % Lower child
                                                activeChild.setNeighbor(dir,0,lowerChild);       % lower
                                            else 
                                                % Internal children
                                                activeChild.setNeighbor(dir,0,lowerChild);       % lower
                                                activeChild.setNeighbor(dir,1,upperChild);       % upper
                                            end
                                        end
                                    end                           
                                end
                            end
                        end
                    end

                    s.isRefined = true;

                    % Recursive refinement
                    for c=1:numel(s.children)
                        activeChild = s.children{c};                
                        activeChild.refineCoarsen(id);
                    end
                    
                    isChanged = true;

                elseif (s.lvl > s.targetLvl)
                    %% Coarsening

                    % Ensure there is a parent
                    if ( ~isempty(s.parent) )
                        
                        % Check if all siblings want coarsening. Also ensure this
                        % value is =>0, by setting it to zero when it's <0
                        siblings = [s.parent.children{:}];                
                        wantCoarsening = max( [siblings.lvl] - [siblings.targetLvl], 0);
                        %siblings.material
                        
                        % All siblings must have the same material
                        uniformMaterial = all([siblings.material]==siblings(1).material);
                        
                        % No sibling can have a neighbor of different material
                        uniformNeighbors = true;
                        for n=1:numel(siblings)
                            neighbors           = [siblings(n).neighbor{:}];
                            neighbors           = neighbors(isvalid(neighbors));
                            neighborMaterials   = [neighbors.material];
                            if ~(all(neighborMaterials==neighborMaterials(1)) && s.material==neighborMaterials(1))
                                uniformNeighbors = false;
                                break;
                            end
                        end
                        
                        if ( all(wantCoarsening) && uniformMaterial && uniformNeighbors )
                            % Save the parent to a temporary variable tmpParent
                            tmpParent = s.parent;

                            % Set the targetLvl of the tmpParent to the one of the children
                            tmpParent.targetLvl = s.targetLvl;

                            % Remove the isRefined flag for the parent
                            tmpParent.isRefined = false;

                            % Remove the children array
                            tmpParent.children = [];

                            % Delete the geometric element 
                            s.geometric.remove(1);

                            % Delete the children of this OctreeElement
                            arrayfun( @(x) x.delete(), siblings );

                            % Call refineCoarsen on the tmpParent
                            tmpParent.refineCoarsen(id);

                            isChanged = true;
                        end
                    end
                end                
            end
        end
        
        % Function which returns a flag to indicate if this oct needs to be
        % refined
        function out = checkCoarsening(s)
            if (s.getNumChild>1)
                coarseningFlag  = false(s.getNumChild,1);
                for i=1:numel(s.ch)
                    coarseningFlag(i)  = s.ch{i}.checkCoarsening();
                end
                if (all(coarseningFlag))
%                     if (~s.wantsRefinement)
%                         s.wantsCoarsening = true;
%                     end
                    for i=1:numel(s.ch)
                        s.ch{i}.wantsRemoval = true;
                    end

%                     s.wantsCoarsening = false;
%                     for i=1:numel(s.ch)
%                         delete(s.ch{i});
%                     end
%                     s.isRefined = false;
                    
                else
                    s.wantsCoarsening = false;
                end
            end
            out = s.wantsCoarsening;
        end
        
        function Z = createZ(s)
            % Check if the element under consideration is refined, which
            % should not be the case.
            if (s.isRefined)
                error('The element is refined, but that should not be possible')
            end
            
            % If all dofLevels are identical, just use an identity array
            % for Z.
            dl = s.dofLevels;
            uniformDofLevel = all(dl(:)==dl(1));
            
            %
            if (s.lvl==0 || uniformDofLevel)
                % sparse identity matrix
                Z = speye( prod(s.finite.dSize) );

            else
                % Initialize the 'reset' and 'zBase' arrays. The 'reset' logical 
                % array is used to ensure unity values on locations in the Z 
                % array which are not influenced by the refinement. The 'zBase'
                % are set in each spatial direction, and multiplied with each 
                % other to obtain a preliminary Z. Finally, the reset array is
                % used to obtain the final Z.
                %reset = true(s.finite.dSize);            
                zBase = cell(4,1);

                for i=1:s.finite.dim
                    zBase{i} = s.finite.basis{i}.Z{ s.position{i} };
                end

                % compute a preliminary Z, which depends on the location of
                % the child wrt the parent
                Z = superkron( zBase(1:s.finite.dim) );

%                 % setup the reset array
%                 layer = cell(2,1);
% 
%                 for dir=1:s.finite.dim
%                     % all directions                    
%                     layer{1} = 1:s.finite.basis{dir}.numBoundDof;                                           % lower layer
%                     layer{2} = 1+s.finite.dSize(dir)-s.finite.basis{dir}.numBoundDof:s.finite.dSize(dir);   % upper layer
% 
%                     for pos=1:2
%                         % lower & upper sides
%                         activeNeighbor = s.neighbor{pos,dir};
% 
%                         if ~isempty( activeNeighbor ) && isvalid( activeNeighbor ) && activeNeighbor.active
%                             if ( s.isMoreCoarse( activeNeighbor ) )
% 
%                                 indices             = repmat({':'}, 1, 4);
%                                 indices{dir}        = layer{pos};                                
%                                 reset( indices{:} ) = false;
%                             else
%                                 layer2 = cell(2,1);
% 
%                                 for dir2=1:s.finite.dim
%                                     layer2{1} = 1:s.finite.basis{dir2}.numBoundDof;                                             % lower layer
%                                     layer2{2} = 1+s.finite.dSize(dir2)-s.finite.basis{dir2}.numBoundDof:s.finite.dSize(dir2);   % upper layer
% 
%                                     if (dir~=dir2)
%                                         for pos2=1:2
%                                             diagonalNeighbor = activeNeighbor.neighbor{pos2,dir2};
%                                             if ~isempty( diagonalNeighbor ) && isvalid( diagonalNeighbor )
%                                                 if ( s.isMoreCoarse( diagonalNeighbor ) )
%                                                     indices             = repmat({':'}, 1, 4);
%                                                     indices{dir}        = layer{pos};
%                                                     indices{dir2}       = layer2{pos2};
%                                                     reset( indices{:} ) = false;
% 
%                                                 end
%                                             end
%                                         end
%                                     end
%                                 end
%                             end
%                         end
%                     end
%                 end
                
                reset = (dl==max(dl(:)));

                Z(  :  ,reset) = 0; 
                Z(reset,  :  ) = 0;
                Z(reset,reset) = eye(nnz(reset));
            end 
        end
        
        function out = getBoundaryQuadratureWeights(s)
            % Function which returns 'true' for quadrature points that are
            % located on the boundary of the domain (the element does not
            % have a neighbor in a certain direction)
            out = ones( s.finite.qSize );
            
            tmp = cell( numel(size(out)), 1 );
            for i=1:numel(tmp)
                tmp{i} = ':';
            end
            
            weight = sqrt(System.settings.comp.boundary.eqWeight);
            
            for dir=1:s.finite.sDim         % only spatial directions!
                for pos=1:2
                    slice = tmp;
                    if ( isempty(s.neighbor{pos,dir}) )
                        if (pos==1)
                            slice{ dir } = 1;
                        else
                            slice{ dir } = size(out,dir);
                        end
                        
                        out( slice{:} ) = weight;
                    end
                end
            end
        end
        
        % Function to delete an OctreeElement
        function delete(s)
            % Protect against accessing properties
            % of partially constructed objects
            if ( ~isempty(s.geometric) && s.geometric~=-1 )
                s.geometric = [];
                s.id        = -1;
                s.parent    = [];
            elseif s.geometric~=-1
                disp('Data is empty')
            else
                % do nothing
            end            
        end
        
        function s = setNeighbor(s,dir,pos,element)
            s.neighbor{pos+1,dir} = element;
        end
        
        function neighborList = getNeighbors(s)
            if (s.isRefined)
                neighborList = [];
                for i=1:numel(s.ch)
                    neighborList = [neighborList; s.ch{i}.getNeighbors()];
                end
            else
                neighborList = s.getNeighborID;
            end
        end
        
        function neighborList = getNeighborID(s,dir,pos)
            if (nargin==2)
                neighborList = zeros(1,6);
                for n=1:6
                    if isempty(s.neighbor{n})
                        neighborList(1,n) = -1;
                    else
                        neighborList(1,n) = s.neighbor{n}.id;
                    end
                end
                if (nargin>1)
                    neighborList = neighborList(dir);
                end
            else
                neighborList = zeros(1,6);
            end
        end
        
        % Check if the neighbor on the 'side' is more coarse than the current element s
        function result = isMoreCoarse(s,neighbor)
            
            if ( ~isa(neighbor,'OctreeElement') && isenum(neighbor) )
                neighbor = s.neighbor{neighbor};
            end
            
            if (neighbor.isRefined)
%                 disp('Unexpected behaviour in oct.isMoreCoarse (0)')
                
                if (s.lvl <= neighbor.lvl)
                    result = false;
                elseif ( (s.lvl-neighbor.lvl)>1 )      
                    
                    disp('Unexpected behaviour in oct.isMoreCoarse (1)')
                    
                    
                    % change the neighbor designation to ensure there's only 1 level difference between the two neighbors
                    neighborPosition = s.getParent.getPosition;

                    switch side
                        case {Neighbor.Right, Neighbor.Left}
                            neighborPosition(1) = 3-neighborPosition(1);
                        case {Neighbor.Bottom, Neighbor.Top}
                            neighborPosition(2) = 3-neighborPosition(2);
                        case {Neighbor.Front, Neighbor.Back}
                            neighborPosition(3) = 3-neighborPosition(3);
                    end
                    
                    s.neighbor{side} = neighbor.ch{neighborPosition(1),neighborPosition(2),neighborPosition(3)};
                    result = s.isMoreCoarse(side);
                else
                    disp('Unexpected behaviour in oct.isMoreCoarse (2)')
                    
                    neighborPosition = s.position;

                    switch side
                        case {Neighbor.Right, Neighbor.Left}
                            neighborPosition(1) = 3-neighborPosition(1);
                        case {Neighbor.Bottom, Neighbor.Top}
                            neighborPosition(2) = 3-neighborPosition(2);
                        case {Neighbor.Front, Neighbor.Back}
                            neighborPosition(3) = 3-neighborPosition(3);
                    end

                    if ( (s.lvl-1) == neighbor.ch{neighborPosition(1),neighborPosition(2),neighborPosition(3)}.lvl )
                        s.neighbor{side} = neighbor.ch{neighborPosition(1),neighborPosition(2),neighborPosition(3)};
                        result = s.isMoreCoarse(side);
                        disp('Check isMoreCoarse function in oct.m - step 1');
                    elseif ( (s.lvl) == neighbor.ch{neighborPosition(1),neighborPosition(2),neighborPosition(3)}.lvl )
                        result = false;
                    else
                        disp('Check isMoreCoarse function in oct.m - step 2');
                    end
                end
            else
                result = (neighbor.getLv < s.getLv);
            end
        end
        
        % In case an element has been deleted the pointer does not exist 
        % anymore (~isvalid). Therefore a new pointer to the neighbor of 
        % the parent is created. This is a resursive function
        function out = getValidSide(s,pos,dir)
            if (isvalid(s.parent.neighbor{pos+1,dir}))
                out = s.parent.neighbor{pos+1,dir};
            else
                out = s.parent.getValidSide(pos,dir);
            end
        end
        
        % Function to check the neihbors, if all is ok it will return 'true'
        function status = checkNeighbors(s)
            status = true;
%             for side=1:6

            for dir=1:s.geometric.dimension
                for pos=0:1
                    
                    activeNeighbor = s.neighbor{pos+1,dir};
                    
                    if ~isempty(activeNeighbor)

                        oppositeNeighbor = activeNeighbor.neighbor{~pos+1,dir};
                        
                        % Check that the neighbors neighbor is the current oct
                        if ( s.lvl == activeNeighbor.lvl )

                            % the current has the same lvl as the neighbor
                            if ~(s==oppositeNeighbor)
                                status = false;
                                fprintf('Element %d is not the neighbor of element %d\n',s.localID,activeNeighbor.localID)
                            end

                        elseif (s.lvl-activeNeighbor.lvl)==1
                            % the current is more fine than the neighbor
                            if ~( s.parent == oppositeNeighbor )
                                status = false;
                                fprintf('Element %d is not the neighbor of element %d\n',s.localID,activeNeighbor.localID)
                            end

                        elseif (s.neighbor{side}.isRefined)
                            % the current is more coarse than the neighbor

                            for c=Neighbor.children(side)
                                if ~(s==s.neighbor{side}.ch{c}.neighbor{ Neighbor.opposite(side) })
                                    status = false;
                                    fprintf('Element %d is not the neighbor of element %d\n',s.localID,activeNeighbor.localID)
                                end
                            end
                        else
                            fprintf('Error with element %d\n',s.localID)
                        end
                    end
                end
            end
        end
        
        function updateNeighbors(s)
            for dir=1:s.geometric.dimension
                for pos=0:1
                    % check if the neighbor exists
                    try
                    if ~isempty(s.neighbor{pos+1,dir}) && isvalid(s.neighbor{pos+1,dir})

%                         if (s.neighbor{pos+1,dir}.isSolid)
%                             s.neighbor{pos+1,dir}=[];           % remove this neighbor if it's a solid
%                             s.parent = [];                      % by removing the parent it's ensured that this element will never coarsen
%                             return;
%                         end
                        
                        if (~isvalid(s.neighbor{pos+1,dir}))
                            s.neighbor{pos+1,dir} = s.getValidSide(pos,dir);
                        end

                        if ( (s.lvl == s.neighbor{pos+1,dir}.lvl) || (~s.neighbor{pos+1,dir}.isRefined && s.lvl>s.neighbor{pos+1,dir}.lvl) )
                            % do nothing, neighbor is already in correct range
                            % (equal level or one level coarser (if the neighbor 
                            % is not refined) than current element)
                        else
                            s.correctNeighbor(pos,dir);
                        end
                        
                    else
                        % If the neighbor is empty, check if the
                        % parent exists and has a neighbor on that side
                        if (~isempty(s.parent))
                            if ( ~isempty(s.parent.neighbor{pos+1,dir}) )
                                s.neighbor{pos+1,dir} = s.parent.neighbor{pos+1,dir};
                            end
                        end
                    end
                    
                    catch MExp
                        error('Problem in OctreeElement.updateNeighbors')
                    end
                    
                    % fix the connectivity of the parent too
                    if ~isempty(s.parent)
                        if ~isempty(s.parent.neighbor{pos+1,dir})

                            % In case an element has been deleted the pointer
                            % does not exist anymore (~isvalid). Therefore a
                            % new pointer to the neighbor of the parent is
                            % created.                        
                            if ( ~isvalid( s.parent.neighbor{pos+1,dir} ) )
                                s.parent.neighbor{pos+1,dir} = s.parent.getValidSide(pos,dir);
                            end

                            if ( (s.parent.lvl == s.parent.neighbor{pos+1,dir}.lvl) || (~s.parent.neighbor{pos+1,dir}.isRefined) )
                                % do nothing, neighbor is already in correct range
                                % (equal level or one level coarser (if the neighbor 
                                % is not refined) than current element)
                            else
                                s.parent.correctNeighbor(pos,dir);
                            end
                        end
                    end
                end
            end
        end

        function out = cornerIsMoreCoarse(s,dir1,pos1,dir2,pos2)
            out = false;
            
            activeNeighbor1 = s.neighbor{pos1+1,dir1};
            
            if (~isempty( activeNeighbor1 ))
                
                activeNeighbor2 = activeNeighbor1.neighbor{pos2+1,dir2};
                
                if (~isempty(activeNeighbor2))
                    if ( activeNeighbor2.lvl < s.lvl )
                        out = true;
                    end
                end
            end
        end
                
        % Check the levels of direct
        function numFlagged = applyRegularityCondition(s,numFlagged)  
                        
%             % Direct neighbor refinement check
%             ownLevel = s.targetLvl;            
%             nb = [s.neighbor{:}];
%             
%             minNeighborLevel = min( [nb.targetLvl] );
%             maxNeighborLevel = max( [nb.targetLvl] );
%             
%             if ( maxNeighborLevel-ownLevel > 1 || ownLevel-minNeighborLevel > 1 )
%                 for n=nb
%                     if (~isempty(n))
%                         if (n.targetLvl-s.targetLvl)>1
%                             s.targetLvl = n.targetLvl-1;
%                             numFlagged = numFlagged+1;
%                         elseif (s.targetLvl-n.targetLvl)>1
%                             n.targetLvl = s.targetLvl-1;
%                             numFlagged = numFlagged+1;
%                         end
%                     end
%                 end
%             end

%             % Diagonal refinement check
%             allLevels = s.geometric.allLevels;
%             if ( max(allLevels(:)) - min(allLevels(:)) >= 1 )
%                 pattern = allLevels == min(allLevels(:));
%                 
%                 % edges
%                 p = {1,1:3,3};
%                 blockIdx = permn([1,2,3],s.finite.dim);
%                 subElementIdx = blockIdx( sum(blockIdx==2,2)==1, :);
%                 for i=1:size(subElementIdx,1)
%                     p1 = p{subElementIdx(i,1)};
%                     p2 = p{subElementIdx(i,2)};
%                     if (s.finite.dim>2)
%                         p3 = p{subElementIdx(i,3)};
%                     else
%                         p3 = 1;
%                     end
%                     
%                     t = squeeze( pattern(p1,p2,p3) );
%                     if (all(t))
%                         if (p1==1)
%                             selectedNeighbor = s.neighbor{1,1};
%                         elseif (p1==3)
%                             selectedNeighbor = s.neighbor{2,1};
%                         else
%                             selectedNeighbor = s;
%                         end
%                         
%                         if ~isempty(selectedNeighbor)
%                             if (p2==1)
%                                 selectedNeighbor = selectedNeighbor.neighbor{1,2};
%                             elseif (p2==3)
%                                 selectedNeighbor = selectedNeighbor.neighbor{2,2};
%                             else
%                                 % do nothing
%                             end
%                         end
%                         
%                         if ~isempty(selectedNeighbor)
%                             if (s.finite.dim>2 && s.finite.refinementRatio(3)>1)
%                                 if (p3==1)
%                                     selectedNeighbor = selectedNeighbor.neighbor{1,3};
%                                 elseif (p3==3)
%                                     selectedNeighbor = selectedNeighbor.neighbor{2,3};
%                                 else
%                                     % do nothing
%                                 end
%                             end
%                         end
%                         
%                         if ~isempty(selectedNeighbor)
%                             if (s.lvl - selectedNeighbor.targetLvl > 1)
%                                 selectedNeighbor.targetLvl = selectedNeighbor.targetLvl+1;
%                                 numFlagged = numFlagged+1;
%                             end
%                         end
%                     end
%                 end
%                 
%                 % vertices
%                 
%             end

            % Developers note:
            % By using 'ownLevel' too rapid coarsening is prevented.
            % Imagine the following: an element is refined, all children
            % are level 4. One of the children (A) can be coarsened, so its
            % targetLvl becomes 3. An diagonal neighbor of this element (B) 
            % is currently level 3 (to ensure only 1 level difference with 
            % all its neighbors), but if it detects that all neighbors have
            % target levels of 3 or less it will set its targetLvl to 2. If
            % all its siblings then also have targetLvl of 2, this will
            % allow coarsening of that element. However, element A will not
            % be coarsened since not all of its siblings have a targetLvl 
            % of 2 yet (so it will remain at level 4). To prevent something
            % like this from happening, the reference level is set as the
            % maximum of the current lvl and the targetLvl.
            ownLevel = max(s.lvl,s.targetLvl);

%             if (~isempty(s.levelLimit))
%                 ownLevel = min(ownLevel,s.levelLimit);
%             end
            
            % Diagonal refinement check
            % Loop over all direct neighbors
            for dir1=1:s.dimension
                for pos1=1:2
                    
                    selectedNeighbor1 = s.neighbor{pos1,dir1};
                    
                    % If the direct neighbor is not empty, loop over their
                    % direct neighbors (except the ones in dir1 direction)
                    if (~isempty(selectedNeighbor1) && isvalid(selectedNeighbor1))
                        % direct neighbor (faces in 3D)
                        if (ownLevel - selectedNeighbor1.targetLvl)>1
                            selectedNeighbor1.targetLvl = selectedNeighbor1.targetLvl+1;
                            numFlagged = numFlagged+1;
                        end
                        
                        % For 2D meshes:
                        for dir2=1:s.dimension
                            if (dir2~=dir1)
                                for pos2=1:2
                                    
                                    selectedNeighbor2 = selectedNeighbor1.neighbor{pos2,dir2};
                                    
                                    % If the 2nd direct neighbor is not empty, loop over their
                                    % direct neighbors (except the ones in dir1 and dir2 direction)
                                    if (~isempty(selectedNeighbor2) && isvalid(selectedNeighbor2))
                                        % In-plane diagonal neighbor (edges in 3D)
                                        if (ownLevel - selectedNeighbor2.targetLvl)>1
                                            selectedNeighbor2.targetLvl = selectedNeighbor2.targetLvl+1;
                                            numFlagged = numFlagged+1;
                                        end
                                        
                                        % For 3D meshes:
                                        if (s.dimension>2)
                                            for dir3=1:3
                                                if (dir3~=dir2 && dir3~=dir1)
                                                    for pos3=1:2
                                                        selectedNeighbor3 = selectedNeighbor2.neighbor{pos3,dir3};

                                                        if (~isempty(selectedNeighbor3)  && isvalid(selectedNeighbor3))
                                                            % Out-of-plane diagonal neighbor (corners in 3D)
                                                            if (ownLevel - selectedNeighbor3.targetLvl)>1
                                                                selectedNeighbor3.targetLvl = selectedNeighbor3.targetLvl+1;
                                                                numFlagged = numFlagged+1;
                                                            end
                                                        end
                                                    end
                                                end
                                            end
                                        end
                                    end
                                end
                            end
                        end     
                    end
                end
            end
        end
        
        function out = directNeighbors(s)
            out = [];
            
            for d=1:s.finite.dim
                for p=0:1
                    selected = s.neighbor{p+1,d};
                    if (~isempty(selected) && isvalid(selected) )
                        if ( (s.targetLvl-selected.targetLvl) > 1 )
                            selected.targetLvl = selected.targetLvl+1;
                        end
                    end
                end
            end
        end
        
        function out = getElements(s,direction,activeMaterial)
            
            if (nargin==1)
                out = [];
                if (s.isRefined)
                    order = reshape(1:numel(s.children),size(s.children));
                    for i=order(:)'
                        out = [out; s.children{i}.getElements()];
                    end
                else
                    out = s;
                end
            
            elseif nargin==2 
                if (s.isRefined)
                    out = []; 

                    try 
                        if (nargin>1 && ~isempty(direction))
                            order = reshape(1:numel(s.children),size(s.ch));

                            if (~direction(1))
                                order = flipud(order);
                            end
                            if (~direction(2))
                                order = fliplr(order);
                            end

                            for i=order(:)'
                                out = [out; s.children{i}.getElements(direction)];
                            end
                        else

                            % Default behavior: 

                            order = reshape(1:numel(s.children),size(s.children));
                            for i=order(:)'
                                out = [out; s.children{i}.getElements([])];
                            end

                        end

                    catch MExp
                       disp('Problem in OctreeElement.getElements') 
                    end

                else
                    out = s;
                end
                
            elseif (nargin==3)
                if (s.isRefined)
                    out = []; 

                    try 

                        if (nargin>1 && ~isempty(direction))
                            order = reshape(1:numel(s.children),size(s.ch));

                            if (~direction(1))
                                order = flipud(order);
                            end
                            if (~direction(2))
                                order = fliplr(order);
                            end

                            for i=order(:)'
                                out = [out; s.children{i}.getElements(direction)];
                            end
                        else

                            % Default behavior: 

                            order = reshape(1:numel(s.children),size(s.children));
                            for i=order(:)'
                                out = [out; s.children{i}.getElements([],activeMaterial)];
                            end

                        end

                    catch MExp
                       disp('Problem in OctreeElement.getElements') 
                    end

                else
                    % return the element if the material matches, otherwise
                    % return an empty element
                    if s.checkMaterial(activeMaterial)
                        out = s;
                    else
                        out = [];
                    end
                end
            end
        end
        
        function out = getLv(s)
            if (numel(s)>1)
                out = s.getAllLv();
            else
                out = s.getElementLv();
            end
        end
        
        function out = getAllLv(s)
            out = zeros(s.getNumElements,1);
%             dim = s.getDim();
            
            position = 0;
            
            for k=1:size(s,3)
                for j=1:size(s,2)
                    for i=1:size(s,1)
                        numChild = s(i,j,k).getNumChild;
                        out(position+1:position+numChild) = s(i,j,k).getElementLv;
                        position = position + numChild;
                    end
                end                
            end
        end
        
        function out = getLvTree(s)
            if (s.isRefined)
                out = cell( s.refineFactor );
                for c=1:numel(out)
                    %if ( ~s.children{c}.isSolid )
                        out{c} = s.children{c}.getLvTree();
                    %else
                    %    out{c} = -99;
                    %end
                end
            else
                out = s.lvl;
            end
        end

        function lvl = getElementLv(s,num,activeMaterial)
            if (s.isRefined)
                lvl = [];
                for i=1:numel(s.children)
                    lvl = [lvl s.children{i}.getElementLv(num,activeMaterial)];
                end
            else
                if nargin==1 || (nargin==3 && s.checkMaterial(activeMaterial))
                    lvl = s.lvl;
                else
                    lvl = [];
                end
            end
            
            if nargin>2 && ~isempty(num)
                if length(lvl)<num
                    fprintf('Error in OCT: the requested child does not exist')
                    lvl = 0;
                else
                    lvl = lvl(num);
                end
            end
        end
        
        % This function checks 
        function checkLevel(s)
            if (s.isRefined)
                disp('Should not be here : oct.checkLevel')

            else
                for dir=1:s.disc.DIM
                    for pos=0:1
                        
                        activeNeighbor = s.neighbor{pos+1,dir};
                        
                        % First check if there is a neighbor at all
                        if ( ~isempty(activeNeighbor) )
                            
                            % If the current element's level is more than 1
                            % higher than the neighbor's level (the current 
                            % element is 2 levels finer than the neighbor) 
                            % ensure the neighbor is not allowed to coarsen 
                            % itself, and should refine itself
                            if ( (s.getLv - activeNeighbor.getLv) > 1 )
                                activeNeighbor.wantsRefinement = true;
                                activeNeighbor.wantsCoarsening = false;
                                
                            % If the levels are 1 apart, flag that the
                            % neighbor does not want to be removed. Next,
                            % do a check for diagonal elements
                            elseif ( (s.getLv - activeNeighbor.getLv) == 1 )     % diagonal level check (for example: a level 0 element shares a corner DOF with a level 2 element)
                                if (activeNeighbor.wantsRemoval)
                                    activeNeighbor.wantsRemoval = false;
                                end

                                for dir2=1:s.disc.DIM
                                    for pos2=0:1
                                        
                                        % Ensures only the Lvl of diagonally neighbors is checked
                                        if (dir~=dir2)
                                            
                                            diagonalNeighbor = activeNeighbor.neighbor{pos2+1,dir2};
                                            
                                            if ( ~isempty(diagonalNeighbor) )
                                                if ( (s.getLv - diagonalNeighbor.getLv) > 1 )
                                                    diagonalNeighbor.wantsRefinement = true;
                                                    diagonalNeighbor.wantsCoarsening = false;
                                                elseif ( ~diagonalNeighbor.isRefined && (s.getLv - diagonalNeighbor.getLv) == 1 && diagonalNeighbor.wantsRemoval)
                                                    diagonalNeighbor.wantsRemoval = false;
                                                elseif (diagonalNeighbor.wantsRemoval)
                                                    disp('')
                                                end
                                            end
                                        end
                                    end
                                end
%                                 
%                                 for j=1:6
%                                     if (i~=j && ~isempty(activeNeighbor.neighbor{j}))        % i~=j ensures only the Lvl of diagonally neighbors is checked (and it's own, but that's ok)
%                                         if ( (s.getLv - s.neighbor{i}.neighbor{j}.getLv) > 1 )
%                                             s.neighbor{i}.neighbor{j}.wantsRefinement = true;
%                                             s.neighbor{i}.neighbor{j}.wantsCoarsening = false;
%                                         elseif ( ~s.neighbor{i}.neighbor{j}.isRefined && (s.getLv - s.neighbor{i}.neighbor{j}.getLv) == 1 && s.neighbor{i}.neighbor{j}.wantsRemoval)
%                                             s.neighbor{i}.neighbor{j}.wantsRemoval = false;
%                                         elseif (s.neighbor{i}.neighbor{j}.wantsRemoval)
%                                             disp('')
%                                         end
%                                     end
%                                 end
                            end
                        end
                    end
                end
                
%                 for i=1:6
%                     if (~isempty(s.neighbor{i}))
%                         if ( (s.getLv - s.neighbor{i}.getLv) > 1 )
% %                             s.refine(lastID);
%                             s.neighbor{i}.wantsRefinement = true;
%                             s.neighbor{i}.wantsCoarsening = false;
%                         elseif ( (s.getLv - s.neighbor{i}.getLv) == 1 )     % diagonal level check (for example: a level 0 element shares a corner DOF with a level 2 element)
%                             if (s.neighbor{i}.wantsRemoval)
%                                 s.neighbor{i}.wantsRemoval = false;
%                             end
%                             
%                             for j=1:6
%                                 if (i~=j && ~isempty(s.neighbor{i}.neighbor{j}))        % i~=j ensures only the Lvl of diagonally neighbors is checked (and it's own, but that's ok)
%                                     if ( (s.getLv - s.neighbor{i}.neighbor{j}.getLv) > 1 )
%                                         s.neighbor{i}.neighbor{j}.wantsRefinement = true;
%                                         s.neighbor{i}.neighbor{j}.wantsCoarsening = false;
%                                     elseif ( ~s.neighbor{i}.neighbor{j}.isRefined && (s.getLv - s.neighbor{i}.neighbor{j}.getLv) == 1 && s.neighbor{i}.neighbor{j}.wantsRemoval)
%                                         s.neighbor{i}.neighbor{j}.wantsRemoval = false;
%                                     elseif (s.neighbor{i}.neighbor{j}.wantsRemoval)
%                                         disp('')
%                                     end
%                                 end
%                             end
%                         end
%                     end
%                 end
                
            end
        end
        
        function resetRefinementFlag(s)
            s.wantsRefinement = false;
        end

        %% Number of elements
       	function out = getNumElements(s)
            if (numel(s)>1)
                out = s.getTotalNumElements();
            else
                out = s.getNumChild();
            end
        end
        
        % Function to determine the total number of active elements (all
        % children included). The parents of children are not included!
        function numElements = getTotalNumElements(s)
            numElements = 0;
%             dim = s.getDim();
            for k=1:size(s,3)
                for j=1:size(s,2)
                    for i=1:size(s,1)
                        numElements = numElements + s(i,j,k).getNumChild;
                    end
                end
            end
        end
        
        function numChild = getNumChild(s,activeMaterial)
            if (s.isRefined)
                numChild = 0;
                if nargin==1
                    for i=1:numel(s.children)
                        numChild = numChild + s.children{i}.getNumChild();
                    end
                else
                    for i=1:numel(s.children)
                        numChild = numChild + s.children{i}.getNumChild(activeMaterial);
                    end
                end
            else
                if (nargin==1)
                    % return all children
                    numChild = 1;
                elseif (nargin==2 && s.checkMaterial(activeMaterial))
                    % return only children of material
                    numChild = 1;
                else
                    numChild = 0;
                end
           end
        end
        
        function out = getNumChildren(s)
            out = ones(1,4);
            if (s.isRefined)
                out(1:numel( size(s.ch) )) = size(s.ch);
            end
        end
        
        function child = getChild(s,num)
            child = s.getChildren(num);
        end
        
        function children = getChildren(s,num)
            if (s.isRefined)
                children = [];
                for i=1:numel(s.children)
                    children = [children; s.children{i}.getChildren()];
                end
            else
                children = s;
            end
            
            if nargin==2
                if length(children)<num
                    fprintf('Error in OCT: the requested child does not exist')
                    children = 0;
                else
                    children = children(num);
                end
            end
        end
                
        %% Get ID
       	function out = getID(s)
            if (numel(s)>1)
                out = s.getAllID();
            else
                out = s.getElementID();
            end
        end
        
        function ids = getElementID(s)
            if (s.isRefined)
                ids = [];
                for i=1:numel(s.ch)
                    ids = [ids s.ch{i}.getElementID()];
                end
            else
                ids = s.id;
            end
        end
        
        function out = getAllID(s)
            out = zeros(s.getNumElements,1);
            position = 0;
            
            for k=1:size(s,3)
                for j=1:size(s,2)
                    for i=1:size(s,1)
                        numChild = s(i,j,k).getNumChild;
                        out(position+1:position+numChild) = s(i,j,k).getID;
                        position = position + numChild;
                    end
                end
            end
        end
        
        function xgbl = getX(s)
            if (s.isRefined)
                xgbl = [];
                for i=1:numel(s.ch)
                    xgbl = [xgbl s.ch{i}.getX()];
                end
            else
                xgbl = s.xelem;
            end
        end
        
        function ygbl = getY(s)
            if (s.isRefined)
                ygbl = [];
                for i=1:numel(s.ch)
                    ygbl = [ygbl s.ch{i}.getY()];
                end
            else
                ygbl = s.yelem;
            end
        end
        
        function zgbl = getZ(s)
            if (s.isRefined)
                zgbl = [];
                for i=1:numel(s.ch)
                    zgbl = [tgbl s.ch{i}.getZ()];
                end
            else
                zgbl = s.zelem;
            end
        end
        
        function varargout = getNodes(s,trueCoordinates)  
            
            if (nargin<2)
                trueCoordinates = true;
            end
            
            varargout = cell(numel( s.nodes ),1);            
            
            if (trueCoordinates)            
                %% assign the node values to the correct direction
                x = s.nodes{1};
                y = s.nodes{2};
                z = s.nodes{3};
                t = s.nodes{4};

                isSpaceTime = (System.settings.time.method==TimeMethod.SpaceTime);
                
                %% change the assignment if spaceTime is used and
                if (isSpaceTime)
                    switch s.geometric.dimension
                        case 2
                            t = s.nodes{2} - System.settings.time.step + System.settings.time.current;
                            y = 0; z = 0;
                        case 3
                            t = s.nodes{3} - System.settings.time.step + System.settings.time.current;
                            z = 0;
                    end
                else
                    t = System.settings.time.current;
                end

                [ varargout{:} ] = ndgrid( x,y,z,t );
            
            else
                %% assign the node values to the correct direction
                x = s.nodes{1};
                y = s.nodes{2};
                z = s.nodes{3};
                t = s.nodes{4};
                
                [ varargout{:} ] = ndgrid( x,y,z,t );
                
                if nargout < numel( s.nodes )
                    if (numel( s.nodes )==4)
                        for i=1:nargout
                            varargout{i} = varargout{i}(:,:,:,1);
                        end
                    elseif (numel( s.nodes )==3)
                        for i=1:nargout
                            varargout{i} = varargout{i}(:,:,1);
                        end
                    elseif (numel( s.nodes )==2)
                        for i=1:nargout
                            varargout{i} = varargout{i}(:,1);
                        end                    
                    end

                elseif nargout > numel( s.nodes )
                    tmpNodes = cell(nargout,1);
                    tmpNodes(1:numel( s.nodes )) = s.nodes(:);
                    tmpNodes(numel( s.nodes )+1:nargout) = repmat({0},nargout-numel( s.nodes ),1);

                    varargout = cell(nargout,1);
                    [ varargout{:} ] = ndgrid( tmpNodes{:} );
                end
            end
        end
        
        function setPosition(s,varargin)
            s.position = [varargin{:}];
        end
        
        function setTargetLvl(s,value)
            s.targetLvl = value;
        end
        
        function out = getTargetLvl(s)
            out = s.targetLvl;
        end
        
        function be = getBoundaryElements(s,boundary)
            if (s.isRefined)
                be = [];
                for i=1:numel(s.ch)
                    be = [be s.ch{i}.getBoundaryElements(boundary)];
                end
            else
                if (s.nb(boundary)==-1)
                    be = s.id;
                else
                    be = [];
                end
            end
        end
        
        function out = isBoundaryElement(s,dir,pos)
            if (s.isRefined)
                disp('Should not be here: check isBoundaryElement in oct.m')
            else
                out = isempty(s.neighbor{pos+1,dir});
            end
        end

        function out = getRank(s)
            if (s.isRefined)
                out = [];
                for i=1:numel(s.ch)
                    out = [out s.ch{i}.getRank()];
                end
            else
                out = s.rank;
            end
        end
        
        % Returns the position of this element wrt its parent as a column 
        % array
        function out = getPosition(s)
            out = s.position;
            if (size(out,1)<size(out,2))
                out = out';
            end
        end
        
        function out = getW(s)
            out = s.finite.W * s.J;
        end
        
        function out = getW2(s)
            out = s.finite.W;
        end
        
        function out = getW_spatial(s)
            spatialJacobian = s.J / s.J(s.finite.dim);
            out = s.finite.W * spatialJacobian;
        end
        
        function out = getH(s,t1,t2)
            if nargin==3
                out = s.finite.H(t1,t2);
            else
                out = s.finite.H;
            end
        end
        
        function out = getDx(s)
            out = s.finite.D{1} / s.J(1);
        end
        
        function out = getDy(s)
            out = s.finite.D{2} / s.J(2);
        end

        function out = getDz(s)
            out = s.finite.D{3} / s.J(3);
        end
        
        function out = getDt(s)            
            out = s.finite.D{s.finite.dim} / s.J(s.finite.dim);
        end
        
        function out = getDxy(s)
            out = s.finite.Dxy / (s.J(1) * s.J(2));
        end
        
      	function out = getDyx(s)
            out = s.finite.Dyx / (s.J(1) * s.J(2));
        end

        function out = getDxz(s)
            out = s.finite.Dxz / (s.J(1) * s.J(3));
        end
        
        function out = getDyz(s)
            out = s.finite.Dyz / (s.J(2) * s.J(3));
        end
        
        function out = getDxx(s)
            out = s.finite.DD{1} / (s.J(1) * s.J(1));
            
            if (isempty(out))
                error('Dxx is empty - C0 basis functions do not have second derivative operators!')
            end
        end
        
        function out = getDyy(s)            
            out = s.finite.DD{2} / (s.J(2) * s.J(2));
            
            if (isempty(out))
                error('Dyy is empty - C0 basis functions do not have second derivative operators!')
            end
        end
        
        function out = getDzz(s)
            out = s.finite.DD{3} / (s.J(3) * s.J(3));
        end
        
     	function out = getDxt(s)
            out = s.finite.D_t{1} / (s.J(1) * s.J(s.finite.dim));
        end
        
     	function out = getDyt(s)
            out = s.finite.D_t{2} / (s.J(2) * s.J(s.finite.dim));
        end
        
     	function out = getDx_t(s)
            out = s.finite.D_t{1} / (s.J(1) * s.J(s.finite.dim));
        end
        
     	function out = getDy_t(s)
            out = s.finite.D_t{2} / (s.J(2) * s.J(s.finite.dim));
        end
        
     	function out = getDxx_t(s)
            out = s.finite.DD_t{1} / (s.J(1) * s.J(1) * s.J(s.finite.dim));
        end
        
     	function out = getDyy_t(s)
            out = s.finite.DD_t{2} / (s.J(2) * s.J(2) * s.J(s.finite.dim));
        end
        
        function out = getDxxx(s)
            out = s.finite.DDD{1} / (s.J(1) * s.J(1) * s.J(1));
        end
        
        function out = getDyyy(s)
            out = s.finite.DDD{2} / (s.J(2) * s.J(2) * s.J(2));
        end
        
        function out = getDxxy(s)
            out = s.finite.Dxxy / (s.J(1) * s.J(1) * s.J(2));
        end
        
        function out = getDyyx(s)
            out = s.finite.Dyyx / (s.J(2) * s.J(2) * s.J(1));
        end
        
        function out = getDxyx(s)
            out = s.disc.Dxyx / (s.J(1) * s.J(1) * s.J(2));
        end

        function out = getMatZ(s)
            out = s.finite.matZ;
        end

        function out = getVecZ(s)
            out = s.finite.vecZ;
        end
        
        function out = getLocations(s,disc)
            if (nargin==1)
                center = mean( vertcat( s.geometric.element(0).X ) );
                
                switch s.finite.dim
                    case 1
                        x = center(1) + s.finite.nodes{1} * s.J(1);
                    case 2
                        x = center(1) + s.finite.nodes{1} * s.J(1);
                        y = center(2) + s.finite.nodes{2} * s.J(2);
                    case 3
                        x = center(1) + s.finite.nodes{1} * s.J(1);
                        y = center(2) + s.finite.nodes{2} * s.J(2);
                        z = center(3) + s.finite.nodes{3} * s.J(3);
                end
                
                sDim = s.finite.sDim;
                
                if (sDim ~= s.finite.dim)
                    t = center(s.finite.dim) + s.finite.nodes{s.finite.dim} * s.J(s.finite.dim) + System.settings.time.current;
                else
                    t = System.settings.time.current;
                end
                
                out{1} = x;
                out{2} = y;
                out{3} = t;
            else
                dx = zeros(s.finite.dim,1);
                for i=1:s.finite.dim
                    dx(i) = s.nodes{i}(end) - s.nodes{i}(1);
                end
                
                out = cell(s.finite.dim,1);
                
                for i=1:s.finite.dim
                    out{i} = s.nodes{i}(1) + dx(i) * (disc.basis{i}.qp + 1)/2;
                end
                
%                 dx = s.xelem(end)-s.xelem(1); out{1} = s.xelem(1) + dx * (disc.xq + 1)/2;
%                 dy = s.yelem(end)-s.yelem(1); out{2} = s.yelem(1) + dy * (disc.yq + 1)/2;
%                 dz = s.zelem(end)-s.zelem(1); out{3} = s.zelem(1) + dz * (disc.zq + 1)/2 + System.settings.time.current;
            end
        end
        
        function setMaterial(s,material)
            s.material = material;
        end
        
        function out = getMaterial(s)
            if (numel(s)>1)
                % Material of an array of elements
                position = 0;
                for k=1:size(s,3)
                    for j=1:size(s,2)
                        for i=1:size(s,1)
                            numChild = s(i,j,k).getNumChild;
                            out(position+1:position+numChild) = s(i,j,k).getMaterial;
                            position = position + numChild;
                        end
                    end                
                end
            
            else
                % Material of a single element
                if (~s.isRefined)
                    out = s.material;
                else
                    out = [];
                    childElements = [s.children{:}];
                    temp = arrayfun( @(x) x.getMaterial, childElements, 'UniformOutput', false );
                    temp = [temp{:}];
                    out = [out, temp ];
                end
            end
        end
        
        function out = checkMaterial(s,activeMaterial)
            if (activeMaterial==s.material || activeMaterial==Material.All)
                out = true;
            else
                out = false;
            end
        end
        
        % The tag is used to label the geometric elements, which can be
        % used to select them easily when a boundary patch needs to be
        % created (to apply boundary conditions).
        function setTag(s,tag,wallMaterial)
            
            % Loop over all neighbors
            for n=1:numel(s.neighbor)
                
                % Convert the neighbor number to an edgeNumber
                switch n
                    case 1
                        edgeNumber = 3;
                    case 2
                        edgeNumber = 4;
                    case 3
                        edgeNumber = 1;
                    case 4
                        edgeNumber = 2;
                end
                
                % If the materials differ, tag the corresponding geometric element
                if ( ~isempty(s.neighbor{n}) && isvalid(s.neighbor{n}) )
                    if ( s.material ~= s.neighbor{n}.getMaterial )
                        s.geometric.setTag( tag, edgeNumber );
                    else
                        s.geometric.setTag( [], edgeNumber );
                    end
                    
                elseif ( isempty(s.neighbor{n}) )
                    if ( s.material ~= wallMaterial(n) )
                        s.geometric.setTag( tag, edgeNumber );
                    else
                        s.geometric.setTag( [], edgeNumber );
                    end
                    
                else
                    error('Problem with setting a tag! Why are we here?')
                end
            end
        end
        
        function [flag,geometricElements,direction,normal,edgePositions] = hasTag(s,tag)
            [flag,geometricElements,edgeFlag] = s.geometric.hasTag(tag);
            
            numGElements 	= numel(geometricElements);
            direction       = zeros(1,numGElements);
            normal          = zeros(1,numGElements);
            
            % Let the normal point to the element center, which requires to
            % flip the direction for the right (4) and upper (2) edges
            edgePositions = find(edgeFlag);
            
            for e=1:numGElements
                direction(e) = find( geometricElements(e).orientation );    % geometrically edges always point in the positive axis direction
                normal(e)    = find( geometricElements(e).normal );         % geometrically edge normals always point in the positive axis direction
                
                switch edgePositions(e)
                    case {2,4}
                        % flip
                        normal(e) = -normal(e);
                    case {1,3}
                        % no flip: do nothing 
                        %normal(e) = normal(e);
                end
            end
            
%             for e=1:numel(edgePositions)
%                 switch edgePositions(e)
%                     case {2,4}
%                         % flip
%                         normal(e) = -normal(e);
%                     case {1,3}
%                         % no flip: do nothing 
%                         %normal(e) = normal(e);
%                 end
%             end
        end
        
%         function setSolid(s,flag)
%             if (flag || flag==1)
%                 s.geometric.isSolid = true;
%             else
%                 s.geometric.isSolid = false;
%             end
%         end
        
%         function out = isSolid(s)
%             out = s.geometric.isSolid;
%         end

        function out = dimension(s)
            if ~isempty(s.geometric)
                out = s.geometric.dimension;
            else
                error('This OctreeElement does not have an associated geometric element')
            end
        end
        
        function out = globalID(s)
            out = s.id;
        end
        
        % Function to return the volume of this OctreeElement, which is
        % determined by summing all child volumes. Depending on the material 
        % it will return a zero volume, as they should be excluded.
        function out = getVolume(s)
            %if (System.settings.mesh.activeMaterial ~= s.material)
            if ( s.checkMaterial(s.material) )
                if (s.isRefined)
                    out = 0;
                    for c=1:4
                        out = out + s.children{c}.getVolume;
                    end
                else
                    out = (1/prod(s.refineFactor))^s.lvl;                   % for 1D 0.5^s.lvl; for 2D 0.25^s.lvl; for 3D 0.125^s.lvl is used
                end
            else
                out = 0;
            end
            
%             if (s.lvl==0)
%                 if (s.isRefined)
%                     out = 0;
%                     for c=1:numel(s.children)
%                         out = out + s.children{c}.getVolume;
%                     end
%                     
%                     
%                 else
%                     out = 1;                   % for 1D 0.5^s.lvl; for 2D 0.25^s.lvl; for 3D 0.125^s.lvl is used
%                 end
%             else
%                 
%             end
        end
    end
    
    methods (Access=private)
        % Recursive function to get the correct neighbor
        function correctNeighbor(s,pos,dir)
            if (s.lvl - s.neighbor{pos+1,dir}.lvl < 0)
                % the neighbor is more refined than this element itself, so
                % just adjust the neighbor to be the parent of the current
                % neighbor
                 s.neighbor{pos+1,dir} = s.neighbor{pos+1,dir}.getParent;               
                 
            else
                if (s.lvl - s.neighbor{pos+1,dir}.lvl == 1)
                    neighborPosition = s.position;
                else
                    % the current element is 2 or more levels finer than 
                    curParent = s.parent;
                    while (curParent.lvl - s.neighbor{pos+1,dir}.lvl ~= 1)
                        curParent = curParent.parent;
                    end
                    neighborPosition = curParent.getPosition;
                end
                
                if (neighborPosition{dir}>1)
                    neighborPosition{dir} = 1;
                else
                    neighborPosition{dir} = s.refineFactor(dir);
                end
                
                s.neighbor{pos+1,dir} = s.neighbor{pos+1,dir}.children{ neighborPosition{:} };
            end
            
            s.updateNeighbors;
        end
    end
end

