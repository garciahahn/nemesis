classdef GeometricVertex < GeometricElement
    %UNTITLED3 Summary of this class goes here
    %   Detailed explanation goes here
    
%     properties (Constant, Access = ?GeometricElement)
%         activeSwitch = [ 2 4 6 ];
%     end
    
    properties (Access = public)
        X;
    end
    
    properties (Access = ?GeometricElement)
        numCoordinates = 0;
        internalVertices;
    end
    
    methods
        function s = GeometricVertex(varargin)
            
            % Process input
            [coordinate,parent,logicalElement] = VerifyInput( varargin );
            
            % Initialize this geometric element
            s@GeometricElement(0,parent,logicalElement);
            s.X = coordinate;
            
            % Set the logicalElement (uses the passed or creates a new)
            s.setLogicalElement( logicalElement );
            
            if (~isnumeric(parent))
                s.boundaryCount = parent.boundaryCount;
            else
                s.boundaryCount = parent;
            end
        end
        
        function addCoordinate(s,coordinate)
            if ( isempty(s.X) )
                s.X = coordinate;
                s.numCoordinates = 1;
                s.periodic = false;
            else
                if (s.numCoordinates==1)
                    tmp = s.X;
                    s.X = cell(1,s.numCoordinates+1);
                    s.X{1} = tmp;
                end
                s.X{s.numCoordinates+1} = coordinate;
                s.numCoordinates = s.numCoordinates+1;
                s.periodic = true;
            end
        end
        
        function out = refine(s,refineFactor,refineFlag)
            if (nargin==2)
                refineFlag = s.refineFlag;
            end
            
            % Refine this edge if requested and if it's still active
            if (refineFlag && ~s.isRefined)
                s.createInternal(1);
                s.isRefined = true;
            end
            
            if (refineFactor ~= size(s.internalVertices))
                error('LSQDIM:GeometricVertex:refine :: refineFactor is not equal to the size of internalVertices')
            end
            
            % Return
            if (s.isRefined)
                out = s;
            else
                out = s.internalVertices;
            end
            
            s.active = false;
        end
        
        function coarsen(s)            
            for v = [s.internalVertices{:}]
                delete( v );
            end
            s.internalVertices = [];
            
            s.isRefined = false;
        end
        
        function delete(s)
            % do nothing
        end
        
        function createInternal(s,refineFactor)
            s.internalVertices  = cell(1,refineFactor);
            
            % internal vertex of this vertex
            s.internalVertices{1} = GeometricVertex( s.X, s );
            
            if (s.periodic)
                if ( isempty(s.leader.internalVertices) )
                    s.leader.createInternal(1);
                end
                s.internalVertices{1}.setPeriodic( s.leader.internalVertices{1} );
            end
                
            s.active = false;
        end
        
        function setPeriodic(s,leader)
            s.periodic = true;
            s.leader   = leader;
        end
        
        function updateActive(s,flag)  
            if (flag)
                if (s.maxDim==s.dimension)
                    if ( s.isRefined )
                        s.active = false;
                        cellfun( @(x) x.updateActive(true), s.allVertices );
                    else
                        s.active = flag;
                    end
                else
                    % Make this vertex active, and deactivate any internal elements
                    s.setActive(flag);

                    % If any parent is active already, ensure this child does
                    % not become active after all
                    parent = s.parent;
                    while ~isempty( parent )
                        if ( parent.active )
                            s.active = false;
                        end
                        parent = parent.parent;
                    end
                    %parent.active = flag;
                end
            end
        end
        
        function updateActivePeriodic(s)            
            if (s.maxDim==s.dimension)
                if ( s.isRefined )
                    cellfun( @(x) x.updateActivePeriodic(), s.allVertices );
                else
                    s.active = flag;                    
                end
            else
                if (s.periodic)
                    if (isvalid(s.leader))
                        if (s.leader.active~=s.active)

                            if ( s.leader.parent.level < s.level && s.leader.parent.active)
                                s.parent.setActive(true);
                            else
                                s.leader.parent.active = false;
                                s.leader.setActive(true);
                            end
                            
%                             if (~isempty(s.leader.parent))
%                                 s.leader.parent.active = false;
%                             end
%                             s.leader.setActive(true);
                        end
                    else
                        s.active = false;
                        s.parent.setActive(true);
                    end
                end
            end
        end
        
        function setActive(s,flag)
            try
                s.active = flag;
                if ( s.isRefined )
                    cellfun( @(x) x.setActive(false), s.internalVertices );
                end
            catch
               error('LSQDIM:element:GeometricVertex :: setActive issue'); 
            end
        end
        
        function resetActive(s,recursively)
            if (nargin==1)
                recursively = true;
            end
            
            s.active = false;
            
            if (recursively)
                if ( s.isRefined )
                    cellfun( @(x) x.resetActive(), s.internalVertices );
                end
            end
        end
        
        function out = getActive(s)
            if (~isempty(s.parent))
                out = s.parent.vertices;
            else
                error('should not be here')
            end
        end
        
        function setTag(s,tag)
            s.tag = tag;
            
            if (s.isRefined)
                arrayfun( @(x) x.setTag(tag), s.internalVertices );
            end
        end
        
        function plot(s,color)
            
            if (nargin==1)
                color = s.levelColors{s.level+1};
            end
            
            if (s.active)
                scatter3(s.X(:,1),s.X(:,2),s.X(:,3), color )
            end
        end
        
        function out = getActiveVertices(s)
            if (s.isRefined && ~s.active)
                % internal vertices
                out = s.internalVertices{1}.getActiveVertices;
            else
                out = {s};
                if (~s.active)
                    out = [];
                end
            end
        end
    end
end

