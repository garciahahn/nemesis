classdef GeometricFace < GeometricElement
    %UNTITLED3 Summary of this class goes here
    %   Detailed explanation goes here
    
%     properties (Constant)
%         activeSwitch = [ 1 1 2 ];
%     end
    
    properties
        normal;
        direction;
        edges;
        
        isSimple = false;
        
        internalVertices;
        internalEdges;
        internalFaces;
        
        allVertices;
        allEdges;
        
        %corners = GeometricVertex();
    end
    
    methods
        function s = GeometricFace(varargin)
            
            [edges,parent,logicalElement] = VerifyInput( varargin );
            
            if (numel(edges)~=4)
                error('Please provide 4 edges')
            end
            
            s@GeometricElement(2,parent,logicalElement);
            
            s.edges     = [edges{:}];
            
            % Set the logicalElement (uses the passed or creates a new)
            s.setLogicalElement( logicalElement );
            
            if (all([s.edges.periodic]))
                s.periodic = true;
            end
            
            for i=1:4
                s.edges(i).addUser(s);
            end
            
            s.boundaryCount = min( [s.edges(:).boundaryCount] );
                        
            s.computeNormal;            
            s.defineSimple();
            
            s.computeJacobian;
            
            %s.corners(1,4) = GeometricVertex();
            %s.corners(1,1:2) = s.edges(1).vertices;
            %s.corners(1,3:4) = s.edges(2).vertices;
        end
        
        function out = vertices(s,n)
            out = [s.edges(1).vertices s.edges(2).vertices];
            %out = s.corners;
            if (nargin>1)
                out = out(n);
            end
        end
        
        function out = faces(s,n)
            out = s;
            if (nargin>1)
                out = out(n);
            end
        end
        
        % Mapping from logical > physical
        function x = mapping(s,xi)
            if ( numel(xi)==2 )
                if ( all( arrayfun( @(x) inrange(x,-1,1), xi) ) )
                    
                    % Option 1
                    % Use the 4 vertices of this face to interpolate
%                     [v0,v1,v2,v3] = s.element(0).X;
%                     
%                     x = ( v0 * ( 1-xi(1) ) * ( 1-xi(2) ) + ...
%                           v1 * ( 1+xi(1) ) * ( 1-xi(2) ) + ...
%                           v2 * ( 1-xi(1) ) * ( 1+xi(2) ) + ...
%                           v3 * ( 1+xi(1) ) * ( 1+xi(2) ) ) / 4;
                      
                    % Option 2
                    % Use the 4 edges of this face to interpolate
                    x = ( s.edges(1).mapping( xi(1) ) * (1-xi(2)) + ...
                          s.edges(2).mapping( xi(1) ) * (1+xi(2)) + ...
                          s.edges(3).mapping( xi(2) ) * (1-xi(1)) + ...
                          s.edges(4).mapping( xi(2) ) * (1+xi(1)) ) / 4;
                    
                else
                    error('Only logical values between -1 and +1 are valid')
                end
            else
                error('Exactly 2 logical values between -1 and +1 are expected (received %d values)',numel(xi))
            end
        end
        
        function dx = derivative(s,direction,xi)
            switch direction
                case 1
                    dx = ( + s.edges(1).derivative( xi(1) ) * (1-xi(2)) ...
                           + s.edges(2).derivative( xi(1) ) * (1+xi(2)) ...
                           - s.edges(3).mapping( xi(2) ) ...                           
                           + s.edges(4).mapping( xi(2) ) ) / 4;
                       
                case 2
                    dx = ( - s.edges(1).mapping( xi(1) ) + ...
                           + s.edges(2).mapping( xi(1) ) ...
                           + s.edges(3).derivative( xi(2) ) * (1-xi(1)) + ...
                           + s.edges(4).derivative( xi(2) ) * (1+xi(1)) ) / 4;
            end
        end
        
        function out = computeJacobian(s,logicalCoordinate)
				
%             logicalCoordinate = [0 0];
%             
%             temp1 = s.derivative( 1, logicalCoordinate );
%             temp2 = s.derivative( 2, logicalCoordinate );
%             
%             dir1 = s.edges(1).orientation;
%             dir2 = s.edges(2).orientation;
% 
%             %s.J = ( (temp1(dir1) * temp2(dir2)) - (temp1(dir2) * temp2(dir1)) ); 
%             
%             s.J = dot(temp1,dir1) * dot(temp2,dir2) - dot(temp1,dir2) * dot(temp2,dir1);
%             
%             % Alternative
% %             tmp = [temp1(1:2); temp2(1:2)]
% %             s.J = det(tmp);
% 
%             out = s.J;
            
            if nargin==1
                logicalCoordinate = [1 1];
            end
            
            dXdxi = zeros(s.dimension,s.dimension);            
            for i=1:s.dimension
                tmp = s.derivative( i, logicalCoordinate );
                dXdxi(:,i) = tmp(1:s.dimension) / System.settings.phys.L0;
            end
            
            s.J     = dXdxi;
            s.detJ  = det( dXdxi );
            out     = s.detJ;
        end
        
        function out = refine(s,refineFactor,refineFlag)
            if nargin==2
                refineFlag = s.refineFlag;
            end
            
            if (refineFlag && ~s.isRefined)
                % This function creates all internal geometric objects,
                % which for a face are: internalVertices, internalEdges and
                % internalFaces. Only the latter needs to be returned.
                s.createInternal(refineFactor);
                s.isRefined = true;
            end
            
            if (refineFactor(1:2) ~= size(s.internalFaces))
                disp('')
            end
            
            if (s.isRefined)
                out = s.internalFaces;
            else
                out = s;
            end
            
            s.active = false;
            arrayfun( @(x) x.updateActive(false), s.vertices );
            arrayfun( @(x) x.updateActive(false), s.edges );
        end
        
        function coarsen(s)
                        
            % Remove the internalFaces as user of edges
            % This should delete the internalEdges
            if (all( isvalid( [s.internalFaces{:}] ) ))
                for f = [s.internalFaces{:}]
                    for e = f.edges
                        e.removeUser( f );
                    end
                end
            elseif (any( isvalid( [s.internalFaces{:}] ) ))
                error('Not all internalFaces are valid')
            else
                % normal behaviour
            end
            
            if (isempty( s.internalVertices ) || all( ~isvalid( [s.internalVertices{:}] ) ))
                 s.internalVertices = [];
            else
                error('Not all internalVertices have been deleted')
            end

            if (isempty( s.internalEdges ) || all( ~isvalid( [s.internalEdges{:}] ) ))
                s.internalEdges = [];
            else
                error('Not all internalEdges have been deleted')
            end
                        
            for f = [s.internalFaces{:}]
                delete( f );
            end
            s.internalFaces = [];
            
            s.isRefined = false; 
            
            % Check if the direct edges of this parent are still
            % valid (refined by another face), and update them accordingly
            for e=1:numel(s.edges)
                if (s.edges(e).isRefined)
                    if (~isempty(s.edges(e).internalEdges))
                        stillRefined = all( isvalid( [s.edges(e).internalEdges{:}] ) );
                        s.edges(e).isRefined = stillRefined;
                        if (~stillRefined)
                            s.edges(e).coarsen;
                        end
                    end
                end
            end
        end
        
        function delete(s)
            if ( all( isvalid( s.edges(:) ) ) )
                for e=1:4
                    s.edges(e).removeUser(s)
                end
            end
        end
                
        function createInternal(s,refineFactor)
            
            %% Vertices
            s.internalVertices  = cell(refineFactor(1)-1,refineFactor(2)-1);
            numInternalVertices = numel( s.internalVertices );
            
            % Determine the logical locations of the internal vertices in
            % the 2 directions
            x1 = linspace(-1,1,refineFactor(1)+1);
            x2 = linspace(-1,1,refineFactor(2)+1);
            
            % internal vertices of this face
            for j=2:refineFactor(2)
                for i=2:refineFactor(1)
                    coordinate = s.mapping([x1(i) x2(j)]);          % the logical location of the additional vertex
                    s.internalVertices{i-1,j-1} = GeometricVertex(coordinate,s);
                    
                    if (s.periodic)
                        if ( isempty(s.leader.internalVertices) )
                            s.leader.createInternal(refineFactor);
                        end
                        
                        s.internalVertices{i-1,j-1}.setPeriodic( s.leader.internalVertices{i-1,j-1} );
                    end
                end
            end
            
            % create internal vertices on edges
            s.edges(1).refine(refineFactor(1),1);
            s.edges(2).refine(refineFactor(1),1);
            s.edges(3).refine(refineFactor(2),1);
            s.edges(4).refine(refineFactor(2),1);
            
            % Fill the allVertices cell array
            allVertices = cell( refineFactor(1)+1, refineFactor(2)+1 );
            
            % Corners
            allVertices{  1 ,  1  } = s.edges(1).vertices(1);
            allVertices{ end,  1  } = s.edges(1).vertices(2);
            allVertices{  1 , end } = s.edges(2).vertices(1);
            allVertices{ end, end } = s.edges(2).vertices(2);
            
            % Edges
            try
            allVertices( 2:end-1 ,    1    ) = s.edges(1).internalVertices;     % dir 1 : lower edge internal vertices
            allVertices( 2:end-1 ,   end   ) = s.edges(2).internalVertices;     % dir 1 : upper edge internal vertices
            allVertices(    1    , 2:end-1 ) = s.edges(3).internalVertices;     % dir 2 : lower edge internal vertices
            allVertices(   end   , 2:end-1 ) = s.edges(4).internalVertices;     % dir 2 : upper edge internal vertices
            catch
                disp('')
            end
            % Core
%             tmp = reshape( s.internalVertices, size( allVertices( 2:end-1, 2:end-1 ) ) );
%             allVertices( 2:end-1, 2:end-1 ) = tmp;
            
            allVertices( 2:end-1, 2:end-1 ) = s.internalVertices;
            s.allVertices = allVertices;
            
            %% Edges
            numInternalEdges    = (refineFactor(1)-1) * refineFactor(2) + (refineFactor(1)) * (refineFactor(2)-1);
            s.internalEdges     = cell(1,numInternalEdges);
            
            % direction 1
            allEdges1 = cell( refineFactor(1), refineFactor(2)+1 );
            
            allEdges1( :,  1  ) = s.edges(1).internalEdges(:);
            allEdges1( :, end ) = s.edges(2).internalEdges(:);
            
            for j=2:refineFactor(2)
                for i=1:refineFactor(1)
                    allEdges1{i,j} = GeometricEdge( allVertices(i:i+1,j), s, [], s.edges(1).coordSystem ); 
                end
            end
            
            % direction 2
            allEdges2 = cell( refineFactor(1)+1, refineFactor(2) );
            allEdges2(  1  ,:) = s.edges(3).internalEdges(:);
            allEdges2( end ,:) = s.edges(4).internalEdges(:);            
            for i=2:refineFactor(1)
                for j=1:refineFactor(2)
                    allEdges2{i,j} = GeometricEdge( allVertices(i,j:j+1), s, [], s.edges(3).coordSystem );
                end
            end
            
            % Save all internalEdges
            s.internalEdges = vertcat( reshape( allEdges1(:,2:end-1), [ numel( allEdges1(:,2:end-1) ) 1] ), ...
                                       reshape( allEdges2(2:end-1,:), [ numel( allEdges2(2:end-1,:) ) 1] ) );
                           
            if (s.periodic)
                if ( isempty(s.leader.internalEdges) )
                    s.leader.createInternal(refineFactor);
                end
                
                for e=1:numInternalEdges
                    s.internalEdges{e}.setPeriodic( s.leader.internalEdges{e} );
                end
            end
                                   
            s.allEdges      = vertcat( allEdges1(:), allEdges2(:) );
            
            
%             allEdges1(:,   1   )    = s.edges(1).internalEdges(:);
%             tmp = reshape( s.internalEdges( 1:refineFactor(1) * (refineFactor(2)-1) ), size( allEdges1(:,2:end-1) ) );
%             allEdges1(:,2:end-1)    = tmp;
%             allEdges1(:,  end  )   	= s.edges(3).internalEdges(:);
            
%             % direction 2
%             allEdges2               = cell( refineFactor(1)+1, refineFactor(2) );
%             allEdges2(   1   ,:)    = s.edges(4).internalEdges(:)';
%             tmp = reshape( s.internalEdges( refineFactor(1) * (refineFactor(2)-1)+1 : end ), size(allEdges2(2:end-1,:)) );
%             allEdges2(2:end-1,:)    = tmp;
%             allEdges2(  end  ,:)   	= s.edges(2).internalEdges(:)';
                                  
            %% Faces
            numInternalFaces    = prod(refineFactor);
            s.internalFaces     = cell(refineFactor);
            
            for j=1:refineFactor(2)
                for i=1:refineFactor(1)
                    s.internalFaces{i,j} = GeometricFace( [ allEdges1(i,j:j+1) allEdges2(i:i+1,j)' ], s );
                    
                    if (s.periodic)
                        if ( isempty(s.leader.internalFaces) )
                            s.leader.createInternal(refineFactor);
                        end
                        
                        s.internalFaces{i,j}.setPeriodic( s.leader.internalFaces{i,j} );
                    end
                end
            end
            
%             % Add to feLists
%             if (s.withFELists)
%                 for v=1:numInternalVertices
%                     feLists{1}.insert(s.internalVertices{v});
%                 end
% 
%                 for e=1:numInternalEdges
%                     feLists{2}.insert(s.internalEdges{e});
%                 end
% 
%                 for f=1:numInternalFaces
%                     feLists{3}.insert(s.internalFaces{f});
%                 end
%             end
            
            % Remove this face as a user of the 4 edges, and remove the
            % edge if no users left
%             for i=1:4
%                 s.edges(i).removeUser(s);
%                 
%                 %if (s.edges(i).numUser==0)
%                 %    delete( s.edges(i) );
%                 %end
%             end            
        end
        
        function setTag(s,tag,direction)            
            if (s.isRefined)
                arrayfun( @(x) x.setTag(tag), s.internalFaces );
            else
                if (nargin==2)
                    arrayfun( @(x) x.setTag(tag), s.edges );                % tag all edges
                else
                    s.edges(direction).setTag(tag);
                end
            end
        end
        
        function [flag,edges,edgeFlag] = hasTag(s,tag)
            %faceFlag = strcmp( s.tag, tag );
            edgeFlag = arrayfun( @(x) strcmp( x.tag, tag ), s.edges);
            flag = any( edgeFlag );
            edges = [s.edges];
            edges = edges( edgeFlag );
        end
        
        function plot(s,id)
            if (s.active)
                if (nargin==1)
                    id = [];
                end
                
                vertices = s.element(0);

                coordinate1 = vertices(1).X; 
                coordinate2 = vertices(2).X;
                coordinate3 = vertices(3).X; 
                coordinate4 = vertices(4).X;

                center = ( coordinate1 + coordinate2 + coordinate3 + coordinate4 ) / 4;            
                scaling = 0.8;

                c1 = center + scaling * ( coordinate1 - center );
                c2 = center + scaling * ( coordinate2 - center );
                c3 = center + scaling * ( coordinate3 - center );
                c4 = center + scaling * ( coordinate4 - center );

                coordinate = [c1 ; c2; c3; c4];   
                x = reshape( coordinate(:,1), [ 2 2 ]);
                y = reshape( coordinate(:,2), [ 2 2 ]);
                z = reshape( coordinate(:,3), [ 2 2 ]);
                surf(x,y,z, 'FaceColor', s.levelColors{s.level+1} )
                
                if (s.maxDim == s.dimension)
                    text(mean(x(:)),mean(y(:)),mean(z(:)),num2str(id),'HorizontalAlignment','center')
                end
                
            else
                %error('This face is not active, but still in the list')
            end
        end
        
        function out = allDoF(s)
            % This function returns all DoF of this face element
            if (s.active)
                v = s.element(0,true);
                e = s.element(1,true);

                try
                out = [  v(1).dof   e(3).dof     v(3).dof
                         e(1).dof  	s.faces.dof  e(2).dof 
                         v(2).dof   e(4).dof     v(4).dof ];

                catch
                    disp('problem with GeometricFace.allDof')
                end
            else
                out = [];
            end
        end
        
        function out = allqDoF(s)
            % This function returns all DoF of this face element
            if (s.active)
                v = s.element(0,true);
                e = s.element(1,true);

                try
                out = [  v(1).qdof   e(3).qdof     v(3).qdof
                         e(1).qdof   s.faces.qdof  e(2).qdof 
                         v(2).qdof   e(4).qdof     v(4).qdof ];

                catch
                    disp('problem with GeometricFace.allqDof')
                end
            else
                out = [];
            end
        end
        
        function out = allLevels(s)
            % This function returns all levels of this face element
           
            onlyActive = false;
            
            v = s.element(0,onlyActive);
            e = s.element(1,onlyActive);
            
            try
            out = [  v(1).level   e(3).level     v(3).level
                     e(1).level   s.faces.level  e(2).level 
                     v(2).level   e(4).level     v(4).level ];
                    
            catch
                disp('problem with GeometricFace.allLevels')
            end
        end
        
        function out = allDofLevels(s)
            % This function returns all levels of this face element
            if (s.active)
                v = s.element(0,true);
                e = s.element(1,true);

                try
                out    = [  v(1).level*ones(size(v(1).dof))  e(3).level*ones(size(e(3).dof))        v(3).level*ones(size(v(3).dof))
                            e(1).level*ones(size(e(1).dof))  s.faces.level*ones(size(s.faces.dof))     e(2).level*ones(size(e(2).dof)) 
                            v(2).level*ones(size(v(2).dof))  e(4).level*ones(size(e(4).dof))        v(4).level*ones(size(v(4).dof))];

                catch MExp
                    error('LSQDIM:element:GeometricFace:allDofLevels')
                end
            else
                out = [];
            end
        end
        
        function out = getActiveVertices(s)
            out = [];
            
            if (s.isRefined && ~s.active)    
                % vertices within internal vertices
                tmp = cellfun( @(x) x.getActiveVertices, s.internalVertices, 'UniformOutput', false );
                tmp = vertcat( tmp{:} );  
                out = [ out; tmp(:) ];
                
                % vertices within internal edges
                tmp = cellfun( @(x) x.getActiveVertices, s.internalEdges, 'UniformOutput', false );
                tmp = vertcat( tmp{:} );  
                out = [ out; tmp(:) ];
                
                % vertices within internal faces
                tmp = cellfun( @(x) x.getActiveVertices, s.internalFaces, 'UniformOutput', false );
                tmp = vertcat( tmp{:} );
                out = [ out; tmp(:) ];
            end
        end
        
        function out = getActiveEdges(s)
            if (s.isRefined && ~s.active)
%                 % internal edges
%                 out = s.internalEdges(:);
                    
                out = [];

                % edges within internal edges
                tmp = cellfun( @(x) x.getActiveEdges, s.internalEdges, 'UniformOutput', false );
                tmp = vertcat( tmp{:} );
                out = [ out; tmp(:) ];
                
                % edges within internal faces                
                tmp = cellfun( @(x) x.getActiveEdges, s.internalFaces, 'UniformOutput', false );
                tmp = vertcat( tmp{:} );
                out = [ out; tmp(:) ];
            else
                out = [];
            end
        end
        
        function out = getActiveFaces(s)
            if (s.isRefined && ~s.active)
                % internal faces
                %out = s.internalFaces(:);
                
                % faces within internal faces                
                tmp = cellfun( @(x) x.getActiveFaces, s.internalFaces, 'UniformOutput', false );
                tmp = vertcat( tmp{:} );
%                 out = [ out; tmp(:) ];
                out = tmp;
            else
                if (s.active)
                    out = {s};
                else
                    out = [];
                end
            end
        end
        
        function setPeriodic(s,leader)
            s.periodic = true;
            s.leader   = leader;
        end
        
        function updateActive(s,flag)
            if (flag)
                if (s.maxDim == s.dimension)
                    if ( s.isRefined )
                        s.active = false;                                           % deactivate this face
                        %arrayfun( @(x) x.updateActive(false), s.vertices );
                        %arrayfun( @(x) x.updateActive(false), s.edges );
                        cellfun( @(x) x.updateActive(flag), s.internalFaces );      % activate internal faces (which will activate their own edges/vertices)
                    else
                        s.active = flag;                                            % activate this face
                        arrayfun( @(x) x.updateActive(flag), s.vertices );          % activate all vertices
                        arrayfun( @(x) x.updateActive(flag), s.edges );             % activate all edges
                    end

                else
                    % Make this face active, and deactivate any internal elements
                    s.setActive(flag);

                    % If any parent is active already, ensure this child does
                    % not become active after all
                    parent = s.parent;
                    while ~isempty( parent )
                        if ( parent.active )
                            s.active = false;
                        end
                        parent = parent.parent;
                    end
                    %parent.active = flag;
                end
            end
        end
        
        function updateActivePeriodic(s)
            if (s.maxDim == s.dimension)
                if ( s.isRefined )
                    cellfun( @(x) x.updateActivePeriodic(), s.internalFaces );
                else
                    %if (s.periodic)
                    %arrayfun( @(x) x.updateActivePeriodic(), s.edges(1).vertices );
                    %arrayfun( @(x) x.updateActivePeriodic(), s.edges(2).vertices );
                    arrayfun( @(x) x.updateActivePeriodic(), s.vertices );
                    arrayfun( @(x) x.updateActivePeriodic(), s.edges );
                    %end
                end

            else
                if (s.periodic)
                    if (isvalid(s.leader))
                        if (s.leader.active~=s.active)
                            
                            if ( s.leader.parent.level < s.level && s.leader.parent.active)
                                s.parent.setActive(true);
                            else
                                s.leader.parent.active = false;
                                s.leader.setActive(true);
                            end
    
%                             if (~isempty(s.leader.parent))
%                                 s.leader.parent.active = false;
%                             end
%                             s.leader.setActive(true);
                        end
                    else
                        s.active = false;
                        s.parent.setActive(true);
                    end
                end
            end
        end
        
        function makeSolid(s,flag)
            if (~s.isSolid && flag)
                s.isSolid = true;
                
                for e=s.edges
                    e.makeSolid(flag);
                end
            end
        end
        
        function setActive(s,flag)
            s.active = flag;
            if ( s.isRefined )
                cellfun( @(x) x.setActive(false), s.internalFaces );
                cellfun( @(x) x.setActive(false), s.internalEdges );
                cellfun( @(x) x.setActive(false), s.internalVertices );
            end
        end
        
        function resetActive(s,recursively)
            if (nargin==1)
                recursively = true;
            end
            
            s.active = false;
            
            if (recursively)
                if ( s.isRefined )
                    cellfun( @(x) x.resetActive(), s.internalFaces );
                    cellfun( @(x) x.resetActive(), s.internalEdges );
                    cellfun( @(x) x.resetActive(), s.internalVertices );
                end
                arrayfun( @(x) x.resetActive(), s.edges );
                arrayfun( @(x) x.resetActive(), s.vertices );
            end
        end
        
%         function out = getActiveEdgesChecked(s)
%             activeEdges = s.element(2,true);
%             
%             activeEdges = cell(1,6);
%             
%             for i=1:6
%                 activeVertices{i} = activeEdges(i).element(0,true,true);
%             end
% 
%             % combine the activeEdges to a full
%             % edge cell array
%             out = cell(1,12);
% 
%             out{1}  = GeometricHex.checkEdge( activeEdges{3},1, activeEdges{5},1 );
%             out{2}  = GeometricHex.checkEdge( activeEdges{4},1, activeEdges{5},2 );
%             out{3}  = GeometricHex.checkEdge( activeEdges{3},2, activeEdges{6},1 );
%             out{4}  = GeometricHex.checkEdge( activeEdges{4},2, activeEdges{6},2 );
%             out{5}  = GeometricHex.checkEdge( activeEdges{1},1, activeEdges{5},3 );
%             out{6}  = GeometricHex.checkEdge( activeEdges{2},1, activeEdges{5},4 );
%             out{7}  = GeometricHex.checkEdge( activeEdges{1},2, activeEdges{6},3 );
%             out{8}  = GeometricHex.checkEdge( activeEdges{2},2, activeEdges{6},4 );
%             out{9}  = GeometricHex.checkEdge( activeEdges{1},3, activeEdges{3},3 );
%             out{10} = GeometricHex.checkEdge( activeEdges{2},3, activeEdges{3},4 );
%             out{11} = GeometricHex.checkEdge( activeEdges{1},4, activeEdges{4},3 );
%             out{12} = GeometricHex.checkEdge( activeEdges{2},4, activeEdges{4},4 );
%             
%             if all( ~cellfun( @(x) x.active,out) )
%                 error('There is an inactive edge!')
%             end
%         end
    end
    
    methods (Access=private)
        
        function computeNormal(s)
            eOrientation = {s.edges.orientation};
            
            % check the orientation of the normal for each edge combination
            s.normal = cross( eOrientation{1}(1:3),eOrientation{3}(1:3));
            s.normal( abs(s.normal)<1e-13 ) = 0;
            
            % Verify if all combination give the same result (or a zero vector if the edge orientation is the same)
            for i=1:4
                for j=i+1:4
                    result = cross( eOrientation{i}(1:3),eOrientation{j}(1:3));
                    result( abs(result)<1e-13 ) = 0;
%                     if ~( all(~result) || isequal(s.normal,result) || isequal(s.normal,-result) )
%                         error('There is a problem with this plane')
%                     end
                end
            end            
        end
        
        function defineSimple(s)
            if norm( s.edges(1).orientation-s.edges(2).orientation ) < 1e-13
                if norm( s.edges(3).orientation-s.edges(4).orientation ) < 1e-13
                    s.isSimple = true;
                end
            end
        end

    end
end