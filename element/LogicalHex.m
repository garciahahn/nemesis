classdef LogicalHex < LogicalElement
    %UNTITLED3 Summary of this class goes here
    %   Detailed explanation goes here
    
%     properties (Constant)
%         activeSwitch = [ 1 1 1 2 ];
%     end
    
    properties (GetAccess=public)
        faces;
        isSimple = false;
        
        internalVertices;
    end
    
    properties (Access=private)
%         faceIDs;
%         faceDirections;
        
        internalEdges;
        internalFaces;
        internalHexes;
        
        allFaces;
        allEdges;
        allVertices;
    end
    
    methods
        function s = LogicalHex(varargin)
            
            [faces,parent,logicalElement] = VerifyInput( varargin );
            
            if (numel(faces)~=6)
                error('Please provide 6 faces')
            end
            
            s@LogicalElement(3,parent);
                        
            s.faces             = faces;
%             s.faceDirections    = [-1 1 -1 1 -1 1];
%             s.faceIDs           = [s.faces.id];
            
            for i=1:6
                s.faces(i).addUser(s);
            end
            
%             s.defineSimple();
%             s.computeJacobian;
        end
        
        function out = edges(s,n)
            out = [ s.faces(5).edges(1) s.faces(5).edges(2) s.faces(6).edges(1) s.faces(6).edges(2) ...
                    s.faces(1).edges(1) s.faces(2).edges(1) s.faces(1).edges(2) s.faces(2).edges(2) ...
                    s.faces(3).edges(3) s.faces(3).edges(4) s.faces(4).edges(3) s.faces(4).edges(4) ]; 
            if (nargin > 1)
                out = out(n);
            end
        end
        
        function out = vertices(s,n)
            out = [ s.faces(5).vertices s.faces(6).vertices ];
            if (nargin > 1)
                out = out(n);
            end
        end
        
        function out = hexes(s,n)
            out = s;
            if (nargin>1)
                out = out(n);
            end
        end
                
        function out = refine(s,refineFactor,refineFlag)
            if nargin==2
                refineFlag = s.refineFlag;
            end
            
            if (refineFlag && ~s.isRefined)
                % This function creates all internal geometric objects,
                % which for a face are: internalVertices, internalEdges and
                % internalFaces. Only the latter needs to be returned.
                s.createInternal(refineFactor);
                s.isRefined = true;
            end
            
            if ( any( refineFactor(1:numel(size(s.internalHexes))) ~= size(s.internalHexes) ) )
                error('Problem with the refineFactor in the GeometricHex')
            end
            
            if (s.isRefined)
                out = s.internalHexes;
            else
                out = s;
            end
            
            s.active = false;
        end
        
        function coarsen(s)
            
            if (s.isRefined)
                % Remove the internalHexes as user of faces
                % This should delete the internalEdges
                for h = [s.internalHexes{:}]
                    for f = h.faces
                        f.removeUser( h );
                    end
                end
                
                % Above for loop should delete:
                %   internalVertices
                %   internalEdges
                %   internalFaces
                %
                if ( isempty( s.internalVertices) || all( ~isvalid( [s.internalVertices{:}] ) ))
                     s.internalVertices = [];
                else
                    error('Not all internalVertices have been deleted')
                end
                
                if ( isempty( s.internalEdges) || all( ~isvalid( [s.internalEdges{:}] ) ))
                    s.internalEdges = [];
                else
                    error('Not all internalEdges have been deleted')
                end

                if ( isempty( s.internalFaces) || all( ~isvalid( [s.internalFaces{:}] ) ))
                    s.internalFaces = [];
                else
                    error('Not all internalFaces have been deleted')
                end
                
                % Check if the direct faces of this parent are still
                % refined, and update them accordingly
                for f=1:6
                    if (s.faces(f).isRefined)
                        stillRefined = all( isvalid( [s.faces(f).internalFaces{:}] ) );
                        s.faces(f).isRefined = stillRefined;
                        if (~stillRefined)
                            s.faces(f).coarsen;
                        end
                    end
                end
                
                for h = [s.internalHexes{:}]
                    delete( h );
                end
                s.internalHexes = [];
            end
            
            s.isRefined = false;
        end
        
        % Function to refine a hex, it will create the internal
        % GeometricElements
        function createInternal(s,refineFactor)
            
            %%%%%%%%%%%%%%
            %% Vertices %%
            %%%%%%%%%%%%%%
            s.internalVertices  = cell(refineFactor(1)-1,refineFactor(2)-1,refineFactor(3)-1);
            
            % Determine the logical locations of the internal vertices in
            % the 2 directions
            x1 = linspace(-1,1,refineFactor(1)+1);
            x2 = linspace(-1,1,refineFactor(2)+1);
            x3 = linspace(-1,1,refineFactor(3)+1);
            
            % internal vertices of this hex
            for k=2:refineFactor(3)
                for j=2:refineFactor(2)
                    for i=2:refineFactor(1)
                        coordinate = s.mapping([x1(i) x2(j) x3(k)]);          % the logical location of the additional vertex
                        s.internalVertices{i-1,j-1,k-1} = GeometricVertex(coordinate,s);
                    end
                end
            end
            
            refineFactor = refineFactor(1:3);            
            for dir=1:3
                flag = true(1,3); flag(dir) = false;
                rf = refineFactor(flag);
                
%                 if (dir==2)
%                     % for the y-direction, the z-refine factor should be
%                     % given first, followed by the x-refine factor
%                     rf = fliplr(rf);
%                 end
                
                for pos=1:2
                    s.faces( (dir-1)*2 + pos ).refine(rf,1);
                end
            end
            
            % Fill the allVertices cell array
            allVertices = cell( refineFactor(1)+1, refineFactor(2)+1, refineFactor(3)+1 );
            
            %% Corners
            corners = s.element(0);
            allVertices{  1 ,  1 ,  1  } = corners(1);
            allVertices{ end,  1 ,  1  } = corners(2);
            allVertices{  1 , end,  1  } = corners(3);
            allVertices{ end, end,  1  } = corners(4);
            allVertices{  1 ,  1 , end } = corners(5);
            allVertices{ end,  1 , end } = corners(6);
            allVertices{  1 , end, end } = corners(7);
            allVertices{ end, end, end } = corners(8);
            
            % Vertices within edges
            % Get the subElements with dimension 1, which is in fact the edge list. 
            % It contains 12 edges:
            %   1-4     :: x-direction
            %   5-8     :: y-direction 
            %   9-12    :: z-direction
            edges = s.element(1);            

            % x-direction
            allVertices( 2:end-1 ,    1    ,    1    ) = edges(1).internalVertices;
            allVertices( 2:end-1 ,   end   ,    1    ) = edges(2).internalVertices;
            allVertices( 2:end-1 ,    1    ,   end   ) = edges(3).internalVertices;
            allVertices( 2:end-1 ,   end   ,   end   ) = edges(4).internalVertices;
            
            % y-direction
            allVertices(    1    , 2:end-1 ,    1    ) = edges(5).internalVertices;
            allVertices(   end   , 2:end-1 ,    1    ) = edges(6).internalVertices;
            allVertices(    1    , 2:end-1 ,   end   ) = edges(7).internalVertices;
            allVertices(   end   , 2:end-1 ,   end   ) = edges(8).internalVertices;
            
            % z-direction
            allVertices(    1    ,    1    , 2:end-1 ) = edges(9).internalVertices;
            allVertices(   end   ,    1    , 2:end-1 ) = edges(10).internalVertices;
            allVertices(    1    ,   end   , 2:end-1 ) = edges(11).internalVertices;
            allVertices(   end   ,   end   , 2:end-1 ) = edges(12).internalVertices;
            
            % Vertices within faces
            % Get the subElements with dimension 2, which is in fact the face list. 
            % It contains 6 faces:
            %   1-2     :: x-normal
            %   3-4     :: y-normal 
            %   5-6     :: z-normal
            faces = s.element(2);
            allVertices(    1    , 2:end-1 , 2:end-1 ) = faces(1).internalVertices;
            allVertices(   end   , 2:end-1 , 2:end-1 ) = faces(2).internalVertices;
            allVertices( 2:end-1 ,    1    , 2:end-1 ) = faces(3).internalVertices;
            allVertices( 2:end-1 ,   end   , 2:end-1 ) = faces(4).internalVertices;
            allVertices( 2:end-1 , 2:end-1 ,    1    ) = faces(5).internalVertices;
            allVertices( 2:end-1 , 2:end-1 ,   end   ) = faces(6).internalVertices;
            
            % Vertices within the hex itself
%             tmp = reshape( s.internalVertices, size( allVertices( 2:end-1, 2:end-1, 2:end-1 ) ) );
            allVertices( 2:end-1, 2:end-1, 2:end-1 ) = s.internalVertices; %tmp;
            
            s.allVertices = allVertices;
            
            %%%%%%%%%%%
            %% Edges %%
            %%%%%%%%%%%
            internalEdges1 = cell((refineFactor(1)  ) , (refineFactor(2)-1) , (refineFactor(3)-1));
            internalEdges2 = cell((refineFactor(1)-1) , (refineFactor(2)  ) , (refineFactor(3)-1));
            internalEdges3 = cell((refineFactor(1)-1) , (refineFactor(2)-1) , (refineFactor(3)  ));
            
            numInternalEdges = numel(internalEdges1) + numel(internalEdges2) + numel(internalEdges3);
            
            % direction 1
            for k=2:refineFactor(3)
                for j=2:refineFactor(2)
                    for i=1:refineFactor(1)
                        internalEdges1{i,j-1,k-1} = GeometricEdge( allVertices(i:i+1,j,k), s );
                    end
                end
            end
            
            % direction 2
            for k=2:refineFactor(3)
                for i=2:refineFactor(1)
                    for j=1:refineFactor(2)
                        internalEdges2{i-1,j,k-1} = GeometricEdge( allVertices(i,j:j+1,k), s );
                    end
                end
            end
            
            % direction 3
            for j=2:refineFactor(2)
                for i=2:refineFactor(1)
                    for k=1:refineFactor(3)
                        internalEdges3{i-1,j-1,k} = GeometricEdge( allVertices(i,j,k:k+1), s );
                    end
                end
            end
            
            % Save all internalEdges
            s.internalEdges = vertcat( [internalEdges1(:)], [internalEdges2(:)], [internalEdges3(:)] );
            
            %% direction 1
            allEdges1 = cell( refineFactor(1), refineFactor(2)+1, refineFactor(3)+1 );
            
            % bottom (z=0) : faces(5)
            edgeSize = [ refineFactor(1) refineFactor(2)-1 ];
            tmpEdges = [s.faces(5).internalEdges( 1:prod(edgeSize) )];
            tmpEdges = reshape( tmpEdges, edgeSize );
            allEdges1(:,2:end-1,   1   ) = tmpEdges;
            
            % top (z=1) : faces(6)
            edgeSize = [ refineFactor(1) refineFactor(2)-1 ];
            tmpEdges = [s.faces(6).internalEdges( 1:prod(edgeSize) )];
            tmpEdges = reshape( tmpEdges, edgeSize );
            allEdges1(:,2:end-1,  end  ) = tmpEdges;
            
            % y=0 : faces(3)
            edgeSize = [ refineFactor(1) refineFactor(3)-1 ];
            tmpEdges = [s.faces(3).internalEdges( 1:prod(edgeSize) )];
            tmpEdges = reshape( tmpEdges, edgeSize );
            allEdges1(:,   1  , 2:end-1) = tmpEdges;
            
            % y=1 : faces(4)
            edgeSize = [ refineFactor(1) refineFactor(3)-1 ];
            tmpEdges = [s.faces(4).internalEdges( 1:prod(edgeSize) )];
            tmpEdges = reshape( tmpEdges, edgeSize );
            allEdges1(:,  end , 2:end-1) = tmpEdges;
            
            % corner edges: edges 1 - 4
            allEdges1(:,  1  ,  1  ) = edges(1).internalEdges;
            allEdges1(:, end ,  1  ) = edges(2).internalEdges;
            allEdges1(:,  1  , end ) = edges(3).internalEdges;
            allEdges1(:, end , end ) = edges(4).internalEdges;
            
            % core edges (local internal)
            allEdges1(:,2:end-1,2:end-1) = internalEdges1;

            % check
            %for k=1:size(allEdges1,3); for j=1:size(allEdges1,2); edge = [allEdges1{:,j,k}]; vert = [edge.vertices]; vertcat( vert.X ), end; end;
            
            %% direction 2
            allEdges2 = cell( refineFactor(1)+1, refineFactor(2), refineFactor(3)+1 );
            
            % bottom (z=0) : faces(5)
            edgeSize = [ refineFactor(1)-1 refineFactor(2) ];
            tmpEdges = [s.faces(5).internalEdges( end-prod(edgeSize)+1:end )];
            tmpEdges = reshape( tmpEdges, edgeSize );
            allEdges2(2:end-1,:,  1   ) = tmpEdges;
            
            % top (z=1) : faces(6)
            edgeSize = [ refineFactor(1)-1 refineFactor(2) ];
            tmpEdges = [s.faces(6).internalEdges( end-prod(edgeSize)+1:end )];
            tmpEdges = reshape( tmpEdges, edgeSize );
            allEdges2(2:end-1,:,  end  ) = tmpEdges;
            
            % x=0 : faces(1)
            edgeSize = [ refineFactor(2) refineFactor(3)-1 ];
            tmpEdges = [s.faces(1).internalEdges( 1:prod(edgeSize) )];
            tmpEdges = reshape( tmpEdges, edgeSize );
            allEdges2(1,   :  , 2:end-1) = tmpEdges;
            
            % x=1 : faces(2)
            edgeSize = [ refineFactor(2) refineFactor(3)-1 ];
            tmpEdges = [s.faces(2).internalEdges( 1:prod(edgeSize) )];
            tmpEdges = reshape( tmpEdges, edgeSize );
            allEdges2(end,  : , 2:end-1) = tmpEdges;
            
            % corner edges: edges 5 - 8
            allEdges2( 1 , : ,  1  ) = edges(5).internalEdges;
            allEdges2(end, : ,  1  ) = edges(6).internalEdges;
            allEdges2( 1 , : , end ) = edges(7).internalEdges;
            allEdges2(end, : , end ) = edges(8).internalEdges;
            
            % core edges (local internal)
            allEdges2(2:end-1,:,2:end-1) = internalEdges2;
            
            % check
            %for k=1:size(allEdges2,3); for i=1:size(allEdges2,1); edge = [allEdges2{i,:,k}]; vert = [edge.vertices]; vertcat( vert.X ), end; end;
            
            %% direction 3
            allEdges3 = cell( refineFactor(1)+1, refineFactor(2)+1, refineFactor(3) );
            
            % x=0 : faces(1)
            edgeSize = [ refineFactor(2)-1 refineFactor(3)  ];
            tmpEdges = [s.faces(1).internalEdges( end-prod(edgeSize)+1:end )];
            tmpEdges = reshape( tmpEdges, edgeSize );
            allEdges3(1,2:end-1,  : ) = tmpEdges;
            
            % x=1 : faces(2)
            edgeSize = [ refineFactor(2)-1 refineFactor(3) ];
            tmpEdges = [s.faces(2).internalEdges( end-prod(edgeSize)+1:end )];
            tmpEdges = reshape( tmpEdges, edgeSize );
            allEdges3(end,2:end-1,: ) = tmpEdges;
            
            % y=0 : faces(3)
            edgeSize = [ refineFactor(1)-1 refineFactor(3) ];
            tmpEdges = [s.faces(3).internalEdges( end-prod(edgeSize)+1:end )];
            tmpEdges = reshape( tmpEdges, edgeSize );
            allEdges3(2:end-1,   1  , :) = tmpEdges;
            
            % y=1 : faces(4)
            edgeSize = [ refineFactor(1)-1 refineFactor(3) ];
            tmpEdges = [s.faces(4).internalEdges( end-prod(edgeSize)+1:end )];
            tmpEdges = reshape( tmpEdges, edgeSize );
            allEdges3(2:end-1, end , : ) = tmpEdges;
            
            % corner edges: edges 9 - 12
            allEdges3( 1 , 1 , : ) = edges(9).internalEdges;
            allEdges3(end, 1 , : ) = edges(10).internalEdges;
            allEdges3( 1 ,end, : ) = edges(11).internalEdges;
            allEdges3(end,end, : ) = edges(12).internalEdges;
            
            % core edges (local internal)
            allEdges3(2:end-1,2:end-1,:) = internalEdges3;
            
            s.allEdges = vertcat( allEdges1(:), allEdges2(:), allEdges3(:) );
            
            % check
            %for j=1:size(allEdges3,2); for i=1:size(allEdges3,1); edge = [allEdges3{i,j,:}]; vert = [edge.vertices]; vertcat( vert.X ), end; end;
            
            %%%%%%%%%%%
            %% Faces %%
            %%%%%%%%%%%            
            allFaces1 = cell((refineFactor(1)+1) , (refineFactor(2)  ) , (refineFactor(3)  ));
            allFaces2 = cell((refineFactor(1)  ) , (refineFactor(2)+1) , (refineFactor(3)  ));
            allFaces3 = cell((refineFactor(1)  ) , (refineFactor(2)  ) , (refineFactor(3)+1));
            
            % faces with normal in direction 1
            allFaces1(  1  , : , : ) = s.faces(1).internalFaces;        % x0 face
            allFaces1( end , : , : ) = s.faces(2).internalFaces;        % x1 face            
            for k=1:refineFactor(3)
                for j=1:refineFactor(2)
                    for i=2:refineFactor(1)
                        allFaces1{i,j,k} = GeometricFace( [ squeeze( allEdges2(i,j,k:k+1) )' allEdges3(i,j:j+1,k) ], s );
                    end
                end
            end
            
            % faces with normal in direction 2
            allFaces2(  : ,  1  , : ) = s.faces(3).internalFaces;        % y0 face
            allFaces2(  : , end , : ) = s.faces(4).internalFaces;        % y1 face 
            for k=1:refineFactor(3)
                for j=2:refineFactor(2)
                    for i=1:refineFactor(1)
                        allFaces2{i,j,k} = GeometricFace( [ squeeze( allEdges1(i,j,k:k+1) )' allEdges3(i:i+1,j,k)' ], s );
                    end
                end
            end
            
            % faces with normal in direction 3
            allFaces3(  : , : ,  1  ) = s.faces(5).internalFaces;        % z0 face
            allFaces3(  : , : , end ) = s.faces(6).internalFaces;        % z1 face 
            for k=2:refineFactor(3)
                for j=1:refineFactor(2)
                    for i=1:refineFactor(1)
                        allFaces3{i,j,k} = GeometricFace( [ allEdges1(i,j:j+1,k) allEdges2(i:i+1,j,k)' ], s );
                    end
                end
            end
            
            % Save all internalFaces
            s.internalFaces = vertcat( reshape( allFaces1(2:refineFactor(1),:,:), [numel(allFaces1(2:refineFactor(1),:,:)) 1] ), ...
                                       reshape( allFaces2(:,2:refineFactor(2),:), [numel(allFaces2(:,2:refineFactor(2),:)) 1] ), ...
                                       reshape( allFaces3(:,:,2:refineFactor(3)), [numel(allFaces3(:,:,2:refineFactor(3))) 1] ) );
                                   
            s.allFaces = vertcat( allFaces1(:), allFaces2(:), allFaces3(:) );
            
            %%%%%%%%%%%
            %% Hexes %%
            %%%%%%%%%%%
            %numInternalHexes    = prod(refineFactor);
            s.internalHexes     = cell(refineFactor);
            
            for k=1:refineFactor(3)
                for j=1:refineFactor(2)
                    for i=1:refineFactor(1)
                        s.internalHexes{i,j,k} = GeometricHex( [ allFaces1(i:i+1,j,k)' allFaces2(i,j:j+1,k) squeeze(allFaces3(i,j,k:k+1))' ], s );
                    end
                end
            end           
        end
        
        function out = allDoF(s)
            % This function returns all DoF of this hex element. The
            % construction is split in 3 layers:
            %   layer 1 : z=0 face
            %   layer 3 : z=1 face
            %   layer 2 : everything in between
            
            v = s.element(0,true);
            e = s.element(1,true);
            f = s.element(2,true);
            
            try
            layer1 = [  v(1).dof   e(5).dof     v(3).dof
                        e(1).dof   f(5).dof     e(2).dof 
                        v(2).dof   e(6).dof     v(4).dof];
                    
            layer2 = [  e(9).dof   f(1).dof     e(11).dof 
                        f(3).dof   s.hexes.dof  f(4).dof
                        e(10).dof  f(2).dof     e(12).dof ];
                    
            layer3 = [  v(5).dof   e(7).dof     v(7).dof 
                        e(3).dof   f(6).dof     e(4).dof
                        v(6).dof   e(8).dof     v(8).dof ];
                    
            catch
                error('LSQDIM:element:GeometricHex:allDoF')
            end
                    
            out = cat(3,layer1,layer2,layer3);
            
        end
        
        function out = allIsRefined(s)
            % This function returns all levels of this face element
            v = s.element(0,true);
            e = s.element(1,true);
            f = s.element(2,true);
            
            try
            layer1 = [  v(1).isRefined  e(5).isRefined      v(3).isRefined
                        e(1).isRefined  f(5).isRefined      e(2).isRefined 
                        v(2).isRefined  e(6).isRefined      v(4).isRefined];
                    
            layer2 = [  e(9).isRefined  f(1).isRefined      e(11).isRefined 
                        f(3).isRefined  s.hexes.isRefined   f(4).isRefined
                        e(10).isRefined f(2).isRefined      e(12).isRefined ];
                    
            layer3 = [  v(5).isRefined  e(7).isRefined      v(7).isRefined 
                        e(3).isRefined  f(6).isRefined      e(4).isRefined
                        v(6).isRefined  e(8).isRefined      v(8).isRefined ];
                    
            catch
                error('LSQDIM:element:GeometricHex:allIsRefined')
            end
                    
            out = cat(3,layer1,layer2,layer3);
        end
        
        function out = allLevels(s)
            % This function returns all levels of this face element
            v = s.element(0,true);
            e = s.element(1,true);
            f = s.element(2,true);
            
            try
            layer1 = [  v(1).level  e(5).level      v(3).level
                        e(1).level  f(5).level      e(2).level 
                        v(2).level  e(6).level      v(4).level];
                    
            layer2 = [  e(9).level  f(1).level      e(11).level 
                        f(3).level  s.hexes.level   f(4).level
                        e(10).level f(2).level      e(12).level ];
                    
            layer3 = [  v(5).level  e(7).level      v(7).level 
                        e(3).level  f(6).level      e(4).level
                        v(6).level  e(8).level      v(8).level ];
                    
            catch
                error('LSQDIM:element:GeometricHex:allLevels')
            end
                    
            out = cat(3,layer1,layer2,layer3);
        end
        
        function out = allDofLevels(s)
            % This function returns all levels of this face element
            v = s.element(0,true);
            e = s.element(1,true);
            f = s.element(2,true);
            
            try
            layer1 = [  v(1).level*ones(size(v(1).dof))  e(5).level*ones(size(e(5).dof))        v(3).level*ones(size(v(3).dof))
                        e(1).level*ones(size(e(1).dof))  f(5).level*ones(size(f(5).dof))        e(2).level*ones(size(e(2).dof)) 
                        v(2).level*ones(size(v(2).dof))  e(6).level*ones(size(e(6).dof))        v(4).level*ones(size(v(4).dof))];
                    
            layer2 = [  e(9).level*ones(size(e(9).dof))  f(1).level*ones(size(f(1).dof))        e(11).level*ones(size(e(11).dof)) 
                        f(3).level*ones(size(f(3).dof))  s.hexes.level*ones(size(s.hexes.dof))  f(4).level*ones(size(f(4).dof))
                        e(10).level*ones(size(e(10).dof)) f(2).level*ones(size(f(2).dof))       e(12).level*ones(size(e(12).dof)) ];
                    
            layer3 = [  v(5).level*ones(size(v(5).dof))  e(7).level*ones(size(e(7).dof))      v(7).level*ones(size(v(7).dof)) 
                        e(3).level*ones(size(e(3).dof))  f(6).level*ones(size(f(6).dof))      e(4).level*ones(size(e(4).dof))
                        v(6).level*ones(size(v(6).dof))  e(8).level*ones(size(e(8).dof))      v(8).level*ones(size(v(8).dof)) ];
                    
            catch
                error('LSQDIM:element:GeometricHex:allDofLevels')
            end
                    
            out = cat(3,layer1,layer2,layer3);
        end
        
        function out = getActiveVertices(s)
            if (s.isRefined && ~s.active)
                % internal vertices
                out = s.internalVertices(:);
                
                % vertices within internal edges
                tmp = cellfun( @(x) x.getActiveVertices, s.internalEdges, 'UniformOutput', false );
                tmp = vertcat( tmp{:} );  
                out = [ out; tmp(:) ];
                
                % vertices within internal faces
                tmp = cellfun( @(x) x.getActiveVertices, s.internalFaces, 'UniformOutput', false );
                tmp = vertcat( tmp{:} );
                out = [ out; tmp(:) ];
                
                % vertices within internal hexes
                tmp = cellfun( @(x) x.getActiveVertices, s.internalHexes, 'UniformOutput', false );
                tmp = vertcat( tmp{:} );
                out = [ out; tmp(:) ];
            else
                out = [];
            end
        end
        
        function out = getActiveEdges(s)
            if (s.isRefined && ~s.active)
                % internal edges
%                 out = s.internalEdges(:);
                
                out = [];

                % edges within internal edges
                tmp = cellfun( @(x) x.getActiveEdges, s.internalEdges, 'UniformOutput', false );
                tmp = vertcat( tmp{:} );
                out = [ out; tmp(:) ];
                
                % edges within internal faces                
                tmp = cellfun( @(x) x.getActiveEdges, s.internalFaces, 'UniformOutput', false );
                tmp = vertcat( tmp{:} );
                out = [ out; tmp(:) ];
                
                % edges within internal hexes                
                tmp = cellfun( @(x) x.getActiveEdges, s.internalHexes, 'UniformOutput', false );
                tmp = vertcat( tmp{:} );
                out = [ out; tmp(:) ];
            else
                out = [];
            end
        end
        
        function out = getActiveFaces(s)
            if (s.isRefined && ~s.active)
                % internal edges
                %out = s.internalFaces(:);
                
                out = [];
                
                % faces within internal faces                
                tmp = cellfun( @(x) x.getActiveFaces, s.internalFaces, 'UniformOutput', false );
                tmp = vertcat( tmp{:} );
                out = [ out; tmp(:) ];
                
                % faces within internal hexes                
                tmp = cellfun( @(x) x.getActiveFaces, s.internalHexes, 'UniformOutput', false );
                tmp = vertcat( tmp{:} );
                out = [ out; tmp(:) ];
            else
                out = [];
            end
        end
        
        function out = getActiveHexes(s)
            if (s.isRefined && ~s.active)
                % internal edges
%                 out = s.internalHexes(:);
                
                % hexes within internal hexes                
                tmp = cellfun( @(x) x.getActiveHexes, s.internalHexes, 'UniformOutput', false );
                tmp = vertcat( tmp{:} );
%                 out = [ out; tmp(:) ];
                out = tmp;
            else
                out = {s};
            end
        end
    
        function updateActive(s,flag)
            if (s.maxDim == s.dimension)
                if ( s.isRefined )
                    s.active = false;
                    cellfun( @(x) x.updateActive(true), s.internalHexes );
                else
                    s.active = true;
                    arrayfun( @(x) x.updateActive(true), s.vertices );
                    arrayfun( @(x) x.updateActive(true), s.edges );
                    arrayfun( @(x) x.updateActive(true), s.faces );
                end
                
            else
                % Make this edge active, and deactivate any internal elements
                s.setActive(true);
                
                % If any parent is active already, ensure this child does
                % not become active after all
                parent = s.parent;
                while ~isempty( parent )
                    if ( parent.active )
                        s.active = false;
                    end
                    parent = parent.parent;
                end
            end
        end
        
        function setActive(s,flag)
            s.active = flag;
            if ( s.isRefined )
                cellfun( @(x) x.setActive(false), s.internalHexes );
                cellfun( @(x) x.setActive(false), s.internalFaces );
                cellfun( @(x) x.setActive(false), s.internalEdges );
                cellfun( @(x) x.setActive(false), s.internalVertices );
            end
        end  
        
        function out = getActiveEdgesChecked(s)
            activeFaces = s.element(2,true,true);
            
            activeEdges = cell(1,6);
            
            for i=1:6
                activeEdges{i} = activeFaces(i).element(1,true,false);
            end

            % combine the activeEdges to a full
            % edge cell array
            out = cell(1,12);

            out{1}  = GeometricHex.checkEdge( activeEdges{3},1, activeEdges{5},1 );
            out{2}  = GeometricHex.checkEdge( activeEdges{4},1, activeEdges{5},2 );
            out{3}  = GeometricHex.checkEdge( activeEdges{3},2, activeEdges{6},1 );
            out{4}  = GeometricHex.checkEdge( activeEdges{4},2, activeEdges{6},2 );
            out{5}  = GeometricHex.checkEdge( activeEdges{1},1, activeEdges{5},3 );
            out{6}  = GeometricHex.checkEdge( activeEdges{2},1, activeEdges{5},4 );
            out{7}  = GeometricHex.checkEdge( activeEdges{1},2, activeEdges{6},3 );
            out{8}  = GeometricHex.checkEdge( activeEdges{2},2, activeEdges{6},4 );
            out{9}  = GeometricHex.checkEdge( activeEdges{1},3, activeEdges{3},3 );
            out{10} = GeometricHex.checkEdge( activeEdges{2},3, activeEdges{3},4 );
            out{11} = GeometricHex.checkEdge( activeEdges{1},4, activeEdges{4},3 );
            out{12} = GeometricHex.checkEdge( activeEdges{2},4, activeEdges{4},4 );
            
            if all( ~cellfun( @(x) x.active,out) )
                error('There is an inactive edge!')
            end
        end
    end
    
    methods (Access=private)
        function defineSimple(s)
            if isequal( s.faces(1).normal, s.faces(2).normal )
                if isequal( s.faces(3).normal, s.faces(4).normal )
                    if isequal( s.faces(5).normal, s.faces(6).normal )
                        s.isSimple = true;
                    end
                end
            end
        end
    end
    
    methods (Static)
        function out = checkEdge(face1,edgePos1,face2,edgePos2)
            if (face1(edgePos1).active && face2(edgePos2).active)
                if (face1(edgePos1)==face2(edgePos2))
                    out = face1(edgePos1);
                else
                    error('Both edges are active, but they are not identical!')
                end
            elseif (face1(edgePos1).active)
                out = face1(edgePos1);
            elseif (face2(edgePos2).active)
                out = face2(edgePos2);
            else                
                if (face1(edgePos1).level < face2(edgePos2).level)
                    out = face1(edgePos1).parent.edges( edgePos2 );
                elseif (face1(edgePos1).level > face2(edgePos2).level)
                    out = face2(edgePos2).parent.edges( edgePos1 );
                else
                    error('None of the edges is active!')
                end
            end
        end
    end
end