classdef LogicalElement < handle
    %LOGICALELEMENT
    %   A LogicalElement is the base for LogicalVertex, LogicalEdge,
    %   LogicalFace and LogicalHex. A LogicalElement does not contain any
    %   geometric information (location data), it only contains logical
    %   connection information of this LogicalElement with respect to other
    %   LogicalElements. The degrees of freedom are determined inside the 
    %   logical elements. This implies that when a GeometricElement shares 
    %   a LogicalElement with other GeometricElements, internally all are
    %   identical (except their locations). In that case it is a periodic 
    %   boundary.
    
    properties (Constant, Access=protected)
        withUserInfo    = false;
        levelColors     = { 'r', 'b', 'g', 'c', 'm' };
    end
    
    properties (Access=protected)
        maxDim;
        boundaryCount = 0;
        userCount = 0;
    end
        
    properties (GetAccess=public)
        parent;
        
        id;
        dimension;      % feObject dimension
        dofObject;
        refineFlag = false;
        coarsenFlag = false;
        isRefined  = false;
        
        feType = 1;
        
        level;
        
        user;        
        active = false;
    end
        
    methods
        function s = LogicalElement(dimension,parent)
            s.maxDim    = System.settings.mesh.dim;
            s.dimension = dimension;
            
            if (~isnumeric(parent))
                s.parent = parent;
            else
                s.parent = [];
            end
            
            if ( isempty(s.parent) )
                s.level = 0;
            else
                s.level = s.parent.level + 1;
            end
            
            s.dofObject = DoF();
        end
        
        % Reset the current GeometricElement and all its parents
        function reset(s)
            s.dofObject.reset();
            if (~isempty(s.parent))
                s.parent.reset();
            end
            if (s.isRefined)
                disp('')
            end
        end
        
        function out = dof(s,includePeriodicity)
            
            if (nargin==1)
                includePeriodicity = true; 
            end
            
            if ~isempty(s.dofObject.get)
%                 out = s.dof;
                out = s.dofObject.get;
            else
                if (s.periodic && includePeriodicity)
                    out = s.leader.dof;
                elseif ( ~isempty(s.parent) && isequal(class(s.parent),class(s)) )
                    if (s.active && ~isempty(s.parent.dof) )
                        error('LSQDIM:element:GeometricElement :: There is a problem with the dof numbering, there is an active element which has a (non-active) parent that has assigned dofs');
                    end                    
                    out = s.parent.dof;
                else
                    out = [];
                end
            end
        end
        
        function setDof(s,values)
            s.dofObject.set( values );
        end
        
        function out = element(s,type,onlyActive,withCheck)
            if nargin==2
                onlyActive = false;
                withCheck  = true;
            elseif (nargin==3)
                withCheck  = true;
            end
                
            switch type
                case 0
                    out = s.vertices;
                case 1
                    out = s.edges;
                case 2
                    out = s.faces;
                case 3
                    out = s.hexes;
            end
            
            if (onlyActive)
                
%                 try 
                    maxLoops = 5;
                    numLoops = 0;
                    while ( any( ~[out.active] ) && numLoops < maxLoops )
                        nonActiveElement = out( ~[out.active] );
                        newElement       = cell(1,numel(nonActiveElement));
                        
                        if (type==0)
                            %% Vertices
                            for t=1:numel(nonActiveElement)
                                if (nonActiveElement(t).parent.dimension==0)
                                    newElement{t} = nonActiveElement(t).parent;

                                elseif (nonActiveElement(t).parent.dimension==1)
                                    % Vertices from an edge:
                                    %   In this case v contains 2 vertices,
                                    %   and one is already present. By
                                    %   fliplr the other vertex is selected
                                    %   as the replacement for the
                                    %   nonActive element
                                    v = nonActiveElement(t).parent.vertices;
                                    alreadyPresent = intersect( v, out);
                                    
                                    if numel(alreadyPresent)==1
                                        vertexPosition = (v==alreadyPresent);
                                            newElement{t} = v( fliplr( vertexPosition ) );
                                    else
                                        newElement{t} = nonActiveElement(t);
                                    end
                                    
%                                     try
%                                         if (~isempty( alreadyPresent ))
%                                             vertexPosition = (v==alreadyPresent);
%                                             newElement{t} = v( fliplr( vertexPosition ) );
%                                         else
%                                             newElement{t} = v(t);
%                                         end
%                                     catch
%                                        newElement{t} = nonActiveElement(t);
%                                     end
                                elseif (nonActiveElement(t).parent.dimension==2)
                                    % Vertices from an face:
                                    %   In this case v contains 4 vertices, and 
                                    %   one is already present. By fliplr the 
                                    %   diagonally opposite vertex is selected 
                                    %   as the replacement for the nonActive 
                                    %   element. The vertices 1 and 4 or
                                    %   'connected', and 2 and 3 as well,
                                    %   that's why fliplr works here as well
                                    v = nonActiveElement(t).parent.vertices;
                                    alreadyPresent = intersect( v, out);
                                
                                    vertexPosition = (v==alreadyPresent);
                                    newElement{t} = v( fliplr( vertexPosition ) );
                                else
                                    disp('')
                                end
                            end

                            try
                                out( ~[out.active] ) = [ newElement{:} ];
                            catch
                                disp('')
                            end

                        elseif (type==1)
                            %% Edges
                            for t=1:numel(nonActiveElement)
                                if (nonActiveElement(t).parent.dimension==1)
                                    % Edges from an edge:
                                    %   In this case the parent is 1 edge,
                                    %   so just use this one
                                    newElement{t} = nonActiveElement(t).parent;
                                elseif (nonActiveElement(t).parent.dimension==2)
                                    % Edges from a face:
                                    %   In this case the parent is a collection 
                                    %   of 4 edges
                                    if ( s.dimension==2 && numel(nonActiveElement)==numel(nonActiveElement(t).parent.edges) )
                                        newElement{t} = nonActiveElement(t).parent.edges(t);
                                    elseif (s.dimension==3)
                                        allEdges = s.getActiveEdgesChecked();
                                        
                                        out = [allEdges{:}];
                                        return                                        
                                    else
                                        newElement{t} = nonActiveElement(t);
                                    end
                                else
                                    newElement{t} = nonActiveElement(t);
                                end
                            end
                            
                            out( ~[out.active] ) = [ newElement{:} ];
                            
                        elseif (type==2)
                            %% Faces
                            for t=1:numel(nonActiveElement)
                                if (nonActiveElement(t).parent.dimension==2)
                                    % Face from an face:
                                    %   In this case the parent is 1 face,
                                    %   so just use this one
                                    newElement{t} = nonActiveElement(t).parent;
                                %elseif (nonActiveElement(t).parent.dimension==2)
                                    % Edges from an face:
                                    %   In this case the parent is a collection 
                                    %   of 4 edges
                                    %disp('')
                                    %e = nonActiveElement(t).parent.edges;
                                    %alreadyPresent = intersect( e, out);
                                else
                                    newElement{t} = nonActiveElement(t);
                                end
                            end
                            
                            out( ~[out.active] ) = [ newElement{:} ];
                            
                        else
                            error('Should not be here')
                        end
                        
                        numLoops = numLoops+1;
                    end
                
%                 catch MExp
%                     error('Fatal error in LSQDIM:FEObject:element (in the onlyActive loop)')
%                 end
                if (withCheck)
                    if (any(~[out.active]))
                        error('There is an element that is not active!')
                    end
                end

            end
        end
        
        function enableRefine(s)
            s.refineFlag = true;
        end
        
        function addUser(s,user)
            if (s.withUserInfo)
                if (isempty(s.user))
                    s.user = GeometricList(user);
                else
                    s.user.insert(user);
                end
            else
                s.userCount = s.userCount+1; 
            end
        end
        
        function remove(s,coarsenFlag)
            if nargin==1
                coarsenFlag = s.coarsenFlag;
            end
            
            if (coarsenFlag)
                try
                    s.parent.coarsen()
                catch MExp
                    error('LSQDIM:element:GeometricElement :: fatal error in remove (coarsening) step of a geometric element')
                end
            end
        end
        
        function removeUser(s,user)
            try
            if (s.withUserInfo)
                s.user.remove(user);
            else
                s.userCount = s.userCount-1;
            end
            
            if (s.numUser==0)
                delete(s);
            end
            
            catch
                disp('')
            end
        end
        
        function out = getUser(s)
            if (s.numUser>0)
                out = s.user.getCellArray;
            end
        end
        
        function out = numUser(s)
            if (s.withUserInfo)
                if ( ~isempty(s.user) )
                    out = s.user.numElements;
                else
                    out = 0;
                end
            else
                out = s.userCount;
            end
        end
        
        function out = getParent(s)
            out = s.parent;
        end
        
        function out = numAttached(s)
            try
                out = s.boundaryCount + s.numUser;
            catch
                error('LSQDIM:element:GeometricElement:numAttached problem')
            end
        end
    end
end

