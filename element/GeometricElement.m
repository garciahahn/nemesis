classdef GeometricElement < handle
    %UNTITLED8 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Constant, Access=protected)
%         maxDim          %= System.settings.mesh.dim;
        withUserInfo    = false;
        
        levelColors     = { 'r', 'b', 'g', 'c', 'm' };
    end
        
    properties (Access=protected)
        logicalElement;
        maxDim;
        boundaryCount = 0;
        leader;
%         parent;
        userCount = 0;
        
    end
        
    properties (GetAccess=public)
        parent;
        
        id;
        dimension;      % feObject dimension
        J;              % Jacobian
        dofObject;
        qdofObject;
        detJ;           % determinant of the Jacobian
        refineFlag = false;
        coarsenFlag = false;
        isRefined  = false;
        
        %isSolid = false;        % An element can become a solid if it is a pillar
        
        feType = 1;
        
        level;
        
        user;   
        tag;
        
        % Activity flags
        active = false;
        activeLevel = 0;
        
        periodic = false;
    end
        
    methods
        function s = GeometricElement(dimension,parent,logicalElement)
            s.maxDim    = System.settings.mesh.dim;
            s.dimension = dimension;
            
            if (~isnumeric(parent))
                s.parent = parent;
                s.tag    = parent.tag;
            else
                s.parent = [];
            end
            
            if ( isempty(s.parent) )
                s.level = 0;
            else
                s.level = s.parent.level + 1;
                %s.isSolid = s.parent.isSolid;
            end
            
            s.dofObject = DoF();
            s.qdofObject = qDoF();
        end
        
        % This function is overloaded by subclasses
%         function setTag(s,tag)
%             s.tag = tag;
%             
%             if (s.isRefined)
%                 arrayfun( @(x) x.setTag(tag), s.internalFaces );
%             else
%                 arrayfun( @(x) x.setTag(tag), s.edges );
%             end
%         end

        function out = getTag(s)
            out = s.tag;
        end
        
        % Reset the current GeometricElement and all its parents
        function reset(s)
            s.dofObject.reset();
            s.qdofObject.reset();
            if (~isempty(s.parent))
                s.parent.reset();
            end
            if (s.isRefined)
                disp('')
            end
        end
        
        function out = dof(s,includePeriodicity)
            
            if (nargin==1)
                includePeriodicity = true; 
            end
            
            out = s.dofObject.get;
            
%             if ~isempty(s.dofObject.get)
% %                 out = s.dof;
%                 out = s.dofObject.get;
            if isempty(out)
                if (s.periodic && includePeriodicity)
                    out = s.leader.dof;
                elseif ( ~isempty(s.parent) && isequal(class(s.parent),class(s)) )
                    if (s.active && ~isempty(s.parent.dof) )
                        error('LSQDIM:element:GeometricElement :: There is a problem with the dof numbering, there is an active element which has a (non-active) parent that has assigned dofs');
                    end                    
                    out = s.parent.dof;
                else
                    out = [];
                end
            end
        end
        
        function out = qdof(s,includePeriodicity)
            
            if (nargin==1)
                includePeriodicity = true; 
            end
            
            out = s.qdofObject.get;
            
%             if ~isempty(s.qdofObject.get)
%                 out = s.qdofObject.get;
%             else
            if (isempty(out))
                if (s.periodic && includePeriodicity)
                    out = s.leader.qdof;
                elseif ( ~isempty(s.parent) && isequal(class(s.parent),class(s)) )
                    if (s.active && ~isempty(s.parent.qdof) )
                        error('LSQDIM:element:GeometricElement :: There is a problem with the qdof numbering, there is an active element which has a (non-active) parent that has assigned qdofs');
                    end                    
                    out = s.parent.qdof;
                else
                    out = [];
                end
            end
        end
        
        function setDof(s,values)
            s.dofObject.set( values );
        end
        
        function setqDof(s,values)
            s.qdofObject.set( values );
        end
        
        function out = element(s,type,onlyActive,withCheck)
            if nargin==2
                onlyActive = false;
                withCheck  = true;
            elseif (nargin==3)
                withCheck  = true;
            end
                
            switch type
                case 0
                    out = s.vertices;
                case 1
                    out = s.edges;
                case 2
                    out = s.faces;
                case 3
                    out = s.hexes;
            end
            
            if (onlyActive)
                
%                 try 
                    maxLoops = 5;
                    numLoops = 0;
                    while ( any( ~[out.active] ) && numLoops < maxLoops )
                        nonActiveElement = out( ~[out.active] );
                        newElement       = cell(1,numel(nonActiveElement));
                        
                        if (type==0)
                            %% Vertices
                            for t=1:numel(nonActiveElement)
                                if (~isempty(nonActiveElement(t).parent))
                                    if (nonActiveElement(t).parent.dimension==0)
                                        newElement{t} = nonActiveElement(t).parent;
                                        % If the parent of the nonActiveElement
                                        % is a vertex (dimension==0), then use
                                        % the parent itself as a newElement

                                    elseif (nonActiveElement(t).parent.dimension==1)
                                        % If the parent of the nonActiveElement
                                        % is an edge (dimension==1), then
                                        % verify which of the vertices of this
                                        % edge 

                                        % Vertices from an edge:
                                        %   In this case v contains 2 vertices,
                                        %   and one is already present. By
                                        %   fliplr the other vertex is selected
                                        %   as the replacement for the
                                        %   nonActive element
                                        v = nonActiveElement(t).parent.vertices;

                                        if (true)
                                            outX = vertcat(out(:).X);
                                            vX   = vertcat(v(:).X);

                                            flags = true( size(vX) );
                                            for i=1:size(vX,2)  
                                                flags(:,i) = ismembc( vX(:,i) , sort(outX(:,i)) );   % check each coordinate direction
                                            end
                                            v_matchLoc = all(flags');

                                            flags = true( size(outX) );
                                            for i=1:size(outX,2)  
                                                flags(:,i) = ismembc( outX(:,i) , sort(vX(:,i)) );   % check each coordinate direction
                                            end
                                            out_matchLoc = all(flags');

                                            % Check if the levels of the 2 match as
                                            % well
                                            if ( any(v_matchLoc) && ( v(v_matchLoc).level == out(out_matchLoc).level ) )
                                                newElement{t} = v( fliplr( v_matchLoc ) );
                                            else
                                                newElement{t} = nonActiveElement(t);
                                            end

                                        else
                                            % MMKK -- 08/05/2019
                                            % The intersect function is quite slow, 
                                            % the ismember function might be faster
                                            alreadyPresent = intersect( v, out);
                                            if numel(alreadyPresent)==1
                                                vertexPosition = (v==alreadyPresent);
                                                newElement{t} = v( fliplr( vertexPosition ) );
                                            else
                                                newElement{t} = nonActiveElement(t);
                                            end
                                        end

                                    elseif (nonActiveElement(t).parent.dimension==2)
                                        % Vertices from an face:
                                        %   In this case v contains 4 vertices, and 
                                        %   one is already present. By fliplr the 
                                        %   diagonally opposite vertex is selected 
                                        %   as the replacement for the nonActive 
                                        %   element. The vertices 1 and 4 or
                                        %   'connected', and 2 and 3 as well,
                                        %   that's why fliplr works here as well
                                        v = nonActiveElement(t).parent.vertices;
                                        alreadyPresent = intersect( v, out);

                                        vertexPosition = (v==alreadyPresent);
                                        newElement{t} = v( fliplr( vertexPosition ) );
                                    else
                                        disp('')
                                    end
                                    
                                else
                                    newElement{t} = nonActiveElement(t);
                                end
                            end

%                             try
                                out( ~[out.active] ) = [ newElement{:} ];
%                             catch
%                                 disp('')
%                             end

                        elseif (type==1)
                            %% Edges
                            for t=1:numel(nonActiveElement)
                                if (~isempty(nonActiveElement(t).parent))
                                    if (nonActiveElement(t).parent.dimension==1)
                                        % Edges from an edge:
                                        %   In this case the parent is 1 edge,
                                        %   so just use this one
                                        newElement{t} = nonActiveElement(t).parent;
                                    elseif (nonActiveElement(t).parent.dimension==2)
                                        % Edges from a face:
                                        %   In this case the parent is a collection 
                                        %   of 4 edges
                                        if ( s.dimension==2 && numel(nonActiveElement)==numel(nonActiveElement(t).parent.edges) )
                                            newElement{t} = nonActiveElement(t).parent.edges(t);
                                        elseif (s.dimension==3)
                                            allEdges = s.getActiveEdgesChecked();

                                            out = [allEdges{:}];
                                            return                                        
                                        else
                                            newElement{t} = nonActiveElement(t);
                                        end
                                    else
                                        newElement{t} = nonActiveElement(t);
                                    end
                                else
                                    newElement{t} = nonActiveElement(t);
                                end
                            end
                            
                            out( ~[out.active] ) = [ newElement{:} ];
                            
                        elseif (type==2)
                            %% Faces
                            for t=1:numel(nonActiveElement)
                                if (~isempty(nonActiveElement(t).parent))
                                    if (nonActiveElement(t).parent.dimension==2)
                                        % Face from an face:
                                        %   In this case the parent is 1 face,
                                        %   so just use this one
                                        newElement{t} = nonActiveElement(t).parent;
                                    %elseif (nonActiveElement(t).parent.dimension==2)
                                        % Edges from an face:
                                        %   In this case the parent is a collection 
                                        %   of 4 edges
                                        %disp('')
                                        %e = nonActiveElement(t).parent.edges;
                                        %alreadyPresent = intersect( e, out);
                                    else
                                        newElement{t} = nonActiveElement(t);
                                    end
                                end
                            end
                            
                            if (~isempty(newElement))
                            %if (~isempty(newElement{:}))
                                out( ~[out.active] ) = [ newElement{:} ];
                            end
                            
                        else
                            error('Should not be here')
                        end
                        
                        numLoops = numLoops+1;
                    end
                
%                 catch MExp
%                     error('Fatal error in LSQDIM:FEObject:element (in the onlyActive loop)')
%                 end


%                 if (withCheck)
%                     if (any(~[out.active]))
%                         error('There is an element that is not active!')
%                         
%                     end
%                 end

            end
        end
        
        function enableRefine(s)
            s.refineFlag = true;
        end
        
        function addUser(s,user)
            if (s.withUserInfo)
                if (isempty(s.user))
                    s.user = GeometricList(user);
                else
                    s.user.insert(user);
                end
            else
                s.userCount = s.userCount+1; 
            end
        end
        
        function remove(s,coarsenFlag)
            if nargin==1
                coarsenFlag = s.coarsenFlag;
            end
            
            if (coarsenFlag)
                try
                    s.parent.coarsen()
                catch MExp
                    error('LSQDIM:element:GeometricElement :: fatal error in remove (coarsening) step of a geometric element')
                end
            end
        end
        
        function removeUser(s,user)
            try
            if (s.withUserInfo)
                s.user.remove(user);
            else
                s.userCount = s.userCount-1;
            end
            
            if (s.numUser==0)
                delete(s);
            end
            
            catch
                error('GeometricElement.removeUser failed\n')
            end
        end
        
        function out = getUser(s)
            if (s.numUser>0)
                out = s.user.getCellArray;
            end
        end
        
        function out = numUser(s)
            if (s.withUserInfo)
                if ( ~isempty(s.user) )
                    out = s.user.numElements;
                else
                    out = 0;
                end
            else
                out = s.userCount;
            end
        end
        
%         function out = get.isSolid(s)
%             out = s.isSolid;
%         end
%         
%         function makeSolid(s,flag)
%             if (~s.isSolid && flag)
%                 s.isSolid = flag;
%             end
%         end
%         
%         function set.isSolid(s,flag)
%             if (~s.isSolid && flag)
%                 s.isSolid = flag;
%             
%                 if (isprop(s,'faces') && ~isempty(s.faces))
%                     for f=s.faces
%                         f.makeSolid(flag);
%                     end
%                 end
% 
% 
%                 if (isprop(s,'edges') && ~isempty(s.edges))
%                     for e=s.edges
%                         e.makeSolid(flag);
%                     end
%                 end
% 
%                 if (isprop(s,'vertices') && ~isempty(s.vertices))
%                     for v=s.vertices
%                         v.makeSolid(flag);
%                     end
%                 end
%             else
%                 disp('')
%             end
%         end
        
        function out = getParent(s)
            out = s.parent;
        end
        
        function out = numAttached(s)
            try
                out = s.boundaryCount + s.numUser;
            catch
                error('LSQDIM:element:GeometricElement:numAttached problem')
            end
        end
    end
    
    methods (Access = protected)
        function setLogicalElement(s,logicalElement)
            if isempty(logicalElement)
                switch s.dimension
                    case 0                        
                        s.logicalElement = LogicalVertex( s.parent );
                    case 1
                        s.logicalElement = LogicalEdge( s.getLogicalElement(0), s.parent );
                    case 2
                        s.logicalElement = LogicalFace( s.getLogicalElement(1), s.parent );
                    case 3
                        s.logicalElement = LogicalHex( s.getLogicalElement(2), s.parent );
                end
            else
                s.logicalElement = logicalElement;
            end
        end
        
        function out = getLogicalElement(s,dimension)
            switch dimension
                case 0
                    out = [ s.vertices.logicalElement ];
                case 1
                    out = [ s.edges.logicalElement ];
                case 2
                    out = [ s.faces.logicalElement ];
                case 3
                    out = [ s.hexes.logicalElement ];
            end
        end
    end
end

