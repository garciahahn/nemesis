classdef FECollection < Singleton
    %FECOLLECTION Collection of Finite Elements
    %   Detailed explanation goes here
    
    properties
        finiteElements;
        numFE;
    end
    
    % Private initialization of this object
    methods(Access=private)
        function s = FECollection()
            s.finiteElements = cell(0);
            s.numFE = 0;
        end
    end    
    
    % Allow only 1 object to exist
    methods(Static)
        function s = instance()
            persistent uniqueInstance
            if isempty(uniqueInstance)
                s = FECollection();
                uniqueInstance = s;
            else
                s = uniqueInstance;
            end
        end
        
        function add(id,finiteElement)
            %ADD Adds an finiteElement to the collection
            s = FECollection.instance();
            
            if ( numel(s.finiteElements)<id || isempty(s.finiteElements{id}) )
                s.numFE = s.numFE + 1;
                s.finiteElements{id} = finiteElement;
            else
                error('There is already a finite element with id=%d\n',id)
            end
        end
        
        function remove(id)
            %REMOVE Removes an finiteElement from the collection
            s = FECollection.instance();
            if ~isempty(s.finiteElements{id})
                s.numFE = s.numFE - 1;
                s.finiteElements{id} = [];
            else
                %error('There is no finite element with id=%d\n',id)
            end
        end
        
        function out = get(id)
            if (isempty(id))
                error('Please provide a valid feType, this one is empty')
            end
            s = FECollection.instance();            
            if ( inrange(id,1,s.numFE) )
                out = s.finiteElements{id};                
            else
                out = [];
                warning('No FiniteElement known with id=%d\n',id)
            end
        end
        
        function info()
            s = FECollection.instance();
            fprintf('    FECollection with %d Finite Elements\n',s.numFE);
        end
        
        function reset()
            s = FECollection.instance();
            s.finiteElements = cell(0);
            s.numFE = 0;
        end
        
        % Function to save a FECollection to a struct. The number of finite 
        % elements and structs of the finite elements are saved (by calling
        % the save(finiteElement) function (see FiniteElement.save).
        function out = save()
            s = FECollection.instance();
            
%             out = struct;
%             out.numFE          = s.numFE;
%             out.finiteElements = cell(s.numFE,1);
%             
%             for i=1:s.numFE
%                 finiteElement = FECollection.get(i);
%                 out.finiteElements{i} = save(finiteElement);
%             end
            
            out = cell(s.numFE,1);
            for i=1:s.numFE
                finiteElement = FECollection.get(i);
                out{i} = save(finiteElement);
            end
        end
        
        % Function to load a FECollection from a saved struct. First the
        % collection is reset, and next the finite elements are loaded and
        % added to the collection (see FiniteElement.load).
        function load(finiteElements)
            FECollection.reset;
            for i=1:numel(finiteElements)
                temp = FiniteElement.load(finiteElements{i});
                FECollection.add(i,temp);
            end
        end
    end
end

