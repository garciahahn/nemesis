classdef GeometricEdge < GeometricElement
    %UNTITLED3 Summary of this class goes here
    %   Detailed explanation goes here
    
%     properties (Constant)
%         activeSwitch = [ 1 2 4 ];
%     end
    
    properties
        orientation;
        vertices;
        
        internalVertices;
        internalEdges;
        
        coordSystem;
        shape;
        radius;
        
        normal;
        
        isSimple = false;
    end
    
    properties (Access=private)
        edgeLengthMap, midPoint;
    end
    
    methods
        function s = GeometricEdge(varargin)
            
            [vertices,parent,logicalElement,coordSystem] = VerifyInput( varargin );
            
            %if (isempty(vertices))
            %    s = [];
            %    return;
            if (numel(vertices)~=2)
                error('Please provide 2 vertices')
            end
            
            s@GeometricElement(1,parent,logicalElement);
            
            s.coordSystem = coordSystem;
            
            switch coordSystem
                case CoordSystem.Cartesian
                    s.shape  = EdgeShape.Linear;
                    s.radius = [];
                    
                case CoordSystem.Cylindrical
                    if (vertices{1}.X(1)==vertices{2}.X(1))
                        s.shape  = EdgeShape.Circular;
                        s.radius = vertices{1}.X(1);
                    elseif (vertices{1}.X(2)==vertices{2}.X(2))
                        s.shape  = EdgeShape.Linear;
                        s.radius = [];
                    else
                        error('Shape not recognized')
                    end
            end
            
            s.vertices      = [vertices{:}];
            s.boundaryCount = min( [s.vertices(:).boundaryCount] );
            
            % Set the logicalElement (uses the passed or creates a new)
            s.setLogicalElement( logicalElement );
            
            if (all([s.vertices.periodic]))
                s.periodic = true;
            end
                
            for i=1:2
                s.vertices(i).addUser(s);
            end
            
            s.computeOrientation;
            
            s.initMapping;
            s.defineSimple();
            
            s.computeJacobian;
        end
        
        function out = edges(s,n)
            out = s;
            if (nargin>1)
                out = out(n);
            end
        end
        
        % Mapping from logical > physical
        function x = mapping(s,xi)
            if ( numel(xi)==1 )
                if ( inrange(xi,-1,1) )       
                    switch s.shape
                        case EdgeShape.Linear
                            x = s.midPoint + xi * s.edgeLengthMap;          % Linear interpolation
                        case EdgeShape.Circular
                            x = s.midPoint;
                            x(2) = x(2) + xi * s.edgeLengthMap(2);          % Adjust the theta value only
                        otherwise
                            error('Unsupported edge shape')
                    end
                else
                    error('Only logical values between -1 and +1 are valid')
                end
            else
                error('Exactly 1 logical values between -1 and +1 is expected (received %d values)',numel(xi))
            end
        end
        
        function out = computeJacobian(s)
            s.J = s.edgeLength;
            
%             s.J = norm( s.derivative );
%             %out = s.J;
%             
%             if nargin==1
%                 logicalCoordinate = [1];
%             end
%             
%             dXdxi = zeros(s.dimension,s.dimension);            
%             for i=1:s.dimension
%                 tmp = s.derivative( logicalCoordinate );
%                 dXdxi(:,i) = tmp(1:s.dimension) / System.settings.phys.L0;
%             end
%             
%             s.detJ  = det( dXdxi );
%             out     = s.detJ;
            
            out = s.edgeLength / System.settings.phys.L0;
        end
    end
    
    methods (Access=private)
        
        function computeOrientation(s)
            [v0,v1]       = s.vertices.X;
            try
            edgeVector    = (v1-v0);            
            catch
               disp('')
            end
            edgeLength    = norm(edgeVector);            
            s.orientation = edgeVector / edgeLength;
            
            s.orientation( abs(s.orientation)<1e-14 ) = 0;
            
            % For a 2D mesh also compute the normal wrt the orientation
            if (s.maxDim==2)
                s.normal = s.orientation;
                s.normal(1) = s.orientation(2);
                s.normal(2) = s.orientation(1);
            end
        end
        
        function initMapping(s)
            switch s.shape
                case EdgeShape.Linear
                    % linear mapping
                    [v0,v1] = s.element(0).X;

                    s.edgeLengthMap = ( v1 - v0 ) / 2;
                    s.midPoint      = ( v1 + v0 ) / 2;
                    
                case EdgeShape.Circular
                    % circular mapping
                    [v0,v1] = s.element(0).X;

                    s.edgeLengthMap    = ( v1 - v0 ) / 2;
                    s.edgeLengthMap(1) = v0(1);
                    
                    s.midPoint         = ( v1 + v0 ) / 2;
                    s.midPoint(1)      = v0(1);
                    
                otherwise
                    error('Unknown edge shape')
            end
        end
        
        function defineSimple(s)
            delta = s.vertices(2).X - s.vertices(1).X;
            delta( abs(delta)<1e-12 ) = 0;
            if ( nnz( delta ) == 1 )
                s.isSimple = true;
            else
                disp('non simple edge\n')
            end
        end

    end
    
    methods        
        
        % Derivative physical to logical (d X / d xi)
        function out = derivative(s,xi)
            switch s.shape
                case EdgeShape.Linear
                    out = s.edgeLengthMap;
                case EdgeShape.Circular
                    out = s.edgeLengthMap; %(1) * s.edgeLengthMap(2);
                otherwise
                    error('Unknown edge shape')
            end
        end
        
        function out = edgeLength(s)
            switch s.shape
                case EdgeShape.Linear
                    out = norm(s.edgeLengthMap);
                case EdgeShape.Circular
                    out = s.edgeLengthMap(1) * s.edgeLengthMap(2);
                otherwise
                    error('Unknown edge shape')
            end
        end
        
        function out = refine(s,refineFactor,refineFlag)
            if (nargin==2)
                refineFlag = s.refineFlag;
            end
            
            % Refine this edge if requested and if it's still active
            if (refineFlag && ~s.isRefined)
                s.createInternal(refineFactor);
                s.isRefined = true;
            end
            
            if (refineFactor ~= size(s.internalEdges))
                disp('')
            end
            
            % Return
            if (s.isRefined)
                out = s;
            else
                out = s.internalEdges;
            end
            
            s.active = false;
            arrayfun( @(x) x.updateActive(false), s.vertices );
        end
        
        function coarsen(s)
                        
            % Remove the internalEdge as user of vertices
            % This should delete the internalVertices
            if (all( isvalid( [s.internalEdges{:}] ) ))
                for e = [s.internalEdges{:}]
                    for v = e.v
                        v.removeUser( e );
                    end
                end
            elseif (any( isvalid( [s.internalEdges{:}] ) ))
                error('Not all internalEdges are valid')
            else
                % normal behaviour
            end
            
            if ( isempty(s.internalVertices) || all( ~isvalid( [s.internalVertices{:}] ) ))
                 s.internalVertices = [];
            else
                error('Not all internalVertices have been deleted')
            end
            
            for e = [s.internalEdges{:}]
                delete( e );
            end
            s.internalEdges = [];
            
            s.isRefined = false;
            
            % Check if the direct vertices of this parent are still
            % valid (refined by another edge), and update them accordingly
            for v=1:numel(s.vertices)
                if (s.vertices(v).isRefined)
                    if (~isempty(s.vertices(v).internalVertices))
                        stillRefined = all( isvalid( [s.vertices(v).internalVertices{:}] ) );
                        s.vertices(v).isRefined = stillRefined;
                        if (~stillRefined)
                            s.vertices(v).coarsen;
                        end
                    end
                end
            end
        end
        
        function delete(s)
            if ( all( isvalid( s.vertices(:) ) ) )
                for v=1:2
                    s.vertices(v).removeUser(s)
                end
            end
        end
        
        function createInternal(s,refineFactor)
            numInternalVertices = refineFactor-1;
            s.internalVertices  = cell(1,refineFactor-1);
            
            numInternalEdges    = refineFactor;
            s.internalEdges     = cell(1,refineFactor);
            
            s.vertices(1).refine(1,1);
            s.vertices(2).refine(1,1);
            
            % Determine the logical locations of the internal vertices
            x1 = linspace(-1,1,refineFactor+1);
            
            % internal vertices of this face
            for i=2:refineFactor
                coordinate = s.mapping( x1(i) );          % the logical location of the additional vertex
                s.internalVertices{i-1} = GeometricVertex( coordinate, s );
                
                if (s.periodic)
                    if ( isempty(s.leader.internalVertices) )
                        s.leader.createInternal(refineFactor);
                    end
                    s.internalVertices{i-1}.setPeriodic( s.leader.internalVertices{i-1} );
                end
            end
            
            allVertices = cell( 1, numel(s.internalVertices)+2 );
            allVertices{1} = s.vertices(1).internalVertices{1};
            if (numel(s.internalVertices) > 1)
                allVertices(2:end-1) = s.internalVertices(:);
            elseif (numel(s.internalVertices) == 1)
                allVertices{2} = s.internalVertices{1};
            end
            allVertices{end} = s.vertices(2).internalVertices{1};
            
            for e=1:numel(allVertices)-1
                s.internalEdges{e} = GeometricEdge( allVertices(e:e+1), s, [], s.coordSystem );
                
                if (s.periodic)
                    if ( isempty(s.leader.internalEdges) )
                        s.leader.createInternal(refineFactor);
                    end
                    s.internalEdges{e}.setPeriodic( s.leader.internalEdges{e} );
                end
            end
            
%             %% Add to feLists
%             if (s.withFELists)
%                 for v=1:numInternalVertices
%                     feLists{1}.insert(s.internalVertices{v});
%                 end            
%                 for e=1:numInternalEdges
%                     feLists{2}.insert(s.internalEdges{e});
%                 end
%             end
            
            s.active = false;
            
%             % Remove this edge as a user of the 2 nodes, and remove the
%             % node if there are no users left
%             for i=1:2
%                 s.vertices(i).removeUser(s);
%                 
%                 %if (s.vertices(i).numUser==0)
%                 %    delete( s.vertices(i) );
%                 %end
%             end
        end
        
        function setTag(s,tag)
            s.tag = tag;
            
            if (s.isRefined)
                arrayfun( @(x) x.setTag(tag), s.internalEdges );
            else
                arrayfun( @(x) x.setTag(tag), s.vertices );
            end
        end
                
        function plot(s,linewidth,scaling)
            
            if (nargin==1)
                linewidth = 1;
                scaling = 0.8;
            end
            
            if (s.active)
                coordinate1 = s.vertices(1).X; 
                coordinate2 = s.vertices(2).X;

                center = 0.5 * ( coordinate1 + coordinate2 );
                delta  = (coordinate2 - coordinate1) / 2;

                c1 = center - scaling*delta;
                c2 = center + scaling*delta;

                coordinate = [c1 ; c2];

                plot3( coordinate(:,1),coordinate(:,2),coordinate(:,3), s.levelColors{ s.level+1 },'linewidth',linewidth)
                
            else
                %error('This edge is not active, but still in the list')                
            end
        end
        
        function out = allDoF(s)
            % This function returns all DoF of this edge element
           
            v = s.element(0,true);
            
            try
                out = [  v(1).dof; s.edges.dof ; v(2).dof ]';
                    
            catch MExp
                disp('problem with GeometricEdge.allDof')
            end
        end
        
        function out = allLevels(s)
            % This function returns all levels of this edge element
           
            v = s.element(0,true);
            
            try
                out = [  v(1).level ; s.edges.level ; v(2).level ]';
                    
            catch MExp
                disp('problem with GeometricEdge.allLevels')
            end
        end
        
        function out = allDofLevels(s)
            % This function returns all levels of this edge element
            v = s.element(0,true);
            
            try
            out    = [  (v(1).level*ones(size(v(1).dof))); s.edges.level*ones(size(s.edges.dof)) ; (v(2).level*ones(size(v(2).dof))) ]';
                    
            catch MExp
                error('LSQDIM:element:GeometricEdge:allDofLevels')
            end
        end
        
        function out = getActiveVertices(s)
            out = [];
            
            if (s.isRefined && ~s.active)
                % internal vertices
                tmp = cellfun( @(x) x.getActiveVertices, s.internalVertices, 'UniformOutput', false );
                tmp = vertcat( tmp{:} );
                out = [ out; tmp(:) ];
                    
                % vertices within internal edges
                tmp = cellfun( @(x) x.getActiveVertices, s.internalEdges, 'UniformOutput', false );
                tmp = vertcat( tmp{:} );
                out = [ out; tmp(:) ];                
            end
        end
        
        function out = getActiveEdges(s)
            if (s.isRefined && ~s.active)                
                % edges within internal edges
                tmp = cellfun( @(x) x.getActiveEdges, s.internalEdges, 'UniformOutput', false );
                out = vertcat( tmp{:} );
            elseif (s.active) 
                out = {s};
            elseif (~s.active)
                if ~isempty(s.parent) && s.parent.active
                    out = s.parent.getActiveEdges();
                else
                    out = [];
                end
            else
                error('why am I here?')
            end
        end
        
        function setPeriodic(s,leader)
            s.periodic = true;
            s.leader   = leader;
        end
        
        function updateActive(s,flag)
            if (flag)
                if (s.maxDim==s.dimension)
                    if ( s.isRefined )
                        s.active = false;
                        %arrayfun( @(x) x.updateActive(false), s.vertices );
                        cellfun( @(x) x.updateActive(flag), s.internalEdges );
                    else
    %                     if (~s.active)
                            s.active = flag;
                            arrayfun( @(x) x.updateActive(flag), s.vertices );
    %                     end
                    end

                else
                    s.setActive(flag);                                      % Make this edge active, and deactivate any internal elements

                    % If any parent is active already, ensure this child does
                    % not become active after all
                    parent = s.parent;
                    while ~isempty( parent )
                        if ( parent.active )
                            s.active = false;
                        end
                        parent = parent.parent;
                    end
                    %parent.active = flag;
                end
            end
        end
        
        function updateActivePeriodic(s)
            if (s.maxDim==s.dimension)
                if ( s.isRefined )
                    cellfun( @(x) x.updateActivePeriodic(), s.internalEdges );
                else
                    arrayfun( @(x) x.updateActivePeriodic(), s.vertices );
                end
                
            else
                if (s.periodic)
                    if (isvalid(s.leader))
                        if (s.leader.active~=s.active)
                            if (~isempty(s.leader.parent))
                                
                                if ( s.leader.parent.level < s.level && s.leader.parent.active)
                                    s.parent.setActive(true);
                                else
                                    s.leader.parent.active = false;
                                    s.leader.setActive(true);
                                end
                            end
                            
                        end
                    else
                        s.active = false;
                        s.parent.setActive(true);
                    end
                end
            end
        end

        function makeSolid(s,flag)
            if (~s.isSolid && flag)
                s.isSolid = true;
                
                for v=s.vertices
                    v.makeSolid(flag);
                end
            end
        end
        
        function setActive(s,flag)
            try
            s.active = flag;
            if ( s.isRefined )
                cellfun( @(x) x.setActive(false), s.internalEdges );
                cellfun( @(x) x.setActive(false), s.internalVertices );
            end
            catch
               error('LSQDIM:element:GeometricEdge :: setActive issue'); 
            end
        end
        
        function resetActive(s,recursively)
            if (nargin==1)
                recursively = true;
            end
            
            s.active = false;
            
            if (recursively)
                if ( s.isRefined )
                    cellfun( @(x) x.resetActive(), s.internalEdges );
                    cellfun( @(x) x.resetActive(), s.internalVertices );
                end
                arrayfun( @(x) x.resetActive(), s.vertices );
            end
        end
    end    
end