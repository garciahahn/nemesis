%*****************************
% 05 May 2014
%*****************************
% addpath('C:\maria\projects\LSQ\codigoMatlab\HighOrderLib');
% addpath('Z:\Privat\LSQ lecture\Matlab code from Carlos\HighOrderLib');
% load HighOrderDB
% global HOLib_DB

classdef LSQdisc_4 < handle
    
    properties (Access = private, Constant)
        ROUNDOFF_LIMIT = 1e-14;
    end
    
    properties (Access = private)
        Pt;
        Qt;
        tq;
        Dt;
        wt;
        Ht;
        Bt0; Bt1;
        Dtt;
        Hxt;    Hyt;
        Ztm, Ztp;
    end
    
    properties
        SDIM;
        spaceTime;
        
        P; Px; Py; Pz; 
        Q; Qx; Qy; Qz;
        C;
        
        dofe = zeros(1,4);
        dofeq = zeros(1,4);
        dofev; dofevq;
        
        numSpatialDofs;
        numSpatialPoints;
        
        xq; yq; zq;
        wx; wy; wz;
                        
        Dx; Dy; Dz;
        W;
        
        H1, D1, DD1;
        
        H;
        Dxx; Dyy; Dzz; Dxy; Dxxy; Dxyx; Dyyx;

        Bx0; Bx1;
        By0; By1;
        Bz0; Bz1;
        Bp;
        
        Hx;  Hy;   Hz;    

        
        Himm;  Himp;  Hipm;  Hipp;
        
        Himmmm;  Himmmp;  Himmpm;  Himmpp;
        Himpmm;  Himpmp;  Himppm;  Himppp;
        Hipmmm;  Hipmmp;  Hipmpm;  Hipmpp;
        Hippmm;  Hippmp;  Hipppm;  Hipppp;
        
        Hismm;  Hismp;  Hispm;  Hispp;
        
        Hismmmm;  Hismmmp;  Hismmpm;  Hismmpp;
        Hismpmm;  Hismpmp;  Hismppm;  Hismppp;
        Hispmmm;  Hispmmp;  Hispmpm;  Hispmpp;
        Hisppmm;  Hisppmp;  Hispppm;  Hispppp;
        
        sec1; sec2;
        
        Z;
        
        Zxm, Zxp; 
        Zym, Zyp;
        Zzm, Zzp;
        
        
        DZxm, DZxp
        
        matZ;
        Id;
        vecZ;
        
    end %properties
    
    
    methods
        % Constructor
        function D = LSQdisc_4(P,Q,C,SDIM,spaceTime)
            % Ensure P, Q and C are all arrays of size (4,1) 
            if (numel(P)==1)
                P = P*ones(1,4);
            end
            if (numel(Q)==1)
                Q = Q*ones(1,4);
            end
            if (numel(C)==1)
                C = C*ones(1,4);
            end
            
            D.SDIM = SDIM;
            D.spaceTime = spaceTime;
            
            % Adjust the values to the mesh settings
            if SDIM==2 
                if spaceTime
                    P(4) = 0;
                    Q(4) = 1;
                else
                    P(3:4) = 0;
                    Q(3:4) = 1;
                end
                
            elseif (SDIM==1)
                % One-dimensional 
                if spaceTime
                    P(3:4) = 0;
                    Q(3:4) = 1;
                else
                    P(2:4) = 0;
                    Q(2:4) = 1;
                end   
            else
                error('not supported yet...')
            end
            
            D.P  = P;
            D.Px = P(1);
            D.Py = P(2);
            D.Pz = P(3);
            D.Pt = P(4);
            
            D.Q  = Q;
            D.Qx = Q(1);
            D.Qy = Q(2);
            D.Qz = Q(3);
            D.Qt = Q(4);

            D.numSpatialDofs    = (D.Px+1) * (D.Py+1);
            D.numSpatialPoints  = D.Qx * D.Qy;
            
            D.C = C;
            
            for s=1:4
                D.dofe(s) = P(s)+1;     % # dof in each direction (for 1 element and 1 variable)
                D.dofeq(s) = Q(s);      % # solution points in each direction (for 1 element and 1 variable)
            end
            
            D.dofev = prod(D.dofe);     % # dof in 1 element for 1 variable
            D.dofevq = prod(D.dofeq);   % # solution points in 1 element for 1 variable

            D.matZ = zeros( D.dofevq , D.dofev );
            D.vecZ = zeros( D.dofevq , 1 ) ;
            
            D.setupBasisFunctions();
        end
        
        function setupBasisFunctions(D)

            
            % Standard element from -1 to +1, Jacobians are all 1
            dAll = 2;
            
            dX = dAll;
            dY = dAll;
            dZ = dAll;
            dT = dAll ;
            
%             dX = 1;
%             dY = 1;
%             dZ = 0.001;
            
            Jx = 2/dX;
            Jy = 2/dY;
            Jt = 2/dT;
            Jz = 2/dZ;
           
             % Compute the GLL points and weights
            [px,pwx] = GLL(D.Qx,-1,1);
            [py,pwy] = GLL(D.Qy,-1,1);
            [pz,pwz] = GLL(D.Qz,-1,1);
            if (D.Qt>0)
                [pt,pwt] = GLL(D.Qt,-1,1);     
            end
            
            D.xq = px; D.wx = pwx;
            D.yq = py; D.wy = pwy;
            D.zq = pz; D.wz = pwz;
            if (D.Qt>0)
                D.tq = pt; D.wt = pwt;
            end
            
            indx=[1 2 5:1:D.dofe(1) 3 4];
            indy=[1 2 5:1:D.dofe(2) 3 4];
            if (D.C(3)==1)
                if (D.dofe(3)<4)
                    error('Pz should be larger than or equal to 3 for C1, maybe switch to C0?')
                end
                indz=[1 2 5:1:D.dofe(3) 3 4];
            else
                indz=[1:1:D.dofe(3)];
            end
            if (D.C(4)==1)
                indt=[1 2 5:1:D.dofe(4) 3 4];
            else
                indt=[1:1:D.dofe(4)];
            end
            
            % x-direction
            Hx1   = HermiteN(indx,px,Jx);
            Dx1   = DHermiteN(indx,px,Jx);
            DDx1  = DDHermiteN(indx,px,Jx);

            % y-direction
            Hy1   = HermiteN(indy,py,Jy);
            Dy1   = DHermiteN(indy,py,Jy);
            DDy1  = DDHermiteN(indy,py,Jy);

            % z-direction
            if (D.C(3)==1)
                Hz1   = HermiteN(indz,pz,Jz);
                Dz1   = DHermiteN(indz,pz,Jz);
                DDz1  = DDHermiteN(indz,pz,Jz);
            else
                Dz1 = LagrangeDerivativeMatrix_GLL2(length(pz)-1,D.dofe(3)-1);
                for j=1:D.dofe(3)
                    Hz1(:,j) = LagrangeGLL(indz(j),pz,D.dofe(3));
                end
                DDz1 = 1;
            end
            
            % t-direction
            Ht1   = 1;
            Dt1   = 1;
            DDt1  = 1;
            
            D.H1  = {  Hx1;  Hy1;  Hz1;  Ht1 };
            D.D1  = {  Dx1;  Dy1;  Dz1;  Dt1 };
            D.DD1 = { DDx1; DDy1; DDz1; DDt1 };
            
%             % t-direction
%             if (D.C(4)==1)
%                 H1{4}   = HermiteN(indt,pt,Jt);
%                 D1{4}   = DHermiteN(indt,pt,Jt);
%                 DD1{4}  = DDHermiteN(indt,pt,Jt);
%             else
%                 Dt1 = LagrangeDerivativeMatrix_GLL2(length(pt)-1,D.dofe(4)-1);
%                 for j=1:D.dofe(4)
%                     Ht1(:,j) = LagrangeGLL(indt(j),pt,D.dofe(4));
%                 end
%                 H1{4} = Ht1;
%                 D1{4} = Dt1;
%             end

            %             for j=1:D.dofe(1)
            %                 H1{1}(:,j)=HermiteN(indx(j),px,Jx) ;
            %                 Dx1(:,j)=DHermiteN(indx(j),px,Jx) ;
            %                 Dxx1(:,j)=DDHermiteN(indx(j),px,Jx) ;
            %             end
            %             for j=1:mesh.dofe1y
            %                 Hy1(:,j)=HermiteN(indy(j),py,Jy) ;
            %                 Dy1(:,j)=DHermiteN(indy(j),py,Jy) ;
            %                 Dyy1(:,j)=DDHermiteN(indy(j),py,Jy) ;
            %             end
            %             if (mesh.Ct_basis=='C1')
            %                 for j=1:mesh.dofe1t
            %                     Ht1(:,j)=HermiteN(indt(j),pt,Jt) ;
            %                     Dt1(:,j)=DHermiteN(indt(j),pt,Jt) ;
            %                     Dtt1(:,j)=DDHermiteN(indt(j),pt,Jt) ;
            %                 end
            %             else
            % 
            %             end
            
            %% 3D Operators
            D.W   = superkron(pwt,pwz,pwy,pwx);
            
            D.H   = D.getTransform();
            D.Dx  = D.getTransform(1,Dx1);
            D.Dy  = D.getTransform(2,Dy1);
            D.Dz  = D.getTransform(3,Dz1);
            D.Dt  = D.getTransform(4,Dt1);
            
            D.Dxx = D.getTransform(1,DDx1);
            D.Dyy = D.getTransform(2,DDy1);
            
            
%             D.H   = superkron( H1{4} , H1{3} , H1{2} , H1{1} );
%             
%             D.Dx  = superkron( H1{4} , H1{3} , H1{2} , D1{1} );
%             D.Dy  = superkron( H1{4} , H1{3} , D1{2} , H1{1} );
%             D.Dz  = superkron( H1{4} , D1{3} , H1{2} , H1{1} );
%             D.Dt  = superkron( D1{4} , H1{3} , H1{2} , H1{1} );
%             
%             D.Dxx = superkron( H1{4}, H1{3} , H1{2} , DD1{1} );
%             D.Dyy = superkron( H1{4}, H1{3} , DD1{2}, H1{1}  );
            if (D.C(3)==1)
                D.Dzz = superkron( Ht1, DDz1 , Hy1 , Hx1 );
            end
            if (D.C(4)==1)
                D.Dtt = superkron( DD1{4} , H1{3} , H1{2} , H1{1} );
            end
            
            D.Dxy  = superkron( Ht1 , Hz1, Dy1 , Dx1  );
            
            D.Dxxy = superkron( Ht1 , Hz1, Dy1 , DDx1  );
            D.Dyyx = superkron( Ht1 , Hz1, DDy1 , Dx1  );
            
            D.Dxyx = superkron( 1, Dy1 , Dx1, Dx1);
            
            %% Boundary elements
            D.Bt0       = struct;
            D.Bt0.W     = superkron(1,pwz,pwy,pwx);
            D.Bt0.H     = D.getTransform(4,Ht1(1,:)); %  superkron( H1{4}(1,:) , H1{2} ,H1{1} ) ;
            D.Bt0.Dn    = D.getTransform(4,Dt1(1,:)); %  superkron( D1{4}(1,:) , H1{2} ,H1{1} ) ;
            
            D.Bt1       = struct;
            D.Bt1.W     = superkron(1,pwz,pwy,pwx);
            D.Bt1.H     = D.getTransform(4,Ht1(end,:));  % superkron( H1{4}(end,:) , H1{2} ,H1{1} ) ;
            D.Bt1.Dn    = D.getTransform(4,Dt1(end,:));   % superkron( D1{4}(end,:) , H1{2} ,H1{1} ) ;
            D.Bt1.Hpos  = D.getPositions(4,Ht1(end,:));
            D.Bt1.Dpos  = D.getPositions(4,Dt1(end,:));
            
            
            D.Bz0       = struct;
            numVar = 1;
            D.Bz0.thickness = numVar;
            
            D.Bz0.W     = superkron(pwt,ones(numVar,1),pwy,pwx);
            D.Bz0.H     = D.getTransform(3,Hz1( 1:numVar,:));         %  superkron( Hz1( 1 ,:) , Hy1 , Hx1 ) ;
            D.Bz0.Dx    = D.getTransform(3,Hz1( 1:numVar,:),1,Dx1); 	%  superkron( Hz1( 1 ,:) , Hy1 , Dx1 ) ;
            D.Bz0.Dy    = D.getTransform(3,Hz1( 1:numVar,:),2,Dy1); 	%  superkron( Hz1( 1 ,:) , Dy1 , Hx1 ) ;
            D.Bz0.Dz    = D.getTransform(3,Dz1( 1:numVar,:));         %  superkron( Dz1( 1 ,:) , Hy1 , Hx1 ) ;
            D.Bz0.Dzz   = D.getTransform(3,DDz1( 1:numVar,:));
            D.Bz0.Dn    = D.Bz0.Dz;
            D.Bz0.Dnn   = D.Bz0.Dzz;
            D.Bz0.Hpos  = D.getPositions(3,Hz1( 1:numVar,:));
            D.Bz0.Dpos  = D.getPositions(3,Dz1( 1:numVar,:));
            
            D.Bz1       = struct;
            numVar = 1;
            D.Bz1.thickness = numVar;
            D.Bz1.W     = superkron(pwt,ones(numVar,1),pwy,pwx);
            D.Bz1.H     = D.getTransform(3,Hz1(end+1-numVar:end,:));         %  superkron( Hz1(end,:) , Hy1 , Hx1 ) ;
            D.Bz1.Dx    = D.getTransform(3,Hz1(end+1-numVar:end,:),1,Dx1);	%  superkron( Hz1(end,:) , Hy1 , Dx1 ) ;
            D.Bz1.Dy    = D.getTransform(3,Hz1(end+1-numVar:end,:),2,Dy1); 	%  superkron( Hz1(end,:) , Dy1 , Hx1 ) ;
            D.Bz1.Dz    = D.getTransform(3,Dz1(end+1-numVar:end,:));         %  superkron( Dz1(end,:) , Hy1 , Hx1 ) ;
            D.Bz1.Dzz   = D.getTransform(3,DDz1(end+1-numVar:end,:)); 
            D.Bz1.Dn    = D.Bz1.Dz;
            D.Bz1.Dnn   = D.Bz1.Dzz;
            D.Bz1.Hpos  = D.getPositions(3,Hz1(end+1-numVar:end,:));
            D.Bz1.Dpos  = D.getPositions(3,Dz1(end+1-numVar:end,:));
            
            D.By0       = struct;
            D.By0.thickness = 1;
            D.By0.W     = superkron(pwt,pwz,1,pwx);
            D.By0.H     = D.getTransform(2,Hy1( 1 ,:));       	%  superkron( Hz1 , Hy1( 1 ,:) , Hx1 ) ;
            D.By0.Dx    = D.getTransform(2,Hy1( 1 ,:),1,Dx1); 	%  superkron( Hz1 , Hy1( 1 ,:) , Dx1 ) ;
            D.By0.Dy    = D.getTransform(2,Dy1( 1 ,:));       	%  superkron( Hz1 , Dy1( 1 ,:) , Hx1 ) ;
            D.By0.Dz    = D.getTransform(2,Hy1( 1 ,:),3,Dz1);  	%  superkron( Dz1 , Hy1( 1 ,:) , Hx1 ) ;
            D.By0.Dyy   = D.getTransform(2,DDy1( 1 ,:));       	%  superkron( Hz1 , Dy1( 1 ,:) , Hx1 ) ;
            D.By0.Dn    = D.By0.Dy;
            D.By0.Dnn   = D.By0.Dyy;
            D.By0.Hpos  = D.getPositions(2,Hy1( 1 ,:));
            D.By0.Dpos  = D.getPositions(2,Dy1( 1 ,:));
            
            D.By1       = struct;
            D.By1.thickness = 1;            
            D.By1.W     = superkron(pwt,pwz,1,pwx);
            D.By1.H     = D.getTransform(2,Hy1(end,:));         %  superkron( Hz1 , Hy1(end,:) , Hx1 ) ;
            D.By1.Dx    = D.getTransform(2,Hy1(end,:),1,Dx1);	%  superkron( Hz1 , Hy1(end,:) , Dx1 ) ;
            D.By1.Dy    = D.getTransform(2,Dy1(end,:));      	%  superkron( Hz1 , Dy1(end,:) , Hx1 ) ;
            D.By1.Dz    = D.getTransform(2,Hy1(end,:),3,Dz1); 	%  superkron( Dz1 , Hy1(end,:) , Hx1 ) ;
            D.By1.Dyy   = D.getTransform(2,DDy1(end,:));
            D.By1.Dn    = D.By1.Dy;
            D.By1.Dnn   = D.By1.Dyy;
            D.By1.Hpos  = D.getPositions(2,Hy1(end,:));
            D.By1.Dpos  = D.getPositions(2,Dy1(end,:));
            
            D.Bx0       = struct;
            D.Bx0.thickness = 1;
            D.Bx0.W   	= superkron(pwt,pwz,pwy,1);
            D.Bx0.H     = D.getTransform(1,Hx1(1,:));           %  superkron( Hz1 , Hy1 , Hx1( 1 ,:) ) ;
            D.Bx0.Dx    = D.getTransform(1,Dx1( 1 ,:));         %  superkron( Hz1 , Hy1 , Dx1( 1 ,:) ) ;
            D.Bx0.Dy    = D.getTransform(1,Hx1( 1 ,:),2,Dy1);  	%  superkron( Hz1 , Dy1 , Hx1( 1 ,:) ) ;
            D.Bx0.Dz    = D.getTransform(1,Hx1( 1 ,:),3,Dz1);  	%  superkron( Dz1 , Hy1 , Hx1( 1 ,:) ) ;
            D.Bx0.Dxx   = D.getTransform(1,DDx1( 1 ,:));
            D.Bx0.Dn    = D.Bx0.Dx;
            D.Bx0.Dnn   = D.Bx0.Dxx;
            D.Bx0.Hpos  = D.getPositions(1,Hx1(1,:));
            D.Bx0.Dpos  = D.getPositions(1,Dx1(1,:));
            
            D.Bx1       = struct;
            D.Bx1.thickness = 1;
            D.Bx1.W     = superkron(pwt,pwz,pwy,1);
            D.Bx1.H     = D.getTransform(1,Hx1(end,:));         %  superkron( Hz1 , Hy1 , Hx1(end,:) ) ;
            D.Bx1.Dx    = D.getTransform(1,Dx1(end,:));         %  superkron( Hz1 , Hy1 , Dx1(end,:) ) ;
            D.Bx1.Dy    = D.getTransform(1,Hx1(end,:),2,Dy1); 	%  superkron( Hz1 , Dy1 , Hx1(end,:) ) ;
            D.Bx1.Dz    = D.getTransform(1,Hx1(end,:),3,Dz1); 	%  superkron( Dz1 , Hy1 , Hx1(end,:) ) ;
            D.Bx1.Dxx   = D.getTransform(1,DDx1(end,:));
            D.Bx1.Dn    = D.Bx1.Dx;
            D.Bx1.Dnn   = D.Bx1.Dxx;
            D.Bx1.Hpos  = D.getPositions(1,Hx1(end,:));
            D.Bx1.Dpos  = D.getPositions(1,Dx1(end,:));
            
            D.Bp        = struct;
            D.Bp.thickness = 1;
            D.Bp.W      = superkron(pwt,pwz,1,1);
            D.Bp.H      = D.getTransform(1,Hx1(1,:),2,Hy1(1,:));
            D.Bp.Hpos   = D.getPositions(1,Hx1(1,:),2,Hy1(1,:));
            
            %-- depends on lv
            
            %% SINGLE LEVEL: refinement
            [pmx,pmwx] = GLL(D.Q(1),-1,0);
            [ppx,ppwx] = GLL(D.Q(1), 0,1);
            [pmy,pmwy] = GLL(D.Q(2),-1,0);
            [ppy,ppwy] = GLL(D.Q(2), 0,1);
            [pmz,pmwz] = GLL(D.Q(3),-1,0);
            [ppz,ppwz] = GLL(D.Q(3), 0,1);
            [pmt,pmwt] = GLL(D.Q(4),-1,0);
            [ppt,ppwt] = GLL(D.Q(4), 0,1);
            
            % x-direction
            Hxim = HermiteN(indx,pmx,Jx);
            Hxip = HermiteN(indx,ppx,Jx);
            
            % y-direction
            Hyim = HermiteN(indy,pmy,Jy) ;
            Hyip = HermiteN(indy,ppy,Jy) ;
            
            % z-direction
            if (D.C(3)==1)
                Hzim = HermiteN(indz,pmz,Jz) ;
                Hzip = HermiteN(indz,pmz,Jz) ;
            else
                for j=1:D.dofe(3)
                    Hzim(:,j) = LagrangeGLL(indz(j),pmz,D.dofe(3));
                    Hzip(:,j) = LagrangeGLL(indz(j),ppz,D.dofe(3));
                end
            end
            
            % t-direction
            if (D.C(4)==1)
                for j=1:D.dofe(4)
                    Htim(:,j)=HermiteN(indt(j),pmt,Jt) ;
                    Htip(:,j)=HermiteN(indt(j),ppt,Jt) ;
                end
            else
                for j=1:D.dofe(4)
                    Htim(:,j) = LagrangeGLL(indt(j),pmt,D.dofe(4));
                    Htip(:,j) = LagrangeGLL(indt(j),ppt,D.dofe(4));
                end
            end
            
            D.Zxm=Hx1\Hxim;     D.Zxm(abs(D.Zxm)<D.ROUNDOFF_LIMIT)=0;
            D.Zxp=Hx1\Hxip;     D.Zxp(abs(D.Zxp)<D.ROUNDOFF_LIMIT)=0;
            
            D.Zym=Hy1\Hyim;     D.Zym(abs(D.Zym)<D.ROUNDOFF_LIMIT)=0;
            D.Zyp=Hy1\Hyip;     D.Zyp(abs(D.Zyp)<D.ROUNDOFF_LIMIT)=0;
            
            D.Zzm=Hz1\Hzim;     D.Zzm(abs(D.Zzm)<D.ROUNDOFF_LIMIT)=0;
            D.Zzp=Hz1\Hzip;     D.Zzp(abs(D.Zzp)<D.ROUNDOFF_LIMIT)=0;
            
            D.Ztm=Ht1\Htim;     D.Ztm(abs(D.Ztm)<D.ROUNDOFF_LIMIT)=0;
            D.Ztp=Ht1\Htip;     D.Ztp(abs(D.Ztp)<D.ROUNDOFF_LIMIT)=0;
            
            D.Hx = Hx1;
            D.Hy = Hy1;
            D.Hz = Hz1;
            D.Ht = Ht1;
            
%             D.Hyt = sparse( kron(Ht1,Hy1) );
            
            D.Himm = superkron( Ht1, Hyim, Hxim );
            D.Himp = superkron( Ht1, Hyim, Hxip );
            D.Hipm = superkron( Ht1, Hyip, Hxim );
            D.Hipp = superkron( Ht1, Hyip, Hxip );
            
            %% SINGLE LEVEL: coarsening
            % Determine which GLL nodes are within which part of a cell
            % which is split into 8
            sxm = find(px<=0)';         sxp = find(px>=0)';
            sym = find(py<=0)';         syp = find(py>=0)';
            stm = find(pt<=0)';         stp = find(pt>=0)';
            
            % Rescale these locations to a standard element (values between -1 and 1)
            psxm = 2*px(sxm) + 1 ;      psxp = 2*px(sxp) - 1 ;
            psym = 2*py(sym) + 1 ;      psyp = 2*py(syp) - 1 ;
            pstm = 2*pt(stm) + 1 ;      pstp = 2*pt(stp) - 1 ;
            
            
            Hxism = HermiteN(indx,psxm,Jx) ;
            Hxisp = HermiteN(indx,psxp,Jx) ;
            Hyism = HermiteN(indy,psym,Jy) ;
            Hyisp = HermiteN(indy,psyp,Jy) ;
                
%             for j=1:D.dofe(1)
%                 Hxism(:,j)=HermiteN(indx(j),psxm,Jx) ;
%                 Hxisp(:,j)=HermiteN(indx(j),psxp,Jx) ;
%             end          
            
            % y-direction
%             for j=1:D.dofe(2)
%                 Hyism(:,j)=HermiteN(indy(j),psym,Jy) ;
%                 Hyisp(:,j)=HermiteN(indy(j),psyp,Jy) ;
%             end

            D.Hismm = superkron( Ht1, Hz1, Hyism, Hxism );
            D.Hismp = superkron( Ht1, Hz1, Hyism, Hxisp );
            D.Hispm = superkron( Ht1, Hz1, Hyisp, Hxism );
            D.Hispp = superkron( Ht1, Hz1, Hyisp, Hxisp );
 
            % Create sec2 -- Lv(e_new)-Lv_old(e_old)==-1
            D.sec1 = cell(4,1) ;
            D.sec1{1} = [] ;  D.sec1{2} = [] ;
            for it=1:D.Q(4)
                for i=1:sym(end)
                    D.sec1{1} = [D.sec1{1} sxm+D.Q(1)*(i-1)+D.Q(1)*D.Q(2)*(it-1)] ;
                    D.sec1{2} = [D.sec1{2} sxp+D.Q(1)*(i-1)+D.Q(1)*D.Q(2)*(it-1)] ;
                end
            end
            D.sec1{3} = [] ;  D.sec1{4} = [] ;
            for it=1:D.Q(4)
                for j=syp(1):syp(end)
                    D.sec1{3} = [D.sec1{3} sxm+D.Q(1)*(j-1)+D.Q(1)*D.Q(2)*(it-1)] ;
                    D.sec1{4} = [D.sec1{4} sxp+D.Q(1)*(j-1)+D.Q(1)*D.Q(2)*(it-1)] ;
                end
            end
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

% % %             %% DOUBLE LEVEL: refinement
% % %             [pmmx,pmwx]=GLL(mesh.Qx,-1  ,-0.5);
% % %             [pmpx,pmwx]=GLL(mesh.Qx,-0.5, 0  );
% % %             [ppmx,pmwx]=GLL(mesh.Qx, 0  , 0.5);
% % %             [pppx,pmwx]=GLL(mesh.Qx, 0.5, 1  );
% % %             [pmmy,pmwy]=GLL(mesh.Qy,-1  ,-0.5);
% % %             [pmpy,pmwy]=GLL(mesh.Qy,-0.5, 0  );
% % %             [ppmy,pmwy]=GLL(mesh.Qy, 0  , 0.5);
% % %             [pppy,pmwy]=GLL(mesh.Qy, 0.5, 1  );
% % %             
% % %             Hxim = zeros(mesh.Qx,mesh.dofe1x);
% % %             Hxip = zeros(mesh.Qx,mesh.dofe1x);
% % %             
% % %             for j=1:mesh.dofe1x
% % %                 Hximm(:,j)=HermiteN(indx(j),pmmx,Jx) ;  % = Hx1 * Zm * Zm
% % %                 Hximp(:,j)=HermiteN(indx(j),pmpx,Jx) ;  % = Hx1 * Zp * Zm
% % %                 Hxipm(:,j)=HermiteN(indx(j),ppmx,Jx) ;  % = Hx1 * Zm * Zp
% % %                 Hxipp(:,j)=HermiteN(indx(j),pppx,Jx) ;  % = Hx1 * Zp * Zp
% % %             end   
% % %                 
% % %             for j=1:mesh.dofe1y
% % %                 Hyimm(:,j)=HermiteN(indy(j),pmmy,Jy) ;
% % %                 Hyimp(:,j)=HermiteN(indy(j),pmpy,Jy) ;
% % %                 Hyipm(:,j)=HermiteN(indy(j),ppmy,Jy) ;
% % %                 Hyipp(:,j)=HermiteN(indy(j),pppy,Jy) ;
% % %             end
% % % 
% % %             % Lv(e_new)=2, Lv_old(e_old)=0
% % %             D.Himmmm = superkron( Hz1, Hyimm, Hximm ); % = superkron(Hz1,Hy1,Hx1) * superkron( eye(5), kron(D.Zym,D.Zxm) * kron(D.Zym,D.Zxm) );
% % %             D.Himmmp = superkron( Hz1, Hyimm, Hximp ); % = superkron(Hz1,Hy1,Hx1) * superkron( eye(5), kron(D.Zym,D.Zxp) * kron(D.Zym,D.Zxm) );
% % %             D.Himmpm = superkron( Hz1, Hyimp, Hximm ); % = superkron(Hz1,Hy1,Hx1) * superkron( eye(5), kron(D.Zyp,D.Zxm) * kron(D.Zym,D.Zxm) );
% % %             D.Himmpp = superkron( Hz1, Hyimp, Hximp ); % = superkron(Hz1,Hy1,Hx1) * superkron( eye(5), kron(D.Zyp,D.Zxp) * kron(D.Zym,D.Zxm) );
% % % 
% % %             D.Himpmm = superkron( Hz1,Hyimm,Hxipm );
% % %             D.Himpmp = superkron( Hz1,Hyimm,Hxipp );
% % %             D.Himppm = superkron( Hz1,Hyimp,Hxipm );
% % %             D.Himppp = superkron( Hz1,Hyimp,Hxipp );
% % % 
% % %             D.Hipmmm = superkron( Hz1,Hyipm,Hximm );
% % %             D.Hipmmp = superkron( Hz1,Hyipm,Hximp );
% % %             D.Hipmpm = superkron( Hz1,Hyipp,Hximm );
% % %             D.Hipmpp = superkron( Hz1,Hyipp,Hximp );
% % % 
% % %             D.Hippmm = superkron( Hz1,Hyipm,Hxipm );
% % %             D.Hippmp = superkron( Hz1,Hyipm,Hxipp );
% % %             D.Hipppm = superkron( Hz1,Hyipp,Hxipm );
% % %             D.Hipppp = superkron( Hz1,Hyipp,Hxipp );
% % % 
% % %             %% DOUBLE LEVEL: coarsening
% % %             % Determine which GLL nodes are within which part of a cell
% % %             % which is split into 64
% % %             sxmm = find(px<=-0.5)';
% % %             sxmp = find((px>=-0.5) & (px<=0))';
% % %             sxpm = find((px>=0) & (px<=0.5))';
% % %             sxpp = find((px>=0.5))';
% % %             
% % %             symm = find(py<=-0.5)';
% % %             symp = find((py>=-0.5) & (py<=0))';
% % %             sypm = find((py>=0) & (py<=0.5))';
% % %             sypp = find((py>=0.5))';
% % %             
% % %             psxmm = 4*px(sxmm) + 3 ;
% % %             psxmp = 4*px(sxmp) + 1 ;
% % %             psxpm = 4*px(sxpm) - 1 ;
% % %             psxpp = 4*px(sxpp) - 3 ;
% % %             psymm = 4*py(symm) + 3 ;
% % %             psymp = 4*py(symp) + 1 ;
% % %             psypm = 4*py(sypm) - 1 ;
% % %             psypp = 4*py(sypp) - 3 ;
% % %             
% % %             for j=1:mesh.dofe1x
% % %                 Hxismm(:,j)=HermiteN(indx(j),psxmm,Jx) ;
% % %                 Hxismp(:,j)=HermiteN(indx(j),psxmp,Jx) ;
% % %                 Hxispm(:,j)=HermiteN(indx(j),psxpm,Jx) ;
% % %                 Hxispp(:,j)=HermiteN(indx(j),psxpp,Jx) ;
% % %             end   
% % %                 
% % %             for j=1:mesh.dofe1y                
% % %                 Hyismm(:,j)=HermiteN(indy(j),psymm,Jy) ;
% % %                 Hyismp(:,j)=HermiteN(indy(j),psymp,Jy) ;
% % %                 Hyispm(:,j)=HermiteN(indy(j),psypm,Jy) ;
% % %                 Hyispp(:,j)=HermiteN(indy(j),psypp,Jy) ;
% % %             end
% % %             
% % %             D.Hismmmm = superkron( Hz1,Hyismm,Hxismm) ;
% % %             D.Hismmmp = superkron(Hz1,Hyismm,Hxismp) ;
% % %             D.Hismmpm = superkron(Hz1,Hyismp,Hxismm) ;
% % %             D.Hismmpp = superkron(Hz1,Hyismp,Hxismp) ;
% % % 
% % %             D.Hismpmm = superkron(Hz1,Hyismm,Hxispm);
% % %             D.Hismpmp = superkron(Hz1,Hyismm,Hxispp) ;
% % %             D.Hismppm = superkron(Hz1,Hyismp,Hxispm) ;
% % %             D.Hismppp = superkron(Hz1,Hyismp,Hxispp) ;
% % % 
% % %             D.Hispmmm = superkron(Hz1,Hyispm,Hxismm) ;
% % %             D.Hispmmp = superkron(Hz1,Hyispm,Hxismp) ;
% % %             D.Hispmpm = superkron(Hz1,Hyispp,Hxismm) ;
% % %             D.Hispmpp = superkron(Hz1,Hyispp,Hxismp) ;
% % % 
% % %             D.Hisppmm = superkron(Hz1,Hyispm,Hxispm) ;
% % %             D.Hisppmp = superkron(Hz1,Hyispm,Hxispp) ;
% % %             D.Hispppm = superkron(Hz1,Hyispp,Hxispm) ;
% % %             D.Hispppp = superkron(Hz1,Hyispp,Hxispp) ;
% % % 
% % %             % Create sec2 -- Lv(e_new)=0, Lv_old(e_old)=2
% % %             D.sec2 = cell(16,1) ;
% % %             D.sec2{1}=[];  D.sec2{2}=[];  D.sec2{5}=[];  D.sec2{6}=[];
% % %             for it=1:mesh.Qz
% % %                 for i=1:symm(end)
% % %                     D.sec2{1} = [D.sec2{1} sxmm+mesh.Qx*(i-1)+mesh.Qx*mesh.Qy*(it-1)] ;
% % %                     D.sec2{2} = [D.sec2{2} sxmp+mesh.Qx*(i-1)+mesh.Qx*mesh.Qy*(it-1)] ;
% % %                     D.sec2{5} = [D.sec2{5} sxpm+mesh.Qx*(i-1)+mesh.Qx*mesh.Qy*(it-1)] ;
% % %                     D.sec2{6} = [D.sec2{6} sxpp+mesh.Qx*(i-1)+mesh.Qx*mesh.Qy*(it-1)] ;
% % %                 end
% % %             end
% % %             D.sec2{3}=[];  D.sec2{4}=[];  D.sec2{7}=[];  D.sec2{8}=[];
% % %             for it=1:mesh.Qz
% % %                 for j=symp(1):symp(end)
% % %                     D.sec2{3} = [D.sec2{3} sxmm+mesh.Qx*(j-1)+mesh.Qx*mesh.Qy*(it-1)] ;
% % %                     D.sec2{4} = [D.sec2{4} sxmp+mesh.Qx*(j-1)+mesh.Qx*mesh.Qy*(it-1)] ;
% % %                     D.sec2{7} = [D.sec2{7} sxpm+mesh.Qx*(j-1)+mesh.Qx*mesh.Qy*(it-1)] ;
% % %                     D.sec2{8} = [D.sec2{8} sxpp+mesh.Qx*(j-1)+mesh.Qx*mesh.Qy*(it-1)] ;
% % %                 end
% % %             end
% % %             D.sec2{9}=[];  D.sec2{10}=[];  D.sec2{13}=[];  D.sec2{14}=[];
% % %             for it=1:mesh.Qz
% % %                 for k=sypm(1):sypm(end)
% % %                     D.sec2{9} = [D.sec2{9} sxmm+mesh.Qx*(k-1)+mesh.Qx*mesh.Qy*(it-1)] ;
% % %                     D.sec2{10} = [D.sec2{10} sxmp+mesh.Qx*(k-1)+mesh.Qx*mesh.Qy*(it-1)] ;
% % %                     D.sec2{13} = [D.sec2{13} sxpm+mesh.Qx*(k-1)+mesh.Qx*mesh.Qy*(it-1)] ;
% % %                     D.sec2{14} = [D.sec2{14} sxpp+mesh.Qx*(k-1)+mesh.Qx*mesh.Qy*(it-1)] ;
% % %                 end
% % %             end
% % %             D.sec2{11}=[];  D.sec2{12}=[];  D.sec2{15}=[];  D.sec2{16}=[];
% % %             for it=1:mesh.Qz
% % %                 for l=sypp(1):sypp(end)
% % %                     D.sec2{11} = [D.sec2{11} sxmm+mesh.Qx*(l-1)+mesh.Qx*mesh.Qy*(it-1)] ;
% % %                     D.sec2{12} = [D.sec2{12} sxmp+mesh.Qx*(l-1)+mesh.Qx*mesh.Qy*(it-1)] ;
% % %                     D.sec2{15} = [D.sec2{15} sxpm+mesh.Qx*(l-1)+mesh.Qx*mesh.Qy*(it-1)] ;
% % %                     D.sec2{16} = [D.sec2{16} sxpp+mesh.Qx*(l-1)+mesh.Qx*mesh.Qy*(it-1)] ;
% % %                 end
% % %             end
            
        end     % end setupHermiteBasis_3D_refine

        function out = getTransform(s,direction,array,direction2,array2)
            temp = cell(4,1);
            for i=1:4
                temp{i} = s.H1{i};
            end
            if nargin>1
                temp{direction} = array; 
            end
            if nargin>3
                temp{direction2} = array2;
            end
            out = superkron( temp{4}, temp{3}, temp{2}, temp{1} );       
        end
        
        function out = getPositions(s,direction,array,direction2,array2)
            temp = cell(4,1);
            for i=1:4
                temp{i} = eye(s.dofe(i));
            end
            if nargin>1
                temp{direction} = array; 
            end
            if nargin>3
                temp{direction2} = array2;
            end
            [~,out] = find( superkron( temp{4}, temp{3}, temp{2}, temp{1} ) );
        end
        
        function out = getH(s)
            out = s.H;
        end
        
        function out = xDof(s)
            out = s.dofe(1);
        end
        
        function out = yDof(s)
            out = s.dofe(2);
        end
        
        function out = zDof(s)
            out = s.dofe(3);
        end
        
        function out = tDof(s)
            out = s.dofe(4);
        end
        
        function [Zmm,Zpm,Zmp,Zpp] = getZrefine(s)
            xDof = s.dofe(1);
            yDof = s.dofe(2);
            zDof = s.dofe(3);
            
%             % Zxyz
%             reset = true(xDof,yDof,zDof);
%             reset(      :    ,     1:2   ,:) = false;       % bottom
%             reset(     1:2   ,      :    ,:) = false;   	% left
%             reset(      :    ,yDof-1:yDof,:) = false;       % top
%             reset(xDof-1:xDof,      :    ,:) = false;       % right
            
            % scaling
            xs = sparse( eye(s.xDof));
            ys = sparse( eye(s.yDof));
            zs = sparse( eye(s.zDof));
            
            % If the grid is refined in a certain direction, adjust the
            % 'base' in that direction. The derivative values have to be
            % scaled with a factor of 2, because the Jacobian is changing 
            % between elements with different levels. These derivative
            % variables are located at location '2' and 'end'.
%             if (s.xRefine>1)
%                 xs( 2 , 2 ) = 0.5;
%                 xs(end,end) = 0.5;
% %             end
%             
% %             if (s.yRefine>1)
%                 ys( 2 , 2 ) = 0.5;
%                 ys(end,end) = 0.5;
%             end
            
%             if (s.zRefine>1)
%                 zBase( 2 , 2 ) = 0.5;
%                 zBase(end,end) = 0.5;
%             end
            
            zBase = sparse( eye(zDof));
            Zmm = superkron(zBase,s.Zym*ys,s.Zxm*xs);
            Zpm = superkron(zBase,s.Zym*ys,s.Zxp*xs);
            Zmp = superkron(zBase,s.Zyp*ys,s.Zxm*xs);
            Zpp = superkron(zBase,s.Zyp*ys,s.Zxp*xs);
            
%             Zmm(  :  ,reset) = 0; 
%             Zmm(reset,  :  ) = 0;
%             Zmm(reset,reset) = eye(nnz(reset));
%             
%             Zpm(  :  ,reset) = 0; 
%             Zpm(reset,  :  ) = 0;
%             Zpm(reset,reset) = eye(nnz(reset));
%             
%             Zmp(  :  ,reset) = 0; 
%             Zmp(reset,  :  ) = 0;
%             Zmp(reset,reset) = eye(nnz(reset));
%             
%             Zpp(  :  ,reset) = 0; 
%             Zpp(reset,  :  ) = 0;
%             Zpp(reset,reset) = eye(nnz(reset));
        end
        
        function plot(s)
            
        end

    	function standardElement = save(s)
            standardElement       	  = struct;
            standardElement.class     = class(s);
            standardElement.P         = s.P;
            standardElement.Q         = s.Q;
            standardElement.C         = s.C;
            standardElement.SDIM      = s.SDIM;
            standardElement.spaceTime = s.spaceTime;
        end
        
    end %methods
    
    methods (Static)
        function s = init()
            settings = System.settings.stde;
            s = LSQdisc_4(settings.Pn,settings.Q,settings.C,settings.sDim,System.settings.time.spaceTime);
        end
        
        function out = load(standardElement,Q)
            P = standardElement.P;
            if (nargin~=2 || isempty(Q) || Q==0)
                Q = standardElement.Q;
            end
            C = standardElement.C;
            SDIM = standardElement.SDIM;
            spaceTime = standardElement.spaceTime;
            
            out = LSQdisc_4(P,Q,C,SDIM,spaceTime);
        end
    end

end %classdef
