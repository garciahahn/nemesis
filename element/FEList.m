classdef FEList < dlnode
    %UNTITLED10 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Access=private)
        dimension;
        
        headNode;
        currentNode;
    end
    
    properties
        numElements;
    end
    
    methods
        function s = FEList(fElements)
            
            if iscell(fElements)
                s.dimension = fElements{1}.dimension;
                s.headNode = dlnode( fElements{1} );
                s.currentNode = s.headNode;
                s.numElements = 1;

                remaining = [fElements{2:end}];
                for fElement = remaining
                    s.insert(fElement);
                end
                
            else
                s.dimension = fElements.dimension;
                s.headNode = dlnode( fElements );  
                s.currentNode = s.headNode;                
                s.numElements = 1;
            end
        end
        
        function insert(s,fElement)
            newNode = dlnode( fElement );
            
            try
            if ( isempty(s.currentNode) || ~isvalid(s.currentNode) )
                s.currentNode = s.headNode;
            end
            catch
                disp('')
                
            end
            
            if ( ~isempty(s.headNode))
                newNode.insertAfter( s.currentNode );
            else
                s.headNode = newNode;
            end
            s.currentNode = newNode;
            
            s.numElements = s.numElements + 1;
        end
        
        function remove(s,fElement)
            s.currentNode = s.headNode;
            found = false;
            while (~isempty( s.currentNode )  && ~found )
                if ( s.current == fElement )
                    s.numElements = s.numElements - 1;
                    
                    if (s.currentNode == s.headNode)
                        s.headNode = s.currentNode.Next;
                        tmp = s.headNode;
                    else
                        tmp = s.currentNode.Prev;
                    end
                    
                    s.currentNode.delete;
                    
                    if ( ~isvalid(s.currentNode) )
                        s.currentNode = tmp;
                    end
                    found = true;
                end
                if (~found)
                    s.currentNode = s.currentNode.Next;
                end
            end
            if (~found)
                error('You tried to remove a fElement from the FEList, but it is not in the list!')
            end
        end
        
        function out = head(s)
            s.currentNode = s.headNode;
            out = s.current;
        end
        
        function out = current(s)
            try
            out = s.currentNode.Data;
            catch
               disp('') 
            end
        end
        
        function out = next(s)
            s.currentNode = s.currentNode.Next;
            if ( ~isempty(s.currentNode) )
                out = s.current;
            else
                out = [];
            end
        end
        
        function replace(s,items)            
            % create nodes for all items to insert
            newNode = dlnode( items );              
            s.currentNode.replace( newNode );
            s.currentNode = newNode;
            
            s.numElements = s.numElements + numel(items)-1;
        end
        
        % Convert the linked list in a cell array
        function out = getCellArray(s)
            out = cell(1,s.numElements);
            out{1} = s.headNode.Data;
            s.currentNode = s.headNode;
            for i=2:s.numElements
                out{i} = s.next;
            end
        end
        
        function check(s)
            s.currentNode = s.headNode;
            count = 0;
            while ( ~isempty(s.currentNode) )
                if ( ~isvalid( s.currentNode.Data ) || isempty( s.currentNode.Data ) )
                    count = count + 1;
                end
                s.currentNode = s.currentNode.Next;
            end
            
            fprintf('There are %d removed nodes\n',count);
        end
        
        function fix(s)
            s.currentNode = s.headNode;
            count = 0;
            while ( ~isempty(s.currentNode) )
                fix = false;
                if ( ~isvalid( s.currentNode.Data ) || isempty( s.currentNode.Data ) )
                    if (s.currentNode==s.headNode)
                        tmp = s.currentNode.Next;
                    else
                        tmp = s.currentNode.Prev;
                    end
                    s.currentNode.delete;
                    s.currentNode = tmp;
                    count = count + 1;      
                    fix = true;
                end
                if (~fix)
                    s.currentNode = s.currentNode.Next;
                end
            end
            
            s.numElements = s.numElements - count;
            
            fprintf('There were %d removed nodes, the new numElements is %d\n',count,s.numElements);
        end
        
        % Function to destroy a linked list efficiently
        function destroy(s)
            while ~isempty(s.headNode)
                oldHead = s.headNode;
                s.headNode = oldHead.Next;
                disconnect(oldHead);
            end
        end 
        
        function out = numActiveElements(s)
            active = zeros(1,s.numElements);
            if (s.numElements>0)
                active(1) = s.headNode.Data.active;
                s.currentNode = s.headNode;
                for i=2:s.numElements
                    active(i) = s.next.active;
                end
                out = nnz(active);
            else
                out = 0;
            end
        end
        
        function disp(s)
            % DISP  Displays a link node.
            fprintf('Doubly-linked list node with %d (%d active) elements\n',s.numElements,s.numActiveElements)
            if (s.numElements>0)
                fprintf('    Node type : %s\n',class(s.headNode.Data))
            end
        end
    end
end

