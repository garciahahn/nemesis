%*****************************
% 05 May 2014
%*****************************

%addpath('/work/lsqdim/library/HighOrderLib');
%addpath('/work/lsqdim/library/LSQDIM/');

% load HighOrderDB
% global HOLib_DB

classdef LSQdisc_5 < handle
    
    properties (Access = private, Constant)
        ROUNDOFF_LIMIT = 1e-14;
    end
    
    properties (Access = private)        
        Pt; Qt;
        tq;
        Dt;
        wt;
        Ht;
        Bt0; Bt1;
        Dtt;
        Hxt;    Hyt;
        Ztm, Ztp;
        
        W1, H1, D1, DD1;
    end
    
    properties
        SDIM;
        DIM;
        spaceTime;
        
        P; Q; C;
        basis;
        
        dofe; %= zeros(1,4);
        dofeq; % = zeros(1,4);
        dofev; dofevq;
        
        numSpatialDofs;
        numSpatialPoints;
                
        refinementRatio;
        
        %qp, qw;
        
        %xq; yq; zq;
        %wx; wy; wz;
                        
        %Dx; Dy; Dz;
        W, H , D , DD;
        Wspatial;
        
        boundary;
        
        connectivity;
                
        Z;
        
        matZ;
        vecZ;
        
        dofPattern;
        dofType;
        dofTypeID;
        
    end %properties
    
    
    methods
        % Constructor
        function D = LSQdisc_5(P,Q,C,SDIM,spaceTime)
            
            % Save input values as properties of this object
            D.SDIM = SDIM;
            D.spaceTime = spaceTime;
            
            % Determine the correct size for arrays
            D.DIM = SDIM + spaceTime;
            
            % Setup P, Q and C
            D.P = D.verifyInput(P,'P');
            D.Q = D.verifyInput(Q,'Q');
            D.C = D.verifyInput(C,'C');
            
            % Assign the refinementRatios
            D.refinementRatio = ones(1,4);
            for i=1:D.DIM
                D.refinementRatio(i) = System.settings.mesh.refinementRatio(i);
            end
            
            % Use the provided input to create the basis functions
            D.setupBasisFunctions();
            
            D.dofe  = ones(1,4);
            D.dofeq = ones(1,4);
            
            for i=1:D.DIM
                D.dofe(i)   = D.basis{i}.dof;
                D.dofeq(i)  = D.basis{i}.dofq;
            end
                        
            % Compute the total number of dofs / quadrature points
            D.dofev  = prod(D.dofe );     
            D.dofevq = prod(D.dofeq);  
            
            % Compute the spatial number of dofs / quadrature points
            if (D.spaceTime)
                D.numSpatialDofs    = D.dofev  / D.basis{end}.dof;          % # dof in 1 element for 1 variable
                D.numSpatialPoints  = D.dofevq / D.basis{end}.dofq;         % # solution points in 1 element for 1 variable
            else
                D.numSpatialDofs    = D.dofev ;
                D.numSpatialPoints  = D.dofevq;
            end
            
            % Setup the zero matrix and vector
            D.matZ = sparse( D.dofevq , D.dofev );
            D.vecZ = sparse( D.dofevq , 1 ) ;
            
            D.setConnectivity;
            
%             D.setDoFPatterns;
            D.setDoFType;
            D.setDoFTypeID;
        end
        
        function out = verifyInput(s,variable,name)
            if (numel(variable)~=s.DIM)
                if (numel(variable)==1)
                    out = variable*ones(1,s.DIM);
                elseif numel(variable)>s.DIM
                    out = variable(1:s.DIM);
                else                    
                    if s.DIM==1
                        error('Please provide just 1 value for %s',name)
                    else
                        error('Please provide just 1 or %d values for %s',s.DIM,name)
                    end
                end
            else
                out = variable;
            end
        end
        
        function setupBasisFunctions(D)

%             % In each direction the standard element goes from -1 to +1, so h=2
%             % This implies the Jacobian in each direction is equal to 1
%             h = 2 * ones(D.dim,1);
%             J = 2./h;
%                         
%             % Compute the quadrature points and weights
%             D.qp = cell(D.dim,1);
%             D.qw = cell(D.dim,1);
%             
%             for i=1:D.dim
%                 [D.qp{i},D.qw{i}] = GLL(D.Q(i),-1,1);
%             end
            
%             % Setup the H, D, and DD arrays, which depend on the selected
%             % continuity level. The values in the arrays are computed for
%             % the quadrature points
%             D.H1  = cell(D.dim,1);
%             D.D1  = cell(D.dim,1);
%             D.DD1 = cell(D.dim,1);
            
            D.basis = cell(D.DIM,1);
            
            for i=1:D.DIM                
                switch D.C(i)
                    case 0
                        % Lagrange C0
                        D.basis{i} = BasisFunction(D.P(i),D.Q(i),D.C(i),FunctionType.Lagrange);
                        
                    case 1
                        % Hermite C1 
                        D.basis{i} = Hermite1(D.P(i),D.Q(i),D.C(i));
                        
%                         if (D.dofe(i)>3)
%                             ind = [1 2 5:1:D.dofe(i) 3 4];
% 
%                             D.H1{i}  = HermiteN(  ind,D.qp{i},J(i));
%                             D.D1{i}  = DHermiteN( ind,D.qp{i},J(i));
%                             D.DD1{i} = DDHermiteN(ind,D.qp{i},J(i));
%                         else
%                             error('P should be larger than or equal to 3 for C1.  Increase the polynomial order, or switch to C0')
%                         end
                    otherwise 
                        if (D.P(i)<2*D.C(i))
                            error('Please ensure P is at least 2*C')
                        end
                        
                        if (D.Q(i)<D.P(i))
                            error('Please ensure Q is at least P')
                        end
                        
                        D.basis{i} = BSpline(D.P(i),D.Q(i),D.C(i));

                end                        
            end
            
            
                        
            %% Multidimensional operators
            % Weights
            D.W1    = cellfun( @(x) x.qw, D.basis, 'UniformOutput', false );
            D.W     = superkron(D.W1);
            if D.spaceTime
                D.Wspatial = superkron(D.W1{1:end-1});
            else
                D.Wspatial = D.W;
            end
            
            % H array (values)
            D.H1    = cellfun( @(x) x.H, D.basis, 'UniformOutput', false );
            D.H     = D.getTransform();
            
            % D array (first derivative)
            D.D1    = cellfun( @(x) x.D, D.basis, 'UniformOutput', false );
            D.D     = cell(D.DIM,1);
            for i=1:D.DIM
                D.D{i} = D.getTransform(i,D.D1{i});
            end
            
            % DD array (second derivative)
            % NB: no 2nd derivative in time required, so only spatial directions
            D.DD1   = cellfun( @(x) x.DD, D.basis, 'UniformOutput', false );
            D.DD = cell(D.SDIM,1);          
            for i=1:D.SDIM
                D.DD{i} = D.getTransform(i,D.DD1{i});
            end
            
            %% Boundary elements
            D.boundary = cell(D.DIM,2);     % for each direction, low/high boundary
            
            % low & high boundaries k
            for k=1:2
                if k==1
                    kPos = 1;
                    n    = 1;
                elseif (k==2)
                    kPos = size(D.W1{i},1);
                    n    = -1;
                end
                
                for i=1:D.DIM
                    D.boundary{i,k}     = struct;

                    boundaryW           = D.W1;
                    boundaryW{i}        = ones(size(D.W1{i})); 
                    D.boundary{i,k}.W   = superkron(boundaryW);

                    D.boundary{i,k}.H   = D.getTransform(i,D.H1{i}( kPos, : ));

                    D.boundary{i,k}.D   = cell(D.DIM,1);
                    for j=1:D.DIM
                        if (i~=j)
                            D.boundary{i,k}.D{j}  = D.getTransform(i,D.H1{i}( kPos, : ),j,D.D1{j});
                        elseif (i==j)
                            D.boundary{i,k}.D{i}  = D.getTransform(i,D.D1{i}( kPos, : ));
                        end
                    end

                    D.boundary{i,k}.normal  = i;
                    D.boundary{i,k}.Dn      = D.boundary{i,k}.D{i};

                    D.boundary{i,k}.Hpos    = D.getPositions(i,D.H1{i}( kPos, : ));
                    D.boundary{i,k}.Dpos    = D.getPositions(i,D.D1{i}( kPos, : ));
                end
            end
            
            %-- depends on lv
            
            %% SINGLE LEVEL: refinement

            for i=1:D.DIM
                D.basis{i}.initRefine( D.refinementRatio(i) );
            end
            
            %% Compute the general Z for refinement
            refRatio    = D.refinementRatio;
            
            zBase       = cellfun( @(x) x.Z, D.basis, 'UniformOutput',false );
            while numel(zBase)<4
                zBase{numel(zBase)+1} = {1};
            end
            
            D.Z         = cell( refRatio );
            
            ranges      = arrayfun( @(x) linspace(1,x,x), refRatio, 'UniformOutput', false );
            
            idxComb     = combvec( ranges{:} );
            
            for n=1:size(idxComb,2)
                
                idx = idxComb(:,n);
                
                tmpBase = cell(4,1);
                for i=1:4
                    tmpBase{i} = zBase{i}{idx(i)};
                end
                
                idx = num2cell( idx );
                D.Z{ idx{:} } = superkron( tmpBase );
            end
            
            % 
%             H_amr = cell(D.dim,ratio);
%             
%             for j=1:ratio
%                 for i=1:D.dim
%                     H_amr{i,j} = 0;
%                 end
%             end
% 
%             % x-direction
%             Hxim = HermiteN(indx,pmx,Jx);
%             Hxip = HermiteN(indx,ppx,Jx);
%             
%             % y-direction
%             Hyim = HermiteN(indy,pmy,Jy) ;
%             Hyip = HermiteN(indy,ppy,Jy) ;
%             
%             % z-direction
%             if (D.C(3)==1)
%                 Hzim = HermiteN(indz,pmz,Jz) ;
%                 Hzip = HermiteN(indz,pmz,Jz) ;
%             else
%                 for j=1:D.dofe(3)
%                     Hzim(:,j) = LagrangeGLL(indz(j),pmz,D.dofe(3));
%                     Hzip(:,j) = LagrangeGLL(indz(j),ppz,D.dofe(3));
%                 end
%             end
%             
%             % t-direction
%             if (D.C(4)==1)
%                 for j=1:D.dofe(4)
%                     Htim(:,j)=HermiteN(indt(j),pmt,Jt) ;
%                     Htip(:,j)=HermiteN(indt(j),ppt,Jt) ;
%                 end
%             else
%                 for j=1:D.dofe(4)
%                     Htim(:,j) = LagrangeGLL(indt(j),pmt,D.dofe(4));
%                     Htip(:,j) = LagrangeGLL(indt(j),ppt,D.dofe(4));
%                 end
%             end
%             
%             D.Zxm=Hx1\Hxim;     D.Zxm(abs(D.Zxm)<D.ROUNDOFF_LIMIT)=0;
%             D.Zxp=Hx1\Hxip;     D.Zxp(abs(D.Zxp)<D.ROUNDOFF_LIMIT)=0;
%             
%             D.Zym=Hy1\Hyim;     D.Zym(abs(D.Zym)<D.ROUNDOFF_LIMIT)=0;
%             D.Zyp=Hy1\Hyip;     D.Zyp(abs(D.Zyp)<D.ROUNDOFF_LIMIT)=0;
%             
%             D.Zzm=Hz1\Hzim;     D.Zzm(abs(D.Zzm)<D.ROUNDOFF_LIMIT)=0;
%             D.Zzp=Hz1\Hzip;     D.Zzp(abs(D.Zzp)<D.ROUNDOFF_LIMIT)=0;
%             
%             D.Ztm=Ht1\Htim;     D.Ztm(abs(D.Ztm)<D.ROUNDOFF_LIMIT)=0;
%             D.Ztp=Ht1\Htip;     D.Ztp(abs(D.Ztp)<D.ROUNDOFF_LIMIT)=0;
            


%             sizeZ = cellfun(@(x) x.refinementRatio, D.basis);
%             D.Z = cell(sizeZ');
            
%             for i=1:D.dim
%                 for j=1:D.basis{i}.refinementRatio
%                     D.Z{}
%                 end
%             end

        end     % end setupHermiteBasis_3D_refine
        
        function setConnectivity(s)            
            % Determine the number of sub elements inside a spectral element
            subElements = s.dofeq-1;
            subElements( subElements==0 ) = 1;
            numSubElements = prod( subElements );
            
            s.connectivity = zeros( numSubElements, 2^s.DIM );
            
            nodeID = 1:s.dofevq;
            nodeID = reshape(nodeID,s.dofeq);
            
            switch s.DIM
                case 1
                    count = 0;
                    for i=1:subElements(1)

                        count = count+1;

                        tmpNodes = nodeID(i:i+1);
                        %tmpNodes([3 4]) = tmpNodes([4 3]);

                        s.connectivity( count, : ) = tmpNodes(:);
                    end
                    %error('Please add connectivity for 1D')
                    
                case 2                    
                    count = 0;
                    for j=1:subElements(2)
                        for i=1:subElements(1)

                            count = count+1;

                            tmpNodes = nodeID(i:i+1,j:j+1);
                            tmpNodes([3 4]) = tmpNodes([4 3]);

                            s.connectivity( count, : ) = tmpNodes(:);
                        end
                    end
                    
                case 3
                    count = 0;
                    for k=1:subElements(3)
                        for j=1:subElements(2)
                            for i=1:subElements(1)

                                count = count+1;
                                
                                tmpNodes = nodeID(i:i+1,j:j+1,k:k+1);
                                tmpNodes([3 4]) = tmpNodes([4 3]);
                                tmpNodes([7 8]) = tmpNodes([8 7]);

                                s.connectivity( count, : ) = tmpNodes(:);
                            end
                        end
                    end
            end

            s.connectivity = (s.connectivity-1)';
        end
%         
%         function setDoFPatterns(s)
%             s.dofPattern = cell(1,4);
%             
%             % vertices
%             s.dofPattern{1} = cell( repmat(2,1,s.DIM) );
%             
%             pat = cell(1,s.DIM);
%             
%             vec = cell(1,s.DIM);
%             for i=1:s.DIM
%                 vec{i} = 1:2;
%             end
%             pos = combvec(vec{:});
%             
%             for i=1:numel(s.dofPattern{1})                
%                 position = pos(:,i);
%           
%                 for v=1:numel(position)
%                     pat{v} = s.basis{v}.dofPattern{position(v)};
%                 end
%                 s.dofPattern{1}{i} = boolean( superkron( pat ) );
%             end
%             
%             % edges
%             s.dofPattern{2} = cell( repmat(2,1,s.DIM) );
%             numBoundDof = cellfun( @(x) x.numBoundDof, s.basis, 'UniformOutput', false);
%             edgeDof    ;
%         end
        
        %
        % Function to determine to what type of geometric object 
        % each DoF belongs:
        %
        %  -1: unassigned
        %   0: vertex
        %   1: edge
        %   2: face
        %   3: hex
        %
        function setDoFType(s)
            % Initialize to unassigned
            s.dofType = -1 * ones(s.dofe);
            
            %% vertices
            vertices = cellfun(@(x) x.dofType==1, s.basis, 'UniformOutput', false);
            s.dofType( boolean( superkron(vertices) ) ) = 0;
                    
            %% edges
            switch s.DIM
                case 1
                    flags = [0 1];
                case 2
                    flags = unique( perms([0 1]), 'rows' );
                case 3
                    flags = unique( perms([0 1 1]), 'rows' );
                case 4
                    flags = unique( perms([0 1 1 1]), 'rows' );     
            end
            for i=1:size(flags,2)
                edges = cellfun(@(x,c) x.dofType==c, s.basis, num2cell( flags(:,i) ), 'UniformOutput', false);
                s.dofType( boolean( superkron(edges) ) ) = 1;
            end
            
            %% faces
            if (s.DIM>1)
                switch s.DIM
                    case 2
                        flags = unique( perms([0 1]), 'rows' );
                    case 3
                        flags = unique( perms([0 0 1]), 'rows' );
                    case 4
                        flags = unique( perms([0 1 1 1]), 'rows' );     
                end
                flags = unique( perms([0 0 1]), 'rows' );
                for i=1:size(flags,2)
                    faces = cellfun(@(x,c) x.dofType==c, s.basis, num2cell( flags(:,i) ), 'UniformOutput', false);
                    s.dofType( boolean( superkron(faces) ) ) = 2;
                end
            end
            
            %% hex
            hex = cellfun(@(x) x.dofType==0, s.basis, 'UniformOutput', false);
            s.dofType( boolean( superkron(hex) ) ) = 3;
        end
        
        %
        % Function that gives an incremental ID to each DoFType
        function setDoFTypeID(s)
                        
            s.dofTypeID = -1 * ones(s.dofe);
            
            % The following cellfun computes unique IDs for each element in 
            % the entire span of basis functions. Next, these temporary IDs 
            % are substituted by incremental IDs (natural order)
            id = cellfun(@(x,c) x.dofID.^(2^c), s.basis, num2cell( [0:numel(s.basis)-1]' ), 'UniformOutput', false);
            id = superkron(id);
            
            %% vertices
            for d=1:4
                subset  = id( s.dofType==(d-1) );
                vIDs    = unique( subset );

                for i=1:numel(vIDs)
                    s.dofTypeID( id==vIDs(i) ) = i;
                end
            end
        end
        
%         function out = getDoFPattern(s,dimensionality)
%             out = s.dofPattern{ dimensionality };
%         end
        
        function out = getTransform(s,direction,array,direction2,array2)
            temp = s.H1;
            if nargin>1
                temp{direction} = array; 
            end
            if nargin>3
                temp{direction2} = array2;
            end
            out = superkron( temp );       
        end
        
        function out = getPositions(s,direction,array,direction2,array2)
            temp = cell(numel(s.dofe),1);
            for i=1:numel(s.dofe)
                temp{i} = eye(s.dofe(i));
            end
            if nargin>1
                temp{direction} = array; 
            end
            if nargin>3
                temp{direction2} = array2;
            end
            [~,out] = find( superkron( temp ) );
        end
        
        function out = getH(s)
            out = s.H;
        end
        
        function out = dof(s,direction)
            if nargin==1
                out = s.dofev;
            else
                out = s.dofe(direction);
            end
        end
        
        function out = dofSize(s)
            out = s.dofe;
        end
        
        function out = xDof(s)
            out = s.dofe(1);
        end
        
        function out = yDof(s)
            if (s.DIM>1)
                out = s.dofe(2);
            else
                out = 1;
            end
        end
        
        function out = zDof(s)
            if (s.DIM>2)
                out = s.dofe(3);
            else
                out = 1;
            end
        end
        
        function out = tDof(s)
            if (s.DIM>3)
                out = s.dofe(4);
            else
                out = 1;
            end
        end
        
        function Z = getZrefine(s)
%             xDof = s.dofe(1);
%             yDof = s.dofe(2);
%             zDof = s.dofe(3);
%             tDof = s.dofe(4);
% 
%             
%             % scaling
%             xs = sparse( eye(s.xDof));
%             ys = sparse( eye(s.yDof));
%             zs = sparse( eye(s.zDof));
%                         
%             zBase = sparse( eye(zDof));
%             Zmm = superkron(zBase,s.Zym*ys,s.Zxm*xs);
%             Zpm = superkron(zBase,s.Zym*ys,s.Zxp*xs);
%             Zmp = superkron(zBase,s.Zyp*ys,s.Zxm*xs);
%             Zpp = superkron(zBase,s.Zyp*ys,s.Zxp*xs);
            
            Z = cell(s.refinementRatio);
            
            for t=1:s.refinementRatio(4)
                if (s.DIM<4)
                    tBase = speye( s.dofe(4) );
                else
                    tBase = s.basis{4}.Z{t};
                end
                
                for z=1:s.refinementRatio(3)
                    if (s.DIM<3)
                        zBase = speye( s.dofe(3) );
                    else
                        zBase = s.basis{3}.Z{z};
                    end
                    
                    for y=1:s.refinementRatio(2)
                        if (s.DIM<2)
                            yBase = speye( s.dofe(2) );
                        else
                            yBase = s.basis{2}.Z{y};
                        end
                
                        for x=1:s.refinementRatio(1)
                            xBase = s.basis{1}.Z{x};
                                                        
                            Z{x,y,z,t} = superkron(tBase,zBase,yBase,xBase);
                        end
                    end
                end
            end
            
        end
        
        function plot(s)
            
        end

    	function standardElement = save(s)
            standardElement       	  = struct;
            standardElement.class     = class(s);
            standardElement.P         = s.P;
            standardElement.Q         = s.Q;
            standardElement.C         = s.C;
            standardElement.SDIM      = s.SDIM;
            standardElement.spaceTime = s.spaceTime;
        end
        
    end %methods
    
    methods (Static)
        function s = init()
            settings = System.settings.stde;
            s = LSQdisc_5(settings.Pn,settings.Q,settings.C,settings.sDim,System.settings.time.spaceTime);
        end
        
        function out = load(standardElement,Q)
            P = standardElement.P;
            if (nargin~=2 || isempty(Q) || Q==0)
                Q = standardElement.Q;
            end
            C = standardElement.C;
            SDIM = standardElement.SDIM;
            spaceTime = standardElement.spaceTime;
            
            out = LSQdisc_5(P,Q,C,SDIM,spaceTime);
        end
    end

end %classdef
