classdef StaticElement < handle
        
    properties (Constant, Access=private)
        isRefined = false;
        lvl       = 0;
        
    end
    
    properties (SetAccess=public,GetAccess=public)
        active;
    end
    
    properties
        rank = 0;                       % processor number where this element is located
        
        id;
        localID;
        
        neighbor;
        
        position;                   % position of this child wrt the parent
        
        feType = 1;
        
        material = Material.Fluid;      % standard material of an element is Fluid (used to create pillars, which are Solid)
    end
    
    properties (GetAccess = public, SetAccess=protected)        
        geometric;
        finite;
    end
    
    methods
    
        function s = StaticElement(id,gElement) %,position,feType) %id,level,neighbors,xelem,yelem,zelem,i,j,k,standardElement)
            if nargin==0
                s.neighbor = [];

            elseif nargin>1
                s.id            = id;                
                s.geometric     = gElement;
                
%                 if ( ~iscell(position) || ~all(size(position)==[1,4]) ) 
%                     error('The position parameter must be a 1x4 cell array')
%                 end
                
%                 s.position      = position;
%                 s.feType        = feType;

                % Initialize the neighbor cell array
                s.neighbor      = cell(2,4);
                
                s.finite        = FECollection.get( s.feType );
            end
        end
        
        function setFinite(s,finiteElement)
            if (nargin==1)
                s.finite = FECollection.get( s.feType );
            else
                s.finite = finiteElement;
            end
        end
        
        function out = nodes(s)            
            % Get the Finite Element of this element
            finiteElement = s.finite; %FECollection.get( s.feType );
            
            if ( s.geometric.isSimple )
                if (~isempty(finiteElement))
                    nodes = finiteElement.nodes;
                else
                    nodes = cell(s.geometric.dimension,1);
                    for i=1:s.geometric.dimension 
                        nodes{i} = [-1 ; 1];
                    end
                end
                
                vertices = s.geometric.element(0);
                offset   = vertices(1).X;
                
                for i=1:s.geometric.dimension                
                    nodes{i} = offset(i) + s.geometric.J(i,i) * System.settings.phys.L0 * (nodes{i}+1);
                end

                out = nodes;
            else
                warning('using non-simple hex!')
                xi = finiteElement.logicalCoordinates;
                out = arrayfun( @(n) s.geometric.mapping( xi(:,n) )', 1:size(xi,2), 'UniformOutput', false );
                %out = cell2mat( tmp );
                out = [out{:}];
            end
        end
        
        function s = setNeighbor(s,loc,neighborElement)
            s.neighbor{loc} = neighborElement;
        end
        
        function neighborList = getNeighbors(s)
            if (s.isRefined)
                neighborList = [];
                for i=1:numel(s.ch)
                    neighborList = [neighborList; s.ch{i}.getNeighbors()];
                end
            else
                neighborList = s.getNeighborID;
            end
        end
        
        function neighborList = getNeighborID(s,dir,pos)
            if (nargin==2)
                neighborList = zeros(1,6);
                for n=1:6
                    if isempty(s.neighbor{n})
                        neighborList(1,n) = -1;
                    else
                        neighborList(1,n) = s.neighbor{n}.id;
                    end
                end
                if (nargin>1)
                    neighborList = neighborList(dir);
                end
            else
                neighborList = zeros(1,6);
            end
        end
        
        % In case an element has been deleted the pointer does not exist 
        % anymore (~isvalid). Therefore a new pointer to the neighbor of 
        % the parent is created. This is a resursive function
        function out = getValidSide(s,side)
            if (isvalid(s.parent{1}.neighbor{side}))
                out = s.parent{1}.neighbor{side};
            else
                out = s.parent{1}.getValidSide(side);
            end
        end
        
        % Function to check the neihbors, if all is ok it will return 'true'
        function status = checkNeighbors(s)
            status = true;
%             for side=1:6

            for dir=1:s.disc.DIM
                for pos=0:1
                    
                    activeNeighbor = s.neighbor{dir,pos+1};
                    
                    if ~isempty(activeNeighbor)

                        oppositeNeighbor = activeNeighbor.neighbor{dir,~pos+1};
                        
                        % Check that the neighbors neighbor is the current oct
                        if ( s.lvl == activeNeighbor.lvl )

                            % the current has the same lvl as the neighbor
                            if ~(s==oppositeNeighbor)
                                status = false;
                                fprintf('Element %d is not the neighbor of element %d\n',s.localID,activeNeighbor.localID)
                            end

                        elseif (s.lvl-activeNeighbor.lvl)==1
                            % the current is more fine than the neighbor
                            if ~( s.parent{1} == oppositeNeighbor )
                                status = false;
                                fprintf('Element %d is not the neighbor of element %d\n',s.localID,activeNeighbor.localID)
                            end

                        elseif (s.neighbor{side}.isRefined)
                            % the current is more coarse than the neighbor

                            for c=Neighbor.children(side)
                                if ~(s==s.neighbor{side}.ch{c}.neighbor{ Neighbor.opposite(side) })
                                    status = false;
                                    fprintf('Element %d is not the neighbor of element %d\n',s.localID,activeNeighbor.localID)
                                end
                            end
                        else
                            fprintf('Error with element %d\n',s.localID)
                        end
                    end
                end
            end
        end
        
        function updateNeighbors(s)
            for dir=1:s.disc.DIM
                for pos=0:1
                    % check if the neighbor exists
                    if ~isempty(s.neighbor{dir,pos+1})

                        if (~isvalid(s.neighbor{dir,pos+1}))
                            s.neighbor{dir,pos+1} = s.getValidSide(dir,pos);
                        end

                        if ( (s.lvl == s.neighbor{dir,pos+1}.lvl) || (~s.neighbor{dir,pos+1}.isRefined && s.lvl>s.neighbor{dir,pos+1}.lvl) )
                            % do nothing, neighbor is already in correct range
                            % (equal level or one level coarser (if the neighbor 
                            % is not refined) than current element)
                        else
                            s.correctNeighbor2(dir,pos);
                        end
                    end
                    
                    % fix the connectivity of the parent too
                    if ~isempty(s.parent)
                        if ~isempty(s.parent{1}.neighbor{dir,pos+1})

                            % In case an element has been deleted the pointer
                            % does not exist anymore (~isvalid). Therefore a
                            % new pointer to the neighbor of the parent is
                            % created.                        
                            if ( ~isvalid( s.parent{1}.neighbor{dir,pos+1} ) )
                                s.parent{1}.neighbor{dir,pos+1} = s.parent{1}.getValidSide(dir,pos);
                            end

                            if ( (s.parent{1}.lvl == s.parent{1}.neighbor{dir,pos+1}.lvl) || (~s.parent{1}.neighbor{dir,pos+1}.isRefined) )
                                % do nothing, neighbor is already in correct range
                                % (equal level or one level coarser (if the neighbor 
                                % is not refined) than current element)
                            else
                                s.parent{1}.correctNeighbor2(dir,pos);
                            end
                        end
                    end
                end
            end
        end
        
        % Recursive function to get the correct neighbor
        function correctNeighbor2(s,dir,pos)
            if (s.lvl - s.neighbor{dir,pos+1}.lvl < 0)
                % the neighbor is more refined than this element itself, so
                % just adjust the neighbor to be the parent of the current
                % neighbor
                 s.neighbor{dir,pos+1} = s.neighbor{dir,pos+1}.getParent;
                 
                 
            else
                if (s.lvl - s.neighbor{dir,pos+1}.lvl == 1)
                    neighborPosition = s.position;
                else
                    % the current element is 2 or more levels finer than 
                    curParent = s.getParent;
                    while (curParent.lvl - s.neighbor{dir,pos+1}.lvl ~= 1)
                        curParent = curParent.getParent;
                    end
                    neighborPosition = curParent.getPosition;
                end
                
%                 switch side
%                     case {Neighbor.Right, Neighbor.Left}
%                         neighborPosition(1) = 3-neighborPosition(1);
%                     case {Neighbor.Bottom, Neighbor.Top}
%                         neighborPosition(2) = 3-neighborPosition(2);
%                     case {Neighbor.Front, Neighbor.Back}
%                         neighborPosition(3) = 3-neighborPosition(3);
%                 end
                
                if (neighborPosition{dir}>1)
                    neighborPosition{dir} = 1;
                else
                    neighborPosition{dir} = s.refineFactor(dir);
                end
                
                s.neighbor{dir,pos+1} = s.neighbor{dir,pos+1}.ch{ neighborPosition{:} };
            end
            
            s.updateNeighbors;
        end
        
        function out = getElements(s,direction,maxLevel)
            if (s.isRefined)
                out = []; 
                
                try 

                    if (nargin>1 && ~isempty(direction))
                        order = reshape(1:numel(s.ch),size(s.ch));

                        if (~direction(1))
                            order = flipud(order);
                        end
                        if (~direction(2))
                            order = fliplr(order);
                        end

                        for i=order(:)'
                            out = [out; s.ch{i}.getElements(direction)];
                        end
                    else
                        
                        % Default behavior: 
                        
                        order = reshape(1:numel(s.ch),size(s.ch));
                        for i=order(:)'
                            out = [out; s.ch{i}.getElements()];
                        end
                        
                    end
                
                catch 
                   disp('Problem in oct.getElements') 
                end
                
%                 out(s.getNumElements) = oct;
%                 count = 0;
%                 for i=1:numel(s.ch)
%                     start = count+1;
%                     final = count+s.ch{i}.getNumElements;
%                     out(start:final) = s.ch{i}.getElements;
%                 end
            else
                out = s;
            end
        end
        
%         function
        function out = getLv(s)
            if (numel(s)>1)
                out = s.getAllLv();
            else
                out = s.getElementLv();
            end
        end
        
        function out = getAllLv(s)
            out = zeros(s.getNumElements,1);
%             dim = s.getDim();
            
            position = 0;
            
            for k=1:size(s,3)
                for j=1:size(s,2)
                    for i=1:size(s,1)
                        numChild = s(i,j,k).getNumChild;
                        out(position+1:position+numChild) = s(i,j,k).getElementLv;
                        position = position + numChild;
                    end
                end                
            end
        end      

        function lvl = getElementLv(s,num)
            if (s.isRefined)
                lvl = [];
                for i=1:numel(s.ch)
                    lvl = [lvl s.ch{i}.getElementLv()];
                end
            else
                lvl = s.lvl;
            end
            
            if nargin==2
                if length(lvl)<num
                    fprintf('Error in OCT: the requested child does not exist')
                    lvl = 0;
                else
                    lvl = lvl(num);
                end
            end
        end
        
        % This function checks 
        function checkLevel(s)
            if (s.isRefined)
                disp('Should not be here : oct.checkLevel')

            else
                for dir=1:s.disc.DIM
                    for pos=0:1
                        
                        activeNeighbor = s.neighbor{dir,pos+1};
                        
                        % First check if there is a neighbor at all
                        if ( ~isempty(activeNeighbor) )
                            
                            % If the current element's level is more than 1
                            % higher than the neighbor's level (the current 
                            % element is 2 levels finer than the neighbor) 
                            % ensure the neighbor is not allowed to coarsen 
                            % itself, and should refine itself
                            if ( (s.getLv - activeNeighbor.getLv) > 1 )
                                activeNeighbor.wantsRefinement = true;
                                activeNeighbor.wantsCoarsening = false;
                                
                            % If the levels are 1 apart, flag that the
                            % neighbor does not want to be removed. Next,
                            % do a check for diagonal elements
                            elseif ( (s.getLv - activeNeighbor.getLv) == 1 )     % diagonal level check (for example: a level 0 element shares a corner DOF with a level 2 element)
                                if (activeNeighbor.wantsRemoval)
                                    activeNeighbor.wantsRemoval = false;
                                end

                                for dir2=1:s.disc.DIM
                                    for pos2=0:1
                                        
                                        % Ensures only the Lvl of diagonally neighbors is checked
                                        if (dir~=dir2)
                                            
                                            diagonalNeighbor = activeNeighbor.neighbor{dir2,pos2+1};
                                            
                                            if ( ~isempty(diagonalNeighbor) )
                                                if ( (s.getLv - diagonalNeighbor.getLv) > 1 )
                                                    diagonalNeighbor.wantsRefinement = true;
                                                    diagonalNeighbor.wantsCoarsening = false;
                                                elseif ( ~diagonalNeighbor.isRefined && (s.getLv - diagonalNeighbor.getLv) == 1 && diagonalNeighbor.wantsRemoval)
                                                    diagonalNeighbor.wantsRemoval = false;
                                                elseif (diagonalNeighbor.wantsRemoval)
                                                    disp('')
                                                end
                                            end
                                        end
                                    end
                                end
%                                 
%                                 for j=1:6
%                                     if (i~=j && ~isempty(activeNeighbor.neighbor{j}))        % i~=j ensures only the Lvl of diagonally neighbors is checked (and it's own, but that's ok)
%                                         if ( (s.getLv - s.neighbor{i}.neighbor{j}.getLv) > 1 )
%                                             s.neighbor{i}.neighbor{j}.wantsRefinement = true;
%                                             s.neighbor{i}.neighbor{j}.wantsCoarsening = false;
%                                         elseif ( ~s.neighbor{i}.neighbor{j}.isRefined && (s.getLv - s.neighbor{i}.neighbor{j}.getLv) == 1 && s.neighbor{i}.neighbor{j}.wantsRemoval)
%                                             s.neighbor{i}.neighbor{j}.wantsRemoval = false;
%                                         elseif (s.neighbor{i}.neighbor{j}.wantsRemoval)
%                                             disp('')
%                                         end
%                                     end
%                                 end
                            end
                        end
                    end
                end
                
%                 for i=1:6
%                     if (~isempty(s.neighbor{i}))
%                         if ( (s.getLv - s.neighbor{i}.getLv) > 1 )
% %                             s.refine(lastID);
%                             s.neighbor{i}.wantsRefinement = true;
%                             s.neighbor{i}.wantsCoarsening = false;
%                         elseif ( (s.getLv - s.neighbor{i}.getLv) == 1 )     % diagonal level check (for example: a level 0 element shares a corner DOF with a level 2 element)
%                             if (s.neighbor{i}.wantsRemoval)
%                                 s.neighbor{i}.wantsRemoval = false;
%                             end
%                             
%                             for j=1:6
%                                 if (i~=j && ~isempty(s.neighbor{i}.neighbor{j}))        % i~=j ensures only the Lvl of diagonally neighbors is checked (and it's own, but that's ok)
%                                     if ( (s.getLv - s.neighbor{i}.neighbor{j}.getLv) > 1 )
%                                         s.neighbor{i}.neighbor{j}.wantsRefinement = true;
%                                         s.neighbor{i}.neighbor{j}.wantsCoarsening = false;
%                                     elseif ( ~s.neighbor{i}.neighbor{j}.isRefined && (s.getLv - s.neighbor{i}.neighbor{j}.getLv) == 1 && s.neighbor{i}.neighbor{j}.wantsRemoval)
%                                         s.neighbor{i}.neighbor{j}.wantsRemoval = false;
%                                     elseif (s.neighbor{i}.neighbor{j}.wantsRemoval)
%                                         disp('')
%                                     end
%                                 end
%                             end
%                         end
%                     end
%                 end
                
            end
        end
        
        %% Number of elements
       	function out = getNumElements(s)
            if (numel(s)>1)
                out = s.getTotalNumElements();
            else
                out = s.getNumChild();
            end
        end
        
        % Function to determine the total number of active elements (all
        % children included). The parents of children are not included!
        function numElements = getTotalNumElements(s)
            numElements = 0;
%             dim = s.getDim();
            for k=1:size(s,3)
                for j=1:size(s,2)
                    for i=1:size(s,1)
                        numElements = numElements + s(i,j,k).getNumChild;
                    end
                end
            end
        end
           
        %% Get ID
       	function out = getID(s)
            if (numel(s)>1)
                out = s.getAllID();
            else
                out = s.getElementID();
            end
        end
        
        function ids = getElementID(s)
            if (s.isRefined)
                ids = [];
                for i=1:numel(s.ch)
                    ids = [ids s.ch{i}.getElementID()];
                end
            else
                ids = s.id;
            end
        end
        
        function out = getAllID(s)
            out = zeros(s.getNumElements,1);
            position = 0;
            
            for k=1:size(s,3)
                for j=1:size(s,2)
                    for i=1:size(s,1)
                        numChild = s(i,j,k).getNumChild;
                        out(position+1:position+numChild) = s(i,j,k).getID;
                        position = position + numChild;
                    end
                end
            end
        end
        
        function [ dofs, lastDoF ] = dof(s,lastDoF,feType,includePeriodicity,activeOnly)
            
            if (activeOnly)
                if (~s.geometric.active)
                    dofs = [];
                    return
                end
            end
            
            if (nargin>2 && ~isempty(feType))
                s.feType = feType;
            end
            
            % By default periodic boundaries are take into account
            if nargin<4
                includePeriodicity = true;
            end
            
            patternSize = zeros(1,s.dimension);
            
            % Get the finite element from the collection
            finiteElement = FECollection.get( s.feType );%s.mesh.feType(e) );
                
%             if (~s.geometric.active)
%                 error('LSQDIM:StaticElement :: The main geometricElement is not active')
%             end
            
            for d=0:s.dimension
                
                % Get the dof flags for the elements with dimension d
                dofPattern      = (finiteElement.dofType==d);
                ePattern        = (finiteElement.dofTypeID .* dofPattern );
                
                % Determine the periodic pattern
                if (numel(includePeriodicity)>1)
                    
                    % Check that the includePeriodicity has a size of at least
                    % dofPattern. Throw an error if not, use the minimum size
                    % otherwise
                    numDim = numel( size(dofPattern) );                                                             % number of dimensions according to dofPattern
                    if ( numDim > size(includePeriodicity,2) )
                        error('Please use a field periodicity with at least %d spatial dimensions\n',numDim)     	% s.phys.system{n}.fdata{1}.periodic
                    end
                    maxDim = min( numDim, size(includePeriodicity,2) );
                    
                    numc = size(includePeriodicity,1);
                    
                    periodicPattern = false( size(dofPattern) );
                    for p=1:maxDim                                          % spatial directions
                        for c=1:size(includePeriodicity,1)                  % continuity (value, derivative etc)
                            if includePeriodicity(c,p)
                                switch p
                                    case 1
                                        periodicPattern(   c   ,:,:) = true;
                                        periodicPattern(end-numc+c,:,:) = true;
                                    case 2
                                        periodicPattern(:,   c   ,:) = true;
                                        periodicPattern(:,end-numc+c,:) = true;
                                    case 3
                                        periodicPattern(:,:,   c   ) = true;
                                        periodicPattern(:,:,end-numc+c) = true;
                                end
                            end
                        end
                    end
                end
                
                % Extract subElements of the hex
                if (d<finiteElement.dim)
                    subElements = s.geometric.element(d,1);
                else
                    subElements = s.geometric;
                end

                for e=1:numel(subElements)
                    
                    selected = subElements(e);                              % select only 1 geometric element
                    
                    % If the dof numbering where periodic boundaries are
                    % taken into account is requested, also check if the
                    % selected subElement is not flagged as periodic.
                    % Otherwise just continue anyways
                    if includePeriodicity
                        periodicFlag = ~selected.periodic;
                    else
                        periodicFlag = true;
                    end
                    
                    if (selected.active && periodicFlag)
                        flags = (ePattern==e);
                        
                        if (numel(includePeriodicity)>1)
                            reusePattern = periodicPattern( flags );
                        else
                            reusePattern = includePeriodicity;
                        end
                        
                        currentDofs  = selected.dof(true);

                        if ( isempty(currentDofs) && (nnz(flags)>0) )
                            dofNumbering = lastDoF + ( 1:nnz( flags ) );

                            if (false)
                                flags( all(~flags,2), : ) = [];                             % Remove zero rows
                                flags( :, all(~flags,1) ) = [];                             % Remove zero columns
                                selected.setDof( reshape( dofNumbering, size(flags) ) );    % Reshape and set the dofs
                            
                            else
                                %% Modified by MMKK on 08/05/2018
                                % The unique operation is quite slow, try
                                % to prevent its use
                                for i=1:numel( size(flags) )
                                    %patternSize(i) = max( unique( sum(flags,i) ) );
                                    temp = sum(flags,i);
                                    patternSize(i) = max( temp(:) );
                                end
                                selected.setDof( reshape( dofNumbering, patternSize ) );
                            end

                            % increase lastDoF with the total of added DoFs
                            lastDoF = lastDoF + nnz( flags );
                            
                        elseif ( selected.periodic && (nnz(flags)>0) && numel(reusePattern)>1 && nnz(reusePattern)~=nnz(flags))
                            if ( isempty(selected.dofObject.get) )
                                dofNumbering = currentDofs;
                                dofNumbering(~reusePattern) = lastDoF + ( 1:nnz( ~reusePattern ) );

                                % reshape
                                for i=1:numel( size(flags) )
                                    %patternSize(i) = max( unique( sum(flags,i) ) );
                                    patternSize(i) = max( sum(flags,i), [], 'all' );
                                end
                                selected.setDof( reshape( dofNumbering, patternSize ) );

                                % increase lastDoF with the total of added DoFs
                                lastDoF = lastDoF + nnz( ~reusePattern );
                            end
                        else
                            disp('')
                        end
                    else
                        disp('')
                    end
                end
            end
            
            % Finally get the DoF values for the element
            try
                dofs = selected.allDoF();
            catch MExp
                error('Problem with allDoF')
            end
        end
        
        function [ qdofs, lastqDoF ] = qdof(s,lastqDoF,feType,includePeriodicity,activeOnly)
            
            if (activeOnly)
                if (~s.geometric.active)
                    qdofs = [];
                    return
                end
            end
            
            if (nargin>2 && ~isempty(feType))
                s.feType = feType;
            end
            
            % By default periodic boundaries are take into account
            if nargin<4
                includePeriodicity = true;
            end
            
            patternSize = zeros(1,s.dimension);
            
            % Get the finite element from the collection
            finiteElement = FECollection.get( s.feType );%s.mesh.feType(e) );
                
            if (~s.geometric.active)
                error('LSQDIM:StaticElement :: The main geometricElement is not active')
            end
            
            for d=0:s.dimension
                
                % Get the dof flags for the elements with dimension d
                qdofPattern     = (finiteElement.qdofType==d);
                ePattern        = (finiteElement.qdofTypeID .* qdofPattern );
                
                % Determine the periodic pattern
                if (numel(includePeriodicity)>1)
                    periodicPattern = false( size(qdofPattern) );
                    for p=1:size(includePeriodicity,2)          % spatial directions
                        for c=1:size(includePeriodicity,1)      % continuity (value, derivative etc)
                            if includePeriodicity(c,p)
                                switch p
                                    case 1
                                        periodicPattern(   c   ,:,:) = true;
                                        periodicPattern(end-2+c,:,:) = true;
                                    case 2
                                        periodicPattern(:,   c   ,:) = true;
                                        periodicPattern(:,end-2+c,:) = true;
                                    case 3
                                        periodicPattern(:,:,   c   ) = true;
                                        periodicPattern(:,:,end-2+c) = true;
                                end
                            end
                        end
                    end
                end
                
                % Extract subElements of the hex
                if (d<finiteElement.dim)
                    subElements = s.geometric.element(d,1);
                else
                    subElements = s.geometric;
                end

                for e=1:numel(subElements)
                    
                    selected = subElements(e);
                    
                    % If the dof numbering where periodic boundaries are
                    % taken into account is requested, also check if the
                    % selected subElement is not flagged as periodic.
                    % Otherwise just continue anyways
                    if includePeriodicity
                        periodicFlag = ~selected.periodic;
                    else
                        periodicFlag = true;
                    end
                    
                    if (selected.active && periodicFlag)
                        flags = (ePattern==e);
                        
                        if (numel(includePeriodicity)>1)
                            reusePattern = periodicPattern( flags );
                        else
                            reusePattern = includePeriodicity;
                        end
                        
                        currentqDofs  = selected.qdof(true);

                        if ( isempty(currentqDofs) && (nnz(flags)>0) )
                            qdofNumbering = lastqDoF + ( 1:nnz( flags ) );

                            % reshape
                            for i=1:numel( size(flags) )
                                %patternSize(i) = max( unique( sum(flags,i) ) );
                                patternSize(i) = max( sum(flags,i) );
                            end
                            selected.setqDof( reshape( qdofNumbering, patternSize ) );

                            % increase lastDoF with the total of added DoFs
                            lastqDoF = lastqDoF + nnz( flags );
                            
                        elseif ( selected.periodic && (nnz(flags)>0) && numel(reusePattern)>1 && nnz(reusePattern)~=nnz(flags))
                            if ( isempty(selected.qdofObject.get) )
                                qdofNumbering = currentqDofs;
                                qdofNumbering(~reusePattern) = lastqDoF + ( 1:nnz( ~reusePattern ) );

                                % reshape
                                for i=1:numel( size(flags) )
                                    %patternSize(i) = max( unique( sum(flags,i) ) );
                                    patternSize(i) = max( sum(flags,i) );
                                end
                                selected.setqDof( reshape( qdofNumbering, patternSize ) );

                                % increase lastDoF with the total of added DoFs
                                lastqDoF = lastqDoF + nnz( ~reusePattern );
                            end
                        else
                            disp('')
                        end
                    else
                        disp('')
                    end
                end
            end
            
            % Finally get the DoF values for the element
            try
                qdofs = selected.allqDoF();
            catch
                error('Problem with allqDoF')
            end
        end
        
        
        % Function to return the levels of all subElements
        function levels = levels(s) %,activeMaterial)
%             if (nargin==1)
%                 activeMaterial = s.activeMaterial;
%             end
            
            selected = s.geometric;
            levels = selected.allLevels();
        end
        
        % Function to return the levels of all subElements
        function levels = dofLevels(s)
            selected = s.geometric;
            levels = selected.allDofLevels();
        end
                    
%         function varargout = getNodes(s)
% %             if nargout ~= numel( s.nodes )
% %                 error('Please request all available nodes');
% %             end
%             
%             varargout = cell(numel( s.nodes ),1);
%             [ varargout{:} ] = ndgrid( s.nodes{:} );
%             
%             if nargout < numel( s.nodes )
%                 if (numel( s.nodes )==4)
%                     for i=1:nargout
%                         varargout{i} = varargout{i}(:,:,:,1);
%                     end
%                 elseif (numel( s.nodes )==3)
%                     for i=1:nargout
%                         varargout{i} = varargout{i}(:,:,1);
%                     end
%                 elseif (numel( s.nodes )==2)
%                     for i=1:nargout
%                         varargout{i} = varargout{i}(:,1);
%                     end                    
%                 end
%                 
%             elseif nargout > numel( s.nodes )
%                 tmpNodes = cell(nargout,1);
%                 tmpNodes(1:numel( s.nodes )) = s.nodes(:);
%                 tmpNodes(numel( s.nodes )+1:nargout) = repmat({0},nargout-numel( s.nodes ),1);
%                 
%                 varargout = cell(nargout,1);
%                 [ varargout{:} ] = ndgrid( tmpNodes{:} );
%             end
%         end
        
        function setPosition(s,varargin)
            s.position = [varargin{:}];
        end
%         
%         function be = getBoundaryElements(s,boundary)
%             if (s.isRefined)
%                 be = [];
%                 for i=1:numel(s.ch)
%                     be = [be s.ch{i}.getBoundaryElements(boundary)];
%                 end
%             else
%                 if (s.nb(boundary)==-1)
%                     be = s.id;
%                 else
%                     be = [];
%                 end
%             end
%         end
        
%         function out = isBoundaryElement(s,dir,pos)
%             if (s.isRefined)
%                 disp('Should not be here: check isBoundaryElement in oct.m')
%             else
%                 out = isempty(s.neighbor{dir,pos+1});
%             end
%         end
        
        function out = getRank(s)
            if (s.isRefined)
                out = [];
                for i=1:numel(s.ch)
                    out = [out s.ch{i}.getRank()];
                end
            else
                out = s.rank;
            end
        end
                
        % Returns the position of this element wrt its parent as a column 
        % array
        function out = getPosition(s)
            out = s.position;
            if (size(out,1)<size(out,2))
                out = out';
            end
        end
        
        % Return the general Jacobian, or the Jacobian in a specific
        % direction
        function out = J(s,n)
            if (nargin==1)
                out = s.geometric.detJ;
            elseif (nargin==2)
                out = s.geometric.J;
                if (n==-1)
                    out = diag(out);
                else
                    out = out(n,n);
                end
            end
            %out = 1./ out;
        end
        
        % Return only the spatial Jacobian
        function out = JSpatial(s)
            if (System.settings.time.spaceTime)
                out = s.geometric.detJ / s.J(System.settings.mesh.dim);
            else
                out = s.geometric.detJ;
            end
        end
        
        function set.active(s,value)
            s.active = value;
            
            if ( ~isempty(s.parent) )
                s.parent.active = value;
            end
        end
        
        function out = get.active(s)
            out = s.active;
        end
        
%         function varargout = getX()
%             x = 0;
%             [ varagout ] = ndgrid(x,y,z,t);
%         end
        
%         function out = getLocations(s,disc)
%             if (nargin==1)
%                 out{1} = s.xelem;
%                 out{2} = s.yelem;
%                 out{3} = s.zelem + System.settings.time.current;
%             else
%                 dx = zeros(s.disc.DIM,1);
%                 for i=1:s.disc.DIM
%                     dx(i) = s.nodes{i}(end) - s.nodes{i}(1);
%                 end
%                 
%                 out = cell(s.disc.DIM,1);
%                 
%                 for i=1:s.disc.DIM
%                     out{i} = s.nodes{i}(1) + dx(i) * (disc.basis{i}.qp + 1)/2;
%                 end
%                 
% %                 dx = s.xelem(end)-s.xelem(1); out{1} = s.xelem(1) + dx * (disc.xq + 1)/2;
% %                 dy = s.yelem(end)-s.yelem(1); out{2} = s.yelem(1) + dy * (disc.yq + 1)/2;
% %                 dz = s.zelem(end)-s.zelem(1); out{3} = s.zelem(1) + dz * (disc.zq + 1)/2 + System.settings.time.current;
%             end
%         end
    end    
end

