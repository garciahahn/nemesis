classdef LogicalEdge < LogicalElement
    %UNTITLED3 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Constant)
        activeSwitch = [ 1 2 4 ];
    end
    
    properties
        orientation;
        vertices;
        
        internalVertices;
        internalEdges;
                
%         shape;
        
        isSimple = false;
    end
    
    properties (Access=private)
        edgeLengthMap, midPoint;
    end
    
    methods
        function s = LogicalEdge(varargin)
            
            [vertices,parent] = VerifyInput( varargin );
            
            if (numel(vertices)~=2)
                error('Please provide 2 vertices')
            end
            
            s@LogicalElement(1,parent);
            
            s.vertices      = vertices;
                
            for i=1:2
                s.vertices(i).addUser(s);
            end
        end
    end
    
    methods (Access=private)
        
%         function computeOrientation(s)
%             [v0,v1]       = s.vertices.X;
%             try
%             edgeVector    = (v1-v0);            
%             catch
%                disp('')
%             end
%             edgeLength    = norm(edgeVector);            
%             s.orientation = edgeVector / edgeLength;
%             
%             s.orientation( abs(s.orientation)<1e-14 ) = 0;
%         end
        
%         function initMapping(s)
%             if (s.shape==0)
%                 % linear mapping
%                 [v0,v1] = s.element(0).X;
%                 
%                 s.edgeLengthMap = ( v1 - v0 ) / 2;
%                 s.midPoint      = ( v1 + v0 ) / 2;
%             else
%                 error('Unknown edge shape')
%             end
%         end
        
%         function defineSimple(s)
%             delta = s.vertices(2).X - s.vertices(1).X;
%             delta( abs(delta)<1e-14 ) = 0;
%             if ( nnz( delta ) == 1 )
%                 s.isSimple = true;
%             else
%                 disp('')
%             end
%         end
    end
    
    methods        
%         % Derivative physical to logical (d X / d xi)
%         function out = derivative(s,xi)
%             switch s.shape
%                 case 0
%                     out = s.edgeLengthMap;
%                 otherwise
%                     error('Unknown edge shape')
%             end
%         end
        
        function out = refine(s,refineFactor,refineFlag)
            if (nargin==2)
                refineFlag = s.refineFlag;
            end
            
            % Refine this edge if requested and if it's still active
            if (refineFlag && ~s.isRefined)
                s.createInternal(refineFactor);
                s.isRefined = true;
            end
            
            if (refineFactor ~= size(s.internalEdges))
                disp('')
            end
            
            % Return
            if (s.isRefined)
                out = s;
            else
                out = s.internalEdges;
            end
            
            s.active = false;
        end
        
        function coarsen(s)
                        
            % Remove the internalEdge as user of vertices
            % This should delete the internalVertices
            if (all( isvalid( [s.internalEdges{:}] ) ))
                for e = [s.internalEdges{:}]
                    for v = e.v
                        v.removeUser( e );
                    end
                end
            elseif (any( isvalid( [s.internalEdges{:}] ) ))
                error('Not all internalEdges are valid')
            else
                % normal behaviour
            end
            
            if ( isempty(s.internalVertices) || all( ~isvalid( [s.internalVertices{:}] ) ))
                 s.internalVertices = [];
            else
                error('Not all internalVertices have been deleted')
            end
            
            for e = [s.internalEdges{:}]
                delete( e );
            end
            s.internalEdges = [];
            
            s.isRefined = false;
            
            % Check if the direct vertices of this parent are still
            % valid (refined by another edge), and update them accordingly
            for v=1:numel(s.vertices)
                if (s.vertices(v).isRefined)
                    if (~isempty(s.vertices(v).internalVertices))
                        stillRefined = all( isvalid( [s.vertices(v).internalVertices{:}] ) );
                        s.vertices(v).isRefined = stillRefined;
                        if (~stillRefined)
                            s.vertices(v).coarsen;
                        end
                    end
                end
            end
        end
        
        function delete(s)
            if ( all( isvalid( s.vertices(:) ) ) )
                for v=1:2
                    s.vertices(v).removeUser(s)
                end
            end
        end
        
        function createInternal(s,refineFactor)
            numInternalVertices = refineFactor-1;
            s.internalVertices  = cell(1,refineFactor-1);
            
            numInternalEdges    = refineFactor;
            s.internalEdges     = cell(1,refineFactor);
            
            s.vertices(1).refine(1,1);
            s.vertices(2).refine(1,1);
            
            % Determine the logical locations of the internal vertices
            x1 = linspace(-1,1,refineFactor+1);
            
            % internal vertices of this face
            for i=2:refineFactor
                coordinate = s.mapping( x1(i) );          % the logical location of the additional vertex
                s.internalVertices{i-1} = GeometricVertex( coordinate, s );
                
                if (s.periodic)
                    s.internalVertices{i-1}.setPeriodic( s.leader.internalVertices{i-1} );
                end
            end
            
            allVertices = cell( 1, numel(s.internalVertices)+2 );
            allVertices{1} = s.vertices(1).internalVertices{1};
            if (numel(s.internalVertices) > 1)
                allVertices(2:end-1) = s.internalVertices(:);
            elseif (numel(s.internalVertices) == 1)
                allVertices{2} = s.internalVertices{1};
            end
            allVertices{end} = s.vertices(2).internalVertices{1};
            
            for e=1:numel(allVertices)-1
                s.internalEdges{e} = GeometricEdge( allVertices(e:e+1), s );
                
                if (s.periodic)
                    s.internalEdges{e}.setPeriodic( s.leader.internalEdges{e} );
                end
            end
            
%             %% Add to feLists
%             if (s.withFELists)
%                 for v=1:numInternalVertices
%                     feLists{1}.insert(s.internalVertices{v});
%                 end            
%                 for e=1:numInternalEdges
%                     feLists{2}.insert(s.internalEdges{e});
%                 end
%             end
            
            s.active = false;
            
%             % Remove this edge as a user of the 2 nodes, and remove the
%             % node if there are no users left
%             for i=1:2
%                 s.vertices(i).removeUser(s);
%                 
%                 %if (s.vertices(i).numUser==0)
%                 %    delete( s.vertices(i) );
%                 %end
%             end
        end
                
        function plot(s)
            if (s.active)
                coordinate1 = s.vertices(1).X; 
                coordinate2 = s.vertices(2).X;

                center = 0.5 * ( coordinate1 + coordinate2 );
                delta  = (coordinate2 - coordinate1) / 2;

                scaling = 0.8;

                c1 = center - scaling*delta;
                c2 = center + scaling*delta;

                coordinate = [c1 ; c2];

                plot3( coordinate(:,1),coordinate(:,2),coordinate(:,3), s.levelColors{ s.level+1 } )
                
            else
                error('This edge is not active, but still in the list')                
            end
        end
        
        function out = getActiveVertices(s)
            out = [];
            
            if (s.isRefined && ~s.active)
                % internal vertices
                tmp = cellfun( @(x) x.getActiveVertices, s.internalVertices, 'UniformOutput', false );
                tmp = vertcat( tmp{:} );
                out = [ out; tmp(:) ];
                    
                % vertices within internal edges
                tmp = cellfun( @(x) x.getActiveVertices, s.internalEdges, 'UniformOutput', false );
                tmp = vertcat( tmp{:} );
                out = [ out; tmp(:) ];                
            end
        end
        
        function out = getActiveEdges(s)
            if (s.isRefined && ~s.active)                
                % edges within internal edges
                tmp = cellfun( @(x) x.getActiveEdges, s.internalEdges, 'UniformOutput', false );
                out = vertcat( tmp{:} );
            elseif (s.active) 
                out = {s};
            elseif (~s.active)
                out = s.parent.getActiveEdges();
            else
                error('why am I here?')
            end
        end
        
        function setPeriodic(s,leader)
            s.periodic = true;
            s.leader   = leader;
        end
        
        function updateActive(s,flag)
            
            if (s.maxDim==s.dimension)
                if ( s.isRefined )
                    s.active = false;
                    cellfun( @(x) x.updateActive(flag), s.internalEdges );
                else
                    s.active = flag;
                    arrayfun( @(x) x.updateActive(flag), s.vertices );
                end
                
            else
                % Make this edge active, and deactivate any internal elements
                s.setActive(flag);
                
                % If any parent is active already, ensure this child does
                % not become active after all
                parent = s.parent;
                while ~isempty( parent )
                    if ( parent.active )
                        s.active = false;
                    end
                    parent = parent.parent;
                end
            end
        end

        function setActive(s,flag)
            try
            s.active = flag;
            if ( s.isRefined )
                cellfun( @(x) x.setActive(false), s.internalEdges );
                cellfun( @(x) x.setActive(false), s.internalVertices );
            end
            catch
               error('LSQDIM:element:GeometricEdge :: setActive issue'); 
            end
        end        
    end    
end