classdef LogicalVertex < LogicalElement
    %UNTITLED3 Summary of this class goes here
    %   Detailed explanation goes here
    
%     properties (Constant, Access = ?GeometricElement)
%         activeSwitch = [ 2 4 6 ];
%     end
    
    properties      
        internalVertices;
    end
    
    methods
        function s = LogicalVertex(varargin)
            [parent] = VerifyInput( varargin );
                        
            s@LogicalElement(0,parent);
        end
        
        function out = refine(s,refineFactor,refineFlag)
            if (nargin==2)
                refineFlag = s.refineFlag;
            end
            
            % Refine this edge if requested and if it's still active
            if (refineFlag && ~s.isRefined)
                s.createInternal(1);
                s.isRefined = true;
            end
            
            if (refineFactor ~= size(s.internalVertices))
                error('LSQDIM:GeometricVertex:refine :: refineFactor is not equal to the size of internalVertices')
            end
            
            % Return
            if (s.isRefined)
                out = s;
            else
                out = s.internalVertices;
            end
            
            s.active = false;
        end
        
        function coarsen(s)            
            for v = [s.internalVertices{:}]
                delete( v );
            end
            s.internalVertices = [];
            
            s.isRefined = false;
        end
        
        function delete(s)
            % do nothing
        end
        
        function createInternal(s,refineFactor)
            s.internalVertices  = cell(1,refineFactor);
            
            % internal vertex of this vertex
            s.internalVertices{1} = GeometricVertex( s.X, s );
            
            if (s.periodic)
                s.internalVertices{1}.setPeriodic( s.leader.internalVertices{1} );
            end
                
            s.active = false;
        end
        
        function setPeriodic(s,leader)
            s.periodic = true;
            s.leader   = leader;
        end
        
        function updateActive(s,flag)
            if (s.maxDim==s.dimension)
                if ( s.isRefined )
                    s.active = false;
                    cellfun( @(x) x.updateActive(true), s.allVertices );
                else
                    s.active = flag;                    
                end
            else
                % Make this vertex active, and deactivate any internal elements
                s.setActive(flag);
                
                % If any parent is active already, ensure this child does
                % not become active after all
                parent = s.parent;
                while ~isempty( parent )
                    if ( parent.active )
                        s.active = false;
                    end
                    parent = parent.parent;
                end
            end
        end
        
        function setActive(s,flag)
            try
                s.active = flag;
                if ( s.isRefined )
                    cellfun( @(x) x.setActive(false), s.internalVertices );
                end
            catch
               error('LSQDIM:element:GeometricVertex :: setActive issue'); 
            end
        end  
        
        function out = getActive(s)
            if (~isempty(s.parent))
                out = s.parent.vertices;
            else
                error('should not be here')
            end
        end
        
        function plot(s)
         	scatter3(s.X(:,1),s.X(:,2),s.X(:,3), s.levelColors{s.level+1} )
        end
        
        function out = getActiveVertices(s)
            if (s.isRefined && ~s.active)
                % internal vertices
                out = s.internalVertices{1}.getActiveVertices;
            else
                out = {s};
                if (~s.active)
                    disp('')
                end
            end
        end
    end
end

