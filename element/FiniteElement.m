%*****************************
% 05 May 2014
%*****************************

%addpath('/work/lsqdim/library/HighOrderLib');
%addpath('/work/lsqdim/library/LSQDIM/');

% load HighOrderDB
% global HOLib_DB

classdef FiniteElement < handle
    
    properties (Access = private, Constant)
        ROUNDOFF_LIMIT = 1e-14;
    end
    
    properties (Access = private)        
        Pt; Qt;
        tq;
        Dt;
        wt;
        Ht;
        Bt0; Bt1;
        Dtt;
        Hxt;    Hyt;
        Ztm, Ztp;
        
        H1, D1, DD1, DDD1;
        
        dofeq, dofe;
        dofev; dofevq;
    end
    
    properties
        sDim;
        dim;
        spaceTime;
        
        P; Q; C;
        basis;
        
        numSpatialDofs;
        numSpatialPoints;
                
        refinementRatio;
        
        nodes;
        logicalCoordinates;
        
        %qp, qw;
        
        %xq; yq; zq;
        %wx; wy; wz;
                        
        %Dx; Dy; Dz;
        W, H , D , DD, DDD;
        D_t, DD_t;
        W1;                 % 1D weights
        W2;                 % 2D weights
        Dxy; Dyx;
        Dxz; Dyz;
        Wspatial;
        
        boundary;
        
        connectivity;
                
        Z;
        
        matZ;
        vecZ;
        
        dofPattern;
        dofType;
        dofTypeID;
        
        qdofType;
        qdofPattern;
        qdofTypeID;
        
        qNodeEdges;
    end %properties
    
    
    methods
        % Constructor
        function D = FiniteElement(P,Q,C,sDim,spaceTime)
            
            activeSettings = System.settings;
            
            if (nargin==0)
                P           = System.settings.stde.Pn;
                Q           = System.settings.stde.Q;
                C           = System.settings.stde.C;
                sDim        = System.settings.mesh.sdim;
                spaceTime   = (System.settings.time.method==TimeMethod.SpaceTime);
            end
            
            % Save input values as properties of this object
            D.sDim = sDim;
            D.spaceTime = spaceTime;
            
            % Determine the correct size for arrays
            D.dim = sDim + spaceTime;
            
            % Setup P, Q and C
            D.P = D.verifyInput(P,'P');
            D.Q = D.verifyInput(Q,'Q');
            D.C = D.verifyInput(C,'C');
            
            % Assign the refinementRatios
            D.refinementRatio = ones(1,4);
            for i=1:D.dim
                D.refinementRatio(i) = activeSettings.mesh.refinementRatio(i);
            end
            
            % Use the provided input to create the basis functions
            D.setupBasisFunctions();
            
            D.dofe  = ones(1,4);
            D.dofeq = ones(1,4);
            
            for i=1:D.dim
                D.dofe(i)   = D.basis{i}.dof;
                D.dofeq(i)  = D.basis{i}.dofq;
            end
                        
            % Compute the total number of dofs / quadrature points
            D.dofev  = prod(D.dofe );     
            D.dofevq = prod(D.dofeq); 
            
            D.setupOperators();
            
            % Compute the spatial number of dofs / quadrature points
            if (D.spaceTime)
                D.numSpatialDofs    = D.dofev  / D.basis{end}.dof;          % # dof in 1 element for 1 variable
                D.numSpatialPoints  = D.dofevq / D.basis{end}.dofq;         % # solution points in 1 element for 1 variable
            else
                D.numSpatialDofs    = D.dofev ;
                D.numSpatialPoints  = D.dofevq;
            end
            
            % Setup the zero matrix and vector
            D.matZ = sparse( D.dofevq , D.dofev );
            D.vecZ = sparse( D.dofevq , 1 ) ;
            
            D.setConnectivity;
            
%           D.setDoFPatterns;
            D.setDoFType;
            D.setDoFTypeID;
            
            %% Quadrature points
            D.setqDoFType;
            D.setqDoFTypeID;
            
            D.computeQuadratureNodes;
            
            if (D.dim==2)
                D.setQuadratureNodeEdges;
            end
        end
        
        function out = verifyInput(s,variable,name)
            if (numel(variable)~=s.dim)
                if (numel(variable)==1)
                    out = variable*ones(1,s.dim);
                elseif numel(variable)>s.dim
                    out = variable(1:s.dim);
                else                    
                    if s.dim==1
                        error('Please provide just 1 value for %s',name)
                    else
                        error('Please provide just 1 or %d values for %s',s.dim,name)
                    end
                end
            else
                out = variable;
            end
        end
        
        function setupBasisFunctions(D)

%             % In each direction the standard element goes from -1 to +1, so h=2
%             % This implies the Jacobian in each direction is equal to 1
%             h = 2 * ones(D.dim,1);
%             J = 2./h;
%                         
%             % Compute the quadrature points and weights
%             D.qp = cell(D.dim,1);
%             D.qw = cell(D.dim,1);
%             
%             for i=1:D.dim
%                 [D.qp{i},D.qw{i}] = GLL(D.Q(i),-1,1);
%             end
            
%             % Setup the H, D, and DD arrays, which depend on the selected
%             % continuity level. The values in the arrays are computed for
%             % the quadrature points
%             D.H1  = cell(D.dim,1);
%             D.D1  = cell(D.dim,1);
%             D.DD1 = cell(D.dim,1);
            
            D.basis = cell(D.dim,1);
            
            scale = ones(4,1); %
            %scale = [6/8, 6/8, 0.01]/2;
            
            for i=1:D.dim                
                switch D.C(i)
                    case 0
                        % Lagrange C0
                        D.basis{i} = BasisFunction(D.P(i),D.Q(i),D.C(i),FunctionType.Lagrange);
                        
                    case 1
                        % Hermite C1 
                        D.basis{i} = Hermite1(D.P(i),D.Q(i),D.C(i),scale(i));
                        
                    case 2
                        % Hermite C1 
                        D.basis{i} = Hermite2(D.P(i),D.Q(i),D.C(i),scale(i));
                        
                    case 3
                        % Hermite C1 
                        D.basis{i} = Hermite3(D.P(i),D.Q(i),D.C(i),scale(i));
                        
%                         if (D.dofe(i)>3)
%                             ind = [1 2 5:1:D.dofe(i) 3 4];
% 
%                             D.H1{i}  = HermiteN(  ind,D.qp{i},J(i));
%                             D.D1{i}  = DHermiteN( ind,D.qp{i},J(i));
%                             D.DD1{i} = DDHermiteN(ind,D.qp{i},J(i));
%                         else
%                             error('P should be larger than or equal to 3 for C1.  Increase the polynomial order, or switch to C0')
%                         end
                    otherwise 
                        if (D.P(i)<2*D.C(i))
                            error('Please ensure P is at least 2*C')
                        end
                        
                        if (D.Q(i)<D.P(i))
                            error('Please ensure Q is at least P')
                        end
                        
                        D.basis{i} = BSpline(D.P(i),D.Q(i),D.C(i));

                end                        
            end
            
        end
        
        function setupOperators(D)
                        
%             if (isprop(D.basis{1},'derScale'))
%                 scale = D.basis{1}.derScale;
%             else
                scale = 1;
%             end
            
            %% Multidimensional operators
            % Weights
            D.W1    = cellfun( @(x) x.qw, D.basis, 'UniformOutput', false );
            D.W     = superkron(D.W1);
            if D.spaceTime
                if (D.sDim==1)
                    D.Wspatial = D.W1;
                else
                    D.Wspatial = superkron(D.W1{end-1:-1:1});
                end
            else
                D.Wspatial = D.W;
            end
            
            % Partial weights
            if (D.dim==3)
                D.W2 = cell(3,1);
                D.W2{1} = superkron(D.W1{3},D.W1{2});
                D.W2{2} = superkron(D.W1{3},D.W1{1});
                D.W2{3} = superkron(D.W1{2},D.W1{1});
            elseif (D.dim==2)
                D.W2 = cell(1,1);
                D.W2{1} = superkron(D.W1{2},D.W1{1});
            elseif (D.dim==1)
                D.W2 = [];
            else
                error('Please check FiniteElement :: error in assigning the weights')
            end
            
            % H array (values)
            D.H1    = cellfun( @(x) x.H, D.basis, 'UniformOutput', false );
            D.H     = D.getTransform();
            
            % D array (first derivative in space and no derivative in time)
            D.D1    = cellfun( @(x) x.D, D.basis, 'UniformOutput', false );
            D.D     = cell(D.dim,1);
            for i=1:D.dim
                D.D{i} = D.getTransform(i,D.D1{i}) * scale;
            end
            
            % Dxy
            if (D.dim>1)
                D.Dxy = D.getTransform(1,D.D1{1},2,D.D1{2}) * scale^2;
                D.Dyx = D.getTransform(1,D.D1{2},2,D.D1{1}) * scale^2;
            end
            
            if (D.sDim==3 && ~D.spaceTime)
                D.Dxz = D.getTransform(1,D.D1{1},3,D.D1{3}) * scale^2;
                D.Dyz = D.getTransform(2,D.D1{2},3,D.D1{3}) * scale^2;
            end
            
            % D_t array (first derivative in space and the first derivative in time)
            if D.spaceTime
                D.D_t = cell(D.dim-1,1);
                for i=1:D.dim-1
                    D.D_t{i} = D.getTransform(i,D.D1{i},D.dim,D.D1{D.dim});
                end
            end
            
            % DD array (second derivative)
            % NB: no 2nd derivative in time required, so only spatial directions
            D.DD1   = cellfun( @(x) x.DD, D.basis, 'UniformOutput', false );
            D.DD = cell(D.sDim,1);          
            for i=1:D.sDim
                D.DD{i} = D.getTransform(i,D.DD1{i}) * scale^2;
            end
            
            % DDD array (third derivative)
            % NB: no 2nd derivative in time required, so only spatial directions
            D.DDD1   = cellfun( @(x) x.DDD, D.basis, 'UniformOutput', false );
            D.DDD = cell(D.sDim,1);          
            for i=1:D.sDim
                D.DDD{i} = D.getTransform(i,D.DDD1{i}) * scale^3;
            end
            
            % DD_t array (second derivative in space and the first derivative in time)
            if D.spaceTime
                D.DD_t = cell(D.dim-1,1);
                for i=1:D.dim-1
                    D.DD_t{i} = D.getTransform(i,D.DD1{i},D.dim,D.D1{D.dim});
                end
            end
            
            %% Boundary elements
            D.boundary = cell(D.dim,2);     % for each direction, low/high boundary
            
            % low & high boundaries k
            for k=1:2
                for i=1:D.dim
                    
                    if k==1
                        kPos = 1;
                        n    = 1;
                    elseif (k==2)
                        kPos = size(D.W1{i},1);
                        n    = -1;
                    end
                
                    D.boundary{i,k}     = struct;

                    if (D.dim==3)
                        vector = 0 * D.W1{i};
                        vector(kPos) = 1;
                        if (i==1)
                            D.boundary{i,k}.W = superkron( D.W1{3},D.W1{2},vector );
                        elseif (i==2)
                            D.boundary{i,k}.W = superkron( D.W1{3},vector,D.W1{1} );
                        elseif (i==3)
                            D.boundary{i,k}.W = superkron( vector,D.W1{2},D.W1{1} );
                        end
                    elseif (D.dim==2)
                        vector = 0 * D.W1{i};
                        vector(kPos) = 1;
                        if (i==1)
                            D.boundary{i,k}.W = superkron( D.W1{2},vector );
                        elseif (i==2)
                            D.boundary{i,k}.W = superkron( vector,D.W1{1} );
                        end
                    elseif (D.dim==1)
                        vector = 0 * D.W1{i};
                        vector(kPos) = 1;
                        D.boundary{i,k}.W = vector;
                    else
                        error('Please add this to FiniteElement :: weights for dimension == %d\n',D.dim)
                    end
                    
                    %boundaryW           = D.W1;
                    %boundaryW{i}        = ones(size(D.W1{i})); 
                    %D.boundary{i,k}.W   = superkron(boundaryW);

                    D.boundary{i,k}.H   = D.getTransform(i,D.H1{i}( kPos, : ));

                    D.boundary{i,k}.D   = cell(D.dim,1);
                    for j=1:D.dim
                        if (i~=j)
                            D.boundary{i,k}.D{j}  = D.getTransform(i,D.H1{i}( kPos, : ),j,D.D1{j});
                        elseif (i==j)
                            D.boundary{i,k}.D{i}  = D.getTransform(i,D.D1{i}( kPos, : ));
                        end
                    end

                    D.boundary{i,k}.normal  = i;
                    D.boundary{i,k}.Dn      = D.boundary{i,k}.D{i};

                    D.boundary{i,k}.Hpos    = D.getPositions(i,D.H1{i}( kPos, : ));
                    D.boundary{i,k}.Dpos    = D.getPositions(i,D.D1{i}( kPos, : ));
                end
            end
            
            %-- depends on lv
            
            %% SINGLE LEVEL: refinement

            for i=1:D.dim
                D.basis{i}.initRefine( D.refinementRatio(i) );
            end
            
            %% Compute the general Z for refinement
            refRatio    = D.refinementRatio;
            
            zBase       = cellfun( @(x) x.Z, D.basis, 'UniformOutput',false );
            while numel(zBase)<4
                zBase{numel(zBase)+1} = {1};
            end
            
            D.Z         = cell( refRatio );
            
            ranges      = arrayfun( @(x) linspace(1,x,x), refRatio, 'UniformOutput', false );
            
            idxComb     = combvec( ranges{:} );         % each column has the indices of a single subelement
            
            for n=1:size(idxComb,2)
                
                idx = idxComb(:,n);
                
                tmpBase = cell(4,1);
                for i=1:4
                    tmpBase{i} = zBase{i}{idx(i)};
                end
                
                idx = num2cell( idx );
                D.Z{ idx{:} } = superkron( tmpBase );
            end
        end     % end setupHermiteBasis_3D_refine
        
        function computeQuadratureNodes(s)
            s.nodes = cell(1,4);
            
            for i=1:s.dim
                s.nodes{i} = s.basis{i}.qp;
            end
            for i=s.dim+1:4
                s.nodes{i} = 0;
            end
            
            % Create logicalCoordinates array for efficient access to all 
            tmp = cell(1,s.dim);
            [ tmp{:} ] = ndgrid( s.nodes{ 1:s.dim } );
            s.logicalCoordinates = zeros(s.dim,numel( tmp{1} ));            
            for i=1:s.dim
                s.logicalCoordinates( i,: ) = tmp{ i }(:);
            end
        end
        
        function setQuadratureNodeEdges(s)
            s.qNodeEdges = cell(4,1);
            
            xNodes = true (s.Q(1),1);
            yNodes = false(s.Q(2),1);
            yNodes(1) = true;
            s.qNodeEdges{1} = logical( reshape( superkron(yNodes,xNodes), s.Q ) );     % Bottom edge
            yNodes(1) = false; yNodes(end) = true;
            s.qNodeEdges{2} = logical( reshape( superkron(yNodes,xNodes), s.Q ) );     % Top edge
            
            xNodes = false(s.Q(1),1);
            yNodes = true (s.Q(2),1);
            xNodes(1) = true;
            s.qNodeEdges{3} = logical( reshape( superkron(yNodes,xNodes), s.Q ) );     % Left edge
            xNodes(1) = false; xNodes(end) = true;
            s.qNodeEdges{4} = logical( reshape( superkron(yNodes,xNodes), s.Q ) );     % Right edge 
            
        end
        
        function setConnectivity(s)            
            % Determine the number of sub elements inside a spectral element
            subElements = s.dofeq-1;
            subElements( subElements==0 ) = 1;
            numSubElements = prod( subElements );
            
            s.connectivity = zeros( numSubElements, 2^s.dim );
            
            nodeID = 1:s.dofevq;
            nodeID = reshape(nodeID,s.dofeq);
            
            switch s.dim
                case 1
                    count = 0;
                    for i=1:subElements(1)

                        count = count+1;

                        tmpNodes = nodeID(i:i+1);
                        %tmpNodes([3 4]) = tmpNodes([4 3]);

                        s.connectivity( count, : ) = tmpNodes(:);
                    end
                    %error('Please add connectivity for 1D')
                    
                case 2                    
                    count = 0;
                    for j=1:subElements(2)
                        for i=1:subElements(1)

                            count = count+1;

                            tmpNodes = nodeID(i:i+1,j:j+1);
                            tmpNodes([3 4]) = tmpNodes([4 3]);

                            s.connectivity( count, : ) = tmpNodes(:);
                        end
                    end
                    
                case 3
                    count = 0;
                    for k=1:subElements(3)
                        for j=1:subElements(2)
                            for i=1:subElements(1)

                                count = count+1;
                                
                                tmpNodes = nodeID(i:i+1,j:j+1,k:k+1);
                                tmpNodes([3 4]) = tmpNodes([4 3]);
                                tmpNodes([7 8]) = tmpNodes([8 7]);

                                s.connectivity( count, : ) = tmpNodes(:);
                            end
                        end
                    end
            end

            s.connectivity = (s.connectivity-1)';
        end
%         
%         function setDoFPatterns(s)
%             s.dofPattern = cell(1,4);
%             
%             % vertices
%             s.dofPattern{1} = cell( repmat(2,1,s.dim) );
%             
%             pat = cell(1,s.dim);
%             
%             vec = cell(1,s.dim);
%             for i=1:s.dim
%                 vec{i} = 1:2;
%             end
%             pos = combvec(vec{:});
%             
%             for i=1:numel(s.dofPattern{1})                
%                 position = pos(:,i);
%           
%                 for v=1:numel(position)
%                     pat{v} = s.basis{v}.dofPattern{position(v)};
%                 end
%                 s.dofPattern{1}{i} = logical( superkron( pat ) );
%             end
%             
%             % edges
%             s.dofPattern{2} = cell( repmat(2,1,s.dim) );
%             numBoundDof = cellfun( @(x) x.numBoundDof, s.basis, 'UniformOutput', false);
%             edgeDof    ;
%         end
        
        %
        % Function to determine to what type of geometric object 
        % each DoF belongs:
        %
        %  -1: unassigned
        %   0: vertex
        %   1: edge
        %   2: face
        %   3: hex
        %
        function setDoFType(s)
            % Initialize to unassigned
            s.dofType = -1 * ones(s.dofe);
            
            %% vertices
            vertices = cellfun(@(x) x.dofType==1, s.basis, 'UniformOutput', false);
            s.dofType( logical( superkron(vertices) ) ) = 0;
                    
            %% edges
            if (s.dim==1)
                edges = cellfun(@(x) x.dofType==0, s.basis, 'UniformOutput', false);
                s.dofType( logical( superkron(edges) ) ) = 1;
                
            elseif (s.dim>1)
                switch s.dim
                    case 2
                        flags = unique( perms([0 1]), 'rows' );
                    case 3
                        flags = unique( perms([0 1 1]), 'rows' );
                    case 4
                        flags = unique( perms([0 1 1 1]), 'rows' );     
                end
                for i=1:size(flags,2)
                    edges = cellfun(@(x,c) x.dofType==c, s.basis, num2cell( flags(:,i) ), 'UniformOutput', false);
                    s.dofType( logical( superkron(edges) ) ) = 1;
                end
            end
            
            %% faces
            if (s.dim==2)
                faces = cellfun(@(x) x.dofType==0, s.basis, 'UniformOutput', false);
                s.dofType( logical( superkron(faces) ) ) = 2;
            
            elseif (s.dim>2)
                switch s.dim
                    case 3
                        flags = unique( perms([0 0 1]), 'rows' );
                    case 4
                        flags = unique( perms([0 1 1 1]), 'rows' );     
                end
                flags = unique( perms([0 0 1]), 'rows' );
                for i=1:size(flags,2)
                    faces = cellfun(@(x,c) x.dofType==c, s.basis, num2cell( flags(:,i) ), 'UniformOutput', false);
                    s.dofType( logical( superkron(faces) ) ) = 2;
                end
            end
            
            %% hex
            if (s.dim==3)
                hex = cellfun(@(x) x.dofType==0, s.basis, 'UniformOutput', false);
                s.dofType( logical( superkron(hex) ) ) = 3;
                
            elseif (s.dim>3)
                error('4D finite elements are not supported')
            end
        end
        
        %
        % Function to determine to what type of geometric object 
        % each DoF belongs:
        %
        %  -1: unassigned
        %   0: vertex
        %   1: edge
        %   2: face
        %   3: hex
        %
        function setqDoFType(s)
            % Initialize to unassigned
            s.qdofType = -1 * ones(s.qSize);
            
            %% vertices
            vertices = cellfun(@(x) x.qdofType==1, s.basis, 'UniformOutput', false);
            s.qdofType( logical( superkron(vertices) ) ) = 0;
                    
            %% edges
            if (s.dim==1)
                edges = cellfun(@(x) x.qdofType==0, s.basis, 'UniformOutput', false);
                s.qdofType( logical( superkron(edges) ) ) = 1;
                
            elseif (s.dim>1)
                switch s.dim
                    case 2
                        flags = unique( perms([0 1]), 'rows' );
                    case 3
                        flags = unique( perms([0 1 1]), 'rows' );
                    case 4
                        flags = unique( perms([0 1 1 1]), 'rows' );     
                end
                for i=1:size(flags,2)
                    edges = cellfun(@(x,c) x.qdofType==c, s.basis, num2cell( flags(:,i) ), 'UniformOutput', false);
                    s.qdofType( logical( superkron(edges) ) ) = 1;
                end
            end
            
            %% faces
            if (s.dim==2)
                faces = cellfun(@(x) x.qdofType==0, s.basis, 'UniformOutput', false);
                s.qdofType( logical( superkron(faces) ) ) = 2;
            
            elseif (s.dim>2)
                switch s.dim
                    case 3
                        flags = unique( perms([0 0 1]), 'rows' );
                    case 4
                        flags = unique( perms([0 1 1 1]), 'rows' );     
                end
                flags = unique( perms([0 0 1]), 'rows' );
                for i=1:size(flags,2)
                    faces = cellfun(@(x,c) x.qdofType==c, s.basis, num2cell( flags(:,i) ), 'UniformOutput', false);
                    s.qdofType( logical( superkron(faces) ) ) = 2;
                end
            end
            
            %% hex
            if (s.dim==3)
                hex = cellfun(@(x) x.qdofType==0, s.basis, 'UniformOutput', false);
                s.qdofType( logical( superkron(hex) ) ) = 3;
                
            elseif (s.dim>3)
                error('4D finite elements are not supported')
            end
        end
        
        %
        % Function that gives an incremental ID to each DoFType
        function setDoFTypeID(s)
                        
            s.dofTypeID = -1 * ones(s.dofe);
            idx         = cell(1,s.dim);
            
            % Create all the possible blockIndices
            blockIdx = permn([1,2,3],s.dim);
            
            % Ensure the order of the elements is correct. This means the
            % first column is changing fastest, and the last column the
            % slowest
            blockIdx = sortrows(blockIdx,[s.dim:-1:1]);
            
            % Number the vertices, the middle segment of a basis function
            % (the 2nd entry of the dofID2 cell array) should be excluded.
            for d=1:s.dim+1
                subElementIdx = blockIdx( sum(blockIdx==2,2)==(d-1), :);
                
                % Ensure each subElement group is sorted as defined:
                %   vertices : x-value changing the fastest, etc.
                %   edges    : first all edges in x-direction, etc.
                %   faces    : first all faces in x-direction, etc.
                %   hexes    : normal x-y-z order, as vertices
                switch d
                    case 2
                        % edges
                        temp = subElementIdx(:,:)==2;
                        [~,index] = sortrows(temp,[s.dim:-1:1]);
                        subElementIdx = subElementIdx(index,:);
                        
                    case 3
                        % faces
                        temp = subElementIdx(:,:)==2;
                        [~,index] = sortrows(temp,[1:s.dim]);
                        subElementIdx = subElementIdx(index,:);                        
                end
                
                for i=1:size(subElementIdx,1)
                    b = subElementIdx(i,:);

                    for dir=1:s.dim
                        idx{dir} = s.basis{dir}.dofID{ b(dir) };
                    end

                    s.dofTypeID( idx{:} ) = i;
                end
            end
        end
        
        %
        % Function that gives an incremental ID to each DoFType
        function setqDoFTypeID(s)
                        
            s.qdofTypeID = -1 * ones(s.qSize);
            idx          = cell(1,s.dim);
            
            % Create all the possible blockIndices
            blockIdx = permn([1,2,3],s.dim);
            
            % Ensure the order of the elements is correct. This means the
            % first column is changing fastest, and the last column the
            % slowest
            blockIdx = sortrows(blockIdx,[s.dim:-1:1]);
            
            % Number the vertices, the middle segment of a basis function
            % (the 2nd entry of the dofID2 cell array) should be excluded.
            for d=1:s.dim+1
                subElementIdx = blockIdx( sum(blockIdx==2,2)==(d-1), :);
                
                % Ensure each subElement group is sorted as defined:
                %   vertices : x-value changing the fastest, etc.
                %   edges    : first all edges in x-direction, etc.
                %   faces    : first all faces in x-direction, etc.
                %   hexes    : normal x-y-z order, as vertices
                switch d
                    case 2
                        % edges
                        temp = subElementIdx(:,:)==2;
                        [~,index] = sortrows(temp,[s.dim:-1:1]);
                        subElementIdx = subElementIdx(index,:);
                        
                    case 3
                        % faces
                        temp = subElementIdx(:,:)==2;
                        [~,index] = sortrows(temp,[1:s.dim]);
                        subElementIdx = subElementIdx(index,:);                        
                end
                
                for i=1:size(subElementIdx,1)
                    b = subElementIdx(i,:);

                    for dir=1:s.dim
                        idx{dir} = s.basis{dir}.qdofID{ b(dir) };
                    end

                    s.qdofTypeID( idx{:} ) = i;
                end
            end
        end
        
        function out = getTransform(s,direction,array,direction2,array2)
            temp = s.H1;
            if nargin>1
                temp{direction} = array; 
            end
            if nargin>3
                temp{direction2} = array2;
            end
            out = superkron( temp );       
        end
        
        function out = getPositions(s,direction,array,direction2,array2)
            temp = cell(numel(s.dofe),1);
            for i=1:numel(s.dofe)
                temp{i} = eye(s.dofe(i));
            end
            if nargin>1
                temp{direction} = array; 
            end
            if nargin>3
                temp{direction2} = array2;
            end
            [~,out] = find( superkron( temp ) );
        end
        
        function out = getH(s)
            out = s.H;
        end
        
        function out = dof(s,direction)
            if nargin==1
                out = s.dofev;
            else
                out = s.dofe(direction);
            end
        end
        
        function out = dofSize(s)
            out = s.dofe;
        end
        
        function out = qdofSize(s)
            out = s.qSize;
        end
        
        function out = xDof(s)
            out = s.dofe(1);
        end
        
        function out = yDof(s)
            if (s.dim>1)
                out = s.dofe(2);
            else
                out = 1;
            end
        end
        
        function out = zDof(s)
            if (s.dim>2)
                out = s.dofe(3);
            else
                out = 1;
            end
        end
        
        function out = tDof(s)
            if (s.dim>3)
                out = s.dofe(4);
            else
                out = 1;
            end
        end
        
        function Z = getZrefine(s)
%             xDof = s.dofe(1);
%             yDof = s.dofe(2);
%             zDof = s.dofe(3);
%             tDof = s.dofe(4);
% 
%             
%             % scaling
%             xs = sparse( eye(s.xDof));
%             ys = sparse( eye(s.yDof));
%             zs = sparse( eye(s.zDof));
%                         
%             zBase = sparse( eye(zDof));
%             Zmm = superkron(zBase,s.Zym*ys,s.Zxm*xs);
%             Zpm = superkron(zBase,s.Zym*ys,s.Zxp*xs);
%             Zmp = superkron(zBase,s.Zyp*ys,s.Zxm*xs);
%             Zpp = superkron(zBase,s.Zyp*ys,s.Zxp*xs);
            
            Z = cell(s.refinementRatio);
            
            for t=1:s.refinementRatio(4)
                if (s.dim<4)
                    tBase = speye( s.dofe(4) );
                else
                    tBase = s.basis{4}.Z{t};
                end
                
                for z=1:s.refinementRatio(3)
                    if (s.dim<3)
                        zBase = speye( s.dofe(3) );
                    else
                        zBase = s.basis{3}.Z{z};
                    end
                    
                    for y=1:s.refinementRatio(2)
                        if (s.dim<2)
                            yBase = speye( s.dofe(2) );
                        else
                            yBase = s.basis{2}.Z{y};
                        end
                
                        for x=1:s.refinementRatio(1)
                            xBase = s.basis{1}.Z{x};
                                                        
                            Z{x,y,z,t} = superkron(tBase,zBase,yBase,xBase);
                        end
                    end
                end
            end
            
        end
        
        function plot(s)
            
        end

    	function standardElement = save(s)
            standardElement       	  = struct;
            standardElement.class     = class(s);
            standardElement.P         = s.P;
            standardElement.Q         = s.Q;
            standardElement.C         = s.C;
            standardElement.sDim      = s.sDim;
            standardElement.spaceTime = s.spaceTime;
        end
        
        function out = numQuadratureNodes(s)
            out = s.dofevq;
        end
        
        function out = numDof(s)
            out = s.dofev;
        end
        
        function out = qSize(s,n)
            out = s.dofeq;
            if (nargin==2)
                out = out(n);
            elseif (nargin>2)
                error('Too many input arguments')
            end
        end
        
        function out = dSize(s,n)
            out = s.dofe;
            if (nargin==2)
                out = out(n);
            elseif (nargin>2)
                error('Too many input arguments')
            end
        end        
    end %methods
    
    methods (Static)
        function s = init()
            settings = System.settings.stde;
            s = FiniteElement(settings.Pn,settings.Q,settings.C,settings.sDim,System.settings.time.spaceTime);
        end
        
        function out = load(standardElement,Q)
            P = standardElement.P;
            if (nargin~=2 || isempty(Q) || Q==0)
                Q = standardElement.Q;
            end
            C = standardElement.C;
            sDim = standardElement.sDim;
            spaceTime = standardElement.spaceTime;
            
            out = FiniteElement(P,Q,C,sDim,spaceTime);
        end
    end

end %classdef
