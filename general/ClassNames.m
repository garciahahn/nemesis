function [packageName,className] = classNames(s)
    names = strsplit(class(s),'.');
    
    if (numel(names)==2)
        packageName = names{1};
        className   = names{2};        
    elseif (numel(names)==1)
        className = names;
    else
        error('Unsupported package-class combination')
    end
end