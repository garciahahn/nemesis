function out = inrange(value,value1,value2)
    if (nargin==3)
        if (value >= value1 && value <= value2)
            out = true;
        else
            out = false;
        end
        
    elseif (nargin==2)
        out = ~isempty( intersect(value,value1) );
        
    else
        error('Wrong number of input arguments. Please use inrange(value,minValue,maxValue), or inrange(value,range)')
    end
end