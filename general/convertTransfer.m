function out = convertTransfer(transfer,rank)
    out = cell( size(transfer) );
    
    if nargin==1
        rank = System.rank;
    end
    
    for i=1:numel(out)
        if (i~=rank+1)
            temp = transfer{ rank+1 };
            selection = temp( temp==rank );
            selection( transfer{ i } ) = i-1;
            temp( temp==rank ) = selection;
            
            maxDof      = numel( transfer{ rank+1 } );
            dofValues   = 1:maxDof;
            
            out{i} = dofValues( temp==i-1 );
        end
    end
end