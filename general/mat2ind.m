function inds = mat2ind(matrix)
    sz = size( matrix );
    inds = repmat({1},1,numel(sz));
    for i=1:numel(inds)
        inds{ i } = 1:sz(i);
    end
end