function [varargout] = VerifyInput(varargin)

    % maximum of nargout optional inputs
    numvarargs = length(varargin{1});
    if numvarargs > nargout
        [ST] = dbstack('-completenames', 1);
        file = ST(1).file;        
        error('%s : requires at most %d optional inputs',file,nargout);
    end

    % set defaults for optional inputs
    if nargin==1
        % no defaults are provided, assume empty
        optargs = cell(1,nargout);        
    else
        % use the defaults as provided by varargin{2}, check that the
        % number of defaults matches the number of output arguments
        if (~iscell(varargin{2}))
            error('The second argument of VerifyInput must be a cell array')
        end
        numDefaultArgs = length(varargin{2});
        if numDefaultArgs ~= nargout
            [ST] = dbstack('-completenames', 1);
            file = ST(1).file;        
            error('call originated in %s : please provide exactly %d default values, or no values at all',file,nargout);
        end
        optargs = varargin{2};
    end

    % now put these defaults into the valuesToUse cell array, 
    % and overwrite the ones specified in varargin.
    %optargs(1:numvarargs) = varargin; 
    if (true)
        nonEmptyCells = cellfun( @(x) ~isempty(x), varargin{1} );
        optargs(nonEmptyCells) = varargin{1}(nonEmptyCells);
    else
        for i=1:numel( varargin{1} )
            if ( ~isempty( varargin{1}(i)) )
                optargs( i ) = varargin{1}(i);
            end
        end
    end
    
    % Place optional args in memorable variable names
    varargout = optargs;
end