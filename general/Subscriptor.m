classdef Subscriptor < handle
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
%    27 #ifdef DEBUG
%    28 namespace
%    29 {
%    30 // create a lock that might be used to control subscription to and
%    31 // unsubscription from objects, as that might happen in parallel.
%    32 // since it should happen rather seldom that several threads try to
%    33 // operate on different objects at the same time (the usual case is
%    34 // that they subscribe to the same object right after thread
%    35 // creation), a global lock should be sufficient, rather than one that
%    36 // operates on a per-object base (in which case we would have to
%    37 // include the huge <thread_management.h> file into the
%    38 // <subscriptor.h> file).
%    39   Threads::Mutex subscription_lock;
%    40 }
%    41 #endif

    properties (Constant, Access=private)
        unknown_subscriber = 'unknown subscriber'; 
    end
    
    properties
        counter;
        object_info;
        counter_map;
    end
    
    methods
        function s = Subscriptor(subscriptor)
            
            switch nargin
                case 0
                    s.counter     = 0;
                    s.object_info = 0;
                case 1
                    if ( isa(subscriptor,'Subscriptor'))
                        s.counter     = 0;
                        s.object_info = subscriptor.object_info;
                        
                        subscriptor.check_no_subscribers();
                    else
                        s.counter     = 0;
                        s.object_info = 0;
                    end
            end
        end
        
        function out = check_no_subscribers(s)
            % Check whether there are still subscriptions to this object. If so, output
            % the actual name of the class to which this object belongs, i.e. the most
            % derived class. Note that the name may be mangled, so it need not be the
            % clear-text class name. However, you can obtain the latter by running the
            % c++filt program over the output.
            if (s.counter ~= 0)
                out = false;
            else
                out = true;
            end
        end
   
        function object_info = operator(s)
            object_info = s.object_info;
%         5   s.check_no_subscribers();
%           166   object_info = s.object_info;
%           167   return *this;
  
        end
        
        function subscribe(s,name)
            if (s.object_info==0)
                s.object_info = class(s);
            end
            
            s.counter = s.counter+1;
            
            find(s.counter_map,name)
        end
%         
%   174 {
%   175 #ifdef DEBUG
%   176   if (object_info == 0)
%   177     object_info = &typeid(*this);
%   178   Threads::Mutex::ScopedLock lock (subscription_lock);
%   179   ++counter;
%   180 
%   181 #  ifndef DEAL_II_WITH_THREADS
%   182   const char *const name = (id != 0) ? id : unknown_subscriber;
%   183 
%   184   map_iterator it = counter_map.find(name);
%   185   if (it == counter_map.end())
%   186     counter_map.insert(map_value_type(name, 1U));
%   187 
%   188   else
%   189     it->second++;
%   190 #  else
%   191   (void)id;
%   192 #  endif
%   193 #else
%   194   (void)id;
%   195 #endif
% %   196 }
% 
%         
%         function unsubscribe(s,name)
%   201 {
%   202 #ifdef DEBUG
%   203   const char *name = (id != 0) ? id : unknown_subscriber;
%   204   AssertNothrow (counter>0, ExcNoSubscriber(object_info->name(), name));
%   205   // This is for the case that we do
%   206   // not abort after the exception
%   207   if (counter == 0)
%   208     return;
%   209 
%   210   Threads::Mutex::ScopedLock lock (subscription_lock);
%   211   --counter;
%   212 
%   213 #  ifndef DEAL_II_WITH_THREADS
%   214   map_iterator it = counter_map.find(name);
%   215   AssertNothrow (it != counter_map.end(), ExcNoSubscriber(object_info->name(), name));
%   216   AssertNothrow (it->second > 0, ExcNoSubscriber(object_info->name(), name));
%   217 
%   218   it->second--;
%   219 #  endif
%   220 #else
%   221   (void)id;
%   222 #endif
%   223 }
  
 
        function out = n_subscriptions(s)
            out = s.counter;
        end  
        
        function out = list_subscribers(s)
            nproc = Parallel3D.MPI.nproc;
            
            if (nproc==1)
                fprintf('subscriptions from:\n')
%                 while
%                      %s\""
%                 end
            else
                fprintf('No subscriber listing with multithreading\n');
            end
            
%   237   for (map_iterator it = counter_map.begin();
%   238        it != counter_map.end(); ++it)
%   239     deallog << it->second << '/'
%   240             << counter << " subscriptions from \""
%   241             << it->first << '\"' << std::endl;
%   242 #else
%   243   deallog << "No subscriber listing with multithreading" << std::endl;
%   244 #endif
        end
        
   
    end
end

% 
% ----------------------------------------------------------------
%    15 
%    16 #include <deal.II/base/thread_management.h>
%    17 #include <deal.II/base/subscriptor.h>
%    18 #include <deal.II/base/logstream.h>
%    19 
%    20 #include <typeinfo>
%    21 #include <string>
%    22 #include <iostream>
%    23 
%    24 DEAL_II_NAMESPACE_OPEN
%    25 
% 
%    46 
%  
%    75 
%    76 
%    77 
%    78 Subscriptor::~Subscriptor ()
%    79 {
%    80   check_no_subscribers();
%    81 
%    82 #ifdef DEAL_II_WITH_CXX11
%    83   object_info = nullptr;
%    84 #else
%    85   object_info = 0;
%    86 #endif
%    87 }
%    88 
%    89 
%    90 void Subscriptor::check_no_subscribers () const
%    91 {
%    92   // Check whether there are still subscriptions to this object. If so, output
%    93   // the actual name of the class to which this object belongs, i.e. the most
%    94   // derived class. Note that the name may be mangled, so it need not be the
%    95   // clear-text class name. However, you can obtain the latter by running the
%    96   // c++filt program over the output.
%    97 #ifdef DEBUG
%    98 
%    99   // If there are still active pointers, show a message and kill the program.
%   100   // However, under some circumstances, this is not so desirable. For example,
%   101   // in code like this:
%   102   //
%   103   //     Triangulation tria;
%   104   //     DoFHandler *dh = new DoFHandler(tria);
%   105   //     ...some function that throws an exception
%   106   //
%   107   // the exception will lead to the destruction of the triangulation, but since
%   108   // the dof_handler is on the heap it will not be destroyed. This will trigger
%   109   // an assertion in the triangulation. If we kill the program at this point, we
%   110   // will never be able to learn what caused the problem. In this situation,
%   111   // just display a message and continue the program.
%   112   if (counter != 0)
%   113     {
%   114       if (std::uncaught_exception() == false)
%   115         {
%   116           std::string infostring;
%   117           for (map_iterator it = counter_map.begin(); it != counter_map.end(); ++it)
%   118             {
%   119               if (it->second > 0)
%   120                 infostring += std::string("\n  from Subscriber ")
%   121                               + std::string(it->first);
%   122             }
%   123 
%   124           if (infostring == "")
%   125             infostring = "<none>";
%   126 
%   127           AssertNothrow (counter == 0,
%   128                          ExcInUse (counter, object_info->name(), infostring));
%   129         }
%   130       else
%   131         {
%   132           std::cerr << "---------------------------------------------------------"
%   133                     << std::endl
%   134                     << "An object pointed to by a SmartPointer is being destroyed."
%   135                     << std::endl
%   136                     << "Under normal circumstances, this would abort the program."
%   137                     << std::endl
%   138                     << "However, another exception is being processed at the"
%   139                     << std::endl
%   140                     << "moment, so the program will continue to run to allow"
%   141                     << std::endl
%   142                     << "this exception to be processed."
%   143                     << std::endl
%   144                     << "---------------------------------------------------------"
%   145                     << std::endl;
%   146         }
%   147     }
%   148 
%   149 #endif
%   150 }
%   151 
%   152 
%   153 
%   154 Subscriptor &Subscriptor::operator = (const Subscriptor &s)
%   155 {
%   156   object_info = s.object_info;
%   157   return *this;
%   158 }
%   159 
%   160 
%   161 
%   162 #ifdef DEAL_II_WITH_CXX11
%   163 Subscriptor &Subscriptor::operator = (Subscriptor &&s)
%   164 {
%   165   s.check_no_subscribers();
%   166   object_info = s.object_info;
%   167   return *this;
%   168 }
%   169 #endif
%   170 
%   171 
%   172 void
%   173 Subscriptor::subscribe(const char *id) const
%   174 {
%   175 #ifdef DEBUG
%   176   if (object_info == 0)
%   177     object_info = &typeid(*this);
%   178   Threads::Mutex::ScopedLock lock (subscription_lock);
%   179   ++counter;
%   180 
%   181 #  ifndef DEAL_II_WITH_THREADS
%   182   const char *const name = (id != 0) ? id : unknown_subscriber;
%   183 
%   184   map_iterator it = counter_map.find(name);
%   185   if (it == counter_map.end())
%   186     counter_map.insert(map_value_type(name, 1U));
%   187 
%   188   else
%   189     it->second++;
%   190 #  else
%   191   (void)id;
%   192 #  endif
%   193 #else
%   194   (void)id;
%   195 #endif
%   196 }
%   197 
%   198 
%   199 void
%   200 Subscriptor::unsubscribe(const char *id) const
%   201 {
%   202 #ifdef DEBUG
%   203   const char *name = (id != 0) ? id : unknown_subscriber;
%   204   AssertNothrow (counter>0, ExcNoSubscriber(object_info->name(), name));
%   205   // This is for the case that we do
%   206   // not abort after the exception
%   207   if (counter == 0)
%   208     return;
%   209 
%   210   Threads::Mutex::ScopedLock lock (subscription_lock);
%   211   --counter;
%   212 
%   213 #  ifndef DEAL_II_WITH_THREADS
%   214   map_iterator it = counter_map.find(name);
%   215   AssertNothrow (it != counter_map.end(), ExcNoSubscriber(object_info->name(), name));
%   216   AssertNothrow (it->second > 0, ExcNoSubscriber(object_info->name(), name));
%   217 
%   218   it->second--;
%   219 #  endif
%   220 #else
%   221   (void)id;
%   222 #endif
%   223 }
%   224 
%   225 
%   226
%   231 
%   232 
%   233 
%   234 void Subscriptor::list_subscribers () const
%   235 {
%   236 #ifndef DEAL_II_WITH_THREADS
%   237   for (map_iterator it = counter_map.begin();
%   238        it != counter_map.end(); ++it)
%   239     deallog << it->second << '/'
%   240             << counter << " subscriptions from \""
%   241             << it->first << '\"' << std::endl;
%   242 #else
%   243   deallog << "No subscriber listing with multithreading" << std::endl;
%   244 #endif
%   245 }
%   246 