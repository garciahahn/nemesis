function out = isbisymmetric(matrix,tol)
%ISBISYMMETRIC Function to verify if the matrix is bisymmetric
%   A matrix is bisymmetric if it is symmetric wrt both diagonals
    
    TOLERANCE = 1e-14;

    if (nargin==2)
        TOLERANCE = tol;
    end
    
    sym1 = issymmetric(matrix);

    sym2 = issymmetric( fliplr(matrix) );
    
    if (~sym2)
        test = fliplr(matrix) - fliplr(matrix)';
        if ( max( abs(test) ) < TOLERANCE )
            sym2 = true;
        else
            sym2 = false;
        end
    end

    out = sym1 * sym2;
end

