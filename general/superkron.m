function [ out ] = superkron( v1,v2,v3,v4 )
%SUPERKRON An implementation of the sparse kronecker product for multidimensional arrays.
%   Provide 3 vectors/arrays

    if nargin==4
        out = kron(v1,superkron(v2,v3,v4));
    elseif nargin==3
        out = kron(v1,superkron(v2,v3));
    elseif nargin == 2
        out = kron(v1,v2);
    elseif nargin == 1
        if iscell(v1)
            switch( numel(v1) )
                case 1
                    out = v1{1};
                case 2 
                    out = superkron(v1{2},v1{1});
                case 3 
                    out = superkron(v1{3},v1{2},v1{1});
                case 4
                    out = superkron(v1{4},v1{3},v1{2},v1{1});
            end
        else
            disp('ERROR in SUPERKRON: unsupported number of arrays')
        end
    end
    
    out = double(out);
    
    if (nnz(out)<numel(out))
        out = sparse(out);
    end
end

