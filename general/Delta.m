function [D,Dx,Dy] = Delta(C,epsilon)
    %HEAVISIDE Summary of this function goes here
    %   Detailed explanation goes here
    
    computeGradients = false;
    option = 1;
    
    if (nargin==1)
        epsilon = 1/160;
    end
        
    D = zeros( size(C) );    
    
    C = C-0.5;
    
    D( C<=-epsilon )      = 0;
    D( abs(C) < epsilon ) = 1/(2*epsilon) * ( 1 + cos( pi*C( abs(C) < epsilon )/epsilon) );
    D( C>=epsilon )       = 0;

%     % Sharp gradients in x and y direction
    if (computeGradients)
        C = reshape(C,[7,7]);
        Dx = zeros( size(C) );
        for j=1:size(C,2)
            for i=1:size(C,1)-1
                if (C(i,j)*C(i+1,j)<=0)
                    deltaC = C(i+1,j) - C(i,j);
                    % Determine the weights of each location, but use the
                    % inverse C to ensure more weight is given to the point
                    % closest to 0
                    switch option
                        case 1
                            Dx(i  ,j) = abs(C(i+1,j)) / deltaC;
                            Dx(i+1,j) = abs(C(i  ,j)) / deltaC;
                        case 2
                            Dx(i  ,j) = sign(deltaC);
                            Dx(i+1,j) = sign(deltaC);
                        case 3
                            if (abs(C(i,j))<abs(C(i+1,j)))
                                Dx(i  ,j) = sign(deltaC);
                            else
                                Dx(i+1,j) = sign(deltaC);
                            end
                    end
                end
            end
        end

        Dy = zeros( size(C) );
        for i=1:size(C,1)
            for j=1:size(C,2)-1
                if (C(i,j)*C(i,j+1)<=0)
                    deltaC = C(i,j+1) - C(i,j);
                    % Determine the weights of each location, but use the
                    % inverse C to ensure more weight is given to the point
                    % closest to 0
                    
                    switch option
                        case 1 % Option 1: distribution by ratio
                            Dy(i,j  ) = abs(C(i,j+1)) / deltaC;
                            Dy(i,j+1) = abs(C(i,j  )) / deltaC;
                    
                        case 2 % Option 2: both location sign function
                            Dy(i,j  ) = sign(deltaC);
                            Dy(i,j+1) = sign(deltaC);
                    
                        case 3 % Option 3: smallest location sign function
                            if (abs(C(i,j))<abs(C(i,j+1)))
                                Dy(i,j  ) = sign(deltaC);
                            else
                                Dy(i,j+1) = sign(deltaC);
                            end
                    end
                end
            end
        end
    else
        Dx = 0;
        Dy = 0;
    end
end

