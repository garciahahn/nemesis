function [D] = spdiag(vec)
    n = length(vec);
    D = spdiags(vec(:),0,n,n);
end