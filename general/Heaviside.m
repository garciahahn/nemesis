function H = Heaviside(C,epsilon)
    %HEAVISIDE Summary of this function goes here
    %   Detailed explanation goes here
    H = zeros( size(C) );
    
    if (nargin==1)
        epsilon = 1/160;
    end
    
    C = C-0.5;
    
    H( C<=-epsilon )      = 0;
    H( abs(C) < epsilon ) = 0.5*( 1 + C( abs(C) < epsilon )/epsilon + sin(pi*C( abs(C) < epsilon )/epsilon) / pi );
    H( C>=epsilon )       = 1;
end

